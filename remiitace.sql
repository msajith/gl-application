--
-- PostgreSQL database dump
--

-- Dumped from database version 13.2
-- Dumped by pg_dump version 13.2

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- Data for Name: remittance_transaction; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO public.remittance_transaction (id, txn_ref_no, account_no, txn_date, txn_date_in_utc, txn_date_in_local, txn_date_for_settlement, value_date, value_date_in_utc, value_date_in_local, value_date_for_settlement, beneficiary_account_no, beneficiary_name, beneficiarybank_swift_code, intermediary_bank_swift_code, beneficiary_bank_name, beneficiary_bank_branch_name, beneficiary_address_1, beneficiary_address_2, beneficiary_address_3, beneficiary_state, beneficiary_pin_code, txn_amount, amount_in_account_currency, amount_in_local_currency, charge_in_account_currency, charge_in_local_currency, funding_source, rate, instructed_amount, instructed_amount_currency, merchant_id, merchant_info, external_ref_no, txn_description, txn_internal_description, txn_addnl_info, created_on, created_by, txn_status, txn_debit_account_no, txn_credit_account_no, charge_debit_account_no, charge_credit_account_no, country_id, txn_currency_id, account_currency_id, instructed_currency_id, purpose_of_txn_id, funding_source_type_id, transaction_type_id, customer_id, casa_account_id, beneficiary_id) VALUES (1401, 'CSMCWKIIMB9319', '00012345678998', '2022-01-19', NULL, NULL, NULL, '2022-01-19', NULL, NULL, NULL, '364783264632463287', 'John Sauel', 'BOAUSWEWXX', NULL, 'Bank of America', 'Newyork', '102 MBH BUILDING', 'MUROOR ROAD', NULL, 'Newyork', NULL, 150.00, 2326.30, NULL, 100.00, NULL, NULL, 0.06448, NULL, NULL, NULL, NULL, NULL, 'test transaction2', NULL, NULL, NULL, NULL, 'ACTIVE', NULL, NULL, NULL, NULL, 34, 6, NULL, NULL, 1, 1, 4, 1002, 1102, 1001);


--
-- PostgreSQL database dump complete
--

