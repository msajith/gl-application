--
-- PostgreSQL database dump
--

-- Dumped from database version 13.2
-- Dumped by pg_dump version 13.2

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- Data for Name: account_type; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO public.account_type (id, name, code, gl_account_type) VALUES (1, 'Savings', 'SAV', 'LIABILITY');
INSERT INTO public.account_type (id, name, code, gl_account_type) VALUES (2, 'Current', 'CUR', 'LIABILITY');
INSERT INTO public.account_type (id, name, code, gl_account_type) VALUES (3, 'Wallet', 'WAL', 'LIABILITY');
INSERT INTO public.account_type (id, name, code, gl_account_type) VALUES (4, 'Loans', 'LOANS', 'ASSET');
INSERT INTO public.account_type (id, name, code, gl_account_type) VALUES (5, 'Sub GL', 'CUR', 'LIABILITY');


--
-- PostgreSQL database dump complete
--

