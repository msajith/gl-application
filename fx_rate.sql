--
-- PostgreSQL database dump
--

-- Dumped from database version 13.2
-- Dumped by pg_dump version 13.2

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- Data for Name: fx_rate; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO public.fx_rate (id, type, base_currency, quote_currency, ask_price, bid_price, mid_price, ask_margin, bid_margin, segment, amount_slab, amount_slab_value, amount_slab_operator, amount_slab_currency, record_status, validfrom, valid_to) VALUES (1001, 'SPOT', 'ZAR', 'USD', 0.07, 0.06, 0.06, 0.00, 0.00, 'Retail', '<=500ZAR', 500.00, '<=', 'ZAR', 'ACTIVE', '2022-01-01', '2026-01-01');


--
-- PostgreSQL database dump complete
--

