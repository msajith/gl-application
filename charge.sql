--
-- PostgreSQL database dump
--

-- Dumped from database version 13.2
-- Dumped by pg_dump version 13.2

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- Data for Name: charge; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO public.charge (id, charge_amount_local_ccy, charge_amount_txn_ccy, amount_slab, amount_slab_value, amount_slab_operator, amount_slab_currency, record_status, validfrom, valid_to, charge_type_id, transaction_type_id, txn_country_id, beneficiary_country_id, base_currency_id, txn_currency_id, charge_account_id, customer_segment_id) VALUES (1051, 100.00, 100.00, '>=5000 RND', 5000.00, '>=', 'RND', 'ACTIVE', '2022-01-01', '2026-01-01', 1001, 9, 32, 34, 7, 6, 1206, 1);


--
-- PostgreSQL database dump complete
--

