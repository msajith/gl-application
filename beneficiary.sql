--
-- PostgreSQL database dump
--

-- Dumped from database version 13.2
-- Dumped by pg_dump version 13.2

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- Data for Name: beneficiary; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO public.beneficiary (id, title, beneficiary_name, beneficiary_account_no, beneficiarybank_swift_code, intermediary_bank_swift_code, beneficiary_bank_name, beneficiary_bank_branch_name, beneficiary_address_1, beneficiary_address_2, beneficiary_address_3, beneficiary_state, beneficiary_pin_code, created_on, created_by, country_id, currency_id, customer_id) VALUES (1001, 'Father', 'John Sauel', '364783264632463287', 'BOAUSWEWXX', '', 'Bank of America', 'Newyork', '102 MBH BUILDING', 'MUROOR ROAD', 'asdasdasd', 'Newyork', '111232', '2022-01-16 20:00:00', NULL, 34, NULL, 1002);


--
-- PostgreSQL database dump complete
--

