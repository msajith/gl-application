--
-- PostgreSQL database dump
--

-- Dumped from database version 13.2
-- Dumped by pg_dump version 13.2

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- Data for Name: casa_account; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO public.casa_account (id, account_no, account_name, date, closing_balance, opening_balance, total_debit, total_credit, available_balance, current_balance, created_on, created_by, account_status, account_type_id, account_category_id, account_currency_id, customer_id) VALUES (1102, '00012345678998', 'Trang K [Primary]- ZAR', NULL, 0.00, 0.00, 0.00, 0.00, 5000000000.00, 5000000000.00, '2019-04-24 02:57:00', 'Ahmed Farook', 'ACTIVE', 1, 1, 7, 1002);
INSERT INTO public.casa_account (id, account_no, account_name, date, closing_balance, opening_balance, total_debit, total_credit, available_balance, current_balance, created_on, created_by, account_status, account_type_id, account_category_id, account_currency_id, customer_id) VALUES (1103, '00098334334444', 'Trang K [Secondary]- ZAR', NULL, 0.00, 0.00, 0.00, 0.00, 2500000000.00, 2500000000.00, '2019-04-24 02:57:00', 'Ahmed Farook', 'ACTIVE', 1, 1, 7, 1002);
INSERT INTO public.casa_account (id, account_no, account_name, date, closing_balance, opening_balance, total_debit, total_credit, available_balance, current_balance, created_on, created_by, account_status, account_type_id, account_category_id, account_currency_id, customer_id) VALUES (1104, '00065734161983', 'Sam Varghese - ZAR', NULL, 0.00, 0.00, 0.00, 0.00, 1000000000.00, 1000000000.00, '2019-04-24 02:57:00', 'Ahmed Farook', 'ACTIVE', 1, 1, 7, 1003);
INSERT INTO public.casa_account (id, account_no, account_name, date, closing_balance, opening_balance, total_debit, total_credit, available_balance, current_balance, created_on, created_by, account_status, account_type_id, account_category_id, account_currency_id, customer_id) VALUES (1202, '999-9875653343', 'Card Center - Merchant Settelement', NULL, 0.00, 0.00, 0.00, 0.00, 250000.00, 250000.00, '2019-04-24 03:07:00', 'Aslam', 'ACTIVE', 5, 1, 7, 5000);
INSERT INTO public.casa_account (id, account_no, account_name, date, closing_balance, opening_balance, total_debit, total_credit, available_balance, current_balance, created_on, created_by, account_status, account_type_id, account_category_id, account_currency_id, customer_id) VALUES (1203, '999-9910101010', 'Wallet - Collection Account USD', NULL, 0.00, 0.00, 0.00, 0.00, 250000.00, 250000.00, '2019-04-24 03:07:00', 'Aslam', 'ACTIVE', 5, 1, 6, 5000);
INSERT INTO public.casa_account (id, account_no, account_name, date, closing_balance, opening_balance, total_debit, total_credit, available_balance, current_balance, created_on, created_by, account_status, account_type_id, account_category_id, account_currency_id, customer_id) VALUES (1204, '999-9910101011', 'Wallet - Collection Account RND', NULL, 0.00, 0.00, 0.00, 0.00, 250000.00, 250000.00, '2019-04-24 03:07:00', 'Aslam', 'ACTIVE', 5, 1, 7, 5000);
INSERT INTO public.casa_account (id, account_no, account_name, date, closing_balance, opening_balance, total_debit, total_credit, available_balance, current_balance, created_on, created_by, account_status, account_type_id, account_category_id, account_currency_id, customer_id) VALUES (1205, '999-9910101012', 'Wallet - Collection Account AED', NULL, 0.00, 0.00, 0.00, 0.00, 250000.00, 250000.00, '2019-04-24 03:07:00', 'Aslam', 'ACTIVE', 5, 1, 3, 5000);
INSERT INTO public.casa_account (id, account_no, account_name, date, closing_balance, opening_balance, total_debit, total_credit, available_balance, current_balance, created_on, created_by, account_status, account_type_id, account_category_id, account_currency_id, customer_id) VALUES (1101, '111-43434343434', 'South African Airlines PJSC - ZAR', NULL, 0.00, 0.00, 0.00, 0.00, 10000000000.00, 10000000000.00, '2019-04-24 02:57:00', 'Ahmed Farook', 'ACTIVE', 2, 1, 7, 1001);
INSERT INTO public.casa_account (id, account_no, account_name, date, closing_balance, opening_balance, total_debit, total_credit, available_balance, current_balance, created_on, created_by, account_status, account_type_id, account_category_id, account_currency_id, customer_id) VALUES (1201, '999-7865438390', 'USD Remittance - Nostro (BOA)', NULL, 0.00, 0.00, 0.00, 0.00, 250000.00, 250000.00, '2019-04-24 03:07:00', 'Aslam', 'ACTIVE', 5, 1, 7, 5000);
INSERT INTO public.casa_account (id, account_no, account_name, date, closing_balance, opening_balance, total_debit, total_credit, available_balance, current_balance, created_on, created_by, account_status, account_type_id, account_category_id, account_currency_id, customer_id) VALUES (1206, '888-8810101011', 'Account for USD Remittance- Charges RND', NULL, 0.00, 0.00, 0.00, 0.00, 250000.00, 250000.00, '2019-04-24 03:07:00', 'Aslam', 'ACTIVE', 5, 1, 7, 5000);


--
-- PostgreSQL database dump complete
--

