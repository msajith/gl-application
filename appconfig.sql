--
-- PostgreSQL database dump
--

-- Dumped from database version 13.2
-- Dumped by pg_dump version 13.2

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- Data for Name: app_config; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO public.app_config (id, code, sub_code, value, description) VALUES (1001, 'REMITTANCE_CORR_BANK_AC', 'USD', '999-7865438390', 'Correspondent Partner/Bank Account for USD currency');
INSERT INTO public.app_config (id, code, sub_code, value, description) VALUES (1051, 'REMITTANCE_CHARGE_AC', 'USD', '999-9875653343', 'USD Remittance Charge Account');


--
-- PostgreSQL database dump complete
--

