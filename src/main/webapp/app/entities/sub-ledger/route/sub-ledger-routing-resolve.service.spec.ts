import { TestBed } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { ActivatedRouteSnapshot, ActivatedRoute, Router, convertToParamMap } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { of } from 'rxjs';

import { ISubLedger, SubLedger } from '../sub-ledger.model';
import { SubLedgerService } from '../service/sub-ledger.service';

import { SubLedgerRoutingResolveService } from './sub-ledger-routing-resolve.service';

describe('SubLedger routing resolve service', () => {
  let mockRouter: Router;
  let mockActivatedRouteSnapshot: ActivatedRouteSnapshot;
  let routingResolveService: SubLedgerRoutingResolveService;
  let service: SubLedgerService;
  let resultSubLedger: ISubLedger | undefined;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule, RouterTestingModule.withRoutes([])],
      providers: [
        {
          provide: ActivatedRoute,
          useValue: {
            snapshot: {
              paramMap: convertToParamMap({}),
            },
          },
        },
      ],
    });
    mockRouter = TestBed.inject(Router);
    jest.spyOn(mockRouter, 'navigate').mockImplementation(() => Promise.resolve(true));
    mockActivatedRouteSnapshot = TestBed.inject(ActivatedRoute).snapshot;
    routingResolveService = TestBed.inject(SubLedgerRoutingResolveService);
    service = TestBed.inject(SubLedgerService);
    resultSubLedger = undefined;
  });

  describe('resolve', () => {
    it('should return ISubLedger returned by find', () => {
      // GIVEN
      service.find = jest.fn(id => of(new HttpResponse({ body: { id } })));
      mockActivatedRouteSnapshot.params = { id: 123 };

      // WHEN
      routingResolveService.resolve(mockActivatedRouteSnapshot).subscribe(result => {
        resultSubLedger = result;
      });

      // THEN
      expect(service.find).toBeCalledWith(123);
      expect(resultSubLedger).toEqual({ id: 123 });
    });

    it('should return new ISubLedger if id is not provided', () => {
      // GIVEN
      service.find = jest.fn();
      mockActivatedRouteSnapshot.params = {};

      // WHEN
      routingResolveService.resolve(mockActivatedRouteSnapshot).subscribe(result => {
        resultSubLedger = result;
      });

      // THEN
      expect(service.find).not.toBeCalled();
      expect(resultSubLedger).toEqual(new SubLedger());
    });

    it('should route to 404 page if data not found in server', () => {
      // GIVEN
      jest.spyOn(service, 'find').mockReturnValue(of(new HttpResponse({ body: null as unknown as SubLedger })));
      mockActivatedRouteSnapshot.params = { id: 123 };

      // WHEN
      routingResolveService.resolve(mockActivatedRouteSnapshot).subscribe(result => {
        resultSubLedger = result;
      });

      // THEN
      expect(service.find).toBeCalledWith(123);
      expect(resultSubLedger).toEqual(undefined);
      expect(mockRouter.navigate).toHaveBeenCalledWith(['404']);
    });
  });
});
