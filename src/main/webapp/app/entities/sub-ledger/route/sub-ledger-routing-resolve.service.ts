import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, Router } from '@angular/router';
import { Observable, of, EMPTY } from 'rxjs';
import { mergeMap } from 'rxjs/operators';

import { ISubLedger, SubLedger } from '../sub-ledger.model';
import { SubLedgerService } from '../service/sub-ledger.service';

@Injectable({ providedIn: 'root' })
export class SubLedgerRoutingResolveService implements Resolve<ISubLedger> {
  constructor(protected service: SubLedgerService, protected router: Router) {}

  resolve(route: ActivatedRouteSnapshot): Observable<ISubLedger> | Observable<never> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        mergeMap((subLedger: HttpResponse<SubLedger>) => {
          if (subLedger.body) {
            return of(subLedger.body);
          } else {
            this.router.navigate(['404']);
            return EMPTY;
          }
        })
      );
    }
    return of(new SubLedger());
  }
}
