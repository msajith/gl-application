import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { UserRouteAccessService } from 'app/core/auth/user-route-access.service';
import { SubLedgerComponent } from '../list/sub-ledger.component';
import { SubLedgerDetailComponent } from '../detail/sub-ledger-detail.component';
import { SubLedgerUpdateComponent } from '../update/sub-ledger-update.component';
import { SubLedgerRoutingResolveService } from './sub-ledger-routing-resolve.service';

const subLedgerRoute: Routes = [
  {
    path: '',
    component: SubLedgerComponent,
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/view',
    component: SubLedgerDetailComponent,
    resolve: {
      subLedger: SubLedgerRoutingResolveService,
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: 'new',
    component: SubLedgerUpdateComponent,
    resolve: {
      subLedger: SubLedgerRoutingResolveService,
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/edit',
    component: SubLedgerUpdateComponent,
    resolve: {
      subLedger: SubLedgerRoutingResolveService,
    },
    canActivate: [UserRouteAccessService],
  },
];

@NgModule({
  imports: [RouterModule.forChild(subLedgerRoute)],
  exports: [RouterModule],
})
export class SubLedgerRoutingModule {}
