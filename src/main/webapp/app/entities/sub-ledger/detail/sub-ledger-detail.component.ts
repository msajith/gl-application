import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { ISubLedger } from '../sub-ledger.model';

@Component({
  selector: 'jhi-sub-ledger-detail',
  templateUrl: './sub-ledger-detail.component.html',
})
export class SubLedgerDetailComponent implements OnInit {
  subLedger: ISubLedger | null = null;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ subLedger }) => {
      this.subLedger = subLedger;
    });
  }

  previousState(): void {
    window.history.back();
  }
}
