import dayjs from 'dayjs/esm';
import { EntryStatus } from 'app/entities/enumerations/entry-status.model';
import { GlEntryType } from 'app/entities/enumerations/gl-entry-type.model';

export interface ISubLedger {
  id?: number;
  subLedgerRefNo?: string | null;
  accountNo?: string | null;
  txnDate?: dayjs.Dayjs | null;
  txnDateInUTC?: dayjs.Dayjs | null;
  txnDateInLocal?: dayjs.Dayjs | null;
  txnDateForSettlement?: dayjs.Dayjs | null;
  amountInAccountCurrency?: number | null;
  amountInLocalCurrency?: number | null;
  rate?: number | null;
  entryCategory?: string | null;
  description1?: string | null;
  description2?: string | null;
  createdOn?: dayjs.Dayjs | null;
  createdBy?: string | null;
  entryStatus?: EntryStatus | null;
  glEntryType?: GlEntryType | null;
}

export class SubLedger implements ISubLedger {
  constructor(
    public id?: number,
    public subLedgerRefNo?: string | null,
    public accountNo?: string | null,
    public txnDate?: dayjs.Dayjs | null,
    public txnDateInUTC?: dayjs.Dayjs | null,
    public txnDateInLocal?: dayjs.Dayjs | null,
    public txnDateForSettlement?: dayjs.Dayjs | null,
    public amountInAccountCurrency?: number | null,
    public amountInLocalCurrency?: number | null,
    public rate?: number | null,
    public entryCategory?: string | null,
    public description1?: string | null,
    public description2?: string | null,
    public createdOn?: dayjs.Dayjs | null,
    public createdBy?: string | null,
    public entryStatus?: EntryStatus | null,
    public glEntryType?: GlEntryType | null
  ) {}
}

export function getSubLedgerIdentifier(subLedger: ISubLedger): number | undefined {
  return subLedger.id;
}
