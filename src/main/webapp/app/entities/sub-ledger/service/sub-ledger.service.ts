import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import dayjs from 'dayjs/esm';

import { isPresent } from 'app/core/util/operators';
import { DATE_FORMAT } from 'app/config/input.constants';
import { ApplicationConfigService } from 'app/core/config/application-config.service';
import { createRequestOption } from 'app/core/request/request-util';
import { ISubLedger, getSubLedgerIdentifier } from '../sub-ledger.model';

export type EntityResponseType = HttpResponse<ISubLedger>;
export type EntityArrayResponseType = HttpResponse<ISubLedger[]>;

@Injectable({ providedIn: 'root' })
export class SubLedgerService {
  protected resourceUrl = this.applicationConfigService.getEndpointFor('api/sub-ledgers');

  constructor(protected http: HttpClient, protected applicationConfigService: ApplicationConfigService) {}

  create(subLedger: ISubLedger): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(subLedger);
    return this.http
      .post<ISubLedger>(this.resourceUrl, copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  update(subLedger: ISubLedger): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(subLedger);
    return this.http
      .put<ISubLedger>(`${this.resourceUrl}/${getSubLedgerIdentifier(subLedger) as number}`, copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  partialUpdate(subLedger: ISubLedger): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(subLedger);
    return this.http
      .patch<ISubLedger>(`${this.resourceUrl}/${getSubLedgerIdentifier(subLedger) as number}`, copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http
      .get<ISubLedger>(`${this.resourceUrl}/${id}`, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http
      .get<ISubLedger[]>(this.resourceUrl, { params: options, observe: 'response' })
      .pipe(map((res: EntityArrayResponseType) => this.convertDateArrayFromServer(res)));
  }

  delete(id: number): Observable<HttpResponse<{}>> {
    return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  addSubLedgerToCollectionIfMissing(
    subLedgerCollection: ISubLedger[],
    ...subLedgersToCheck: (ISubLedger | null | undefined)[]
  ): ISubLedger[] {
    const subLedgers: ISubLedger[] = subLedgersToCheck.filter(isPresent);
    if (subLedgers.length > 0) {
      const subLedgerCollectionIdentifiers = subLedgerCollection.map(subLedgerItem => getSubLedgerIdentifier(subLedgerItem)!);
      const subLedgersToAdd = subLedgers.filter(subLedgerItem => {
        const subLedgerIdentifier = getSubLedgerIdentifier(subLedgerItem);
        if (subLedgerIdentifier == null || subLedgerCollectionIdentifiers.includes(subLedgerIdentifier)) {
          return false;
        }
        subLedgerCollectionIdentifiers.push(subLedgerIdentifier);
        return true;
      });
      return [...subLedgersToAdd, ...subLedgerCollection];
    }
    return subLedgerCollection;
  }

  protected convertDateFromClient(subLedger: ISubLedger): ISubLedger {
    return Object.assign({}, subLedger, {
      txnDate: subLedger.txnDate?.isValid() ? subLedger.txnDate.format(DATE_FORMAT) : undefined,
      txnDateInUTC: subLedger.txnDateInUTC?.isValid() ? subLedger.txnDateInUTC.toJSON() : undefined,
      txnDateInLocal: subLedger.txnDateInLocal?.isValid() ? subLedger.txnDateInLocal.toJSON() : undefined,
      txnDateForSettlement: subLedger.txnDateForSettlement?.isValid() ? subLedger.txnDateForSettlement.toJSON() : undefined,
      createdOn: subLedger.createdOn?.isValid() ? subLedger.createdOn.toJSON() : undefined,
    });
  }

  protected convertDateFromServer(res: EntityResponseType): EntityResponseType {
    if (res.body) {
      res.body.txnDate = res.body.txnDate ? dayjs(res.body.txnDate) : undefined;
      res.body.txnDateInUTC = res.body.txnDateInUTC ? dayjs(res.body.txnDateInUTC) : undefined;
      res.body.txnDateInLocal = res.body.txnDateInLocal ? dayjs(res.body.txnDateInLocal) : undefined;
      res.body.txnDateForSettlement = res.body.txnDateForSettlement ? dayjs(res.body.txnDateForSettlement) : undefined;
      res.body.createdOn = res.body.createdOn ? dayjs(res.body.createdOn) : undefined;
    }
    return res;
  }

  protected convertDateArrayFromServer(res: EntityArrayResponseType): EntityArrayResponseType {
    if (res.body) {
      res.body.forEach((subLedger: ISubLedger) => {
        subLedger.txnDate = subLedger.txnDate ? dayjs(subLedger.txnDate) : undefined;
        subLedger.txnDateInUTC = subLedger.txnDateInUTC ? dayjs(subLedger.txnDateInUTC) : undefined;
        subLedger.txnDateInLocal = subLedger.txnDateInLocal ? dayjs(subLedger.txnDateInLocal) : undefined;
        subLedger.txnDateForSettlement = subLedger.txnDateForSettlement ? dayjs(subLedger.txnDateForSettlement) : undefined;
        subLedger.createdOn = subLedger.createdOn ? dayjs(subLedger.createdOn) : undefined;
      });
    }
    return res;
  }
}
