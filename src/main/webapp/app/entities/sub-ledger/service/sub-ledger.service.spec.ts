import { TestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import dayjs from 'dayjs/esm';

import { DATE_FORMAT, DATE_TIME_FORMAT } from 'app/config/input.constants';
import { EntryStatus } from 'app/entities/enumerations/entry-status.model';
import { GlEntryType } from 'app/entities/enumerations/gl-entry-type.model';
import { ISubLedger, SubLedger } from '../sub-ledger.model';

import { SubLedgerService } from './sub-ledger.service';

describe('SubLedger Service', () => {
  let service: SubLedgerService;
  let httpMock: HttpTestingController;
  let elemDefault: ISubLedger;
  let expectedResult: ISubLedger | ISubLedger[] | boolean | null;
  let currentDate: dayjs.Dayjs;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
    });
    expectedResult = null;
    service = TestBed.inject(SubLedgerService);
    httpMock = TestBed.inject(HttpTestingController);
    currentDate = dayjs();

    elemDefault = {
      id: 0,
      subLedgerRefNo: 'AAAAAAA',
      accountNo: 'AAAAAAA',
      txnDate: currentDate,
      txnDateInUTC: currentDate,
      txnDateInLocal: currentDate,
      txnDateForSettlement: currentDate,
      amountInAccountCurrency: 0,
      amountInLocalCurrency: 0,
      rate: 0,
      entryCategory: 'AAAAAAA',
      description1: 'AAAAAAA',
      description2: 'AAAAAAA',
      createdOn: currentDate,
      createdBy: 'AAAAAAA',
      entryStatus: EntryStatus.ACTIVE,
      glEntryType: GlEntryType.DR,
    };
  });

  describe('Service methods', () => {
    it('should find an element', () => {
      const returnedFromService = Object.assign(
        {
          txnDate: currentDate.format(DATE_FORMAT),
          txnDateInUTC: currentDate.format(DATE_TIME_FORMAT),
          txnDateInLocal: currentDate.format(DATE_TIME_FORMAT),
          txnDateForSettlement: currentDate.format(DATE_TIME_FORMAT),
          createdOn: currentDate.format(DATE_TIME_FORMAT),
        },
        elemDefault
      );

      service.find(123).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'GET' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(elemDefault);
    });

    it('should create a SubLedger', () => {
      const returnedFromService = Object.assign(
        {
          id: 0,
          txnDate: currentDate.format(DATE_FORMAT),
          txnDateInUTC: currentDate.format(DATE_TIME_FORMAT),
          txnDateInLocal: currentDate.format(DATE_TIME_FORMAT),
          txnDateForSettlement: currentDate.format(DATE_TIME_FORMAT),
          createdOn: currentDate.format(DATE_TIME_FORMAT),
        },
        elemDefault
      );

      const expected = Object.assign(
        {
          txnDate: currentDate,
          txnDateInUTC: currentDate,
          txnDateInLocal: currentDate,
          txnDateForSettlement: currentDate,
          createdOn: currentDate,
        },
        returnedFromService
      );

      service.create(new SubLedger()).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'POST' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(expected);
    });

    it('should update a SubLedger', () => {
      const returnedFromService = Object.assign(
        {
          id: 1,
          subLedgerRefNo: 'BBBBBB',
          accountNo: 'BBBBBB',
          txnDate: currentDate.format(DATE_FORMAT),
          txnDateInUTC: currentDate.format(DATE_TIME_FORMAT),
          txnDateInLocal: currentDate.format(DATE_TIME_FORMAT),
          txnDateForSettlement: currentDate.format(DATE_TIME_FORMAT),
          amountInAccountCurrency: 1,
          amountInLocalCurrency: 1,
          rate: 1,
          entryCategory: 'BBBBBB',
          description1: 'BBBBBB',
          description2: 'BBBBBB',
          createdOn: currentDate.format(DATE_TIME_FORMAT),
          createdBy: 'BBBBBB',
          entryStatus: 'BBBBBB',
          glEntryType: 'BBBBBB',
        },
        elemDefault
      );

      const expected = Object.assign(
        {
          txnDate: currentDate,
          txnDateInUTC: currentDate,
          txnDateInLocal: currentDate,
          txnDateForSettlement: currentDate,
          createdOn: currentDate,
        },
        returnedFromService
      );

      service.update(expected).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'PUT' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(expected);
    });

    it('should partial update a SubLedger', () => {
      const patchObject = Object.assign(
        {
          subLedgerRefNo: 'BBBBBB',
          accountNo: 'BBBBBB',
          txnDate: currentDate.format(DATE_FORMAT),
          txnDateInUTC: currentDate.format(DATE_TIME_FORMAT),
          amountInAccountCurrency: 1,
          amountInLocalCurrency: 1,
          rate: 1,
          entryCategory: 'BBBBBB',
          description1: 'BBBBBB',
        },
        new SubLedger()
      );

      const returnedFromService = Object.assign(patchObject, elemDefault);

      const expected = Object.assign(
        {
          txnDate: currentDate,
          txnDateInUTC: currentDate,
          txnDateInLocal: currentDate,
          txnDateForSettlement: currentDate,
          createdOn: currentDate,
        },
        returnedFromService
      );

      service.partialUpdate(patchObject).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'PATCH' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(expected);
    });

    it('should return a list of SubLedger', () => {
      const returnedFromService = Object.assign(
        {
          id: 1,
          subLedgerRefNo: 'BBBBBB',
          accountNo: 'BBBBBB',
          txnDate: currentDate.format(DATE_FORMAT),
          txnDateInUTC: currentDate.format(DATE_TIME_FORMAT),
          txnDateInLocal: currentDate.format(DATE_TIME_FORMAT),
          txnDateForSettlement: currentDate.format(DATE_TIME_FORMAT),
          amountInAccountCurrency: 1,
          amountInLocalCurrency: 1,
          rate: 1,
          entryCategory: 'BBBBBB',
          description1: 'BBBBBB',
          description2: 'BBBBBB',
          createdOn: currentDate.format(DATE_TIME_FORMAT),
          createdBy: 'BBBBBB',
          entryStatus: 'BBBBBB',
          glEntryType: 'BBBBBB',
        },
        elemDefault
      );

      const expected = Object.assign(
        {
          txnDate: currentDate,
          txnDateInUTC: currentDate,
          txnDateInLocal: currentDate,
          txnDateForSettlement: currentDate,
          createdOn: currentDate,
        },
        returnedFromService
      );

      service.query().subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'GET' });
      req.flush([returnedFromService]);
      httpMock.verify();
      expect(expectedResult).toContainEqual(expected);
    });

    it('should delete a SubLedger', () => {
      service.delete(123).subscribe(resp => (expectedResult = resp.ok));

      const req = httpMock.expectOne({ method: 'DELETE' });
      req.flush({ status: 200 });
      expect(expectedResult);
    });

    describe('addSubLedgerToCollectionIfMissing', () => {
      it('should add a SubLedger to an empty array', () => {
        const subLedger: ISubLedger = { id: 123 };
        expectedResult = service.addSubLedgerToCollectionIfMissing([], subLedger);
        expect(expectedResult).toHaveLength(1);
        expect(expectedResult).toContain(subLedger);
      });

      it('should not add a SubLedger to an array that contains it', () => {
        const subLedger: ISubLedger = { id: 123 };
        const subLedgerCollection: ISubLedger[] = [
          {
            ...subLedger,
          },
          { id: 456 },
        ];
        expectedResult = service.addSubLedgerToCollectionIfMissing(subLedgerCollection, subLedger);
        expect(expectedResult).toHaveLength(2);
      });

      it("should add a SubLedger to an array that doesn't contain it", () => {
        const subLedger: ISubLedger = { id: 123 };
        const subLedgerCollection: ISubLedger[] = [{ id: 456 }];
        expectedResult = service.addSubLedgerToCollectionIfMissing(subLedgerCollection, subLedger);
        expect(expectedResult).toHaveLength(2);
        expect(expectedResult).toContain(subLedger);
      });

      it('should add only unique SubLedger to an array', () => {
        const subLedgerArray: ISubLedger[] = [{ id: 123 }, { id: 456 }, { id: 1458 }];
        const subLedgerCollection: ISubLedger[] = [{ id: 123 }];
        expectedResult = service.addSubLedgerToCollectionIfMissing(subLedgerCollection, ...subLedgerArray);
        expect(expectedResult).toHaveLength(3);
      });

      it('should accept varargs', () => {
        const subLedger: ISubLedger = { id: 123 };
        const subLedger2: ISubLedger = { id: 456 };
        expectedResult = service.addSubLedgerToCollectionIfMissing([], subLedger, subLedger2);
        expect(expectedResult).toHaveLength(2);
        expect(expectedResult).toContain(subLedger);
        expect(expectedResult).toContain(subLedger2);
      });

      it('should accept null and undefined values', () => {
        const subLedger: ISubLedger = { id: 123 };
        expectedResult = service.addSubLedgerToCollectionIfMissing([], null, subLedger, undefined);
        expect(expectedResult).toHaveLength(1);
        expect(expectedResult).toContain(subLedger);
      });

      it('should return initial array if no SubLedger is added', () => {
        const subLedgerCollection: ISubLedger[] = [{ id: 123 }];
        expectedResult = service.addSubLedgerToCollectionIfMissing(subLedgerCollection, undefined, null);
        expect(expectedResult).toEqual(subLedgerCollection);
      });
    });
  });

  afterEach(() => {
    httpMock.verify();
  });
});
