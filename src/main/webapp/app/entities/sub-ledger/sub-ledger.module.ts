import { NgModule } from '@angular/core';
import { SharedModule } from 'app/shared/shared.module';
import { SubLedgerComponent } from './list/sub-ledger.component';
import { SubLedgerDetailComponent } from './detail/sub-ledger-detail.component';
import { SubLedgerUpdateComponent } from './update/sub-ledger-update.component';
import { SubLedgerDeleteDialogComponent } from './delete/sub-ledger-delete-dialog.component';
import { SubLedgerRoutingModule } from './route/sub-ledger-routing.module';

@NgModule({
  imports: [SharedModule, SubLedgerRoutingModule],
  declarations: [SubLedgerComponent, SubLedgerDetailComponent, SubLedgerUpdateComponent, SubLedgerDeleteDialogComponent],
  entryComponents: [SubLedgerDeleteDialogComponent],
})
export class SubLedgerModule {}
