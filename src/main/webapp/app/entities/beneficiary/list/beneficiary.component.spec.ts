import { ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpHeaders, HttpResponse } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { of } from 'rxjs';

import { BeneficiaryService } from '../service/beneficiary.service';

import { BeneficiaryComponent } from './beneficiary.component';

describe('Beneficiary Management Component', () => {
  let comp: BeneficiaryComponent;
  let fixture: ComponentFixture<BeneficiaryComponent>;
  let service: BeneficiaryService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      declarations: [BeneficiaryComponent],
    })
      .overrideTemplate(BeneficiaryComponent, '')
      .compileComponents();

    fixture = TestBed.createComponent(BeneficiaryComponent);
    comp = fixture.componentInstance;
    service = TestBed.inject(BeneficiaryService);

    const headers = new HttpHeaders();
    jest.spyOn(service, 'query').mockReturnValue(
      of(
        new HttpResponse({
          body: [{ id: 123 }],
          headers,
        })
      )
    );
  });

  it('Should call load all on init', () => {
    // WHEN
    comp.ngOnInit();

    // THEN
    expect(service.query).toHaveBeenCalled();
    expect(comp.beneficiaries?.[0]).toEqual(expect.objectContaining({ id: 123 }));
  });
});
