import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, Router } from '@angular/router';
import { Observable, of, EMPTY } from 'rxjs';
import { mergeMap } from 'rxjs/operators';

import { ICustomerSegment, CustomerSegment } from '../customer-segment.model';
import { CustomerSegmentService } from '../service/customer-segment.service';

@Injectable({ providedIn: 'root' })
export class CustomerSegmentRoutingResolveService implements Resolve<ICustomerSegment> {
  constructor(protected service: CustomerSegmentService, protected router: Router) {}

  resolve(route: ActivatedRouteSnapshot): Observable<ICustomerSegment> | Observable<never> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        mergeMap((customerSegment: HttpResponse<CustomerSegment>) => {
          if (customerSegment.body) {
            return of(customerSegment.body);
          } else {
            this.router.navigate(['404']);
            return EMPTY;
          }
        })
      );
    }
    return of(new CustomerSegment());
  }
}
