import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { UserRouteAccessService } from 'app/core/auth/user-route-access.service';
import { CustomerSegmentComponent } from '../list/customer-segment.component';
import { CustomerSegmentDetailComponent } from '../detail/customer-segment-detail.component';
import { CustomerSegmentUpdateComponent } from '../update/customer-segment-update.component';
import { CustomerSegmentRoutingResolveService } from './customer-segment-routing-resolve.service';

const customerSegmentRoute: Routes = [
  {
    path: '',
    component: CustomerSegmentComponent,
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/view',
    component: CustomerSegmentDetailComponent,
    resolve: {
      customerSegment: CustomerSegmentRoutingResolveService,
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: 'new',
    component: CustomerSegmentUpdateComponent,
    resolve: {
      customerSegment: CustomerSegmentRoutingResolveService,
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/edit',
    component: CustomerSegmentUpdateComponent,
    resolve: {
      customerSegment: CustomerSegmentRoutingResolveService,
    },
    canActivate: [UserRouteAccessService],
  },
];

@NgModule({
  imports: [RouterModule.forChild(customerSegmentRoute)],
  exports: [RouterModule],
})
export class CustomerSegmentRoutingModule {}
