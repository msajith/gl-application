import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

import { ICustomerSegment } from '../customer-segment.model';
import { CustomerSegmentService } from '../service/customer-segment.service';
import { CustomerSegmentDeleteDialogComponent } from '../delete/customer-segment-delete-dialog.component';

@Component({
  selector: 'jhi-customer-segment',
  templateUrl: './customer-segment.component.html',
})
export class CustomerSegmentComponent implements OnInit {
  customerSegments?: ICustomerSegment[];
  isLoading = false;

  constructor(protected customerSegmentService: CustomerSegmentService, protected modalService: NgbModal) {}

  loadAll(): void {
    this.isLoading = true;

    this.customerSegmentService.query().subscribe({
      next: (res: HttpResponse<ICustomerSegment[]>) => {
        this.isLoading = false;
        this.customerSegments = res.body ?? [];
      },
      error: () => {
        this.isLoading = false;
      },
    });
  }

  ngOnInit(): void {
    this.loadAll();
  }

  trackId(index: number, item: ICustomerSegment): number {
    return item.id!;
  }

  delete(customerSegment: ICustomerSegment): void {
    const modalRef = this.modalService.open(CustomerSegmentDeleteDialogComponent, { size: 'lg', backdrop: 'static' });
    modalRef.componentInstance.customerSegment = customerSegment;
    // unsubscribe not needed because closed completes on modal close
    modalRef.closed.subscribe(reason => {
      if (reason === 'deleted') {
        this.loadAll();
      }
    });
  }
}
