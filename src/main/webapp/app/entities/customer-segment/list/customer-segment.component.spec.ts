import { ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpHeaders, HttpResponse } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { of } from 'rxjs';

import { CustomerSegmentService } from '../service/customer-segment.service';

import { CustomerSegmentComponent } from './customer-segment.component';

describe('CustomerSegment Management Component', () => {
  let comp: CustomerSegmentComponent;
  let fixture: ComponentFixture<CustomerSegmentComponent>;
  let service: CustomerSegmentService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      declarations: [CustomerSegmentComponent],
    })
      .overrideTemplate(CustomerSegmentComponent, '')
      .compileComponents();

    fixture = TestBed.createComponent(CustomerSegmentComponent);
    comp = fixture.componentInstance;
    service = TestBed.inject(CustomerSegmentService);

    const headers = new HttpHeaders();
    jest.spyOn(service, 'query').mockReturnValue(
      of(
        new HttpResponse({
          body: [{ id: 123 }],
          headers,
        })
      )
    );
  });

  it('Should call load all on init', () => {
    // WHEN
    comp.ngOnInit();

    // THEN
    expect(service.query).toHaveBeenCalled();
    expect(comp.customerSegments?.[0]).toEqual(expect.objectContaining({ id: 123 }));
  });
});
