import { NgModule } from '@angular/core';
import { SharedModule } from 'app/shared/shared.module';
import { CustomerSegmentComponent } from './list/customer-segment.component';
import { CustomerSegmentDetailComponent } from './detail/customer-segment-detail.component';
import { CustomerSegmentUpdateComponent } from './update/customer-segment-update.component';
import { CustomerSegmentDeleteDialogComponent } from './delete/customer-segment-delete-dialog.component';
import { CustomerSegmentRoutingModule } from './route/customer-segment-routing.module';

@NgModule({
  imports: [SharedModule, CustomerSegmentRoutingModule],
  declarations: [
    CustomerSegmentComponent,
    CustomerSegmentDetailComponent,
    CustomerSegmentUpdateComponent,
    CustomerSegmentDeleteDialogComponent,
  ],
  entryComponents: [CustomerSegmentDeleteDialogComponent],
})
export class CustomerSegmentModule {}
