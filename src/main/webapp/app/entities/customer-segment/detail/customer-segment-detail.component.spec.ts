import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { CustomerSegmentDetailComponent } from './customer-segment-detail.component';

describe('CustomerSegment Management Detail Component', () => {
  let comp: CustomerSegmentDetailComponent;
  let fixture: ComponentFixture<CustomerSegmentDetailComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [CustomerSegmentDetailComponent],
      providers: [
        {
          provide: ActivatedRoute,
          useValue: { data: of({ customerSegment: { id: 123 } }) },
        },
      ],
    })
      .overrideTemplate(CustomerSegmentDetailComponent, '')
      .compileComponents();
    fixture = TestBed.createComponent(CustomerSegmentDetailComponent);
    comp = fixture.componentInstance;
  });

  describe('OnInit', () => {
    it('Should load customerSegment on init', () => {
      // WHEN
      comp.ngOnInit();

      // THEN
      expect(comp.customerSegment).toEqual(expect.objectContaining({ id: 123 }));
    });
  });
});
