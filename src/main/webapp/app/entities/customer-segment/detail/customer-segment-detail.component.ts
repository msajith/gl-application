import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { ICustomerSegment } from '../customer-segment.model';

@Component({
  selector: 'jhi-customer-segment-detail',
  templateUrl: './customer-segment-detail.component.html',
})
export class CustomerSegmentDetailComponent implements OnInit {
  customerSegment: ICustomerSegment | null = null;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ customerSegment }) => {
      this.customerSegment = customerSegment;
    });
  }

  previousState(): void {
    window.history.back();
  }
}
