import { TestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';

import { ICustomerSegment, CustomerSegment } from '../customer-segment.model';

import { CustomerSegmentService } from './customer-segment.service';

describe('CustomerSegment Service', () => {
  let service: CustomerSegmentService;
  let httpMock: HttpTestingController;
  let elemDefault: ICustomerSegment;
  let expectedResult: ICustomerSegment | ICustomerSegment[] | boolean | null;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
    });
    expectedResult = null;
    service = TestBed.inject(CustomerSegmentService);
    httpMock = TestBed.inject(HttpTestingController);

    elemDefault = {
      id: 0,
      name: 'AAAAAAA',
      code: 'AAAAAAA',
    };
  });

  describe('Service methods', () => {
    it('should find an element', () => {
      const returnedFromService = Object.assign({}, elemDefault);

      service.find(123).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'GET' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(elemDefault);
    });

    it('should create a CustomerSegment', () => {
      const returnedFromService = Object.assign(
        {
          id: 0,
        },
        elemDefault
      );

      const expected = Object.assign({}, returnedFromService);

      service.create(new CustomerSegment()).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'POST' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(expected);
    });

    it('should update a CustomerSegment', () => {
      const returnedFromService = Object.assign(
        {
          id: 1,
          name: 'BBBBBB',
          code: 'BBBBBB',
        },
        elemDefault
      );

      const expected = Object.assign({}, returnedFromService);

      service.update(expected).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'PUT' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(expected);
    });

    it('should partial update a CustomerSegment', () => {
      const patchObject = Object.assign(
        {
          name: 'BBBBBB',
        },
        new CustomerSegment()
      );

      const returnedFromService = Object.assign(patchObject, elemDefault);

      const expected = Object.assign({}, returnedFromService);

      service.partialUpdate(patchObject).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'PATCH' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(expected);
    });

    it('should return a list of CustomerSegment', () => {
      const returnedFromService = Object.assign(
        {
          id: 1,
          name: 'BBBBBB',
          code: 'BBBBBB',
        },
        elemDefault
      );

      const expected = Object.assign({}, returnedFromService);

      service.query().subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'GET' });
      req.flush([returnedFromService]);
      httpMock.verify();
      expect(expectedResult).toContainEqual(expected);
    });

    it('should delete a CustomerSegment', () => {
      service.delete(123).subscribe(resp => (expectedResult = resp.ok));

      const req = httpMock.expectOne({ method: 'DELETE' });
      req.flush({ status: 200 });
      expect(expectedResult);
    });

    describe('addCustomerSegmentToCollectionIfMissing', () => {
      it('should add a CustomerSegment to an empty array', () => {
        const customerSegment: ICustomerSegment = { id: 123 };
        expectedResult = service.addCustomerSegmentToCollectionIfMissing([], customerSegment);
        expect(expectedResult).toHaveLength(1);
        expect(expectedResult).toContain(customerSegment);
      });

      it('should not add a CustomerSegment to an array that contains it', () => {
        const customerSegment: ICustomerSegment = { id: 123 };
        const customerSegmentCollection: ICustomerSegment[] = [
          {
            ...customerSegment,
          },
          { id: 456 },
        ];
        expectedResult = service.addCustomerSegmentToCollectionIfMissing(customerSegmentCollection, customerSegment);
        expect(expectedResult).toHaveLength(2);
      });

      it("should add a CustomerSegment to an array that doesn't contain it", () => {
        const customerSegment: ICustomerSegment = { id: 123 };
        const customerSegmentCollection: ICustomerSegment[] = [{ id: 456 }];
        expectedResult = service.addCustomerSegmentToCollectionIfMissing(customerSegmentCollection, customerSegment);
        expect(expectedResult).toHaveLength(2);
        expect(expectedResult).toContain(customerSegment);
      });

      it('should add only unique CustomerSegment to an array', () => {
        const customerSegmentArray: ICustomerSegment[] = [{ id: 123 }, { id: 456 }, { id: 65023 }];
        const customerSegmentCollection: ICustomerSegment[] = [{ id: 123 }];
        expectedResult = service.addCustomerSegmentToCollectionIfMissing(customerSegmentCollection, ...customerSegmentArray);
        expect(expectedResult).toHaveLength(3);
      });

      it('should accept varargs', () => {
        const customerSegment: ICustomerSegment = { id: 123 };
        const customerSegment2: ICustomerSegment = { id: 456 };
        expectedResult = service.addCustomerSegmentToCollectionIfMissing([], customerSegment, customerSegment2);
        expect(expectedResult).toHaveLength(2);
        expect(expectedResult).toContain(customerSegment);
        expect(expectedResult).toContain(customerSegment2);
      });

      it('should accept null and undefined values', () => {
        const customerSegment: ICustomerSegment = { id: 123 };
        expectedResult = service.addCustomerSegmentToCollectionIfMissing([], null, customerSegment, undefined);
        expect(expectedResult).toHaveLength(1);
        expect(expectedResult).toContain(customerSegment);
      });

      it('should return initial array if no CustomerSegment is added', () => {
        const customerSegmentCollection: ICustomerSegment[] = [{ id: 123 }];
        expectedResult = service.addCustomerSegmentToCollectionIfMissing(customerSegmentCollection, undefined, null);
        expect(expectedResult).toEqual(customerSegmentCollection);
      });
    });
  });

  afterEach(() => {
    httpMock.verify();
  });
});
