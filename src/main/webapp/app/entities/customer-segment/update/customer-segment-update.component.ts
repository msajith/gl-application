import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { finalize } from 'rxjs/operators';

import { ICustomerSegment, CustomerSegment } from '../customer-segment.model';
import { CustomerSegmentService } from '../service/customer-segment.service';

@Component({
  selector: 'jhi-customer-segment-update',
  templateUrl: './customer-segment-update.component.html',
})
export class CustomerSegmentUpdateComponent implements OnInit {
  isSaving = false;

  editForm = this.fb.group({
    id: [],
    name: [],
    code: [],
  });

  constructor(
    protected customerSegmentService: CustomerSegmentService,
    protected activatedRoute: ActivatedRoute,
    protected fb: FormBuilder
  ) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ customerSegment }) => {
      this.updateForm(customerSegment);
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const customerSegment = this.createFromForm();
    if (customerSegment.id !== undefined) {
      this.subscribeToSaveResponse(this.customerSegmentService.update(customerSegment));
    } else {
      this.subscribeToSaveResponse(this.customerSegmentService.create(customerSegment));
    }
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<ICustomerSegment>>): void {
    result.pipe(finalize(() => this.onSaveFinalize())).subscribe({
      next: () => this.onSaveSuccess(),
      error: () => this.onSaveError(),
    });
  }

  protected onSaveSuccess(): void {
    this.previousState();
  }

  protected onSaveError(): void {
    // Api for inheritance.
  }

  protected onSaveFinalize(): void {
    this.isSaving = false;
  }

  protected updateForm(customerSegment: ICustomerSegment): void {
    this.editForm.patchValue({
      id: customerSegment.id,
      name: customerSegment.name,
      code: customerSegment.code,
    });
  }

  protected createFromForm(): ICustomerSegment {
    return {
      ...new CustomerSegment(),
      id: this.editForm.get(['id'])!.value,
      name: this.editForm.get(['name'])!.value,
      code: this.editForm.get(['code'])!.value,
    };
  }
}
