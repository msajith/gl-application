export interface ICustomerSegment {
  id?: number;
  name?: string | null;
  code?: string | null;
}

export class CustomerSegment implements ICustomerSegment {
  constructor(public id?: number, public name?: string | null, public code?: string | null) {}
}

export function getCustomerSegmentIdentifier(customerSegment: ICustomerSegment): number | undefined {
  return customerSegment.id;
}
