import dayjs from 'dayjs/esm';
import { ICurrency } from 'app/entities/currency/currency.model';
import { IGlAccountCategory } from 'app/entities/gl-account-category/gl-account-category.model';
import { GlAccountStatus } from 'app/entities/enumerations/gl-account-status.model';
import { GlAccountType } from 'app/entities/enumerations/gl-account-type.model';

export interface IGlAccount {
  id?: number;
  accountNo?: string;
  accountName?: string;
  date?: dayjs.Dayjs | null;
  closingBalance?: number | null;
  openingBalance?: number | null;
  totalDebit?: number | null;
  totalCredit?: number | null;
  availableBalance?: number | null;
  currentBalance?: number | null;
  createdOn?: dayjs.Dayjs | null;
  createdBy?: string | null;
  glAccountStatus?: GlAccountStatus | null;
  glAccountType?: GlAccountType | null;
  accountCurrency?: ICurrency | null;
  glAccountCategory?: IGlAccountCategory | null;
}

export class GlAccount implements IGlAccount {
  constructor(
    public id?: number,
    public accountNo?: string,
    public accountName?: string,
    public date?: dayjs.Dayjs | null,
    public closingBalance?: number | null,
    public openingBalance?: number | null,
    public totalDebit?: number | null,
    public totalCredit?: number | null,
    public availableBalance?: number | null,
    public currentBalance?: number | null,
    public createdOn?: dayjs.Dayjs | null,
    public createdBy?: string | null,
    public glAccountStatus?: GlAccountStatus | null,
    public glAccountType?: GlAccountType | null,
    public accountCurrency?: ICurrency | null,
    public glAccountCategory?: IGlAccountCategory | null
  ) {}
}

export function getGlAccountIdentifier(glAccount: IGlAccount): number | undefined {
  return glAccount.id;
}
