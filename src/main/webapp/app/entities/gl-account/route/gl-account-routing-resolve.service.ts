import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, Router } from '@angular/router';
import { Observable, of, EMPTY } from 'rxjs';
import { mergeMap } from 'rxjs/operators';

import { IGlAccount, GlAccount } from '../gl-account.model';
import { GlAccountService } from '../service/gl-account.service';

@Injectable({ providedIn: 'root' })
export class GlAccountRoutingResolveService implements Resolve<IGlAccount> {
  constructor(protected service: GlAccountService, protected router: Router) {}

  resolve(route: ActivatedRouteSnapshot): Observable<IGlAccount> | Observable<never> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        mergeMap((glAccount: HttpResponse<GlAccount>) => {
          if (glAccount.body) {
            return of(glAccount.body);
          } else {
            this.router.navigate(['404']);
            return EMPTY;
          }
        })
      );
    }
    return of(new GlAccount());
  }
}
