import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

import { IGlAccount } from '../gl-account.model';
import { GlAccountService } from '../service/gl-account.service';
import { GlAccountDeleteDialogComponent } from '../delete/gl-account-delete-dialog.component';

@Component({
  selector: 'jhi-gl-account',
  templateUrl: './gl-account.component.html',
})
export class GlAccountComponent implements OnInit {
  glAccounts?: IGlAccount[];
  isLoading = false;

  constructor(protected glAccountService: GlAccountService, protected modalService: NgbModal) {}

  loadAll(): void {
    this.isLoading = true;

    this.glAccountService.query().subscribe({
      next: (res: HttpResponse<IGlAccount[]>) => {
        this.isLoading = false;
        this.glAccounts = res.body ?? [];
      },
      error: () => {
        this.isLoading = false;
      },
    });
  }

  ngOnInit(): void {
    this.loadAll();
  }

  trackId(index: number, item: IGlAccount): number {
    return item.id!;
  }

  delete(glAccount: IGlAccount): void {
    const modalRef = this.modalService.open(GlAccountDeleteDialogComponent, { size: 'lg', backdrop: 'static' });
    modalRef.componentInstance.glAccount = glAccount;
    // unsubscribe not needed because closed completes on modal close
    modalRef.closed.subscribe(reason => {
      if (reason === 'deleted') {
        this.loadAll();
      }
    });
  }
}
