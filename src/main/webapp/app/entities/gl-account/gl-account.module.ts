import { NgModule } from '@angular/core';
import { SharedModule } from 'app/shared/shared.module';
import { GlAccountComponent } from './list/gl-account.component';
import { GlAccountDetailComponent } from './detail/gl-account-detail.component';
import { GlAccountUpdateComponent } from './update/gl-account-update.component';
import { GlAccountDeleteDialogComponent } from './delete/gl-account-delete-dialog.component';
import { GlAccountRoutingModule } from './route/gl-account-routing.module';

@NgModule({
  imports: [SharedModule, GlAccountRoutingModule],
  declarations: [GlAccountComponent, GlAccountDetailComponent, GlAccountUpdateComponent, GlAccountDeleteDialogComponent],
  entryComponents: [GlAccountDeleteDialogComponent],
})
export class GlAccountModule {}
