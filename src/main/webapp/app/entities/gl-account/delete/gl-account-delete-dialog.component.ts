import { Component } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

import { IGlAccount } from '../gl-account.model';
import { GlAccountService } from '../service/gl-account.service';

@Component({
  templateUrl: './gl-account-delete-dialog.component.html',
})
export class GlAccountDeleteDialogComponent {
  glAccount?: IGlAccount;

  constructor(protected glAccountService: GlAccountService, protected activeModal: NgbActiveModal) {}

  cancel(): void {
    this.activeModal.dismiss();
  }

  confirmDelete(id: number): void {
    this.glAccountService.delete(id).subscribe(() => {
      this.activeModal.close('deleted');
    });
  }
}
