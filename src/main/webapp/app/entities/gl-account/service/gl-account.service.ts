import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import dayjs from 'dayjs/esm';

import { isPresent } from 'app/core/util/operators';
import { DATE_FORMAT } from 'app/config/input.constants';
import { ApplicationConfigService } from 'app/core/config/application-config.service';
import { createRequestOption } from 'app/core/request/request-util';
import { IGlAccount, getGlAccountIdentifier } from '../gl-account.model';

export type EntityResponseType = HttpResponse<IGlAccount>;
export type EntityArrayResponseType = HttpResponse<IGlAccount[]>;

@Injectable({ providedIn: 'root' })
export class GlAccountService {
  protected resourceUrl = this.applicationConfigService.getEndpointFor('api/gl-accounts');

  constructor(protected http: HttpClient, protected applicationConfigService: ApplicationConfigService) {}

  create(glAccount: IGlAccount): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(glAccount);
    return this.http
      .post<IGlAccount>(this.resourceUrl, copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  update(glAccount: IGlAccount): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(glAccount);
    return this.http
      .put<IGlAccount>(`${this.resourceUrl}/${getGlAccountIdentifier(glAccount) as number}`, copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  partialUpdate(glAccount: IGlAccount): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(glAccount);
    return this.http
      .patch<IGlAccount>(`${this.resourceUrl}/${getGlAccountIdentifier(glAccount) as number}`, copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http
      .get<IGlAccount>(`${this.resourceUrl}/${id}`, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http
      .get<IGlAccount[]>(this.resourceUrl, { params: options, observe: 'response' })
      .pipe(map((res: EntityArrayResponseType) => this.convertDateArrayFromServer(res)));
  }

  delete(id: number): Observable<HttpResponse<{}>> {
    return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  addGlAccountToCollectionIfMissing(
    glAccountCollection: IGlAccount[],
    ...glAccountsToCheck: (IGlAccount | null | undefined)[]
  ): IGlAccount[] {
    const glAccounts: IGlAccount[] = glAccountsToCheck.filter(isPresent);
    if (glAccounts.length > 0) {
      const glAccountCollectionIdentifiers = glAccountCollection.map(glAccountItem => getGlAccountIdentifier(glAccountItem)!);
      const glAccountsToAdd = glAccounts.filter(glAccountItem => {
        const glAccountIdentifier = getGlAccountIdentifier(glAccountItem);
        if (glAccountIdentifier == null || glAccountCollectionIdentifiers.includes(glAccountIdentifier)) {
          return false;
        }
        glAccountCollectionIdentifiers.push(glAccountIdentifier);
        return true;
      });
      return [...glAccountsToAdd, ...glAccountCollection];
    }
    return glAccountCollection;
  }

  protected convertDateFromClient(glAccount: IGlAccount): IGlAccount {
    return Object.assign({}, glAccount, {
      date: glAccount.date?.isValid() ? glAccount.date.format(DATE_FORMAT) : undefined,
      createdOn: glAccount.createdOn?.isValid() ? glAccount.createdOn.toJSON() : undefined,
    });
  }

  protected convertDateFromServer(res: EntityResponseType): EntityResponseType {
    if (res.body) {
      res.body.date = res.body.date ? dayjs(res.body.date) : undefined;
      res.body.createdOn = res.body.createdOn ? dayjs(res.body.createdOn) : undefined;
    }
    return res;
  }

  protected convertDateArrayFromServer(res: EntityArrayResponseType): EntityArrayResponseType {
    if (res.body) {
      res.body.forEach((glAccount: IGlAccount) => {
        glAccount.date = glAccount.date ? dayjs(glAccount.date) : undefined;
        glAccount.createdOn = glAccount.createdOn ? dayjs(glAccount.createdOn) : undefined;
      });
    }
    return res;
  }
}
