import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { GlAccountDetailComponent } from './gl-account-detail.component';

describe('GlAccount Management Detail Component', () => {
  let comp: GlAccountDetailComponent;
  let fixture: ComponentFixture<GlAccountDetailComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [GlAccountDetailComponent],
      providers: [
        {
          provide: ActivatedRoute,
          useValue: { data: of({ glAccount: { id: 123 } }) },
        },
      ],
    })
      .overrideTemplate(GlAccountDetailComponent, '')
      .compileComponents();
    fixture = TestBed.createComponent(GlAccountDetailComponent);
    comp = fixture.componentInstance;
  });

  describe('OnInit', () => {
    it('Should load glAccount on init', () => {
      // WHEN
      comp.ngOnInit();

      // THEN
      expect(comp.glAccount).toEqual(expect.objectContaining({ id: 123 }));
    });
  });
});
