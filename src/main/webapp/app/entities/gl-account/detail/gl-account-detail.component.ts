import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IGlAccount } from '../gl-account.model';

@Component({
  selector: 'jhi-gl-account-detail',
  templateUrl: './gl-account-detail.component.html',
})
export class GlAccountDetailComponent implements OnInit {
  glAccount: IGlAccount | null = null;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ glAccount }) => {
      this.glAccount = glAccount;
    });
  }

  previousState(): void {
    window.history.back();
  }
}
