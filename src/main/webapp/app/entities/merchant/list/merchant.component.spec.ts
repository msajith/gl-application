import { ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpHeaders, HttpResponse } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { of } from 'rxjs';

import { MerchantService } from '../service/merchant.service';

import { MerchantComponent } from './merchant.component';

describe('Merchant Management Component', () => {
  let comp: MerchantComponent;
  let fixture: ComponentFixture<MerchantComponent>;
  let service: MerchantService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      declarations: [MerchantComponent],
    })
      .overrideTemplate(MerchantComponent, '')
      .compileComponents();

    fixture = TestBed.createComponent(MerchantComponent);
    comp = fixture.componentInstance;
    service = TestBed.inject(MerchantService);

    const headers = new HttpHeaders();
    jest.spyOn(service, 'query').mockReturnValue(
      of(
        new HttpResponse({
          body: [{ id: 123 }],
          headers,
        })
      )
    );
  });

  it('Should call load all on init', () => {
    // WHEN
    comp.ngOnInit();

    // THEN
    expect(service.query).toHaveBeenCalled();
    expect(comp.merchants?.[0]).toEqual(expect.objectContaining({ id: 123 }));
  });
});
