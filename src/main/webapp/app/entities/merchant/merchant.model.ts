import dayjs from 'dayjs/esm';
import { ICountry } from 'app/entities/country/country.model';
import { MerchantStatus } from 'app/entities/enumerations/merchant-status.model';

export interface IMerchant {
  id?: number;
  merchantID?: string | null;
  merchantName?: string | null;
  merchantCategory?: string | null;
  merchantGLAccountNo?: string | null;
  merchantBankAccountNo?: string | null;
  merchantBankAccountCurrency?: string | null;
  merchantBankCode?: string | null;
  merchantBankName?: string | null;
  merchantAddress?: string | null;
  merchantCity?: string | null;
  createdOn?: dayjs.Dayjs | null;
  createdBy?: string | null;
  merchantStataus?: MerchantStatus | null;
  country?: ICountry | null;
}

export class Merchant implements IMerchant {
  constructor(
    public id?: number,
    public merchantID?: string | null,
    public merchantName?: string | null,
    public merchantCategory?: string | null,
    public merchantGLAccountNo?: string | null,
    public merchantBankAccountNo?: string | null,
    public merchantBankAccountCurrency?: string | null,
    public merchantBankCode?: string | null,
    public merchantBankName?: string | null,
    public merchantAddress?: string | null,
    public merchantCity?: string | null,
    public createdOn?: dayjs.Dayjs | null,
    public createdBy?: string | null,
    public merchantStataus?: MerchantStatus | null,
    public country?: ICountry | null
  ) {}
}

export function getMerchantIdentifier(merchant: IMerchant): number | undefined {
  return merchant.id;
}
