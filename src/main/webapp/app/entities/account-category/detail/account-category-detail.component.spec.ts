import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { AccountCategoryDetailComponent } from './account-category-detail.component';

describe('AccountCategory Management Detail Component', () => {
  let comp: AccountCategoryDetailComponent;
  let fixture: ComponentFixture<AccountCategoryDetailComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [AccountCategoryDetailComponent],
      providers: [
        {
          provide: ActivatedRoute,
          useValue: { data: of({ accountCategory: { id: 123 } }) },
        },
      ],
    })
      .overrideTemplate(AccountCategoryDetailComponent, '')
      .compileComponents();
    fixture = TestBed.createComponent(AccountCategoryDetailComponent);
    comp = fixture.componentInstance;
  });

  describe('OnInit', () => {
    it('Should load accountCategory on init', () => {
      // WHEN
      comp.ngOnInit();

      // THEN
      expect(comp.accountCategory).toEqual(expect.objectContaining({ id: 123 }));
    });
  });
});
