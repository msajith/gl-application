import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IAccountCategory } from '../account-category.model';

@Component({
  selector: 'jhi-account-category-detail',
  templateUrl: './account-category-detail.component.html',
})
export class AccountCategoryDetailComponent implements OnInit {
  accountCategory: IAccountCategory | null = null;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ accountCategory }) => {
      this.accountCategory = accountCategory;
    });
  }

  previousState(): void {
    window.history.back();
  }
}
