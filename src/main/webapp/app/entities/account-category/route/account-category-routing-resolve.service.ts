import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, Router } from '@angular/router';
import { Observable, of, EMPTY } from 'rxjs';
import { mergeMap } from 'rxjs/operators';

import { IAccountCategory, AccountCategory } from '../account-category.model';
import { AccountCategoryService } from '../service/account-category.service';

@Injectable({ providedIn: 'root' })
export class AccountCategoryRoutingResolveService implements Resolve<IAccountCategory> {
  constructor(protected service: AccountCategoryService, protected router: Router) {}

  resolve(route: ActivatedRouteSnapshot): Observable<IAccountCategory> | Observable<never> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        mergeMap((accountCategory: HttpResponse<AccountCategory>) => {
          if (accountCategory.body) {
            return of(accountCategory.body);
          } else {
            this.router.navigate(['404']);
            return EMPTY;
          }
        })
      );
    }
    return of(new AccountCategory());
  }
}
