import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { UserRouteAccessService } from 'app/core/auth/user-route-access.service';
import { AccountCategoryComponent } from '../list/account-category.component';
import { AccountCategoryDetailComponent } from '../detail/account-category-detail.component';
import { AccountCategoryUpdateComponent } from '../update/account-category-update.component';
import { AccountCategoryRoutingResolveService } from './account-category-routing-resolve.service';

const accountCategoryRoute: Routes = [
  {
    path: '',
    component: AccountCategoryComponent,
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/view',
    component: AccountCategoryDetailComponent,
    resolve: {
      accountCategory: AccountCategoryRoutingResolveService,
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: 'new',
    component: AccountCategoryUpdateComponent,
    resolve: {
      accountCategory: AccountCategoryRoutingResolveService,
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/edit',
    component: AccountCategoryUpdateComponent,
    resolve: {
      accountCategory: AccountCategoryRoutingResolveService,
    },
    canActivate: [UserRouteAccessService],
  },
];

@NgModule({
  imports: [RouterModule.forChild(accountCategoryRoute)],
  exports: [RouterModule],
})
export class AccountCategoryRoutingModule {}
