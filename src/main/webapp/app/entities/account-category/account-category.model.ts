export interface IAccountCategory {
  id?: number;
  name?: string | null;
  code?: string | null;
}

export class AccountCategory implements IAccountCategory {
  constructor(public id?: number, public name?: string | null, public code?: string | null) {}
}

export function getAccountCategoryIdentifier(accountCategory: IAccountCategory): number | undefined {
  return accountCategory.id;
}
