import { ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { of, Subject, from } from 'rxjs';

import { AccountCategoryService } from '../service/account-category.service';
import { IAccountCategory, AccountCategory } from '../account-category.model';

import { AccountCategoryUpdateComponent } from './account-category-update.component';

describe('AccountCategory Management Update Component', () => {
  let comp: AccountCategoryUpdateComponent;
  let fixture: ComponentFixture<AccountCategoryUpdateComponent>;
  let activatedRoute: ActivatedRoute;
  let accountCategoryService: AccountCategoryService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule, RouterTestingModule.withRoutes([])],
      declarations: [AccountCategoryUpdateComponent],
      providers: [
        FormBuilder,
        {
          provide: ActivatedRoute,
          useValue: {
            params: from([{}]),
          },
        },
      ],
    })
      .overrideTemplate(AccountCategoryUpdateComponent, '')
      .compileComponents();

    fixture = TestBed.createComponent(AccountCategoryUpdateComponent);
    activatedRoute = TestBed.inject(ActivatedRoute);
    accountCategoryService = TestBed.inject(AccountCategoryService);

    comp = fixture.componentInstance;
  });

  describe('ngOnInit', () => {
    it('Should update editForm', () => {
      const accountCategory: IAccountCategory = { id: 456 };

      activatedRoute.data = of({ accountCategory });
      comp.ngOnInit();

      expect(comp.editForm.value).toEqual(expect.objectContaining(accountCategory));
    });
  });

  describe('save', () => {
    it('Should call update service on save for existing entity', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<AccountCategory>>();
      const accountCategory = { id: 123 };
      jest.spyOn(accountCategoryService, 'update').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ accountCategory });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.next(new HttpResponse({ body: accountCategory }));
      saveSubject.complete();

      // THEN
      expect(comp.previousState).toHaveBeenCalled();
      expect(accountCategoryService.update).toHaveBeenCalledWith(accountCategory);
      expect(comp.isSaving).toEqual(false);
    });

    it('Should call create service on save for new entity', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<AccountCategory>>();
      const accountCategory = new AccountCategory();
      jest.spyOn(accountCategoryService, 'create').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ accountCategory });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.next(new HttpResponse({ body: accountCategory }));
      saveSubject.complete();

      // THEN
      expect(accountCategoryService.create).toHaveBeenCalledWith(accountCategory);
      expect(comp.isSaving).toEqual(false);
      expect(comp.previousState).toHaveBeenCalled();
    });

    it('Should set isSaving to false on error', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<AccountCategory>>();
      const accountCategory = { id: 123 };
      jest.spyOn(accountCategoryService, 'update').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ accountCategory });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.error('This is an error!');

      // THEN
      expect(accountCategoryService.update).toHaveBeenCalledWith(accountCategory);
      expect(comp.isSaving).toEqual(false);
      expect(comp.previousState).not.toHaveBeenCalled();
    });
  });
});
