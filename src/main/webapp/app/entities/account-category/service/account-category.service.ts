import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { isPresent } from 'app/core/util/operators';
import { ApplicationConfigService } from 'app/core/config/application-config.service';
import { createRequestOption } from 'app/core/request/request-util';
import { IAccountCategory, getAccountCategoryIdentifier } from '../account-category.model';

export type EntityResponseType = HttpResponse<IAccountCategory>;
export type EntityArrayResponseType = HttpResponse<IAccountCategory[]>;

@Injectable({ providedIn: 'root' })
export class AccountCategoryService {
  protected resourceUrl = this.applicationConfigService.getEndpointFor('api/account-categories');

  constructor(protected http: HttpClient, protected applicationConfigService: ApplicationConfigService) {}

  create(accountCategory: IAccountCategory): Observable<EntityResponseType> {
    return this.http.post<IAccountCategory>(this.resourceUrl, accountCategory, { observe: 'response' });
  }

  update(accountCategory: IAccountCategory): Observable<EntityResponseType> {
    return this.http.put<IAccountCategory>(
      `${this.resourceUrl}/${getAccountCategoryIdentifier(accountCategory) as number}`,
      accountCategory,
      { observe: 'response' }
    );
  }

  partialUpdate(accountCategory: IAccountCategory): Observable<EntityResponseType> {
    return this.http.patch<IAccountCategory>(
      `${this.resourceUrl}/${getAccountCategoryIdentifier(accountCategory) as number}`,
      accountCategory,
      { observe: 'response' }
    );
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http.get<IAccountCategory>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<IAccountCategory[]>(this.resourceUrl, { params: options, observe: 'response' });
  }

  delete(id: number): Observable<HttpResponse<{}>> {
    return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  addAccountCategoryToCollectionIfMissing(
    accountCategoryCollection: IAccountCategory[],
    ...accountCategoriesToCheck: (IAccountCategory | null | undefined)[]
  ): IAccountCategory[] {
    const accountCategories: IAccountCategory[] = accountCategoriesToCheck.filter(isPresent);
    if (accountCategories.length > 0) {
      const accountCategoryCollectionIdentifiers = accountCategoryCollection.map(
        accountCategoryItem => getAccountCategoryIdentifier(accountCategoryItem)!
      );
      const accountCategoriesToAdd = accountCategories.filter(accountCategoryItem => {
        const accountCategoryIdentifier = getAccountCategoryIdentifier(accountCategoryItem);
        if (accountCategoryIdentifier == null || accountCategoryCollectionIdentifiers.includes(accountCategoryIdentifier)) {
          return false;
        }
        accountCategoryCollectionIdentifiers.push(accountCategoryIdentifier);
        return true;
      });
      return [...accountCategoriesToAdd, ...accountCategoryCollection];
    }
    return accountCategoryCollection;
  }
}
