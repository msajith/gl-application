import { NgModule } from '@angular/core';
import { SharedModule } from 'app/shared/shared.module';
import { AccountCategoryComponent } from './list/account-category.component';
import { AccountCategoryDetailComponent } from './detail/account-category-detail.component';
import { AccountCategoryUpdateComponent } from './update/account-category-update.component';
import { AccountCategoryDeleteDialogComponent } from './delete/account-category-delete-dialog.component';
import { AccountCategoryRoutingModule } from './route/account-category-routing.module';

@NgModule({
  imports: [SharedModule, AccountCategoryRoutingModule],
  declarations: [
    AccountCategoryComponent,
    AccountCategoryDetailComponent,
    AccountCategoryUpdateComponent,
    AccountCategoryDeleteDialogComponent,
  ],
  entryComponents: [AccountCategoryDeleteDialogComponent],
})
export class AccountCategoryModule {}
