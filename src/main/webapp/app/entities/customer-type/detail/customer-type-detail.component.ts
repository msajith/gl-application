import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { ICustomerType } from '../customer-type.model';

@Component({
  selector: 'jhi-customer-type-detail',
  templateUrl: './customer-type-detail.component.html',
})
export class CustomerTypeDetailComponent implements OnInit {
  customerType: ICustomerType | null = null;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ customerType }) => {
      this.customerType = customerType;
    });
  }

  previousState(): void {
    window.history.back();
  }
}
