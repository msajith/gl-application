import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { CustomerTypeDetailComponent } from './customer-type-detail.component';

describe('CustomerType Management Detail Component', () => {
  let comp: CustomerTypeDetailComponent;
  let fixture: ComponentFixture<CustomerTypeDetailComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [CustomerTypeDetailComponent],
      providers: [
        {
          provide: ActivatedRoute,
          useValue: { data: of({ customerType: { id: 123 } }) },
        },
      ],
    })
      .overrideTemplate(CustomerTypeDetailComponent, '')
      .compileComponents();
    fixture = TestBed.createComponent(CustomerTypeDetailComponent);
    comp = fixture.componentInstance;
  });

  describe('OnInit', () => {
    it('Should load customerType on init', () => {
      // WHEN
      comp.ngOnInit();

      // THEN
      expect(comp.customerType).toEqual(expect.objectContaining({ id: 123 }));
    });
  });
});
