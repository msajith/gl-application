import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

import { ICustomerType } from '../customer-type.model';
import { CustomerTypeService } from '../service/customer-type.service';
import { CustomerTypeDeleteDialogComponent } from '../delete/customer-type-delete-dialog.component';

@Component({
  selector: 'jhi-customer-type',
  templateUrl: './customer-type.component.html',
})
export class CustomerTypeComponent implements OnInit {
  customerTypes?: ICustomerType[];
  isLoading = false;

  constructor(protected customerTypeService: CustomerTypeService, protected modalService: NgbModal) {}

  loadAll(): void {
    this.isLoading = true;

    this.customerTypeService.query().subscribe({
      next: (res: HttpResponse<ICustomerType[]>) => {
        this.isLoading = false;
        this.customerTypes = res.body ?? [];
      },
      error: () => {
        this.isLoading = false;
      },
    });
  }

  ngOnInit(): void {
    this.loadAll();
  }

  trackId(index: number, item: ICustomerType): number {
    return item.id!;
  }

  delete(customerType: ICustomerType): void {
    const modalRef = this.modalService.open(CustomerTypeDeleteDialogComponent, { size: 'lg', backdrop: 'static' });
    modalRef.componentInstance.customerType = customerType;
    // unsubscribe not needed because closed completes on modal close
    modalRef.closed.subscribe(reason => {
      if (reason === 'deleted') {
        this.loadAll();
      }
    });
  }
}
