import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

import { ICustomerAddress } from '../customer-address.model';
import { CustomerAddressService } from '../service/customer-address.service';
import { CustomerAddressDeleteDialogComponent } from '../delete/customer-address-delete-dialog.component';

@Component({
  selector: 'jhi-customer-address',
  templateUrl: './customer-address.component.html',
})
export class CustomerAddressComponent implements OnInit {
  customerAddresses?: ICustomerAddress[];
  isLoading = false;

  constructor(protected customerAddressService: CustomerAddressService, protected modalService: NgbModal) {}

  loadAll(): void {
    this.isLoading = true;

    this.customerAddressService.query().subscribe({
      next: (res: HttpResponse<ICustomerAddress[]>) => {
        this.isLoading = false;
        this.customerAddresses = res.body ?? [];
      },
      error: () => {
        this.isLoading = false;
      },
    });
  }

  ngOnInit(): void {
    this.loadAll();
  }

  trackId(index: number, item: ICustomerAddress): number {
    return item.id!;
  }

  delete(customerAddress: ICustomerAddress): void {
    const modalRef = this.modalService.open(CustomerAddressDeleteDialogComponent, { size: 'lg', backdrop: 'static' });
    modalRef.componentInstance.customerAddress = customerAddress;
    // unsubscribe not needed because closed completes on modal close
    modalRef.closed.subscribe(reason => {
      if (reason === 'deleted') {
        this.loadAll();
      }
    });
  }
}
