import dayjs from 'dayjs/esm';
import { ICountry } from 'app/entities/country/country.model';
import { ICustomer } from 'app/entities/customer/customer.model';

export interface ICustomerAddress {
  id?: number;
  title?: string;
  cif?: string;
  address1?: string | null;
  address2?: string | null;
  address3?: string | null;
  city?: string | null;
  createdOn?: dayjs.Dayjs | null;
  createdBy?: string | null;
  country?: ICountry | null;
  customer?: ICustomer | null;
}

export class CustomerAddress implements ICustomerAddress {
  constructor(
    public id?: number,
    public title?: string,
    public cif?: string,
    public address1?: string | null,
    public address2?: string | null,
    public address3?: string | null,
    public city?: string | null,
    public createdOn?: dayjs.Dayjs | null,
    public createdBy?: string | null,
    public country?: ICountry | null,
    public customer?: ICustomer | null
  ) {}
}

export function getCustomerAddressIdentifier(customerAddress: ICustomerAddress): number | undefined {
  return customerAddress.id;
}
