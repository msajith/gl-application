import { ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpHeaders, HttpResponse } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { of } from 'rxjs';

import { WalletTopupTransactionService } from '../service/wallet-topup-transaction.service';

import { WalletTopupTransactionComponent } from './wallet-topup-transaction.component';

describe('WalletTopupTransaction Management Component', () => {
  let comp: WalletTopupTransactionComponent;
  let fixture: ComponentFixture<WalletTopupTransactionComponent>;
  let service: WalletTopupTransactionService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      declarations: [WalletTopupTransactionComponent],
    })
      .overrideTemplate(WalletTopupTransactionComponent, '')
      .compileComponents();

    fixture = TestBed.createComponent(WalletTopupTransactionComponent);
    comp = fixture.componentInstance;
    service = TestBed.inject(WalletTopupTransactionService);

    const headers = new HttpHeaders();
    jest.spyOn(service, 'query').mockReturnValue(
      of(
        new HttpResponse({
          body: [{ id: 123 }],
          headers,
        })
      )
    );
  });

  it('Should call load all on init', () => {
    // WHEN
    comp.ngOnInit();

    // THEN
    expect(service.query).toHaveBeenCalled();
    expect(comp.walletTopupTransactions?.[0]).toEqual(expect.objectContaining({ id: 123 }));
  });
});
