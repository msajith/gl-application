import { TestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import dayjs from 'dayjs/esm';

import { DATE_FORMAT, DATE_TIME_FORMAT } from 'app/config/input.constants';
import { TxnStatus } from 'app/entities/enumerations/txn-status.model';
import { IWalletTopupTransaction, WalletTopupTransaction } from '../wallet-topup-transaction.model';

import { WalletTopupTransactionService } from './wallet-topup-transaction.service';

describe('WalletTopupTransaction Service', () => {
  let service: WalletTopupTransactionService;
  let httpMock: HttpTestingController;
  let elemDefault: IWalletTopupTransaction;
  let expectedResult: IWalletTopupTransaction | IWalletTopupTransaction[] | boolean | null;
  let currentDate: dayjs.Dayjs;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
    });
    expectedResult = null;
    service = TestBed.inject(WalletTopupTransactionService);
    httpMock = TestBed.inject(HttpTestingController);
    currentDate = dayjs();

    elemDefault = {
      id: 0,
      txnRefNo: 'AAAAAAA',
      accountNo: 'AAAAAAA',
      txnDate: currentDate,
      txnDateInUTC: currentDate,
      txnDateInLocal: currentDate,
      txnDateForSettlement: currentDate,
      valueDate: currentDate,
      valueDateInUTC: currentDate,
      valueDateInLocal: currentDate,
      valueDateForSettlement: currentDate,
      txnAmount: 0,
      instructedAmount: 0,
      amountInAccountCurrency: 0,
      amountInLocalCurrency: 0,
      chargeInAccountCurrency: 0,
      chargeInLocalCurrency: 0,
      fundingSource: 'AAAAAAA',
      merchantID: 'AAAAAAA',
      merchantInfo: 'AAAAAAA',
      bankCode: 'AAAAAAA',
      externalRefNo: 'AAAAAAA',
      txnDescription: 'AAAAAAA',
      txnInternalDescription: 'AAAAAAA',
      txnAddnlInfo: 'AAAAAAA',
      createdOn: currentDate,
      createdBy: 'AAAAAAA',
      txnStatus: TxnStatus.ACTIVE,
      txnDebitAccountNo: 'AAAAAAA',
      txnCreditAccountNo: 'AAAAAAA',
      chargeDebitAccountNo: 'AAAAAAA',
      chargeCreditAccountNo: 'AAAAAAA',
    };
  });

  describe('Service methods', () => {
    it('should find an element', () => {
      const returnedFromService = Object.assign(
        {
          txnDate: currentDate.format(DATE_FORMAT),
          txnDateInUTC: currentDate.format(DATE_TIME_FORMAT),
          txnDateInLocal: currentDate.format(DATE_TIME_FORMAT),
          txnDateForSettlement: currentDate.format(DATE_TIME_FORMAT),
          valueDate: currentDate.format(DATE_FORMAT),
          valueDateInUTC: currentDate.format(DATE_TIME_FORMAT),
          valueDateInLocal: currentDate.format(DATE_TIME_FORMAT),
          valueDateForSettlement: currentDate.format(DATE_TIME_FORMAT),
          createdOn: currentDate.format(DATE_TIME_FORMAT),
        },
        elemDefault
      );

      service.find(123).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'GET' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(elemDefault);
    });

    it('should create a WalletTopupTransaction', () => {
      const returnedFromService = Object.assign(
        {
          id: 0,
          txnDate: currentDate.format(DATE_FORMAT),
          txnDateInUTC: currentDate.format(DATE_TIME_FORMAT),
          txnDateInLocal: currentDate.format(DATE_TIME_FORMAT),
          txnDateForSettlement: currentDate.format(DATE_TIME_FORMAT),
          valueDate: currentDate.format(DATE_FORMAT),
          valueDateInUTC: currentDate.format(DATE_TIME_FORMAT),
          valueDateInLocal: currentDate.format(DATE_TIME_FORMAT),
          valueDateForSettlement: currentDate.format(DATE_TIME_FORMAT),
          createdOn: currentDate.format(DATE_TIME_FORMAT),
        },
        elemDefault
      );

      const expected = Object.assign(
        {
          txnDate: currentDate,
          txnDateInUTC: currentDate,
          txnDateInLocal: currentDate,
          txnDateForSettlement: currentDate,
          valueDate: currentDate,
          valueDateInUTC: currentDate,
          valueDateInLocal: currentDate,
          valueDateForSettlement: currentDate,
          createdOn: currentDate,
        },
        returnedFromService
      );

      service.create(new WalletTopupTransaction()).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'POST' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(expected);
    });

    it('should update a WalletTopupTransaction', () => {
      const returnedFromService = Object.assign(
        {
          id: 1,
          txnRefNo: 'BBBBBB',
          accountNo: 'BBBBBB',
          txnDate: currentDate.format(DATE_FORMAT),
          txnDateInUTC: currentDate.format(DATE_TIME_FORMAT),
          txnDateInLocal: currentDate.format(DATE_TIME_FORMAT),
          txnDateForSettlement: currentDate.format(DATE_TIME_FORMAT),
          valueDate: currentDate.format(DATE_FORMAT),
          valueDateInUTC: currentDate.format(DATE_TIME_FORMAT),
          valueDateInLocal: currentDate.format(DATE_TIME_FORMAT),
          valueDateForSettlement: currentDate.format(DATE_TIME_FORMAT),
          txnAmount: 1,
          instructedAmount: 1,
          amountInAccountCurrency: 1,
          amountInLocalCurrency: 1,
          chargeInAccountCurrency: 1,
          chargeInLocalCurrency: 1,
          fundingSource: 'BBBBBB',
          merchantID: 'BBBBBB',
          merchantInfo: 'BBBBBB',
          bankCode: 'BBBBBB',
          externalRefNo: 'BBBBBB',
          txnDescription: 'BBBBBB',
          txnInternalDescription: 'BBBBBB',
          txnAddnlInfo: 'BBBBBB',
          createdOn: currentDate.format(DATE_TIME_FORMAT),
          createdBy: 'BBBBBB',
          txnStatus: 'BBBBBB',
          txnDebitAccountNo: 'BBBBBB',
          txnCreditAccountNo: 'BBBBBB',
          chargeDebitAccountNo: 'BBBBBB',
          chargeCreditAccountNo: 'BBBBBB',
        },
        elemDefault
      );

      const expected = Object.assign(
        {
          txnDate: currentDate,
          txnDateInUTC: currentDate,
          txnDateInLocal: currentDate,
          txnDateForSettlement: currentDate,
          valueDate: currentDate,
          valueDateInUTC: currentDate,
          valueDateInLocal: currentDate,
          valueDateForSettlement: currentDate,
          createdOn: currentDate,
        },
        returnedFromService
      );

      service.update(expected).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'PUT' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(expected);
    });

    it('should partial update a WalletTopupTransaction', () => {
      const patchObject = Object.assign(
        {
          txnRefNo: 'BBBBBB',
          accountNo: 'BBBBBB',
          txnDateInUTC: currentDate.format(DATE_TIME_FORMAT),
          valueDate: currentDate.format(DATE_FORMAT),
          valueDateInUTC: currentDate.format(DATE_TIME_FORMAT),
          valueDateInLocal: currentDate.format(DATE_TIME_FORMAT),
          instructedAmount: 1,
          amountInAccountCurrency: 1,
          amountInLocalCurrency: 1,
          chargeInAccountCurrency: 1,
          fundingSource: 'BBBBBB',
          merchantInfo: 'BBBBBB',
          externalRefNo: 'BBBBBB',
          txnDescription: 'BBBBBB',
          txnInternalDescription: 'BBBBBB',
          txnStatus: 'BBBBBB',
          txnDebitAccountNo: 'BBBBBB',
          txnCreditAccountNo: 'BBBBBB',
          chargeDebitAccountNo: 'BBBBBB',
          chargeCreditAccountNo: 'BBBBBB',
        },
        new WalletTopupTransaction()
      );

      const returnedFromService = Object.assign(patchObject, elemDefault);

      const expected = Object.assign(
        {
          txnDate: currentDate,
          txnDateInUTC: currentDate,
          txnDateInLocal: currentDate,
          txnDateForSettlement: currentDate,
          valueDate: currentDate,
          valueDateInUTC: currentDate,
          valueDateInLocal: currentDate,
          valueDateForSettlement: currentDate,
          createdOn: currentDate,
        },
        returnedFromService
      );

      service.partialUpdate(patchObject).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'PATCH' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(expected);
    });

    it('should return a list of WalletTopupTransaction', () => {
      const returnedFromService = Object.assign(
        {
          id: 1,
          txnRefNo: 'BBBBBB',
          accountNo: 'BBBBBB',
          txnDate: currentDate.format(DATE_FORMAT),
          txnDateInUTC: currentDate.format(DATE_TIME_FORMAT),
          txnDateInLocal: currentDate.format(DATE_TIME_FORMAT),
          txnDateForSettlement: currentDate.format(DATE_TIME_FORMAT),
          valueDate: currentDate.format(DATE_FORMAT),
          valueDateInUTC: currentDate.format(DATE_TIME_FORMAT),
          valueDateInLocal: currentDate.format(DATE_TIME_FORMAT),
          valueDateForSettlement: currentDate.format(DATE_TIME_FORMAT),
          txnAmount: 1,
          instructedAmount: 1,
          amountInAccountCurrency: 1,
          amountInLocalCurrency: 1,
          chargeInAccountCurrency: 1,
          chargeInLocalCurrency: 1,
          fundingSource: 'BBBBBB',
          merchantID: 'BBBBBB',
          merchantInfo: 'BBBBBB',
          bankCode: 'BBBBBB',
          externalRefNo: 'BBBBBB',
          txnDescription: 'BBBBBB',
          txnInternalDescription: 'BBBBBB',
          txnAddnlInfo: 'BBBBBB',
          createdOn: currentDate.format(DATE_TIME_FORMAT),
          createdBy: 'BBBBBB',
          txnStatus: 'BBBBBB',
          txnDebitAccountNo: 'BBBBBB',
          txnCreditAccountNo: 'BBBBBB',
          chargeDebitAccountNo: 'BBBBBB',
          chargeCreditAccountNo: 'BBBBBB',
        },
        elemDefault
      );

      const expected = Object.assign(
        {
          txnDate: currentDate,
          txnDateInUTC: currentDate,
          txnDateInLocal: currentDate,
          txnDateForSettlement: currentDate,
          valueDate: currentDate,
          valueDateInUTC: currentDate,
          valueDateInLocal: currentDate,
          valueDateForSettlement: currentDate,
          createdOn: currentDate,
        },
        returnedFromService
      );

      service.query().subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'GET' });
      req.flush([returnedFromService]);
      httpMock.verify();
      expect(expectedResult).toContainEqual(expected);
    });

    it('should delete a WalletTopupTransaction', () => {
      service.delete(123).subscribe(resp => (expectedResult = resp.ok));

      const req = httpMock.expectOne({ method: 'DELETE' });
      req.flush({ status: 200 });
      expect(expectedResult);
    });

    describe('addWalletTopupTransactionToCollectionIfMissing', () => {
      it('should add a WalletTopupTransaction to an empty array', () => {
        const walletTopupTransaction: IWalletTopupTransaction = { id: 123 };
        expectedResult = service.addWalletTopupTransactionToCollectionIfMissing([], walletTopupTransaction);
        expect(expectedResult).toHaveLength(1);
        expect(expectedResult).toContain(walletTopupTransaction);
      });

      it('should not add a WalletTopupTransaction to an array that contains it', () => {
        const walletTopupTransaction: IWalletTopupTransaction = { id: 123 };
        const walletTopupTransactionCollection: IWalletTopupTransaction[] = [
          {
            ...walletTopupTransaction,
          },
          { id: 456 },
        ];
        expectedResult = service.addWalletTopupTransactionToCollectionIfMissing(walletTopupTransactionCollection, walletTopupTransaction);
        expect(expectedResult).toHaveLength(2);
      });

      it("should add a WalletTopupTransaction to an array that doesn't contain it", () => {
        const walletTopupTransaction: IWalletTopupTransaction = { id: 123 };
        const walletTopupTransactionCollection: IWalletTopupTransaction[] = [{ id: 456 }];
        expectedResult = service.addWalletTopupTransactionToCollectionIfMissing(walletTopupTransactionCollection, walletTopupTransaction);
        expect(expectedResult).toHaveLength(2);
        expect(expectedResult).toContain(walletTopupTransaction);
      });

      it('should add only unique WalletTopupTransaction to an array', () => {
        const walletTopupTransactionArray: IWalletTopupTransaction[] = [{ id: 123 }, { id: 456 }, { id: 59053 }];
        const walletTopupTransactionCollection: IWalletTopupTransaction[] = [{ id: 123 }];
        expectedResult = service.addWalletTopupTransactionToCollectionIfMissing(
          walletTopupTransactionCollection,
          ...walletTopupTransactionArray
        );
        expect(expectedResult).toHaveLength(3);
      });

      it('should accept varargs', () => {
        const walletTopupTransaction: IWalletTopupTransaction = { id: 123 };
        const walletTopupTransaction2: IWalletTopupTransaction = { id: 456 };
        expectedResult = service.addWalletTopupTransactionToCollectionIfMissing([], walletTopupTransaction, walletTopupTransaction2);
        expect(expectedResult).toHaveLength(2);
        expect(expectedResult).toContain(walletTopupTransaction);
        expect(expectedResult).toContain(walletTopupTransaction2);
      });

      it('should accept null and undefined values', () => {
        const walletTopupTransaction: IWalletTopupTransaction = { id: 123 };
        expectedResult = service.addWalletTopupTransactionToCollectionIfMissing([], null, walletTopupTransaction, undefined);
        expect(expectedResult).toHaveLength(1);
        expect(expectedResult).toContain(walletTopupTransaction);
      });

      it('should return initial array if no WalletTopupTransaction is added', () => {
        const walletTopupTransactionCollection: IWalletTopupTransaction[] = [{ id: 123 }];
        expectedResult = service.addWalletTopupTransactionToCollectionIfMissing(walletTopupTransactionCollection, undefined, null);
        expect(expectedResult).toEqual(walletTopupTransactionCollection);
      });
    });
  });

  afterEach(() => {
    httpMock.verify();
  });
});
