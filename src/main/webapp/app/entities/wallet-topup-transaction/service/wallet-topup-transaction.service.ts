import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import dayjs from 'dayjs/esm';

import { isPresent } from 'app/core/util/operators';
import { DATE_FORMAT } from 'app/config/input.constants';
import { ApplicationConfigService } from 'app/core/config/application-config.service';
import { createRequestOption } from 'app/core/request/request-util';
import { IWalletTopupTransaction, getWalletTopupTransactionIdentifier } from '../wallet-topup-transaction.model';

export type EntityResponseType = HttpResponse<IWalletTopupTransaction>;
export type EntityArrayResponseType = HttpResponse<IWalletTopupTransaction[]>;

@Injectable({ providedIn: 'root' })
export class WalletTopupTransactionService {
  protected resourceUrl = this.applicationConfigService.getEndpointFor('api/wallet-topup-transactions');

  constructor(protected http: HttpClient, protected applicationConfigService: ApplicationConfigService) {}

  create(walletTopupTransaction: IWalletTopupTransaction): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(walletTopupTransaction);
    return this.http
      .post<IWalletTopupTransaction>(this.resourceUrl, copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  update(walletTopupTransaction: IWalletTopupTransaction): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(walletTopupTransaction);
    return this.http
      .put<IWalletTopupTransaction>(`${this.resourceUrl}/${getWalletTopupTransactionIdentifier(walletTopupTransaction) as number}`, copy, {
        observe: 'response',
      })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  partialUpdate(walletTopupTransaction: IWalletTopupTransaction): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(walletTopupTransaction);
    return this.http
      .patch<IWalletTopupTransaction>(
        `${this.resourceUrl}/${getWalletTopupTransactionIdentifier(walletTopupTransaction) as number}`,
        copy,
        { observe: 'response' }
      )
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http
      .get<IWalletTopupTransaction>(`${this.resourceUrl}/${id}`, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http
      .get<IWalletTopupTransaction[]>(this.resourceUrl, { params: options, observe: 'response' })
      .pipe(map((res: EntityArrayResponseType) => this.convertDateArrayFromServer(res)));
  }

  delete(id: number): Observable<HttpResponse<{}>> {
    return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  addWalletTopupTransactionToCollectionIfMissing(
    walletTopupTransactionCollection: IWalletTopupTransaction[],
    ...walletTopupTransactionsToCheck: (IWalletTopupTransaction | null | undefined)[]
  ): IWalletTopupTransaction[] {
    const walletTopupTransactions: IWalletTopupTransaction[] = walletTopupTransactionsToCheck.filter(isPresent);
    if (walletTopupTransactions.length > 0) {
      const walletTopupTransactionCollectionIdentifiers = walletTopupTransactionCollection.map(
        walletTopupTransactionItem => getWalletTopupTransactionIdentifier(walletTopupTransactionItem)!
      );
      const walletTopupTransactionsToAdd = walletTopupTransactions.filter(walletTopupTransactionItem => {
        const walletTopupTransactionIdentifier = getWalletTopupTransactionIdentifier(walletTopupTransactionItem);
        if (
          walletTopupTransactionIdentifier == null ||
          walletTopupTransactionCollectionIdentifiers.includes(walletTopupTransactionIdentifier)
        ) {
          return false;
        }
        walletTopupTransactionCollectionIdentifiers.push(walletTopupTransactionIdentifier);
        return true;
      });
      return [...walletTopupTransactionsToAdd, ...walletTopupTransactionCollection];
    }
    return walletTopupTransactionCollection;
  }

  protected convertDateFromClient(walletTopupTransaction: IWalletTopupTransaction): IWalletTopupTransaction {
    return Object.assign({}, walletTopupTransaction, {
      txnDate: walletTopupTransaction.txnDate?.isValid() ? walletTopupTransaction.txnDate.format(DATE_FORMAT) : undefined,
      txnDateInUTC: walletTopupTransaction.txnDateInUTC?.isValid() ? walletTopupTransaction.txnDateInUTC.toJSON() : undefined,
      txnDateInLocal: walletTopupTransaction.txnDateInLocal?.isValid() ? walletTopupTransaction.txnDateInLocal.toJSON() : undefined,
      txnDateForSettlement: walletTopupTransaction.txnDateForSettlement?.isValid()
        ? walletTopupTransaction.txnDateForSettlement.toJSON()
        : undefined,
      valueDate: walletTopupTransaction.valueDate?.isValid() ? walletTopupTransaction.valueDate.format(DATE_FORMAT) : undefined,
      valueDateInUTC: walletTopupTransaction.valueDateInUTC?.isValid() ? walletTopupTransaction.valueDateInUTC.toJSON() : undefined,
      valueDateInLocal: walletTopupTransaction.valueDateInLocal?.isValid() ? walletTopupTransaction.valueDateInLocal.toJSON() : undefined,
      valueDateForSettlement: walletTopupTransaction.valueDateForSettlement?.isValid()
        ? walletTopupTransaction.valueDateForSettlement.toJSON()
        : undefined,
      createdOn: walletTopupTransaction.createdOn?.isValid() ? walletTopupTransaction.createdOn.toJSON() : undefined,
    });
  }

  protected convertDateFromServer(res: EntityResponseType): EntityResponseType {
    if (res.body) {
      res.body.txnDate = res.body.txnDate ? dayjs(res.body.txnDate) : undefined;
      res.body.txnDateInUTC = res.body.txnDateInUTC ? dayjs(res.body.txnDateInUTC) : undefined;
      res.body.txnDateInLocal = res.body.txnDateInLocal ? dayjs(res.body.txnDateInLocal) : undefined;
      res.body.txnDateForSettlement = res.body.txnDateForSettlement ? dayjs(res.body.txnDateForSettlement) : undefined;
      res.body.valueDate = res.body.valueDate ? dayjs(res.body.valueDate) : undefined;
      res.body.valueDateInUTC = res.body.valueDateInUTC ? dayjs(res.body.valueDateInUTC) : undefined;
      res.body.valueDateInLocal = res.body.valueDateInLocal ? dayjs(res.body.valueDateInLocal) : undefined;
      res.body.valueDateForSettlement = res.body.valueDateForSettlement ? dayjs(res.body.valueDateForSettlement) : undefined;
      res.body.createdOn = res.body.createdOn ? dayjs(res.body.createdOn) : undefined;
    }
    return res;
  }

  protected convertDateArrayFromServer(res: EntityArrayResponseType): EntityArrayResponseType {
    if (res.body) {
      res.body.forEach((walletTopupTransaction: IWalletTopupTransaction) => {
        walletTopupTransaction.txnDate = walletTopupTransaction.txnDate ? dayjs(walletTopupTransaction.txnDate) : undefined;
        walletTopupTransaction.txnDateInUTC = walletTopupTransaction.txnDateInUTC ? dayjs(walletTopupTransaction.txnDateInUTC) : undefined;
        walletTopupTransaction.txnDateInLocal = walletTopupTransaction.txnDateInLocal
          ? dayjs(walletTopupTransaction.txnDateInLocal)
          : undefined;
        walletTopupTransaction.txnDateForSettlement = walletTopupTransaction.txnDateForSettlement
          ? dayjs(walletTopupTransaction.txnDateForSettlement)
          : undefined;
        walletTopupTransaction.valueDate = walletTopupTransaction.valueDate ? dayjs(walletTopupTransaction.valueDate) : undefined;
        walletTopupTransaction.valueDateInUTC = walletTopupTransaction.valueDateInUTC
          ? dayjs(walletTopupTransaction.valueDateInUTC)
          : undefined;
        walletTopupTransaction.valueDateInLocal = walletTopupTransaction.valueDateInLocal
          ? dayjs(walletTopupTransaction.valueDateInLocal)
          : undefined;
        walletTopupTransaction.valueDateForSettlement = walletTopupTransaction.valueDateForSettlement
          ? dayjs(walletTopupTransaction.valueDateForSettlement)
          : undefined;
        walletTopupTransaction.createdOn = walletTopupTransaction.createdOn ? dayjs(walletTopupTransaction.createdOn) : undefined;
      });
    }
    return res;
  }
}
