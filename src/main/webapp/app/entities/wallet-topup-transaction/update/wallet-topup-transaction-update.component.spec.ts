import { ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { of, Subject, from } from 'rxjs';

import { WalletTopupTransactionService } from '../service/wallet-topup-transaction.service';
import { IWalletTopupTransaction, WalletTopupTransaction } from '../wallet-topup-transaction.model';
import { ICountry } from 'app/entities/country/country.model';
import { CountryService } from 'app/entities/country/service/country.service';
import { ICurrency } from 'app/entities/currency/currency.model';
import { CurrencyService } from 'app/entities/currency/service/currency.service';
import { IPurposeOfTxn } from 'app/entities/purpose-of-txn/purpose-of-txn.model';
import { PurposeOfTxnService } from 'app/entities/purpose-of-txn/service/purpose-of-txn.service';
import { IFundingSourceType } from 'app/entities/funding-source-type/funding-source-type.model';
import { FundingSourceTypeService } from 'app/entities/funding-source-type/service/funding-source-type.service';

import { WalletTopupTransactionUpdateComponent } from './wallet-topup-transaction-update.component';

describe('WalletTopupTransaction Management Update Component', () => {
  let comp: WalletTopupTransactionUpdateComponent;
  let fixture: ComponentFixture<WalletTopupTransactionUpdateComponent>;
  let activatedRoute: ActivatedRoute;
  let walletTopupTransactionService: WalletTopupTransactionService;
  let countryService: CountryService;
  let currencyService: CurrencyService;
  let purposeOfTxnService: PurposeOfTxnService;
  let fundingSourceTypeService: FundingSourceTypeService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule, RouterTestingModule.withRoutes([])],
      declarations: [WalletTopupTransactionUpdateComponent],
      providers: [
        FormBuilder,
        {
          provide: ActivatedRoute,
          useValue: {
            params: from([{}]),
          },
        },
      ],
    })
      .overrideTemplate(WalletTopupTransactionUpdateComponent, '')
      .compileComponents();

    fixture = TestBed.createComponent(WalletTopupTransactionUpdateComponent);
    activatedRoute = TestBed.inject(ActivatedRoute);
    walletTopupTransactionService = TestBed.inject(WalletTopupTransactionService);
    countryService = TestBed.inject(CountryService);
    currencyService = TestBed.inject(CurrencyService);
    purposeOfTxnService = TestBed.inject(PurposeOfTxnService);
    fundingSourceTypeService = TestBed.inject(FundingSourceTypeService);

    comp = fixture.componentInstance;
  });

  describe('ngOnInit', () => {
    it('Should call Country query and add missing value', () => {
      const walletTopupTransaction: IWalletTopupTransaction = { id: 456 };
      const country: ICountry = { id: 80167 };
      walletTopupTransaction.country = country;

      const countryCollection: ICountry[] = [{ id: 41159 }];
      jest.spyOn(countryService, 'query').mockReturnValue(of(new HttpResponse({ body: countryCollection })));
      const additionalCountries = [country];
      const expectedCollection: ICountry[] = [...additionalCountries, ...countryCollection];
      jest.spyOn(countryService, 'addCountryToCollectionIfMissing').mockReturnValue(expectedCollection);

      activatedRoute.data = of({ walletTopupTransaction });
      comp.ngOnInit();

      expect(countryService.query).toHaveBeenCalled();
      expect(countryService.addCountryToCollectionIfMissing).toHaveBeenCalledWith(countryCollection, ...additionalCountries);
      expect(comp.countriesSharedCollection).toEqual(expectedCollection);
    });

    it('Should call Currency query and add missing value', () => {
      const walletTopupTransaction: IWalletTopupTransaction = { id: 456 };
      const txnCurrency: ICurrency = { id: 18913 };
      walletTopupTransaction.txnCurrency = txnCurrency;
      const accountCurrency: ICurrency = { id: 71796 };
      walletTopupTransaction.accountCurrency = accountCurrency;
      const instructedCurrency: ICurrency = { id: 34439 };
      walletTopupTransaction.instructedCurrency = instructedCurrency;

      const currencyCollection: ICurrency[] = [{ id: 63198 }];
      jest.spyOn(currencyService, 'query').mockReturnValue(of(new HttpResponse({ body: currencyCollection })));
      const additionalCurrencies = [txnCurrency, accountCurrency, instructedCurrency];
      const expectedCollection: ICurrency[] = [...additionalCurrencies, ...currencyCollection];
      jest.spyOn(currencyService, 'addCurrencyToCollectionIfMissing').mockReturnValue(expectedCollection);

      activatedRoute.data = of({ walletTopupTransaction });
      comp.ngOnInit();

      expect(currencyService.query).toHaveBeenCalled();
      expect(currencyService.addCurrencyToCollectionIfMissing).toHaveBeenCalledWith(currencyCollection, ...additionalCurrencies);
      expect(comp.currenciesSharedCollection).toEqual(expectedCollection);
    });

    it('Should call PurposeOfTxn query and add missing value', () => {
      const walletTopupTransaction: IWalletTopupTransaction = { id: 456 };
      const purposeOfTxn: IPurposeOfTxn = { id: 11002 };
      walletTopupTransaction.purposeOfTxn = purposeOfTxn;

      const purposeOfTxnCollection: IPurposeOfTxn[] = [{ id: 81683 }];
      jest.spyOn(purposeOfTxnService, 'query').mockReturnValue(of(new HttpResponse({ body: purposeOfTxnCollection })));
      const additionalPurposeOfTxns = [purposeOfTxn];
      const expectedCollection: IPurposeOfTxn[] = [...additionalPurposeOfTxns, ...purposeOfTxnCollection];
      jest.spyOn(purposeOfTxnService, 'addPurposeOfTxnToCollectionIfMissing').mockReturnValue(expectedCollection);

      activatedRoute.data = of({ walletTopupTransaction });
      comp.ngOnInit();

      expect(purposeOfTxnService.query).toHaveBeenCalled();
      expect(purposeOfTxnService.addPurposeOfTxnToCollectionIfMissing).toHaveBeenCalledWith(
        purposeOfTxnCollection,
        ...additionalPurposeOfTxns
      );
      expect(comp.purposeOfTxnsSharedCollection).toEqual(expectedCollection);
    });

    it('Should call FundingSourceType query and add missing value', () => {
      const walletTopupTransaction: IWalletTopupTransaction = { id: 456 };
      const fundingSourceType: IFundingSourceType = { id: 27275 };
      walletTopupTransaction.fundingSourceType = fundingSourceType;

      const fundingSourceTypeCollection: IFundingSourceType[] = [{ id: 34366 }];
      jest.spyOn(fundingSourceTypeService, 'query').mockReturnValue(of(new HttpResponse({ body: fundingSourceTypeCollection })));
      const additionalFundingSourceTypes = [fundingSourceType];
      const expectedCollection: IFundingSourceType[] = [...additionalFundingSourceTypes, ...fundingSourceTypeCollection];
      jest.spyOn(fundingSourceTypeService, 'addFundingSourceTypeToCollectionIfMissing').mockReturnValue(expectedCollection);

      activatedRoute.data = of({ walletTopupTransaction });
      comp.ngOnInit();

      expect(fundingSourceTypeService.query).toHaveBeenCalled();
      expect(fundingSourceTypeService.addFundingSourceTypeToCollectionIfMissing).toHaveBeenCalledWith(
        fundingSourceTypeCollection,
        ...additionalFundingSourceTypes
      );
      expect(comp.fundingSourceTypesSharedCollection).toEqual(expectedCollection);
    });

    it('Should update editForm', () => {
      const walletTopupTransaction: IWalletTopupTransaction = { id: 456 };
      const country: ICountry = { id: 90985 };
      walletTopupTransaction.country = country;
      const txnCurrency: ICurrency = { id: 63741 };
      walletTopupTransaction.txnCurrency = txnCurrency;
      const accountCurrency: ICurrency = { id: 11653 };
      walletTopupTransaction.accountCurrency = accountCurrency;
      const instructedCurrency: ICurrency = { id: 98135 };
      walletTopupTransaction.instructedCurrency = instructedCurrency;
      const purposeOfTxn: IPurposeOfTxn = { id: 97143 };
      walletTopupTransaction.purposeOfTxn = purposeOfTxn;
      const fundingSourceType: IFundingSourceType = { id: 11927 };
      walletTopupTransaction.fundingSourceType = fundingSourceType;

      activatedRoute.data = of({ walletTopupTransaction });
      comp.ngOnInit();

      expect(comp.editForm.value).toEqual(expect.objectContaining(walletTopupTransaction));
      expect(comp.countriesSharedCollection).toContain(country);
      expect(comp.currenciesSharedCollection).toContain(txnCurrency);
      expect(comp.currenciesSharedCollection).toContain(accountCurrency);
      expect(comp.currenciesSharedCollection).toContain(instructedCurrency);
      expect(comp.purposeOfTxnsSharedCollection).toContain(purposeOfTxn);
      expect(comp.fundingSourceTypesSharedCollection).toContain(fundingSourceType);
    });
  });

  describe('save', () => {
    it('Should call update service on save for existing entity', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<WalletTopupTransaction>>();
      const walletTopupTransaction = { id: 123 };
      jest.spyOn(walletTopupTransactionService, 'update').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ walletTopupTransaction });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.next(new HttpResponse({ body: walletTopupTransaction }));
      saveSubject.complete();

      // THEN
      expect(comp.previousState).toHaveBeenCalled();
      expect(walletTopupTransactionService.update).toHaveBeenCalledWith(walletTopupTransaction);
      expect(comp.isSaving).toEqual(false);
    });

    it('Should call create service on save for new entity', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<WalletTopupTransaction>>();
      const walletTopupTransaction = new WalletTopupTransaction();
      jest.spyOn(walletTopupTransactionService, 'create').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ walletTopupTransaction });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.next(new HttpResponse({ body: walletTopupTransaction }));
      saveSubject.complete();

      // THEN
      expect(walletTopupTransactionService.create).toHaveBeenCalledWith(walletTopupTransaction);
      expect(comp.isSaving).toEqual(false);
      expect(comp.previousState).toHaveBeenCalled();
    });

    it('Should set isSaving to false on error', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<WalletTopupTransaction>>();
      const walletTopupTransaction = { id: 123 };
      jest.spyOn(walletTopupTransactionService, 'update').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ walletTopupTransaction });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.error('This is an error!');

      // THEN
      expect(walletTopupTransactionService.update).toHaveBeenCalledWith(walletTopupTransaction);
      expect(comp.isSaving).toEqual(false);
      expect(comp.previousState).not.toHaveBeenCalled();
    });
  });

  describe('Tracking relationships identifiers', () => {
    describe('trackCountryById', () => {
      it('Should return tracked Country primary key', () => {
        const entity = { id: 123 };
        const trackResult = comp.trackCountryById(0, entity);
        expect(trackResult).toEqual(entity.id);
      });
    });

    describe('trackCurrencyById', () => {
      it('Should return tracked Currency primary key', () => {
        const entity = { id: 123 };
        const trackResult = comp.trackCurrencyById(0, entity);
        expect(trackResult).toEqual(entity.id);
      });
    });

    describe('trackPurposeOfTxnById', () => {
      it('Should return tracked PurposeOfTxn primary key', () => {
        const entity = { id: 123 };
        const trackResult = comp.trackPurposeOfTxnById(0, entity);
        expect(trackResult).toEqual(entity.id);
      });
    });

    describe('trackFundingSourceTypeById', () => {
      it('Should return tracked FundingSourceType primary key', () => {
        const entity = { id: 123 };
        const trackResult = comp.trackFundingSourceTypeById(0, entity);
        expect(trackResult).toEqual(entity.id);
      });
    });
  });
});
