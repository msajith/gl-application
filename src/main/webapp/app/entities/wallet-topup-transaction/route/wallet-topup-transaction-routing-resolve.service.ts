import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, Router } from '@angular/router';
import { Observable, of, EMPTY } from 'rxjs';
import { mergeMap } from 'rxjs/operators';

import { IWalletTopupTransaction, WalletTopupTransaction } from '../wallet-topup-transaction.model';
import { WalletTopupTransactionService } from '../service/wallet-topup-transaction.service';

@Injectable({ providedIn: 'root' })
export class WalletTopupTransactionRoutingResolveService implements Resolve<IWalletTopupTransaction> {
  constructor(protected service: WalletTopupTransactionService, protected router: Router) {}

  resolve(route: ActivatedRouteSnapshot): Observable<IWalletTopupTransaction> | Observable<never> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        mergeMap((walletTopupTransaction: HttpResponse<WalletTopupTransaction>) => {
          if (walletTopupTransaction.body) {
            return of(walletTopupTransaction.body);
          } else {
            this.router.navigate(['404']);
            return EMPTY;
          }
        })
      );
    }
    return of(new WalletTopupTransaction());
  }
}
