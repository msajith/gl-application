import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { UserRouteAccessService } from 'app/core/auth/user-route-access.service';
import { WalletTopupTransactionComponent } from '../list/wallet-topup-transaction.component';
import { WalletTopupTransactionDetailComponent } from '../detail/wallet-topup-transaction-detail.component';
import { WalletTopupTransactionUpdateComponent } from '../update/wallet-topup-transaction-update.component';
import { WalletTopupTransactionRoutingResolveService } from './wallet-topup-transaction-routing-resolve.service';

const walletTopupTransactionRoute: Routes = [
  {
    path: '',
    component: WalletTopupTransactionComponent,
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/view',
    component: WalletTopupTransactionDetailComponent,
    resolve: {
      walletTopupTransaction: WalletTopupTransactionRoutingResolveService,
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: 'new',
    component: WalletTopupTransactionUpdateComponent,
    resolve: {
      walletTopupTransaction: WalletTopupTransactionRoutingResolveService,
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/edit',
    component: WalletTopupTransactionUpdateComponent,
    resolve: {
      walletTopupTransaction: WalletTopupTransactionRoutingResolveService,
    },
    canActivate: [UserRouteAccessService],
  },
];

@NgModule({
  imports: [RouterModule.forChild(walletTopupTransactionRoute)],
  exports: [RouterModule],
})
export class WalletTopupTransactionRoutingModule {}
