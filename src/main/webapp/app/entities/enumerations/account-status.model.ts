export enum AccountStatus {
  ACTIVE = 'ACTIVE',

  INACTIVE = 'INACTIVE',

  CLOSED = 'CLOSED',
}
