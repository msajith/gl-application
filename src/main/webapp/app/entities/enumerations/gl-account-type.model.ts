export enum GlAccountType {
  ASSET = 'ASSET',

  LIABILITY = 'LIABILITY',

  REVENUE = 'REVENUE',

  EXPENSE = 'EXPENSE',

  GAIN = 'GAIN',

  LOSS = 'LOSS',
}
