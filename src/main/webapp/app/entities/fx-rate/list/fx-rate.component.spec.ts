import { ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpHeaders, HttpResponse } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { of } from 'rxjs';

import { FxRateService } from '../service/fx-rate.service';

import { FxRateComponent } from './fx-rate.component';

describe('FxRate Management Component', () => {
  let comp: FxRateComponent;
  let fixture: ComponentFixture<FxRateComponent>;
  let service: FxRateService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      declarations: [FxRateComponent],
    })
      .overrideTemplate(FxRateComponent, '')
      .compileComponents();

    fixture = TestBed.createComponent(FxRateComponent);
    comp = fixture.componentInstance;
    service = TestBed.inject(FxRateService);

    const headers = new HttpHeaders();
    jest.spyOn(service, 'query').mockReturnValue(
      of(
        new HttpResponse({
          body: [{ id: 123 }],
          headers,
        })
      )
    );
  });

  it('Should call load all on init', () => {
    // WHEN
    comp.ngOnInit();

    // THEN
    expect(service.query).toHaveBeenCalled();
    expect(comp.fxRates?.[0]).toEqual(expect.objectContaining({ id: 123 }));
  });
});
