import dayjs from 'dayjs/esm';
import { FxRecordStatus } from 'app/entities/enumerations/fx-record-status.model';

export interface IFxRate {
  id?: number;
  type?: string | null;
  baseCurrency?: string | null;
  quoteCurrency?: string | null;
  askPrice?: number | null;
  bidPrice?: number | null;
  midPrice?: number | null;
  askMargin?: number | null;
  bidMargin?: number | null;
  segment?: string | null;
  amountSlab?: string | null;
  amountSlabValue?: number | null;
  amountSlabOperator?: string | null;
  amountSlabCurrency?: string | null;
  recordStatus?: FxRecordStatus | null;
  validfrom?: dayjs.Dayjs | null;
  validTo?: dayjs.Dayjs | null;
}

export class FxRate implements IFxRate {
  constructor(
    public id?: number,
    public type?: string | null,
    public baseCurrency?: string | null,
    public quoteCurrency?: string | null,
    public askPrice?: number | null,
    public bidPrice?: number | null,
    public midPrice?: number | null,
    public askMargin?: number | null,
    public bidMargin?: number | null,
    public segment?: string | null,
    public amountSlab?: string | null,
    public amountSlabValue?: number | null,
    public amountSlabOperator?: string | null,
    public amountSlabCurrency?: string | null,
    public recordStatus?: FxRecordStatus | null,
    public validfrom?: dayjs.Dayjs | null,
    public validTo?: dayjs.Dayjs | null
  ) {}
}

export function getFxRateIdentifier(fxRate: IFxRate): number | undefined {
  return fxRate.id;
}
