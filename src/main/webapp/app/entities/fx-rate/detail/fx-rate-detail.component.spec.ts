import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { FxRateDetailComponent } from './fx-rate-detail.component';

describe('FxRate Management Detail Component', () => {
  let comp: FxRateDetailComponent;
  let fixture: ComponentFixture<FxRateDetailComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [FxRateDetailComponent],
      providers: [
        {
          provide: ActivatedRoute,
          useValue: { data: of({ fxRate: { id: 123 } }) },
        },
      ],
    })
      .overrideTemplate(FxRateDetailComponent, '')
      .compileComponents();
    fixture = TestBed.createComponent(FxRateDetailComponent);
    comp = fixture.componentInstance;
  });

  describe('OnInit', () => {
    it('Should load fxRate on init', () => {
      // WHEN
      comp.ngOnInit();

      // THEN
      expect(comp.fxRate).toEqual(expect.objectContaining({ id: 123 }));
    });
  });
});
