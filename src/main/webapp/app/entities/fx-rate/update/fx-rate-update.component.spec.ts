import { ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { of, Subject, from } from 'rxjs';

import { FxRateService } from '../service/fx-rate.service';
import { IFxRate, FxRate } from '../fx-rate.model';

import { FxRateUpdateComponent } from './fx-rate-update.component';

describe('FxRate Management Update Component', () => {
  let comp: FxRateUpdateComponent;
  let fixture: ComponentFixture<FxRateUpdateComponent>;
  let activatedRoute: ActivatedRoute;
  let fxRateService: FxRateService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule, RouterTestingModule.withRoutes([])],
      declarations: [FxRateUpdateComponent],
      providers: [
        FormBuilder,
        {
          provide: ActivatedRoute,
          useValue: {
            params: from([{}]),
          },
        },
      ],
    })
      .overrideTemplate(FxRateUpdateComponent, '')
      .compileComponents();

    fixture = TestBed.createComponent(FxRateUpdateComponent);
    activatedRoute = TestBed.inject(ActivatedRoute);
    fxRateService = TestBed.inject(FxRateService);

    comp = fixture.componentInstance;
  });

  describe('ngOnInit', () => {
    it('Should update editForm', () => {
      const fxRate: IFxRate = { id: 456 };

      activatedRoute.data = of({ fxRate });
      comp.ngOnInit();

      expect(comp.editForm.value).toEqual(expect.objectContaining(fxRate));
    });
  });

  describe('save', () => {
    it('Should call update service on save for existing entity', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<FxRate>>();
      const fxRate = { id: 123 };
      jest.spyOn(fxRateService, 'update').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ fxRate });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.next(new HttpResponse({ body: fxRate }));
      saveSubject.complete();

      // THEN
      expect(comp.previousState).toHaveBeenCalled();
      expect(fxRateService.update).toHaveBeenCalledWith(fxRate);
      expect(comp.isSaving).toEqual(false);
    });

    it('Should call create service on save for new entity', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<FxRate>>();
      const fxRate = new FxRate();
      jest.spyOn(fxRateService, 'create').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ fxRate });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.next(new HttpResponse({ body: fxRate }));
      saveSubject.complete();

      // THEN
      expect(fxRateService.create).toHaveBeenCalledWith(fxRate);
      expect(comp.isSaving).toEqual(false);
      expect(comp.previousState).toHaveBeenCalled();
    });

    it('Should set isSaving to false on error', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<FxRate>>();
      const fxRate = { id: 123 };
      jest.spyOn(fxRateService, 'update').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ fxRate });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.error('This is an error!');

      // THEN
      expect(fxRateService.update).toHaveBeenCalledWith(fxRate);
      expect(comp.isSaving).toEqual(false);
      expect(comp.previousState).not.toHaveBeenCalled();
    });
  });
});
