import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { finalize } from 'rxjs/operators';

import { IFxRate, FxRate } from '../fx-rate.model';
import { FxRateService } from '../service/fx-rate.service';
import { FxRecordStatus } from 'app/entities/enumerations/fx-record-status.model';

@Component({
  selector: 'jhi-fx-rate-update',
  templateUrl: './fx-rate-update.component.html',
})
export class FxRateUpdateComponent implements OnInit {
  isSaving = false;
  fxRecordStatusValues = Object.keys(FxRecordStatus);

  editForm = this.fb.group({
    id: [],
    type: [],
    baseCurrency: [],
    quoteCurrency: [],
    askPrice: [],
    bidPrice: [],
    midPrice: [],
    askMargin: [],
    bidMargin: [],
    segment: [],
    amountSlab: [],
    amountSlabValue: [],
    amountSlabOperator: [],
    amountSlabCurrency: [],
    recordStatus: [],
    validfrom: [],
    validTo: [],
  });

  constructor(protected fxRateService: FxRateService, protected activatedRoute: ActivatedRoute, protected fb: FormBuilder) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ fxRate }) => {
      this.updateForm(fxRate);
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const fxRate = this.createFromForm();
    if (fxRate.id !== undefined) {
      this.subscribeToSaveResponse(this.fxRateService.update(fxRate));
    } else {
      this.subscribeToSaveResponse(this.fxRateService.create(fxRate));
    }
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IFxRate>>): void {
    result.pipe(finalize(() => this.onSaveFinalize())).subscribe({
      next: () => this.onSaveSuccess(),
      error: () => this.onSaveError(),
    });
  }

  protected onSaveSuccess(): void {
    this.previousState();
  }

  protected onSaveError(): void {
    // Api for inheritance.
  }

  protected onSaveFinalize(): void {
    this.isSaving = false;
  }

  protected updateForm(fxRate: IFxRate): void {
    this.editForm.patchValue({
      id: fxRate.id,
      type: fxRate.type,
      baseCurrency: fxRate.baseCurrency,
      quoteCurrency: fxRate.quoteCurrency,
      askPrice: fxRate.askPrice,
      bidPrice: fxRate.bidPrice,
      midPrice: fxRate.midPrice,
      askMargin: fxRate.askMargin,
      bidMargin: fxRate.bidMargin,
      segment: fxRate.segment,
      amountSlab: fxRate.amountSlab,
      amountSlabValue: fxRate.amountSlabValue,
      amountSlabOperator: fxRate.amountSlabOperator,
      amountSlabCurrency: fxRate.amountSlabCurrency,
      recordStatus: fxRate.recordStatus,
      validfrom: fxRate.validfrom,
      validTo: fxRate.validTo,
    });
  }

  protected createFromForm(): IFxRate {
    return {
      ...new FxRate(),
      id: this.editForm.get(['id'])!.value,
      type: this.editForm.get(['type'])!.value,
      baseCurrency: this.editForm.get(['baseCurrency'])!.value,
      quoteCurrency: this.editForm.get(['quoteCurrency'])!.value,
      askPrice: this.editForm.get(['askPrice'])!.value,
      bidPrice: this.editForm.get(['bidPrice'])!.value,
      midPrice: this.editForm.get(['midPrice'])!.value,
      askMargin: this.editForm.get(['askMargin'])!.value,
      bidMargin: this.editForm.get(['bidMargin'])!.value,
      segment: this.editForm.get(['segment'])!.value,
      amountSlab: this.editForm.get(['amountSlab'])!.value,
      amountSlabValue: this.editForm.get(['amountSlabValue'])!.value,
      amountSlabOperator: this.editForm.get(['amountSlabOperator'])!.value,
      amountSlabCurrency: this.editForm.get(['amountSlabCurrency'])!.value,
      recordStatus: this.editForm.get(['recordStatus'])!.value,
      validfrom: this.editForm.get(['validfrom'])!.value,
      validTo: this.editForm.get(['validTo'])!.value,
    };
  }
}
