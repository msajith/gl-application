import { ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { of, Subject, from } from 'rxjs';

import { GeneralLedgerService } from '../service/general-ledger.service';
import { IGeneralLedger, GeneralLedger } from '../general-ledger.model';

import { GeneralLedgerUpdateComponent } from './general-ledger-update.component';

describe('GeneralLedger Management Update Component', () => {
  let comp: GeneralLedgerUpdateComponent;
  let fixture: ComponentFixture<GeneralLedgerUpdateComponent>;
  let activatedRoute: ActivatedRoute;
  let generalLedgerService: GeneralLedgerService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule, RouterTestingModule.withRoutes([])],
      declarations: [GeneralLedgerUpdateComponent],
      providers: [
        FormBuilder,
        {
          provide: ActivatedRoute,
          useValue: {
            params: from([{}]),
          },
        },
      ],
    })
      .overrideTemplate(GeneralLedgerUpdateComponent, '')
      .compileComponents();

    fixture = TestBed.createComponent(GeneralLedgerUpdateComponent);
    activatedRoute = TestBed.inject(ActivatedRoute);
    generalLedgerService = TestBed.inject(GeneralLedgerService);

    comp = fixture.componentInstance;
  });

  describe('ngOnInit', () => {
    it('Should update editForm', () => {
      const generalLedger: IGeneralLedger = { id: 456 };

      activatedRoute.data = of({ generalLedger });
      comp.ngOnInit();

      expect(comp.editForm.value).toEqual(expect.objectContaining(generalLedger));
    });
  });

  describe('save', () => {
    it('Should call update service on save for existing entity', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<GeneralLedger>>();
      const generalLedger = { id: 123 };
      jest.spyOn(generalLedgerService, 'update').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ generalLedger });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.next(new HttpResponse({ body: generalLedger }));
      saveSubject.complete();

      // THEN
      expect(comp.previousState).toHaveBeenCalled();
      expect(generalLedgerService.update).toHaveBeenCalledWith(generalLedger);
      expect(comp.isSaving).toEqual(false);
    });

    it('Should call create service on save for new entity', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<GeneralLedger>>();
      const generalLedger = new GeneralLedger();
      jest.spyOn(generalLedgerService, 'create').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ generalLedger });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.next(new HttpResponse({ body: generalLedger }));
      saveSubject.complete();

      // THEN
      expect(generalLedgerService.create).toHaveBeenCalledWith(generalLedger);
      expect(comp.isSaving).toEqual(false);
      expect(comp.previousState).toHaveBeenCalled();
    });

    it('Should set isSaving to false on error', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<GeneralLedger>>();
      const generalLedger = { id: 123 };
      jest.spyOn(generalLedgerService, 'update').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ generalLedger });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.error('This is an error!');

      // THEN
      expect(generalLedgerService.update).toHaveBeenCalledWith(generalLedger);
      expect(comp.isSaving).toEqual(false);
      expect(comp.previousState).not.toHaveBeenCalled();
    });
  });
});
