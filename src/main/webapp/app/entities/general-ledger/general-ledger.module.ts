import { NgModule } from '@angular/core';
import { SharedModule } from 'app/shared/shared.module';
import { GeneralLedgerComponent } from './list/general-ledger.component';
import { GeneralLedgerDetailComponent } from './detail/general-ledger-detail.component';
import { GeneralLedgerUpdateComponent } from './update/general-ledger-update.component';
import { GeneralLedgerDeleteDialogComponent } from './delete/general-ledger-delete-dialog.component';
import { GeneralLedgerRoutingModule } from './route/general-ledger-routing.module';

@NgModule({
  imports: [SharedModule, GeneralLedgerRoutingModule],
  declarations: [GeneralLedgerComponent, GeneralLedgerDetailComponent, GeneralLedgerUpdateComponent, GeneralLedgerDeleteDialogComponent],
  entryComponents: [GeneralLedgerDeleteDialogComponent],
})
export class GeneralLedgerModule {}
