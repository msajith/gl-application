import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, Router } from '@angular/router';
import { Observable, of, EMPTY } from 'rxjs';
import { mergeMap } from 'rxjs/operators';

import { IGeneralLedger, GeneralLedger } from '../general-ledger.model';
import { GeneralLedgerService } from '../service/general-ledger.service';

@Injectable({ providedIn: 'root' })
export class GeneralLedgerRoutingResolveService implements Resolve<IGeneralLedger> {
  constructor(protected service: GeneralLedgerService, protected router: Router) {}

  resolve(route: ActivatedRouteSnapshot): Observable<IGeneralLedger> | Observable<never> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        mergeMap((generalLedger: HttpResponse<GeneralLedger>) => {
          if (generalLedger.body) {
            return of(generalLedger.body);
          } else {
            this.router.navigate(['404']);
            return EMPTY;
          }
        })
      );
    }
    return of(new GeneralLedger());
  }
}
