import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { UserRouteAccessService } from 'app/core/auth/user-route-access.service';
import { GeneralLedgerComponent } from '../list/general-ledger.component';
import { GeneralLedgerDetailComponent } from '../detail/general-ledger-detail.component';
import { GeneralLedgerUpdateComponent } from '../update/general-ledger-update.component';
import { GeneralLedgerRoutingResolveService } from './general-ledger-routing-resolve.service';

const generalLedgerRoute: Routes = [
  {
    path: '',
    component: GeneralLedgerComponent,
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/view',
    component: GeneralLedgerDetailComponent,
    resolve: {
      generalLedger: GeneralLedgerRoutingResolveService,
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: 'new',
    component: GeneralLedgerUpdateComponent,
    resolve: {
      generalLedger: GeneralLedgerRoutingResolveService,
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/edit',
    component: GeneralLedgerUpdateComponent,
    resolve: {
      generalLedger: GeneralLedgerRoutingResolveService,
    },
    canActivate: [UserRouteAccessService],
  },
];

@NgModule({
  imports: [RouterModule.forChild(generalLedgerRoute)],
  exports: [RouterModule],
})
export class GeneralLedgerRoutingModule {}
