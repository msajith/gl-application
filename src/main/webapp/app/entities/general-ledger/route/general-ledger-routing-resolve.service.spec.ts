import { TestBed } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { ActivatedRouteSnapshot, ActivatedRoute, Router, convertToParamMap } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { of } from 'rxjs';

import { IGeneralLedger, GeneralLedger } from '../general-ledger.model';
import { GeneralLedgerService } from '../service/general-ledger.service';

import { GeneralLedgerRoutingResolveService } from './general-ledger-routing-resolve.service';

describe('GeneralLedger routing resolve service', () => {
  let mockRouter: Router;
  let mockActivatedRouteSnapshot: ActivatedRouteSnapshot;
  let routingResolveService: GeneralLedgerRoutingResolveService;
  let service: GeneralLedgerService;
  let resultGeneralLedger: IGeneralLedger | undefined;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule, RouterTestingModule.withRoutes([])],
      providers: [
        {
          provide: ActivatedRoute,
          useValue: {
            snapshot: {
              paramMap: convertToParamMap({}),
            },
          },
        },
      ],
    });
    mockRouter = TestBed.inject(Router);
    jest.spyOn(mockRouter, 'navigate').mockImplementation(() => Promise.resolve(true));
    mockActivatedRouteSnapshot = TestBed.inject(ActivatedRoute).snapshot;
    routingResolveService = TestBed.inject(GeneralLedgerRoutingResolveService);
    service = TestBed.inject(GeneralLedgerService);
    resultGeneralLedger = undefined;
  });

  describe('resolve', () => {
    it('should return IGeneralLedger returned by find', () => {
      // GIVEN
      service.find = jest.fn(id => of(new HttpResponse({ body: { id } })));
      mockActivatedRouteSnapshot.params = { id: 123 };

      // WHEN
      routingResolveService.resolve(mockActivatedRouteSnapshot).subscribe(result => {
        resultGeneralLedger = result;
      });

      // THEN
      expect(service.find).toBeCalledWith(123);
      expect(resultGeneralLedger).toEqual({ id: 123 });
    });

    it('should return new IGeneralLedger if id is not provided', () => {
      // GIVEN
      service.find = jest.fn();
      mockActivatedRouteSnapshot.params = {};

      // WHEN
      routingResolveService.resolve(mockActivatedRouteSnapshot).subscribe(result => {
        resultGeneralLedger = result;
      });

      // THEN
      expect(service.find).not.toBeCalled();
      expect(resultGeneralLedger).toEqual(new GeneralLedger());
    });

    it('should route to 404 page if data not found in server', () => {
      // GIVEN
      jest.spyOn(service, 'find').mockReturnValue(of(new HttpResponse({ body: null as unknown as GeneralLedger })));
      mockActivatedRouteSnapshot.params = { id: 123 };

      // WHEN
      routingResolveService.resolve(mockActivatedRouteSnapshot).subscribe(result => {
        resultGeneralLedger = result;
      });

      // THEN
      expect(service.find).toBeCalledWith(123);
      expect(resultGeneralLedger).toEqual(undefined);
      expect(mockRouter.navigate).toHaveBeenCalledWith(['404']);
    });
  });
});
