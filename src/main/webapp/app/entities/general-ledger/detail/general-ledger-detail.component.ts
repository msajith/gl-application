import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IGeneralLedger } from '../general-ledger.model';

@Component({
  selector: 'jhi-general-ledger-detail',
  templateUrl: './general-ledger-detail.component.html',
})
export class GeneralLedgerDetailComponent implements OnInit {
  generalLedger: IGeneralLedger | null = null;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ generalLedger }) => {
      this.generalLedger = generalLedger;
    });
  }

  previousState(): void {
    window.history.back();
  }
}
