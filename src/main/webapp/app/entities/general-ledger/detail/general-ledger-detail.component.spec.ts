import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { GeneralLedgerDetailComponent } from './general-ledger-detail.component';

describe('GeneralLedger Management Detail Component', () => {
  let comp: GeneralLedgerDetailComponent;
  let fixture: ComponentFixture<GeneralLedgerDetailComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [GeneralLedgerDetailComponent],
      providers: [
        {
          provide: ActivatedRoute,
          useValue: { data: of({ generalLedger: { id: 123 } }) },
        },
      ],
    })
      .overrideTemplate(GeneralLedgerDetailComponent, '')
      .compileComponents();
    fixture = TestBed.createComponent(GeneralLedgerDetailComponent);
    comp = fixture.componentInstance;
  });

  describe('OnInit', () => {
    it('Should load generalLedger on init', () => {
      // WHEN
      comp.ngOnInit();

      // THEN
      expect(comp.generalLedger).toEqual(expect.objectContaining({ id: 123 }));
    });
  });
});
