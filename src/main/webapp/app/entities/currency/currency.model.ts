export interface ICurrency {
  id?: number;
  name?: string | null;
  code?: string | null;
}

export class Currency implements ICurrency {
  constructor(public id?: number, public name?: string | null, public code?: string | null) {}
}

export function getCurrencyIdentifier(currency: ICurrency): number | undefined {
  return currency.id;
}
