import { NgModule } from '@angular/core';
import { SharedModule } from 'app/shared/shared.module';
import { PurposeOfTxnComponent } from './list/purpose-of-txn.component';
import { PurposeOfTxnDetailComponent } from './detail/purpose-of-txn-detail.component';
import { PurposeOfTxnUpdateComponent } from './update/purpose-of-txn-update.component';
import { PurposeOfTxnDeleteDialogComponent } from './delete/purpose-of-txn-delete-dialog.component';
import { PurposeOfTxnRoutingModule } from './route/purpose-of-txn-routing.module';

@NgModule({
  imports: [SharedModule, PurposeOfTxnRoutingModule],
  declarations: [PurposeOfTxnComponent, PurposeOfTxnDetailComponent, PurposeOfTxnUpdateComponent, PurposeOfTxnDeleteDialogComponent],
  entryComponents: [PurposeOfTxnDeleteDialogComponent],
})
export class PurposeOfTxnModule {}
