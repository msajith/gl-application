import { Component } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

import { IPurposeOfTxn } from '../purpose-of-txn.model';
import { PurposeOfTxnService } from '../service/purpose-of-txn.service';

@Component({
  templateUrl: './purpose-of-txn-delete-dialog.component.html',
})
export class PurposeOfTxnDeleteDialogComponent {
  purposeOfTxn?: IPurposeOfTxn;

  constructor(protected purposeOfTxnService: PurposeOfTxnService, protected activeModal: NgbActiveModal) {}

  cancel(): void {
    this.activeModal.dismiss();
  }

  confirmDelete(id: number): void {
    this.purposeOfTxnService.delete(id).subscribe(() => {
      this.activeModal.close('deleted');
    });
  }
}
