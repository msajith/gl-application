import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IPurposeOfTxn } from '../purpose-of-txn.model';

@Component({
  selector: 'jhi-purpose-of-txn-detail',
  templateUrl: './purpose-of-txn-detail.component.html',
})
export class PurposeOfTxnDetailComponent implements OnInit {
  purposeOfTxn: IPurposeOfTxn | null = null;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ purposeOfTxn }) => {
      this.purposeOfTxn = purposeOfTxn;
    });
  }

  previousState(): void {
    window.history.back();
  }
}
