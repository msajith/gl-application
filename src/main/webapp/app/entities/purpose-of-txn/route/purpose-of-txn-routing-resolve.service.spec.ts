import { TestBed } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { ActivatedRouteSnapshot, ActivatedRoute, Router, convertToParamMap } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { of } from 'rxjs';

import { IPurposeOfTxn, PurposeOfTxn } from '../purpose-of-txn.model';
import { PurposeOfTxnService } from '../service/purpose-of-txn.service';

import { PurposeOfTxnRoutingResolveService } from './purpose-of-txn-routing-resolve.service';

describe('PurposeOfTxn routing resolve service', () => {
  let mockRouter: Router;
  let mockActivatedRouteSnapshot: ActivatedRouteSnapshot;
  let routingResolveService: PurposeOfTxnRoutingResolveService;
  let service: PurposeOfTxnService;
  let resultPurposeOfTxn: IPurposeOfTxn | undefined;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule, RouterTestingModule.withRoutes([])],
      providers: [
        {
          provide: ActivatedRoute,
          useValue: {
            snapshot: {
              paramMap: convertToParamMap({}),
            },
          },
        },
      ],
    });
    mockRouter = TestBed.inject(Router);
    jest.spyOn(mockRouter, 'navigate').mockImplementation(() => Promise.resolve(true));
    mockActivatedRouteSnapshot = TestBed.inject(ActivatedRoute).snapshot;
    routingResolveService = TestBed.inject(PurposeOfTxnRoutingResolveService);
    service = TestBed.inject(PurposeOfTxnService);
    resultPurposeOfTxn = undefined;
  });

  describe('resolve', () => {
    it('should return IPurposeOfTxn returned by find', () => {
      // GIVEN
      service.find = jest.fn(id => of(new HttpResponse({ body: { id } })));
      mockActivatedRouteSnapshot.params = { id: 123 };

      // WHEN
      routingResolveService.resolve(mockActivatedRouteSnapshot).subscribe(result => {
        resultPurposeOfTxn = result;
      });

      // THEN
      expect(service.find).toBeCalledWith(123);
      expect(resultPurposeOfTxn).toEqual({ id: 123 });
    });

    it('should return new IPurposeOfTxn if id is not provided', () => {
      // GIVEN
      service.find = jest.fn();
      mockActivatedRouteSnapshot.params = {};

      // WHEN
      routingResolveService.resolve(mockActivatedRouteSnapshot).subscribe(result => {
        resultPurposeOfTxn = result;
      });

      // THEN
      expect(service.find).not.toBeCalled();
      expect(resultPurposeOfTxn).toEqual(new PurposeOfTxn());
    });

    it('should route to 404 page if data not found in server', () => {
      // GIVEN
      jest.spyOn(service, 'find').mockReturnValue(of(new HttpResponse({ body: null as unknown as PurposeOfTxn })));
      mockActivatedRouteSnapshot.params = { id: 123 };

      // WHEN
      routingResolveService.resolve(mockActivatedRouteSnapshot).subscribe(result => {
        resultPurposeOfTxn = result;
      });

      // THEN
      expect(service.find).toBeCalledWith(123);
      expect(resultPurposeOfTxn).toEqual(undefined);
      expect(mockRouter.navigate).toHaveBeenCalledWith(['404']);
    });
  });
});
