import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { UserRouteAccessService } from 'app/core/auth/user-route-access.service';
import { PurposeOfTxnComponent } from '../list/purpose-of-txn.component';
import { PurposeOfTxnDetailComponent } from '../detail/purpose-of-txn-detail.component';
import { PurposeOfTxnUpdateComponent } from '../update/purpose-of-txn-update.component';
import { PurposeOfTxnRoutingResolveService } from './purpose-of-txn-routing-resolve.service';

const purposeOfTxnRoute: Routes = [
  {
    path: '',
    component: PurposeOfTxnComponent,
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/view',
    component: PurposeOfTxnDetailComponent,
    resolve: {
      purposeOfTxn: PurposeOfTxnRoutingResolveService,
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: 'new',
    component: PurposeOfTxnUpdateComponent,
    resolve: {
      purposeOfTxn: PurposeOfTxnRoutingResolveService,
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/edit',
    component: PurposeOfTxnUpdateComponent,
    resolve: {
      purposeOfTxn: PurposeOfTxnRoutingResolveService,
    },
    canActivate: [UserRouteAccessService],
  },
];

@NgModule({
  imports: [RouterModule.forChild(purposeOfTxnRoute)],
  exports: [RouterModule],
})
export class PurposeOfTxnRoutingModule {}
