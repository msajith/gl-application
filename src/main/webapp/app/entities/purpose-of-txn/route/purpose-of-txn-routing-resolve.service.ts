import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, Router } from '@angular/router';
import { Observable, of, EMPTY } from 'rxjs';
import { mergeMap } from 'rxjs/operators';

import { IPurposeOfTxn, PurposeOfTxn } from '../purpose-of-txn.model';
import { PurposeOfTxnService } from '../service/purpose-of-txn.service';

@Injectable({ providedIn: 'root' })
export class PurposeOfTxnRoutingResolveService implements Resolve<IPurposeOfTxn> {
  constructor(protected service: PurposeOfTxnService, protected router: Router) {}

  resolve(route: ActivatedRouteSnapshot): Observable<IPurposeOfTxn> | Observable<never> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        mergeMap((purposeOfTxn: HttpResponse<PurposeOfTxn>) => {
          if (purposeOfTxn.body) {
            return of(purposeOfTxn.body);
          } else {
            this.router.navigate(['404']);
            return EMPTY;
          }
        })
      );
    }
    return of(new PurposeOfTxn());
  }
}
