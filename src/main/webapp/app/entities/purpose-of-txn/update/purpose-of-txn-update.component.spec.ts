import { ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { of, Subject, from } from 'rxjs';

import { PurposeOfTxnService } from '../service/purpose-of-txn.service';
import { IPurposeOfTxn, PurposeOfTxn } from '../purpose-of-txn.model';

import { PurposeOfTxnUpdateComponent } from './purpose-of-txn-update.component';

describe('PurposeOfTxn Management Update Component', () => {
  let comp: PurposeOfTxnUpdateComponent;
  let fixture: ComponentFixture<PurposeOfTxnUpdateComponent>;
  let activatedRoute: ActivatedRoute;
  let purposeOfTxnService: PurposeOfTxnService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule, RouterTestingModule.withRoutes([])],
      declarations: [PurposeOfTxnUpdateComponent],
      providers: [
        FormBuilder,
        {
          provide: ActivatedRoute,
          useValue: {
            params: from([{}]),
          },
        },
      ],
    })
      .overrideTemplate(PurposeOfTxnUpdateComponent, '')
      .compileComponents();

    fixture = TestBed.createComponent(PurposeOfTxnUpdateComponent);
    activatedRoute = TestBed.inject(ActivatedRoute);
    purposeOfTxnService = TestBed.inject(PurposeOfTxnService);

    comp = fixture.componentInstance;
  });

  describe('ngOnInit', () => {
    it('Should update editForm', () => {
      const purposeOfTxn: IPurposeOfTxn = { id: 456 };

      activatedRoute.data = of({ purposeOfTxn });
      comp.ngOnInit();

      expect(comp.editForm.value).toEqual(expect.objectContaining(purposeOfTxn));
    });
  });

  describe('save', () => {
    it('Should call update service on save for existing entity', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<PurposeOfTxn>>();
      const purposeOfTxn = { id: 123 };
      jest.spyOn(purposeOfTxnService, 'update').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ purposeOfTxn });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.next(new HttpResponse({ body: purposeOfTxn }));
      saveSubject.complete();

      // THEN
      expect(comp.previousState).toHaveBeenCalled();
      expect(purposeOfTxnService.update).toHaveBeenCalledWith(purposeOfTxn);
      expect(comp.isSaving).toEqual(false);
    });

    it('Should call create service on save for new entity', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<PurposeOfTxn>>();
      const purposeOfTxn = new PurposeOfTxn();
      jest.spyOn(purposeOfTxnService, 'create').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ purposeOfTxn });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.next(new HttpResponse({ body: purposeOfTxn }));
      saveSubject.complete();

      // THEN
      expect(purposeOfTxnService.create).toHaveBeenCalledWith(purposeOfTxn);
      expect(comp.isSaving).toEqual(false);
      expect(comp.previousState).toHaveBeenCalled();
    });

    it('Should set isSaving to false on error', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<PurposeOfTxn>>();
      const purposeOfTxn = { id: 123 };
      jest.spyOn(purposeOfTxnService, 'update').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ purposeOfTxn });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.error('This is an error!');

      // THEN
      expect(purposeOfTxnService.update).toHaveBeenCalledWith(purposeOfTxn);
      expect(comp.isSaving).toEqual(false);
      expect(comp.previousState).not.toHaveBeenCalled();
    });
  });
});
