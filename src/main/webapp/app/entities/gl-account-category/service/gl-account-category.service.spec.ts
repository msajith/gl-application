import { TestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';

import { IGlAccountCategory, GlAccountCategory } from '../gl-account-category.model';

import { GlAccountCategoryService } from './gl-account-category.service';

describe('GlAccountCategory Service', () => {
  let service: GlAccountCategoryService;
  let httpMock: HttpTestingController;
  let elemDefault: IGlAccountCategory;
  let expectedResult: IGlAccountCategory | IGlAccountCategory[] | boolean | null;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
    });
    expectedResult = null;
    service = TestBed.inject(GlAccountCategoryService);
    httpMock = TestBed.inject(HttpTestingController);

    elemDefault = {
      id: 0,
      name: 'AAAAAAA',
      code: 'AAAAAAA',
    };
  });

  describe('Service methods', () => {
    it('should find an element', () => {
      const returnedFromService = Object.assign({}, elemDefault);

      service.find(123).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'GET' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(elemDefault);
    });

    it('should create a GlAccountCategory', () => {
      const returnedFromService = Object.assign(
        {
          id: 0,
        },
        elemDefault
      );

      const expected = Object.assign({}, returnedFromService);

      service.create(new GlAccountCategory()).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'POST' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(expected);
    });

    it('should update a GlAccountCategory', () => {
      const returnedFromService = Object.assign(
        {
          id: 1,
          name: 'BBBBBB',
          code: 'BBBBBB',
        },
        elemDefault
      );

      const expected = Object.assign({}, returnedFromService);

      service.update(expected).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'PUT' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(expected);
    });

    it('should partial update a GlAccountCategory', () => {
      const patchObject = Object.assign({}, new GlAccountCategory());

      const returnedFromService = Object.assign(patchObject, elemDefault);

      const expected = Object.assign({}, returnedFromService);

      service.partialUpdate(patchObject).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'PATCH' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(expected);
    });

    it('should return a list of GlAccountCategory', () => {
      const returnedFromService = Object.assign(
        {
          id: 1,
          name: 'BBBBBB',
          code: 'BBBBBB',
        },
        elemDefault
      );

      const expected = Object.assign({}, returnedFromService);

      service.query().subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'GET' });
      req.flush([returnedFromService]);
      httpMock.verify();
      expect(expectedResult).toContainEqual(expected);
    });

    it('should delete a GlAccountCategory', () => {
      service.delete(123).subscribe(resp => (expectedResult = resp.ok));

      const req = httpMock.expectOne({ method: 'DELETE' });
      req.flush({ status: 200 });
      expect(expectedResult);
    });

    describe('addGlAccountCategoryToCollectionIfMissing', () => {
      it('should add a GlAccountCategory to an empty array', () => {
        const glAccountCategory: IGlAccountCategory = { id: 123 };
        expectedResult = service.addGlAccountCategoryToCollectionIfMissing([], glAccountCategory);
        expect(expectedResult).toHaveLength(1);
        expect(expectedResult).toContain(glAccountCategory);
      });

      it('should not add a GlAccountCategory to an array that contains it', () => {
        const glAccountCategory: IGlAccountCategory = { id: 123 };
        const glAccountCategoryCollection: IGlAccountCategory[] = [
          {
            ...glAccountCategory,
          },
          { id: 456 },
        ];
        expectedResult = service.addGlAccountCategoryToCollectionIfMissing(glAccountCategoryCollection, glAccountCategory);
        expect(expectedResult).toHaveLength(2);
      });

      it("should add a GlAccountCategory to an array that doesn't contain it", () => {
        const glAccountCategory: IGlAccountCategory = { id: 123 };
        const glAccountCategoryCollection: IGlAccountCategory[] = [{ id: 456 }];
        expectedResult = service.addGlAccountCategoryToCollectionIfMissing(glAccountCategoryCollection, glAccountCategory);
        expect(expectedResult).toHaveLength(2);
        expect(expectedResult).toContain(glAccountCategory);
      });

      it('should add only unique GlAccountCategory to an array', () => {
        const glAccountCategoryArray: IGlAccountCategory[] = [{ id: 123 }, { id: 456 }, { id: 15491 }];
        const glAccountCategoryCollection: IGlAccountCategory[] = [{ id: 123 }];
        expectedResult = service.addGlAccountCategoryToCollectionIfMissing(glAccountCategoryCollection, ...glAccountCategoryArray);
        expect(expectedResult).toHaveLength(3);
      });

      it('should accept varargs', () => {
        const glAccountCategory: IGlAccountCategory = { id: 123 };
        const glAccountCategory2: IGlAccountCategory = { id: 456 };
        expectedResult = service.addGlAccountCategoryToCollectionIfMissing([], glAccountCategory, glAccountCategory2);
        expect(expectedResult).toHaveLength(2);
        expect(expectedResult).toContain(glAccountCategory);
        expect(expectedResult).toContain(glAccountCategory2);
      });

      it('should accept null and undefined values', () => {
        const glAccountCategory: IGlAccountCategory = { id: 123 };
        expectedResult = service.addGlAccountCategoryToCollectionIfMissing([], null, glAccountCategory, undefined);
        expect(expectedResult).toHaveLength(1);
        expect(expectedResult).toContain(glAccountCategory);
      });

      it('should return initial array if no GlAccountCategory is added', () => {
        const glAccountCategoryCollection: IGlAccountCategory[] = [{ id: 123 }];
        expectedResult = service.addGlAccountCategoryToCollectionIfMissing(glAccountCategoryCollection, undefined, null);
        expect(expectedResult).toEqual(glAccountCategoryCollection);
      });
    });
  });

  afterEach(() => {
    httpMock.verify();
  });
});
