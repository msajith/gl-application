import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { isPresent } from 'app/core/util/operators';
import { ApplicationConfigService } from 'app/core/config/application-config.service';
import { createRequestOption } from 'app/core/request/request-util';
import { IGlAccountCategory, getGlAccountCategoryIdentifier } from '../gl-account-category.model';

export type EntityResponseType = HttpResponse<IGlAccountCategory>;
export type EntityArrayResponseType = HttpResponse<IGlAccountCategory[]>;

@Injectable({ providedIn: 'root' })
export class GlAccountCategoryService {
  protected resourceUrl = this.applicationConfigService.getEndpointFor('api/gl-account-categories');

  constructor(protected http: HttpClient, protected applicationConfigService: ApplicationConfigService) {}

  create(glAccountCategory: IGlAccountCategory): Observable<EntityResponseType> {
    return this.http.post<IGlAccountCategory>(this.resourceUrl, glAccountCategory, { observe: 'response' });
  }

  update(glAccountCategory: IGlAccountCategory): Observable<EntityResponseType> {
    return this.http.put<IGlAccountCategory>(
      `${this.resourceUrl}/${getGlAccountCategoryIdentifier(glAccountCategory) as number}`,
      glAccountCategory,
      { observe: 'response' }
    );
  }

  partialUpdate(glAccountCategory: IGlAccountCategory): Observable<EntityResponseType> {
    return this.http.patch<IGlAccountCategory>(
      `${this.resourceUrl}/${getGlAccountCategoryIdentifier(glAccountCategory) as number}`,
      glAccountCategory,
      { observe: 'response' }
    );
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http.get<IGlAccountCategory>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<IGlAccountCategory[]>(this.resourceUrl, { params: options, observe: 'response' });
  }

  delete(id: number): Observable<HttpResponse<{}>> {
    return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  addGlAccountCategoryToCollectionIfMissing(
    glAccountCategoryCollection: IGlAccountCategory[],
    ...glAccountCategoriesToCheck: (IGlAccountCategory | null | undefined)[]
  ): IGlAccountCategory[] {
    const glAccountCategories: IGlAccountCategory[] = glAccountCategoriesToCheck.filter(isPresent);
    if (glAccountCategories.length > 0) {
      const glAccountCategoryCollectionIdentifiers = glAccountCategoryCollection.map(
        glAccountCategoryItem => getGlAccountCategoryIdentifier(glAccountCategoryItem)!
      );
      const glAccountCategoriesToAdd = glAccountCategories.filter(glAccountCategoryItem => {
        const glAccountCategoryIdentifier = getGlAccountCategoryIdentifier(glAccountCategoryItem);
        if (glAccountCategoryIdentifier == null || glAccountCategoryCollectionIdentifiers.includes(glAccountCategoryIdentifier)) {
          return false;
        }
        glAccountCategoryCollectionIdentifiers.push(glAccountCategoryIdentifier);
        return true;
      });
      return [...glAccountCategoriesToAdd, ...glAccountCategoryCollection];
    }
    return glAccountCategoryCollection;
  }
}
