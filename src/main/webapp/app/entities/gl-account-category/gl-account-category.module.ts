import { NgModule } from '@angular/core';
import { SharedModule } from 'app/shared/shared.module';
import { GlAccountCategoryComponent } from './list/gl-account-category.component';
import { GlAccountCategoryDetailComponent } from './detail/gl-account-category-detail.component';
import { GlAccountCategoryUpdateComponent } from './update/gl-account-category-update.component';
import { GlAccountCategoryDeleteDialogComponent } from './delete/gl-account-category-delete-dialog.component';
import { GlAccountCategoryRoutingModule } from './route/gl-account-category-routing.module';

@NgModule({
  imports: [SharedModule, GlAccountCategoryRoutingModule],
  declarations: [
    GlAccountCategoryComponent,
    GlAccountCategoryDetailComponent,
    GlAccountCategoryUpdateComponent,
    GlAccountCategoryDeleteDialogComponent,
  ],
  entryComponents: [GlAccountCategoryDeleteDialogComponent],
})
export class GlAccountCategoryModule {}
