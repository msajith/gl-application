import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { finalize } from 'rxjs/operators';

import { IGlAccountCategory, GlAccountCategory } from '../gl-account-category.model';
import { GlAccountCategoryService } from '../service/gl-account-category.service';

@Component({
  selector: 'jhi-gl-account-category-update',
  templateUrl: './gl-account-category-update.component.html',
})
export class GlAccountCategoryUpdateComponent implements OnInit {
  isSaving = false;

  editForm = this.fb.group({
    id: [],
    name: [],
    code: [],
  });

  constructor(
    protected glAccountCategoryService: GlAccountCategoryService,
    protected activatedRoute: ActivatedRoute,
    protected fb: FormBuilder
  ) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ glAccountCategory }) => {
      this.updateForm(glAccountCategory);
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const glAccountCategory = this.createFromForm();
    if (glAccountCategory.id !== undefined) {
      this.subscribeToSaveResponse(this.glAccountCategoryService.update(glAccountCategory));
    } else {
      this.subscribeToSaveResponse(this.glAccountCategoryService.create(glAccountCategory));
    }
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IGlAccountCategory>>): void {
    result.pipe(finalize(() => this.onSaveFinalize())).subscribe({
      next: () => this.onSaveSuccess(),
      error: () => this.onSaveError(),
    });
  }

  protected onSaveSuccess(): void {
    this.previousState();
  }

  protected onSaveError(): void {
    // Api for inheritance.
  }

  protected onSaveFinalize(): void {
    this.isSaving = false;
  }

  protected updateForm(glAccountCategory: IGlAccountCategory): void {
    this.editForm.patchValue({
      id: glAccountCategory.id,
      name: glAccountCategory.name,
      code: glAccountCategory.code,
    });
  }

  protected createFromForm(): IGlAccountCategory {
    return {
      ...new GlAccountCategory(),
      id: this.editForm.get(['id'])!.value,
      name: this.editForm.get(['name'])!.value,
      code: this.editForm.get(['code'])!.value,
    };
  }
}
