import { ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpHeaders, HttpResponse } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { of } from 'rxjs';

import { TransactionTypeService } from '../service/transaction-type.service';

import { TransactionTypeComponent } from './transaction-type.component';

describe('TransactionType Management Component', () => {
  let comp: TransactionTypeComponent;
  let fixture: ComponentFixture<TransactionTypeComponent>;
  let service: TransactionTypeService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      declarations: [TransactionTypeComponent],
    })
      .overrideTemplate(TransactionTypeComponent, '')
      .compileComponents();

    fixture = TestBed.createComponent(TransactionTypeComponent);
    comp = fixture.componentInstance;
    service = TestBed.inject(TransactionTypeService);

    const headers = new HttpHeaders();
    jest.spyOn(service, 'query').mockReturnValue(
      of(
        new HttpResponse({
          body: [{ id: 123 }],
          headers,
        })
      )
    );
  });

  it('Should call load all on init', () => {
    // WHEN
    comp.ngOnInit();

    // THEN
    expect(service.query).toHaveBeenCalled();
    expect(comp.transactionTypes?.[0]).toEqual(expect.objectContaining({ id: 123 }));
  });
});
