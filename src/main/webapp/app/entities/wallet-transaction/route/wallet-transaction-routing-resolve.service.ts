import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, Router } from '@angular/router';
import { Observable, of, EMPTY } from 'rxjs';
import { mergeMap } from 'rxjs/operators';

import { IWalletTransaction, WalletTransaction } from '../wallet-transaction.model';
import { WalletTransactionService } from '../service/wallet-transaction.service';

@Injectable({ providedIn: 'root' })
export class WalletTransactionRoutingResolveService implements Resolve<IWalletTransaction> {
  constructor(protected service: WalletTransactionService, protected router: Router) {}

  resolve(route: ActivatedRouteSnapshot): Observable<IWalletTransaction> | Observable<never> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        mergeMap((walletTransaction: HttpResponse<WalletTransaction>) => {
          if (walletTransaction.body) {
            return of(walletTransaction.body);
          } else {
            this.router.navigate(['404']);
            return EMPTY;
          }
        })
      );
    }
    return of(new WalletTransaction());
  }
}
