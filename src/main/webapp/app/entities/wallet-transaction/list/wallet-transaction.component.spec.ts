import { ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpHeaders, HttpResponse } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { of } from 'rxjs';

import { WalletTransactionService } from '../service/wallet-transaction.service';

import { WalletTransactionComponent } from './wallet-transaction.component';

describe('WalletTransaction Management Component', () => {
  let comp: WalletTransactionComponent;
  let fixture: ComponentFixture<WalletTransactionComponent>;
  let service: WalletTransactionService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      declarations: [WalletTransactionComponent],
    })
      .overrideTemplate(WalletTransactionComponent, '')
      .compileComponents();

    fixture = TestBed.createComponent(WalletTransactionComponent);
    comp = fixture.componentInstance;
    service = TestBed.inject(WalletTransactionService);

    const headers = new HttpHeaders();
    jest.spyOn(service, 'query').mockReturnValue(
      of(
        new HttpResponse({
          body: [{ id: 123 }],
          headers,
        })
      )
    );
  });

  it('Should call load all on init', () => {
    // WHEN
    comp.ngOnInit();

    // THEN
    expect(service.query).toHaveBeenCalled();
    expect(comp.walletTransactions?.[0]).toEqual(expect.objectContaining({ id: 123 }));
  });
});
