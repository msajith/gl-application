import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { WalletTransactionDetailComponent } from './wallet-transaction-detail.component';

describe('WalletTransaction Management Detail Component', () => {
  let comp: WalletTransactionDetailComponent;
  let fixture: ComponentFixture<WalletTransactionDetailComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [WalletTransactionDetailComponent],
      providers: [
        {
          provide: ActivatedRoute,
          useValue: { data: of({ walletTransaction: { id: 123 } }) },
        },
      ],
    })
      .overrideTemplate(WalletTransactionDetailComponent, '')
      .compileComponents();
    fixture = TestBed.createComponent(WalletTransactionDetailComponent);
    comp = fixture.componentInstance;
  });

  describe('OnInit', () => {
    it('Should load walletTransaction on init', () => {
      // WHEN
      comp.ngOnInit();

      // THEN
      expect(comp.walletTransaction).toEqual(expect.objectContaining({ id: 123 }));
    });
  });
});
