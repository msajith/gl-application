import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IWalletTransaction } from '../wallet-transaction.model';

@Component({
  selector: 'jhi-wallet-transaction-detail',
  templateUrl: './wallet-transaction-detail.component.html',
})
export class WalletTransactionDetailComponent implements OnInit {
  walletTransaction: IWalletTransaction | null = null;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ walletTransaction }) => {
      this.walletTransaction = walletTransaction;
    });
  }

  previousState(): void {
    window.history.back();
  }
}
