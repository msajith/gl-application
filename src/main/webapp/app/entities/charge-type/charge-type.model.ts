export interface IChargeType {
  id?: number;
  name?: string | null;
  code?: string | null;
}

export class ChargeType implements IChargeType {
  constructor(public id?: number, public name?: string | null, public code?: string | null) {}
}

export function getChargeTypeIdentifier(chargeType: IChargeType): number | undefined {
  return chargeType.id;
}
