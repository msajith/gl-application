import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { isPresent } from 'app/core/util/operators';
import { ApplicationConfigService } from 'app/core/config/application-config.service';
import { createRequestOption } from 'app/core/request/request-util';
import { IChargeType, getChargeTypeIdentifier } from '../charge-type.model';

export type EntityResponseType = HttpResponse<IChargeType>;
export type EntityArrayResponseType = HttpResponse<IChargeType[]>;

@Injectable({ providedIn: 'root' })
export class ChargeTypeService {
  protected resourceUrl = this.applicationConfigService.getEndpointFor('api/charge-types');

  constructor(protected http: HttpClient, protected applicationConfigService: ApplicationConfigService) {}

  create(chargeType: IChargeType): Observable<EntityResponseType> {
    return this.http.post<IChargeType>(this.resourceUrl, chargeType, { observe: 'response' });
  }

  update(chargeType: IChargeType): Observable<EntityResponseType> {
    return this.http.put<IChargeType>(`${this.resourceUrl}/${getChargeTypeIdentifier(chargeType) as number}`, chargeType, {
      observe: 'response',
    });
  }

  partialUpdate(chargeType: IChargeType): Observable<EntityResponseType> {
    return this.http.patch<IChargeType>(`${this.resourceUrl}/${getChargeTypeIdentifier(chargeType) as number}`, chargeType, {
      observe: 'response',
    });
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http.get<IChargeType>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<IChargeType[]>(this.resourceUrl, { params: options, observe: 'response' });
  }

  delete(id: number): Observable<HttpResponse<{}>> {
    return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  addChargeTypeToCollectionIfMissing(
    chargeTypeCollection: IChargeType[],
    ...chargeTypesToCheck: (IChargeType | null | undefined)[]
  ): IChargeType[] {
    const chargeTypes: IChargeType[] = chargeTypesToCheck.filter(isPresent);
    if (chargeTypes.length > 0) {
      const chargeTypeCollectionIdentifiers = chargeTypeCollection.map(chargeTypeItem => getChargeTypeIdentifier(chargeTypeItem)!);
      const chargeTypesToAdd = chargeTypes.filter(chargeTypeItem => {
        const chargeTypeIdentifier = getChargeTypeIdentifier(chargeTypeItem);
        if (chargeTypeIdentifier == null || chargeTypeCollectionIdentifiers.includes(chargeTypeIdentifier)) {
          return false;
        }
        chargeTypeCollectionIdentifiers.push(chargeTypeIdentifier);
        return true;
      });
      return [...chargeTypesToAdd, ...chargeTypeCollection];
    }
    return chargeTypeCollection;
  }
}
