import { TestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';

import { IChargeType, ChargeType } from '../charge-type.model';

import { ChargeTypeService } from './charge-type.service';

describe('ChargeType Service', () => {
  let service: ChargeTypeService;
  let httpMock: HttpTestingController;
  let elemDefault: IChargeType;
  let expectedResult: IChargeType | IChargeType[] | boolean | null;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
    });
    expectedResult = null;
    service = TestBed.inject(ChargeTypeService);
    httpMock = TestBed.inject(HttpTestingController);

    elemDefault = {
      id: 0,
      name: 'AAAAAAA',
      code: 'AAAAAAA',
    };
  });

  describe('Service methods', () => {
    it('should find an element', () => {
      const returnedFromService = Object.assign({}, elemDefault);

      service.find(123).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'GET' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(elemDefault);
    });

    it('should create a ChargeType', () => {
      const returnedFromService = Object.assign(
        {
          id: 0,
        },
        elemDefault
      );

      const expected = Object.assign({}, returnedFromService);

      service.create(new ChargeType()).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'POST' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(expected);
    });

    it('should update a ChargeType', () => {
      const returnedFromService = Object.assign(
        {
          id: 1,
          name: 'BBBBBB',
          code: 'BBBBBB',
        },
        elemDefault
      );

      const expected = Object.assign({}, returnedFromService);

      service.update(expected).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'PUT' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(expected);
    });

    it('should partial update a ChargeType', () => {
      const patchObject = Object.assign(
        {
          name: 'BBBBBB',
          code: 'BBBBBB',
        },
        new ChargeType()
      );

      const returnedFromService = Object.assign(patchObject, elemDefault);

      const expected = Object.assign({}, returnedFromService);

      service.partialUpdate(patchObject).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'PATCH' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(expected);
    });

    it('should return a list of ChargeType', () => {
      const returnedFromService = Object.assign(
        {
          id: 1,
          name: 'BBBBBB',
          code: 'BBBBBB',
        },
        elemDefault
      );

      const expected = Object.assign({}, returnedFromService);

      service.query().subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'GET' });
      req.flush([returnedFromService]);
      httpMock.verify();
      expect(expectedResult).toContainEqual(expected);
    });

    it('should delete a ChargeType', () => {
      service.delete(123).subscribe(resp => (expectedResult = resp.ok));

      const req = httpMock.expectOne({ method: 'DELETE' });
      req.flush({ status: 200 });
      expect(expectedResult);
    });

    describe('addChargeTypeToCollectionIfMissing', () => {
      it('should add a ChargeType to an empty array', () => {
        const chargeType: IChargeType = { id: 123 };
        expectedResult = service.addChargeTypeToCollectionIfMissing([], chargeType);
        expect(expectedResult).toHaveLength(1);
        expect(expectedResult).toContain(chargeType);
      });

      it('should not add a ChargeType to an array that contains it', () => {
        const chargeType: IChargeType = { id: 123 };
        const chargeTypeCollection: IChargeType[] = [
          {
            ...chargeType,
          },
          { id: 456 },
        ];
        expectedResult = service.addChargeTypeToCollectionIfMissing(chargeTypeCollection, chargeType);
        expect(expectedResult).toHaveLength(2);
      });

      it("should add a ChargeType to an array that doesn't contain it", () => {
        const chargeType: IChargeType = { id: 123 };
        const chargeTypeCollection: IChargeType[] = [{ id: 456 }];
        expectedResult = service.addChargeTypeToCollectionIfMissing(chargeTypeCollection, chargeType);
        expect(expectedResult).toHaveLength(2);
        expect(expectedResult).toContain(chargeType);
      });

      it('should add only unique ChargeType to an array', () => {
        const chargeTypeArray: IChargeType[] = [{ id: 123 }, { id: 456 }, { id: 64854 }];
        const chargeTypeCollection: IChargeType[] = [{ id: 123 }];
        expectedResult = service.addChargeTypeToCollectionIfMissing(chargeTypeCollection, ...chargeTypeArray);
        expect(expectedResult).toHaveLength(3);
      });

      it('should accept varargs', () => {
        const chargeType: IChargeType = { id: 123 };
        const chargeType2: IChargeType = { id: 456 };
        expectedResult = service.addChargeTypeToCollectionIfMissing([], chargeType, chargeType2);
        expect(expectedResult).toHaveLength(2);
        expect(expectedResult).toContain(chargeType);
        expect(expectedResult).toContain(chargeType2);
      });

      it('should accept null and undefined values', () => {
        const chargeType: IChargeType = { id: 123 };
        expectedResult = service.addChargeTypeToCollectionIfMissing([], null, chargeType, undefined);
        expect(expectedResult).toHaveLength(1);
        expect(expectedResult).toContain(chargeType);
      });

      it('should return initial array if no ChargeType is added', () => {
        const chargeTypeCollection: IChargeType[] = [{ id: 123 }];
        expectedResult = service.addChargeTypeToCollectionIfMissing(chargeTypeCollection, undefined, null);
        expect(expectedResult).toEqual(chargeTypeCollection);
      });
    });
  });

  afterEach(() => {
    httpMock.verify();
  });
});
