import { ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { of, Subject, from } from 'rxjs';

import { ChargeTypeService } from '../service/charge-type.service';
import { IChargeType, ChargeType } from '../charge-type.model';

import { ChargeTypeUpdateComponent } from './charge-type-update.component';

describe('ChargeType Management Update Component', () => {
  let comp: ChargeTypeUpdateComponent;
  let fixture: ComponentFixture<ChargeTypeUpdateComponent>;
  let activatedRoute: ActivatedRoute;
  let chargeTypeService: ChargeTypeService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule, RouterTestingModule.withRoutes([])],
      declarations: [ChargeTypeUpdateComponent],
      providers: [
        FormBuilder,
        {
          provide: ActivatedRoute,
          useValue: {
            params: from([{}]),
          },
        },
      ],
    })
      .overrideTemplate(ChargeTypeUpdateComponent, '')
      .compileComponents();

    fixture = TestBed.createComponent(ChargeTypeUpdateComponent);
    activatedRoute = TestBed.inject(ActivatedRoute);
    chargeTypeService = TestBed.inject(ChargeTypeService);

    comp = fixture.componentInstance;
  });

  describe('ngOnInit', () => {
    it('Should update editForm', () => {
      const chargeType: IChargeType = { id: 456 };

      activatedRoute.data = of({ chargeType });
      comp.ngOnInit();

      expect(comp.editForm.value).toEqual(expect.objectContaining(chargeType));
    });
  });

  describe('save', () => {
    it('Should call update service on save for existing entity', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<ChargeType>>();
      const chargeType = { id: 123 };
      jest.spyOn(chargeTypeService, 'update').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ chargeType });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.next(new HttpResponse({ body: chargeType }));
      saveSubject.complete();

      // THEN
      expect(comp.previousState).toHaveBeenCalled();
      expect(chargeTypeService.update).toHaveBeenCalledWith(chargeType);
      expect(comp.isSaving).toEqual(false);
    });

    it('Should call create service on save for new entity', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<ChargeType>>();
      const chargeType = new ChargeType();
      jest.spyOn(chargeTypeService, 'create').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ chargeType });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.next(new HttpResponse({ body: chargeType }));
      saveSubject.complete();

      // THEN
      expect(chargeTypeService.create).toHaveBeenCalledWith(chargeType);
      expect(comp.isSaving).toEqual(false);
      expect(comp.previousState).toHaveBeenCalled();
    });

    it('Should set isSaving to false on error', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<ChargeType>>();
      const chargeType = { id: 123 };
      jest.spyOn(chargeTypeService, 'update').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ chargeType });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.error('This is an error!');

      // THEN
      expect(chargeTypeService.update).toHaveBeenCalledWith(chargeType);
      expect(comp.isSaving).toEqual(false);
      expect(comp.previousState).not.toHaveBeenCalled();
    });
  });
});
