import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

@NgModule({
  imports: [
    RouterModule.forChild([
      {
        path: 'country',
        data: { pageTitle: 'Countries' },
        loadChildren: () => import('./country/country.module').then(m => m.CountryModule),
      },
      {
        path: 'currency',
        data: { pageTitle: 'Currencies' },
        loadChildren: () => import('./currency/currency.module').then(m => m.CurrencyModule),
      },
      {
        path: 'account-type',
        data: { pageTitle: 'AccountTypes' },
        loadChildren: () => import('./account-type/account-type.module').then(m => m.AccountTypeModule),
      },
      {
        path: 'account-category',
        data: { pageTitle: 'AccountCategories' },
        loadChildren: () => import('./account-category/account-category.module').then(m => m.AccountCategoryModule),
      },
      {
        path: 'customer-type',
        data: { pageTitle: 'CustomerTypes' },
        loadChildren: () => import('./customer-type/customer-type.module').then(m => m.CustomerTypeModule),
      },
      {
        path: 'customer-segment',
        data: { pageTitle: 'CustomerSegments' },
        loadChildren: () => import('./customer-segment/customer-segment.module').then(m => m.CustomerSegmentModule),
      },
      {
        path: 'purpose-of-txn',
        data: { pageTitle: 'PurposeOfTxns' },
        loadChildren: () => import('./purpose-of-txn/purpose-of-txn.module').then(m => m.PurposeOfTxnModule),
      },
      {
        path: 'gl-entry-type',
        data: { pageTitle: 'GlEntryTypes' },
        loadChildren: () => import('./gl-entry-type/gl-entry-type.module').then(m => m.GlEntryTypeModule),
      },
      {
        path: 'funding-source-type',
        data: { pageTitle: 'FundingSourceTypes' },
        loadChildren: () => import('./funding-source-type/funding-source-type.module').then(m => m.FundingSourceTypeModule),
      },
      {
        path: 'gl-account-category',
        data: { pageTitle: 'GlAccountCategories' },
        loadChildren: () => import('./gl-account-category/gl-account-category.module').then(m => m.GlAccountCategoryModule),
      },
      {
        path: 'transaction-type',
        data: { pageTitle: 'TransactionTypes' },
        loadChildren: () => import('./transaction-type/transaction-type.module').then(m => m.TransactionTypeModule),
      },
      {
        path: 'customer',
        data: { pageTitle: 'Customers' },
        loadChildren: () => import('./customer/customer.module').then(m => m.CustomerModule),
      },
      {
        path: 'customer-address',
        data: { pageTitle: 'CustomerAddresses' },
        loadChildren: () => import('./customer-address/customer-address.module').then(m => m.CustomerAddressModule),
      },
      {
        path: 'wallet-account',
        data: { pageTitle: 'WalletAccounts' },
        loadChildren: () => import('./wallet-account/wallet-account.module').then(m => m.WalletAccountModule),
      },
      {
        path: 'casa-account',
        data: { pageTitle: 'CasaAccounts' },
        loadChildren: () => import('./casa-account/casa-account.module').then(m => m.CasaAccountModule),
      },
      {
        path: 'merchant',
        data: { pageTitle: 'Merchants' },
        loadChildren: () => import('./merchant/merchant.module').then(m => m.MerchantModule),
      },
      {
        path: 'wallet-topup-transaction',
        data: { pageTitle: 'WalletTopupTransactions' },
        loadChildren: () => import('./wallet-topup-transaction/wallet-topup-transaction.module').then(m => m.WalletTopupTransactionModule),
      },
      {
        path: 'wallet-transaction',
        data: { pageTitle: 'WalletTransactions' },
        loadChildren: () => import('./wallet-transaction/wallet-transaction.module').then(m => m.WalletTransactionModule),
      },
      {
        path: 'remittance-transaction',
        data: { pageTitle: 'RemittanceTransactions' },
        loadChildren: () => import('./remittance-transaction/remittance-transaction.module').then(m => m.RemittanceTransactionModule),
      },
      {
        path: 'sub-ledger',
        data: { pageTitle: 'SubLedgers' },
        loadChildren: () => import('./sub-ledger/sub-ledger.module').then(m => m.SubLedgerModule),
      },
      {
        path: 'gl-account',
        data: { pageTitle: 'GlAccounts' },
        loadChildren: () => import('./gl-account/gl-account.module').then(m => m.GlAccountModule),
      },
      {
        path: 'general-ledger',
        data: { pageTitle: 'GeneralLedgers' },
        loadChildren: () => import('./general-ledger/general-ledger.module').then(m => m.GeneralLedgerModule),
      },
      {
        path: 'fx-rate',
        data: { pageTitle: 'FxRates' },
        loadChildren: () => import('./fx-rate/fx-rate.module').then(m => m.FxRateModule),
      },
      {
        path: 'charge-type',
        data: { pageTitle: 'ChargeTypes' },
        loadChildren: () => import('./charge-type/charge-type.module').then(m => m.ChargeTypeModule),
      },
      {
        path: 'charge',
        data: { pageTitle: 'Charges' },
        loadChildren: () => import('./charge/charge.module').then(m => m.ChargeModule),
      },
      {
        path: 'app-config',
        data: { pageTitle: 'AppConfigs' },
        loadChildren: () => import('./app-config/app-config.module').then(m => m.AppConfigModule),
      },
      {
        path: 'casa-account-day-balance',
        data: { pageTitle: 'CasaAccountDayBalances' },
        loadChildren: () => import('./casa-account-day-balance/casa-account-day-balance.module').then(m => m.CasaAccountDayBalanceModule),
      },
      {
        path: 'beneficiary',
        data: { pageTitle: 'Beneficiaries' },
        loadChildren: () => import('./beneficiary/beneficiary.module').then(m => m.BeneficiaryModule),
      },
      /* jhipster-needle-add-entity-route - JHipster will add entity modules routes here */
    ]),
  ],
})
export class EntityRoutingModule {}
