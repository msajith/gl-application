import dayjs from 'dayjs/esm';
import { ICasaAccount } from 'app/entities/casa-account/casa-account.model';

export interface ICasaAccountDayBalance {
  id?: number;
  accountNo?: string;
  date?: dayjs.Dayjs | null;
  closingBalance?: number | null;
  openingBalance?: number | null;
  totalDebit?: number | null;
  totalCredit?: number | null;
  availableBalance?: number | null;
  currentBalance?: number | null;
  createdOn?: dayjs.Dayjs | null;
  createdBy?: string | null;
  casaAccount?: ICasaAccount | null;
}

export class CasaAccountDayBalance implements ICasaAccountDayBalance {
  constructor(
    public id?: number,
    public accountNo?: string,
    public date?: dayjs.Dayjs | null,
    public closingBalance?: number | null,
    public openingBalance?: number | null,
    public totalDebit?: number | null,
    public totalCredit?: number | null,
    public availableBalance?: number | null,
    public currentBalance?: number | null,
    public createdOn?: dayjs.Dayjs | null,
    public createdBy?: string | null,
    public casaAccount?: ICasaAccount | null
  ) {}
}

export function getCasaAccountDayBalanceIdentifier(casaAccountDayBalance: ICasaAccountDayBalance): number | undefined {
  return casaAccountDayBalance.id;
}
