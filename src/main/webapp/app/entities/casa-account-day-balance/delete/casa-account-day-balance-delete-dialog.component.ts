import { Component } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

import { ICasaAccountDayBalance } from '../casa-account-day-balance.model';
import { CasaAccountDayBalanceService } from '../service/casa-account-day-balance.service';

@Component({
  templateUrl: './casa-account-day-balance-delete-dialog.component.html',
})
export class CasaAccountDayBalanceDeleteDialogComponent {
  casaAccountDayBalance?: ICasaAccountDayBalance;

  constructor(protected casaAccountDayBalanceService: CasaAccountDayBalanceService, protected activeModal: NgbActiveModal) {}

  cancel(): void {
    this.activeModal.dismiss();
  }

  confirmDelete(id: number): void {
    this.casaAccountDayBalanceService.delete(id).subscribe(() => {
      this.activeModal.close('deleted');
    });
  }
}
