import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { finalize, map } from 'rxjs/operators';

import dayjs from 'dayjs/esm';
import { DATE_TIME_FORMAT } from 'app/config/input.constants';

import { ICasaAccountDayBalance, CasaAccountDayBalance } from '../casa-account-day-balance.model';
import { CasaAccountDayBalanceService } from '../service/casa-account-day-balance.service';
import { ICasaAccount } from 'app/entities/casa-account/casa-account.model';
import { CasaAccountService } from 'app/entities/casa-account/service/casa-account.service';

@Component({
  selector: 'jhi-casa-account-day-balance-update',
  templateUrl: './casa-account-day-balance-update.component.html',
})
export class CasaAccountDayBalanceUpdateComponent implements OnInit {
  isSaving = false;

  casaAccountsSharedCollection: ICasaAccount[] = [];

  editForm = this.fb.group({
    id: [],
    accountNo: [null, [Validators.required]],
    date: [],
    closingBalance: [],
    openingBalance: [],
    totalDebit: [],
    totalCredit: [],
    availableBalance: [],
    currentBalance: [],
    createdOn: [],
    createdBy: [],
    casaAccount: [],
  });

  constructor(
    protected casaAccountDayBalanceService: CasaAccountDayBalanceService,
    protected casaAccountService: CasaAccountService,
    protected activatedRoute: ActivatedRoute,
    protected fb: FormBuilder
  ) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ casaAccountDayBalance }) => {
      if (casaAccountDayBalance.id === undefined) {
        const today = dayjs().startOf('day');
        casaAccountDayBalance.createdOn = today;
      }

      this.updateForm(casaAccountDayBalance);

      this.loadRelationshipsOptions();
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const casaAccountDayBalance = this.createFromForm();
    if (casaAccountDayBalance.id !== undefined) {
      this.subscribeToSaveResponse(this.casaAccountDayBalanceService.update(casaAccountDayBalance));
    } else {
      this.subscribeToSaveResponse(this.casaAccountDayBalanceService.create(casaAccountDayBalance));
    }
  }

  trackCasaAccountById(index: number, item: ICasaAccount): number {
    return item.id!;
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<ICasaAccountDayBalance>>): void {
    result.pipe(finalize(() => this.onSaveFinalize())).subscribe({
      next: () => this.onSaveSuccess(),
      error: () => this.onSaveError(),
    });
  }

  protected onSaveSuccess(): void {
    this.previousState();
  }

  protected onSaveError(): void {
    // Api for inheritance.
  }

  protected onSaveFinalize(): void {
    this.isSaving = false;
  }

  protected updateForm(casaAccountDayBalance: ICasaAccountDayBalance): void {
    this.editForm.patchValue({
      id: casaAccountDayBalance.id,
      accountNo: casaAccountDayBalance.accountNo,
      date: casaAccountDayBalance.date,
      closingBalance: casaAccountDayBalance.closingBalance,
      openingBalance: casaAccountDayBalance.openingBalance,
      totalDebit: casaAccountDayBalance.totalDebit,
      totalCredit: casaAccountDayBalance.totalCredit,
      availableBalance: casaAccountDayBalance.availableBalance,
      currentBalance: casaAccountDayBalance.currentBalance,
      createdOn: casaAccountDayBalance.createdOn ? casaAccountDayBalance.createdOn.format(DATE_TIME_FORMAT) : null,
      createdBy: casaAccountDayBalance.createdBy,
      casaAccount: casaAccountDayBalance.casaAccount,
    });

    this.casaAccountsSharedCollection = this.casaAccountService.addCasaAccountToCollectionIfMissing(
      this.casaAccountsSharedCollection,
      casaAccountDayBalance.casaAccount
    );
  }

  protected loadRelationshipsOptions(): void {
    this.casaAccountService
      .query()
      .pipe(map((res: HttpResponse<ICasaAccount[]>) => res.body ?? []))
      .pipe(
        map((casaAccounts: ICasaAccount[]) =>
          this.casaAccountService.addCasaAccountToCollectionIfMissing(casaAccounts, this.editForm.get('casaAccount')!.value)
        )
      )
      .subscribe((casaAccounts: ICasaAccount[]) => (this.casaAccountsSharedCollection = casaAccounts));
  }

  protected createFromForm(): ICasaAccountDayBalance {
    return {
      ...new CasaAccountDayBalance(),
      id: this.editForm.get(['id'])!.value,
      accountNo: this.editForm.get(['accountNo'])!.value,
      date: this.editForm.get(['date'])!.value,
      closingBalance: this.editForm.get(['closingBalance'])!.value,
      openingBalance: this.editForm.get(['openingBalance'])!.value,
      totalDebit: this.editForm.get(['totalDebit'])!.value,
      totalCredit: this.editForm.get(['totalCredit'])!.value,
      availableBalance: this.editForm.get(['availableBalance'])!.value,
      currentBalance: this.editForm.get(['currentBalance'])!.value,
      createdOn: this.editForm.get(['createdOn'])!.value ? dayjs(this.editForm.get(['createdOn'])!.value, DATE_TIME_FORMAT) : undefined,
      createdBy: this.editForm.get(['createdBy'])!.value,
      casaAccount: this.editForm.get(['casaAccount'])!.value,
    };
  }
}
