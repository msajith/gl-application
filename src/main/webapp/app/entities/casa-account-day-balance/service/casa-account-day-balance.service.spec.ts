import { TestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import dayjs from 'dayjs/esm';

import { DATE_FORMAT, DATE_TIME_FORMAT } from 'app/config/input.constants';
import { ICasaAccountDayBalance, CasaAccountDayBalance } from '../casa-account-day-balance.model';

import { CasaAccountDayBalanceService } from './casa-account-day-balance.service';

describe('CasaAccountDayBalance Service', () => {
  let service: CasaAccountDayBalanceService;
  let httpMock: HttpTestingController;
  let elemDefault: ICasaAccountDayBalance;
  let expectedResult: ICasaAccountDayBalance | ICasaAccountDayBalance[] | boolean | null;
  let currentDate: dayjs.Dayjs;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
    });
    expectedResult = null;
    service = TestBed.inject(CasaAccountDayBalanceService);
    httpMock = TestBed.inject(HttpTestingController);
    currentDate = dayjs();

    elemDefault = {
      id: 0,
      accountNo: 'AAAAAAA',
      date: currentDate,
      closingBalance: 0,
      openingBalance: 0,
      totalDebit: 0,
      totalCredit: 0,
      availableBalance: 0,
      currentBalance: 0,
      createdOn: currentDate,
      createdBy: 'AAAAAAA',
    };
  });

  describe('Service methods', () => {
    it('should find an element', () => {
      const returnedFromService = Object.assign(
        {
          date: currentDate.format(DATE_FORMAT),
          createdOn: currentDate.format(DATE_TIME_FORMAT),
        },
        elemDefault
      );

      service.find(123).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'GET' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(elemDefault);
    });

    it('should create a CasaAccountDayBalance', () => {
      const returnedFromService = Object.assign(
        {
          id: 0,
          date: currentDate.format(DATE_FORMAT),
          createdOn: currentDate.format(DATE_TIME_FORMAT),
        },
        elemDefault
      );

      const expected = Object.assign(
        {
          date: currentDate,
          createdOn: currentDate,
        },
        returnedFromService
      );

      service.create(new CasaAccountDayBalance()).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'POST' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(expected);
    });

    it('should update a CasaAccountDayBalance', () => {
      const returnedFromService = Object.assign(
        {
          id: 1,
          accountNo: 'BBBBBB',
          date: currentDate.format(DATE_FORMAT),
          closingBalance: 1,
          openingBalance: 1,
          totalDebit: 1,
          totalCredit: 1,
          availableBalance: 1,
          currentBalance: 1,
          createdOn: currentDate.format(DATE_TIME_FORMAT),
          createdBy: 'BBBBBB',
        },
        elemDefault
      );

      const expected = Object.assign(
        {
          date: currentDate,
          createdOn: currentDate,
        },
        returnedFromService
      );

      service.update(expected).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'PUT' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(expected);
    });

    it('should partial update a CasaAccountDayBalance', () => {
      const patchObject = Object.assign(
        {
          date: currentDate.format(DATE_FORMAT),
          openingBalance: 1,
          totalDebit: 1,
          availableBalance: 1,
        },
        new CasaAccountDayBalance()
      );

      const returnedFromService = Object.assign(patchObject, elemDefault);

      const expected = Object.assign(
        {
          date: currentDate,
          createdOn: currentDate,
        },
        returnedFromService
      );

      service.partialUpdate(patchObject).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'PATCH' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(expected);
    });

    it('should return a list of CasaAccountDayBalance', () => {
      const returnedFromService = Object.assign(
        {
          id: 1,
          accountNo: 'BBBBBB',
          date: currentDate.format(DATE_FORMAT),
          closingBalance: 1,
          openingBalance: 1,
          totalDebit: 1,
          totalCredit: 1,
          availableBalance: 1,
          currentBalance: 1,
          createdOn: currentDate.format(DATE_TIME_FORMAT),
          createdBy: 'BBBBBB',
        },
        elemDefault
      );

      const expected = Object.assign(
        {
          date: currentDate,
          createdOn: currentDate,
        },
        returnedFromService
      );

      service.query().subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'GET' });
      req.flush([returnedFromService]);
      httpMock.verify();
      expect(expectedResult).toContainEqual(expected);
    });

    it('should delete a CasaAccountDayBalance', () => {
      service.delete(123).subscribe(resp => (expectedResult = resp.ok));

      const req = httpMock.expectOne({ method: 'DELETE' });
      req.flush({ status: 200 });
      expect(expectedResult);
    });

    describe('addCasaAccountDayBalanceToCollectionIfMissing', () => {
      it('should add a CasaAccountDayBalance to an empty array', () => {
        const casaAccountDayBalance: ICasaAccountDayBalance = { id: 123 };
        expectedResult = service.addCasaAccountDayBalanceToCollectionIfMissing([], casaAccountDayBalance);
        expect(expectedResult).toHaveLength(1);
        expect(expectedResult).toContain(casaAccountDayBalance);
      });

      it('should not add a CasaAccountDayBalance to an array that contains it', () => {
        const casaAccountDayBalance: ICasaAccountDayBalance = { id: 123 };
        const casaAccountDayBalanceCollection: ICasaAccountDayBalance[] = [
          {
            ...casaAccountDayBalance,
          },
          { id: 456 },
        ];
        expectedResult = service.addCasaAccountDayBalanceToCollectionIfMissing(casaAccountDayBalanceCollection, casaAccountDayBalance);
        expect(expectedResult).toHaveLength(2);
      });

      it("should add a CasaAccountDayBalance to an array that doesn't contain it", () => {
        const casaAccountDayBalance: ICasaAccountDayBalance = { id: 123 };
        const casaAccountDayBalanceCollection: ICasaAccountDayBalance[] = [{ id: 456 }];
        expectedResult = service.addCasaAccountDayBalanceToCollectionIfMissing(casaAccountDayBalanceCollection, casaAccountDayBalance);
        expect(expectedResult).toHaveLength(2);
        expect(expectedResult).toContain(casaAccountDayBalance);
      });

      it('should add only unique CasaAccountDayBalance to an array', () => {
        const casaAccountDayBalanceArray: ICasaAccountDayBalance[] = [{ id: 123 }, { id: 456 }, { id: 20776 }];
        const casaAccountDayBalanceCollection: ICasaAccountDayBalance[] = [{ id: 123 }];
        expectedResult = service.addCasaAccountDayBalanceToCollectionIfMissing(
          casaAccountDayBalanceCollection,
          ...casaAccountDayBalanceArray
        );
        expect(expectedResult).toHaveLength(3);
      });

      it('should accept varargs', () => {
        const casaAccountDayBalance: ICasaAccountDayBalance = { id: 123 };
        const casaAccountDayBalance2: ICasaAccountDayBalance = { id: 456 };
        expectedResult = service.addCasaAccountDayBalanceToCollectionIfMissing([], casaAccountDayBalance, casaAccountDayBalance2);
        expect(expectedResult).toHaveLength(2);
        expect(expectedResult).toContain(casaAccountDayBalance);
        expect(expectedResult).toContain(casaAccountDayBalance2);
      });

      it('should accept null and undefined values', () => {
        const casaAccountDayBalance: ICasaAccountDayBalance = { id: 123 };
        expectedResult = service.addCasaAccountDayBalanceToCollectionIfMissing([], null, casaAccountDayBalance, undefined);
        expect(expectedResult).toHaveLength(1);
        expect(expectedResult).toContain(casaAccountDayBalance);
      });

      it('should return initial array if no CasaAccountDayBalance is added', () => {
        const casaAccountDayBalanceCollection: ICasaAccountDayBalance[] = [{ id: 123 }];
        expectedResult = service.addCasaAccountDayBalanceToCollectionIfMissing(casaAccountDayBalanceCollection, undefined, null);
        expect(expectedResult).toEqual(casaAccountDayBalanceCollection);
      });
    });
  });

  afterEach(() => {
    httpMock.verify();
  });
});
