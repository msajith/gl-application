import { NgModule } from '@angular/core';
import { SharedModule } from 'app/shared/shared.module';
import { CasaAccountDayBalanceComponent } from './list/casa-account-day-balance.component';
import { CasaAccountDayBalanceDetailComponent } from './detail/casa-account-day-balance-detail.component';
import { CasaAccountDayBalanceUpdateComponent } from './update/casa-account-day-balance-update.component';
import { CasaAccountDayBalanceDeleteDialogComponent } from './delete/casa-account-day-balance-delete-dialog.component';
import { CasaAccountDayBalanceRoutingModule } from './route/casa-account-day-balance-routing.module';

@NgModule({
  imports: [SharedModule, CasaAccountDayBalanceRoutingModule],
  declarations: [
    CasaAccountDayBalanceComponent,
    CasaAccountDayBalanceDetailComponent,
    CasaAccountDayBalanceUpdateComponent,
    CasaAccountDayBalanceDeleteDialogComponent,
  ],
  entryComponents: [CasaAccountDayBalanceDeleteDialogComponent],
})
export class CasaAccountDayBalanceModule {}
