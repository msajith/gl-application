import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

import { ICasaAccount } from '../casa-account.model';
import { CasaAccountService } from '../service/casa-account.service';
import { CasaAccountDeleteDialogComponent } from '../delete/casa-account-delete-dialog.component';

@Component({
  selector: 'jhi-casa-account',
  templateUrl: './casa-account.component.html',
})
export class CasaAccountComponent implements OnInit {
  casaAccounts?: ICasaAccount[];
  isLoading = false;

  constructor(protected casaAccountService: CasaAccountService, protected modalService: NgbModal) {}

  loadAll(): void {
    this.isLoading = true;

    this.casaAccountService.query().subscribe({
      next: (res: HttpResponse<ICasaAccount[]>) => {
        this.isLoading = false;
        this.casaAccounts = res.body ?? [];
      },
      error: () => {
        this.isLoading = false;
      },
    });
  }

  ngOnInit(): void {
    this.loadAll();
  }

  trackId(index: number, item: ICasaAccount): number {
    return item.id!;
  }

  delete(casaAccount: ICasaAccount): void {
    const modalRef = this.modalService.open(CasaAccountDeleteDialogComponent, { size: 'lg', backdrop: 'static' });
    modalRef.componentInstance.casaAccount = casaAccount;
    // unsubscribe not needed because closed completes on modal close
    modalRef.closed.subscribe(reason => {
      if (reason === 'deleted') {
        this.loadAll();
      }
    });
  }
}
