import { ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpHeaders, HttpResponse } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { of } from 'rxjs';

import { CasaAccountService } from '../service/casa-account.service';

import { CasaAccountComponent } from './casa-account.component';

describe('CasaAccount Management Component', () => {
  let comp: CasaAccountComponent;
  let fixture: ComponentFixture<CasaAccountComponent>;
  let service: CasaAccountService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      declarations: [CasaAccountComponent],
    })
      .overrideTemplate(CasaAccountComponent, '')
      .compileComponents();

    fixture = TestBed.createComponent(CasaAccountComponent);
    comp = fixture.componentInstance;
    service = TestBed.inject(CasaAccountService);

    const headers = new HttpHeaders();
    jest.spyOn(service, 'query').mockReturnValue(
      of(
        new HttpResponse({
          body: [{ id: 123 }],
          headers,
        })
      )
    );
  });

  it('Should call load all on init', () => {
    // WHEN
    comp.ngOnInit();

    // THEN
    expect(service.query).toHaveBeenCalled();
    expect(comp.casaAccounts?.[0]).toEqual(expect.objectContaining({ id: 123 }));
  });
});
