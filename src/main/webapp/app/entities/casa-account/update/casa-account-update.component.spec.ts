import { ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { of, Subject, from } from 'rxjs';

import { CasaAccountService } from '../service/casa-account.service';
import { ICasaAccount, CasaAccount } from '../casa-account.model';
import { IAccountType } from 'app/entities/account-type/account-type.model';
import { AccountTypeService } from 'app/entities/account-type/service/account-type.service';
import { IAccountCategory } from 'app/entities/account-category/account-category.model';
import { AccountCategoryService } from 'app/entities/account-category/service/account-category.service';
import { ICurrency } from 'app/entities/currency/currency.model';
import { CurrencyService } from 'app/entities/currency/service/currency.service';
import { ICustomer } from 'app/entities/customer/customer.model';
import { CustomerService } from 'app/entities/customer/service/customer.service';

import { CasaAccountUpdateComponent } from './casa-account-update.component';

describe('CasaAccount Management Update Component', () => {
  let comp: CasaAccountUpdateComponent;
  let fixture: ComponentFixture<CasaAccountUpdateComponent>;
  let activatedRoute: ActivatedRoute;
  let casaAccountService: CasaAccountService;
  let accountTypeService: AccountTypeService;
  let accountCategoryService: AccountCategoryService;
  let currencyService: CurrencyService;
  let customerService: CustomerService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule, RouterTestingModule.withRoutes([])],
      declarations: [CasaAccountUpdateComponent],
      providers: [
        FormBuilder,
        {
          provide: ActivatedRoute,
          useValue: {
            params: from([{}]),
          },
        },
      ],
    })
      .overrideTemplate(CasaAccountUpdateComponent, '')
      .compileComponents();

    fixture = TestBed.createComponent(CasaAccountUpdateComponent);
    activatedRoute = TestBed.inject(ActivatedRoute);
    casaAccountService = TestBed.inject(CasaAccountService);
    accountTypeService = TestBed.inject(AccountTypeService);
    accountCategoryService = TestBed.inject(AccountCategoryService);
    currencyService = TestBed.inject(CurrencyService);
    customerService = TestBed.inject(CustomerService);

    comp = fixture.componentInstance;
  });

  describe('ngOnInit', () => {
    it('Should call AccountType query and add missing value', () => {
      const casaAccount: ICasaAccount = { id: 456 };
      const accountType: IAccountType = { id: 88572 };
      casaAccount.accountType = accountType;

      const accountTypeCollection: IAccountType[] = [{ id: 34614 }];
      jest.spyOn(accountTypeService, 'query').mockReturnValue(of(new HttpResponse({ body: accountTypeCollection })));
      const additionalAccountTypes = [accountType];
      const expectedCollection: IAccountType[] = [...additionalAccountTypes, ...accountTypeCollection];
      jest.spyOn(accountTypeService, 'addAccountTypeToCollectionIfMissing').mockReturnValue(expectedCollection);

      activatedRoute.data = of({ casaAccount });
      comp.ngOnInit();

      expect(accountTypeService.query).toHaveBeenCalled();
      expect(accountTypeService.addAccountTypeToCollectionIfMissing).toHaveBeenCalledWith(accountTypeCollection, ...additionalAccountTypes);
      expect(comp.accountTypesSharedCollection).toEqual(expectedCollection);
    });

    it('Should call AccountCategory query and add missing value', () => {
      const casaAccount: ICasaAccount = { id: 456 };
      const accountCategory: IAccountCategory = { id: 18202 };
      casaAccount.accountCategory = accountCategory;

      const accountCategoryCollection: IAccountCategory[] = [{ id: 3246 }];
      jest.spyOn(accountCategoryService, 'query').mockReturnValue(of(new HttpResponse({ body: accountCategoryCollection })));
      const additionalAccountCategories = [accountCategory];
      const expectedCollection: IAccountCategory[] = [...additionalAccountCategories, ...accountCategoryCollection];
      jest.spyOn(accountCategoryService, 'addAccountCategoryToCollectionIfMissing').mockReturnValue(expectedCollection);

      activatedRoute.data = of({ casaAccount });
      comp.ngOnInit();

      expect(accountCategoryService.query).toHaveBeenCalled();
      expect(accountCategoryService.addAccountCategoryToCollectionIfMissing).toHaveBeenCalledWith(
        accountCategoryCollection,
        ...additionalAccountCategories
      );
      expect(comp.accountCategoriesSharedCollection).toEqual(expectedCollection);
    });

    it('Should call Currency query and add missing value', () => {
      const casaAccount: ICasaAccount = { id: 456 };
      const accountCurrency: ICurrency = { id: 20788 };
      casaAccount.accountCurrency = accountCurrency;

      const currencyCollection: ICurrency[] = [{ id: 417 }];
      jest.spyOn(currencyService, 'query').mockReturnValue(of(new HttpResponse({ body: currencyCollection })));
      const additionalCurrencies = [accountCurrency];
      const expectedCollection: ICurrency[] = [...additionalCurrencies, ...currencyCollection];
      jest.spyOn(currencyService, 'addCurrencyToCollectionIfMissing').mockReturnValue(expectedCollection);

      activatedRoute.data = of({ casaAccount });
      comp.ngOnInit();

      expect(currencyService.query).toHaveBeenCalled();
      expect(currencyService.addCurrencyToCollectionIfMissing).toHaveBeenCalledWith(currencyCollection, ...additionalCurrencies);
      expect(comp.currenciesSharedCollection).toEqual(expectedCollection);
    });

    it('Should call Customer query and add missing value', () => {
      const casaAccount: ICasaAccount = { id: 456 };
      const customer: ICustomer = { id: 82112 };
      casaAccount.customer = customer;

      const customerCollection: ICustomer[] = [{ id: 45163 }];
      jest.spyOn(customerService, 'query').mockReturnValue(of(new HttpResponse({ body: customerCollection })));
      const additionalCustomers = [customer];
      const expectedCollection: ICustomer[] = [...additionalCustomers, ...customerCollection];
      jest.spyOn(customerService, 'addCustomerToCollectionIfMissing').mockReturnValue(expectedCollection);

      activatedRoute.data = of({ casaAccount });
      comp.ngOnInit();

      expect(customerService.query).toHaveBeenCalled();
      expect(customerService.addCustomerToCollectionIfMissing).toHaveBeenCalledWith(customerCollection, ...additionalCustomers);
      expect(comp.customersSharedCollection).toEqual(expectedCollection);
    });

    it('Should update editForm', () => {
      const casaAccount: ICasaAccount = { id: 456 };
      const accountType: IAccountType = { id: 77242 };
      casaAccount.accountType = accountType;
      const accountCategory: IAccountCategory = { id: 61880 };
      casaAccount.accountCategory = accountCategory;
      const accountCurrency: ICurrency = { id: 80779 };
      casaAccount.accountCurrency = accountCurrency;
      const customer: ICustomer = { id: 62821 };
      casaAccount.customer = customer;

      activatedRoute.data = of({ casaAccount });
      comp.ngOnInit();

      expect(comp.editForm.value).toEqual(expect.objectContaining(casaAccount));
      expect(comp.accountTypesSharedCollection).toContain(accountType);
      expect(comp.accountCategoriesSharedCollection).toContain(accountCategory);
      expect(comp.currenciesSharedCollection).toContain(accountCurrency);
      expect(comp.customersSharedCollection).toContain(customer);
    });
  });

  describe('save', () => {
    it('Should call update service on save for existing entity', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<CasaAccount>>();
      const casaAccount = { id: 123 };
      jest.spyOn(casaAccountService, 'update').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ casaAccount });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.next(new HttpResponse({ body: casaAccount }));
      saveSubject.complete();

      // THEN
      expect(comp.previousState).toHaveBeenCalled();
      expect(casaAccountService.update).toHaveBeenCalledWith(casaAccount);
      expect(comp.isSaving).toEqual(false);
    });

    it('Should call create service on save for new entity', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<CasaAccount>>();
      const casaAccount = new CasaAccount();
      jest.spyOn(casaAccountService, 'create').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ casaAccount });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.next(new HttpResponse({ body: casaAccount }));
      saveSubject.complete();

      // THEN
      expect(casaAccountService.create).toHaveBeenCalledWith(casaAccount);
      expect(comp.isSaving).toEqual(false);
      expect(comp.previousState).toHaveBeenCalled();
    });

    it('Should set isSaving to false on error', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<CasaAccount>>();
      const casaAccount = { id: 123 };
      jest.spyOn(casaAccountService, 'update').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ casaAccount });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.error('This is an error!');

      // THEN
      expect(casaAccountService.update).toHaveBeenCalledWith(casaAccount);
      expect(comp.isSaving).toEqual(false);
      expect(comp.previousState).not.toHaveBeenCalled();
    });
  });

  describe('Tracking relationships identifiers', () => {
    describe('trackAccountTypeById', () => {
      it('Should return tracked AccountType primary key', () => {
        const entity = { id: 123 };
        const trackResult = comp.trackAccountTypeById(0, entity);
        expect(trackResult).toEqual(entity.id);
      });
    });

    describe('trackAccountCategoryById', () => {
      it('Should return tracked AccountCategory primary key', () => {
        const entity = { id: 123 };
        const trackResult = comp.trackAccountCategoryById(0, entity);
        expect(trackResult).toEqual(entity.id);
      });
    });

    describe('trackCurrencyById', () => {
      it('Should return tracked Currency primary key', () => {
        const entity = { id: 123 };
        const trackResult = comp.trackCurrencyById(0, entity);
        expect(trackResult).toEqual(entity.id);
      });
    });

    describe('trackCustomerById', () => {
      it('Should return tracked Customer primary key', () => {
        const entity = { id: 123 };
        const trackResult = comp.trackCustomerById(0, entity);
        expect(trackResult).toEqual(entity.id);
      });
    });
  });
});
