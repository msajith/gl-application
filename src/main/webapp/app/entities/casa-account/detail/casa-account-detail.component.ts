import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { ICasaAccount } from '../casa-account.model';

@Component({
  selector: 'jhi-casa-account-detail',
  templateUrl: './casa-account-detail.component.html',
})
export class CasaAccountDetailComponent implements OnInit {
  casaAccount: ICasaAccount | null = null;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ casaAccount }) => {
      this.casaAccount = casaAccount;
    });
  }

  previousState(): void {
    window.history.back();
  }
}
