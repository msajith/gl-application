import { NgModule } from '@angular/core';
import { SharedModule } from 'app/shared/shared.module';
import { CasaAccountComponent } from './list/casa-account.component';
import { CasaAccountDetailComponent } from './detail/casa-account-detail.component';
import { CasaAccountUpdateComponent } from './update/casa-account-update.component';
import { CasaAccountDeleteDialogComponent } from './delete/casa-account-delete-dialog.component';
import { CasaAccountRoutingModule } from './route/casa-account-routing.module';

@NgModule({
  imports: [SharedModule, CasaAccountRoutingModule],
  declarations: [CasaAccountComponent, CasaAccountDetailComponent, CasaAccountUpdateComponent, CasaAccountDeleteDialogComponent],
  entryComponents: [CasaAccountDeleteDialogComponent],
})
export class CasaAccountModule {}
