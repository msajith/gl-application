import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { RemittanceTransactionDetailComponent } from './remittance-transaction-detail.component';

describe('RemittanceTransaction Management Detail Component', () => {
  let comp: RemittanceTransactionDetailComponent;
  let fixture: ComponentFixture<RemittanceTransactionDetailComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [RemittanceTransactionDetailComponent],
      providers: [
        {
          provide: ActivatedRoute,
          useValue: { data: of({ remittanceTransaction: { id: 123 } }) },
        },
      ],
    })
      .overrideTemplate(RemittanceTransactionDetailComponent, '')
      .compileComponents();
    fixture = TestBed.createComponent(RemittanceTransactionDetailComponent);
    comp = fixture.componentInstance;
  });

  describe('OnInit', () => {
    it('Should load remittanceTransaction on init', () => {
      // WHEN
      comp.ngOnInit();

      // THEN
      expect(comp.remittanceTransaction).toEqual(expect.objectContaining({ id: 123 }));
    });
  });
});
