import dayjs from 'dayjs/esm';
import { ICountry } from 'app/entities/country/country.model';
import { ICurrency } from 'app/entities/currency/currency.model';
import { IPurposeOfTxn } from 'app/entities/purpose-of-txn/purpose-of-txn.model';
import { IFundingSourceType } from 'app/entities/funding-source-type/funding-source-type.model';
import { ITransactionType } from 'app/entities/transaction-type/transaction-type.model';
import { ICustomer } from 'app/entities/customer/customer.model';
import { ICasaAccount } from 'app/entities/casa-account/casa-account.model';
import { IBeneficiary } from 'app/entities/beneficiary/beneficiary.model';
import { TxnStatus } from 'app/entities/enumerations/txn-status.model';

export interface IRemittanceTransaction {
  id?: number;
  txnRefNo?: string | null;
  accountNo?: string | null;
  txnDate?: dayjs.Dayjs | null;
  txnDateInUTC?: dayjs.Dayjs | null;
  txnDateInLocal?: dayjs.Dayjs | null;
  txnDateForSettlement?: dayjs.Dayjs | null;
  valueDate?: dayjs.Dayjs | null;
  valueDateInUTC?: dayjs.Dayjs | null;
  valueDateInLocal?: dayjs.Dayjs | null;
  valueDateForSettlement?: dayjs.Dayjs | null;
  beneficiaryAccountNo?: string | null;
  beneficiaryName?: string | null;
  beneficiarybankSwiftCode?: string | null;
  intermediaryBankSwiftCode?: string | null;
  beneficiaryBankName?: string | null;
  beneficiaryBankBranchName?: string | null;
  beneficiaryAddress1?: string | null;
  beneficiaryAddress2?: string | null;
  beneficiaryAddress3?: string | null;
  beneficiaryState?: string | null;
  beneficiaryPinCode?: string | null;
  txnAmount?: number | null;
  amountInAccountCurrency?: number | null;
  amountInLocalCurrency?: number | null;
  chargeInAccountCurrency?: number | null;
  chargeInLocalCurrency?: number | null;
  fundingSource?: string | null;
  rate?: number | null;
  instructedAmount?: number | null;
  instructedAmountCurrency?: string | null;
  merchantID?: string | null;
  merchantInfo?: string | null;
  externalRefNo?: string | null;
  txnDescription?: string | null;
  txnInternalDescription?: string | null;
  txnAddnlInfo?: string | null;
  createdOn?: dayjs.Dayjs | null;
  createdBy?: string | null;
  txnStatus?: TxnStatus | null;
  txnDebitAccountNo?: string | null;
  txnCreditAccountNo?: string | null;
  chargeDebitAccountNo?: string | null;
  chargeCreditAccountNo?: string | null;
  country?: ICountry | null;
  txnCurrency?: ICurrency | null;
  accountCurrency?: ICurrency | null;
  instructedCurrency?: ICurrency | null;
  purposeOfTxn?: IPurposeOfTxn | null;
  fundingSourceType?: IFundingSourceType | null;
  transactionType?: ITransactionType | null;
  customer?: ICustomer | null;
  casaAccount?: ICasaAccount | null;
  beneficiary?: IBeneficiary | null;
}

export class RemittanceTransaction implements IRemittanceTransaction {
  constructor(
    public id?: number,
    public txnRefNo?: string | null,
    public accountNo?: string | null,
    public txnDate?: dayjs.Dayjs | null,
    public txnDateInUTC?: dayjs.Dayjs | null,
    public txnDateInLocal?: dayjs.Dayjs | null,
    public txnDateForSettlement?: dayjs.Dayjs | null,
    public valueDate?: dayjs.Dayjs | null,
    public valueDateInUTC?: dayjs.Dayjs | null,
    public valueDateInLocal?: dayjs.Dayjs | null,
    public valueDateForSettlement?: dayjs.Dayjs | null,
    public beneficiaryAccountNo?: string | null,
    public beneficiaryName?: string | null,
    public beneficiarybankSwiftCode?: string | null,
    public intermediaryBankSwiftCode?: string | null,
    public beneficiaryBankName?: string | null,
    public beneficiaryBankBranchName?: string | null,
    public beneficiaryAddress1?: string | null,
    public beneficiaryAddress2?: string | null,
    public beneficiaryAddress3?: string | null,
    public beneficiaryState?: string | null,
    public beneficiaryPinCode?: string | null,
    public txnAmount?: number | null,
    public amountInAccountCurrency?: number | null,
    public amountInLocalCurrency?: number | null,
    public chargeInAccountCurrency?: number | null,
    public chargeInLocalCurrency?: number | null,
    public fundingSource?: string | null,
    public rate?: number | null,
    public instructedAmount?: number | null,
    public instructedAmountCurrency?: string | null,
    public merchantID?: string | null,
    public merchantInfo?: string | null,
    public externalRefNo?: string | null,
    public txnDescription?: string | null,
    public txnInternalDescription?: string | null,
    public txnAddnlInfo?: string | null,
    public createdOn?: dayjs.Dayjs | null,
    public createdBy?: string | null,
    public txnStatus?: TxnStatus | null,
    public txnDebitAccountNo?: string | null,
    public txnCreditAccountNo?: string | null,
    public chargeDebitAccountNo?: string | null,
    public chargeCreditAccountNo?: string | null,
    public country?: ICountry | null,
    public txnCurrency?: ICurrency | null,
    public accountCurrency?: ICurrency | null,
    public instructedCurrency?: ICurrency | null,
    public purposeOfTxn?: IPurposeOfTxn | null,
    public fundingSourceType?: IFundingSourceType | null,
    public transactionType?: ITransactionType | null,
    public customer?: ICustomer | null,
    public casaAccount?: ICasaAccount | null,
    public beneficiary?: IBeneficiary | null
  ) {}
}

export function getRemittanceTransactionIdentifier(remittanceTransaction: IRemittanceTransaction): number | undefined {
  return remittanceTransaction.id;
}
