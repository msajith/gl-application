import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { finalize, map } from 'rxjs/operators';

import dayjs from 'dayjs/esm';
import { DATE_TIME_FORMAT } from 'app/config/input.constants';

import { IRemittanceTransaction, RemittanceTransaction } from '../remittance-transaction.model';
import { RemittanceTransactionService } from '../service/remittance-transaction.service';
import { ICountry } from 'app/entities/country/country.model';
import { CountryService } from 'app/entities/country/service/country.service';
import { ICurrency } from 'app/entities/currency/currency.model';
import { CurrencyService } from 'app/entities/currency/service/currency.service';
import { IPurposeOfTxn } from 'app/entities/purpose-of-txn/purpose-of-txn.model';
import { PurposeOfTxnService } from 'app/entities/purpose-of-txn/service/purpose-of-txn.service';
import { IFundingSourceType } from 'app/entities/funding-source-type/funding-source-type.model';
import { FundingSourceTypeService } from 'app/entities/funding-source-type/service/funding-source-type.service';
import { ITransactionType } from 'app/entities/transaction-type/transaction-type.model';
import { TransactionTypeService } from 'app/entities/transaction-type/service/transaction-type.service';
import { ICustomer } from 'app/entities/customer/customer.model';
import { CustomerService } from 'app/entities/customer/service/customer.service';
import { ICasaAccount } from 'app/entities/casa-account/casa-account.model';
import { CasaAccountService } from 'app/entities/casa-account/service/casa-account.service';
import { IBeneficiary } from 'app/entities/beneficiary/beneficiary.model';
import { BeneficiaryService } from 'app/entities/beneficiary/service/beneficiary.service';
import { TxnStatus } from 'app/entities/enumerations/txn-status.model';

@Component({
  selector: 'jhi-remittance-transaction-update',
  templateUrl: './remittance-transaction-update.component.html',
})
export class RemittanceTransactionUpdateComponent implements OnInit {
  isSaving = false;
  txnStatusValues = Object.keys(TxnStatus);

  countriesSharedCollection: ICountry[] = [];
  currenciesSharedCollection: ICurrency[] = [];
  purposeOfTxnsSharedCollection: IPurposeOfTxn[] = [];
  fundingSourceTypesSharedCollection: IFundingSourceType[] = [];
  transactionTypesSharedCollection: ITransactionType[] = [];
  customersSharedCollection: ICustomer[] = [];
  casaAccountsSharedCollection: ICasaAccount[] = [];
  beneficiariesSharedCollection: IBeneficiary[] = [];

  editForm = this.fb.group({
    id: [],
    txnRefNo: [],
    accountNo: [],
    txnDate: [],
    txnDateInUTC: [],
    txnDateInLocal: [],
    txnDateForSettlement: [],
    valueDate: [],
    valueDateInUTC: [],
    valueDateInLocal: [],
    valueDateForSettlement: [],
    beneficiaryAccountNo: [],
    beneficiaryName: [],
    beneficiarybankSwiftCode: [],
    intermediaryBankSwiftCode: [],
    beneficiaryBankName: [],
    beneficiaryBankBranchName: [],
    beneficiaryAddress1: [],
    beneficiaryAddress2: [],
    beneficiaryAddress3: [],
    beneficiaryState: [],
    beneficiaryPinCode: [],
    txnAmount: [],
    amountInAccountCurrency: [],
    amountInLocalCurrency: [],
    chargeInAccountCurrency: [],
    chargeInLocalCurrency: [],
    fundingSource: [],
    rate: [],
    instructedAmount: [],
    instructedAmountCurrency: [],
    merchantID: [],
    merchantInfo: [],
    externalRefNo: [],
    txnDescription: [],
    txnInternalDescription: [],
    txnAddnlInfo: [],
    createdOn: [],
    createdBy: [],
    txnStatus: [],
    txnDebitAccountNo: [],
    txnCreditAccountNo: [],
    chargeDebitAccountNo: [],
    chargeCreditAccountNo: [],
    country: [],
    txnCurrency: [],
    accountCurrency: [],
    instructedCurrency: [],
    purposeOfTxn: [],
    fundingSourceType: [],
    transactionType: [],
    customer: [],
    casaAccount: [],
    beneficiary: [],
  });

  constructor(
    protected remittanceTransactionService: RemittanceTransactionService,
    protected countryService: CountryService,
    protected currencyService: CurrencyService,
    protected purposeOfTxnService: PurposeOfTxnService,
    protected fundingSourceTypeService: FundingSourceTypeService,
    protected transactionTypeService: TransactionTypeService,
    protected customerService: CustomerService,
    protected casaAccountService: CasaAccountService,
    protected beneficiaryService: BeneficiaryService,
    protected activatedRoute: ActivatedRoute,
    protected fb: FormBuilder
  ) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ remittanceTransaction }) => {
      if (remittanceTransaction.id === undefined) {
        const today = dayjs().startOf('day');
        remittanceTransaction.txnDateInUTC = today;
        remittanceTransaction.txnDateInLocal = today;
        remittanceTransaction.txnDateForSettlement = today;
        remittanceTransaction.valueDateInUTC = today;
        remittanceTransaction.valueDateInLocal = today;
        remittanceTransaction.valueDateForSettlement = today;
        remittanceTransaction.createdOn = today;
      }

      this.updateForm(remittanceTransaction);

      this.loadRelationshipsOptions();
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const remittanceTransaction = this.createFromForm();
    if (remittanceTransaction.id !== undefined) {
      this.subscribeToSaveResponse(this.remittanceTransactionService.update(remittanceTransaction));
    } else {
      this.subscribeToSaveResponse(this.remittanceTransactionService.create(remittanceTransaction));
    }
  }

  trackCountryById(index: number, item: ICountry): number {
    return item.id!;
  }

  trackCurrencyById(index: number, item: ICurrency): number {
    return item.id!;
  }

  trackPurposeOfTxnById(index: number, item: IPurposeOfTxn): number {
    return item.id!;
  }

  trackFundingSourceTypeById(index: number, item: IFundingSourceType): number {
    return item.id!;
  }

  trackTransactionTypeById(index: number, item: ITransactionType): number {
    return item.id!;
  }

  trackCustomerById(index: number, item: ICustomer): number {
    return item.id!;
  }

  trackCasaAccountById(index: number, item: ICasaAccount): number {
    return item.id!;
  }

  trackBeneficiaryById(index: number, item: IBeneficiary): number {
    return item.id!;
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IRemittanceTransaction>>): void {
    result.pipe(finalize(() => this.onSaveFinalize())).subscribe({
      next: () => this.onSaveSuccess(),
      error: () => this.onSaveError(),
    });
  }

  protected onSaveSuccess(): void {
    this.previousState();
  }

  protected onSaveError(): void {
    // Api for inheritance.
  }

  protected onSaveFinalize(): void {
    this.isSaving = false;
  }

  protected updateForm(remittanceTransaction: IRemittanceTransaction): void {
    this.editForm.patchValue({
      id: remittanceTransaction.id,
      txnRefNo: remittanceTransaction.txnRefNo,
      accountNo: remittanceTransaction.accountNo,
      txnDate: remittanceTransaction.txnDate,
      txnDateInUTC: remittanceTransaction.txnDateInUTC ? remittanceTransaction.txnDateInUTC.format(DATE_TIME_FORMAT) : null,
      txnDateInLocal: remittanceTransaction.txnDateInLocal ? remittanceTransaction.txnDateInLocal.format(DATE_TIME_FORMAT) : null,
      txnDateForSettlement: remittanceTransaction.txnDateForSettlement
        ? remittanceTransaction.txnDateForSettlement.format(DATE_TIME_FORMAT)
        : null,
      valueDate: remittanceTransaction.valueDate,
      valueDateInUTC: remittanceTransaction.valueDateInUTC ? remittanceTransaction.valueDateInUTC.format(DATE_TIME_FORMAT) : null,
      valueDateInLocal: remittanceTransaction.valueDateInLocal ? remittanceTransaction.valueDateInLocal.format(DATE_TIME_FORMAT) : null,
      valueDateForSettlement: remittanceTransaction.valueDateForSettlement
        ? remittanceTransaction.valueDateForSettlement.format(DATE_TIME_FORMAT)
        : null,
      beneficiaryAccountNo: remittanceTransaction.beneficiaryAccountNo,
      beneficiaryName: remittanceTransaction.beneficiaryName,
      beneficiarybankSwiftCode: remittanceTransaction.beneficiarybankSwiftCode,
      intermediaryBankSwiftCode: remittanceTransaction.intermediaryBankSwiftCode,
      beneficiaryBankName: remittanceTransaction.beneficiaryBankName,
      beneficiaryBankBranchName: remittanceTransaction.beneficiaryBankBranchName,
      beneficiaryAddress1: remittanceTransaction.beneficiaryAddress1,
      beneficiaryAddress2: remittanceTransaction.beneficiaryAddress2,
      beneficiaryAddress3: remittanceTransaction.beneficiaryAddress3,
      beneficiaryState: remittanceTransaction.beneficiaryState,
      beneficiaryPinCode: remittanceTransaction.beneficiaryPinCode,
      txnAmount: remittanceTransaction.txnAmount,
      amountInAccountCurrency: remittanceTransaction.amountInAccountCurrency,
      amountInLocalCurrency: remittanceTransaction.amountInLocalCurrency,
      chargeInAccountCurrency: remittanceTransaction.chargeInAccountCurrency,
      chargeInLocalCurrency: remittanceTransaction.chargeInLocalCurrency,
      fundingSource: remittanceTransaction.fundingSource,
      rate: remittanceTransaction.rate,
      instructedAmount: remittanceTransaction.instructedAmount,
      instructedAmountCurrency: remittanceTransaction.instructedAmountCurrency,
      merchantID: remittanceTransaction.merchantID,
      merchantInfo: remittanceTransaction.merchantInfo,
      externalRefNo: remittanceTransaction.externalRefNo,
      txnDescription: remittanceTransaction.txnDescription,
      txnInternalDescription: remittanceTransaction.txnInternalDescription,
      txnAddnlInfo: remittanceTransaction.txnAddnlInfo,
      createdOn: remittanceTransaction.createdOn ? remittanceTransaction.createdOn.format(DATE_TIME_FORMAT) : null,
      createdBy: remittanceTransaction.createdBy,
      txnStatus: remittanceTransaction.txnStatus,
      txnDebitAccountNo: remittanceTransaction.txnDebitAccountNo,
      txnCreditAccountNo: remittanceTransaction.txnCreditAccountNo,
      chargeDebitAccountNo: remittanceTransaction.chargeDebitAccountNo,
      chargeCreditAccountNo: remittanceTransaction.chargeCreditAccountNo,
      country: remittanceTransaction.country,
      txnCurrency: remittanceTransaction.txnCurrency,
      accountCurrency: remittanceTransaction.accountCurrency,
      instructedCurrency: remittanceTransaction.instructedCurrency,
      purposeOfTxn: remittanceTransaction.purposeOfTxn,
      fundingSourceType: remittanceTransaction.fundingSourceType,
      transactionType: remittanceTransaction.transactionType,
      customer: remittanceTransaction.customer,
      casaAccount: remittanceTransaction.casaAccount,
      beneficiary: remittanceTransaction.beneficiary,
    });

    this.countriesSharedCollection = this.countryService.addCountryToCollectionIfMissing(
      this.countriesSharedCollection,
      remittanceTransaction.country
    );
    this.currenciesSharedCollection = this.currencyService.addCurrencyToCollectionIfMissing(
      this.currenciesSharedCollection,
      remittanceTransaction.txnCurrency,
      remittanceTransaction.accountCurrency,
      remittanceTransaction.instructedCurrency
    );
    this.purposeOfTxnsSharedCollection = this.purposeOfTxnService.addPurposeOfTxnToCollectionIfMissing(
      this.purposeOfTxnsSharedCollection,
      remittanceTransaction.purposeOfTxn
    );
    this.fundingSourceTypesSharedCollection = this.fundingSourceTypeService.addFundingSourceTypeToCollectionIfMissing(
      this.fundingSourceTypesSharedCollection,
      remittanceTransaction.fundingSourceType
    );
    this.transactionTypesSharedCollection = this.transactionTypeService.addTransactionTypeToCollectionIfMissing(
      this.transactionTypesSharedCollection,
      remittanceTransaction.transactionType
    );
    this.customersSharedCollection = this.customerService.addCustomerToCollectionIfMissing(
      this.customersSharedCollection,
      remittanceTransaction.customer
    );
    this.casaAccountsSharedCollection = this.casaAccountService.addCasaAccountToCollectionIfMissing(
      this.casaAccountsSharedCollection,
      remittanceTransaction.casaAccount
    );
    this.beneficiariesSharedCollection = this.beneficiaryService.addBeneficiaryToCollectionIfMissing(
      this.beneficiariesSharedCollection,
      remittanceTransaction.beneficiary
    );
  }

  protected loadRelationshipsOptions(): void {
    this.countryService
      .query()
      .pipe(map((res: HttpResponse<ICountry[]>) => res.body ?? []))
      .pipe(
        map((countries: ICountry[]) => this.countryService.addCountryToCollectionIfMissing(countries, this.editForm.get('country')!.value))
      )
      .subscribe((countries: ICountry[]) => (this.countriesSharedCollection = countries));

    this.currencyService
      .query()
      .pipe(map((res: HttpResponse<ICurrency[]>) => res.body ?? []))
      .pipe(
        map((currencies: ICurrency[]) =>
          this.currencyService.addCurrencyToCollectionIfMissing(
            currencies,
            this.editForm.get('txnCurrency')!.value,
            this.editForm.get('accountCurrency')!.value,
            this.editForm.get('instructedCurrency')!.value
          )
        )
      )
      .subscribe((currencies: ICurrency[]) => (this.currenciesSharedCollection = currencies));

    this.purposeOfTxnService
      .query()
      .pipe(map((res: HttpResponse<IPurposeOfTxn[]>) => res.body ?? []))
      .pipe(
        map((purposeOfTxns: IPurposeOfTxn[]) =>
          this.purposeOfTxnService.addPurposeOfTxnToCollectionIfMissing(purposeOfTxns, this.editForm.get('purposeOfTxn')!.value)
        )
      )
      .subscribe((purposeOfTxns: IPurposeOfTxn[]) => (this.purposeOfTxnsSharedCollection = purposeOfTxns));

    this.fundingSourceTypeService
      .query()
      .pipe(map((res: HttpResponse<IFundingSourceType[]>) => res.body ?? []))
      .pipe(
        map((fundingSourceTypes: IFundingSourceType[]) =>
          this.fundingSourceTypeService.addFundingSourceTypeToCollectionIfMissing(
            fundingSourceTypes,
            this.editForm.get('fundingSourceType')!.value
          )
        )
      )
      .subscribe((fundingSourceTypes: IFundingSourceType[]) => (this.fundingSourceTypesSharedCollection = fundingSourceTypes));

    this.transactionTypeService
      .query()
      .pipe(map((res: HttpResponse<ITransactionType[]>) => res.body ?? []))
      .pipe(
        map((transactionTypes: ITransactionType[]) =>
          this.transactionTypeService.addTransactionTypeToCollectionIfMissing(transactionTypes, this.editForm.get('transactionType')!.value)
        )
      )
      .subscribe((transactionTypes: ITransactionType[]) => (this.transactionTypesSharedCollection = transactionTypes));

    this.customerService
      .query()
      .pipe(map((res: HttpResponse<ICustomer[]>) => res.body ?? []))
      .pipe(
        map((customers: ICustomer[]) =>
          this.customerService.addCustomerToCollectionIfMissing(customers, this.editForm.get('customer')!.value)
        )
      )
      .subscribe((customers: ICustomer[]) => (this.customersSharedCollection = customers));

    this.casaAccountService
      .query()
      .pipe(map((res: HttpResponse<ICasaAccount[]>) => res.body ?? []))
      .pipe(
        map((casaAccounts: ICasaAccount[]) =>
          this.casaAccountService.addCasaAccountToCollectionIfMissing(casaAccounts, this.editForm.get('casaAccount')!.value)
        )
      )
      .subscribe((casaAccounts: ICasaAccount[]) => (this.casaAccountsSharedCollection = casaAccounts));

    this.beneficiaryService
      .query()
      .pipe(map((res: HttpResponse<IBeneficiary[]>) => res.body ?? []))
      .pipe(
        map((beneficiaries: IBeneficiary[]) =>
          this.beneficiaryService.addBeneficiaryToCollectionIfMissing(beneficiaries, this.editForm.get('beneficiary')!.value)
        )
      )
      .subscribe((beneficiaries: IBeneficiary[]) => (this.beneficiariesSharedCollection = beneficiaries));
  }

  protected createFromForm(): IRemittanceTransaction {
    return {
      ...new RemittanceTransaction(),
      id: this.editForm.get(['id'])!.value,
      txnRefNo: this.editForm.get(['txnRefNo'])!.value,
      accountNo: this.editForm.get(['accountNo'])!.value,
      txnDate: this.editForm.get(['txnDate'])!.value,
      txnDateInUTC: this.editForm.get(['txnDateInUTC'])!.value
        ? dayjs(this.editForm.get(['txnDateInUTC'])!.value, DATE_TIME_FORMAT)
        : undefined,
      txnDateInLocal: this.editForm.get(['txnDateInLocal'])!.value
        ? dayjs(this.editForm.get(['txnDateInLocal'])!.value, DATE_TIME_FORMAT)
        : undefined,
      txnDateForSettlement: this.editForm.get(['txnDateForSettlement'])!.value
        ? dayjs(this.editForm.get(['txnDateForSettlement'])!.value, DATE_TIME_FORMAT)
        : undefined,
      valueDate: this.editForm.get(['valueDate'])!.value,
      valueDateInUTC: this.editForm.get(['valueDateInUTC'])!.value
        ? dayjs(this.editForm.get(['valueDateInUTC'])!.value, DATE_TIME_FORMAT)
        : undefined,
      valueDateInLocal: this.editForm.get(['valueDateInLocal'])!.value
        ? dayjs(this.editForm.get(['valueDateInLocal'])!.value, DATE_TIME_FORMAT)
        : undefined,
      valueDateForSettlement: this.editForm.get(['valueDateForSettlement'])!.value
        ? dayjs(this.editForm.get(['valueDateForSettlement'])!.value, DATE_TIME_FORMAT)
        : undefined,
      beneficiaryAccountNo: this.editForm.get(['beneficiaryAccountNo'])!.value,
      beneficiaryName: this.editForm.get(['beneficiaryName'])!.value,
      beneficiarybankSwiftCode: this.editForm.get(['beneficiarybankSwiftCode'])!.value,
      intermediaryBankSwiftCode: this.editForm.get(['intermediaryBankSwiftCode'])!.value,
      beneficiaryBankName: this.editForm.get(['beneficiaryBankName'])!.value,
      beneficiaryBankBranchName: this.editForm.get(['beneficiaryBankBranchName'])!.value,
      beneficiaryAddress1: this.editForm.get(['beneficiaryAddress1'])!.value,
      beneficiaryAddress2: this.editForm.get(['beneficiaryAddress2'])!.value,
      beneficiaryAddress3: this.editForm.get(['beneficiaryAddress3'])!.value,
      beneficiaryState: this.editForm.get(['beneficiaryState'])!.value,
      beneficiaryPinCode: this.editForm.get(['beneficiaryPinCode'])!.value,
      txnAmount: this.editForm.get(['txnAmount'])!.value,
      amountInAccountCurrency: this.editForm.get(['amountInAccountCurrency'])!.value,
      amountInLocalCurrency: this.editForm.get(['amountInLocalCurrency'])!.value,
      chargeInAccountCurrency: this.editForm.get(['chargeInAccountCurrency'])!.value,
      chargeInLocalCurrency: this.editForm.get(['chargeInLocalCurrency'])!.value,
      fundingSource: this.editForm.get(['fundingSource'])!.value,
      rate: this.editForm.get(['rate'])!.value,
      instructedAmount: this.editForm.get(['instructedAmount'])!.value,
      instructedAmountCurrency: this.editForm.get(['instructedAmountCurrency'])!.value,
      merchantID: this.editForm.get(['merchantID'])!.value,
      merchantInfo: this.editForm.get(['merchantInfo'])!.value,
      externalRefNo: this.editForm.get(['externalRefNo'])!.value,
      txnDescription: this.editForm.get(['txnDescription'])!.value,
      txnInternalDescription: this.editForm.get(['txnInternalDescription'])!.value,
      txnAddnlInfo: this.editForm.get(['txnAddnlInfo'])!.value,
      createdOn: this.editForm.get(['createdOn'])!.value ? dayjs(this.editForm.get(['createdOn'])!.value, DATE_TIME_FORMAT) : undefined,
      createdBy: this.editForm.get(['createdBy'])!.value,
      txnStatus: this.editForm.get(['txnStatus'])!.value,
      txnDebitAccountNo: this.editForm.get(['txnDebitAccountNo'])!.value,
      txnCreditAccountNo: this.editForm.get(['txnCreditAccountNo'])!.value,
      chargeDebitAccountNo: this.editForm.get(['chargeDebitAccountNo'])!.value,
      chargeCreditAccountNo: this.editForm.get(['chargeCreditAccountNo'])!.value,
      country: this.editForm.get(['country'])!.value,
      txnCurrency: this.editForm.get(['txnCurrency'])!.value,
      accountCurrency: this.editForm.get(['accountCurrency'])!.value,
      instructedCurrency: this.editForm.get(['instructedCurrency'])!.value,
      purposeOfTxn: this.editForm.get(['purposeOfTxn'])!.value,
      fundingSourceType: this.editForm.get(['fundingSourceType'])!.value,
      transactionType: this.editForm.get(['transactionType'])!.value,
      customer: this.editForm.get(['customer'])!.value,
      casaAccount: this.editForm.get(['casaAccount'])!.value,
      beneficiary: this.editForm.get(['beneficiary'])!.value,
    };
  }
}
