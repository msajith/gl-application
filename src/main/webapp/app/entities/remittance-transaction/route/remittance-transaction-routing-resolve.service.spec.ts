import { TestBed } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { ActivatedRouteSnapshot, ActivatedRoute, Router, convertToParamMap } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { of } from 'rxjs';

import { IRemittanceTransaction, RemittanceTransaction } from '../remittance-transaction.model';
import { RemittanceTransactionService } from '../service/remittance-transaction.service';

import { RemittanceTransactionRoutingResolveService } from './remittance-transaction-routing-resolve.service';

describe('RemittanceTransaction routing resolve service', () => {
  let mockRouter: Router;
  let mockActivatedRouteSnapshot: ActivatedRouteSnapshot;
  let routingResolveService: RemittanceTransactionRoutingResolveService;
  let service: RemittanceTransactionService;
  let resultRemittanceTransaction: IRemittanceTransaction | undefined;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule, RouterTestingModule.withRoutes([])],
      providers: [
        {
          provide: ActivatedRoute,
          useValue: {
            snapshot: {
              paramMap: convertToParamMap({}),
            },
          },
        },
      ],
    });
    mockRouter = TestBed.inject(Router);
    jest.spyOn(mockRouter, 'navigate').mockImplementation(() => Promise.resolve(true));
    mockActivatedRouteSnapshot = TestBed.inject(ActivatedRoute).snapshot;
    routingResolveService = TestBed.inject(RemittanceTransactionRoutingResolveService);
    service = TestBed.inject(RemittanceTransactionService);
    resultRemittanceTransaction = undefined;
  });

  describe('resolve', () => {
    it('should return IRemittanceTransaction returned by find', () => {
      // GIVEN
      service.find = jest.fn(id => of(new HttpResponse({ body: { id } })));
      mockActivatedRouteSnapshot.params = { id: 123 };

      // WHEN
      routingResolveService.resolve(mockActivatedRouteSnapshot).subscribe(result => {
        resultRemittanceTransaction = result;
      });

      // THEN
      expect(service.find).toBeCalledWith(123);
      expect(resultRemittanceTransaction).toEqual({ id: 123 });
    });

    it('should return new IRemittanceTransaction if id is not provided', () => {
      // GIVEN
      service.find = jest.fn();
      mockActivatedRouteSnapshot.params = {};

      // WHEN
      routingResolveService.resolve(mockActivatedRouteSnapshot).subscribe(result => {
        resultRemittanceTransaction = result;
      });

      // THEN
      expect(service.find).not.toBeCalled();
      expect(resultRemittanceTransaction).toEqual(new RemittanceTransaction());
    });

    it('should route to 404 page if data not found in server', () => {
      // GIVEN
      jest.spyOn(service, 'find').mockReturnValue(of(new HttpResponse({ body: null as unknown as RemittanceTransaction })));
      mockActivatedRouteSnapshot.params = { id: 123 };

      // WHEN
      routingResolveService.resolve(mockActivatedRouteSnapshot).subscribe(result => {
        resultRemittanceTransaction = result;
      });

      // THEN
      expect(service.find).toBeCalledWith(123);
      expect(resultRemittanceTransaction).toEqual(undefined);
      expect(mockRouter.navigate).toHaveBeenCalledWith(['404']);
    });
  });
});
