import { Component } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

import { IRemittanceTransaction } from '../remittance-transaction.model';
import { RemittanceTransactionService } from '../service/remittance-transaction.service';

@Component({
  templateUrl: './remittance-transaction-delete-dialog.component.html',
})
export class RemittanceTransactionDeleteDialogComponent {
  remittanceTransaction?: IRemittanceTransaction;

  constructor(protected remittanceTransactionService: RemittanceTransactionService, protected activeModal: NgbActiveModal) {}

  cancel(): void {
    this.activeModal.dismiss();
  }

  confirmDelete(id: number): void {
    this.remittanceTransactionService.delete(id).subscribe(() => {
      this.activeModal.close('deleted');
    });
  }
}
