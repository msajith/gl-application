import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

import { IAppConfig } from '../app-config.model';
import { AppConfigService } from '../service/app-config.service';
import { AppConfigDeleteDialogComponent } from '../delete/app-config-delete-dialog.component';

@Component({
  selector: 'jhi-app-config',
  templateUrl: './app-config.component.html',
})
export class AppConfigComponent implements OnInit {
  appConfigs?: IAppConfig[];
  isLoading = false;

  constructor(protected appConfigService: AppConfigService, protected modalService: NgbModal) {}

  loadAll(): void {
    this.isLoading = true;

    this.appConfigService.query().subscribe({
      next: (res: HttpResponse<IAppConfig[]>) => {
        this.isLoading = false;
        this.appConfigs = res.body ?? [];
      },
      error: () => {
        this.isLoading = false;
      },
    });
  }

  ngOnInit(): void {
    this.loadAll();
  }

  trackId(index: number, item: IAppConfig): number {
    return item.id!;
  }

  delete(appConfig: IAppConfig): void {
    const modalRef = this.modalService.open(AppConfigDeleteDialogComponent, { size: 'lg', backdrop: 'static' });
    modalRef.componentInstance.appConfig = appConfig;
    // unsubscribe not needed because closed completes on modal close
    modalRef.closed.subscribe(reason => {
      if (reason === 'deleted') {
        this.loadAll();
      }
    });
  }
}
