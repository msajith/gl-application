import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IAppConfig } from '../app-config.model';

@Component({
  selector: 'jhi-app-config-detail',
  templateUrl: './app-config-detail.component.html',
})
export class AppConfigDetailComponent implements OnInit {
  appConfig: IAppConfig | null = null;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ appConfig }) => {
      this.appConfig = appConfig;
    });
  }

  previousState(): void {
    window.history.back();
  }
}
