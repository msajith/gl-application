import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { AppConfigDetailComponent } from './app-config-detail.component';

describe('AppConfig Management Detail Component', () => {
  let comp: AppConfigDetailComponent;
  let fixture: ComponentFixture<AppConfigDetailComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [AppConfigDetailComponent],
      providers: [
        {
          provide: ActivatedRoute,
          useValue: { data: of({ appConfig: { id: 123 } }) },
        },
      ],
    })
      .overrideTemplate(AppConfigDetailComponent, '')
      .compileComponents();
    fixture = TestBed.createComponent(AppConfigDetailComponent);
    comp = fixture.componentInstance;
  });

  describe('OnInit', () => {
    it('Should load appConfig on init', () => {
      // WHEN
      comp.ngOnInit();

      // THEN
      expect(comp.appConfig).toEqual(expect.objectContaining({ id: 123 }));
    });
  });
});
