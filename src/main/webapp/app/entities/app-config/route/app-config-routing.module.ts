import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { UserRouteAccessService } from 'app/core/auth/user-route-access.service';
import { AppConfigComponent } from '../list/app-config.component';
import { AppConfigDetailComponent } from '../detail/app-config-detail.component';
import { AppConfigUpdateComponent } from '../update/app-config-update.component';
import { AppConfigRoutingResolveService } from './app-config-routing-resolve.service';

const appConfigRoute: Routes = [
  {
    path: '',
    component: AppConfigComponent,
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/view',
    component: AppConfigDetailComponent,
    resolve: {
      appConfig: AppConfigRoutingResolveService,
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: 'new',
    component: AppConfigUpdateComponent,
    resolve: {
      appConfig: AppConfigRoutingResolveService,
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/edit',
    component: AppConfigUpdateComponent,
    resolve: {
      appConfig: AppConfigRoutingResolveService,
    },
    canActivate: [UserRouteAccessService],
  },
];

@NgModule({
  imports: [RouterModule.forChild(appConfigRoute)],
  exports: [RouterModule],
})
export class AppConfigRoutingModule {}
