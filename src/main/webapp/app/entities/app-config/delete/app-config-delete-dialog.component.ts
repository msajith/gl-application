import { Component } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

import { IAppConfig } from '../app-config.model';
import { AppConfigService } from '../service/app-config.service';

@Component({
  templateUrl: './app-config-delete-dialog.component.html',
})
export class AppConfigDeleteDialogComponent {
  appConfig?: IAppConfig;

  constructor(protected appConfigService: AppConfigService, protected activeModal: NgbActiveModal) {}

  cancel(): void {
    this.activeModal.dismiss();
  }

  confirmDelete(id: number): void {
    this.appConfigService.delete(id).subscribe(() => {
      this.activeModal.close('deleted');
    });
  }
}
