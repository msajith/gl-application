import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { isPresent } from 'app/core/util/operators';
import { ApplicationConfigService } from 'app/core/config/application-config.service';
import { createRequestOption } from 'app/core/request/request-util';
import { IAppConfig, getAppConfigIdentifier } from '../app-config.model';

export type EntityResponseType = HttpResponse<IAppConfig>;
export type EntityArrayResponseType = HttpResponse<IAppConfig[]>;

@Injectable({ providedIn: 'root' })
export class AppConfigService {
  protected resourceUrl = this.applicationConfigService.getEndpointFor('api/app-configs');

  constructor(protected http: HttpClient, protected applicationConfigService: ApplicationConfigService) {}

  create(appConfig: IAppConfig): Observable<EntityResponseType> {
    return this.http.post<IAppConfig>(this.resourceUrl, appConfig, { observe: 'response' });
  }

  update(appConfig: IAppConfig): Observable<EntityResponseType> {
    return this.http.put<IAppConfig>(`${this.resourceUrl}/${getAppConfigIdentifier(appConfig) as number}`, appConfig, {
      observe: 'response',
    });
  }

  partialUpdate(appConfig: IAppConfig): Observable<EntityResponseType> {
    return this.http.patch<IAppConfig>(`${this.resourceUrl}/${getAppConfigIdentifier(appConfig) as number}`, appConfig, {
      observe: 'response',
    });
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http.get<IAppConfig>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<IAppConfig[]>(this.resourceUrl, { params: options, observe: 'response' });
  }

  delete(id: number): Observable<HttpResponse<{}>> {
    return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  addAppConfigToCollectionIfMissing(
    appConfigCollection: IAppConfig[],
    ...appConfigsToCheck: (IAppConfig | null | undefined)[]
  ): IAppConfig[] {
    const appConfigs: IAppConfig[] = appConfigsToCheck.filter(isPresent);
    if (appConfigs.length > 0) {
      const appConfigCollectionIdentifiers = appConfigCollection.map(appConfigItem => getAppConfigIdentifier(appConfigItem)!);
      const appConfigsToAdd = appConfigs.filter(appConfigItem => {
        const appConfigIdentifier = getAppConfigIdentifier(appConfigItem);
        if (appConfigIdentifier == null || appConfigCollectionIdentifiers.includes(appConfigIdentifier)) {
          return false;
        }
        appConfigCollectionIdentifiers.push(appConfigIdentifier);
        return true;
      });
      return [...appConfigsToAdd, ...appConfigCollection];
    }
    return appConfigCollection;
  }
}
