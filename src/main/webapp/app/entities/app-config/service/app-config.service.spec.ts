import { TestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';

import { IAppConfig, AppConfig } from '../app-config.model';

import { AppConfigService } from './app-config.service';

describe('AppConfig Service', () => {
  let service: AppConfigService;
  let httpMock: HttpTestingController;
  let elemDefault: IAppConfig;
  let expectedResult: IAppConfig | IAppConfig[] | boolean | null;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
    });
    expectedResult = null;
    service = TestBed.inject(AppConfigService);
    httpMock = TestBed.inject(HttpTestingController);

    elemDefault = {
      id: 0,
      code: 'AAAAAAA',
      subCode: 'AAAAAAA',
      value: 'AAAAAAA',
      description: 'AAAAAAA',
    };
  });

  describe('Service methods', () => {
    it('should find an element', () => {
      const returnedFromService = Object.assign({}, elemDefault);

      service.find(123).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'GET' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(elemDefault);
    });

    it('should create a AppConfig', () => {
      const returnedFromService = Object.assign(
        {
          id: 0,
        },
        elemDefault
      );

      const expected = Object.assign({}, returnedFromService);

      service.create(new AppConfig()).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'POST' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(expected);
    });

    it('should update a AppConfig', () => {
      const returnedFromService = Object.assign(
        {
          id: 1,
          code: 'BBBBBB',
          subCode: 'BBBBBB',
          value: 'BBBBBB',
          description: 'BBBBBB',
        },
        elemDefault
      );

      const expected = Object.assign({}, returnedFromService);

      service.update(expected).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'PUT' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(expected);
    });

    it('should partial update a AppConfig', () => {
      const patchObject = Object.assign(
        {
          subCode: 'BBBBBB',
          value: 'BBBBBB',
          description: 'BBBBBB',
        },
        new AppConfig()
      );

      const returnedFromService = Object.assign(patchObject, elemDefault);

      const expected = Object.assign({}, returnedFromService);

      service.partialUpdate(patchObject).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'PATCH' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(expected);
    });

    it('should return a list of AppConfig', () => {
      const returnedFromService = Object.assign(
        {
          id: 1,
          code: 'BBBBBB',
          subCode: 'BBBBBB',
          value: 'BBBBBB',
          description: 'BBBBBB',
        },
        elemDefault
      );

      const expected = Object.assign({}, returnedFromService);

      service.query().subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'GET' });
      req.flush([returnedFromService]);
      httpMock.verify();
      expect(expectedResult).toContainEqual(expected);
    });

    it('should delete a AppConfig', () => {
      service.delete(123).subscribe(resp => (expectedResult = resp.ok));

      const req = httpMock.expectOne({ method: 'DELETE' });
      req.flush({ status: 200 });
      expect(expectedResult);
    });

    describe('addAppConfigToCollectionIfMissing', () => {
      it('should add a AppConfig to an empty array', () => {
        const appConfig: IAppConfig = { id: 123 };
        expectedResult = service.addAppConfigToCollectionIfMissing([], appConfig);
        expect(expectedResult).toHaveLength(1);
        expect(expectedResult).toContain(appConfig);
      });

      it('should not add a AppConfig to an array that contains it', () => {
        const appConfig: IAppConfig = { id: 123 };
        const appConfigCollection: IAppConfig[] = [
          {
            ...appConfig,
          },
          { id: 456 },
        ];
        expectedResult = service.addAppConfigToCollectionIfMissing(appConfigCollection, appConfig);
        expect(expectedResult).toHaveLength(2);
      });

      it("should add a AppConfig to an array that doesn't contain it", () => {
        const appConfig: IAppConfig = { id: 123 };
        const appConfigCollection: IAppConfig[] = [{ id: 456 }];
        expectedResult = service.addAppConfigToCollectionIfMissing(appConfigCollection, appConfig);
        expect(expectedResult).toHaveLength(2);
        expect(expectedResult).toContain(appConfig);
      });

      it('should add only unique AppConfig to an array', () => {
        const appConfigArray: IAppConfig[] = [{ id: 123 }, { id: 456 }, { id: 113 }];
        const appConfigCollection: IAppConfig[] = [{ id: 123 }];
        expectedResult = service.addAppConfigToCollectionIfMissing(appConfigCollection, ...appConfigArray);
        expect(expectedResult).toHaveLength(3);
      });

      it('should accept varargs', () => {
        const appConfig: IAppConfig = { id: 123 };
        const appConfig2: IAppConfig = { id: 456 };
        expectedResult = service.addAppConfigToCollectionIfMissing([], appConfig, appConfig2);
        expect(expectedResult).toHaveLength(2);
        expect(expectedResult).toContain(appConfig);
        expect(expectedResult).toContain(appConfig2);
      });

      it('should accept null and undefined values', () => {
        const appConfig: IAppConfig = { id: 123 };
        expectedResult = service.addAppConfigToCollectionIfMissing([], null, appConfig, undefined);
        expect(expectedResult).toHaveLength(1);
        expect(expectedResult).toContain(appConfig);
      });

      it('should return initial array if no AppConfig is added', () => {
        const appConfigCollection: IAppConfig[] = [{ id: 123 }];
        expectedResult = service.addAppConfigToCollectionIfMissing(appConfigCollection, undefined, null);
        expect(expectedResult).toEqual(appConfigCollection);
      });
    });
  });

  afterEach(() => {
    httpMock.verify();
  });
});
