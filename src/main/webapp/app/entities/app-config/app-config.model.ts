export interface IAppConfig {
  id?: number;
  code?: string | null;
  subCode?: string | null;
  value?: string | null;
  description?: string | null;
}

export class AppConfig implements IAppConfig {
  constructor(
    public id?: number,
    public code?: string | null,
    public subCode?: string | null,
    public value?: string | null,
    public description?: string | null
  ) {}
}

export function getAppConfigIdentifier(appConfig: IAppConfig): number | undefined {
  return appConfig.id;
}
