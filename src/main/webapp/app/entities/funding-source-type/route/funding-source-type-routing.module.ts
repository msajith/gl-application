import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { UserRouteAccessService } from 'app/core/auth/user-route-access.service';
import { FundingSourceTypeComponent } from '../list/funding-source-type.component';
import { FundingSourceTypeDetailComponent } from '../detail/funding-source-type-detail.component';
import { FundingSourceTypeUpdateComponent } from '../update/funding-source-type-update.component';
import { FundingSourceTypeRoutingResolveService } from './funding-source-type-routing-resolve.service';

const fundingSourceTypeRoute: Routes = [
  {
    path: '',
    component: FundingSourceTypeComponent,
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/view',
    component: FundingSourceTypeDetailComponent,
    resolve: {
      fundingSourceType: FundingSourceTypeRoutingResolveService,
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: 'new',
    component: FundingSourceTypeUpdateComponent,
    resolve: {
      fundingSourceType: FundingSourceTypeRoutingResolveService,
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/edit',
    component: FundingSourceTypeUpdateComponent,
    resolve: {
      fundingSourceType: FundingSourceTypeRoutingResolveService,
    },
    canActivate: [UserRouteAccessService],
  },
];

@NgModule({
  imports: [RouterModule.forChild(fundingSourceTypeRoute)],
  exports: [RouterModule],
})
export class FundingSourceTypeRoutingModule {}
