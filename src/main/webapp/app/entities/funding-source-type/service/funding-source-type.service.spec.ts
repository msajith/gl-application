import { TestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';

import { IFundingSourceType, FundingSourceType } from '../funding-source-type.model';

import { FundingSourceTypeService } from './funding-source-type.service';

describe('FundingSourceType Service', () => {
  let service: FundingSourceTypeService;
  let httpMock: HttpTestingController;
  let elemDefault: IFundingSourceType;
  let expectedResult: IFundingSourceType | IFundingSourceType[] | boolean | null;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
    });
    expectedResult = null;
    service = TestBed.inject(FundingSourceTypeService);
    httpMock = TestBed.inject(HttpTestingController);

    elemDefault = {
      id: 0,
      name: 'AAAAAAA',
      code: 'AAAAAAA',
    };
  });

  describe('Service methods', () => {
    it('should find an element', () => {
      const returnedFromService = Object.assign({}, elemDefault);

      service.find(123).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'GET' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(elemDefault);
    });

    it('should create a FundingSourceType', () => {
      const returnedFromService = Object.assign(
        {
          id: 0,
        },
        elemDefault
      );

      const expected = Object.assign({}, returnedFromService);

      service.create(new FundingSourceType()).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'POST' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(expected);
    });

    it('should update a FundingSourceType', () => {
      const returnedFromService = Object.assign(
        {
          id: 1,
          name: 'BBBBBB',
          code: 'BBBBBB',
        },
        elemDefault
      );

      const expected = Object.assign({}, returnedFromService);

      service.update(expected).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'PUT' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(expected);
    });

    it('should partial update a FundingSourceType', () => {
      const patchObject = Object.assign(
        {
          code: 'BBBBBB',
        },
        new FundingSourceType()
      );

      const returnedFromService = Object.assign(patchObject, elemDefault);

      const expected = Object.assign({}, returnedFromService);

      service.partialUpdate(patchObject).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'PATCH' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(expected);
    });

    it('should return a list of FundingSourceType', () => {
      const returnedFromService = Object.assign(
        {
          id: 1,
          name: 'BBBBBB',
          code: 'BBBBBB',
        },
        elemDefault
      );

      const expected = Object.assign({}, returnedFromService);

      service.query().subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'GET' });
      req.flush([returnedFromService]);
      httpMock.verify();
      expect(expectedResult).toContainEqual(expected);
    });

    it('should delete a FundingSourceType', () => {
      service.delete(123).subscribe(resp => (expectedResult = resp.ok));

      const req = httpMock.expectOne({ method: 'DELETE' });
      req.flush({ status: 200 });
      expect(expectedResult);
    });

    describe('addFundingSourceTypeToCollectionIfMissing', () => {
      it('should add a FundingSourceType to an empty array', () => {
        const fundingSourceType: IFundingSourceType = { id: 123 };
        expectedResult = service.addFundingSourceTypeToCollectionIfMissing([], fundingSourceType);
        expect(expectedResult).toHaveLength(1);
        expect(expectedResult).toContain(fundingSourceType);
      });

      it('should not add a FundingSourceType to an array that contains it', () => {
        const fundingSourceType: IFundingSourceType = { id: 123 };
        const fundingSourceTypeCollection: IFundingSourceType[] = [
          {
            ...fundingSourceType,
          },
          { id: 456 },
        ];
        expectedResult = service.addFundingSourceTypeToCollectionIfMissing(fundingSourceTypeCollection, fundingSourceType);
        expect(expectedResult).toHaveLength(2);
      });

      it("should add a FundingSourceType to an array that doesn't contain it", () => {
        const fundingSourceType: IFundingSourceType = { id: 123 };
        const fundingSourceTypeCollection: IFundingSourceType[] = [{ id: 456 }];
        expectedResult = service.addFundingSourceTypeToCollectionIfMissing(fundingSourceTypeCollection, fundingSourceType);
        expect(expectedResult).toHaveLength(2);
        expect(expectedResult).toContain(fundingSourceType);
      });

      it('should add only unique FundingSourceType to an array', () => {
        const fundingSourceTypeArray: IFundingSourceType[] = [{ id: 123 }, { id: 456 }, { id: 39458 }];
        const fundingSourceTypeCollection: IFundingSourceType[] = [{ id: 123 }];
        expectedResult = service.addFundingSourceTypeToCollectionIfMissing(fundingSourceTypeCollection, ...fundingSourceTypeArray);
        expect(expectedResult).toHaveLength(3);
      });

      it('should accept varargs', () => {
        const fundingSourceType: IFundingSourceType = { id: 123 };
        const fundingSourceType2: IFundingSourceType = { id: 456 };
        expectedResult = service.addFundingSourceTypeToCollectionIfMissing([], fundingSourceType, fundingSourceType2);
        expect(expectedResult).toHaveLength(2);
        expect(expectedResult).toContain(fundingSourceType);
        expect(expectedResult).toContain(fundingSourceType2);
      });

      it('should accept null and undefined values', () => {
        const fundingSourceType: IFundingSourceType = { id: 123 };
        expectedResult = service.addFundingSourceTypeToCollectionIfMissing([], null, fundingSourceType, undefined);
        expect(expectedResult).toHaveLength(1);
        expect(expectedResult).toContain(fundingSourceType);
      });

      it('should return initial array if no FundingSourceType is added', () => {
        const fundingSourceTypeCollection: IFundingSourceType[] = [{ id: 123 }];
        expectedResult = service.addFundingSourceTypeToCollectionIfMissing(fundingSourceTypeCollection, undefined, null);
        expect(expectedResult).toEqual(fundingSourceTypeCollection);
      });
    });
  });

  afterEach(() => {
    httpMock.verify();
  });
});
