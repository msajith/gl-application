import { ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { of, Subject, from } from 'rxjs';

import { FundingSourceTypeService } from '../service/funding-source-type.service';
import { IFundingSourceType, FundingSourceType } from '../funding-source-type.model';

import { FundingSourceTypeUpdateComponent } from './funding-source-type-update.component';

describe('FundingSourceType Management Update Component', () => {
  let comp: FundingSourceTypeUpdateComponent;
  let fixture: ComponentFixture<FundingSourceTypeUpdateComponent>;
  let activatedRoute: ActivatedRoute;
  let fundingSourceTypeService: FundingSourceTypeService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule, RouterTestingModule.withRoutes([])],
      declarations: [FundingSourceTypeUpdateComponent],
      providers: [
        FormBuilder,
        {
          provide: ActivatedRoute,
          useValue: {
            params: from([{}]),
          },
        },
      ],
    })
      .overrideTemplate(FundingSourceTypeUpdateComponent, '')
      .compileComponents();

    fixture = TestBed.createComponent(FundingSourceTypeUpdateComponent);
    activatedRoute = TestBed.inject(ActivatedRoute);
    fundingSourceTypeService = TestBed.inject(FundingSourceTypeService);

    comp = fixture.componentInstance;
  });

  describe('ngOnInit', () => {
    it('Should update editForm', () => {
      const fundingSourceType: IFundingSourceType = { id: 456 };

      activatedRoute.data = of({ fundingSourceType });
      comp.ngOnInit();

      expect(comp.editForm.value).toEqual(expect.objectContaining(fundingSourceType));
    });
  });

  describe('save', () => {
    it('Should call update service on save for existing entity', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<FundingSourceType>>();
      const fundingSourceType = { id: 123 };
      jest.spyOn(fundingSourceTypeService, 'update').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ fundingSourceType });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.next(new HttpResponse({ body: fundingSourceType }));
      saveSubject.complete();

      // THEN
      expect(comp.previousState).toHaveBeenCalled();
      expect(fundingSourceTypeService.update).toHaveBeenCalledWith(fundingSourceType);
      expect(comp.isSaving).toEqual(false);
    });

    it('Should call create service on save for new entity', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<FundingSourceType>>();
      const fundingSourceType = new FundingSourceType();
      jest.spyOn(fundingSourceTypeService, 'create').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ fundingSourceType });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.next(new HttpResponse({ body: fundingSourceType }));
      saveSubject.complete();

      // THEN
      expect(fundingSourceTypeService.create).toHaveBeenCalledWith(fundingSourceType);
      expect(comp.isSaving).toEqual(false);
      expect(comp.previousState).toHaveBeenCalled();
    });

    it('Should set isSaving to false on error', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<FundingSourceType>>();
      const fundingSourceType = { id: 123 };
      jest.spyOn(fundingSourceTypeService, 'update').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ fundingSourceType });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.error('This is an error!');

      // THEN
      expect(fundingSourceTypeService.update).toHaveBeenCalledWith(fundingSourceType);
      expect(comp.isSaving).toEqual(false);
      expect(comp.previousState).not.toHaveBeenCalled();
    });
  });
});
