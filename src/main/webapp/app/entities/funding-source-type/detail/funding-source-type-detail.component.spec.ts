import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { FundingSourceTypeDetailComponent } from './funding-source-type-detail.component';

describe('FundingSourceType Management Detail Component', () => {
  let comp: FundingSourceTypeDetailComponent;
  let fixture: ComponentFixture<FundingSourceTypeDetailComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [FundingSourceTypeDetailComponent],
      providers: [
        {
          provide: ActivatedRoute,
          useValue: { data: of({ fundingSourceType: { id: 123 } }) },
        },
      ],
    })
      .overrideTemplate(FundingSourceTypeDetailComponent, '')
      .compileComponents();
    fixture = TestBed.createComponent(FundingSourceTypeDetailComponent);
    comp = fixture.componentInstance;
  });

  describe('OnInit', () => {
    it('Should load fundingSourceType on init', () => {
      // WHEN
      comp.ngOnInit();

      // THEN
      expect(comp.fundingSourceType).toEqual(expect.objectContaining({ id: 123 }));
    });
  });
});
