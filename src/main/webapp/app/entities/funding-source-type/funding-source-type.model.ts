export interface IFundingSourceType {
  id?: number;
  name?: string | null;
  code?: string | null;
}

export class FundingSourceType implements IFundingSourceType {
  constructor(public id?: number, public name?: string | null, public code?: string | null) {}
}

export function getFundingSourceTypeIdentifier(fundingSourceType: IFundingSourceType): number | undefined {
  return fundingSourceType.id;
}
