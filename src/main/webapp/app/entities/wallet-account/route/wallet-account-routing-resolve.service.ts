import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, Router } from '@angular/router';
import { Observable, of, EMPTY } from 'rxjs';
import { mergeMap } from 'rxjs/operators';

import { IWalletAccount, WalletAccount } from '../wallet-account.model';
import { WalletAccountService } from '../service/wallet-account.service';

@Injectable({ providedIn: 'root' })
export class WalletAccountRoutingResolveService implements Resolve<IWalletAccount> {
  constructor(protected service: WalletAccountService, protected router: Router) {}

  resolve(route: ActivatedRouteSnapshot): Observable<IWalletAccount> | Observable<never> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        mergeMap((walletAccount: HttpResponse<WalletAccount>) => {
          if (walletAccount.body) {
            return of(walletAccount.body);
          } else {
            this.router.navigate(['404']);
            return EMPTY;
          }
        })
      );
    }
    return of(new WalletAccount());
  }
}
