import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { WalletAccountDetailComponent } from './wallet-account-detail.component';

describe('WalletAccount Management Detail Component', () => {
  let comp: WalletAccountDetailComponent;
  let fixture: ComponentFixture<WalletAccountDetailComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [WalletAccountDetailComponent],
      providers: [
        {
          provide: ActivatedRoute,
          useValue: { data: of({ walletAccount: { id: 123 } }) },
        },
      ],
    })
      .overrideTemplate(WalletAccountDetailComponent, '')
      .compileComponents();
    fixture = TestBed.createComponent(WalletAccountDetailComponent);
    comp = fixture.componentInstance;
  });

  describe('OnInit', () => {
    it('Should load walletAccount on init', () => {
      // WHEN
      comp.ngOnInit();

      // THEN
      expect(comp.walletAccount).toEqual(expect.objectContaining({ id: 123 }));
    });
  });
});
