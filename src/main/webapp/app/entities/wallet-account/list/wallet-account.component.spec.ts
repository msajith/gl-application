import { ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpHeaders, HttpResponse } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { of } from 'rxjs';

import { WalletAccountService } from '../service/wallet-account.service';

import { WalletAccountComponent } from './wallet-account.component';

describe('WalletAccount Management Component', () => {
  let comp: WalletAccountComponent;
  let fixture: ComponentFixture<WalletAccountComponent>;
  let service: WalletAccountService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      declarations: [WalletAccountComponent],
    })
      .overrideTemplate(WalletAccountComponent, '')
      .compileComponents();

    fixture = TestBed.createComponent(WalletAccountComponent);
    comp = fixture.componentInstance;
    service = TestBed.inject(WalletAccountService);

    const headers = new HttpHeaders();
    jest.spyOn(service, 'query').mockReturnValue(
      of(
        new HttpResponse({
          body: [{ id: 123 }],
          headers,
        })
      )
    );
  });

  it('Should call load all on init', () => {
    // WHEN
    comp.ngOnInit();

    // THEN
    expect(service.query).toHaveBeenCalled();
    expect(comp.walletAccounts?.[0]).toEqual(expect.objectContaining({ id: 123 }));
  });
});
