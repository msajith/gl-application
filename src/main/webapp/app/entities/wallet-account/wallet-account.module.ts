import { NgModule } from '@angular/core';
import { SharedModule } from 'app/shared/shared.module';
import { WalletAccountComponent } from './list/wallet-account.component';
import { WalletAccountDetailComponent } from './detail/wallet-account-detail.component';
import { WalletAccountUpdateComponent } from './update/wallet-account-update.component';
import { WalletAccountDeleteDialogComponent } from './delete/wallet-account-delete-dialog.component';
import { WalletAccountRoutingModule } from './route/wallet-account-routing.module';

@NgModule({
  imports: [SharedModule, WalletAccountRoutingModule],
  declarations: [WalletAccountComponent, WalletAccountDetailComponent, WalletAccountUpdateComponent, WalletAccountDeleteDialogComponent],
  entryComponents: [WalletAccountDeleteDialogComponent],
})
export class WalletAccountModule {}
