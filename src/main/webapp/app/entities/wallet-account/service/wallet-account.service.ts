import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import dayjs from 'dayjs/esm';

import { isPresent } from 'app/core/util/operators';
import { DATE_FORMAT } from 'app/config/input.constants';
import { ApplicationConfigService } from 'app/core/config/application-config.service';
import { createRequestOption } from 'app/core/request/request-util';
import { IWalletAccount, getWalletAccountIdentifier } from '../wallet-account.model';

export type EntityResponseType = HttpResponse<IWalletAccount>;
export type EntityArrayResponseType = HttpResponse<IWalletAccount[]>;

@Injectable({ providedIn: 'root' })
export class WalletAccountService {
  protected resourceUrl = this.applicationConfigService.getEndpointFor('api/wallet-accounts');

  constructor(protected http: HttpClient, protected applicationConfigService: ApplicationConfigService) {}

  create(walletAccount: IWalletAccount): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(walletAccount);
    return this.http
      .post<IWalletAccount>(this.resourceUrl, copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  update(walletAccount: IWalletAccount): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(walletAccount);
    return this.http
      .put<IWalletAccount>(`${this.resourceUrl}/${getWalletAccountIdentifier(walletAccount) as number}`, copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  partialUpdate(walletAccount: IWalletAccount): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(walletAccount);
    return this.http
      .patch<IWalletAccount>(`${this.resourceUrl}/${getWalletAccountIdentifier(walletAccount) as number}`, copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http
      .get<IWalletAccount>(`${this.resourceUrl}/${id}`, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http
      .get<IWalletAccount[]>(this.resourceUrl, { params: options, observe: 'response' })
      .pipe(map((res: EntityArrayResponseType) => this.convertDateArrayFromServer(res)));
  }

  delete(id: number): Observable<HttpResponse<{}>> {
    return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  addWalletAccountToCollectionIfMissing(
    walletAccountCollection: IWalletAccount[],
    ...walletAccountsToCheck: (IWalletAccount | null | undefined)[]
  ): IWalletAccount[] {
    const walletAccounts: IWalletAccount[] = walletAccountsToCheck.filter(isPresent);
    if (walletAccounts.length > 0) {
      const walletAccountCollectionIdentifiers = walletAccountCollection.map(
        walletAccountItem => getWalletAccountIdentifier(walletAccountItem)!
      );
      const walletAccountsToAdd = walletAccounts.filter(walletAccountItem => {
        const walletAccountIdentifier = getWalletAccountIdentifier(walletAccountItem);
        if (walletAccountIdentifier == null || walletAccountCollectionIdentifiers.includes(walletAccountIdentifier)) {
          return false;
        }
        walletAccountCollectionIdentifiers.push(walletAccountIdentifier);
        return true;
      });
      return [...walletAccountsToAdd, ...walletAccountCollection];
    }
    return walletAccountCollection;
  }

  protected convertDateFromClient(walletAccount: IWalletAccount): IWalletAccount {
    return Object.assign({}, walletAccount, {
      date: walletAccount.date?.isValid() ? walletAccount.date.format(DATE_FORMAT) : undefined,
      createdOn: walletAccount.createdOn?.isValid() ? walletAccount.createdOn.toJSON() : undefined,
    });
  }

  protected convertDateFromServer(res: EntityResponseType): EntityResponseType {
    if (res.body) {
      res.body.date = res.body.date ? dayjs(res.body.date) : undefined;
      res.body.createdOn = res.body.createdOn ? dayjs(res.body.createdOn) : undefined;
    }
    return res;
  }

  protected convertDateArrayFromServer(res: EntityArrayResponseType): EntityArrayResponseType {
    if (res.body) {
      res.body.forEach((walletAccount: IWalletAccount) => {
        walletAccount.date = walletAccount.date ? dayjs(walletAccount.date) : undefined;
        walletAccount.createdOn = walletAccount.createdOn ? dayjs(walletAccount.createdOn) : undefined;
      });
    }
    return res;
  }
}
