import { ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { of, Subject, from } from 'rxjs';

import { WalletAccountService } from '../service/wallet-account.service';
import { IWalletAccount, WalletAccount } from '../wallet-account.model';
import { IAccountType } from 'app/entities/account-type/account-type.model';
import { AccountTypeService } from 'app/entities/account-type/service/account-type.service';
import { IAccountCategory } from 'app/entities/account-category/account-category.model';
import { AccountCategoryService } from 'app/entities/account-category/service/account-category.service';
import { ICurrency } from 'app/entities/currency/currency.model';
import { CurrencyService } from 'app/entities/currency/service/currency.service';
import { ICustomer } from 'app/entities/customer/customer.model';
import { CustomerService } from 'app/entities/customer/service/customer.service';

import { WalletAccountUpdateComponent } from './wallet-account-update.component';

describe('WalletAccount Management Update Component', () => {
  let comp: WalletAccountUpdateComponent;
  let fixture: ComponentFixture<WalletAccountUpdateComponent>;
  let activatedRoute: ActivatedRoute;
  let walletAccountService: WalletAccountService;
  let accountTypeService: AccountTypeService;
  let accountCategoryService: AccountCategoryService;
  let currencyService: CurrencyService;
  let customerService: CustomerService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule, RouterTestingModule.withRoutes([])],
      declarations: [WalletAccountUpdateComponent],
      providers: [
        FormBuilder,
        {
          provide: ActivatedRoute,
          useValue: {
            params: from([{}]),
          },
        },
      ],
    })
      .overrideTemplate(WalletAccountUpdateComponent, '')
      .compileComponents();

    fixture = TestBed.createComponent(WalletAccountUpdateComponent);
    activatedRoute = TestBed.inject(ActivatedRoute);
    walletAccountService = TestBed.inject(WalletAccountService);
    accountTypeService = TestBed.inject(AccountTypeService);
    accountCategoryService = TestBed.inject(AccountCategoryService);
    currencyService = TestBed.inject(CurrencyService);
    customerService = TestBed.inject(CustomerService);

    comp = fixture.componentInstance;
  });

  describe('ngOnInit', () => {
    it('Should call AccountType query and add missing value', () => {
      const walletAccount: IWalletAccount = { id: 456 };
      const accountType: IAccountType = { id: 23677 };
      walletAccount.accountType = accountType;

      const accountTypeCollection: IAccountType[] = [{ id: 5574 }];
      jest.spyOn(accountTypeService, 'query').mockReturnValue(of(new HttpResponse({ body: accountTypeCollection })));
      const additionalAccountTypes = [accountType];
      const expectedCollection: IAccountType[] = [...additionalAccountTypes, ...accountTypeCollection];
      jest.spyOn(accountTypeService, 'addAccountTypeToCollectionIfMissing').mockReturnValue(expectedCollection);

      activatedRoute.data = of({ walletAccount });
      comp.ngOnInit();

      expect(accountTypeService.query).toHaveBeenCalled();
      expect(accountTypeService.addAccountTypeToCollectionIfMissing).toHaveBeenCalledWith(accountTypeCollection, ...additionalAccountTypes);
      expect(comp.accountTypesSharedCollection).toEqual(expectedCollection);
    });

    it('Should call AccountCategory query and add missing value', () => {
      const walletAccount: IWalletAccount = { id: 456 };
      const accountCategory: IAccountCategory = { id: 14766 };
      walletAccount.accountCategory = accountCategory;

      const accountCategoryCollection: IAccountCategory[] = [{ id: 15291 }];
      jest.spyOn(accountCategoryService, 'query').mockReturnValue(of(new HttpResponse({ body: accountCategoryCollection })));
      const additionalAccountCategories = [accountCategory];
      const expectedCollection: IAccountCategory[] = [...additionalAccountCategories, ...accountCategoryCollection];
      jest.spyOn(accountCategoryService, 'addAccountCategoryToCollectionIfMissing').mockReturnValue(expectedCollection);

      activatedRoute.data = of({ walletAccount });
      comp.ngOnInit();

      expect(accountCategoryService.query).toHaveBeenCalled();
      expect(accountCategoryService.addAccountCategoryToCollectionIfMissing).toHaveBeenCalledWith(
        accountCategoryCollection,
        ...additionalAccountCategories
      );
      expect(comp.accountCategoriesSharedCollection).toEqual(expectedCollection);
    });

    it('Should call Currency query and add missing value', () => {
      const walletAccount: IWalletAccount = { id: 456 };
      const accountCurrency: ICurrency = { id: 29689 };
      walletAccount.accountCurrency = accountCurrency;

      const currencyCollection: ICurrency[] = [{ id: 23327 }];
      jest.spyOn(currencyService, 'query').mockReturnValue(of(new HttpResponse({ body: currencyCollection })));
      const additionalCurrencies = [accountCurrency];
      const expectedCollection: ICurrency[] = [...additionalCurrencies, ...currencyCollection];
      jest.spyOn(currencyService, 'addCurrencyToCollectionIfMissing').mockReturnValue(expectedCollection);

      activatedRoute.data = of({ walletAccount });
      comp.ngOnInit();

      expect(currencyService.query).toHaveBeenCalled();
      expect(currencyService.addCurrencyToCollectionIfMissing).toHaveBeenCalledWith(currencyCollection, ...additionalCurrencies);
      expect(comp.currenciesSharedCollection).toEqual(expectedCollection);
    });

    it('Should call Customer query and add missing value', () => {
      const walletAccount: IWalletAccount = { id: 456 };
      const customer: ICustomer = { id: 74990 };
      walletAccount.customer = customer;

      const customerCollection: ICustomer[] = [{ id: 354 }];
      jest.spyOn(customerService, 'query').mockReturnValue(of(new HttpResponse({ body: customerCollection })));
      const additionalCustomers = [customer];
      const expectedCollection: ICustomer[] = [...additionalCustomers, ...customerCollection];
      jest.spyOn(customerService, 'addCustomerToCollectionIfMissing').mockReturnValue(expectedCollection);

      activatedRoute.data = of({ walletAccount });
      comp.ngOnInit();

      expect(customerService.query).toHaveBeenCalled();
      expect(customerService.addCustomerToCollectionIfMissing).toHaveBeenCalledWith(customerCollection, ...additionalCustomers);
      expect(comp.customersSharedCollection).toEqual(expectedCollection);
    });

    it('Should update editForm', () => {
      const walletAccount: IWalletAccount = { id: 456 };
      const accountType: IAccountType = { id: 28773 };
      walletAccount.accountType = accountType;
      const accountCategory: IAccountCategory = { id: 95943 };
      walletAccount.accountCategory = accountCategory;
      const accountCurrency: ICurrency = { id: 1413 };
      walletAccount.accountCurrency = accountCurrency;
      const customer: ICustomer = { id: 72212 };
      walletAccount.customer = customer;

      activatedRoute.data = of({ walletAccount });
      comp.ngOnInit();

      expect(comp.editForm.value).toEqual(expect.objectContaining(walletAccount));
      expect(comp.accountTypesSharedCollection).toContain(accountType);
      expect(comp.accountCategoriesSharedCollection).toContain(accountCategory);
      expect(comp.currenciesSharedCollection).toContain(accountCurrency);
      expect(comp.customersSharedCollection).toContain(customer);
    });
  });

  describe('save', () => {
    it('Should call update service on save for existing entity', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<WalletAccount>>();
      const walletAccount = { id: 123 };
      jest.spyOn(walletAccountService, 'update').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ walletAccount });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.next(new HttpResponse({ body: walletAccount }));
      saveSubject.complete();

      // THEN
      expect(comp.previousState).toHaveBeenCalled();
      expect(walletAccountService.update).toHaveBeenCalledWith(walletAccount);
      expect(comp.isSaving).toEqual(false);
    });

    it('Should call create service on save for new entity', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<WalletAccount>>();
      const walletAccount = new WalletAccount();
      jest.spyOn(walletAccountService, 'create').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ walletAccount });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.next(new HttpResponse({ body: walletAccount }));
      saveSubject.complete();

      // THEN
      expect(walletAccountService.create).toHaveBeenCalledWith(walletAccount);
      expect(comp.isSaving).toEqual(false);
      expect(comp.previousState).toHaveBeenCalled();
    });

    it('Should set isSaving to false on error', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<WalletAccount>>();
      const walletAccount = { id: 123 };
      jest.spyOn(walletAccountService, 'update').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ walletAccount });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.error('This is an error!');

      // THEN
      expect(walletAccountService.update).toHaveBeenCalledWith(walletAccount);
      expect(comp.isSaving).toEqual(false);
      expect(comp.previousState).not.toHaveBeenCalled();
    });
  });

  describe('Tracking relationships identifiers', () => {
    describe('trackAccountTypeById', () => {
      it('Should return tracked AccountType primary key', () => {
        const entity = { id: 123 };
        const trackResult = comp.trackAccountTypeById(0, entity);
        expect(trackResult).toEqual(entity.id);
      });
    });

    describe('trackAccountCategoryById', () => {
      it('Should return tracked AccountCategory primary key', () => {
        const entity = { id: 123 };
        const trackResult = comp.trackAccountCategoryById(0, entity);
        expect(trackResult).toEqual(entity.id);
      });
    });

    describe('trackCurrencyById', () => {
      it('Should return tracked Currency primary key', () => {
        const entity = { id: 123 };
        const trackResult = comp.trackCurrencyById(0, entity);
        expect(trackResult).toEqual(entity.id);
      });
    });

    describe('trackCustomerById', () => {
      it('Should return tracked Customer primary key', () => {
        const entity = { id: 123 };
        const trackResult = comp.trackCustomerById(0, entity);
        expect(trackResult).toEqual(entity.id);
      });
    });
  });
});
