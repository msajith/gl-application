import { Component } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

import { IWalletAccount } from '../wallet-account.model';
import { WalletAccountService } from '../service/wallet-account.service';

@Component({
  templateUrl: './wallet-account-delete-dialog.component.html',
})
export class WalletAccountDeleteDialogComponent {
  walletAccount?: IWalletAccount;

  constructor(protected walletAccountService: WalletAccountService, protected activeModal: NgbActiveModal) {}

  cancel(): void {
    this.activeModal.dismiss();
  }

  confirmDelete(id: number): void {
    this.walletAccountService.delete(id).subscribe(() => {
      this.activeModal.close('deleted');
    });
  }
}
