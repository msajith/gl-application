import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

import { ICharge } from '../charge.model';
import { ChargeService } from '../service/charge.service';
import { ChargeDeleteDialogComponent } from '../delete/charge-delete-dialog.component';

@Component({
  selector: 'jhi-charge',
  templateUrl: './charge.component.html',
})
export class ChargeComponent implements OnInit {
  charges?: ICharge[];
  isLoading = false;

  constructor(protected chargeService: ChargeService, protected modalService: NgbModal) {}

  loadAll(): void {
    this.isLoading = true;

    this.chargeService.query().subscribe({
      next: (res: HttpResponse<ICharge[]>) => {
        this.isLoading = false;
        this.charges = res.body ?? [];
      },
      error: () => {
        this.isLoading = false;
      },
    });
  }

  ngOnInit(): void {
    this.loadAll();
  }

  trackId(index: number, item: ICharge): number {
    return item.id!;
  }

  delete(charge: ICharge): void {
    const modalRef = this.modalService.open(ChargeDeleteDialogComponent, { size: 'lg', backdrop: 'static' });
    modalRef.componentInstance.charge = charge;
    // unsubscribe not needed because closed completes on modal close
    modalRef.closed.subscribe(reason => {
      if (reason === 'deleted') {
        this.loadAll();
      }
    });
  }
}
