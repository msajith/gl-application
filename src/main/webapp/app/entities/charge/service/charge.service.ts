import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import dayjs from 'dayjs/esm';

import { isPresent } from 'app/core/util/operators';
import { DATE_FORMAT } from 'app/config/input.constants';
import { ApplicationConfigService } from 'app/core/config/application-config.service';
import { createRequestOption } from 'app/core/request/request-util';
import { ICharge, getChargeIdentifier } from '../charge.model';

export type EntityResponseType = HttpResponse<ICharge>;
export type EntityArrayResponseType = HttpResponse<ICharge[]>;

@Injectable({ providedIn: 'root' })
export class ChargeService {
  protected resourceUrl = this.applicationConfigService.getEndpointFor('api/charges');

  constructor(protected http: HttpClient, protected applicationConfigService: ApplicationConfigService) {}

  create(charge: ICharge): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(charge);
    return this.http
      .post<ICharge>(this.resourceUrl, copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  update(charge: ICharge): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(charge);
    return this.http
      .put<ICharge>(`${this.resourceUrl}/${getChargeIdentifier(charge) as number}`, copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  partialUpdate(charge: ICharge): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(charge);
    return this.http
      .patch<ICharge>(`${this.resourceUrl}/${getChargeIdentifier(charge) as number}`, copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http
      .get<ICharge>(`${this.resourceUrl}/${id}`, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http
      .get<ICharge[]>(this.resourceUrl, { params: options, observe: 'response' })
      .pipe(map((res: EntityArrayResponseType) => this.convertDateArrayFromServer(res)));
  }

  delete(id: number): Observable<HttpResponse<{}>> {
    return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  addChargeToCollectionIfMissing(chargeCollection: ICharge[], ...chargesToCheck: (ICharge | null | undefined)[]): ICharge[] {
    const charges: ICharge[] = chargesToCheck.filter(isPresent);
    if (charges.length > 0) {
      const chargeCollectionIdentifiers = chargeCollection.map(chargeItem => getChargeIdentifier(chargeItem)!);
      const chargesToAdd = charges.filter(chargeItem => {
        const chargeIdentifier = getChargeIdentifier(chargeItem);
        if (chargeIdentifier == null || chargeCollectionIdentifiers.includes(chargeIdentifier)) {
          return false;
        }
        chargeCollectionIdentifiers.push(chargeIdentifier);
        return true;
      });
      return [...chargesToAdd, ...chargeCollection];
    }
    return chargeCollection;
  }

  protected convertDateFromClient(charge: ICharge): ICharge {
    return Object.assign({}, charge, {
      validfrom: charge.validfrom?.isValid() ? charge.validfrom.format(DATE_FORMAT) : undefined,
      validTo: charge.validTo?.isValid() ? charge.validTo.format(DATE_FORMAT) : undefined,
    });
  }

  protected convertDateFromServer(res: EntityResponseType): EntityResponseType {
    if (res.body) {
      res.body.validfrom = res.body.validfrom ? dayjs(res.body.validfrom) : undefined;
      res.body.validTo = res.body.validTo ? dayjs(res.body.validTo) : undefined;
    }
    return res;
  }

  protected convertDateArrayFromServer(res: EntityArrayResponseType): EntityArrayResponseType {
    if (res.body) {
      res.body.forEach((charge: ICharge) => {
        charge.validfrom = charge.validfrom ? dayjs(charge.validfrom) : undefined;
        charge.validTo = charge.validTo ? dayjs(charge.validTo) : undefined;
      });
    }
    return res;
  }
}
