import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { finalize, map } from 'rxjs/operators';

import { ICharge, Charge } from '../charge.model';
import { ChargeService } from '../service/charge.service';
import { IChargeType } from 'app/entities/charge-type/charge-type.model';
import { ChargeTypeService } from 'app/entities/charge-type/service/charge-type.service';
import { ITransactionType } from 'app/entities/transaction-type/transaction-type.model';
import { TransactionTypeService } from 'app/entities/transaction-type/service/transaction-type.service';
import { ICountry } from 'app/entities/country/country.model';
import { CountryService } from 'app/entities/country/service/country.service';
import { ICurrency } from 'app/entities/currency/currency.model';
import { CurrencyService } from 'app/entities/currency/service/currency.service';
import { ICasaAccount } from 'app/entities/casa-account/casa-account.model';
import { CasaAccountService } from 'app/entities/casa-account/service/casa-account.service';
import { ICustomerSegment } from 'app/entities/customer-segment/customer-segment.model';
import { CustomerSegmentService } from 'app/entities/customer-segment/service/customer-segment.service';
import { RecordStatus } from 'app/entities/enumerations/record-status.model';

@Component({
  selector: 'jhi-charge-update',
  templateUrl: './charge-update.component.html',
})
export class ChargeUpdateComponent implements OnInit {
  isSaving = false;
  recordStatusValues = Object.keys(RecordStatus);

  chargeTypesSharedCollection: IChargeType[] = [];
  transactionTypesSharedCollection: ITransactionType[] = [];
  countriesSharedCollection: ICountry[] = [];
  currenciesSharedCollection: ICurrency[] = [];
  casaAccountsSharedCollection: ICasaAccount[] = [];
  customerSegmentsSharedCollection: ICustomerSegment[] = [];

  editForm = this.fb.group({
    id: [],
    chargeAmountLocalCcy: [],
    chargeAmountTxnCcy: [],
    amountSlab: [],
    amountSlabValue: [],
    amountSlabOperator: [],
    amountSlabCurrency: [],
    recordStatus: [],
    validfrom: [],
    validTo: [],
    chargeType: [],
    transactionType: [],
    txnCountry: [],
    beneficiaryCountry: [],
    baseCurrency: [],
    txnCurrency: [],
    chargeAccount: [],
    customerSegment: [],
  });

  constructor(
    protected chargeService: ChargeService,
    protected chargeTypeService: ChargeTypeService,
    protected transactionTypeService: TransactionTypeService,
    protected countryService: CountryService,
    protected currencyService: CurrencyService,
    protected casaAccountService: CasaAccountService,
    protected customerSegmentService: CustomerSegmentService,
    protected activatedRoute: ActivatedRoute,
    protected fb: FormBuilder
  ) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ charge }) => {
      this.updateForm(charge);

      this.loadRelationshipsOptions();
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const charge = this.createFromForm();
    if (charge.id !== undefined) {
      this.subscribeToSaveResponse(this.chargeService.update(charge));
    } else {
      this.subscribeToSaveResponse(this.chargeService.create(charge));
    }
  }

  trackChargeTypeById(index: number, item: IChargeType): number {
    return item.id!;
  }

  trackTransactionTypeById(index: number, item: ITransactionType): number {
    return item.id!;
  }

  trackCountryById(index: number, item: ICountry): number {
    return item.id!;
  }

  trackCurrencyById(index: number, item: ICurrency): number {
    return item.id!;
  }

  trackCasaAccountById(index: number, item: ICasaAccount): number {
    return item.id!;
  }

  trackCustomerSegmentById(index: number, item: ICustomerSegment): number {
    return item.id!;
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<ICharge>>): void {
    result.pipe(finalize(() => this.onSaveFinalize())).subscribe({
      next: () => this.onSaveSuccess(),
      error: () => this.onSaveError(),
    });
  }

  protected onSaveSuccess(): void {
    this.previousState();
  }

  protected onSaveError(): void {
    // Api for inheritance.
  }

  protected onSaveFinalize(): void {
    this.isSaving = false;
  }

  protected updateForm(charge: ICharge): void {
    this.editForm.patchValue({
      id: charge.id,
      chargeAmountLocalCcy: charge.chargeAmountLocalCcy,
      chargeAmountTxnCcy: charge.chargeAmountTxnCcy,
      amountSlab: charge.amountSlab,
      amountSlabValue: charge.amountSlabValue,
      amountSlabOperator: charge.amountSlabOperator,
      amountSlabCurrency: charge.amountSlabCurrency,
      recordStatus: charge.recordStatus,
      validfrom: charge.validfrom,
      validTo: charge.validTo,
      chargeType: charge.chargeType,
      transactionType: charge.transactionType,
      txnCountry: charge.txnCountry,
      beneficiaryCountry: charge.beneficiaryCountry,
      baseCurrency: charge.baseCurrency,
      txnCurrency: charge.txnCurrency,
      chargeAccount: charge.chargeAccount,
      customerSegment: charge.customerSegment,
    });

    this.chargeTypesSharedCollection = this.chargeTypeService.addChargeTypeToCollectionIfMissing(
      this.chargeTypesSharedCollection,
      charge.chargeType
    );
    this.transactionTypesSharedCollection = this.transactionTypeService.addTransactionTypeToCollectionIfMissing(
      this.transactionTypesSharedCollection,
      charge.transactionType
    );
    this.countriesSharedCollection = this.countryService.addCountryToCollectionIfMissing(
      this.countriesSharedCollection,
      charge.txnCountry,
      charge.beneficiaryCountry
    );
    this.currenciesSharedCollection = this.currencyService.addCurrencyToCollectionIfMissing(
      this.currenciesSharedCollection,
      charge.baseCurrency,
      charge.txnCurrency
    );
    this.casaAccountsSharedCollection = this.casaAccountService.addCasaAccountToCollectionIfMissing(
      this.casaAccountsSharedCollection,
      charge.chargeAccount
    );
    this.customerSegmentsSharedCollection = this.customerSegmentService.addCustomerSegmentToCollectionIfMissing(
      this.customerSegmentsSharedCollection,
      charge.customerSegment
    );
  }

  protected loadRelationshipsOptions(): void {
    this.chargeTypeService
      .query()
      .pipe(map((res: HttpResponse<IChargeType[]>) => res.body ?? []))
      .pipe(
        map((chargeTypes: IChargeType[]) =>
          this.chargeTypeService.addChargeTypeToCollectionIfMissing(chargeTypes, this.editForm.get('chargeType')!.value)
        )
      )
      .subscribe((chargeTypes: IChargeType[]) => (this.chargeTypesSharedCollection = chargeTypes));

    this.transactionTypeService
      .query()
      .pipe(map((res: HttpResponse<ITransactionType[]>) => res.body ?? []))
      .pipe(
        map((transactionTypes: ITransactionType[]) =>
          this.transactionTypeService.addTransactionTypeToCollectionIfMissing(transactionTypes, this.editForm.get('transactionType')!.value)
        )
      )
      .subscribe((transactionTypes: ITransactionType[]) => (this.transactionTypesSharedCollection = transactionTypes));

    this.countryService
      .query()
      .pipe(map((res: HttpResponse<ICountry[]>) => res.body ?? []))
      .pipe(
        map((countries: ICountry[]) =>
          this.countryService.addCountryToCollectionIfMissing(
            countries,
            this.editForm.get('txnCountry')!.value,
            this.editForm.get('beneficiaryCountry')!.value
          )
        )
      )
      .subscribe((countries: ICountry[]) => (this.countriesSharedCollection = countries));

    this.currencyService
      .query()
      .pipe(map((res: HttpResponse<ICurrency[]>) => res.body ?? []))
      .pipe(
        map((currencies: ICurrency[]) =>
          this.currencyService.addCurrencyToCollectionIfMissing(
            currencies,
            this.editForm.get('baseCurrency')!.value,
            this.editForm.get('txnCurrency')!.value
          )
        )
      )
      .subscribe((currencies: ICurrency[]) => (this.currenciesSharedCollection = currencies));

    this.casaAccountService
      .query()
      .pipe(map((res: HttpResponse<ICasaAccount[]>) => res.body ?? []))
      .pipe(
        map((casaAccounts: ICasaAccount[]) =>
          this.casaAccountService.addCasaAccountToCollectionIfMissing(casaAccounts, this.editForm.get('chargeAccount')!.value)
        )
      )
      .subscribe((casaAccounts: ICasaAccount[]) => (this.casaAccountsSharedCollection = casaAccounts));

    this.customerSegmentService
      .query()
      .pipe(map((res: HttpResponse<ICustomerSegment[]>) => res.body ?? []))
      .pipe(
        map((customerSegments: ICustomerSegment[]) =>
          this.customerSegmentService.addCustomerSegmentToCollectionIfMissing(customerSegments, this.editForm.get('customerSegment')!.value)
        )
      )
      .subscribe((customerSegments: ICustomerSegment[]) => (this.customerSegmentsSharedCollection = customerSegments));
  }

  protected createFromForm(): ICharge {
    return {
      ...new Charge(),
      id: this.editForm.get(['id'])!.value,
      chargeAmountLocalCcy: this.editForm.get(['chargeAmountLocalCcy'])!.value,
      chargeAmountTxnCcy: this.editForm.get(['chargeAmountTxnCcy'])!.value,
      amountSlab: this.editForm.get(['amountSlab'])!.value,
      amountSlabValue: this.editForm.get(['amountSlabValue'])!.value,
      amountSlabOperator: this.editForm.get(['amountSlabOperator'])!.value,
      amountSlabCurrency: this.editForm.get(['amountSlabCurrency'])!.value,
      recordStatus: this.editForm.get(['recordStatus'])!.value,
      validfrom: this.editForm.get(['validfrom'])!.value,
      validTo: this.editForm.get(['validTo'])!.value,
      chargeType: this.editForm.get(['chargeType'])!.value,
      transactionType: this.editForm.get(['transactionType'])!.value,
      txnCountry: this.editForm.get(['txnCountry'])!.value,
      beneficiaryCountry: this.editForm.get(['beneficiaryCountry'])!.value,
      baseCurrency: this.editForm.get(['baseCurrency'])!.value,
      txnCurrency: this.editForm.get(['txnCurrency'])!.value,
      chargeAccount: this.editForm.get(['chargeAccount'])!.value,
      customerSegment: this.editForm.get(['customerSegment'])!.value,
    };
  }
}
