import { ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpHeaders, HttpResponse } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { of } from 'rxjs';

import { GlEntryTypeService } from '../service/gl-entry-type.service';

import { GlEntryTypeComponent } from './gl-entry-type.component';

describe('GlEntryType Management Component', () => {
  let comp: GlEntryTypeComponent;
  let fixture: ComponentFixture<GlEntryTypeComponent>;
  let service: GlEntryTypeService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      declarations: [GlEntryTypeComponent],
    })
      .overrideTemplate(GlEntryTypeComponent, '')
      .compileComponents();

    fixture = TestBed.createComponent(GlEntryTypeComponent);
    comp = fixture.componentInstance;
    service = TestBed.inject(GlEntryTypeService);

    const headers = new HttpHeaders();
    jest.spyOn(service, 'query').mockReturnValue(
      of(
        new HttpResponse({
          body: [{ id: 123 }],
          headers,
        })
      )
    );
  });

  it('Should call load all on init', () => {
    // WHEN
    comp.ngOnInit();

    // THEN
    expect(service.query).toHaveBeenCalled();
    expect(comp.glEntryTypes?.[0]).toEqual(expect.objectContaining({ id: 123 }));
  });
});
