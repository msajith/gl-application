import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

import { IGlEntryType } from '../gl-entry-type.model';
import { GlEntryTypeService } from '../service/gl-entry-type.service';
import { GlEntryTypeDeleteDialogComponent } from '../delete/gl-entry-type-delete-dialog.component';

@Component({
  selector: 'jhi-gl-entry-type',
  templateUrl: './gl-entry-type.component.html',
})
export class GlEntryTypeComponent implements OnInit {
  glEntryTypes?: IGlEntryType[];
  isLoading = false;

  constructor(protected glEntryTypeService: GlEntryTypeService, protected modalService: NgbModal) {}

  loadAll(): void {
    this.isLoading = true;

    this.glEntryTypeService.query().subscribe({
      next: (res: HttpResponse<IGlEntryType[]>) => {
        this.isLoading = false;
        this.glEntryTypes = res.body ?? [];
      },
      error: () => {
        this.isLoading = false;
      },
    });
  }

  ngOnInit(): void {
    this.loadAll();
  }

  trackId(index: number, item: IGlEntryType): number {
    return item.id!;
  }

  delete(glEntryType: IGlEntryType): void {
    const modalRef = this.modalService.open(GlEntryTypeDeleteDialogComponent, { size: 'lg', backdrop: 'static' });
    modalRef.componentInstance.glEntryType = glEntryType;
    // unsubscribe not needed because closed completes on modal close
    modalRef.closed.subscribe(reason => {
      if (reason === 'deleted') {
        this.loadAll();
      }
    });
  }
}
