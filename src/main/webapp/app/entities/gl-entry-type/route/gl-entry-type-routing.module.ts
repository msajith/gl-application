import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { UserRouteAccessService } from 'app/core/auth/user-route-access.service';
import { GlEntryTypeComponent } from '../list/gl-entry-type.component';
import { GlEntryTypeDetailComponent } from '../detail/gl-entry-type-detail.component';
import { GlEntryTypeUpdateComponent } from '../update/gl-entry-type-update.component';
import { GlEntryTypeRoutingResolveService } from './gl-entry-type-routing-resolve.service';

const glEntryTypeRoute: Routes = [
  {
    path: '',
    component: GlEntryTypeComponent,
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/view',
    component: GlEntryTypeDetailComponent,
    resolve: {
      glEntryType: GlEntryTypeRoutingResolveService,
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: 'new',
    component: GlEntryTypeUpdateComponent,
    resolve: {
      glEntryType: GlEntryTypeRoutingResolveService,
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/edit',
    component: GlEntryTypeUpdateComponent,
    resolve: {
      glEntryType: GlEntryTypeRoutingResolveService,
    },
    canActivate: [UserRouteAccessService],
  },
];

@NgModule({
  imports: [RouterModule.forChild(glEntryTypeRoute)],
  exports: [RouterModule],
})
export class GlEntryTypeRoutingModule {}
