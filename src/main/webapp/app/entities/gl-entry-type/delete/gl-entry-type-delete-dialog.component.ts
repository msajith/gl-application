import { Component } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

import { IGlEntryType } from '../gl-entry-type.model';
import { GlEntryTypeService } from '../service/gl-entry-type.service';

@Component({
  templateUrl: './gl-entry-type-delete-dialog.component.html',
})
export class GlEntryTypeDeleteDialogComponent {
  glEntryType?: IGlEntryType;

  constructor(protected glEntryTypeService: GlEntryTypeService, protected activeModal: NgbActiveModal) {}

  cancel(): void {
    this.activeModal.dismiss();
  }

  confirmDelete(id: number): void {
    this.glEntryTypeService.delete(id).subscribe(() => {
      this.activeModal.close('deleted');
    });
  }
}
