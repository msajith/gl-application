import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IGlEntryType } from '../gl-entry-type.model';

@Component({
  selector: 'jhi-gl-entry-type-detail',
  templateUrl: './gl-entry-type-detail.component.html',
})
export class GlEntryTypeDetailComponent implements OnInit {
  glEntryType: IGlEntryType | null = null;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ glEntryType }) => {
      this.glEntryType = glEntryType;
    });
  }

  previousState(): void {
    window.history.back();
  }
}
