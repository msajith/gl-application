import { NgModule } from '@angular/core';
import { SharedModule } from 'app/shared/shared.module';
import { GlEntryTypeComponent } from './list/gl-entry-type.component';
import { GlEntryTypeDetailComponent } from './detail/gl-entry-type-detail.component';
import { GlEntryTypeUpdateComponent } from './update/gl-entry-type-update.component';
import { GlEntryTypeDeleteDialogComponent } from './delete/gl-entry-type-delete-dialog.component';
import { GlEntryTypeRoutingModule } from './route/gl-entry-type-routing.module';

@NgModule({
  imports: [SharedModule, GlEntryTypeRoutingModule],
  declarations: [GlEntryTypeComponent, GlEntryTypeDetailComponent, GlEntryTypeUpdateComponent, GlEntryTypeDeleteDialogComponent],
  entryComponents: [GlEntryTypeDeleteDialogComponent],
})
export class GlEntryTypeModule {}
