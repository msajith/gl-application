import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

import { IWalletTransaction } from '../wallet-transaction.model';
import { WalletTransactionService } from '../service/wallet-transaction.service';
import { WalletTransactionDeleteDialogComponent } from '../delete/wallet-transaction-delete-dialog.component';

@Component({
  selector: 'jhi-wallet-transaction',
  templateUrl: './wallet-transaction.component.html',
})
export class WalletTransactionComponent implements OnInit {
  walletTransactions?: IWalletTransaction[];
  isLoading = false;

  constructor(protected walletTransactionService: WalletTransactionService, protected modalService: NgbModal) {}

  loadAll(): void {
    this.isLoading = true;

    this.walletTransactionService.query().subscribe({
      next: (res: HttpResponse<IWalletTransaction[]>) => {
        this.isLoading = false;
        this.walletTransactions = res.body ?? [];
      },
      error: () => {
        this.isLoading = false;
      },
    });
  }

  ngOnInit(): void {
    this.loadAll();
  }

  trackId(index: number, item: IWalletTransaction): number {
    return item.id!;
  }

  delete(walletTransaction: IWalletTransaction): void {
    const modalRef = this.modalService.open(WalletTransactionDeleteDialogComponent, { size: 'lg', backdrop: 'static' });
    modalRef.componentInstance.walletTransaction = walletTransaction;
    // unsubscribe not needed because closed completes on modal close
    modalRef.closed.subscribe(reason => {
      if (reason === 'deleted') {
        this.loadAll();
      }
    });
  }
}
