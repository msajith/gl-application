import { NgModule } from '@angular/core';
import { SharedModule } from 'app/shared/shared.module';
import { WalletTransactionComponent } from './list/wallet-transaction.component';
import { WalletTransactionDetailComponent } from './detail/wallet-transaction-detail.component';
import { WalletTransactionUpdateComponent } from './update/wallet-transaction-update.component';
import { WalletTransactionDeleteDialogComponent } from './delete/wallet-transaction-delete-dialog.component';
import { WalletTransactionRoutingModule } from './route/wallet-transaction-routing.module';

@NgModule({
  imports: [SharedModule, WalletTransactionRoutingModule],
  declarations: [
    WalletTransactionComponent,
    WalletTransactionDetailComponent,
    WalletTransactionUpdateComponent,
    WalletTransactionDeleteDialogComponent,
  ],
  entryComponents: [WalletTransactionDeleteDialogComponent],
})
export class WalletTransactionModule {}
