import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import dayjs from 'dayjs/esm';

import { isPresent } from 'app/core/util/operators';
import { DATE_FORMAT } from 'app/config/input.constants';
import { ApplicationConfigService } from 'app/core/config/application-config.service';
import { createRequestOption } from 'app/core/request/request-util';
import { IWalletTransaction, getWalletTransactionIdentifier } from '../wallet-transaction.model';

export type EntityResponseType = HttpResponse<IWalletTransaction>;
export type EntityArrayResponseType = HttpResponse<IWalletTransaction[]>;

@Injectable({ providedIn: 'root' })
export class WalletTransactionService {
  protected resourceUrl = this.applicationConfigService.getEndpointFor('api/wallet-transactions');

  constructor(protected http: HttpClient, protected applicationConfigService: ApplicationConfigService) {}

  create(walletTransaction: IWalletTransaction): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(walletTransaction);
    return this.http
      .post<IWalletTransaction>(this.resourceUrl, copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  update(walletTransaction: IWalletTransaction): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(walletTransaction);
    return this.http
      .put<IWalletTransaction>(`${this.resourceUrl}/${getWalletTransactionIdentifier(walletTransaction) as number}`, copy, {
        observe: 'response',
      })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  partialUpdate(walletTransaction: IWalletTransaction): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(walletTransaction);
    return this.http
      .patch<IWalletTransaction>(`${this.resourceUrl}/${getWalletTransactionIdentifier(walletTransaction) as number}`, copy, {
        observe: 'response',
      })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http
      .get<IWalletTransaction>(`${this.resourceUrl}/${id}`, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http
      .get<IWalletTransaction[]>(this.resourceUrl, { params: options, observe: 'response' })
      .pipe(map((res: EntityArrayResponseType) => this.convertDateArrayFromServer(res)));
  }

  delete(id: number): Observable<HttpResponse<{}>> {
    return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  addWalletTransactionToCollectionIfMissing(
    walletTransactionCollection: IWalletTransaction[],
    ...walletTransactionsToCheck: (IWalletTransaction | null | undefined)[]
  ): IWalletTransaction[] {
    const walletTransactions: IWalletTransaction[] = walletTransactionsToCheck.filter(isPresent);
    if (walletTransactions.length > 0) {
      const walletTransactionCollectionIdentifiers = walletTransactionCollection.map(
        walletTransactionItem => getWalletTransactionIdentifier(walletTransactionItem)!
      );
      const walletTransactionsToAdd = walletTransactions.filter(walletTransactionItem => {
        const walletTransactionIdentifier = getWalletTransactionIdentifier(walletTransactionItem);
        if (walletTransactionIdentifier == null || walletTransactionCollectionIdentifiers.includes(walletTransactionIdentifier)) {
          return false;
        }
        walletTransactionCollectionIdentifiers.push(walletTransactionIdentifier);
        return true;
      });
      return [...walletTransactionsToAdd, ...walletTransactionCollection];
    }
    return walletTransactionCollection;
  }

  protected convertDateFromClient(walletTransaction: IWalletTransaction): IWalletTransaction {
    return Object.assign({}, walletTransaction, {
      txnDate: walletTransaction.txnDate?.isValid() ? walletTransaction.txnDate.format(DATE_FORMAT) : undefined,
      txnDateInUTC: walletTransaction.txnDateInUTC?.isValid() ? walletTransaction.txnDateInUTC.toJSON() : undefined,
      txnDateInLocal: walletTransaction.txnDateInLocal?.isValid() ? walletTransaction.txnDateInLocal.toJSON() : undefined,
      txnDateForSettlement: walletTransaction.txnDateForSettlement?.isValid() ? walletTransaction.txnDateForSettlement.toJSON() : undefined,
      valueDate: walletTransaction.valueDate?.isValid() ? walletTransaction.valueDate.format(DATE_FORMAT) : undefined,
      valueDateInUTC: walletTransaction.valueDateInUTC?.isValid() ? walletTransaction.valueDateInUTC.toJSON() : undefined,
      valueDateInLocal: walletTransaction.valueDateInLocal?.isValid() ? walletTransaction.valueDateInLocal.toJSON() : undefined,
      valueDateForSettlement: walletTransaction.valueDateForSettlement?.isValid()
        ? walletTransaction.valueDateForSettlement.toJSON()
        : undefined,
      createdOn: walletTransaction.createdOn?.isValid() ? walletTransaction.createdOn.toJSON() : undefined,
    });
  }

  protected convertDateFromServer(res: EntityResponseType): EntityResponseType {
    if (res.body) {
      res.body.txnDate = res.body.txnDate ? dayjs(res.body.txnDate) : undefined;
      res.body.txnDateInUTC = res.body.txnDateInUTC ? dayjs(res.body.txnDateInUTC) : undefined;
      res.body.txnDateInLocal = res.body.txnDateInLocal ? dayjs(res.body.txnDateInLocal) : undefined;
      res.body.txnDateForSettlement = res.body.txnDateForSettlement ? dayjs(res.body.txnDateForSettlement) : undefined;
      res.body.valueDate = res.body.valueDate ? dayjs(res.body.valueDate) : undefined;
      res.body.valueDateInUTC = res.body.valueDateInUTC ? dayjs(res.body.valueDateInUTC) : undefined;
      res.body.valueDateInLocal = res.body.valueDateInLocal ? dayjs(res.body.valueDateInLocal) : undefined;
      res.body.valueDateForSettlement = res.body.valueDateForSettlement ? dayjs(res.body.valueDateForSettlement) : undefined;
      res.body.createdOn = res.body.createdOn ? dayjs(res.body.createdOn) : undefined;
    }
    return res;
  }

  protected convertDateArrayFromServer(res: EntityArrayResponseType): EntityArrayResponseType {
    if (res.body) {
      res.body.forEach((walletTransaction: IWalletTransaction) => {
        walletTransaction.txnDate = walletTransaction.txnDate ? dayjs(walletTransaction.txnDate) : undefined;
        walletTransaction.txnDateInUTC = walletTransaction.txnDateInUTC ? dayjs(walletTransaction.txnDateInUTC) : undefined;
        walletTransaction.txnDateInLocal = walletTransaction.txnDateInLocal ? dayjs(walletTransaction.txnDateInLocal) : undefined;
        walletTransaction.txnDateForSettlement = walletTransaction.txnDateForSettlement
          ? dayjs(walletTransaction.txnDateForSettlement)
          : undefined;
        walletTransaction.valueDate = walletTransaction.valueDate ? dayjs(walletTransaction.valueDate) : undefined;
        walletTransaction.valueDateInUTC = walletTransaction.valueDateInUTC ? dayjs(walletTransaction.valueDateInUTC) : undefined;
        walletTransaction.valueDateInLocal = walletTransaction.valueDateInLocal ? dayjs(walletTransaction.valueDateInLocal) : undefined;
        walletTransaction.valueDateForSettlement = walletTransaction.valueDateForSettlement
          ? dayjs(walletTransaction.valueDateForSettlement)
          : undefined;
        walletTransaction.createdOn = walletTransaction.createdOn ? dayjs(walletTransaction.createdOn) : undefined;
      });
    }
    return res;
  }
}
