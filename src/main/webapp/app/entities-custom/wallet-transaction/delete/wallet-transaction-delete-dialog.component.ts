import { Component } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

import { IWalletTransaction } from '../wallet-transaction.model';
import { WalletTransactionService } from '../service/wallet-transaction.service';

@Component({
  templateUrl: './wallet-transaction-delete-dialog.component.html',
})
export class WalletTransactionDeleteDialogComponent {
  walletTransaction?: IWalletTransaction;

  constructor(protected walletTransactionService: WalletTransactionService, protected activeModal: NgbActiveModal) {}

  cancel(): void {
    this.activeModal.dismiss();
  }

  confirmDelete(id: number): void {
    this.walletTransactionService.delete(id).subscribe(() => {
      this.activeModal.close('deleted');
    });
  }
}
