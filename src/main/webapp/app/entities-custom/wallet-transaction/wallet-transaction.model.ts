import dayjs from 'dayjs/esm';
import { ICountry } from 'app/entities/country/country.model';
import { ICurrency } from 'app/entities/currency/currency.model';
import { IPurposeOfTxn } from 'app/entities/purpose-of-txn/purpose-of-txn.model';
import { IFundingSourceType } from 'app/entities/funding-source-type/funding-source-type.model';
import { TxnStatus } from 'app/entities/enumerations/txn-status.model';

export interface IWalletTransaction {
  id?: number;
  txnRefNo?: string | null;
  accountNo?: string | null;
  txnDate?: dayjs.Dayjs | null;
  txnDateInUTC?: dayjs.Dayjs | null;
  txnDateInLocal?: dayjs.Dayjs | null;
  txnDateForSettlement?: dayjs.Dayjs | null;
  valueDate?: dayjs.Dayjs | null;
  valueDateInUTC?: dayjs.Dayjs | null;
  valueDateInLocal?: dayjs.Dayjs | null;
  valueDateForSettlement?: dayjs.Dayjs | null;
  txnAmount?: number | null;
  instructedAmount?: number | null;
  amountInAccountCurrency?: number | null;
  amountInLocalCurrency?: number | null;
  chargeInAccountCurrency?: number | null;
  chargeInLocalCurrency?: number | null;
  fundingSource?: string | null;
  merchantID?: string | null;
  merchantInfo?: string | null;
  externalRefNo?: string | null;
  txnDescription?: string | null;
  txnInternalDescription?: string | null;
  createdOn?: dayjs.Dayjs | null;
  createdBy?: string | null;
  txnStatus?: TxnStatus | null;
  txnDebitAccountNo?: string | null;
  txnCreditAccountNo?: string | null;
  chargeDebitAccountNo?: string | null;
  chargeCreditAccountNo?: string | null;
  country?: ICountry | null;
  txnCurrency?: ICurrency | null;
  accountCurrency?: ICurrency | null;
  instructedCurrency?: ICurrency | null;
  purposeOfTxn?: IPurposeOfTxn | null;
  fundingSourceType?: IFundingSourceType | null;
}

export class WalletTransaction implements IWalletTransaction {
  constructor(
    public id?: number,
    public txnRefNo?: string | null,
    public accountNo?: string | null,
    public txnDate?: dayjs.Dayjs | null,
    public txnDateInUTC?: dayjs.Dayjs | null,
    public txnDateInLocal?: dayjs.Dayjs | null,
    public txnDateForSettlement?: dayjs.Dayjs | null,
    public valueDate?: dayjs.Dayjs | null,
    public valueDateInUTC?: dayjs.Dayjs | null,
    public valueDateInLocal?: dayjs.Dayjs | null,
    public valueDateForSettlement?: dayjs.Dayjs | null,
    public txnAmount?: number | null,
    public instructedAmount?: number | null,
    public amountInAccountCurrency?: number | null,
    public amountInLocalCurrency?: number | null,
    public chargeInAccountCurrency?: number | null,
    public chargeInLocalCurrency?: number | null,
    public fundingSource?: string | null,
    public merchantID?: string | null,
    public merchantInfo?: string | null,
    public externalRefNo?: string | null,
    public txnDescription?: string | null,
    public txnInternalDescription?: string | null,
    public createdOn?: dayjs.Dayjs | null,
    public createdBy?: string | null,
    public txnStatus?: TxnStatus | null,
    public txnDebitAccountNo?: string | null,
    public txnCreditAccountNo?: string | null,
    public chargeDebitAccountNo?: string | null,
    public chargeCreditAccountNo?: string | null,
    public country?: ICountry | null,
    public txnCurrency?: ICurrency | null,
    public accountCurrency?: ICurrency | null,
    public instructedCurrency?: ICurrency | null,
    public purposeOfTxn?: IPurposeOfTxn | null,
    public fundingSourceType?: IFundingSourceType | null
  ) {}
}

export function getWalletTransactionIdentifier(walletTransaction: IWalletTransaction): number | undefined {
  return walletTransaction.id;
}
