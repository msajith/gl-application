import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { UserRouteAccessService } from 'app/core/auth/user-route-access.service';
import { WalletTransactionComponent } from '../list/wallet-transaction.component';
import { WalletTransactionDetailComponent } from '../detail/wallet-transaction-detail.component';
import { WalletTransactionUpdateComponent } from '../update/wallet-transaction-update.component';
import { WalletTransactionRoutingResolveService } from './wallet-transaction-routing-resolve.service';

const walletTransactionRoute: Routes = [
  {
    path: '',
    component: WalletTransactionComponent,
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/view',
    component: WalletTransactionDetailComponent,
    resolve: {
      walletTransaction: WalletTransactionRoutingResolveService,
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: 'new',
    component: WalletTransactionUpdateComponent,
    resolve: {
      walletTransaction: WalletTransactionRoutingResolveService,
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/edit',
    component: WalletTransactionUpdateComponent,
    resolve: {
      walletTransaction: WalletTransactionRoutingResolveService,
    },
    canActivate: [UserRouteAccessService],
  },
];

@NgModule({
  imports: [RouterModule.forChild(walletTransactionRoute)],
  exports: [RouterModule],
})
export class WalletTransactionRoutingModule {}
