import { TestBed } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { ActivatedRouteSnapshot, ActivatedRoute, Router, convertToParamMap } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { of } from 'rxjs';

import { IWalletTransaction, WalletTransaction } from '../wallet-transaction.model';
import { WalletTransactionService } from '../service/wallet-transaction.service';

import { WalletTransactionRoutingResolveService } from './wallet-transaction-routing-resolve.service';

describe('WalletTransaction routing resolve service', () => {
  let mockRouter: Router;
  let mockActivatedRouteSnapshot: ActivatedRouteSnapshot;
  let routingResolveService: WalletTransactionRoutingResolveService;
  let service: WalletTransactionService;
  let resultWalletTransaction: IWalletTransaction | undefined;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule, RouterTestingModule.withRoutes([])],
      providers: [
        {
          provide: ActivatedRoute,
          useValue: {
            snapshot: {
              paramMap: convertToParamMap({}),
            },
          },
        },
      ],
    });
    mockRouter = TestBed.inject(Router);
    jest.spyOn(mockRouter, 'navigate').mockImplementation(() => Promise.resolve(true));
    mockActivatedRouteSnapshot = TestBed.inject(ActivatedRoute).snapshot;
    routingResolveService = TestBed.inject(WalletTransactionRoutingResolveService);
    service = TestBed.inject(WalletTransactionService);
    resultWalletTransaction = undefined;
  });

  describe('resolve', () => {
    it('should return IWalletTransaction returned by find', () => {
      // GIVEN
      service.find = jest.fn(id => of(new HttpResponse({ body: { id } })));
      mockActivatedRouteSnapshot.params = { id: 123 };

      // WHEN
      routingResolveService.resolve(mockActivatedRouteSnapshot).subscribe(result => {
        resultWalletTransaction = result;
      });

      // THEN
      expect(service.find).toBeCalledWith(123);
      expect(resultWalletTransaction).toEqual({ id: 123 });
    });

    it('should return new IWalletTransaction if id is not provided', () => {
      // GIVEN
      service.find = jest.fn();
      mockActivatedRouteSnapshot.params = {};

      // WHEN
      routingResolveService.resolve(mockActivatedRouteSnapshot).subscribe(result => {
        resultWalletTransaction = result;
      });

      // THEN
      expect(service.find).not.toBeCalled();
      expect(resultWalletTransaction).toEqual(new WalletTransaction());
    });

    it('should route to 404 page if data not found in server', () => {
      // GIVEN
      jest.spyOn(service, 'find').mockReturnValue(of(new HttpResponse({ body: null as unknown as WalletTransaction })));
      mockActivatedRouteSnapshot.params = { id: 123 };

      // WHEN
      routingResolveService.resolve(mockActivatedRouteSnapshot).subscribe(result => {
        resultWalletTransaction = result;
      });

      // THEN
      expect(service.find).toBeCalledWith(123);
      expect(resultWalletTransaction).toEqual(undefined);
      expect(mockRouter.navigate).toHaveBeenCalledWith(['404']);
    });
  });
});
