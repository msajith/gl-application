import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { PurposeOfTxnDetailComponent } from './purpose-of-txn-detail.component';

describe('PurposeOfTxn Management Detail Component', () => {
  let comp: PurposeOfTxnDetailComponent;
  let fixture: ComponentFixture<PurposeOfTxnDetailComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [PurposeOfTxnDetailComponent],
      providers: [
        {
          provide: ActivatedRoute,
          useValue: { data: of({ purposeOfTxn: { id: 123 } }) },
        },
      ],
    })
      .overrideTemplate(PurposeOfTxnDetailComponent, '')
      .compileComponents();
    fixture = TestBed.createComponent(PurposeOfTxnDetailComponent);
    comp = fixture.componentInstance;
  });

  describe('OnInit', () => {
    it('Should load purposeOfTxn on init', () => {
      // WHEN
      comp.ngOnInit();

      // THEN
      expect(comp.purposeOfTxn).toEqual(expect.objectContaining({ id: 123 }));
    });
  });
});
