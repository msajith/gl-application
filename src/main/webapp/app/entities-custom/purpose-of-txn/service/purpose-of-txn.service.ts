import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { isPresent } from 'app/core/util/operators';
import { ApplicationConfigService } from 'app/core/config/application-config.service';
import { createRequestOption } from 'app/core/request/request-util';
import { IPurposeOfTxn, getPurposeOfTxnIdentifier } from '../purpose-of-txn.model';

export type EntityResponseType = HttpResponse<IPurposeOfTxn>;
export type EntityArrayResponseType = HttpResponse<IPurposeOfTxn[]>;

@Injectable({ providedIn: 'root' })
export class PurposeOfTxnService {
  protected resourceUrl = this.applicationConfigService.getEndpointFor('api/purpose-of-txns');

  constructor(protected http: HttpClient, protected applicationConfigService: ApplicationConfigService) {}

  create(purposeOfTxn: IPurposeOfTxn): Observable<EntityResponseType> {
    return this.http.post<IPurposeOfTxn>(this.resourceUrl, purposeOfTxn, { observe: 'response' });
  }

  update(purposeOfTxn: IPurposeOfTxn): Observable<EntityResponseType> {
    return this.http.put<IPurposeOfTxn>(`${this.resourceUrl}/${getPurposeOfTxnIdentifier(purposeOfTxn) as number}`, purposeOfTxn, {
      observe: 'response',
    });
  }

  partialUpdate(purposeOfTxn: IPurposeOfTxn): Observable<EntityResponseType> {
    return this.http.patch<IPurposeOfTxn>(`${this.resourceUrl}/${getPurposeOfTxnIdentifier(purposeOfTxn) as number}`, purposeOfTxn, {
      observe: 'response',
    });
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http.get<IPurposeOfTxn>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<IPurposeOfTxn[]>(this.resourceUrl, { params: options, observe: 'response' });
  }

  delete(id: number): Observable<HttpResponse<{}>> {
    return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  addPurposeOfTxnToCollectionIfMissing(
    purposeOfTxnCollection: IPurposeOfTxn[],
    ...purposeOfTxnsToCheck: (IPurposeOfTxn | null | undefined)[]
  ): IPurposeOfTxn[] {
    const purposeOfTxns: IPurposeOfTxn[] = purposeOfTxnsToCheck.filter(isPresent);
    if (purposeOfTxns.length > 0) {
      const purposeOfTxnCollectionIdentifiers = purposeOfTxnCollection.map(
        purposeOfTxnItem => getPurposeOfTxnIdentifier(purposeOfTxnItem)!
      );
      const purposeOfTxnsToAdd = purposeOfTxns.filter(purposeOfTxnItem => {
        const purposeOfTxnIdentifier = getPurposeOfTxnIdentifier(purposeOfTxnItem);
        if (purposeOfTxnIdentifier == null || purposeOfTxnCollectionIdentifiers.includes(purposeOfTxnIdentifier)) {
          return false;
        }
        purposeOfTxnCollectionIdentifiers.push(purposeOfTxnIdentifier);
        return true;
      });
      return [...purposeOfTxnsToAdd, ...purposeOfTxnCollection];
    }
    return purposeOfTxnCollection;
  }
}
