import { TestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';

import { IPurposeOfTxn, PurposeOfTxn } from '../purpose-of-txn.model';

import { PurposeOfTxnService } from './purpose-of-txn.service';

describe('PurposeOfTxn Service', () => {
  let service: PurposeOfTxnService;
  let httpMock: HttpTestingController;
  let elemDefault: IPurposeOfTxn;
  let expectedResult: IPurposeOfTxn | IPurposeOfTxn[] | boolean | null;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
    });
    expectedResult = null;
    service = TestBed.inject(PurposeOfTxnService);
    httpMock = TestBed.inject(HttpTestingController);

    elemDefault = {
      id: 0,
      name: 'AAAAAAA',
      code: 'AAAAAAA',
    };
  });

  describe('Service methods', () => {
    it('should find an element', () => {
      const returnedFromService = Object.assign({}, elemDefault);

      service.find(123).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'GET' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(elemDefault);
    });

    it('should create a PurposeOfTxn', () => {
      const returnedFromService = Object.assign(
        {
          id: 0,
        },
        elemDefault
      );

      const expected = Object.assign({}, returnedFromService);

      service.create(new PurposeOfTxn()).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'POST' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(expected);
    });

    it('should update a PurposeOfTxn', () => {
      const returnedFromService = Object.assign(
        {
          id: 1,
          name: 'BBBBBB',
          code: 'BBBBBB',
        },
        elemDefault
      );

      const expected = Object.assign({}, returnedFromService);

      service.update(expected).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'PUT' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(expected);
    });

    it('should partial update a PurposeOfTxn', () => {
      const patchObject = Object.assign(
        {
          name: 'BBBBBB',
        },
        new PurposeOfTxn()
      );

      const returnedFromService = Object.assign(patchObject, elemDefault);

      const expected = Object.assign({}, returnedFromService);

      service.partialUpdate(patchObject).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'PATCH' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(expected);
    });

    it('should return a list of PurposeOfTxn', () => {
      const returnedFromService = Object.assign(
        {
          id: 1,
          name: 'BBBBBB',
          code: 'BBBBBB',
        },
        elemDefault
      );

      const expected = Object.assign({}, returnedFromService);

      service.query().subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'GET' });
      req.flush([returnedFromService]);
      httpMock.verify();
      expect(expectedResult).toContainEqual(expected);
    });

    it('should delete a PurposeOfTxn', () => {
      service.delete(123).subscribe(resp => (expectedResult = resp.ok));

      const req = httpMock.expectOne({ method: 'DELETE' });
      req.flush({ status: 200 });
      expect(expectedResult);
    });

    describe('addPurposeOfTxnToCollectionIfMissing', () => {
      it('should add a PurposeOfTxn to an empty array', () => {
        const purposeOfTxn: IPurposeOfTxn = { id: 123 };
        expectedResult = service.addPurposeOfTxnToCollectionIfMissing([], purposeOfTxn);
        expect(expectedResult).toHaveLength(1);
        expect(expectedResult).toContain(purposeOfTxn);
      });

      it('should not add a PurposeOfTxn to an array that contains it', () => {
        const purposeOfTxn: IPurposeOfTxn = { id: 123 };
        const purposeOfTxnCollection: IPurposeOfTxn[] = [
          {
            ...purposeOfTxn,
          },
          { id: 456 },
        ];
        expectedResult = service.addPurposeOfTxnToCollectionIfMissing(purposeOfTxnCollection, purposeOfTxn);
        expect(expectedResult).toHaveLength(2);
      });

      it("should add a PurposeOfTxn to an array that doesn't contain it", () => {
        const purposeOfTxn: IPurposeOfTxn = { id: 123 };
        const purposeOfTxnCollection: IPurposeOfTxn[] = [{ id: 456 }];
        expectedResult = service.addPurposeOfTxnToCollectionIfMissing(purposeOfTxnCollection, purposeOfTxn);
        expect(expectedResult).toHaveLength(2);
        expect(expectedResult).toContain(purposeOfTxn);
      });

      it('should add only unique PurposeOfTxn to an array', () => {
        const purposeOfTxnArray: IPurposeOfTxn[] = [{ id: 123 }, { id: 456 }, { id: 99875 }];
        const purposeOfTxnCollection: IPurposeOfTxn[] = [{ id: 123 }];
        expectedResult = service.addPurposeOfTxnToCollectionIfMissing(purposeOfTxnCollection, ...purposeOfTxnArray);
        expect(expectedResult).toHaveLength(3);
      });

      it('should accept varargs', () => {
        const purposeOfTxn: IPurposeOfTxn = { id: 123 };
        const purposeOfTxn2: IPurposeOfTxn = { id: 456 };
        expectedResult = service.addPurposeOfTxnToCollectionIfMissing([], purposeOfTxn, purposeOfTxn2);
        expect(expectedResult).toHaveLength(2);
        expect(expectedResult).toContain(purposeOfTxn);
        expect(expectedResult).toContain(purposeOfTxn2);
      });

      it('should accept null and undefined values', () => {
        const purposeOfTxn: IPurposeOfTxn = { id: 123 };
        expectedResult = service.addPurposeOfTxnToCollectionIfMissing([], null, purposeOfTxn, undefined);
        expect(expectedResult).toHaveLength(1);
        expect(expectedResult).toContain(purposeOfTxn);
      });

      it('should return initial array if no PurposeOfTxn is added', () => {
        const purposeOfTxnCollection: IPurposeOfTxn[] = [{ id: 123 }];
        expectedResult = service.addPurposeOfTxnToCollectionIfMissing(purposeOfTxnCollection, undefined, null);
        expect(expectedResult).toEqual(purposeOfTxnCollection);
      });
    });
  });

  afterEach(() => {
    httpMock.verify();
  });
});
