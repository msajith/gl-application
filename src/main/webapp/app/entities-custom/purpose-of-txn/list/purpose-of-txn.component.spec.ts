import { ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpHeaders, HttpResponse } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { of } from 'rxjs';

import { PurposeOfTxnService } from '../service/purpose-of-txn.service';

import { PurposeOfTxnComponent } from './purpose-of-txn.component';

describe('PurposeOfTxn Management Component', () => {
  let comp: PurposeOfTxnComponent;
  let fixture: ComponentFixture<PurposeOfTxnComponent>;
  let service: PurposeOfTxnService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      declarations: [PurposeOfTxnComponent],
    })
      .overrideTemplate(PurposeOfTxnComponent, '')
      .compileComponents();

    fixture = TestBed.createComponent(PurposeOfTxnComponent);
    comp = fixture.componentInstance;
    service = TestBed.inject(PurposeOfTxnService);

    const headers = new HttpHeaders();
    jest.spyOn(service, 'query').mockReturnValue(
      of(
        new HttpResponse({
          body: [{ id: 123 }],
          headers,
        })
      )
    );
  });

  it('Should call load all on init', () => {
    // WHEN
    comp.ngOnInit();

    // THEN
    expect(service.query).toHaveBeenCalled();
    expect(comp.purposeOfTxns?.[0]).toEqual(expect.objectContaining({ id: 123 }));
  });
});
