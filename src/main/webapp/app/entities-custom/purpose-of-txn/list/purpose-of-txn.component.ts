import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

import { IPurposeOfTxn } from '../purpose-of-txn.model';
import { PurposeOfTxnService } from '../service/purpose-of-txn.service';
import { PurposeOfTxnDeleteDialogComponent } from '../delete/purpose-of-txn-delete-dialog.component';

@Component({
  selector: 'jhi-purpose-of-txn',
  templateUrl: './purpose-of-txn.component.html',
})
export class PurposeOfTxnComponent implements OnInit {
  purposeOfTxns?: IPurposeOfTxn[];
  isLoading = false;

  constructor(protected purposeOfTxnService: PurposeOfTxnService, protected modalService: NgbModal) {}

  loadAll(): void {
    this.isLoading = true;

    this.purposeOfTxnService.query().subscribe({
      next: (res: HttpResponse<IPurposeOfTxn[]>) => {
        this.isLoading = false;
        this.purposeOfTxns = res.body ?? [];
      },
      error: () => {
        this.isLoading = false;
      },
    });
  }

  ngOnInit(): void {
    this.loadAll();
  }

  trackId(index: number, item: IPurposeOfTxn): number {
    return item.id!;
  }

  delete(purposeOfTxn: IPurposeOfTxn): void {
    const modalRef = this.modalService.open(PurposeOfTxnDeleteDialogComponent, { size: 'lg', backdrop: 'static' });
    modalRef.componentInstance.purposeOfTxn = purposeOfTxn;
    // unsubscribe not needed because closed completes on modal close
    modalRef.closed.subscribe(reason => {
      if (reason === 'deleted') {
        this.loadAll();
      }
    });
  }
}
