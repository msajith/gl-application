export interface IPurposeOfTxn {
  id?: number;
  name?: string | null;
  code?: string | null;
}

export class PurposeOfTxn implements IPurposeOfTxn {
  constructor(public id?: number, public name?: string | null, public code?: string | null) {}
}

export function getPurposeOfTxnIdentifier(purposeOfTxn: IPurposeOfTxn): number | undefined {
  return purposeOfTxn.id;
}
