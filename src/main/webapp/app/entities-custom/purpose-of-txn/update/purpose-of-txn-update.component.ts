import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { finalize } from 'rxjs/operators';

import { IPurposeOfTxn, PurposeOfTxn } from '../purpose-of-txn.model';
import { PurposeOfTxnService } from '../service/purpose-of-txn.service';

@Component({
  selector: 'jhi-purpose-of-txn-update',
  templateUrl: './purpose-of-txn-update.component.html',
})
export class PurposeOfTxnUpdateComponent implements OnInit {
  isSaving = false;

  editForm = this.fb.group({
    id: [],
    name: [],
    code: [],
  });

  constructor(protected purposeOfTxnService: PurposeOfTxnService, protected activatedRoute: ActivatedRoute, protected fb: FormBuilder) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ purposeOfTxn }) => {
      this.updateForm(purposeOfTxn);
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const purposeOfTxn = this.createFromForm();
    if (purposeOfTxn.id !== undefined) {
      this.subscribeToSaveResponse(this.purposeOfTxnService.update(purposeOfTxn));
    } else {
      this.subscribeToSaveResponse(this.purposeOfTxnService.create(purposeOfTxn));
    }
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IPurposeOfTxn>>): void {
    result.pipe(finalize(() => this.onSaveFinalize())).subscribe({
      next: () => this.onSaveSuccess(),
      error: () => this.onSaveError(),
    });
  }

  protected onSaveSuccess(): void {
    this.previousState();
  }

  protected onSaveError(): void {
    // Api for inheritance.
  }

  protected onSaveFinalize(): void {
    this.isSaving = false;
  }

  protected updateForm(purposeOfTxn: IPurposeOfTxn): void {
    this.editForm.patchValue({
      id: purposeOfTxn.id,
      name: purposeOfTxn.name,
      code: purposeOfTxn.code,
    });
  }

  protected createFromForm(): IPurposeOfTxn {
    return {
      ...new PurposeOfTxn(),
      id: this.editForm.get(['id'])!.value,
      name: this.editForm.get(['name'])!.value,
      code: this.editForm.get(['code'])!.value,
    };
  }
}
