import { GlAccountType } from 'app/entities/enumerations/gl-account-type.model';

export interface IAccountType {
  id?: number;
  name?: string | null;
  code?: string | null;
  glAccountType?: GlAccountType | null;
}

export class AccountType implements IAccountType {
  constructor(public id?: number, public name?: string | null, public code?: string | null, public glAccountType?: GlAccountType | null) {}
}

export function getAccountTypeIdentifier(accountType: IAccountType): number | undefined {
  return accountType.id;
}
