import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { GlEntryTypeDetailComponent } from './gl-entry-type-detail.component';

describe('GlEntryType Management Detail Component', () => {
  let comp: GlEntryTypeDetailComponent;
  let fixture: ComponentFixture<GlEntryTypeDetailComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [GlEntryTypeDetailComponent],
      providers: [
        {
          provide: ActivatedRoute,
          useValue: { data: of({ glEntryType: { id: 123 } }) },
        },
      ],
    })
      .overrideTemplate(GlEntryTypeDetailComponent, '')
      .compileComponents();
    fixture = TestBed.createComponent(GlEntryTypeDetailComponent);
    comp = fixture.componentInstance;
  });

  describe('OnInit', () => {
    it('Should load glEntryType on init', () => {
      // WHEN
      comp.ngOnInit();

      // THEN
      expect(comp.glEntryType).toEqual(expect.objectContaining({ id: 123 }));
    });
  });
});
