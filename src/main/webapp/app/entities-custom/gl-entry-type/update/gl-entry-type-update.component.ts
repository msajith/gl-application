import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { finalize } from 'rxjs/operators';

import { IGlEntryType, GlEntryType } from '../gl-entry-type.model';
import { GlEntryTypeService } from '../service/gl-entry-type.service';

@Component({
  selector: 'jhi-gl-entry-type-update',
  templateUrl: './gl-entry-type-update.component.html',
})
export class GlEntryTypeUpdateComponent implements OnInit {
  isSaving = false;

  editForm = this.fb.group({
    id: [],
    name: [],
    code: [],
  });

  constructor(protected glEntryTypeService: GlEntryTypeService, protected activatedRoute: ActivatedRoute, protected fb: FormBuilder) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ glEntryType }) => {
      this.updateForm(glEntryType);
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const glEntryType = this.createFromForm();
    if (glEntryType.id !== undefined) {
      this.subscribeToSaveResponse(this.glEntryTypeService.update(glEntryType));
    } else {
      this.subscribeToSaveResponse(this.glEntryTypeService.create(glEntryType));
    }
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IGlEntryType>>): void {
    result.pipe(finalize(() => this.onSaveFinalize())).subscribe({
      next: () => this.onSaveSuccess(),
      error: () => this.onSaveError(),
    });
  }

  protected onSaveSuccess(): void {
    this.previousState();
  }

  protected onSaveError(): void {
    // Api for inheritance.
  }

  protected onSaveFinalize(): void {
    this.isSaving = false;
  }

  protected updateForm(glEntryType: IGlEntryType): void {
    this.editForm.patchValue({
      id: glEntryType.id,
      name: glEntryType.name,
      code: glEntryType.code,
    });
  }

  protected createFromForm(): IGlEntryType {
    return {
      ...new GlEntryType(),
      id: this.editForm.get(['id'])!.value,
      name: this.editForm.get(['name'])!.value,
      code: this.editForm.get(['code'])!.value,
    };
  }
}
