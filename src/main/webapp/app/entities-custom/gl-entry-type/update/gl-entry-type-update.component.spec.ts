import { ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { of, Subject, from } from 'rxjs';

import { GlEntryTypeService } from '../service/gl-entry-type.service';
import { IGlEntryType, GlEntryType } from '../gl-entry-type.model';

import { GlEntryTypeUpdateComponent } from './gl-entry-type-update.component';

describe('GlEntryType Management Update Component', () => {
  let comp: GlEntryTypeUpdateComponent;
  let fixture: ComponentFixture<GlEntryTypeUpdateComponent>;
  let activatedRoute: ActivatedRoute;
  let glEntryTypeService: GlEntryTypeService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule, RouterTestingModule.withRoutes([])],
      declarations: [GlEntryTypeUpdateComponent],
      providers: [
        FormBuilder,
        {
          provide: ActivatedRoute,
          useValue: {
            params: from([{}]),
          },
        },
      ],
    })
      .overrideTemplate(GlEntryTypeUpdateComponent, '')
      .compileComponents();

    fixture = TestBed.createComponent(GlEntryTypeUpdateComponent);
    activatedRoute = TestBed.inject(ActivatedRoute);
    glEntryTypeService = TestBed.inject(GlEntryTypeService);

    comp = fixture.componentInstance;
  });

  describe('ngOnInit', () => {
    it('Should update editForm', () => {
      const glEntryType: IGlEntryType = { id: 456 };

      activatedRoute.data = of({ glEntryType });
      comp.ngOnInit();

      expect(comp.editForm.value).toEqual(expect.objectContaining(glEntryType));
    });
  });

  describe('save', () => {
    it('Should call update service on save for existing entity', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<GlEntryType>>();
      const glEntryType = { id: 123 };
      jest.spyOn(glEntryTypeService, 'update').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ glEntryType });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.next(new HttpResponse({ body: glEntryType }));
      saveSubject.complete();

      // THEN
      expect(comp.previousState).toHaveBeenCalled();
      expect(glEntryTypeService.update).toHaveBeenCalledWith(glEntryType);
      expect(comp.isSaving).toEqual(false);
    });

    it('Should call create service on save for new entity', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<GlEntryType>>();
      const glEntryType = new GlEntryType();
      jest.spyOn(glEntryTypeService, 'create').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ glEntryType });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.next(new HttpResponse({ body: glEntryType }));
      saveSubject.complete();

      // THEN
      expect(glEntryTypeService.create).toHaveBeenCalledWith(glEntryType);
      expect(comp.isSaving).toEqual(false);
      expect(comp.previousState).toHaveBeenCalled();
    });

    it('Should set isSaving to false on error', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<GlEntryType>>();
      const glEntryType = { id: 123 };
      jest.spyOn(glEntryTypeService, 'update').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ glEntryType });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.error('This is an error!');

      // THEN
      expect(glEntryTypeService.update).toHaveBeenCalledWith(glEntryType);
      expect(comp.isSaving).toEqual(false);
      expect(comp.previousState).not.toHaveBeenCalled();
    });
  });
});
