export interface IGlEntryType {
  id?: number;
  name?: string | null;
  code?: string | null;
}

export class GlEntryType implements IGlEntryType {
  constructor(public id?: number, public name?: string | null, public code?: string | null) {}
}

export function getGlEntryTypeIdentifier(glEntryType: IGlEntryType): number | undefined {
  return glEntryType.id;
}
