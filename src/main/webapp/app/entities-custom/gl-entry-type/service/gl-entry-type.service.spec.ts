import { TestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';

import { IGlEntryType, GlEntryType } from '../gl-entry-type.model';

import { GlEntryTypeService } from './gl-entry-type.service';

describe('GlEntryType Service', () => {
  let service: GlEntryTypeService;
  let httpMock: HttpTestingController;
  let elemDefault: IGlEntryType;
  let expectedResult: IGlEntryType | IGlEntryType[] | boolean | null;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
    });
    expectedResult = null;
    service = TestBed.inject(GlEntryTypeService);
    httpMock = TestBed.inject(HttpTestingController);

    elemDefault = {
      id: 0,
      name: 'AAAAAAA',
      code: 'AAAAAAA',
    };
  });

  describe('Service methods', () => {
    it('should find an element', () => {
      const returnedFromService = Object.assign({}, elemDefault);

      service.find(123).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'GET' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(elemDefault);
    });

    it('should create a GlEntryType', () => {
      const returnedFromService = Object.assign(
        {
          id: 0,
        },
        elemDefault
      );

      const expected = Object.assign({}, returnedFromService);

      service.create(new GlEntryType()).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'POST' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(expected);
    });

    it('should update a GlEntryType', () => {
      const returnedFromService = Object.assign(
        {
          id: 1,
          name: 'BBBBBB',
          code: 'BBBBBB',
        },
        elemDefault
      );

      const expected = Object.assign({}, returnedFromService);

      service.update(expected).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'PUT' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(expected);
    });

    it('should partial update a GlEntryType', () => {
      const patchObject = Object.assign(
        {
          name: 'BBBBBB',
          code: 'BBBBBB',
        },
        new GlEntryType()
      );

      const returnedFromService = Object.assign(patchObject, elemDefault);

      const expected = Object.assign({}, returnedFromService);

      service.partialUpdate(patchObject).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'PATCH' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(expected);
    });

    it('should return a list of GlEntryType', () => {
      const returnedFromService = Object.assign(
        {
          id: 1,
          name: 'BBBBBB',
          code: 'BBBBBB',
        },
        elemDefault
      );

      const expected = Object.assign({}, returnedFromService);

      service.query().subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'GET' });
      req.flush([returnedFromService]);
      httpMock.verify();
      expect(expectedResult).toContainEqual(expected);
    });

    it('should delete a GlEntryType', () => {
      service.delete(123).subscribe(resp => (expectedResult = resp.ok));

      const req = httpMock.expectOne({ method: 'DELETE' });
      req.flush({ status: 200 });
      expect(expectedResult);
    });

    describe('addGlEntryTypeToCollectionIfMissing', () => {
      it('should add a GlEntryType to an empty array', () => {
        const glEntryType: IGlEntryType = { id: 123 };
        expectedResult = service.addGlEntryTypeToCollectionIfMissing([], glEntryType);
        expect(expectedResult).toHaveLength(1);
        expect(expectedResult).toContain(glEntryType);
      });

      it('should not add a GlEntryType to an array that contains it', () => {
        const glEntryType: IGlEntryType = { id: 123 };
        const glEntryTypeCollection: IGlEntryType[] = [
          {
            ...glEntryType,
          },
          { id: 456 },
        ];
        expectedResult = service.addGlEntryTypeToCollectionIfMissing(glEntryTypeCollection, glEntryType);
        expect(expectedResult).toHaveLength(2);
      });

      it("should add a GlEntryType to an array that doesn't contain it", () => {
        const glEntryType: IGlEntryType = { id: 123 };
        const glEntryTypeCollection: IGlEntryType[] = [{ id: 456 }];
        expectedResult = service.addGlEntryTypeToCollectionIfMissing(glEntryTypeCollection, glEntryType);
        expect(expectedResult).toHaveLength(2);
        expect(expectedResult).toContain(glEntryType);
      });

      it('should add only unique GlEntryType to an array', () => {
        const glEntryTypeArray: IGlEntryType[] = [{ id: 123 }, { id: 456 }, { id: 71048 }];
        const glEntryTypeCollection: IGlEntryType[] = [{ id: 123 }];
        expectedResult = service.addGlEntryTypeToCollectionIfMissing(glEntryTypeCollection, ...glEntryTypeArray);
        expect(expectedResult).toHaveLength(3);
      });

      it('should accept varargs', () => {
        const glEntryType: IGlEntryType = { id: 123 };
        const glEntryType2: IGlEntryType = { id: 456 };
        expectedResult = service.addGlEntryTypeToCollectionIfMissing([], glEntryType, glEntryType2);
        expect(expectedResult).toHaveLength(2);
        expect(expectedResult).toContain(glEntryType);
        expect(expectedResult).toContain(glEntryType2);
      });

      it('should accept null and undefined values', () => {
        const glEntryType: IGlEntryType = { id: 123 };
        expectedResult = service.addGlEntryTypeToCollectionIfMissing([], null, glEntryType, undefined);
        expect(expectedResult).toHaveLength(1);
        expect(expectedResult).toContain(glEntryType);
      });

      it('should return initial array if no GlEntryType is added', () => {
        const glEntryTypeCollection: IGlEntryType[] = [{ id: 123 }];
        expectedResult = service.addGlEntryTypeToCollectionIfMissing(glEntryTypeCollection, undefined, null);
        expect(expectedResult).toEqual(glEntryTypeCollection);
      });
    });
  });

  afterEach(() => {
    httpMock.verify();
  });
});
