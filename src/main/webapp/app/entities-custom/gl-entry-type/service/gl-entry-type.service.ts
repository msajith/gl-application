import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { isPresent } from 'app/core/util/operators';
import { ApplicationConfigService } from 'app/core/config/application-config.service';
import { createRequestOption } from 'app/core/request/request-util';
import { IGlEntryType, getGlEntryTypeIdentifier } from '../gl-entry-type.model';

export type EntityResponseType = HttpResponse<IGlEntryType>;
export type EntityArrayResponseType = HttpResponse<IGlEntryType[]>;

@Injectable({ providedIn: 'root' })
export class GlEntryTypeService {
  protected resourceUrl = this.applicationConfigService.getEndpointFor('api/gl-entry-types');

  constructor(protected http: HttpClient, protected applicationConfigService: ApplicationConfigService) {}

  create(glEntryType: IGlEntryType): Observable<EntityResponseType> {
    return this.http.post<IGlEntryType>(this.resourceUrl, glEntryType, { observe: 'response' });
  }

  update(glEntryType: IGlEntryType): Observable<EntityResponseType> {
    return this.http.put<IGlEntryType>(`${this.resourceUrl}/${getGlEntryTypeIdentifier(glEntryType) as number}`, glEntryType, {
      observe: 'response',
    });
  }

  partialUpdate(glEntryType: IGlEntryType): Observable<EntityResponseType> {
    return this.http.patch<IGlEntryType>(`${this.resourceUrl}/${getGlEntryTypeIdentifier(glEntryType) as number}`, glEntryType, {
      observe: 'response',
    });
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http.get<IGlEntryType>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<IGlEntryType[]>(this.resourceUrl, { params: options, observe: 'response' });
  }

  delete(id: number): Observable<HttpResponse<{}>> {
    return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  addGlEntryTypeToCollectionIfMissing(
    glEntryTypeCollection: IGlEntryType[],
    ...glEntryTypesToCheck: (IGlEntryType | null | undefined)[]
  ): IGlEntryType[] {
    const glEntryTypes: IGlEntryType[] = glEntryTypesToCheck.filter(isPresent);
    if (glEntryTypes.length > 0) {
      const glEntryTypeCollectionIdentifiers = glEntryTypeCollection.map(glEntryTypeItem => getGlEntryTypeIdentifier(glEntryTypeItem)!);
      const glEntryTypesToAdd = glEntryTypes.filter(glEntryTypeItem => {
        const glEntryTypeIdentifier = getGlEntryTypeIdentifier(glEntryTypeItem);
        if (glEntryTypeIdentifier == null || glEntryTypeCollectionIdentifiers.includes(glEntryTypeIdentifier)) {
          return false;
        }
        glEntryTypeCollectionIdentifiers.push(glEntryTypeIdentifier);
        return true;
      });
      return [...glEntryTypesToAdd, ...glEntryTypeCollection];
    }
    return glEntryTypeCollection;
  }
}
