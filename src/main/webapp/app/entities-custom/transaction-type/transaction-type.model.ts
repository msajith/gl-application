export interface ITransactionType {
  id?: number;
  name?: string | null;
  code?: string | null;
}

export class TransactionType implements ITransactionType {
  constructor(public id?: number, public name?: string | null, public code?: string | null) {}
}

export function getTransactionTypeIdentifier(transactionType: ITransactionType): number | undefined {
  return transactionType.id;
}
