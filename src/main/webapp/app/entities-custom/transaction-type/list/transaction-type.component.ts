import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

import { ITransactionType } from '../transaction-type.model';
import { TransactionTypeService } from '../service/transaction-type.service';
import { TransactionTypeDeleteDialogComponent } from '../delete/transaction-type-delete-dialog.component';

@Component({
  selector: 'jhi-transaction-type',
  templateUrl: './transaction-type.component.html',
})
export class TransactionTypeComponent implements OnInit {
  transactionTypes?: ITransactionType[];
  isLoading = false;

  constructor(protected transactionTypeService: TransactionTypeService, protected modalService: NgbModal) {}

  loadAll(): void {
    this.isLoading = true;

    this.transactionTypeService.query().subscribe({
      next: (res: HttpResponse<ITransactionType[]>) => {
        this.isLoading = false;
        this.transactionTypes = res.body ?? [];
      },
      error: () => {
        this.isLoading = false;
      },
    });
  }

  ngOnInit(): void {
    this.loadAll();
  }

  trackId(index: number, item: ITransactionType): number {
    return item.id!;
  }

  delete(transactionType: ITransactionType): void {
    const modalRef = this.modalService.open(TransactionTypeDeleteDialogComponent, { size: 'lg', backdrop: 'static' });
    modalRef.componentInstance.transactionType = transactionType;
    // unsubscribe not needed because closed completes on modal close
    modalRef.closed.subscribe(reason => {
      if (reason === 'deleted') {
        this.loadAll();
      }
    });
  }
}
