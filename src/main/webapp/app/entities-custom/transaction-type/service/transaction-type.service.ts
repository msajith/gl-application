import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { isPresent } from 'app/core/util/operators';
import { ApplicationConfigService } from 'app/core/config/application-config.service';
import { createRequestOption } from 'app/core/request/request-util';
import { ITransactionType, getTransactionTypeIdentifier } from '../transaction-type.model';

export type EntityResponseType = HttpResponse<ITransactionType>;
export type EntityArrayResponseType = HttpResponse<ITransactionType[]>;

@Injectable({ providedIn: 'root' })
export class TransactionTypeService {
  protected resourceUrl = this.applicationConfigService.getEndpointFor('api/transaction-types');

  constructor(protected http: HttpClient, protected applicationConfigService: ApplicationConfigService) {}

  create(transactionType: ITransactionType): Observable<EntityResponseType> {
    return this.http.post<ITransactionType>(this.resourceUrl, transactionType, { observe: 'response' });
  }

  update(transactionType: ITransactionType): Observable<EntityResponseType> {
    return this.http.put<ITransactionType>(
      `${this.resourceUrl}/${getTransactionTypeIdentifier(transactionType) as number}`,
      transactionType,
      { observe: 'response' }
    );
  }

  partialUpdate(transactionType: ITransactionType): Observable<EntityResponseType> {
    return this.http.patch<ITransactionType>(
      `${this.resourceUrl}/${getTransactionTypeIdentifier(transactionType) as number}`,
      transactionType,
      { observe: 'response' }
    );
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http.get<ITransactionType>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<ITransactionType[]>(this.resourceUrl, { params: options, observe: 'response' });
  }

  delete(id: number): Observable<HttpResponse<{}>> {
    return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  addTransactionTypeToCollectionIfMissing(
    transactionTypeCollection: ITransactionType[],
    ...transactionTypesToCheck: (ITransactionType | null | undefined)[]
  ): ITransactionType[] {
    const transactionTypes: ITransactionType[] = transactionTypesToCheck.filter(isPresent);
    if (transactionTypes.length > 0) {
      const transactionTypeCollectionIdentifiers = transactionTypeCollection.map(
        transactionTypeItem => getTransactionTypeIdentifier(transactionTypeItem)!
      );
      const transactionTypesToAdd = transactionTypes.filter(transactionTypeItem => {
        const transactionTypeIdentifier = getTransactionTypeIdentifier(transactionTypeItem);
        if (transactionTypeIdentifier == null || transactionTypeCollectionIdentifiers.includes(transactionTypeIdentifier)) {
          return false;
        }
        transactionTypeCollectionIdentifiers.push(transactionTypeIdentifier);
        return true;
      });
      return [...transactionTypesToAdd, ...transactionTypeCollection];
    }
    return transactionTypeCollection;
  }
}
