import { TestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';

import { ITransactionType, TransactionType } from '../transaction-type.model';

import { TransactionTypeService } from './transaction-type.service';

describe('TransactionType Service', () => {
  let service: TransactionTypeService;
  let httpMock: HttpTestingController;
  let elemDefault: ITransactionType;
  let expectedResult: ITransactionType | ITransactionType[] | boolean | null;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
    });
    expectedResult = null;
    service = TestBed.inject(TransactionTypeService);
    httpMock = TestBed.inject(HttpTestingController);

    elemDefault = {
      id: 0,
      name: 'AAAAAAA',
      code: 'AAAAAAA',
    };
  });

  describe('Service methods', () => {
    it('should find an element', () => {
      const returnedFromService = Object.assign({}, elemDefault);

      service.find(123).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'GET' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(elemDefault);
    });

    it('should create a TransactionType', () => {
      const returnedFromService = Object.assign(
        {
          id: 0,
        },
        elemDefault
      );

      const expected = Object.assign({}, returnedFromService);

      service.create(new TransactionType()).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'POST' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(expected);
    });

    it('should update a TransactionType', () => {
      const returnedFromService = Object.assign(
        {
          id: 1,
          name: 'BBBBBB',
          code: 'BBBBBB',
        },
        elemDefault
      );

      const expected = Object.assign({}, returnedFromService);

      service.update(expected).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'PUT' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(expected);
    });

    it('should partial update a TransactionType', () => {
      const patchObject = Object.assign(
        {
          name: 'BBBBBB',
        },
        new TransactionType()
      );

      const returnedFromService = Object.assign(patchObject, elemDefault);

      const expected = Object.assign({}, returnedFromService);

      service.partialUpdate(patchObject).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'PATCH' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(expected);
    });

    it('should return a list of TransactionType', () => {
      const returnedFromService = Object.assign(
        {
          id: 1,
          name: 'BBBBBB',
          code: 'BBBBBB',
        },
        elemDefault
      );

      const expected = Object.assign({}, returnedFromService);

      service.query().subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'GET' });
      req.flush([returnedFromService]);
      httpMock.verify();
      expect(expectedResult).toContainEqual(expected);
    });

    it('should delete a TransactionType', () => {
      service.delete(123).subscribe(resp => (expectedResult = resp.ok));

      const req = httpMock.expectOne({ method: 'DELETE' });
      req.flush({ status: 200 });
      expect(expectedResult);
    });

    describe('addTransactionTypeToCollectionIfMissing', () => {
      it('should add a TransactionType to an empty array', () => {
        const transactionType: ITransactionType = { id: 123 };
        expectedResult = service.addTransactionTypeToCollectionIfMissing([], transactionType);
        expect(expectedResult).toHaveLength(1);
        expect(expectedResult).toContain(transactionType);
      });

      it('should not add a TransactionType to an array that contains it', () => {
        const transactionType: ITransactionType = { id: 123 };
        const transactionTypeCollection: ITransactionType[] = [
          {
            ...transactionType,
          },
          { id: 456 },
        ];
        expectedResult = service.addTransactionTypeToCollectionIfMissing(transactionTypeCollection, transactionType);
        expect(expectedResult).toHaveLength(2);
      });

      it("should add a TransactionType to an array that doesn't contain it", () => {
        const transactionType: ITransactionType = { id: 123 };
        const transactionTypeCollection: ITransactionType[] = [{ id: 456 }];
        expectedResult = service.addTransactionTypeToCollectionIfMissing(transactionTypeCollection, transactionType);
        expect(expectedResult).toHaveLength(2);
        expect(expectedResult).toContain(transactionType);
      });

      it('should add only unique TransactionType to an array', () => {
        const transactionTypeArray: ITransactionType[] = [{ id: 123 }, { id: 456 }, { id: 96581 }];
        const transactionTypeCollection: ITransactionType[] = [{ id: 123 }];
        expectedResult = service.addTransactionTypeToCollectionIfMissing(transactionTypeCollection, ...transactionTypeArray);
        expect(expectedResult).toHaveLength(3);
      });

      it('should accept varargs', () => {
        const transactionType: ITransactionType = { id: 123 };
        const transactionType2: ITransactionType = { id: 456 };
        expectedResult = service.addTransactionTypeToCollectionIfMissing([], transactionType, transactionType2);
        expect(expectedResult).toHaveLength(2);
        expect(expectedResult).toContain(transactionType);
        expect(expectedResult).toContain(transactionType2);
      });

      it('should accept null and undefined values', () => {
        const transactionType: ITransactionType = { id: 123 };
        expectedResult = service.addTransactionTypeToCollectionIfMissing([], null, transactionType, undefined);
        expect(expectedResult).toHaveLength(1);
        expect(expectedResult).toContain(transactionType);
      });

      it('should return initial array if no TransactionType is added', () => {
        const transactionTypeCollection: ITransactionType[] = [{ id: 123 }];
        expectedResult = service.addTransactionTypeToCollectionIfMissing(transactionTypeCollection, undefined, null);
        expect(expectedResult).toEqual(transactionTypeCollection);
      });
    });
  });

  afterEach(() => {
    httpMock.verify();
  });
});
