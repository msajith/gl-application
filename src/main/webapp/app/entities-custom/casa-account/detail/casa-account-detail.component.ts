import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { ICasaAccount } from '../casa-account.model';
import { AccountStatus } from 'app/entities/enumerations/account-status.model';

@Component({
  selector: 'jhi-casa-account-detail',
  templateUrl: './casa-account-detail.component.html',
})
export class CasaAccountDetailComponent implements OnInit {
  public AccountStatus = AccountStatus;
  casaAccount: ICasaAccount | null = null;
  public acStatus: AccountStatus | null = null;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ casaAccount }) => {
      this.casaAccount = casaAccount;
      this.acStatus = casaAccount.accountStatus != null ? AccountStatus[casaAccount.accountStatus as AccountStatus] : null;
    });
  }

  previousState(): void {
    window.history.back();
  }
}
