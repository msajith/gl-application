import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { CasaAccountDetailComponent } from './casa-account-detail.component';

describe('CasaAccount Management Detail Component', () => {
  let comp: CasaAccountDetailComponent;
  let fixture: ComponentFixture<CasaAccountDetailComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [CasaAccountDetailComponent],
      providers: [
        {
          provide: ActivatedRoute,
          useValue: { data: of({ casaAccount: { id: 123 } }) },
        },
      ],
    })
      .overrideTemplate(CasaAccountDetailComponent, '')
      .compileComponents();
    fixture = TestBed.createComponent(CasaAccountDetailComponent);
    comp = fixture.componentInstance;
  });

  describe('OnInit', () => {
    it('Should load casaAccount on init', () => {
      // WHEN
      comp.ngOnInit();

      // THEN
      expect(comp.casaAccount).toEqual(expect.objectContaining({ id: 123 }));
    });
  });
});
