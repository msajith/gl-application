import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { finalize, map } from 'rxjs/operators';

import dayjs from 'dayjs/esm';
import { DATE_TIME_FORMAT } from 'app/config/input.constants';

import { ICasaAccount, CasaAccount } from '../casa-account.model';
import { CasaAccountService } from '../service/casa-account.service';
import { IAccountType } from 'app/entities/account-type/account-type.model';
import { AccountTypeService } from 'app/entities/account-type/service/account-type.service';
import { IAccountCategory } from 'app/entities/account-category/account-category.model';
import { AccountCategoryService } from 'app/entities/account-category/service/account-category.service';
import { ICurrency } from 'app/entities/currency/currency.model';
import { CurrencyService } from 'app/entities/currency/service/currency.service';
import { ICustomer } from 'app/entities/customer/customer.model';
import { CustomerService } from 'app/entities/customer/service/customer.service';
import { AccountStatus } from 'app/entities/enumerations/account-status.model';

@Component({
  selector: 'jhi-casa-account-update',
  templateUrl: './casa-account-update.component.html',
})
export class CasaAccountUpdateComponent implements OnInit {
  isSaving = false;
  accountStatusValues = Object.keys(AccountStatus);

  accountTypesSharedCollection: IAccountType[] = [];
  accountCategoriesSharedCollection: IAccountCategory[] = [];
  currenciesSharedCollection: ICurrency[] = [];
  customersSharedCollection: ICustomer[] = [];

  editForm = this.fb.group({
    id: [],
    accountNo: [null, [Validators.required]],
    accountName: [null, [Validators.required]],
    date: [],
    closingBalance: [],
    openingBalance: [],
    totalDebit: [],
    totalCredit: [],
    availableBalance: [],
    currentBalance: [],
    createdOn: [],
    createdBy: [],
    accountStatus: [],
    accountType: [],
    accountCategory: [],
    accountCurrency: [],
    customer: [],
  });

  constructor(
    protected casaAccountService: CasaAccountService,
    protected accountTypeService: AccountTypeService,
    protected accountCategoryService: AccountCategoryService,
    protected currencyService: CurrencyService,
    protected customerService: CustomerService,
    protected activatedRoute: ActivatedRoute,
    protected fb: FormBuilder
  ) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ casaAccount }) => {
      if (casaAccount.id === undefined) {
        const today = dayjs().startOf('day');
        casaAccount.createdOn = today;
      }

      this.updateForm(casaAccount);

      this.loadRelationshipsOptions();
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const casaAccount = this.createFromForm();
    if (casaAccount.id !== undefined) {
      this.subscribeToSaveResponse(this.casaAccountService.update(casaAccount));
    } else {
      this.subscribeToSaveResponse(this.casaAccountService.create(casaAccount));
    }
  }

  trackAccountTypeById(index: number, item: IAccountType): number {
    return item.id!;
  }

  trackAccountCategoryById(index: number, item: IAccountCategory): number {
    return item.id!;
  }

  trackCurrencyById(index: number, item: ICurrency): number {
    return item.id!;
  }

  trackCustomerById(index: number, item: ICustomer): number {
    return item.id!;
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<ICasaAccount>>): void {
    result.pipe(finalize(() => this.onSaveFinalize())).subscribe({
      next: () => this.onSaveSuccess(),
      error: () => this.onSaveError(),
    });
  }

  protected onSaveSuccess(): void {
    this.previousState();
  }

  protected onSaveError(): void {
    // Api for inheritance.
  }

  protected onSaveFinalize(): void {
    this.isSaving = false;
  }

  protected updateForm(casaAccount: ICasaAccount): void {
    this.editForm.patchValue({
      id: casaAccount.id,
      accountNo: casaAccount.accountNo,
      accountName: casaAccount.accountName,
      date: casaAccount.date,
      closingBalance: casaAccount.closingBalance,
      openingBalance: casaAccount.openingBalance,
      totalDebit: casaAccount.totalDebit,
      totalCredit: casaAccount.totalCredit,
      availableBalance: casaAccount.availableBalance,
      currentBalance: casaAccount.currentBalance,
      createdOn: casaAccount.createdOn ? casaAccount.createdOn.format(DATE_TIME_FORMAT) : null,
      createdBy: casaAccount.createdBy,
      accountStatus: casaAccount.accountStatus,
      accountType: casaAccount.accountType,
      accountCategory: casaAccount.accountCategory,
      accountCurrency: casaAccount.accountCurrency,
      customer: casaAccount.customer,
    });

    this.accountTypesSharedCollection = this.accountTypeService.addAccountTypeToCollectionIfMissing(
      this.accountTypesSharedCollection,
      casaAccount.accountType
    );
    this.accountCategoriesSharedCollection = this.accountCategoryService.addAccountCategoryToCollectionIfMissing(
      this.accountCategoriesSharedCollection,
      casaAccount.accountCategory
    );
    this.currenciesSharedCollection = this.currencyService.addCurrencyToCollectionIfMissing(
      this.currenciesSharedCollection,
      casaAccount.accountCurrency
    );
    this.customersSharedCollection = this.customerService.addCustomerToCollectionIfMissing(
      this.customersSharedCollection,
      casaAccount.customer
    );
  }

  protected loadRelationshipsOptions(): void {
    this.accountTypeService
      .query()
      .pipe(map((res: HttpResponse<IAccountType[]>) => res.body ?? []))
      .pipe(
        map((accountTypes: IAccountType[]) =>
          this.accountTypeService.addAccountTypeToCollectionIfMissing(accountTypes, this.editForm.get('accountType')!.value)
        )
      )
      .subscribe((accountTypes: IAccountType[]) => (this.accountTypesSharedCollection = accountTypes));

    this.accountCategoryService
      .query()
      .pipe(map((res: HttpResponse<IAccountCategory[]>) => res.body ?? []))
      .pipe(
        map((accountCategories: IAccountCategory[]) =>
          this.accountCategoryService.addAccountCategoryToCollectionIfMissing(
            accountCategories,
            this.editForm.get('accountCategory')!.value
          )
        )
      )
      .subscribe((accountCategories: IAccountCategory[]) => (this.accountCategoriesSharedCollection = accountCategories));

    this.currencyService
      .query()
      .pipe(map((res: HttpResponse<ICurrency[]>) => res.body ?? []))
      .pipe(
        map((currencies: ICurrency[]) =>
          this.currencyService.addCurrencyToCollectionIfMissing(currencies, this.editForm.get('accountCurrency')!.value)
        )
      )
      .subscribe((currencies: ICurrency[]) => (this.currenciesSharedCollection = currencies));

    this.customerService
      .query()
      .pipe(map((res: HttpResponse<ICustomer[]>) => res.body ?? []))
      .pipe(
        map((customers: ICustomer[]) =>
          this.customerService.addCustomerToCollectionIfMissing(customers, this.editForm.get('customer')!.value)
        )
      )
      .subscribe((customers: ICustomer[]) => (this.customersSharedCollection = customers));
  }

  protected createFromForm(): ICasaAccount {
    return {
      ...new CasaAccount(),
      id: this.editForm.get(['id'])!.value,
      accountNo: this.editForm.get(['accountNo'])!.value,
      accountName: this.editForm.get(['accountName'])!.value,
      date: this.editForm.get(['date'])!.value,
      closingBalance: this.editForm.get(['closingBalance'])!.value,
      openingBalance: this.editForm.get(['openingBalance'])!.value,
      totalDebit: this.editForm.get(['totalDebit'])!.value,
      totalCredit: this.editForm.get(['totalCredit'])!.value,
      availableBalance: this.editForm.get(['availableBalance'])!.value,
      currentBalance: this.editForm.get(['currentBalance'])!.value,
      createdOn: this.editForm.get(['createdOn'])!.value ? dayjs(this.editForm.get(['createdOn'])!.value, DATE_TIME_FORMAT) : undefined,
      createdBy: this.editForm.get(['createdBy'])!.value,
      accountStatus: this.editForm.get(['accountStatus'])!.value,
      accountType: this.editForm.get(['accountType'])!.value,
      accountCategory: this.editForm.get(['accountCategory'])!.value,
      accountCurrency: this.editForm.get(['accountCurrency'])!.value,
      customer: this.editForm.get(['customer'])!.value,
    };
  }
}
