import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { UserRouteAccessService } from 'app/core/auth/user-route-access.service';
import { CasaAccountComponent } from '../list/casa-account.component';
import { CasaAccountDetailComponent } from '../detail/casa-account-detail.component';
import { CasaAccountUpdateComponent } from '../update/casa-account-update.component';
import { CasaAccountRoutingResolveService } from './casa-account-routing-resolve.service';

const casaAccountRoute: Routes = [
  {
    path: '',
    component: CasaAccountComponent,
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/view',
    component: CasaAccountDetailComponent,
    resolve: {
      casaAccount: CasaAccountRoutingResolveService,
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: 'new',
    component: CasaAccountUpdateComponent,
    resolve: {
      casaAccount: CasaAccountRoutingResolveService,
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/edit',
    component: CasaAccountUpdateComponent,
    resolve: {
      casaAccount: CasaAccountRoutingResolveService,
    },
    canActivate: [UserRouteAccessService],
  },
];

@NgModule({
  imports: [RouterModule.forChild(casaAccountRoute)],
  exports: [RouterModule],
})
export class CasaAccountRoutingModule {}
