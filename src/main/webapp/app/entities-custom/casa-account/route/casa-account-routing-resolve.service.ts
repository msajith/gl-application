import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, Router } from '@angular/router';
import { Observable, of, EMPTY } from 'rxjs';
import { mergeMap } from 'rxjs/operators';

import { ICasaAccount, CasaAccount } from '../casa-account.model';
import { CasaAccountService } from '../service/casa-account.service';

@Injectable({ providedIn: 'root' })
export class CasaAccountRoutingResolveService implements Resolve<ICasaAccount> {
  constructor(protected service: CasaAccountService, protected router: Router) {}

  resolve(route: ActivatedRouteSnapshot): Observable<ICasaAccount> | Observable<never> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        mergeMap((casaAccount: HttpResponse<CasaAccount>) => {
          if (casaAccount.body) {
            return of(casaAccount.body);
          } else {
            this.router.navigate(['404']);
            return EMPTY;
          }
        })
      );
    }
    return of(new CasaAccount());
  }
}
