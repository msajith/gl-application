import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import dayjs from 'dayjs/esm';

import { isPresent } from 'app/core/util/operators';
import { DATE_FORMAT } from 'app/config/input.constants';
import { ApplicationConfigService } from 'app/core/config/application-config.service';
import { createRequestOption } from 'app/core/request/request-util';
import { ICasaAccount, getCasaAccountIdentifier } from '../casa-account.model';

export type EntityResponseType = HttpResponse<ICasaAccount>;
export type EntityArrayResponseType = HttpResponse<ICasaAccount[]>;

@Injectable({ providedIn: 'root' })
export class CasaAccountService {
  protected resourceUrl = this.applicationConfigService.getEndpointFor('api/casa-accounts');
  protected resourceUrlV1 = this.applicationConfigService.getEndpointFor('api/v1/casa-accounts/customer');

  constructor(protected http: HttpClient, protected applicationConfigService: ApplicationConfigService) {}

  create(casaAccount: ICasaAccount): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(casaAccount);
    return this.http
      .post<ICasaAccount>(this.resourceUrl, copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  update(casaAccount: ICasaAccount): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(casaAccount);
    return this.http
      .put<ICasaAccount>(`${this.resourceUrl}/${getCasaAccountIdentifier(casaAccount) as number}`, copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  partialUpdate(casaAccount: ICasaAccount): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(casaAccount);
    return this.http
      .patch<ICasaAccount>(`${this.resourceUrl}/${getCasaAccountIdentifier(casaAccount) as number}`, copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http
      .get<ICasaAccount>(`${this.resourceUrl}/${id}`, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http
      .get<ICasaAccount[]>(this.resourceUrl, { params: options, observe: 'response' })
      .pipe(map((res: EntityArrayResponseType) => this.convertDateArrayFromServer(res)));
  }
  queryByCustomerId(id: number): Observable<EntityArrayResponseType> {
    return this.http
      .get<ICasaAccount[]>(`${this.resourceUrlV1}/${id}`, { observe: 'response' })
      .pipe(map((res: EntityArrayResponseType) => this.convertDateArrayFromServer(res)));
  }

  delete(id: number): Observable<HttpResponse<{}>> {
    return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  addCasaAccountToCollectionIfMissing(
    casaAccountCollection: ICasaAccount[],
    ...casaAccountsToCheck: (ICasaAccount | null | undefined)[]
  ): ICasaAccount[] {
    const casaAccounts: ICasaAccount[] = casaAccountsToCheck.filter(isPresent);
    if (casaAccounts.length > 0) {
      const casaAccountCollectionIdentifiers = casaAccountCollection.map(casaAccountItem => getCasaAccountIdentifier(casaAccountItem)!);
      const casaAccountsToAdd = casaAccounts.filter(casaAccountItem => {
        const casaAccountIdentifier = getCasaAccountIdentifier(casaAccountItem);
        if (casaAccountIdentifier == null || casaAccountCollectionIdentifiers.includes(casaAccountIdentifier)) {
          return false;
        }
        casaAccountCollectionIdentifiers.push(casaAccountIdentifier);
        return true;
      });
      return [...casaAccountsToAdd, ...casaAccountCollection];
    }
    return casaAccountCollection;
  }

  protected convertDateFromClient(casaAccount: ICasaAccount): ICasaAccount {
    return Object.assign({}, casaAccount, {
      date: casaAccount.date?.isValid() ? casaAccount.date.format(DATE_FORMAT) : undefined,
      createdOn: casaAccount.createdOn?.isValid() ? casaAccount.createdOn.toJSON() : undefined,
    });
  }

  protected convertDateFromServer(res: EntityResponseType): EntityResponseType {
    if (res.body) {
      res.body.date = res.body.date ? dayjs(res.body.date) : undefined;
      res.body.createdOn = res.body.createdOn ? dayjs(res.body.createdOn) : undefined;
    }
    return res;
  }

  protected convertDateArrayFromServer(res: EntityArrayResponseType): EntityArrayResponseType {
    if (res.body) {
      res.body.forEach((casaAccount: ICasaAccount) => {
        casaAccount.date = casaAccount.date ? dayjs(casaAccount.date) : undefined;
        casaAccount.createdOn = casaAccount.createdOn ? dayjs(casaAccount.createdOn) : undefined;
      });
    }
    return res;
  }
}
