import { TestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import dayjs from 'dayjs/esm';

import { DATE_FORMAT, DATE_TIME_FORMAT } from 'app/config/input.constants';
import { AccountStatus } from 'app/entities/enumerations/account-status.model';
import { ICasaAccount, CasaAccount } from '../casa-account.model';

import { CasaAccountService } from './casa-account.service';

describe('CasaAccount Service', () => {
  let service: CasaAccountService;
  let httpMock: HttpTestingController;
  let elemDefault: ICasaAccount;
  let expectedResult: ICasaAccount | ICasaAccount[] | boolean | null;
  let currentDate: dayjs.Dayjs;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
    });
    expectedResult = null;
    service = TestBed.inject(CasaAccountService);
    httpMock = TestBed.inject(HttpTestingController);
    currentDate = dayjs();

    elemDefault = {
      id: 0,
      accountNo: 'AAAAAAA',
      accountName: 'AAAAAAA',
      date: currentDate,
      closingBalance: 0,
      openingBalance: 0,
      totalDebit: 0,
      totalCredit: 0,
      availableBalance: 0,
      currentBalance: 0,
      createdOn: currentDate,
      createdBy: 'AAAAAAA',
      accountStatus: AccountStatus.ACTIVE,
    };
  });

  describe('Service methods', () => {
    it('should find an element', () => {
      const returnedFromService = Object.assign(
        {
          date: currentDate.format(DATE_FORMAT),
          createdOn: currentDate.format(DATE_TIME_FORMAT),
        },
        elemDefault
      );

      service.find(123).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'GET' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(elemDefault);
    });

    it('should create a CasaAccount', () => {
      const returnedFromService = Object.assign(
        {
          id: 0,
          date: currentDate.format(DATE_FORMAT),
          createdOn: currentDate.format(DATE_TIME_FORMAT),
        },
        elemDefault
      );

      const expected = Object.assign(
        {
          date: currentDate,
          createdOn: currentDate,
        },
        returnedFromService
      );

      service.create(new CasaAccount()).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'POST' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(expected);
    });

    it('should update a CasaAccount', () => {
      const returnedFromService = Object.assign(
        {
          id: 1,
          accountNo: 'BBBBBB',
          accountName: 'BBBBBB',
          date: currentDate.format(DATE_FORMAT),
          closingBalance: 1,
          openingBalance: 1,
          totalDebit: 1,
          totalCredit: 1,
          availableBalance: 1,
          currentBalance: 1,
          createdOn: currentDate.format(DATE_TIME_FORMAT),
          createdBy: 'BBBBBB',
          accountStatus: 'BBBBBB',
        },
        elemDefault
      );

      const expected = Object.assign(
        {
          date: currentDate,
          createdOn: currentDate,
        },
        returnedFromService
      );

      service.update(expected).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'PUT' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(expected);
    });

    it('should partial update a CasaAccount', () => {
      const patchObject = Object.assign(
        {
          accountName: 'BBBBBB',
          date: currentDate.format(DATE_FORMAT),
          openingBalance: 1,
          totalDebit: 1,
          availableBalance: 1,
          createdBy: 'BBBBBB',
        },
        new CasaAccount()
      );

      const returnedFromService = Object.assign(patchObject, elemDefault);

      const expected = Object.assign(
        {
          date: currentDate,
          createdOn: currentDate,
        },
        returnedFromService
      );

      service.partialUpdate(patchObject).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'PATCH' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(expected);
    });

    it('should return a list of CasaAccount', () => {
      const returnedFromService = Object.assign(
        {
          id: 1,
          accountNo: 'BBBBBB',
          accountName: 'BBBBBB',
          date: currentDate.format(DATE_FORMAT),
          closingBalance: 1,
          openingBalance: 1,
          totalDebit: 1,
          totalCredit: 1,
          availableBalance: 1,
          currentBalance: 1,
          createdOn: currentDate.format(DATE_TIME_FORMAT),
          createdBy: 'BBBBBB',
          accountStatus: 'BBBBBB',
        },
        elemDefault
      );

      const expected = Object.assign(
        {
          date: currentDate,
          createdOn: currentDate,
        },
        returnedFromService
      );

      service.query().subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'GET' });
      req.flush([returnedFromService]);
      httpMock.verify();
      expect(expectedResult).toContainEqual(expected);
    });

    it('should delete a CasaAccount', () => {
      service.delete(123).subscribe(resp => (expectedResult = resp.ok));

      const req = httpMock.expectOne({ method: 'DELETE' });
      req.flush({ status: 200 });
      expect(expectedResult);
    });

    describe('addCasaAccountToCollectionIfMissing', () => {
      it('should add a CasaAccount to an empty array', () => {
        const casaAccount: ICasaAccount = { id: 123 };
        expectedResult = service.addCasaAccountToCollectionIfMissing([], casaAccount);
        expect(expectedResult).toHaveLength(1);
        expect(expectedResult).toContain(casaAccount);
      });

      it('should not add a CasaAccount to an array that contains it', () => {
        const casaAccount: ICasaAccount = { id: 123 };
        const casaAccountCollection: ICasaAccount[] = [
          {
            ...casaAccount,
          },
          { id: 456 },
        ];
        expectedResult = service.addCasaAccountToCollectionIfMissing(casaAccountCollection, casaAccount);
        expect(expectedResult).toHaveLength(2);
      });

      it("should add a CasaAccount to an array that doesn't contain it", () => {
        const casaAccount: ICasaAccount = { id: 123 };
        const casaAccountCollection: ICasaAccount[] = [{ id: 456 }];
        expectedResult = service.addCasaAccountToCollectionIfMissing(casaAccountCollection, casaAccount);
        expect(expectedResult).toHaveLength(2);
        expect(expectedResult).toContain(casaAccount);
      });

      it('should add only unique CasaAccount to an array', () => {
        const casaAccountArray: ICasaAccount[] = [{ id: 123 }, { id: 456 }, { id: 43437 }];
        const casaAccountCollection: ICasaAccount[] = [{ id: 123 }];
        expectedResult = service.addCasaAccountToCollectionIfMissing(casaAccountCollection, ...casaAccountArray);
        expect(expectedResult).toHaveLength(3);
      });

      it('should accept varargs', () => {
        const casaAccount: ICasaAccount = { id: 123 };
        const casaAccount2: ICasaAccount = { id: 456 };
        expectedResult = service.addCasaAccountToCollectionIfMissing([], casaAccount, casaAccount2);
        expect(expectedResult).toHaveLength(2);
        expect(expectedResult).toContain(casaAccount);
        expect(expectedResult).toContain(casaAccount2);
      });

      it('should accept null and undefined values', () => {
        const casaAccount: ICasaAccount = { id: 123 };
        expectedResult = service.addCasaAccountToCollectionIfMissing([], null, casaAccount, undefined);
        expect(expectedResult).toHaveLength(1);
        expect(expectedResult).toContain(casaAccount);
      });

      it('should return initial array if no CasaAccount is added', () => {
        const casaAccountCollection: ICasaAccount[] = [{ id: 123 }];
        expectedResult = service.addCasaAccountToCollectionIfMissing(casaAccountCollection, undefined, null);
        expect(expectedResult).toEqual(casaAccountCollection);
      });
    });
  });

  afterEach(() => {
    httpMock.verify();
  });
});
