import { Component } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

import { ICasaAccount } from '../casa-account.model';
import { CasaAccountService } from '../service/casa-account.service';

@Component({
  templateUrl: './casa-account-delete-dialog.component.html',
})
export class CasaAccountDeleteDialogComponent {
  casaAccount?: ICasaAccount;

  constructor(protected casaAccountService: CasaAccountService, protected activeModal: NgbActiveModal) {}

  cancel(): void {
    this.activeModal.dismiss();
  }

  confirmDelete(id: number): void {
    this.casaAccountService.delete(id).subscribe(() => {
      this.activeModal.close('deleted');
    });
  }
}
