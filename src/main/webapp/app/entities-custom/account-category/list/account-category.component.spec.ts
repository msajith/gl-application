import { ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpHeaders, HttpResponse } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { of } from 'rxjs';

import { AccountCategoryService } from '../service/account-category.service';

import { AccountCategoryComponent } from './account-category.component';

describe('AccountCategory Management Component', () => {
  let comp: AccountCategoryComponent;
  let fixture: ComponentFixture<AccountCategoryComponent>;
  let service: AccountCategoryService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      declarations: [AccountCategoryComponent],
    })
      .overrideTemplate(AccountCategoryComponent, '')
      .compileComponents();

    fixture = TestBed.createComponent(AccountCategoryComponent);
    comp = fixture.componentInstance;
    service = TestBed.inject(AccountCategoryService);

    const headers = new HttpHeaders();
    jest.spyOn(service, 'query').mockReturnValue(
      of(
        new HttpResponse({
          body: [{ id: 123 }],
          headers,
        })
      )
    );
  });

  it('Should call load all on init', () => {
    // WHEN
    comp.ngOnInit();

    // THEN
    expect(service.query).toHaveBeenCalled();
    expect(comp.accountCategories?.[0]).toEqual(expect.objectContaining({ id: 123 }));
  });
});
