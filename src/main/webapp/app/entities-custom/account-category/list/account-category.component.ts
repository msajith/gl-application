import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

import { IAccountCategory } from '../account-category.model';
import { AccountCategoryService } from '../service/account-category.service';
import { AccountCategoryDeleteDialogComponent } from '../delete/account-category-delete-dialog.component';

@Component({
  selector: 'jhi-account-category',
  templateUrl: './account-category.component.html',
})
export class AccountCategoryComponent implements OnInit {
  accountCategories?: IAccountCategory[];
  isLoading = false;

  constructor(protected accountCategoryService: AccountCategoryService, protected modalService: NgbModal) {}

  loadAll(): void {
    this.isLoading = true;

    this.accountCategoryService.query().subscribe({
      next: (res: HttpResponse<IAccountCategory[]>) => {
        this.isLoading = false;
        this.accountCategories = res.body ?? [];
      },
      error: () => {
        this.isLoading = false;
      },
    });
  }

  ngOnInit(): void {
    this.loadAll();
  }

  trackId(index: number, item: IAccountCategory): number {
    return item.id!;
  }

  delete(accountCategory: IAccountCategory): void {
    const modalRef = this.modalService.open(AccountCategoryDeleteDialogComponent, { size: 'lg', backdrop: 'static' });
    modalRef.componentInstance.accountCategory = accountCategory;
    // unsubscribe not needed because closed completes on modal close
    modalRef.closed.subscribe(reason => {
      if (reason === 'deleted') {
        this.loadAll();
      }
    });
  }
}
