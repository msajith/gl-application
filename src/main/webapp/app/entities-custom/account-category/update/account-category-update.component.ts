import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { finalize } from 'rxjs/operators';

import { IAccountCategory, AccountCategory } from '../account-category.model';
import { AccountCategoryService } from '../service/account-category.service';

@Component({
  selector: 'jhi-account-category-update',
  templateUrl: './account-category-update.component.html',
})
export class AccountCategoryUpdateComponent implements OnInit {
  isSaving = false;

  editForm = this.fb.group({
    id: [],
    name: [],
    code: [],
  });

  constructor(
    protected accountCategoryService: AccountCategoryService,
    protected activatedRoute: ActivatedRoute,
    protected fb: FormBuilder
  ) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ accountCategory }) => {
      this.updateForm(accountCategory);
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const accountCategory = this.createFromForm();
    if (accountCategory.id !== undefined) {
      this.subscribeToSaveResponse(this.accountCategoryService.update(accountCategory));
    } else {
      this.subscribeToSaveResponse(this.accountCategoryService.create(accountCategory));
    }
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IAccountCategory>>): void {
    result.pipe(finalize(() => this.onSaveFinalize())).subscribe({
      next: () => this.onSaveSuccess(),
      error: () => this.onSaveError(),
    });
  }

  protected onSaveSuccess(): void {
    this.previousState();
  }

  protected onSaveError(): void {
    // Api for inheritance.
  }

  protected onSaveFinalize(): void {
    this.isSaving = false;
  }

  protected updateForm(accountCategory: IAccountCategory): void {
    this.editForm.patchValue({
      id: accountCategory.id,
      name: accountCategory.name,
      code: accountCategory.code,
    });
  }

  protected createFromForm(): IAccountCategory {
    return {
      ...new AccountCategory(),
      id: this.editForm.get(['id'])!.value,
      name: this.editForm.get(['name'])!.value,
      code: this.editForm.get(['code'])!.value,
    };
  }
}
