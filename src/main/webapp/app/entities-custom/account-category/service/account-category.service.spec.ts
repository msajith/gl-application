import { TestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';

import { IAccountCategory, AccountCategory } from '../account-category.model';

import { AccountCategoryService } from './account-category.service';

describe('AccountCategory Service', () => {
  let service: AccountCategoryService;
  let httpMock: HttpTestingController;
  let elemDefault: IAccountCategory;
  let expectedResult: IAccountCategory | IAccountCategory[] | boolean | null;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
    });
    expectedResult = null;
    service = TestBed.inject(AccountCategoryService);
    httpMock = TestBed.inject(HttpTestingController);

    elemDefault = {
      id: 0,
      name: 'AAAAAAA',
      code: 'AAAAAAA',
    };
  });

  describe('Service methods', () => {
    it('should find an element', () => {
      const returnedFromService = Object.assign({}, elemDefault);

      service.find(123).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'GET' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(elemDefault);
    });

    it('should create a AccountCategory', () => {
      const returnedFromService = Object.assign(
        {
          id: 0,
        },
        elemDefault
      );

      const expected = Object.assign({}, returnedFromService);

      service.create(new AccountCategory()).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'POST' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(expected);
    });

    it('should update a AccountCategory', () => {
      const returnedFromService = Object.assign(
        {
          id: 1,
          name: 'BBBBBB',
          code: 'BBBBBB',
        },
        elemDefault
      );

      const expected = Object.assign({}, returnedFromService);

      service.update(expected).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'PUT' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(expected);
    });

    it('should partial update a AccountCategory', () => {
      const patchObject = Object.assign(
        {
          code: 'BBBBBB',
        },
        new AccountCategory()
      );

      const returnedFromService = Object.assign(patchObject, elemDefault);

      const expected = Object.assign({}, returnedFromService);

      service.partialUpdate(patchObject).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'PATCH' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(expected);
    });

    it('should return a list of AccountCategory', () => {
      const returnedFromService = Object.assign(
        {
          id: 1,
          name: 'BBBBBB',
          code: 'BBBBBB',
        },
        elemDefault
      );

      const expected = Object.assign({}, returnedFromService);

      service.query().subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'GET' });
      req.flush([returnedFromService]);
      httpMock.verify();
      expect(expectedResult).toContainEqual(expected);
    });

    it('should delete a AccountCategory', () => {
      service.delete(123).subscribe(resp => (expectedResult = resp.ok));

      const req = httpMock.expectOne({ method: 'DELETE' });
      req.flush({ status: 200 });
      expect(expectedResult);
    });

    describe('addAccountCategoryToCollectionIfMissing', () => {
      it('should add a AccountCategory to an empty array', () => {
        const accountCategory: IAccountCategory = { id: 123 };
        expectedResult = service.addAccountCategoryToCollectionIfMissing([], accountCategory);
        expect(expectedResult).toHaveLength(1);
        expect(expectedResult).toContain(accountCategory);
      });

      it('should not add a AccountCategory to an array that contains it', () => {
        const accountCategory: IAccountCategory = { id: 123 };
        const accountCategoryCollection: IAccountCategory[] = [
          {
            ...accountCategory,
          },
          { id: 456 },
        ];
        expectedResult = service.addAccountCategoryToCollectionIfMissing(accountCategoryCollection, accountCategory);
        expect(expectedResult).toHaveLength(2);
      });

      it("should add a AccountCategory to an array that doesn't contain it", () => {
        const accountCategory: IAccountCategory = { id: 123 };
        const accountCategoryCollection: IAccountCategory[] = [{ id: 456 }];
        expectedResult = service.addAccountCategoryToCollectionIfMissing(accountCategoryCollection, accountCategory);
        expect(expectedResult).toHaveLength(2);
        expect(expectedResult).toContain(accountCategory);
      });

      it('should add only unique AccountCategory to an array', () => {
        const accountCategoryArray: IAccountCategory[] = [{ id: 123 }, { id: 456 }, { id: 58928 }];
        const accountCategoryCollection: IAccountCategory[] = [{ id: 123 }];
        expectedResult = service.addAccountCategoryToCollectionIfMissing(accountCategoryCollection, ...accountCategoryArray);
        expect(expectedResult).toHaveLength(3);
      });

      it('should accept varargs', () => {
        const accountCategory: IAccountCategory = { id: 123 };
        const accountCategory2: IAccountCategory = { id: 456 };
        expectedResult = service.addAccountCategoryToCollectionIfMissing([], accountCategory, accountCategory2);
        expect(expectedResult).toHaveLength(2);
        expect(expectedResult).toContain(accountCategory);
        expect(expectedResult).toContain(accountCategory2);
      });

      it('should accept null and undefined values', () => {
        const accountCategory: IAccountCategory = { id: 123 };
        expectedResult = service.addAccountCategoryToCollectionIfMissing([], null, accountCategory, undefined);
        expect(expectedResult).toHaveLength(1);
        expect(expectedResult).toContain(accountCategory);
      });

      it('should return initial array if no AccountCategory is added', () => {
        const accountCategoryCollection: IAccountCategory[] = [{ id: 123 }];
        expectedResult = service.addAccountCategoryToCollectionIfMissing(accountCategoryCollection, undefined, null);
        expect(expectedResult).toEqual(accountCategoryCollection);
      });
    });
  });

  afterEach(() => {
    httpMock.verify();
  });
});
