import { Component } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

import { IAccountCategory } from '../account-category.model';
import { AccountCategoryService } from '../service/account-category.service';

@Component({
  templateUrl: './account-category-delete-dialog.component.html',
})
export class AccountCategoryDeleteDialogComponent {
  accountCategory?: IAccountCategory;

  constructor(protected accountCategoryService: AccountCategoryService, protected activeModal: NgbActiveModal) {}

  cancel(): void {
    this.activeModal.dismiss();
  }

  confirmDelete(id: number): void {
    this.accountCategoryService.delete(id).subscribe(() => {
      this.activeModal.close('deleted');
    });
  }
}
