import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IWalletAccount } from '../wallet-account.model';

@Component({
  selector: 'jhi-wallet-account-detail',
  templateUrl: './wallet-account-detail.component.html',
})
export class WalletAccountDetailComponent implements OnInit {
  walletAccount: IWalletAccount | null = null;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ walletAccount }) => {
      this.walletAccount = walletAccount;
    });
  }

  previousState(): void {
    window.history.back();
  }
}
