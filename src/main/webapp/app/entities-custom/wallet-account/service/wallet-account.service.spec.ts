import { TestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import dayjs from 'dayjs/esm';

import { DATE_FORMAT, DATE_TIME_FORMAT } from 'app/config/input.constants';
import { AccountStatus } from 'app/entities/enumerations/account-status.model';
import { IWalletAccount, WalletAccount } from '../wallet-account.model';

import { WalletAccountService } from './wallet-account.service';

describe('WalletAccount Service', () => {
  let service: WalletAccountService;
  let httpMock: HttpTestingController;
  let elemDefault: IWalletAccount;
  let expectedResult: IWalletAccount | IWalletAccount[] | boolean | null;
  let currentDate: dayjs.Dayjs;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
    });
    expectedResult = null;
    service = TestBed.inject(WalletAccountService);
    httpMock = TestBed.inject(HttpTestingController);
    currentDate = dayjs();

    elemDefault = {
      id: 0,
      accountNo: 'AAAAAAA',
      cif: 'AAAAAAA',
      accountName: 'AAAAAAA',
      date: currentDate,
      closingBalance: 0,
      openingBalance: 0,
      totalDebit: 0,
      totalCredit: 0,
      availableBalance: 0,
      currentBalance: 0,
      createdOn: currentDate,
      createdBy: 'AAAAAAA',
      accountStatus: AccountStatus.ACTIVE,
    };
  });

  describe('Service methods', () => {
    it('should find an element', () => {
      const returnedFromService = Object.assign(
        {
          date: currentDate.format(DATE_FORMAT),
          createdOn: currentDate.format(DATE_TIME_FORMAT),
        },
        elemDefault
      );

      service.find(123).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'GET' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(elemDefault);
    });

    it('should create a WalletAccount', () => {
      const returnedFromService = Object.assign(
        {
          id: 0,
          date: currentDate.format(DATE_FORMAT),
          createdOn: currentDate.format(DATE_TIME_FORMAT),
        },
        elemDefault
      );

      const expected = Object.assign(
        {
          date: currentDate,
          createdOn: currentDate,
        },
        returnedFromService
      );

      service.create(new WalletAccount()).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'POST' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(expected);
    });

    it('should update a WalletAccount', () => {
      const returnedFromService = Object.assign(
        {
          id: 1,
          accountNo: 'BBBBBB',
          cif: 'BBBBBB',
          accountName: 'BBBBBB',
          date: currentDate.format(DATE_FORMAT),
          closingBalance: 1,
          openingBalance: 1,
          totalDebit: 1,
          totalCredit: 1,
          availableBalance: 1,
          currentBalance: 1,
          createdOn: currentDate.format(DATE_TIME_FORMAT),
          createdBy: 'BBBBBB',
          accountStatus: 'BBBBBB',
        },
        elemDefault
      );

      const expected = Object.assign(
        {
          date: currentDate,
          createdOn: currentDate,
        },
        returnedFromService
      );

      service.update(expected).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'PUT' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(expected);
    });

    it('should partial update a WalletAccount', () => {
      const patchObject = Object.assign(
        {
          accountName: 'BBBBBB',
          openingBalance: 1,
          totalDebit: 1,
          availableBalance: 1,
          createdOn: currentDate.format(DATE_TIME_FORMAT),
          createdBy: 'BBBBBB',
          accountStatus: 'BBBBBB',
        },
        new WalletAccount()
      );

      const returnedFromService = Object.assign(patchObject, elemDefault);

      const expected = Object.assign(
        {
          date: currentDate,
          createdOn: currentDate,
        },
        returnedFromService
      );

      service.partialUpdate(patchObject).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'PATCH' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(expected);
    });

    it('should return a list of WalletAccount', () => {
      const returnedFromService = Object.assign(
        {
          id: 1,
          accountNo: 'BBBBBB',
          cif: 'BBBBBB',
          accountName: 'BBBBBB',
          date: currentDate.format(DATE_FORMAT),
          closingBalance: 1,
          openingBalance: 1,
          totalDebit: 1,
          totalCredit: 1,
          availableBalance: 1,
          currentBalance: 1,
          createdOn: currentDate.format(DATE_TIME_FORMAT),
          createdBy: 'BBBBBB',
          accountStatus: 'BBBBBB',
        },
        elemDefault
      );

      const expected = Object.assign(
        {
          date: currentDate,
          createdOn: currentDate,
        },
        returnedFromService
      );

      service.query().subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'GET' });
      req.flush([returnedFromService]);
      httpMock.verify();
      expect(expectedResult).toContainEqual(expected);
    });

    it('should delete a WalletAccount', () => {
      service.delete(123).subscribe(resp => (expectedResult = resp.ok));

      const req = httpMock.expectOne({ method: 'DELETE' });
      req.flush({ status: 200 });
      expect(expectedResult);
    });

    describe('addWalletAccountToCollectionIfMissing', () => {
      it('should add a WalletAccount to an empty array', () => {
        const walletAccount: IWalletAccount = { id: 123 };
        expectedResult = service.addWalletAccountToCollectionIfMissing([], walletAccount);
        expect(expectedResult).toHaveLength(1);
        expect(expectedResult).toContain(walletAccount);
      });

      it('should not add a WalletAccount to an array that contains it', () => {
        const walletAccount: IWalletAccount = { id: 123 };
        const walletAccountCollection: IWalletAccount[] = [
          {
            ...walletAccount,
          },
          { id: 456 },
        ];
        expectedResult = service.addWalletAccountToCollectionIfMissing(walletAccountCollection, walletAccount);
        expect(expectedResult).toHaveLength(2);
      });

      it("should add a WalletAccount to an array that doesn't contain it", () => {
        const walletAccount: IWalletAccount = { id: 123 };
        const walletAccountCollection: IWalletAccount[] = [{ id: 456 }];
        expectedResult = service.addWalletAccountToCollectionIfMissing(walletAccountCollection, walletAccount);
        expect(expectedResult).toHaveLength(2);
        expect(expectedResult).toContain(walletAccount);
      });

      it('should add only unique WalletAccount to an array', () => {
        const walletAccountArray: IWalletAccount[] = [{ id: 123 }, { id: 456 }, { id: 82871 }];
        const walletAccountCollection: IWalletAccount[] = [{ id: 123 }];
        expectedResult = service.addWalletAccountToCollectionIfMissing(walletAccountCollection, ...walletAccountArray);
        expect(expectedResult).toHaveLength(3);
      });

      it('should accept varargs', () => {
        const walletAccount: IWalletAccount = { id: 123 };
        const walletAccount2: IWalletAccount = { id: 456 };
        expectedResult = service.addWalletAccountToCollectionIfMissing([], walletAccount, walletAccount2);
        expect(expectedResult).toHaveLength(2);
        expect(expectedResult).toContain(walletAccount);
        expect(expectedResult).toContain(walletAccount2);
      });

      it('should accept null and undefined values', () => {
        const walletAccount: IWalletAccount = { id: 123 };
        expectedResult = service.addWalletAccountToCollectionIfMissing([], null, walletAccount, undefined);
        expect(expectedResult).toHaveLength(1);
        expect(expectedResult).toContain(walletAccount);
      });

      it('should return initial array if no WalletAccount is added', () => {
        const walletAccountCollection: IWalletAccount[] = [{ id: 123 }];
        expectedResult = service.addWalletAccountToCollectionIfMissing(walletAccountCollection, undefined, null);
        expect(expectedResult).toEqual(walletAccountCollection);
      });
    });
  });

  afterEach(() => {
    httpMock.verify();
  });
});
