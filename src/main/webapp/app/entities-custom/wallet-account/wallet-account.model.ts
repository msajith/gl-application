import dayjs from 'dayjs/esm';
import { IAccountType } from 'app/entities/account-type/account-type.model';
import { IAccountCategory } from 'app/entities/account-category/account-category.model';
import { ICurrency } from 'app/entities/currency/currency.model';
import { ICustomer } from 'app/entities/customer/customer.model';
import { AccountStatus } from 'app/entities/enumerations/account-status.model';

export interface IWalletAccount {
  id?: number;
  accountNo?: string;
  cif?: string;
  accountName?: string;
  date?: dayjs.Dayjs | null;
  closingBalance?: number | null;
  openingBalance?: number | null;
  totalDebit?: number | null;
  totalCredit?: number | null;
  availableBalance?: number | null;
  currentBalance?: number | null;
  createdOn?: dayjs.Dayjs | null;
  createdBy?: string | null;
  accountStatus?: AccountStatus | null;
  accountType?: IAccountType | null;
  accountCategory?: IAccountCategory | null;
  accountCurrency?: ICurrency | null;
  customer?: ICustomer | null;
}

export class WalletAccount implements IWalletAccount {
  constructor(
    public id?: number,
    public accountNo?: string,
    public cif?: string,
    public accountName?: string,
    public date?: dayjs.Dayjs | null,
    public closingBalance?: number | null,
    public openingBalance?: number | null,
    public totalDebit?: number | null,
    public totalCredit?: number | null,
    public availableBalance?: number | null,
    public currentBalance?: number | null,
    public createdOn?: dayjs.Dayjs | null,
    public createdBy?: string | null,
    public accountStatus?: AccountStatus | null,
    public accountType?: IAccountType | null,
    public accountCategory?: IAccountCategory | null,
    public accountCurrency?: ICurrency | null,
    public customer?: ICustomer | null
  ) {}
}

export function getWalletAccountIdentifier(walletAccount: IWalletAccount): number | undefined {
  return walletAccount.id;
}
