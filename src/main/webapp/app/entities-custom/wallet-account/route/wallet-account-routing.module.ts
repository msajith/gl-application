import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { UserRouteAccessService } from 'app/core/auth/user-route-access.service';
import { WalletAccountComponent } from '../list/wallet-account.component';
import { WalletAccountDetailComponent } from '../detail/wallet-account-detail.component';
import { WalletAccountUpdateComponent } from '../update/wallet-account-update.component';
import { WalletAccountRoutingResolveService } from './wallet-account-routing-resolve.service';

const walletAccountRoute: Routes = [
  {
    path: '',
    component: WalletAccountComponent,
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/view',
    component: WalletAccountDetailComponent,
    resolve: {
      walletAccount: WalletAccountRoutingResolveService,
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: 'new',
    component: WalletAccountUpdateComponent,
    resolve: {
      walletAccount: WalletAccountRoutingResolveService,
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/edit',
    component: WalletAccountUpdateComponent,
    resolve: {
      walletAccount: WalletAccountRoutingResolveService,
    },
    canActivate: [UserRouteAccessService],
  },
];

@NgModule({
  imports: [RouterModule.forChild(walletAccountRoute)],
  exports: [RouterModule],
})
export class WalletAccountRoutingModule {}
