import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { finalize, map } from 'rxjs/operators';

import dayjs from 'dayjs/esm';
import { DATE_TIME_FORMAT } from 'app/config/input.constants';

import { IWalletAccount, WalletAccount } from '../wallet-account.model';
import { WalletAccountService } from '../service/wallet-account.service';
import { IAccountType } from 'app/entities/account-type/account-type.model';
import { AccountTypeService } from 'app/entities/account-type/service/account-type.service';
import { IAccountCategory } from 'app/entities/account-category/account-category.model';
import { AccountCategoryService } from 'app/entities/account-category/service/account-category.service';
import { ICurrency } from 'app/entities/currency/currency.model';
import { CurrencyService } from 'app/entities/currency/service/currency.service';
import { ICustomer } from 'app/entities/customer/customer.model';
import { CustomerService } from 'app/entities/customer/service/customer.service';
import { AccountStatus } from 'app/entities/enumerations/account-status.model';

@Component({
  selector: 'jhi-wallet-account-update',
  templateUrl: './wallet-account-update.component.html',
})
export class WalletAccountUpdateComponent implements OnInit {
  isSaving = false;
  accountStatusValues = Object.keys(AccountStatus);

  accountTypesSharedCollection: IAccountType[] = [];
  accountCategoriesSharedCollection: IAccountCategory[] = [];
  currenciesSharedCollection: ICurrency[] = [];
  customersSharedCollection: ICustomer[] = [];

  editForm = this.fb.group({
    id: [],
    accountNo: [null, [Validators.required]],
    cif: [null, [Validators.required]],
    accountName: [null, [Validators.required]],
    date: [],
    closingBalance: [],
    openingBalance: [],
    totalDebit: [],
    totalCredit: [],
    availableBalance: [],
    currentBalance: [],
    createdOn: [],
    createdBy: [],
    accountStatus: [],
    accountType: [],
    accountCategory: [],
    accountCurrency: [],
    customer: [],
  });

  constructor(
    protected walletAccountService: WalletAccountService,
    protected accountTypeService: AccountTypeService,
    protected accountCategoryService: AccountCategoryService,
    protected currencyService: CurrencyService,
    protected customerService: CustomerService,
    protected activatedRoute: ActivatedRoute,
    protected fb: FormBuilder
  ) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ walletAccount }) => {
      if (walletAccount.id === undefined) {
        const today = dayjs().startOf('day');
        walletAccount.createdOn = today;
      }

      this.updateForm(walletAccount);

      this.loadRelationshipsOptions();
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const walletAccount = this.createFromForm();
    if (walletAccount.id !== undefined) {
      this.subscribeToSaveResponse(this.walletAccountService.update(walletAccount));
    } else {
      this.subscribeToSaveResponse(this.walletAccountService.create(walletAccount));
    }
  }

  trackAccountTypeById(index: number, item: IAccountType): number {
    return item.id!;
  }

  trackAccountCategoryById(index: number, item: IAccountCategory): number {
    return item.id!;
  }

  trackCurrencyById(index: number, item: ICurrency): number {
    return item.id!;
  }

  trackCustomerById(index: number, item: ICustomer): number {
    return item.id!;
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IWalletAccount>>): void {
    result.pipe(finalize(() => this.onSaveFinalize())).subscribe({
      next: () => this.onSaveSuccess(),
      error: () => this.onSaveError(),
    });
  }

  protected onSaveSuccess(): void {
    this.previousState();
  }

  protected onSaveError(): void {
    // Api for inheritance.
  }

  protected onSaveFinalize(): void {
    this.isSaving = false;
  }

  protected updateForm(walletAccount: IWalletAccount): void {
    this.editForm.patchValue({
      id: walletAccount.id,
      accountNo: walletAccount.accountNo,
      cif: walletAccount.cif,
      accountName: walletAccount.accountName,
      date: walletAccount.date,
      closingBalance: walletAccount.closingBalance,
      openingBalance: walletAccount.openingBalance,
      totalDebit: walletAccount.totalDebit,
      totalCredit: walletAccount.totalCredit,
      availableBalance: walletAccount.availableBalance,
      currentBalance: walletAccount.currentBalance,
      createdOn: walletAccount.createdOn ? walletAccount.createdOn.format(DATE_TIME_FORMAT) : null,
      createdBy: walletAccount.createdBy,
      accountStatus: walletAccount.accountStatus,
      accountType: walletAccount.accountType,
      accountCategory: walletAccount.accountCategory,
      accountCurrency: walletAccount.accountCurrency,
      customer: walletAccount.customer,
    });

    this.accountTypesSharedCollection = this.accountTypeService.addAccountTypeToCollectionIfMissing(
      this.accountTypesSharedCollection,
      walletAccount.accountType
    );
    this.accountCategoriesSharedCollection = this.accountCategoryService.addAccountCategoryToCollectionIfMissing(
      this.accountCategoriesSharedCollection,
      walletAccount.accountCategory
    );
    this.currenciesSharedCollection = this.currencyService.addCurrencyToCollectionIfMissing(
      this.currenciesSharedCollection,
      walletAccount.accountCurrency
    );
    this.customersSharedCollection = this.customerService.addCustomerToCollectionIfMissing(
      this.customersSharedCollection,
      walletAccount.customer
    );
  }

  protected loadRelationshipsOptions(): void {
    this.accountTypeService
      .query()
      .pipe(map((res: HttpResponse<IAccountType[]>) => res.body ?? []))
      .pipe(
        map((accountTypes: IAccountType[]) =>
          this.accountTypeService.addAccountTypeToCollectionIfMissing(accountTypes, this.editForm.get('accountType')!.value)
        )
      )
      .subscribe((accountTypes: IAccountType[]) => (this.accountTypesSharedCollection = accountTypes));

    this.accountCategoryService
      .query()
      .pipe(map((res: HttpResponse<IAccountCategory[]>) => res.body ?? []))
      .pipe(
        map((accountCategories: IAccountCategory[]) =>
          this.accountCategoryService.addAccountCategoryToCollectionIfMissing(
            accountCategories,
            this.editForm.get('accountCategory')!.value
          )
        )
      )
      .subscribe((accountCategories: IAccountCategory[]) => (this.accountCategoriesSharedCollection = accountCategories));

    this.currencyService
      .query()
      .pipe(map((res: HttpResponse<ICurrency[]>) => res.body ?? []))
      .pipe(
        map((currencies: ICurrency[]) =>
          this.currencyService.addCurrencyToCollectionIfMissing(currencies, this.editForm.get('accountCurrency')!.value)
        )
      )
      .subscribe((currencies: ICurrency[]) => (this.currenciesSharedCollection = currencies));

    this.customerService
      .query()
      .pipe(map((res: HttpResponse<ICustomer[]>) => res.body ?? []))
      .pipe(
        map((customers: ICustomer[]) =>
          this.customerService.addCustomerToCollectionIfMissing(customers, this.editForm.get('customer')!.value)
        )
      )
      .subscribe((customers: ICustomer[]) => (this.customersSharedCollection = customers));
  }

  protected createFromForm(): IWalletAccount {
    return {
      ...new WalletAccount(),
      id: this.editForm.get(['id'])!.value,
      accountNo: this.editForm.get(['accountNo'])!.value,
      cif: this.editForm.get(['cif'])!.value,
      accountName: this.editForm.get(['accountName'])!.value,
      date: this.editForm.get(['date'])!.value,
      closingBalance: this.editForm.get(['closingBalance'])!.value,
      openingBalance: this.editForm.get(['openingBalance'])!.value,
      totalDebit: this.editForm.get(['totalDebit'])!.value,
      totalCredit: this.editForm.get(['totalCredit'])!.value,
      availableBalance: this.editForm.get(['availableBalance'])!.value,
      currentBalance: this.editForm.get(['currentBalance'])!.value,
      createdOn: this.editForm.get(['createdOn'])!.value ? dayjs(this.editForm.get(['createdOn'])!.value, DATE_TIME_FORMAT) : undefined,
      createdBy: this.editForm.get(['createdBy'])!.value,
      accountStatus: this.editForm.get(['accountStatus'])!.value,
      accountType: this.editForm.get(['accountType'])!.value,
      accountCategory: this.editForm.get(['accountCategory'])!.value,
      accountCurrency: this.editForm.get(['accountCurrency'])!.value,
      customer: this.editForm.get(['customer'])!.value,
    };
  }
}
