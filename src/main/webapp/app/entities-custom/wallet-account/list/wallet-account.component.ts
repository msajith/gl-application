import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

import { IWalletAccount } from '../wallet-account.model';
import { WalletAccountService } from '../service/wallet-account.service';
import { WalletAccountDeleteDialogComponent } from '../delete/wallet-account-delete-dialog.component';

@Component({
  selector: 'jhi-wallet-account',
  templateUrl: './wallet-account.component.html',
})
export class WalletAccountComponent implements OnInit {
  walletAccounts?: IWalletAccount[];
  isLoading = false;

  constructor(protected walletAccountService: WalletAccountService, protected modalService: NgbModal) {}

  loadAll(): void {
    this.isLoading = true;

    this.walletAccountService.query().subscribe({
      next: (res: HttpResponse<IWalletAccount[]>) => {
        this.isLoading = false;
        this.walletAccounts = res.body ?? [];
      },
      error: () => {
        this.isLoading = false;
      },
    });
  }

  ngOnInit(): void {
    this.loadAll();
  }

  trackId(index: number, item: IWalletAccount): number {
    return item.id!;
  }

  delete(walletAccount: IWalletAccount): void {
    const modalRef = this.modalService.open(WalletAccountDeleteDialogComponent, { size: 'lg', backdrop: 'static' });
    modalRef.componentInstance.walletAccount = walletAccount;
    // unsubscribe not needed because closed completes on modal close
    modalRef.closed.subscribe(reason => {
      if (reason === 'deleted') {
        this.loadAll();
      }
    });
  }
}
