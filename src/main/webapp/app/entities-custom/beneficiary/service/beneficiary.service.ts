import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import dayjs from 'dayjs/esm';

import { isPresent } from 'app/core/util/operators';
import { ApplicationConfigService } from 'app/core/config/application-config.service';
import { createRequestOption } from 'app/core/request/request-util';
import { IBeneficiary, getBeneficiaryIdentifier } from '../beneficiary.model';

export type EntityResponseType = HttpResponse<IBeneficiary>;
export type EntityArrayResponseType = HttpResponse<IBeneficiary[]>;

@Injectable({ providedIn: 'root' })
export class BeneficiaryService {
  protected resourceUrl = this.applicationConfigService.getEndpointFor('api/beneficiaries');
  protected resourceUrlV1 = this.applicationConfigService.getEndpointFor('api/v1/beneficiaries/customer');

  constructor(protected http: HttpClient, protected applicationConfigService: ApplicationConfigService) {}

  create(beneficiary: IBeneficiary): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(beneficiary);
    return this.http
      .post<IBeneficiary>(this.resourceUrl, copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  update(beneficiary: IBeneficiary): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(beneficiary);
    return this.http
      .put<IBeneficiary>(`${this.resourceUrl}/${getBeneficiaryIdentifier(beneficiary) as number}`, copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  partialUpdate(beneficiary: IBeneficiary): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(beneficiary);
    return this.http
      .patch<IBeneficiary>(`${this.resourceUrl}/${getBeneficiaryIdentifier(beneficiary) as number}`, copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http
      .get<IBeneficiary>(`${this.resourceUrl}/${id}`, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http
      .get<IBeneficiary[]>(this.resourceUrl, { params: options, observe: 'response' })
      .pipe(map((res: EntityArrayResponseType) => this.convertDateArrayFromServer(res)));
  }

  queryByCustomerId(id: number): Observable<EntityArrayResponseType> {
    return this.http
      .get<IBeneficiary[]>(`${this.resourceUrlV1}/${id}`, { observe: 'response' })
      .pipe(map((res: EntityArrayResponseType) => this.convertDateArrayFromServer(res)));
  }

  delete(id: number): Observable<HttpResponse<{}>> {
    return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  addBeneficiaryToCollectionIfMissing(
    beneficiaryCollection: IBeneficiary[],
    ...beneficiariesToCheck: (IBeneficiary | null | undefined)[]
  ): IBeneficiary[] {
    const beneficiaries: IBeneficiary[] = beneficiariesToCheck.filter(isPresent);
    if (beneficiaries.length > 0) {
      const beneficiaryCollectionIdentifiers = beneficiaryCollection.map(beneficiaryItem => getBeneficiaryIdentifier(beneficiaryItem)!);
      const beneficiariesToAdd = beneficiaries.filter(beneficiaryItem => {
        const beneficiaryIdentifier = getBeneficiaryIdentifier(beneficiaryItem);
        if (beneficiaryIdentifier == null || beneficiaryCollectionIdentifiers.includes(beneficiaryIdentifier)) {
          return false;
        }
        beneficiaryCollectionIdentifiers.push(beneficiaryIdentifier);
        return true;
      });
      return [...beneficiariesToAdd, ...beneficiaryCollection];
    }
    return beneficiaryCollection;
  }

  protected convertDateFromClient(beneficiary: IBeneficiary): IBeneficiary {
    return Object.assign({}, beneficiary, {
      createdOn: beneficiary.createdOn?.isValid() ? beneficiary.createdOn.toJSON() : undefined,
    });
  }

  protected convertDateFromServer(res: EntityResponseType): EntityResponseType {
    if (res.body) {
      res.body.createdOn = res.body.createdOn ? dayjs(res.body.createdOn) : undefined;
    }
    return res;
  }

  protected convertDateArrayFromServer(res: EntityArrayResponseType): EntityArrayResponseType {
    if (res.body) {
      res.body.forEach((beneficiary: IBeneficiary) => {
        beneficiary.createdOn = beneficiary.createdOn ? dayjs(beneficiary.createdOn) : undefined;
      });
    }
    return res;
  }
}
