import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { finalize, map } from 'rxjs/operators';

import dayjs from 'dayjs/esm';
import { DATE_TIME_FORMAT } from 'app/config/input.constants';

import { IBeneficiary, Beneficiary } from '../beneficiary.model';
import { BeneficiaryService } from '../service/beneficiary.service';
import { ICountry } from 'app/entities/country/country.model';
import { CountryService } from 'app/entities/country/service/country.service';
import { ICurrency } from 'app/entities/currency/currency.model';
import { CurrencyService } from 'app/entities/currency/service/currency.service';
import { ICustomer } from 'app/entities/customer/customer.model';
import { CustomerService } from 'app/entities/customer/service/customer.service';

@Component({
  selector: 'jhi-beneficiary-update',
  templateUrl: './beneficiary-update.component.html',
})
export class BeneficiaryUpdateComponent implements OnInit {
  isSaving = false;

  countriesSharedCollection: ICountry[] = [];
  currenciesSharedCollection: ICurrency[] = [];
  customersSharedCollection: ICustomer[] = [];

  editForm = this.fb.group({
    id: [],
    title: [],
    beneficiaryName: [],
    beneficiaryAccountNo: [],
    beneficiarybankSwiftCode: [],
    intermediaryBankSwiftCode: [],
    beneficiaryBankName: [],
    beneficiaryBankBranchName: [],
    beneficiaryAddress1: [],
    beneficiaryAddress2: [],
    beneficiaryAddress3: [],
    beneficiaryState: [],
    beneficiaryPinCode: [],
    createdOn: [],
    createdBy: [],
    country: [],
    currency: [],
    customer: [],
  });

  constructor(
    protected beneficiaryService: BeneficiaryService,
    protected countryService: CountryService,
    protected currencyService: CurrencyService,
    protected customerService: CustomerService,
    protected activatedRoute: ActivatedRoute,
    protected fb: FormBuilder
  ) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ beneficiary }) => {
      if (beneficiary.id === undefined) {
        const today = dayjs().startOf('day');
        beneficiary.createdOn = today;
      }

      this.updateForm(beneficiary);

      this.loadRelationshipsOptions();
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const beneficiary = this.createFromForm();
    if (beneficiary.id !== undefined) {
      this.subscribeToSaveResponse(this.beneficiaryService.update(beneficiary));
    } else {
      this.subscribeToSaveResponse(this.beneficiaryService.create(beneficiary));
    }
  }

  trackCountryById(index: number, item: ICountry): number {
    return item.id!;
  }

  trackCurrencyById(index: number, item: ICurrency): number {
    return item.id!;
  }

  trackCustomerById(index: number, item: ICustomer): number {
    return item.id!;
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IBeneficiary>>): void {
    result.pipe(finalize(() => this.onSaveFinalize())).subscribe({
      next: () => this.onSaveSuccess(),
      error: () => this.onSaveError(),
    });
  }

  protected onSaveSuccess(): void {
    this.previousState();
  }

  protected onSaveError(): void {
    // Api for inheritance.
  }

  protected onSaveFinalize(): void {
    this.isSaving = false;
  }

  protected updateForm(beneficiary: IBeneficiary): void {
    this.editForm.patchValue({
      id: beneficiary.id,
      title: beneficiary.title,
      beneficiaryName: beneficiary.beneficiaryName,
      beneficiaryAccountNo: beneficiary.beneficiaryAccountNo,
      beneficiarybankSwiftCode: beneficiary.beneficiarybankSwiftCode,
      intermediaryBankSwiftCode: beneficiary.intermediaryBankSwiftCode,
      beneficiaryBankName: beneficiary.beneficiaryBankName,
      beneficiaryBankBranchName: beneficiary.beneficiaryBankBranchName,
      beneficiaryAddress1: beneficiary.beneficiaryAddress1,
      beneficiaryAddress2: beneficiary.beneficiaryAddress2,
      beneficiaryAddress3: beneficiary.beneficiaryAddress3,
      beneficiaryState: beneficiary.beneficiaryState,
      beneficiaryPinCode: beneficiary.beneficiaryPinCode,
      createdOn: beneficiary.createdOn ? beneficiary.createdOn.format(DATE_TIME_FORMAT) : null,
      createdBy: beneficiary.createdBy,
      country: beneficiary.country,
      currency: beneficiary.currency,
      customer: beneficiary.customer,
    });

    this.countriesSharedCollection = this.countryService.addCountryToCollectionIfMissing(
      this.countriesSharedCollection,
      beneficiary.country
    );
    this.currenciesSharedCollection = this.currencyService.addCurrencyToCollectionIfMissing(
      this.currenciesSharedCollection,
      beneficiary.currency
    );
    this.customersSharedCollection = this.customerService.addCustomerToCollectionIfMissing(
      this.customersSharedCollection,
      beneficiary.customer
    );
  }

  protected loadRelationshipsOptions(): void {
    this.countryService
      .query()
      .pipe(map((res: HttpResponse<ICountry[]>) => res.body ?? []))
      .pipe(
        map((countries: ICountry[]) => this.countryService.addCountryToCollectionIfMissing(countries, this.editForm.get('country')!.value))
      )
      .subscribe((countries: ICountry[]) => (this.countriesSharedCollection = countries));

    this.currencyService
      .query()
      .pipe(map((res: HttpResponse<ICurrency[]>) => res.body ?? []))
      .pipe(
        map((currencies: ICurrency[]) =>
          this.currencyService.addCurrencyToCollectionIfMissing(currencies, this.editForm.get('currency')!.value)
        )
      )
      .subscribe((currencies: ICurrency[]) => (this.currenciesSharedCollection = currencies));

    this.customerService
      .query()
      .pipe(map((res: HttpResponse<ICustomer[]>) => res.body ?? []))
      .pipe(
        map((customers: ICustomer[]) =>
          this.customerService.addCustomerToCollectionIfMissing(customers, this.editForm.get('customer')!.value)
        )
      )
      .subscribe((customers: ICustomer[]) => (this.customersSharedCollection = customers));
  }

  protected createFromForm(): IBeneficiary {
    return {
      ...new Beneficiary(),
      id: this.editForm.get(['id'])!.value,
      title: this.editForm.get(['title'])!.value,
      beneficiaryName: this.editForm.get(['beneficiaryName'])!.value,
      beneficiaryAccountNo: this.editForm.get(['beneficiaryAccountNo'])!.value,
      beneficiarybankSwiftCode: this.editForm.get(['beneficiarybankSwiftCode'])!.value,
      intermediaryBankSwiftCode: this.editForm.get(['intermediaryBankSwiftCode'])!.value,
      beneficiaryBankName: this.editForm.get(['beneficiaryBankName'])!.value,
      beneficiaryBankBranchName: this.editForm.get(['beneficiaryBankBranchName'])!.value,
      beneficiaryAddress1: this.editForm.get(['beneficiaryAddress1'])!.value,
      beneficiaryAddress2: this.editForm.get(['beneficiaryAddress2'])!.value,
      beneficiaryAddress3: this.editForm.get(['beneficiaryAddress3'])!.value,
      beneficiaryState: this.editForm.get(['beneficiaryState'])!.value,
      beneficiaryPinCode: this.editForm.get(['beneficiaryPinCode'])!.value,
      createdOn: this.editForm.get(['createdOn'])!.value ? dayjs(this.editForm.get(['createdOn'])!.value, DATE_TIME_FORMAT) : undefined,
      createdBy: this.editForm.get(['createdBy'])!.value,
      country: this.editForm.get(['country'])!.value,
      currency: this.editForm.get(['currency'])!.value,
      customer: this.editForm.get(['customer'])!.value,
    };
  }
}
