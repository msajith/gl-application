import dayjs from 'dayjs/esm';
import { ICountry } from 'app/entities/country/country.model';
import { ICurrency } from 'app/entities/currency/currency.model';
import { ICustomer } from 'app/entities/customer/customer.model';

export interface IBeneficiary {
  id?: number;
  title?: string | null;
  beneficiaryName?: string | null;
  beneficiaryAccountNo?: string | null;
  beneficiarybankSwiftCode?: string | null;
  intermediaryBankSwiftCode?: string | null;
  beneficiaryBankName?: string | null;
  beneficiaryBankBranchName?: string | null;
  beneficiaryAddress1?: string | null;
  beneficiaryAddress2?: string | null;
  beneficiaryAddress3?: string | null;
  beneficiaryState?: string | null;
  beneficiaryPinCode?: string | null;
  createdOn?: dayjs.Dayjs | null;
  createdBy?: string | null;
  country?: ICountry | null;
  currency?: ICurrency | null;
  customer?: ICustomer | null;
}

export class Beneficiary implements IBeneficiary {
  constructor(
    public id?: number,
    public title?: string | null,
    public beneficiaryName?: string | null,
    public beneficiaryAccountNo?: string | null,
    public beneficiarybankSwiftCode?: string | null,
    public intermediaryBankSwiftCode?: string | null,
    public beneficiaryBankName?: string | null,
    public beneficiaryBankBranchName?: string | null,
    public beneficiaryAddress1?: string | null,
    public beneficiaryAddress2?: string | null,
    public beneficiaryAddress3?: string | null,
    public beneficiaryState?: string | null,
    public beneficiaryPinCode?: string | null,
    public createdOn?: dayjs.Dayjs | null,
    public createdBy?: string | null,
    public country?: ICountry | null,
    public currency?: ICurrency | null,
    public customer?: ICustomer | null
  ) {}
}

export function getBeneficiaryIdentifier(beneficiary: IBeneficiary): number | undefined {
  return beneficiary.id;
}
