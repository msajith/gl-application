import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

import { IBeneficiary } from '../beneficiary.model';
import { BeneficiaryService } from '../service/beneficiary.service';
import { BeneficiaryDeleteDialogComponent } from '../delete/beneficiary-delete-dialog.component';

@Component({
  selector: 'jhi-beneficiary',
  templateUrl: './beneficiary.component.html',
})
export class BeneficiaryComponent implements OnInit {
  beneficiaries?: IBeneficiary[];
  isLoading = false;

  constructor(protected beneficiaryService: BeneficiaryService, protected modalService: NgbModal) {}

  loadAll(): void {
    this.isLoading = true;

    this.beneficiaryService.query().subscribe({
      next: (res: HttpResponse<IBeneficiary[]>) => {
        this.isLoading = false;
        this.beneficiaries = res.body ?? [];
      },
      error: () => {
        this.isLoading = false;
      },
    });
  }

  ngOnInit(): void {
    this.loadAll();
  }

  trackId(index: number, item: IBeneficiary): number {
    return item.id!;
  }

  delete(beneficiary: IBeneficiary): void {
    const modalRef = this.modalService.open(BeneficiaryDeleteDialogComponent, { size: 'lg', backdrop: 'static' });
    modalRef.componentInstance.beneficiary = beneficiary;
    // unsubscribe not needed because closed completes on modal close
    modalRef.closed.subscribe(reason => {
      if (reason === 'deleted') {
        this.loadAll();
      }
    });
  }
}
