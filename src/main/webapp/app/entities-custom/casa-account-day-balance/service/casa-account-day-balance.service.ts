import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import dayjs from 'dayjs/esm';

import { isPresent } from 'app/core/util/operators';
import { DATE_FORMAT } from 'app/config/input.constants';
import { ApplicationConfigService } from 'app/core/config/application-config.service';
import { createRequestOption } from 'app/core/request/request-util';
import { ICasaAccountDayBalance, getCasaAccountDayBalanceIdentifier } from '../casa-account-day-balance.model';

export type EntityResponseType = HttpResponse<ICasaAccountDayBalance>;
export type EntityArrayResponseType = HttpResponse<ICasaAccountDayBalance[]>;

@Injectable({ providedIn: 'root' })
export class CasaAccountDayBalanceService {
  protected resourceUrl = this.applicationConfigService.getEndpointFor('api/casa-account-day-balances');

  constructor(protected http: HttpClient, protected applicationConfigService: ApplicationConfigService) {}

  create(casaAccountDayBalance: ICasaAccountDayBalance): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(casaAccountDayBalance);
    return this.http
      .post<ICasaAccountDayBalance>(this.resourceUrl, copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  update(casaAccountDayBalance: ICasaAccountDayBalance): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(casaAccountDayBalance);
    return this.http
      .put<ICasaAccountDayBalance>(`${this.resourceUrl}/${getCasaAccountDayBalanceIdentifier(casaAccountDayBalance) as number}`, copy, {
        observe: 'response',
      })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  partialUpdate(casaAccountDayBalance: ICasaAccountDayBalance): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(casaAccountDayBalance);
    return this.http
      .patch<ICasaAccountDayBalance>(`${this.resourceUrl}/${getCasaAccountDayBalanceIdentifier(casaAccountDayBalance) as number}`, copy, {
        observe: 'response',
      })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http
      .get<ICasaAccountDayBalance>(`${this.resourceUrl}/${id}`, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http
      .get<ICasaAccountDayBalance[]>(this.resourceUrl, { params: options, observe: 'response' })
      .pipe(map((res: EntityArrayResponseType) => this.convertDateArrayFromServer(res)));
  }

  delete(id: number): Observable<HttpResponse<{}>> {
    return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  addCasaAccountDayBalanceToCollectionIfMissing(
    casaAccountDayBalanceCollection: ICasaAccountDayBalance[],
    ...casaAccountDayBalancesToCheck: (ICasaAccountDayBalance | null | undefined)[]
  ): ICasaAccountDayBalance[] {
    const casaAccountDayBalances: ICasaAccountDayBalance[] = casaAccountDayBalancesToCheck.filter(isPresent);
    if (casaAccountDayBalances.length > 0) {
      const casaAccountDayBalanceCollectionIdentifiers = casaAccountDayBalanceCollection.map(
        casaAccountDayBalanceItem => getCasaAccountDayBalanceIdentifier(casaAccountDayBalanceItem)!
      );
      const casaAccountDayBalancesToAdd = casaAccountDayBalances.filter(casaAccountDayBalanceItem => {
        const casaAccountDayBalanceIdentifier = getCasaAccountDayBalanceIdentifier(casaAccountDayBalanceItem);
        if (
          casaAccountDayBalanceIdentifier == null ||
          casaAccountDayBalanceCollectionIdentifiers.includes(casaAccountDayBalanceIdentifier)
        ) {
          return false;
        }
        casaAccountDayBalanceCollectionIdentifiers.push(casaAccountDayBalanceIdentifier);
        return true;
      });
      return [...casaAccountDayBalancesToAdd, ...casaAccountDayBalanceCollection];
    }
    return casaAccountDayBalanceCollection;
  }

  protected convertDateFromClient(casaAccountDayBalance: ICasaAccountDayBalance): ICasaAccountDayBalance {
    return Object.assign({}, casaAccountDayBalance, {
      date: casaAccountDayBalance.date?.isValid() ? casaAccountDayBalance.date.format(DATE_FORMAT) : undefined,
      createdOn: casaAccountDayBalance.createdOn?.isValid() ? casaAccountDayBalance.createdOn.toJSON() : undefined,
    });
  }

  protected convertDateFromServer(res: EntityResponseType): EntityResponseType {
    if (res.body) {
      res.body.date = res.body.date ? dayjs(res.body.date) : undefined;
      res.body.createdOn = res.body.createdOn ? dayjs(res.body.createdOn) : undefined;
    }
    return res;
  }

  protected convertDateArrayFromServer(res: EntityArrayResponseType): EntityArrayResponseType {
    if (res.body) {
      res.body.forEach((casaAccountDayBalance: ICasaAccountDayBalance) => {
        casaAccountDayBalance.date = casaAccountDayBalance.date ? dayjs(casaAccountDayBalance.date) : undefined;
        casaAccountDayBalance.createdOn = casaAccountDayBalance.createdOn ? dayjs(casaAccountDayBalance.createdOn) : undefined;
      });
    }
    return res;
  }
}
