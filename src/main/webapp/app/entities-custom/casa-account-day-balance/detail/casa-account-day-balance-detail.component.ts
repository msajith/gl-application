import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { ICasaAccountDayBalance } from '../casa-account-day-balance.model';

@Component({
  selector: 'jhi-casa-account-day-balance-detail',
  templateUrl: './casa-account-day-balance-detail.component.html',
})
export class CasaAccountDayBalanceDetailComponent implements OnInit {
  casaAccountDayBalance: ICasaAccountDayBalance | null = null;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ casaAccountDayBalance }) => {
      this.casaAccountDayBalance = casaAccountDayBalance;
    });
  }

  previousState(): void {
    window.history.back();
  }
}
