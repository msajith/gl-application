import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { CasaAccountDayBalanceDetailComponent } from './casa-account-day-balance-detail.component';

describe('CasaAccountDayBalance Management Detail Component', () => {
  let comp: CasaAccountDayBalanceDetailComponent;
  let fixture: ComponentFixture<CasaAccountDayBalanceDetailComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [CasaAccountDayBalanceDetailComponent],
      providers: [
        {
          provide: ActivatedRoute,
          useValue: { data: of({ casaAccountDayBalance: { id: 123 } }) },
        },
      ],
    })
      .overrideTemplate(CasaAccountDayBalanceDetailComponent, '')
      .compileComponents();
    fixture = TestBed.createComponent(CasaAccountDayBalanceDetailComponent);
    comp = fixture.componentInstance;
  });

  describe('OnInit', () => {
    it('Should load casaAccountDayBalance on init', () => {
      // WHEN
      comp.ngOnInit();

      // THEN
      expect(comp.casaAccountDayBalance).toEqual(expect.objectContaining({ id: 123 }));
    });
  });
});
