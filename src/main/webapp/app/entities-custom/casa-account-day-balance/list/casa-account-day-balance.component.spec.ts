import { ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpHeaders, HttpResponse } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { of } from 'rxjs';

import { CasaAccountDayBalanceService } from '../service/casa-account-day-balance.service';

import { CasaAccountDayBalanceComponent } from './casa-account-day-balance.component';

describe('CasaAccountDayBalance Management Component', () => {
  let comp: CasaAccountDayBalanceComponent;
  let fixture: ComponentFixture<CasaAccountDayBalanceComponent>;
  let service: CasaAccountDayBalanceService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      declarations: [CasaAccountDayBalanceComponent],
    })
      .overrideTemplate(CasaAccountDayBalanceComponent, '')
      .compileComponents();

    fixture = TestBed.createComponent(CasaAccountDayBalanceComponent);
    comp = fixture.componentInstance;
    service = TestBed.inject(CasaAccountDayBalanceService);

    const headers = new HttpHeaders();
    jest.spyOn(service, 'query').mockReturnValue(
      of(
        new HttpResponse({
          body: [{ id: 123 }],
          headers,
        })
      )
    );
  });

  it('Should call load all on init', () => {
    // WHEN
    comp.ngOnInit();

    // THEN
    expect(service.query).toHaveBeenCalled();
    expect(comp.casaAccountDayBalances?.[0]).toEqual(expect.objectContaining({ id: 123 }));
  });
});
