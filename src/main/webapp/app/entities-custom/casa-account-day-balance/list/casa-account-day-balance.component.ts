import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

import { ICasaAccountDayBalance } from '../casa-account-day-balance.model';
import { CasaAccountDayBalanceService } from '../service/casa-account-day-balance.service';
import { CasaAccountDayBalanceDeleteDialogComponent } from '../delete/casa-account-day-balance-delete-dialog.component';

@Component({
  selector: 'jhi-casa-account-day-balance',
  templateUrl: './casa-account-day-balance.component.html',
})
export class CasaAccountDayBalanceComponent implements OnInit {
  casaAccountDayBalances?: ICasaAccountDayBalance[];
  isLoading = false;

  constructor(protected casaAccountDayBalanceService: CasaAccountDayBalanceService, protected modalService: NgbModal) {}

  loadAll(): void {
    this.isLoading = true;

    this.casaAccountDayBalanceService.query().subscribe({
      next: (res: HttpResponse<ICasaAccountDayBalance[]>) => {
        this.isLoading = false;
        this.casaAccountDayBalances = res.body ?? [];
      },
      error: () => {
        this.isLoading = false;
      },
    });
  }

  ngOnInit(): void {
    this.loadAll();
  }

  trackId(index: number, item: ICasaAccountDayBalance): number {
    return item.id!;
  }

  delete(casaAccountDayBalance: ICasaAccountDayBalance): void {
    const modalRef = this.modalService.open(CasaAccountDayBalanceDeleteDialogComponent, { size: 'lg', backdrop: 'static' });
    modalRef.componentInstance.casaAccountDayBalance = casaAccountDayBalance;
    // unsubscribe not needed because closed completes on modal close
    modalRef.closed.subscribe(reason => {
      if (reason === 'deleted') {
        this.loadAll();
      }
    });
  }
}
