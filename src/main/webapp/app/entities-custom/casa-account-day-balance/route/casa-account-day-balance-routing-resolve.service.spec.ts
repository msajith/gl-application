import { TestBed } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { ActivatedRouteSnapshot, ActivatedRoute, Router, convertToParamMap } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { of } from 'rxjs';

import { ICasaAccountDayBalance, CasaAccountDayBalance } from '../casa-account-day-balance.model';
import { CasaAccountDayBalanceService } from '../service/casa-account-day-balance.service';

import { CasaAccountDayBalanceRoutingResolveService } from './casa-account-day-balance-routing-resolve.service';

describe('CasaAccountDayBalance routing resolve service', () => {
  let mockRouter: Router;
  let mockActivatedRouteSnapshot: ActivatedRouteSnapshot;
  let routingResolveService: CasaAccountDayBalanceRoutingResolveService;
  let service: CasaAccountDayBalanceService;
  let resultCasaAccountDayBalance: ICasaAccountDayBalance | undefined;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule, RouterTestingModule.withRoutes([])],
      providers: [
        {
          provide: ActivatedRoute,
          useValue: {
            snapshot: {
              paramMap: convertToParamMap({}),
            },
          },
        },
      ],
    });
    mockRouter = TestBed.inject(Router);
    jest.spyOn(mockRouter, 'navigate').mockImplementation(() => Promise.resolve(true));
    mockActivatedRouteSnapshot = TestBed.inject(ActivatedRoute).snapshot;
    routingResolveService = TestBed.inject(CasaAccountDayBalanceRoutingResolveService);
    service = TestBed.inject(CasaAccountDayBalanceService);
    resultCasaAccountDayBalance = undefined;
  });

  describe('resolve', () => {
    it('should return ICasaAccountDayBalance returned by find', () => {
      // GIVEN
      service.find = jest.fn(id => of(new HttpResponse({ body: { id } })));
      mockActivatedRouteSnapshot.params = { id: 123 };

      // WHEN
      routingResolveService.resolve(mockActivatedRouteSnapshot).subscribe(result => {
        resultCasaAccountDayBalance = result;
      });

      // THEN
      expect(service.find).toBeCalledWith(123);
      expect(resultCasaAccountDayBalance).toEqual({ id: 123 });
    });

    it('should return new ICasaAccountDayBalance if id is not provided', () => {
      // GIVEN
      service.find = jest.fn();
      mockActivatedRouteSnapshot.params = {};

      // WHEN
      routingResolveService.resolve(mockActivatedRouteSnapshot).subscribe(result => {
        resultCasaAccountDayBalance = result;
      });

      // THEN
      expect(service.find).not.toBeCalled();
      expect(resultCasaAccountDayBalance).toEqual(new CasaAccountDayBalance());
    });

    it('should route to 404 page if data not found in server', () => {
      // GIVEN
      jest.spyOn(service, 'find').mockReturnValue(of(new HttpResponse({ body: null as unknown as CasaAccountDayBalance })));
      mockActivatedRouteSnapshot.params = { id: 123 };

      // WHEN
      routingResolveService.resolve(mockActivatedRouteSnapshot).subscribe(result => {
        resultCasaAccountDayBalance = result;
      });

      // THEN
      expect(service.find).toBeCalledWith(123);
      expect(resultCasaAccountDayBalance).toEqual(undefined);
      expect(mockRouter.navigate).toHaveBeenCalledWith(['404']);
    });
  });
});
