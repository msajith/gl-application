import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { UserRouteAccessService } from 'app/core/auth/user-route-access.service';
import { CasaAccountDayBalanceComponent } from '../list/casa-account-day-balance.component';
import { CasaAccountDayBalanceDetailComponent } from '../detail/casa-account-day-balance-detail.component';
import { CasaAccountDayBalanceUpdateComponent } from '../update/casa-account-day-balance-update.component';
import { CasaAccountDayBalanceRoutingResolveService } from './casa-account-day-balance-routing-resolve.service';

const casaAccountDayBalanceRoute: Routes = [
  {
    path: '',
    component: CasaAccountDayBalanceComponent,
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/view',
    component: CasaAccountDayBalanceDetailComponent,
    resolve: {
      casaAccountDayBalance: CasaAccountDayBalanceRoutingResolveService,
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: 'new',
    component: CasaAccountDayBalanceUpdateComponent,
    resolve: {
      casaAccountDayBalance: CasaAccountDayBalanceRoutingResolveService,
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/edit',
    component: CasaAccountDayBalanceUpdateComponent,
    resolve: {
      casaAccountDayBalance: CasaAccountDayBalanceRoutingResolveService,
    },
    canActivate: [UserRouteAccessService],
  },
];

@NgModule({
  imports: [RouterModule.forChild(casaAccountDayBalanceRoute)],
  exports: [RouterModule],
})
export class CasaAccountDayBalanceRoutingModule {}
