import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, Router } from '@angular/router';
import { Observable, of, EMPTY } from 'rxjs';
import { mergeMap } from 'rxjs/operators';

import { ICasaAccountDayBalance, CasaAccountDayBalance } from '../casa-account-day-balance.model';
import { CasaAccountDayBalanceService } from '../service/casa-account-day-balance.service';

@Injectable({ providedIn: 'root' })
export class CasaAccountDayBalanceRoutingResolveService implements Resolve<ICasaAccountDayBalance> {
  constructor(protected service: CasaAccountDayBalanceService, protected router: Router) {}

  resolve(route: ActivatedRouteSnapshot): Observable<ICasaAccountDayBalance> | Observable<never> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        mergeMap((casaAccountDayBalance: HttpResponse<CasaAccountDayBalance>) => {
          if (casaAccountDayBalance.body) {
            return of(casaAccountDayBalance.body);
          } else {
            this.router.navigate(['404']);
            return EMPTY;
          }
        })
      );
    }
    return of(new CasaAccountDayBalance());
  }
}
