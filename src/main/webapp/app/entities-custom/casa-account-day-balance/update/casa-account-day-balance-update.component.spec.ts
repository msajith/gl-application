import { ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { of, Subject, from } from 'rxjs';

import { CasaAccountDayBalanceService } from '../service/casa-account-day-balance.service';
import { ICasaAccountDayBalance, CasaAccountDayBalance } from '../casa-account-day-balance.model';
import { ICasaAccount } from 'app/entities/casa-account/casa-account.model';
import { CasaAccountService } from 'app/entities/casa-account/service/casa-account.service';

import { CasaAccountDayBalanceUpdateComponent } from './casa-account-day-balance-update.component';

describe('CasaAccountDayBalance Management Update Component', () => {
  let comp: CasaAccountDayBalanceUpdateComponent;
  let fixture: ComponentFixture<CasaAccountDayBalanceUpdateComponent>;
  let activatedRoute: ActivatedRoute;
  let casaAccountDayBalanceService: CasaAccountDayBalanceService;
  let casaAccountService: CasaAccountService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule, RouterTestingModule.withRoutes([])],
      declarations: [CasaAccountDayBalanceUpdateComponent],
      providers: [
        FormBuilder,
        {
          provide: ActivatedRoute,
          useValue: {
            params: from([{}]),
          },
        },
      ],
    })
      .overrideTemplate(CasaAccountDayBalanceUpdateComponent, '')
      .compileComponents();

    fixture = TestBed.createComponent(CasaAccountDayBalanceUpdateComponent);
    activatedRoute = TestBed.inject(ActivatedRoute);
    casaAccountDayBalanceService = TestBed.inject(CasaAccountDayBalanceService);
    casaAccountService = TestBed.inject(CasaAccountService);

    comp = fixture.componentInstance;
  });

  describe('ngOnInit', () => {
    it('Should call CasaAccount query and add missing value', () => {
      const casaAccountDayBalance: ICasaAccountDayBalance = { id: 456 };
      const casaAccount: ICasaAccount = { id: 95843 };
      casaAccountDayBalance.casaAccount = casaAccount;

      const casaAccountCollection: ICasaAccount[] = [{ id: 49200 }];
      jest.spyOn(casaAccountService, 'query').mockReturnValue(of(new HttpResponse({ body: casaAccountCollection })));
      const additionalCasaAccounts = [casaAccount];
      const expectedCollection: ICasaAccount[] = [...additionalCasaAccounts, ...casaAccountCollection];
      jest.spyOn(casaAccountService, 'addCasaAccountToCollectionIfMissing').mockReturnValue(expectedCollection);

      activatedRoute.data = of({ casaAccountDayBalance });
      comp.ngOnInit();

      expect(casaAccountService.query).toHaveBeenCalled();
      expect(casaAccountService.addCasaAccountToCollectionIfMissing).toHaveBeenCalledWith(casaAccountCollection, ...additionalCasaAccounts);
      expect(comp.casaAccountsSharedCollection).toEqual(expectedCollection);
    });

    it('Should update editForm', () => {
      const casaAccountDayBalance: ICasaAccountDayBalance = { id: 456 };
      const casaAccount: ICasaAccount = { id: 47400 };
      casaAccountDayBalance.casaAccount = casaAccount;

      activatedRoute.data = of({ casaAccountDayBalance });
      comp.ngOnInit();

      expect(comp.editForm.value).toEqual(expect.objectContaining(casaAccountDayBalance));
      expect(comp.casaAccountsSharedCollection).toContain(casaAccount);
    });
  });

  describe('save', () => {
    it('Should call update service on save for existing entity', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<CasaAccountDayBalance>>();
      const casaAccountDayBalance = { id: 123 };
      jest.spyOn(casaAccountDayBalanceService, 'update').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ casaAccountDayBalance });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.next(new HttpResponse({ body: casaAccountDayBalance }));
      saveSubject.complete();

      // THEN
      expect(comp.previousState).toHaveBeenCalled();
      expect(casaAccountDayBalanceService.update).toHaveBeenCalledWith(casaAccountDayBalance);
      expect(comp.isSaving).toEqual(false);
    });

    it('Should call create service on save for new entity', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<CasaAccountDayBalance>>();
      const casaAccountDayBalance = new CasaAccountDayBalance();
      jest.spyOn(casaAccountDayBalanceService, 'create').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ casaAccountDayBalance });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.next(new HttpResponse({ body: casaAccountDayBalance }));
      saveSubject.complete();

      // THEN
      expect(casaAccountDayBalanceService.create).toHaveBeenCalledWith(casaAccountDayBalance);
      expect(comp.isSaving).toEqual(false);
      expect(comp.previousState).toHaveBeenCalled();
    });

    it('Should set isSaving to false on error', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<CasaAccountDayBalance>>();
      const casaAccountDayBalance = { id: 123 };
      jest.spyOn(casaAccountDayBalanceService, 'update').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ casaAccountDayBalance });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.error('This is an error!');

      // THEN
      expect(casaAccountDayBalanceService.update).toHaveBeenCalledWith(casaAccountDayBalance);
      expect(comp.isSaving).toEqual(false);
      expect(comp.previousState).not.toHaveBeenCalled();
    });
  });

  describe('Tracking relationships identifiers', () => {
    describe('trackCasaAccountById', () => {
      it('Should return tracked CasaAccount primary key', () => {
        const entity = { id: 123 };
        const trackResult = comp.trackCasaAccountById(0, entity);
        expect(trackResult).toEqual(entity.id);
      });
    });
  });
});
