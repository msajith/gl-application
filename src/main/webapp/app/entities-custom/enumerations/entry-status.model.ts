export enum EntryStatus {
  ACTIVE = 'ACTIVE',

  CANCELLED = 'CANCELLED',
}
