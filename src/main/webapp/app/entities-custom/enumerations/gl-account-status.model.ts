export enum GlAccountStatus {
  ACTIVE = 'ACTIVE',

  INACTIVE = 'INACTIVE',

  CLOSED = 'CLOSED',
}
