export enum TxnStatus {
  ACTIVE = 'ACTIVE',

  CANCELLED = 'CANCELLED',
}
