import { ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpHeaders, HttpResponse } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { of } from 'rxjs';

import { CustomerTypeService } from '../service/customer-type.service';

import { CustomerTypeComponent } from './customer-type.component';

describe('CustomerType Management Component', () => {
  let comp: CustomerTypeComponent;
  let fixture: ComponentFixture<CustomerTypeComponent>;
  let service: CustomerTypeService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      declarations: [CustomerTypeComponent],
    })
      .overrideTemplate(CustomerTypeComponent, '')
      .compileComponents();

    fixture = TestBed.createComponent(CustomerTypeComponent);
    comp = fixture.componentInstance;
    service = TestBed.inject(CustomerTypeService);

    const headers = new HttpHeaders();
    jest.spyOn(service, 'query').mockReturnValue(
      of(
        new HttpResponse({
          body: [{ id: 123 }],
          headers,
        })
      )
    );
  });

  it('Should call load all on init', () => {
    // WHEN
    comp.ngOnInit();

    // THEN
    expect(service.query).toHaveBeenCalled();
    expect(comp.customerTypes?.[0]).toEqual(expect.objectContaining({ id: 123 }));
  });
});
