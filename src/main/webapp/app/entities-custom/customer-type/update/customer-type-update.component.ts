import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { finalize } from 'rxjs/operators';

import { ICustomerType, CustomerType } from '../customer-type.model';
import { CustomerTypeService } from '../service/customer-type.service';

@Component({
  selector: 'jhi-customer-type-update',
  templateUrl: './customer-type-update.component.html',
})
export class CustomerTypeUpdateComponent implements OnInit {
  isSaving = false;

  editForm = this.fb.group({
    id: [],
    name: [],
    code: [],
  });

  constructor(protected customerTypeService: CustomerTypeService, protected activatedRoute: ActivatedRoute, protected fb: FormBuilder) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ customerType }) => {
      this.updateForm(customerType);
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const customerType = this.createFromForm();
    if (customerType.id !== undefined) {
      this.subscribeToSaveResponse(this.customerTypeService.update(customerType));
    } else {
      this.subscribeToSaveResponse(this.customerTypeService.create(customerType));
    }
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<ICustomerType>>): void {
    result.pipe(finalize(() => this.onSaveFinalize())).subscribe({
      next: () => this.onSaveSuccess(),
      error: () => this.onSaveError(),
    });
  }

  protected onSaveSuccess(): void {
    this.previousState();
  }

  protected onSaveError(): void {
    // Api for inheritance.
  }

  protected onSaveFinalize(): void {
    this.isSaving = false;
  }

  protected updateForm(customerType: ICustomerType): void {
    this.editForm.patchValue({
      id: customerType.id,
      name: customerType.name,
      code: customerType.code,
    });
  }

  protected createFromForm(): ICustomerType {
    return {
      ...new CustomerType(),
      id: this.editForm.get(['id'])!.value,
      name: this.editForm.get(['name'])!.value,
      code: this.editForm.get(['code'])!.value,
    };
  }
}
