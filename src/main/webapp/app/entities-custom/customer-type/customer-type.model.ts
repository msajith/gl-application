export interface ICustomerType {
  id?: number;
  name?: string | null;
  code?: string | null;
}

export class CustomerType implements ICustomerType {
  constructor(public id?: number, public name?: string | null, public code?: string | null) {}
}

export function getCustomerTypeIdentifier(customerType: ICustomerType): number | undefined {
  return customerType.id;
}
