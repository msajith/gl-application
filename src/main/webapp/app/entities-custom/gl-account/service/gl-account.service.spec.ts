import { TestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import dayjs from 'dayjs/esm';

import { DATE_FORMAT, DATE_TIME_FORMAT } from 'app/config/input.constants';
import { GlAccountStatus } from 'app/entities/enumerations/gl-account-status.model';
import { GlAccountType } from 'app/entities/enumerations/gl-account-type.model';
import { IGlAccount, GlAccount } from '../gl-account.model';

import { GlAccountService } from './gl-account.service';

describe('GlAccount Service', () => {
  let service: GlAccountService;
  let httpMock: HttpTestingController;
  let elemDefault: IGlAccount;
  let expectedResult: IGlAccount | IGlAccount[] | boolean | null;
  let currentDate: dayjs.Dayjs;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
    });
    expectedResult = null;
    service = TestBed.inject(GlAccountService);
    httpMock = TestBed.inject(HttpTestingController);
    currentDate = dayjs();

    elemDefault = {
      id: 0,
      accountNo: 'AAAAAAA',
      accountName: 'AAAAAAA',
      date: currentDate,
      closingBalance: 0,
      openingBalance: 0,
      totalDebit: 0,
      totalCredit: 0,
      availableBalance: 0,
      currentBalance: 0,
      createdOn: currentDate,
      createdBy: 'AAAAAAA',
      glAccountStatus: GlAccountStatus.ACTIVE,
      glAccountType: GlAccountType.ASSET,
    };
  });

  describe('Service methods', () => {
    it('should find an element', () => {
      const returnedFromService = Object.assign(
        {
          date: currentDate.format(DATE_FORMAT),
          createdOn: currentDate.format(DATE_TIME_FORMAT),
        },
        elemDefault
      );

      service.find(123).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'GET' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(elemDefault);
    });

    it('should create a GlAccount', () => {
      const returnedFromService = Object.assign(
        {
          id: 0,
          date: currentDate.format(DATE_FORMAT),
          createdOn: currentDate.format(DATE_TIME_FORMAT),
        },
        elemDefault
      );

      const expected = Object.assign(
        {
          date: currentDate,
          createdOn: currentDate,
        },
        returnedFromService
      );

      service.create(new GlAccount()).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'POST' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(expected);
    });

    it('should update a GlAccount', () => {
      const returnedFromService = Object.assign(
        {
          id: 1,
          accountNo: 'BBBBBB',
          accountName: 'BBBBBB',
          date: currentDate.format(DATE_FORMAT),
          closingBalance: 1,
          openingBalance: 1,
          totalDebit: 1,
          totalCredit: 1,
          availableBalance: 1,
          currentBalance: 1,
          createdOn: currentDate.format(DATE_TIME_FORMAT),
          createdBy: 'BBBBBB',
          glAccountStatus: 'BBBBBB',
          glAccountType: 'BBBBBB',
        },
        elemDefault
      );

      const expected = Object.assign(
        {
          date: currentDate,
          createdOn: currentDate,
        },
        returnedFromService
      );

      service.update(expected).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'PUT' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(expected);
    });

    it('should partial update a GlAccount', () => {
      const patchObject = Object.assign(
        {
          accountNo: 'BBBBBB',
          accountName: 'BBBBBB',
          availableBalance: 1,
        },
        new GlAccount()
      );

      const returnedFromService = Object.assign(patchObject, elemDefault);

      const expected = Object.assign(
        {
          date: currentDate,
          createdOn: currentDate,
        },
        returnedFromService
      );

      service.partialUpdate(patchObject).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'PATCH' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(expected);
    });

    it('should return a list of GlAccount', () => {
      const returnedFromService = Object.assign(
        {
          id: 1,
          accountNo: 'BBBBBB',
          accountName: 'BBBBBB',
          date: currentDate.format(DATE_FORMAT),
          closingBalance: 1,
          openingBalance: 1,
          totalDebit: 1,
          totalCredit: 1,
          availableBalance: 1,
          currentBalance: 1,
          createdOn: currentDate.format(DATE_TIME_FORMAT),
          createdBy: 'BBBBBB',
          glAccountStatus: 'BBBBBB',
          glAccountType: 'BBBBBB',
        },
        elemDefault
      );

      const expected = Object.assign(
        {
          date: currentDate,
          createdOn: currentDate,
        },
        returnedFromService
      );

      service.query().subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'GET' });
      req.flush([returnedFromService]);
      httpMock.verify();
      expect(expectedResult).toContainEqual(expected);
    });

    it('should delete a GlAccount', () => {
      service.delete(123).subscribe(resp => (expectedResult = resp.ok));

      const req = httpMock.expectOne({ method: 'DELETE' });
      req.flush({ status: 200 });
      expect(expectedResult);
    });

    describe('addGlAccountToCollectionIfMissing', () => {
      it('should add a GlAccount to an empty array', () => {
        const glAccount: IGlAccount = { id: 123 };
        expectedResult = service.addGlAccountToCollectionIfMissing([], glAccount);
        expect(expectedResult).toHaveLength(1);
        expect(expectedResult).toContain(glAccount);
      });

      it('should not add a GlAccount to an array that contains it', () => {
        const glAccount: IGlAccount = { id: 123 };
        const glAccountCollection: IGlAccount[] = [
          {
            ...glAccount,
          },
          { id: 456 },
        ];
        expectedResult = service.addGlAccountToCollectionIfMissing(glAccountCollection, glAccount);
        expect(expectedResult).toHaveLength(2);
      });

      it("should add a GlAccount to an array that doesn't contain it", () => {
        const glAccount: IGlAccount = { id: 123 };
        const glAccountCollection: IGlAccount[] = [{ id: 456 }];
        expectedResult = service.addGlAccountToCollectionIfMissing(glAccountCollection, glAccount);
        expect(expectedResult).toHaveLength(2);
        expect(expectedResult).toContain(glAccount);
      });

      it('should add only unique GlAccount to an array', () => {
        const glAccountArray: IGlAccount[] = [{ id: 123 }, { id: 456 }, { id: 61564 }];
        const glAccountCollection: IGlAccount[] = [{ id: 123 }];
        expectedResult = service.addGlAccountToCollectionIfMissing(glAccountCollection, ...glAccountArray);
        expect(expectedResult).toHaveLength(3);
      });

      it('should accept varargs', () => {
        const glAccount: IGlAccount = { id: 123 };
        const glAccount2: IGlAccount = { id: 456 };
        expectedResult = service.addGlAccountToCollectionIfMissing([], glAccount, glAccount2);
        expect(expectedResult).toHaveLength(2);
        expect(expectedResult).toContain(glAccount);
        expect(expectedResult).toContain(glAccount2);
      });

      it('should accept null and undefined values', () => {
        const glAccount: IGlAccount = { id: 123 };
        expectedResult = service.addGlAccountToCollectionIfMissing([], null, glAccount, undefined);
        expect(expectedResult).toHaveLength(1);
        expect(expectedResult).toContain(glAccount);
      });

      it('should return initial array if no GlAccount is added', () => {
        const glAccountCollection: IGlAccount[] = [{ id: 123 }];
        expectedResult = service.addGlAccountToCollectionIfMissing(glAccountCollection, undefined, null);
        expect(expectedResult).toEqual(glAccountCollection);
      });
    });
  });

  afterEach(() => {
    httpMock.verify();
  });
});
