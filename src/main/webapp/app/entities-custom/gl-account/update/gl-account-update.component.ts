import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { finalize, map } from 'rxjs/operators';

import dayjs from 'dayjs/esm';
import { DATE_TIME_FORMAT } from 'app/config/input.constants';

import { IGlAccount, GlAccount } from '../gl-account.model';
import { GlAccountService } from '../service/gl-account.service';
import { ICurrency } from 'app/entities/currency/currency.model';
import { CurrencyService } from 'app/entities/currency/service/currency.service';
import { IGlAccountCategory } from 'app/entities/gl-account-category/gl-account-category.model';
import { GlAccountCategoryService } from 'app/entities/gl-account-category/service/gl-account-category.service';
import { GlAccountStatus } from 'app/entities/enumerations/gl-account-status.model';
import { GlAccountType } from 'app/entities/enumerations/gl-account-type.model';

@Component({
  selector: 'jhi-gl-account-update',
  templateUrl: './gl-account-update.component.html',
})
export class GlAccountUpdateComponent implements OnInit {
  isSaving = false;
  glAccountStatusValues = Object.keys(GlAccountStatus);
  glAccountTypeValues = Object.keys(GlAccountType);

  currenciesSharedCollection: ICurrency[] = [];
  glAccountCategoriesSharedCollection: IGlAccountCategory[] = [];

  editForm = this.fb.group({
    id: [],
    accountNo: [null, [Validators.required]],
    accountName: [null, [Validators.required]],
    date: [],
    closingBalance: [],
    openingBalance: [],
    totalDebit: [],
    totalCredit: [],
    availableBalance: [],
    currentBalance: [],
    createdOn: [],
    createdBy: [],
    glAccountStatus: [],
    glAccountType: [],
    accountCurrency: [],
    glAccountCategory: [],
  });

  constructor(
    protected glAccountService: GlAccountService,
    protected currencyService: CurrencyService,
    protected glAccountCategoryService: GlAccountCategoryService,
    protected activatedRoute: ActivatedRoute,
    protected fb: FormBuilder
  ) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ glAccount }) => {
      if (glAccount.id === undefined) {
        const today = dayjs().startOf('day');
        glAccount.createdOn = today;
      }

      this.updateForm(glAccount);

      this.loadRelationshipsOptions();
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const glAccount = this.createFromForm();
    if (glAccount.id !== undefined) {
      this.subscribeToSaveResponse(this.glAccountService.update(glAccount));
    } else {
      this.subscribeToSaveResponse(this.glAccountService.create(glAccount));
    }
  }

  trackCurrencyById(index: number, item: ICurrency): number {
    return item.id!;
  }

  trackGlAccountCategoryById(index: number, item: IGlAccountCategory): number {
    return item.id!;
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IGlAccount>>): void {
    result.pipe(finalize(() => this.onSaveFinalize())).subscribe({
      next: () => this.onSaveSuccess(),
      error: () => this.onSaveError(),
    });
  }

  protected onSaveSuccess(): void {
    this.previousState();
  }

  protected onSaveError(): void {
    // Api for inheritance.
  }

  protected onSaveFinalize(): void {
    this.isSaving = false;
  }

  protected updateForm(glAccount: IGlAccount): void {
    this.editForm.patchValue({
      id: glAccount.id,
      accountNo: glAccount.accountNo,
      accountName: glAccount.accountName,
      date: glAccount.date,
      closingBalance: glAccount.closingBalance,
      openingBalance: glAccount.openingBalance,
      totalDebit: glAccount.totalDebit,
      totalCredit: glAccount.totalCredit,
      availableBalance: glAccount.availableBalance,
      currentBalance: glAccount.currentBalance,
      createdOn: glAccount.createdOn ? glAccount.createdOn.format(DATE_TIME_FORMAT) : null,
      createdBy: glAccount.createdBy,
      glAccountStatus: glAccount.glAccountStatus,
      glAccountType: glAccount.glAccountType,
      accountCurrency: glAccount.accountCurrency,
      glAccountCategory: glAccount.glAccountCategory,
    });

    this.currenciesSharedCollection = this.currencyService.addCurrencyToCollectionIfMissing(
      this.currenciesSharedCollection,
      glAccount.accountCurrency
    );
    this.glAccountCategoriesSharedCollection = this.glAccountCategoryService.addGlAccountCategoryToCollectionIfMissing(
      this.glAccountCategoriesSharedCollection,
      glAccount.glAccountCategory
    );
  }

  protected loadRelationshipsOptions(): void {
    this.currencyService
      .query()
      .pipe(map((res: HttpResponse<ICurrency[]>) => res.body ?? []))
      .pipe(
        map((currencies: ICurrency[]) =>
          this.currencyService.addCurrencyToCollectionIfMissing(currencies, this.editForm.get('accountCurrency')!.value)
        )
      )
      .subscribe((currencies: ICurrency[]) => (this.currenciesSharedCollection = currencies));

    this.glAccountCategoryService
      .query()
      .pipe(map((res: HttpResponse<IGlAccountCategory[]>) => res.body ?? []))
      .pipe(
        map((glAccountCategories: IGlAccountCategory[]) =>
          this.glAccountCategoryService.addGlAccountCategoryToCollectionIfMissing(
            glAccountCategories,
            this.editForm.get('glAccountCategory')!.value
          )
        )
      )
      .subscribe((glAccountCategories: IGlAccountCategory[]) => (this.glAccountCategoriesSharedCollection = glAccountCategories));
  }

  protected createFromForm(): IGlAccount {
    return {
      ...new GlAccount(),
      id: this.editForm.get(['id'])!.value,
      accountNo: this.editForm.get(['accountNo'])!.value,
      accountName: this.editForm.get(['accountName'])!.value,
      date: this.editForm.get(['date'])!.value,
      closingBalance: this.editForm.get(['closingBalance'])!.value,
      openingBalance: this.editForm.get(['openingBalance'])!.value,
      totalDebit: this.editForm.get(['totalDebit'])!.value,
      totalCredit: this.editForm.get(['totalCredit'])!.value,
      availableBalance: this.editForm.get(['availableBalance'])!.value,
      currentBalance: this.editForm.get(['currentBalance'])!.value,
      createdOn: this.editForm.get(['createdOn'])!.value ? dayjs(this.editForm.get(['createdOn'])!.value, DATE_TIME_FORMAT) : undefined,
      createdBy: this.editForm.get(['createdBy'])!.value,
      glAccountStatus: this.editForm.get(['glAccountStatus'])!.value,
      glAccountType: this.editForm.get(['glAccountType'])!.value,
      accountCurrency: this.editForm.get(['accountCurrency'])!.value,
      glAccountCategory: this.editForm.get(['glAccountCategory'])!.value,
    };
  }
}
