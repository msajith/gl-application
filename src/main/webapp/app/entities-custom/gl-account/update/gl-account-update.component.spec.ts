import { ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { of, Subject, from } from 'rxjs';

import { GlAccountService } from '../service/gl-account.service';
import { IGlAccount, GlAccount } from '../gl-account.model';
import { ICurrency } from 'app/entities/currency/currency.model';
import { CurrencyService } from 'app/entities/currency/service/currency.service';
import { IGlAccountCategory } from 'app/entities/gl-account-category/gl-account-category.model';
import { GlAccountCategoryService } from 'app/entities/gl-account-category/service/gl-account-category.service';

import { GlAccountUpdateComponent } from './gl-account-update.component';

describe('GlAccount Management Update Component', () => {
  let comp: GlAccountUpdateComponent;
  let fixture: ComponentFixture<GlAccountUpdateComponent>;
  let activatedRoute: ActivatedRoute;
  let glAccountService: GlAccountService;
  let currencyService: CurrencyService;
  let glAccountCategoryService: GlAccountCategoryService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule, RouterTestingModule.withRoutes([])],
      declarations: [GlAccountUpdateComponent],
      providers: [
        FormBuilder,
        {
          provide: ActivatedRoute,
          useValue: {
            params: from([{}]),
          },
        },
      ],
    })
      .overrideTemplate(GlAccountUpdateComponent, '')
      .compileComponents();

    fixture = TestBed.createComponent(GlAccountUpdateComponent);
    activatedRoute = TestBed.inject(ActivatedRoute);
    glAccountService = TestBed.inject(GlAccountService);
    currencyService = TestBed.inject(CurrencyService);
    glAccountCategoryService = TestBed.inject(GlAccountCategoryService);

    comp = fixture.componentInstance;
  });

  describe('ngOnInit', () => {
    it('Should call Currency query and add missing value', () => {
      const glAccount: IGlAccount = { id: 456 };
      const accountCurrency: ICurrency = { id: 74917 };
      glAccount.accountCurrency = accountCurrency;

      const currencyCollection: ICurrency[] = [{ id: 90081 }];
      jest.spyOn(currencyService, 'query').mockReturnValue(of(new HttpResponse({ body: currencyCollection })));
      const additionalCurrencies = [accountCurrency];
      const expectedCollection: ICurrency[] = [...additionalCurrencies, ...currencyCollection];
      jest.spyOn(currencyService, 'addCurrencyToCollectionIfMissing').mockReturnValue(expectedCollection);

      activatedRoute.data = of({ glAccount });
      comp.ngOnInit();

      expect(currencyService.query).toHaveBeenCalled();
      expect(currencyService.addCurrencyToCollectionIfMissing).toHaveBeenCalledWith(currencyCollection, ...additionalCurrencies);
      expect(comp.currenciesSharedCollection).toEqual(expectedCollection);
    });

    it('Should call GlAccountCategory query and add missing value', () => {
      const glAccount: IGlAccount = { id: 456 };
      const glAccountCategory: IGlAccountCategory = { id: 65103 };
      glAccount.glAccountCategory = glAccountCategory;

      const glAccountCategoryCollection: IGlAccountCategory[] = [{ id: 68120 }];
      jest.spyOn(glAccountCategoryService, 'query').mockReturnValue(of(new HttpResponse({ body: glAccountCategoryCollection })));
      const additionalGlAccountCategories = [glAccountCategory];
      const expectedCollection: IGlAccountCategory[] = [...additionalGlAccountCategories, ...glAccountCategoryCollection];
      jest.spyOn(glAccountCategoryService, 'addGlAccountCategoryToCollectionIfMissing').mockReturnValue(expectedCollection);

      activatedRoute.data = of({ glAccount });
      comp.ngOnInit();

      expect(glAccountCategoryService.query).toHaveBeenCalled();
      expect(glAccountCategoryService.addGlAccountCategoryToCollectionIfMissing).toHaveBeenCalledWith(
        glAccountCategoryCollection,
        ...additionalGlAccountCategories
      );
      expect(comp.glAccountCategoriesSharedCollection).toEqual(expectedCollection);
    });

    it('Should update editForm', () => {
      const glAccount: IGlAccount = { id: 456 };
      const accountCurrency: ICurrency = { id: 90948 };
      glAccount.accountCurrency = accountCurrency;
      const glAccountCategory: IGlAccountCategory = { id: 38086 };
      glAccount.glAccountCategory = glAccountCategory;

      activatedRoute.data = of({ glAccount });
      comp.ngOnInit();

      expect(comp.editForm.value).toEqual(expect.objectContaining(glAccount));
      expect(comp.currenciesSharedCollection).toContain(accountCurrency);
      expect(comp.glAccountCategoriesSharedCollection).toContain(glAccountCategory);
    });
  });

  describe('save', () => {
    it('Should call update service on save for existing entity', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<GlAccount>>();
      const glAccount = { id: 123 };
      jest.spyOn(glAccountService, 'update').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ glAccount });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.next(new HttpResponse({ body: glAccount }));
      saveSubject.complete();

      // THEN
      expect(comp.previousState).toHaveBeenCalled();
      expect(glAccountService.update).toHaveBeenCalledWith(glAccount);
      expect(comp.isSaving).toEqual(false);
    });

    it('Should call create service on save for new entity', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<GlAccount>>();
      const glAccount = new GlAccount();
      jest.spyOn(glAccountService, 'create').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ glAccount });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.next(new HttpResponse({ body: glAccount }));
      saveSubject.complete();

      // THEN
      expect(glAccountService.create).toHaveBeenCalledWith(glAccount);
      expect(comp.isSaving).toEqual(false);
      expect(comp.previousState).toHaveBeenCalled();
    });

    it('Should set isSaving to false on error', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<GlAccount>>();
      const glAccount = { id: 123 };
      jest.spyOn(glAccountService, 'update').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ glAccount });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.error('This is an error!');

      // THEN
      expect(glAccountService.update).toHaveBeenCalledWith(glAccount);
      expect(comp.isSaving).toEqual(false);
      expect(comp.previousState).not.toHaveBeenCalled();
    });
  });

  describe('Tracking relationships identifiers', () => {
    describe('trackCurrencyById', () => {
      it('Should return tracked Currency primary key', () => {
        const entity = { id: 123 };
        const trackResult = comp.trackCurrencyById(0, entity);
        expect(trackResult).toEqual(entity.id);
      });
    });

    describe('trackGlAccountCategoryById', () => {
      it('Should return tracked GlAccountCategory primary key', () => {
        const entity = { id: 123 };
        const trackResult = comp.trackGlAccountCategoryById(0, entity);
        expect(trackResult).toEqual(entity.id);
      });
    });
  });
});
