import { ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpHeaders, HttpResponse } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { of } from 'rxjs';

import { GlAccountService } from '../service/gl-account.service';

import { GlAccountComponent } from './gl-account.component';

describe('GlAccount Management Component', () => {
  let comp: GlAccountComponent;
  let fixture: ComponentFixture<GlAccountComponent>;
  let service: GlAccountService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      declarations: [GlAccountComponent],
    })
      .overrideTemplate(GlAccountComponent, '')
      .compileComponents();

    fixture = TestBed.createComponent(GlAccountComponent);
    comp = fixture.componentInstance;
    service = TestBed.inject(GlAccountService);

    const headers = new HttpHeaders();
    jest.spyOn(service, 'query').mockReturnValue(
      of(
        new HttpResponse({
          body: [{ id: 123 }],
          headers,
        })
      )
    );
  });

  it('Should call load all on init', () => {
    // WHEN
    comp.ngOnInit();

    // THEN
    expect(service.query).toHaveBeenCalled();
    expect(comp.glAccounts?.[0]).toEqual(expect.objectContaining({ id: 123 }));
  });
});
