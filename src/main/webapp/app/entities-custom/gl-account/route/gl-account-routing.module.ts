import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { UserRouteAccessService } from 'app/core/auth/user-route-access.service';
import { GlAccountComponent } from '../list/gl-account.component';
import { GlAccountDetailComponent } from '../detail/gl-account-detail.component';
import { GlAccountUpdateComponent } from '../update/gl-account-update.component';
import { GlAccountRoutingResolveService } from './gl-account-routing-resolve.service';

const glAccountRoute: Routes = [
  {
    path: '',
    component: GlAccountComponent,
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/view',
    component: GlAccountDetailComponent,
    resolve: {
      glAccount: GlAccountRoutingResolveService,
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: 'new',
    component: GlAccountUpdateComponent,
    resolve: {
      glAccount: GlAccountRoutingResolveService,
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/edit',
    component: GlAccountUpdateComponent,
    resolve: {
      glAccount: GlAccountRoutingResolveService,
    },
    canActivate: [UserRouteAccessService],
  },
];

@NgModule({
  imports: [RouterModule.forChild(glAccountRoute)],
  exports: [RouterModule],
})
export class GlAccountRoutingModule {}
