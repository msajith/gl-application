import { TestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import dayjs from 'dayjs/esm';

import { DATE_TIME_FORMAT } from 'app/config/input.constants';
import { MerchantStatus } from 'app/entities/enumerations/merchant-status.model';
import { IMerchant, Merchant } from '../merchant.model';

import { MerchantService } from './merchant.service';

describe('Merchant Service', () => {
  let service: MerchantService;
  let httpMock: HttpTestingController;
  let elemDefault: IMerchant;
  let expectedResult: IMerchant | IMerchant[] | boolean | null;
  let currentDate: dayjs.Dayjs;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
    });
    expectedResult = null;
    service = TestBed.inject(MerchantService);
    httpMock = TestBed.inject(HttpTestingController);
    currentDate = dayjs();

    elemDefault = {
      id: 0,
      merchantID: 'AAAAAAA',
      merchantName: 'AAAAAAA',
      merchantCategory: 'AAAAAAA',
      merchantGLAccountNo: 'AAAAAAA',
      merchantBankAccountNo: 'AAAAAAA',
      merchantBankAccountCurrency: 'AAAAAAA',
      merchantBankCode: 'AAAAAAA',
      merchantBankName: 'AAAAAAA',
      merchantAddress: 'AAAAAAA',
      merchantCity: 'AAAAAAA',
      createdOn: currentDate,
      createdBy: 'AAAAAAA',
      merchantStataus: MerchantStatus.ACTIVE,
    };
  });

  describe('Service methods', () => {
    it('should find an element', () => {
      const returnedFromService = Object.assign(
        {
          createdOn: currentDate.format(DATE_TIME_FORMAT),
        },
        elemDefault
      );

      service.find(123).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'GET' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(elemDefault);
    });

    it('should create a Merchant', () => {
      const returnedFromService = Object.assign(
        {
          id: 0,
          createdOn: currentDate.format(DATE_TIME_FORMAT),
        },
        elemDefault
      );

      const expected = Object.assign(
        {
          createdOn: currentDate,
        },
        returnedFromService
      );

      service.create(new Merchant()).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'POST' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(expected);
    });

    it('should update a Merchant', () => {
      const returnedFromService = Object.assign(
        {
          id: 1,
          merchantID: 'BBBBBB',
          merchantName: 'BBBBBB',
          merchantCategory: 'BBBBBB',
          merchantGLAccountNo: 'BBBBBB',
          merchantBankAccountNo: 'BBBBBB',
          merchantBankAccountCurrency: 'BBBBBB',
          merchantBankCode: 'BBBBBB',
          merchantBankName: 'BBBBBB',
          merchantAddress: 'BBBBBB',
          merchantCity: 'BBBBBB',
          createdOn: currentDate.format(DATE_TIME_FORMAT),
          createdBy: 'BBBBBB',
          merchantStataus: 'BBBBBB',
        },
        elemDefault
      );

      const expected = Object.assign(
        {
          createdOn: currentDate,
        },
        returnedFromService
      );

      service.update(expected).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'PUT' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(expected);
    });

    it('should partial update a Merchant', () => {
      const patchObject = Object.assign(
        {
          merchantID: 'BBBBBB',
          merchantCategory: 'BBBBBB',
          merchantGLAccountNo: 'BBBBBB',
          merchantBankAccountNo: 'BBBBBB',
          merchantBankAccountCurrency: 'BBBBBB',
          merchantBankCode: 'BBBBBB',
          merchantCity: 'BBBBBB',
          createdBy: 'BBBBBB',
          merchantStataus: 'BBBBBB',
        },
        new Merchant()
      );

      const returnedFromService = Object.assign(patchObject, elemDefault);

      const expected = Object.assign(
        {
          createdOn: currentDate,
        },
        returnedFromService
      );

      service.partialUpdate(patchObject).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'PATCH' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(expected);
    });

    it('should return a list of Merchant', () => {
      const returnedFromService = Object.assign(
        {
          id: 1,
          merchantID: 'BBBBBB',
          merchantName: 'BBBBBB',
          merchantCategory: 'BBBBBB',
          merchantGLAccountNo: 'BBBBBB',
          merchantBankAccountNo: 'BBBBBB',
          merchantBankAccountCurrency: 'BBBBBB',
          merchantBankCode: 'BBBBBB',
          merchantBankName: 'BBBBBB',
          merchantAddress: 'BBBBBB',
          merchantCity: 'BBBBBB',
          createdOn: currentDate.format(DATE_TIME_FORMAT),
          createdBy: 'BBBBBB',
          merchantStataus: 'BBBBBB',
        },
        elemDefault
      );

      const expected = Object.assign(
        {
          createdOn: currentDate,
        },
        returnedFromService
      );

      service.query().subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'GET' });
      req.flush([returnedFromService]);
      httpMock.verify();
      expect(expectedResult).toContainEqual(expected);
    });

    it('should delete a Merchant', () => {
      service.delete(123).subscribe(resp => (expectedResult = resp.ok));

      const req = httpMock.expectOne({ method: 'DELETE' });
      req.flush({ status: 200 });
      expect(expectedResult);
    });

    describe('addMerchantToCollectionIfMissing', () => {
      it('should add a Merchant to an empty array', () => {
        const merchant: IMerchant = { id: 123 };
        expectedResult = service.addMerchantToCollectionIfMissing([], merchant);
        expect(expectedResult).toHaveLength(1);
        expect(expectedResult).toContain(merchant);
      });

      it('should not add a Merchant to an array that contains it', () => {
        const merchant: IMerchant = { id: 123 };
        const merchantCollection: IMerchant[] = [
          {
            ...merchant,
          },
          { id: 456 },
        ];
        expectedResult = service.addMerchantToCollectionIfMissing(merchantCollection, merchant);
        expect(expectedResult).toHaveLength(2);
      });

      it("should add a Merchant to an array that doesn't contain it", () => {
        const merchant: IMerchant = { id: 123 };
        const merchantCollection: IMerchant[] = [{ id: 456 }];
        expectedResult = service.addMerchantToCollectionIfMissing(merchantCollection, merchant);
        expect(expectedResult).toHaveLength(2);
        expect(expectedResult).toContain(merchant);
      });

      it('should add only unique Merchant to an array', () => {
        const merchantArray: IMerchant[] = [{ id: 123 }, { id: 456 }, { id: 45800 }];
        const merchantCollection: IMerchant[] = [{ id: 123 }];
        expectedResult = service.addMerchantToCollectionIfMissing(merchantCollection, ...merchantArray);
        expect(expectedResult).toHaveLength(3);
      });

      it('should accept varargs', () => {
        const merchant: IMerchant = { id: 123 };
        const merchant2: IMerchant = { id: 456 };
        expectedResult = service.addMerchantToCollectionIfMissing([], merchant, merchant2);
        expect(expectedResult).toHaveLength(2);
        expect(expectedResult).toContain(merchant);
        expect(expectedResult).toContain(merchant2);
      });

      it('should accept null and undefined values', () => {
        const merchant: IMerchant = { id: 123 };
        expectedResult = service.addMerchantToCollectionIfMissing([], null, merchant, undefined);
        expect(expectedResult).toHaveLength(1);
        expect(expectedResult).toContain(merchant);
      });

      it('should return initial array if no Merchant is added', () => {
        const merchantCollection: IMerchant[] = [{ id: 123 }];
        expectedResult = service.addMerchantToCollectionIfMissing(merchantCollection, undefined, null);
        expect(expectedResult).toEqual(merchantCollection);
      });
    });
  });

  afterEach(() => {
    httpMock.verify();
  });
});
