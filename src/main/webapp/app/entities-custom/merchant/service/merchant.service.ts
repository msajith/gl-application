import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import dayjs from 'dayjs/esm';

import { isPresent } from 'app/core/util/operators';
import { ApplicationConfigService } from 'app/core/config/application-config.service';
import { createRequestOption } from 'app/core/request/request-util';
import { IMerchant, getMerchantIdentifier } from '../merchant.model';

export type EntityResponseType = HttpResponse<IMerchant>;
export type EntityArrayResponseType = HttpResponse<IMerchant[]>;

@Injectable({ providedIn: 'root' })
export class MerchantService {
  protected resourceUrl = this.applicationConfigService.getEndpointFor('api/merchants');

  constructor(protected http: HttpClient, protected applicationConfigService: ApplicationConfigService) {}

  create(merchant: IMerchant): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(merchant);
    return this.http
      .post<IMerchant>(this.resourceUrl, copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  update(merchant: IMerchant): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(merchant);
    return this.http
      .put<IMerchant>(`${this.resourceUrl}/${getMerchantIdentifier(merchant) as number}`, copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  partialUpdate(merchant: IMerchant): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(merchant);
    return this.http
      .patch<IMerchant>(`${this.resourceUrl}/${getMerchantIdentifier(merchant) as number}`, copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http
      .get<IMerchant>(`${this.resourceUrl}/${id}`, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http
      .get<IMerchant[]>(this.resourceUrl, { params: options, observe: 'response' })
      .pipe(map((res: EntityArrayResponseType) => this.convertDateArrayFromServer(res)));
  }

  delete(id: number): Observable<HttpResponse<{}>> {
    return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  addMerchantToCollectionIfMissing(merchantCollection: IMerchant[], ...merchantsToCheck: (IMerchant | null | undefined)[]): IMerchant[] {
    const merchants: IMerchant[] = merchantsToCheck.filter(isPresent);
    if (merchants.length > 0) {
      const merchantCollectionIdentifiers = merchantCollection.map(merchantItem => getMerchantIdentifier(merchantItem)!);
      const merchantsToAdd = merchants.filter(merchantItem => {
        const merchantIdentifier = getMerchantIdentifier(merchantItem);
        if (merchantIdentifier == null || merchantCollectionIdentifiers.includes(merchantIdentifier)) {
          return false;
        }
        merchantCollectionIdentifiers.push(merchantIdentifier);
        return true;
      });
      return [...merchantsToAdd, ...merchantCollection];
    }
    return merchantCollection;
  }

  protected convertDateFromClient(merchant: IMerchant): IMerchant {
    return Object.assign({}, merchant, {
      createdOn: merchant.createdOn?.isValid() ? merchant.createdOn.toJSON() : undefined,
    });
  }

  protected convertDateFromServer(res: EntityResponseType): EntityResponseType {
    if (res.body) {
      res.body.createdOn = res.body.createdOn ? dayjs(res.body.createdOn) : undefined;
    }
    return res;
  }

  protected convertDateArrayFromServer(res: EntityArrayResponseType): EntityArrayResponseType {
    if (res.body) {
      res.body.forEach((merchant: IMerchant) => {
        merchant.createdOn = merchant.createdOn ? dayjs(merchant.createdOn) : undefined;
      });
    }
    return res;
  }
}
