import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { finalize, map } from 'rxjs/operators';

import dayjs from 'dayjs/esm';
import { DATE_TIME_FORMAT } from 'app/config/input.constants';

import { IMerchant, Merchant } from '../merchant.model';
import { MerchantService } from '../service/merchant.service';
import { ICountry } from 'app/entities/country/country.model';
import { CountryService } from 'app/entities/country/service/country.service';
import { MerchantStatus } from 'app/entities/enumerations/merchant-status.model';

@Component({
  selector: 'jhi-merchant-update',
  templateUrl: './merchant-update.component.html',
})
export class MerchantUpdateComponent implements OnInit {
  isSaving = false;
  merchantStatusValues = Object.keys(MerchantStatus);

  countriesSharedCollection: ICountry[] = [];

  editForm = this.fb.group({
    id: [],
    merchantID: [],
    merchantName: [],
    merchantCategory: [],
    merchantGLAccountNo: [],
    merchantBankAccountNo: [],
    merchantBankAccountCurrency: [],
    merchantBankCode: [],
    merchantBankName: [],
    merchantAddress: [],
    merchantCity: [],
    createdOn: [],
    createdBy: [],
    merchantStataus: [],
    country: [],
  });

  constructor(
    protected merchantService: MerchantService,
    protected countryService: CountryService,
    protected activatedRoute: ActivatedRoute,
    protected fb: FormBuilder
  ) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ merchant }) => {
      if (merchant.id === undefined) {
        const today = dayjs().startOf('day');
        merchant.createdOn = today;
      }

      this.updateForm(merchant);

      this.loadRelationshipsOptions();
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const merchant = this.createFromForm();
    if (merchant.id !== undefined) {
      this.subscribeToSaveResponse(this.merchantService.update(merchant));
    } else {
      this.subscribeToSaveResponse(this.merchantService.create(merchant));
    }
  }

  trackCountryById(index: number, item: ICountry): number {
    return item.id!;
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IMerchant>>): void {
    result.pipe(finalize(() => this.onSaveFinalize())).subscribe({
      next: () => this.onSaveSuccess(),
      error: () => this.onSaveError(),
    });
  }

  protected onSaveSuccess(): void {
    this.previousState();
  }

  protected onSaveError(): void {
    // Api for inheritance.
  }

  protected onSaveFinalize(): void {
    this.isSaving = false;
  }

  protected updateForm(merchant: IMerchant): void {
    this.editForm.patchValue({
      id: merchant.id,
      merchantID: merchant.merchantID,
      merchantName: merchant.merchantName,
      merchantCategory: merchant.merchantCategory,
      merchantGLAccountNo: merchant.merchantGLAccountNo,
      merchantBankAccountNo: merchant.merchantBankAccountNo,
      merchantBankAccountCurrency: merchant.merchantBankAccountCurrency,
      merchantBankCode: merchant.merchantBankCode,
      merchantBankName: merchant.merchantBankName,
      merchantAddress: merchant.merchantAddress,
      merchantCity: merchant.merchantCity,
      createdOn: merchant.createdOn ? merchant.createdOn.format(DATE_TIME_FORMAT) : null,
      createdBy: merchant.createdBy,
      merchantStataus: merchant.merchantStataus,
      country: merchant.country,
    });

    this.countriesSharedCollection = this.countryService.addCountryToCollectionIfMissing(this.countriesSharedCollection, merchant.country);
  }

  protected loadRelationshipsOptions(): void {
    this.countryService
      .query()
      .pipe(map((res: HttpResponse<ICountry[]>) => res.body ?? []))
      .pipe(
        map((countries: ICountry[]) => this.countryService.addCountryToCollectionIfMissing(countries, this.editForm.get('country')!.value))
      )
      .subscribe((countries: ICountry[]) => (this.countriesSharedCollection = countries));
  }

  protected createFromForm(): IMerchant {
    return {
      ...new Merchant(),
      id: this.editForm.get(['id'])!.value,
      merchantID: this.editForm.get(['merchantID'])!.value,
      merchantName: this.editForm.get(['merchantName'])!.value,
      merchantCategory: this.editForm.get(['merchantCategory'])!.value,
      merchantGLAccountNo: this.editForm.get(['merchantGLAccountNo'])!.value,
      merchantBankAccountNo: this.editForm.get(['merchantBankAccountNo'])!.value,
      merchantBankAccountCurrency: this.editForm.get(['merchantBankAccountCurrency'])!.value,
      merchantBankCode: this.editForm.get(['merchantBankCode'])!.value,
      merchantBankName: this.editForm.get(['merchantBankName'])!.value,
      merchantAddress: this.editForm.get(['merchantAddress'])!.value,
      merchantCity: this.editForm.get(['merchantCity'])!.value,
      createdOn: this.editForm.get(['createdOn'])!.value ? dayjs(this.editForm.get(['createdOn'])!.value, DATE_TIME_FORMAT) : undefined,
      createdBy: this.editForm.get(['createdBy'])!.value,
      merchantStataus: this.editForm.get(['merchantStataus'])!.value,
      country: this.editForm.get(['country'])!.value,
    };
  }
}
