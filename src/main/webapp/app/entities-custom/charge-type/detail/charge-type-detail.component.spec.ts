import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { ChargeTypeDetailComponent } from './charge-type-detail.component';

describe('ChargeType Management Detail Component', () => {
  let comp: ChargeTypeDetailComponent;
  let fixture: ComponentFixture<ChargeTypeDetailComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [ChargeTypeDetailComponent],
      providers: [
        {
          provide: ActivatedRoute,
          useValue: { data: of({ chargeType: { id: 123 } }) },
        },
      ],
    })
      .overrideTemplate(ChargeTypeDetailComponent, '')
      .compileComponents();
    fixture = TestBed.createComponent(ChargeTypeDetailComponent);
    comp = fixture.componentInstance;
  });

  describe('OnInit', () => {
    it('Should load chargeType on init', () => {
      // WHEN
      comp.ngOnInit();

      // THEN
      expect(comp.chargeType).toEqual(expect.objectContaining({ id: 123 }));
    });
  });
});
