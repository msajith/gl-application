import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IChargeType } from '../charge-type.model';

@Component({
  selector: 'jhi-charge-type-detail',
  templateUrl: './charge-type-detail.component.html',
})
export class ChargeTypeDetailComponent implements OnInit {
  chargeType: IChargeType | null = null;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ chargeType }) => {
      this.chargeType = chargeType;
    });
  }

  previousState(): void {
    window.history.back();
  }
}
