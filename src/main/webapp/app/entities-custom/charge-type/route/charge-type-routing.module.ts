import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { UserRouteAccessService } from 'app/core/auth/user-route-access.service';
import { ChargeTypeComponent } from '../list/charge-type.component';
import { ChargeTypeDetailComponent } from '../detail/charge-type-detail.component';
import { ChargeTypeUpdateComponent } from '../update/charge-type-update.component';
import { ChargeTypeRoutingResolveService } from './charge-type-routing-resolve.service';

const chargeTypeRoute: Routes = [
  {
    path: '',
    component: ChargeTypeComponent,
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/view',
    component: ChargeTypeDetailComponent,
    resolve: {
      chargeType: ChargeTypeRoutingResolveService,
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: 'new',
    component: ChargeTypeUpdateComponent,
    resolve: {
      chargeType: ChargeTypeRoutingResolveService,
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/edit',
    component: ChargeTypeUpdateComponent,
    resolve: {
      chargeType: ChargeTypeRoutingResolveService,
    },
    canActivate: [UserRouteAccessService],
  },
];

@NgModule({
  imports: [RouterModule.forChild(chargeTypeRoute)],
  exports: [RouterModule],
})
export class ChargeTypeRoutingModule {}
