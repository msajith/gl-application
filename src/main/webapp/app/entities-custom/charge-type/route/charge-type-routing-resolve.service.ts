import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, Router } from '@angular/router';
import { Observable, of, EMPTY } from 'rxjs';
import { mergeMap } from 'rxjs/operators';

import { IChargeType, ChargeType } from '../charge-type.model';
import { ChargeTypeService } from '../service/charge-type.service';

@Injectable({ providedIn: 'root' })
export class ChargeTypeRoutingResolveService implements Resolve<IChargeType> {
  constructor(protected service: ChargeTypeService, protected router: Router) {}

  resolve(route: ActivatedRouteSnapshot): Observable<IChargeType> | Observable<never> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        mergeMap((chargeType: HttpResponse<ChargeType>) => {
          if (chargeType.body) {
            return of(chargeType.body);
          } else {
            this.router.navigate(['404']);
            return EMPTY;
          }
        })
      );
    }
    return of(new ChargeType());
  }
}
