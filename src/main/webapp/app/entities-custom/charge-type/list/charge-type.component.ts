import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

import { IChargeType } from '../charge-type.model';
import { ChargeTypeService } from '../service/charge-type.service';
import { ChargeTypeDeleteDialogComponent } from '../delete/charge-type-delete-dialog.component';

@Component({
  selector: 'jhi-charge-type',
  templateUrl: './charge-type.component.html',
})
export class ChargeTypeComponent implements OnInit {
  chargeTypes?: IChargeType[];
  isLoading = false;

  constructor(protected chargeTypeService: ChargeTypeService, protected modalService: NgbModal) {}

  loadAll(): void {
    this.isLoading = true;

    this.chargeTypeService.query().subscribe({
      next: (res: HttpResponse<IChargeType[]>) => {
        this.isLoading = false;
        this.chargeTypes = res.body ?? [];
      },
      error: () => {
        this.isLoading = false;
      },
    });
  }

  ngOnInit(): void {
    this.loadAll();
  }

  trackId(index: number, item: IChargeType): number {
    return item.id!;
  }

  delete(chargeType: IChargeType): void {
    const modalRef = this.modalService.open(ChargeTypeDeleteDialogComponent, { size: 'lg', backdrop: 'static' });
    modalRef.componentInstance.chargeType = chargeType;
    // unsubscribe not needed because closed completes on modal close
    modalRef.closed.subscribe(reason => {
      if (reason === 'deleted') {
        this.loadAll();
      }
    });
  }
}
