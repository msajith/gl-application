import { ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpHeaders, HttpResponse } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { of } from 'rxjs';

import { ChargeTypeService } from '../service/charge-type.service';

import { ChargeTypeComponent } from './charge-type.component';

describe('ChargeType Management Component', () => {
  let comp: ChargeTypeComponent;
  let fixture: ComponentFixture<ChargeTypeComponent>;
  let service: ChargeTypeService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      declarations: [ChargeTypeComponent],
    })
      .overrideTemplate(ChargeTypeComponent, '')
      .compileComponents();

    fixture = TestBed.createComponent(ChargeTypeComponent);
    comp = fixture.componentInstance;
    service = TestBed.inject(ChargeTypeService);

    const headers = new HttpHeaders();
    jest.spyOn(service, 'query').mockReturnValue(
      of(
        new HttpResponse({
          body: [{ id: 123 }],
          headers,
        })
      )
    );
  });

  it('Should call load all on init', () => {
    // WHEN
    comp.ngOnInit();

    // THEN
    expect(service.query).toHaveBeenCalled();
    expect(comp.chargeTypes?.[0]).toEqual(expect.objectContaining({ id: 123 }));
  });
});
