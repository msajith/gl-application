import { Component } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

import { IChargeType } from '../charge-type.model';
import { ChargeTypeService } from '../service/charge-type.service';

@Component({
  templateUrl: './charge-type-delete-dialog.component.html',
})
export class ChargeTypeDeleteDialogComponent {
  chargeType?: IChargeType;

  constructor(protected chargeTypeService: ChargeTypeService, protected activeModal: NgbActiveModal) {}

  cancel(): void {
    this.activeModal.dismiss();
  }

  confirmDelete(id: number): void {
    this.chargeTypeService.delete(id).subscribe(() => {
      this.activeModal.close('deleted');
    });
  }
}
