import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { finalize } from 'rxjs/operators';

import { IChargeType, ChargeType } from '../charge-type.model';
import { ChargeTypeService } from '../service/charge-type.service';

@Component({
  selector: 'jhi-charge-type-update',
  templateUrl: './charge-type-update.component.html',
})
export class ChargeTypeUpdateComponent implements OnInit {
  isSaving = false;

  editForm = this.fb.group({
    id: [],
    name: [],
    code: [],
  });

  constructor(protected chargeTypeService: ChargeTypeService, protected activatedRoute: ActivatedRoute, protected fb: FormBuilder) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ chargeType }) => {
      this.updateForm(chargeType);
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const chargeType = this.createFromForm();
    if (chargeType.id !== undefined) {
      this.subscribeToSaveResponse(this.chargeTypeService.update(chargeType));
    } else {
      this.subscribeToSaveResponse(this.chargeTypeService.create(chargeType));
    }
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IChargeType>>): void {
    result.pipe(finalize(() => this.onSaveFinalize())).subscribe({
      next: () => this.onSaveSuccess(),
      error: () => this.onSaveError(),
    });
  }

  protected onSaveSuccess(): void {
    this.previousState();
  }

  protected onSaveError(): void {
    // Api for inheritance.
  }

  protected onSaveFinalize(): void {
    this.isSaving = false;
  }

  protected updateForm(chargeType: IChargeType): void {
    this.editForm.patchValue({
      id: chargeType.id,
      name: chargeType.name,
      code: chargeType.code,
    });
  }

  protected createFromForm(): IChargeType {
    return {
      ...new ChargeType(),
      id: this.editForm.get(['id'])!.value,
      name: this.editForm.get(['name'])!.value,
      code: this.editForm.get(['code'])!.value,
    };
  }
}
