import { NgModule } from '@angular/core';
import { SharedModule } from 'app/shared/shared.module';
import { ChargeTypeComponent } from './list/charge-type.component';
import { ChargeTypeDetailComponent } from './detail/charge-type-detail.component';
import { ChargeTypeUpdateComponent } from './update/charge-type-update.component';
import { ChargeTypeDeleteDialogComponent } from './delete/charge-type-delete-dialog.component';
import { ChargeTypeRoutingModule } from './route/charge-type-routing.module';

@NgModule({
  imports: [SharedModule, ChargeTypeRoutingModule],
  declarations: [ChargeTypeComponent, ChargeTypeDetailComponent, ChargeTypeUpdateComponent, ChargeTypeDeleteDialogComponent],
  entryComponents: [ChargeTypeDeleteDialogComponent],
})
export class ChargeTypeModule {}
