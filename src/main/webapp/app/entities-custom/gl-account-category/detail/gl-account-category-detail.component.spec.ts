import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { GlAccountCategoryDetailComponent } from './gl-account-category-detail.component';

describe('GlAccountCategory Management Detail Component', () => {
  let comp: GlAccountCategoryDetailComponent;
  let fixture: ComponentFixture<GlAccountCategoryDetailComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [GlAccountCategoryDetailComponent],
      providers: [
        {
          provide: ActivatedRoute,
          useValue: { data: of({ glAccountCategory: { id: 123 } }) },
        },
      ],
    })
      .overrideTemplate(GlAccountCategoryDetailComponent, '')
      .compileComponents();
    fixture = TestBed.createComponent(GlAccountCategoryDetailComponent);
    comp = fixture.componentInstance;
  });

  describe('OnInit', () => {
    it('Should load glAccountCategory on init', () => {
      // WHEN
      comp.ngOnInit();

      // THEN
      expect(comp.glAccountCategory).toEqual(expect.objectContaining({ id: 123 }));
    });
  });
});
