import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IGlAccountCategory } from '../gl-account-category.model';

@Component({
  selector: 'jhi-gl-account-category-detail',
  templateUrl: './gl-account-category-detail.component.html',
})
export class GlAccountCategoryDetailComponent implements OnInit {
  glAccountCategory: IGlAccountCategory | null = null;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ glAccountCategory }) => {
      this.glAccountCategory = glAccountCategory;
    });
  }

  previousState(): void {
    window.history.back();
  }
}
