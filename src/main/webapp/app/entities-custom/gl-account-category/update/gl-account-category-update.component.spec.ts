import { ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { of, Subject, from } from 'rxjs';

import { GlAccountCategoryService } from '../service/gl-account-category.service';
import { IGlAccountCategory, GlAccountCategory } from '../gl-account-category.model';

import { GlAccountCategoryUpdateComponent } from './gl-account-category-update.component';

describe('GlAccountCategory Management Update Component', () => {
  let comp: GlAccountCategoryUpdateComponent;
  let fixture: ComponentFixture<GlAccountCategoryUpdateComponent>;
  let activatedRoute: ActivatedRoute;
  let glAccountCategoryService: GlAccountCategoryService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule, RouterTestingModule.withRoutes([])],
      declarations: [GlAccountCategoryUpdateComponent],
      providers: [
        FormBuilder,
        {
          provide: ActivatedRoute,
          useValue: {
            params: from([{}]),
          },
        },
      ],
    })
      .overrideTemplate(GlAccountCategoryUpdateComponent, '')
      .compileComponents();

    fixture = TestBed.createComponent(GlAccountCategoryUpdateComponent);
    activatedRoute = TestBed.inject(ActivatedRoute);
    glAccountCategoryService = TestBed.inject(GlAccountCategoryService);

    comp = fixture.componentInstance;
  });

  describe('ngOnInit', () => {
    it('Should update editForm', () => {
      const glAccountCategory: IGlAccountCategory = { id: 456 };

      activatedRoute.data = of({ glAccountCategory });
      comp.ngOnInit();

      expect(comp.editForm.value).toEqual(expect.objectContaining(glAccountCategory));
    });
  });

  describe('save', () => {
    it('Should call update service on save for existing entity', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<GlAccountCategory>>();
      const glAccountCategory = { id: 123 };
      jest.spyOn(glAccountCategoryService, 'update').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ glAccountCategory });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.next(new HttpResponse({ body: glAccountCategory }));
      saveSubject.complete();

      // THEN
      expect(comp.previousState).toHaveBeenCalled();
      expect(glAccountCategoryService.update).toHaveBeenCalledWith(glAccountCategory);
      expect(comp.isSaving).toEqual(false);
    });

    it('Should call create service on save for new entity', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<GlAccountCategory>>();
      const glAccountCategory = new GlAccountCategory();
      jest.spyOn(glAccountCategoryService, 'create').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ glAccountCategory });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.next(new HttpResponse({ body: glAccountCategory }));
      saveSubject.complete();

      // THEN
      expect(glAccountCategoryService.create).toHaveBeenCalledWith(glAccountCategory);
      expect(comp.isSaving).toEqual(false);
      expect(comp.previousState).toHaveBeenCalled();
    });

    it('Should set isSaving to false on error', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<GlAccountCategory>>();
      const glAccountCategory = { id: 123 };
      jest.spyOn(glAccountCategoryService, 'update').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ glAccountCategory });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.error('This is an error!');

      // THEN
      expect(glAccountCategoryService.update).toHaveBeenCalledWith(glAccountCategory);
      expect(comp.isSaving).toEqual(false);
      expect(comp.previousState).not.toHaveBeenCalled();
    });
  });
});
