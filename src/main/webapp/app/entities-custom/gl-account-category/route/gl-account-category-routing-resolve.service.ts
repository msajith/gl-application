import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, Router } from '@angular/router';
import { Observable, of, EMPTY } from 'rxjs';
import { mergeMap } from 'rxjs/operators';

import { IGlAccountCategory, GlAccountCategory } from '../gl-account-category.model';
import { GlAccountCategoryService } from '../service/gl-account-category.service';

@Injectable({ providedIn: 'root' })
export class GlAccountCategoryRoutingResolveService implements Resolve<IGlAccountCategory> {
  constructor(protected service: GlAccountCategoryService, protected router: Router) {}

  resolve(route: ActivatedRouteSnapshot): Observable<IGlAccountCategory> | Observable<never> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        mergeMap((glAccountCategory: HttpResponse<GlAccountCategory>) => {
          if (glAccountCategory.body) {
            return of(glAccountCategory.body);
          } else {
            this.router.navigate(['404']);
            return EMPTY;
          }
        })
      );
    }
    return of(new GlAccountCategory());
  }
}
