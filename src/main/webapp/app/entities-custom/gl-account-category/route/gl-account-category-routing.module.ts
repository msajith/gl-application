import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { UserRouteAccessService } from 'app/core/auth/user-route-access.service';
import { GlAccountCategoryComponent } from '../list/gl-account-category.component';
import { GlAccountCategoryDetailComponent } from '../detail/gl-account-category-detail.component';
import { GlAccountCategoryUpdateComponent } from '../update/gl-account-category-update.component';
import { GlAccountCategoryRoutingResolveService } from './gl-account-category-routing-resolve.service';

const glAccountCategoryRoute: Routes = [
  {
    path: '',
    component: GlAccountCategoryComponent,
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/view',
    component: GlAccountCategoryDetailComponent,
    resolve: {
      glAccountCategory: GlAccountCategoryRoutingResolveService,
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: 'new',
    component: GlAccountCategoryUpdateComponent,
    resolve: {
      glAccountCategory: GlAccountCategoryRoutingResolveService,
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/edit',
    component: GlAccountCategoryUpdateComponent,
    resolve: {
      glAccountCategory: GlAccountCategoryRoutingResolveService,
    },
    canActivate: [UserRouteAccessService],
  },
];

@NgModule({
  imports: [RouterModule.forChild(glAccountCategoryRoute)],
  exports: [RouterModule],
})
export class GlAccountCategoryRoutingModule {}
