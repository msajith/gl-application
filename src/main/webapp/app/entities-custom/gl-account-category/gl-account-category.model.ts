export interface IGlAccountCategory {
  id?: number;
  name?: string | null;
  code?: string | null;
}

export class GlAccountCategory implements IGlAccountCategory {
  constructor(public id?: number, public name?: string | null, public code?: string | null) {}
}

export function getGlAccountCategoryIdentifier(glAccountCategory: IGlAccountCategory): number | undefined {
  return glAccountCategory.id;
}
