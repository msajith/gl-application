import { ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpHeaders, HttpResponse } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { of } from 'rxjs';

import { GlAccountCategoryService } from '../service/gl-account-category.service';

import { GlAccountCategoryComponent } from './gl-account-category.component';

describe('GlAccountCategory Management Component', () => {
  let comp: GlAccountCategoryComponent;
  let fixture: ComponentFixture<GlAccountCategoryComponent>;
  let service: GlAccountCategoryService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      declarations: [GlAccountCategoryComponent],
    })
      .overrideTemplate(GlAccountCategoryComponent, '')
      .compileComponents();

    fixture = TestBed.createComponent(GlAccountCategoryComponent);
    comp = fixture.componentInstance;
    service = TestBed.inject(GlAccountCategoryService);

    const headers = new HttpHeaders();
    jest.spyOn(service, 'query').mockReturnValue(
      of(
        new HttpResponse({
          body: [{ id: 123 }],
          headers,
        })
      )
    );
  });

  it('Should call load all on init', () => {
    // WHEN
    comp.ngOnInit();

    // THEN
    expect(service.query).toHaveBeenCalled();
    expect(comp.glAccountCategories?.[0]).toEqual(expect.objectContaining({ id: 123 }));
  });
});
