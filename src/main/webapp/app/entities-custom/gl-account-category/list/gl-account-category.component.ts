import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

import { IGlAccountCategory } from '../gl-account-category.model';
import { GlAccountCategoryService } from '../service/gl-account-category.service';
import { GlAccountCategoryDeleteDialogComponent } from '../delete/gl-account-category-delete-dialog.component';

@Component({
  selector: 'jhi-gl-account-category',
  templateUrl: './gl-account-category.component.html',
})
export class GlAccountCategoryComponent implements OnInit {
  glAccountCategories?: IGlAccountCategory[];
  isLoading = false;

  constructor(protected glAccountCategoryService: GlAccountCategoryService, protected modalService: NgbModal) {}

  loadAll(): void {
    this.isLoading = true;

    this.glAccountCategoryService.query().subscribe({
      next: (res: HttpResponse<IGlAccountCategory[]>) => {
        this.isLoading = false;
        this.glAccountCategories = res.body ?? [];
      },
      error: () => {
        this.isLoading = false;
      },
    });
  }

  ngOnInit(): void {
    this.loadAll();
  }

  trackId(index: number, item: IGlAccountCategory): number {
    return item.id!;
  }

  delete(glAccountCategory: IGlAccountCategory): void {
    const modalRef = this.modalService.open(GlAccountCategoryDeleteDialogComponent, { size: 'lg', backdrop: 'static' });
    modalRef.componentInstance.glAccountCategory = glAccountCategory;
    // unsubscribe not needed because closed completes on modal close
    modalRef.closed.subscribe(reason => {
      if (reason === 'deleted') {
        this.loadAll();
      }
    });
  }
}
