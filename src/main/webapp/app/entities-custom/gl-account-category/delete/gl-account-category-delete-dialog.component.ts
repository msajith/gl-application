import { Component } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

import { IGlAccountCategory } from '../gl-account-category.model';
import { GlAccountCategoryService } from '../service/gl-account-category.service';

@Component({
  templateUrl: './gl-account-category-delete-dialog.component.html',
})
export class GlAccountCategoryDeleteDialogComponent {
  glAccountCategory?: IGlAccountCategory;

  constructor(protected glAccountCategoryService: GlAccountCategoryService, protected activeModal: NgbActiveModal) {}

  cancel(): void {
    this.activeModal.dismiss();
  }

  confirmDelete(id: number): void {
    this.glAccountCategoryService.delete(id).subscribe(() => {
      this.activeModal.close('deleted');
    });
  }
}
