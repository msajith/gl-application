import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import dayjs from 'dayjs/esm';

import { isPresent } from 'app/core/util/operators';
import { DATE_FORMAT } from 'app/config/input.constants';
import { ApplicationConfigService } from 'app/core/config/application-config.service';
import { createRequestOption } from 'app/core/request/request-util';
import { IFxRate, getFxRateIdentifier } from '../fx-rate.model';

export type EntityResponseType = HttpResponse<IFxRate>;
export type EntityArrayResponseType = HttpResponse<IFxRate[]>;

@Injectable({ providedIn: 'root' })
export class FxRateService {
  protected resourceUrl = this.applicationConfigService.getEndpointFor('api/fx-rates');
  protected resourceUrlv1 = this.applicationConfigService.getEndpointFor('api/v1/fx-rates');

  constructor(protected http: HttpClient, protected applicationConfigService: ApplicationConfigService) {}

  create(fxRate: IFxRate): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(fxRate);
    return this.http
      .post<IFxRate>(this.resourceUrl, copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  update(fxRate: IFxRate): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(fxRate);
    return this.http
      .put<IFxRate>(`${this.resourceUrl}/${getFxRateIdentifier(fxRate) as number}`, copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  partialUpdate(fxRate: IFxRate): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(fxRate);
    return this.http
      .patch<IFxRate>(`${this.resourceUrl}/${getFxRateIdentifier(fxRate) as number}`, copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http
      .get<IFxRate>(`${this.resourceUrl}/${id}`, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }
  getRate(baseccy: string, quoteccy: string): Observable<IFxRate> {
    return this.http.get<IFxRate>(`${this.resourceUrlv1}/${baseccy}/${quoteccy}`);
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http
      .get<IFxRate[]>(this.resourceUrl, { params: options, observe: 'response' })
      .pipe(map((res: EntityArrayResponseType) => this.convertDateArrayFromServer(res)));
  }

  delete(id: number): Observable<HttpResponse<{}>> {
    return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  addFxRateToCollectionIfMissing(fxRateCollection: IFxRate[], ...fxRatesToCheck: (IFxRate | null | undefined)[]): IFxRate[] {
    const fxRates: IFxRate[] = fxRatesToCheck.filter(isPresent);
    if (fxRates.length > 0) {
      const fxRateCollectionIdentifiers = fxRateCollection.map(fxRateItem => getFxRateIdentifier(fxRateItem)!);
      const fxRatesToAdd = fxRates.filter(fxRateItem => {
        const fxRateIdentifier = getFxRateIdentifier(fxRateItem);
        if (fxRateIdentifier == null || fxRateCollectionIdentifiers.includes(fxRateIdentifier)) {
          return false;
        }
        fxRateCollectionIdentifiers.push(fxRateIdentifier);
        return true;
      });
      return [...fxRatesToAdd, ...fxRateCollection];
    }
    return fxRateCollection;
  }

  protected convertDateFromClient(fxRate: IFxRate): IFxRate {
    return Object.assign({}, fxRate, {
      validfrom: fxRate.validfrom?.isValid() ? fxRate.validfrom.format(DATE_FORMAT) : undefined,
      validTo: fxRate.validTo?.isValid() ? fxRate.validTo.format(DATE_FORMAT) : undefined,
    });
  }

  protected convertDateFromServer(res: EntityResponseType): EntityResponseType {
    if (res.body) {
      res.body.validfrom = res.body.validfrom ? dayjs(res.body.validfrom) : undefined;
      res.body.validTo = res.body.validTo ? dayjs(res.body.validTo) : undefined;
    }
    return res;
  }

  protected convertDateArrayFromServer(res: EntityArrayResponseType): EntityArrayResponseType {
    if (res.body) {
      res.body.forEach((fxRate: IFxRate) => {
        fxRate.validfrom = fxRate.validfrom ? dayjs(fxRate.validfrom) : undefined;
        fxRate.validTo = fxRate.validTo ? dayjs(fxRate.validTo) : undefined;
      });
    }
    return res;
  }
}
