import { TestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import dayjs from 'dayjs/esm';

import { DATE_FORMAT } from 'app/config/input.constants';
import { FxRecordStatus } from 'app/entities/enumerations/fx-record-status.model';
import { IFxRate, FxRate } from '../fx-rate.model';

import { FxRateService } from './fx-rate.service';

describe('FxRate Service', () => {
  let service: FxRateService;
  let httpMock: HttpTestingController;
  let elemDefault: IFxRate;
  let expectedResult: IFxRate | IFxRate[] | boolean | null;
  let currentDate: dayjs.Dayjs;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
    });
    expectedResult = null;
    service = TestBed.inject(FxRateService);
    httpMock = TestBed.inject(HttpTestingController);
    currentDate = dayjs();

    elemDefault = {
      id: 0,
      type: 'AAAAAAA',
      baseCurrency: 'AAAAAAA',
      quoteCurrency: 'AAAAAAA',
      askPrice: 0,
      bidPrice: 0,
      midPrice: 0,
      askMargin: 0,
      bidMargin: 0,
      segment: 'AAAAAAA',
      amountSlab: 'AAAAAAA',
      amountSlabValue: 0,
      amountSlabOperator: 'AAAAAAA',
      amountSlabCurrency: 'AAAAAAA',
      recordStatus: FxRecordStatus.ACTIVE,
      validfrom: currentDate,
      validTo: currentDate,
    };
  });

  describe('Service methods', () => {
    it('should find an element', () => {
      const returnedFromService = Object.assign(
        {
          validfrom: currentDate.format(DATE_FORMAT),
          validTo: currentDate.format(DATE_FORMAT),
        },
        elemDefault
      );

      service.find(123).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'GET' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(elemDefault);
    });

    it('should create a FxRate', () => {
      const returnedFromService = Object.assign(
        {
          id: 0,
          validfrom: currentDate.format(DATE_FORMAT),
          validTo: currentDate.format(DATE_FORMAT),
        },
        elemDefault
      );

      const expected = Object.assign(
        {
          validfrom: currentDate,
          validTo: currentDate,
        },
        returnedFromService
      );

      service.create(new FxRate()).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'POST' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(expected);
    });

    it('should update a FxRate', () => {
      const returnedFromService = Object.assign(
        {
          id: 1,
          type: 'BBBBBB',
          baseCurrency: 'BBBBBB',
          quoteCurrency: 'BBBBBB',
          askPrice: 1,
          bidPrice: 1,
          midPrice: 1,
          askMargin: 1,
          bidMargin: 1,
          segment: 'BBBBBB',
          amountSlab: 'BBBBBB',
          amountSlabValue: 1,
          amountSlabOperator: 'BBBBBB',
          amountSlabCurrency: 'BBBBBB',
          recordStatus: 'BBBBBB',
          validfrom: currentDate.format(DATE_FORMAT),
          validTo: currentDate.format(DATE_FORMAT),
        },
        elemDefault
      );

      const expected = Object.assign(
        {
          validfrom: currentDate,
          validTo: currentDate,
        },
        returnedFromService
      );

      service.update(expected).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'PUT' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(expected);
    });

    it('should partial update a FxRate', () => {
      const patchObject = Object.assign(
        {
          askPrice: 1,
          askMargin: 1,
          amountSlab: 'BBBBBB',
          amountSlabOperator: 'BBBBBB',
          amountSlabCurrency: 'BBBBBB',
        },
        new FxRate()
      );

      const returnedFromService = Object.assign(patchObject, elemDefault);

      const expected = Object.assign(
        {
          validfrom: currentDate,
          validTo: currentDate,
        },
        returnedFromService
      );

      service.partialUpdate(patchObject).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'PATCH' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(expected);
    });

    it('should return a list of FxRate', () => {
      const returnedFromService = Object.assign(
        {
          id: 1,
          type: 'BBBBBB',
          baseCurrency: 'BBBBBB',
          quoteCurrency: 'BBBBBB',
          askPrice: 1,
          bidPrice: 1,
          midPrice: 1,
          askMargin: 1,
          bidMargin: 1,
          segment: 'BBBBBB',
          amountSlab: 'BBBBBB',
          amountSlabValue: 1,
          amountSlabOperator: 'BBBBBB',
          amountSlabCurrency: 'BBBBBB',
          recordStatus: 'BBBBBB',
          validfrom: currentDate.format(DATE_FORMAT),
          validTo: currentDate.format(DATE_FORMAT),
        },
        elemDefault
      );

      const expected = Object.assign(
        {
          validfrom: currentDate,
          validTo: currentDate,
        },
        returnedFromService
      );

      service.query().subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'GET' });
      req.flush([returnedFromService]);
      httpMock.verify();
      expect(expectedResult).toContainEqual(expected);
    });

    it('should delete a FxRate', () => {
      service.delete(123).subscribe(resp => (expectedResult = resp.ok));

      const req = httpMock.expectOne({ method: 'DELETE' });
      req.flush({ status: 200 });
      expect(expectedResult);
    });

    describe('addFxRateToCollectionIfMissing', () => {
      it('should add a FxRate to an empty array', () => {
        const fxRate: IFxRate = { id: 123 };
        expectedResult = service.addFxRateToCollectionIfMissing([], fxRate);
        expect(expectedResult).toHaveLength(1);
        expect(expectedResult).toContain(fxRate);
      });

      it('should not add a FxRate to an array that contains it', () => {
        const fxRate: IFxRate = { id: 123 };
        const fxRateCollection: IFxRate[] = [
          {
            ...fxRate,
          },
          { id: 456 },
        ];
        expectedResult = service.addFxRateToCollectionIfMissing(fxRateCollection, fxRate);
        expect(expectedResult).toHaveLength(2);
      });

      it("should add a FxRate to an array that doesn't contain it", () => {
        const fxRate: IFxRate = { id: 123 };
        const fxRateCollection: IFxRate[] = [{ id: 456 }];
        expectedResult = service.addFxRateToCollectionIfMissing(fxRateCollection, fxRate);
        expect(expectedResult).toHaveLength(2);
        expect(expectedResult).toContain(fxRate);
      });

      it('should add only unique FxRate to an array', () => {
        const fxRateArray: IFxRate[] = [{ id: 123 }, { id: 456 }, { id: 99116 }];
        const fxRateCollection: IFxRate[] = [{ id: 123 }];
        expectedResult = service.addFxRateToCollectionIfMissing(fxRateCollection, ...fxRateArray);
        expect(expectedResult).toHaveLength(3);
      });

      it('should accept varargs', () => {
        const fxRate: IFxRate = { id: 123 };
        const fxRate2: IFxRate = { id: 456 };
        expectedResult = service.addFxRateToCollectionIfMissing([], fxRate, fxRate2);
        expect(expectedResult).toHaveLength(2);
        expect(expectedResult).toContain(fxRate);
        expect(expectedResult).toContain(fxRate2);
      });

      it('should accept null and undefined values', () => {
        const fxRate: IFxRate = { id: 123 };
        expectedResult = service.addFxRateToCollectionIfMissing([], null, fxRate, undefined);
        expect(expectedResult).toHaveLength(1);
        expect(expectedResult).toContain(fxRate);
      });

      it('should return initial array if no FxRate is added', () => {
        const fxRateCollection: IFxRate[] = [{ id: 123 }];
        expectedResult = service.addFxRateToCollectionIfMissing(fxRateCollection, undefined, null);
        expect(expectedResult).toEqual(fxRateCollection);
      });
    });
  });

  afterEach(() => {
    httpMock.verify();
  });
});
