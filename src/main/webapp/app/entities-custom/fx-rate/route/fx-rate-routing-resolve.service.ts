import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, Router } from '@angular/router';
import { Observable, of, EMPTY } from 'rxjs';
import { mergeMap } from 'rxjs/operators';

import { IFxRate, FxRate } from '../fx-rate.model';
import { FxRateService } from '../service/fx-rate.service';

@Injectable({ providedIn: 'root' })
export class FxRateRoutingResolveService implements Resolve<IFxRate> {
  constructor(protected service: FxRateService, protected router: Router) {}

  resolve(route: ActivatedRouteSnapshot): Observable<IFxRate> | Observable<never> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        mergeMap((fxRate: HttpResponse<FxRate>) => {
          if (fxRate.body) {
            return of(fxRate.body);
          } else {
            this.router.navigate(['404']);
            return EMPTY;
          }
        })
      );
    }
    return of(new FxRate());
  }
}
