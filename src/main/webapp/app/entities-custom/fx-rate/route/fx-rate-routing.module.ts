import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { UserRouteAccessService } from 'app/core/auth/user-route-access.service';
import { FxRateComponent } from '../list/fx-rate.component';
import { FxRateDetailComponent } from '../detail/fx-rate-detail.component';
import { FxRateUpdateComponent } from '../update/fx-rate-update.component';
import { FxRateRoutingResolveService } from './fx-rate-routing-resolve.service';

const fxRateRoute: Routes = [
  {
    path: '',
    component: FxRateComponent,
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/view',
    component: FxRateDetailComponent,
    resolve: {
      fxRate: FxRateRoutingResolveService,
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: 'new',
    component: FxRateUpdateComponent,
    resolve: {
      fxRate: FxRateRoutingResolveService,
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/edit',
    component: FxRateUpdateComponent,
    resolve: {
      fxRate: FxRateRoutingResolveService,
    },
    canActivate: [UserRouteAccessService],
  },
];

@NgModule({
  imports: [RouterModule.forChild(fxRateRoute)],
  exports: [RouterModule],
})
export class FxRateRoutingModule {}
