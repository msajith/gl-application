import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IFxRate } from '../fx-rate.model';

@Component({
  selector: 'jhi-fx-rate-detail',
  templateUrl: './fx-rate-detail.component.html',
})
export class FxRateDetailComponent implements OnInit {
  fxRate: IFxRate | null = null;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ fxRate }) => {
      this.fxRate = fxRate;
    });
  }

  previousState(): void {
    window.history.back();
  }
}
