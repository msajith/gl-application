import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

import { IFxRate } from '../fx-rate.model';
import { FxRateService } from '../service/fx-rate.service';
import { FxRateDeleteDialogComponent } from '../delete/fx-rate-delete-dialog.component';

@Component({
  selector: 'jhi-fx-rate',
  templateUrl: './fx-rate.component.html',
})
export class FxRateComponent implements OnInit {
  fxRates?: IFxRate[];
  isLoading = false;

  constructor(protected fxRateService: FxRateService, protected modalService: NgbModal) {}

  loadAll(): void {
    this.isLoading = true;

    this.fxRateService.query().subscribe({
      next: (res: HttpResponse<IFxRate[]>) => {
        this.isLoading = false;
        this.fxRates = res.body ?? [];
      },
      error: () => {
        this.isLoading = false;
      },
    });
  }

  ngOnInit(): void {
    this.loadAll();
  }

  trackId(index: number, item: IFxRate): number {
    return item.id!;
  }

  delete(fxRate: IFxRate): void {
    const modalRef = this.modalService.open(FxRateDeleteDialogComponent, { size: 'lg', backdrop: 'static' });
    modalRef.componentInstance.fxRate = fxRate;
    // unsubscribe not needed because closed completes on modal close
    modalRef.closed.subscribe(reason => {
      if (reason === 'deleted') {
        this.loadAll();
      }
    });
  }
}
