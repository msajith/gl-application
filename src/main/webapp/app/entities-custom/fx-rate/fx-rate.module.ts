import { NgModule } from '@angular/core';
import { SharedModule } from 'app/shared/shared.module';
import { FxRateComponent } from './list/fx-rate.component';
import { FxRateDetailComponent } from './detail/fx-rate-detail.component';
import { FxRateUpdateComponent } from './update/fx-rate-update.component';
import { FxRateDeleteDialogComponent } from './delete/fx-rate-delete-dialog.component';
import { FxRateRoutingModule } from './route/fx-rate-routing.module';

@NgModule({
  imports: [SharedModule, FxRateRoutingModule],
  declarations: [FxRateComponent, FxRateDetailComponent, FxRateUpdateComponent, FxRateDeleteDialogComponent],
  entryComponents: [FxRateDeleteDialogComponent],
})
export class FxRateModule {}
