import { Component } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

import { IFxRate } from '../fx-rate.model';
import { FxRateService } from '../service/fx-rate.service';

@Component({
  templateUrl: './fx-rate-delete-dialog.component.html',
})
export class FxRateDeleteDialogComponent {
  fxRate?: IFxRate;

  constructor(protected fxRateService: FxRateService, protected activeModal: NgbActiveModal) {}

  cancel(): void {
    this.activeModal.dismiss();
  }

  confirmDelete(id: number): void {
    this.fxRateService.delete(id).subscribe(() => {
      this.activeModal.close('deleted');
    });
  }
}
