import { ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpHeaders, HttpResponse } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { of } from 'rxjs';

import { ChargeService } from '../service/charge.service';

import { ChargeComponent } from './charge.component';

describe('Charge Management Component', () => {
  let comp: ChargeComponent;
  let fixture: ComponentFixture<ChargeComponent>;
  let service: ChargeService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      declarations: [ChargeComponent],
    })
      .overrideTemplate(ChargeComponent, '')
      .compileComponents();

    fixture = TestBed.createComponent(ChargeComponent);
    comp = fixture.componentInstance;
    service = TestBed.inject(ChargeService);

    const headers = new HttpHeaders();
    jest.spyOn(service, 'query').mockReturnValue(
      of(
        new HttpResponse({
          body: [{ id: 123 }],
          headers,
        })
      )
    );
  });

  it('Should call load all on init', () => {
    // WHEN
    comp.ngOnInit();

    // THEN
    expect(service.query).toHaveBeenCalled();
    expect(comp.charges?.[0]).toEqual(expect.objectContaining({ id: 123 }));
  });
});
