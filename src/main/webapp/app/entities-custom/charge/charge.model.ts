import dayjs from 'dayjs/esm';
import { IChargeType } from 'app/entities/charge-type/charge-type.model';
import { ITransactionType } from 'app/entities/transaction-type/transaction-type.model';
import { ICountry } from 'app/entities/country/country.model';
import { ICurrency } from 'app/entities/currency/currency.model';
import { ICasaAccount } from 'app/entities/casa-account/casa-account.model';
import { ICustomerSegment } from 'app/entities/customer-segment/customer-segment.model';
import { RecordStatus } from 'app/entities/enumerations/record-status.model';

export interface ICharge {
  id?: number;
  chargeAmountLocalCcy?: number | null;
  chargeAmountTxnCcy?: number | null;
  amountSlab?: string | null;
  amountSlabValue?: number | null;
  amountSlabOperator?: string | null;
  amountSlabCurrency?: string | null;
  recordStatus?: RecordStatus | null;
  validfrom?: dayjs.Dayjs | null;
  validTo?: dayjs.Dayjs | null;
  chargeType?: IChargeType | null;
  transactionType?: ITransactionType | null;
  txnCountry?: ICountry | null;
  beneficiaryCountry?: ICountry | null;
  baseCurrency?: ICurrency | null;
  txnCurrency?: ICurrency | null;
  chargeAccount?: ICasaAccount | null;
  customerSegment?: ICustomerSegment | null;
}

export class Charge implements ICharge {
  constructor(
    public id?: number,
    public chargeAmountLocalCcy?: number | null,
    public chargeAmountTxnCcy?: number | null,
    public amountSlab?: string | null,
    public amountSlabValue?: number | null,
    public amountSlabOperator?: string | null,
    public amountSlabCurrency?: string | null,
    public recordStatus?: RecordStatus | null,
    public validfrom?: dayjs.Dayjs | null,
    public validTo?: dayjs.Dayjs | null,
    public chargeType?: IChargeType | null,
    public transactionType?: ITransactionType | null,
    public txnCountry?: ICountry | null,
    public beneficiaryCountry?: ICountry | null,
    public baseCurrency?: ICurrency | null,
    public txnCurrency?: ICurrency | null,
    public chargeAccount?: ICasaAccount | null,
    public customerSegment?: ICustomerSegment | null
  ) {}
}

export function getChargeIdentifier(charge: ICharge): number | undefined {
  return charge.id;
}
