import { ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { of, Subject, from } from 'rxjs';

import { ChargeService } from '../service/charge.service';
import { ICharge, Charge } from '../charge.model';
import { IChargeType } from 'app/entities/charge-type/charge-type.model';
import { ChargeTypeService } from 'app/entities/charge-type/service/charge-type.service';
import { ITransactionType } from 'app/entities/transaction-type/transaction-type.model';
import { TransactionTypeService } from 'app/entities/transaction-type/service/transaction-type.service';
import { ICountry } from 'app/entities/country/country.model';
import { CountryService } from 'app/entities/country/service/country.service';
import { ICurrency } from 'app/entities/currency/currency.model';
import { CurrencyService } from 'app/entities/currency/service/currency.service';
import { ICasaAccount } from 'app/entities/casa-account/casa-account.model';
import { CasaAccountService } from 'app/entities/casa-account/service/casa-account.service';
import { ICustomerSegment } from 'app/entities/customer-segment/customer-segment.model';
import { CustomerSegmentService } from 'app/entities/customer-segment/service/customer-segment.service';

import { ChargeUpdateComponent } from './charge-update.component';

describe('Charge Management Update Component', () => {
  let comp: ChargeUpdateComponent;
  let fixture: ComponentFixture<ChargeUpdateComponent>;
  let activatedRoute: ActivatedRoute;
  let chargeService: ChargeService;
  let chargeTypeService: ChargeTypeService;
  let transactionTypeService: TransactionTypeService;
  let countryService: CountryService;
  let currencyService: CurrencyService;
  let casaAccountService: CasaAccountService;
  let customerSegmentService: CustomerSegmentService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule, RouterTestingModule.withRoutes([])],
      declarations: [ChargeUpdateComponent],
      providers: [
        FormBuilder,
        {
          provide: ActivatedRoute,
          useValue: {
            params: from([{}]),
          },
        },
      ],
    })
      .overrideTemplate(ChargeUpdateComponent, '')
      .compileComponents();

    fixture = TestBed.createComponent(ChargeUpdateComponent);
    activatedRoute = TestBed.inject(ActivatedRoute);
    chargeService = TestBed.inject(ChargeService);
    chargeTypeService = TestBed.inject(ChargeTypeService);
    transactionTypeService = TestBed.inject(TransactionTypeService);
    countryService = TestBed.inject(CountryService);
    currencyService = TestBed.inject(CurrencyService);
    casaAccountService = TestBed.inject(CasaAccountService);
    customerSegmentService = TestBed.inject(CustomerSegmentService);

    comp = fixture.componentInstance;
  });

  describe('ngOnInit', () => {
    it('Should call ChargeType query and add missing value', () => {
      const charge: ICharge = { id: 456 };
      const chargeType: IChargeType = { id: 59587 };
      charge.chargeType = chargeType;

      const chargeTypeCollection: IChargeType[] = [{ id: 46248 }];
      jest.spyOn(chargeTypeService, 'query').mockReturnValue(of(new HttpResponse({ body: chargeTypeCollection })));
      const additionalChargeTypes = [chargeType];
      const expectedCollection: IChargeType[] = [...additionalChargeTypes, ...chargeTypeCollection];
      jest.spyOn(chargeTypeService, 'addChargeTypeToCollectionIfMissing').mockReturnValue(expectedCollection);

      activatedRoute.data = of({ charge });
      comp.ngOnInit();

      expect(chargeTypeService.query).toHaveBeenCalled();
      expect(chargeTypeService.addChargeTypeToCollectionIfMissing).toHaveBeenCalledWith(chargeTypeCollection, ...additionalChargeTypes);
      expect(comp.chargeTypesSharedCollection).toEqual(expectedCollection);
    });

    it('Should call TransactionType query and add missing value', () => {
      const charge: ICharge = { id: 456 };
      const transactionType: ITransactionType = { id: 86764 };
      charge.transactionType = transactionType;

      const transactionTypeCollection: ITransactionType[] = [{ id: 48002 }];
      jest.spyOn(transactionTypeService, 'query').mockReturnValue(of(new HttpResponse({ body: transactionTypeCollection })));
      const additionalTransactionTypes = [transactionType];
      const expectedCollection: ITransactionType[] = [...additionalTransactionTypes, ...transactionTypeCollection];
      jest.spyOn(transactionTypeService, 'addTransactionTypeToCollectionIfMissing').mockReturnValue(expectedCollection);

      activatedRoute.data = of({ charge });
      comp.ngOnInit();

      expect(transactionTypeService.query).toHaveBeenCalled();
      expect(transactionTypeService.addTransactionTypeToCollectionIfMissing).toHaveBeenCalledWith(
        transactionTypeCollection,
        ...additionalTransactionTypes
      );
      expect(comp.transactionTypesSharedCollection).toEqual(expectedCollection);
    });

    it('Should call Country query and add missing value', () => {
      const charge: ICharge = { id: 456 };
      const txnCountry: ICountry = { id: 49615 };
      charge.txnCountry = txnCountry;
      const beneficiaryCountry: ICountry = { id: 36042 };
      charge.beneficiaryCountry = beneficiaryCountry;

      const countryCollection: ICountry[] = [{ id: 35526 }];
      jest.spyOn(countryService, 'query').mockReturnValue(of(new HttpResponse({ body: countryCollection })));
      const additionalCountries = [txnCountry, beneficiaryCountry];
      const expectedCollection: ICountry[] = [...additionalCountries, ...countryCollection];
      jest.spyOn(countryService, 'addCountryToCollectionIfMissing').mockReturnValue(expectedCollection);

      activatedRoute.data = of({ charge });
      comp.ngOnInit();

      expect(countryService.query).toHaveBeenCalled();
      expect(countryService.addCountryToCollectionIfMissing).toHaveBeenCalledWith(countryCollection, ...additionalCountries);
      expect(comp.countriesSharedCollection).toEqual(expectedCollection);
    });

    it('Should call Currency query and add missing value', () => {
      const charge: ICharge = { id: 456 };
      const baseCurrency: ICurrency = { id: 49634 };
      charge.baseCurrency = baseCurrency;
      const txnCurrency: ICurrency = { id: 52206 };
      charge.txnCurrency = txnCurrency;

      const currencyCollection: ICurrency[] = [{ id: 56347 }];
      jest.spyOn(currencyService, 'query').mockReturnValue(of(new HttpResponse({ body: currencyCollection })));
      const additionalCurrencies = [baseCurrency, txnCurrency];
      const expectedCollection: ICurrency[] = [...additionalCurrencies, ...currencyCollection];
      jest.spyOn(currencyService, 'addCurrencyToCollectionIfMissing').mockReturnValue(expectedCollection);

      activatedRoute.data = of({ charge });
      comp.ngOnInit();

      expect(currencyService.query).toHaveBeenCalled();
      expect(currencyService.addCurrencyToCollectionIfMissing).toHaveBeenCalledWith(currencyCollection, ...additionalCurrencies);
      expect(comp.currenciesSharedCollection).toEqual(expectedCollection);
    });

    it('Should call CasaAccount query and add missing value', () => {
      const charge: ICharge = { id: 456 };
      const chargeAccount: ICasaAccount = { id: 85839 };
      charge.chargeAccount = chargeAccount;

      const casaAccountCollection: ICasaAccount[] = [{ id: 79590 }];
      jest.spyOn(casaAccountService, 'query').mockReturnValue(of(new HttpResponse({ body: casaAccountCollection })));
      const additionalCasaAccounts = [chargeAccount];
      const expectedCollection: ICasaAccount[] = [...additionalCasaAccounts, ...casaAccountCollection];
      jest.spyOn(casaAccountService, 'addCasaAccountToCollectionIfMissing').mockReturnValue(expectedCollection);

      activatedRoute.data = of({ charge });
      comp.ngOnInit();

      expect(casaAccountService.query).toHaveBeenCalled();
      expect(casaAccountService.addCasaAccountToCollectionIfMissing).toHaveBeenCalledWith(casaAccountCollection, ...additionalCasaAccounts);
      expect(comp.casaAccountsSharedCollection).toEqual(expectedCollection);
    });

    it('Should call CustomerSegment query and add missing value', () => {
      const charge: ICharge = { id: 456 };
      const customerSegment: ICustomerSegment = { id: 25447 };
      charge.customerSegment = customerSegment;

      const customerSegmentCollection: ICustomerSegment[] = [{ id: 84005 }];
      jest.spyOn(customerSegmentService, 'query').mockReturnValue(of(new HttpResponse({ body: customerSegmentCollection })));
      const additionalCustomerSegments = [customerSegment];
      const expectedCollection: ICustomerSegment[] = [...additionalCustomerSegments, ...customerSegmentCollection];
      jest.spyOn(customerSegmentService, 'addCustomerSegmentToCollectionIfMissing').mockReturnValue(expectedCollection);

      activatedRoute.data = of({ charge });
      comp.ngOnInit();

      expect(customerSegmentService.query).toHaveBeenCalled();
      expect(customerSegmentService.addCustomerSegmentToCollectionIfMissing).toHaveBeenCalledWith(
        customerSegmentCollection,
        ...additionalCustomerSegments
      );
      expect(comp.customerSegmentsSharedCollection).toEqual(expectedCollection);
    });

    it('Should update editForm', () => {
      const charge: ICharge = { id: 456 };
      const chargeType: IChargeType = { id: 7920 };
      charge.chargeType = chargeType;
      const transactionType: ITransactionType = { id: 99064 };
      charge.transactionType = transactionType;
      const txnCountry: ICountry = { id: 2858 };
      charge.txnCountry = txnCountry;
      const beneficiaryCountry: ICountry = { id: 97621 };
      charge.beneficiaryCountry = beneficiaryCountry;
      const baseCurrency: ICurrency = { id: 80855 };
      charge.baseCurrency = baseCurrency;
      const txnCurrency: ICurrency = { id: 45108 };
      charge.txnCurrency = txnCurrency;
      const chargeAccount: ICasaAccount = { id: 6319 };
      charge.chargeAccount = chargeAccount;
      const customerSegment: ICustomerSegment = { id: 63910 };
      charge.customerSegment = customerSegment;

      activatedRoute.data = of({ charge });
      comp.ngOnInit();

      expect(comp.editForm.value).toEqual(expect.objectContaining(charge));
      expect(comp.chargeTypesSharedCollection).toContain(chargeType);
      expect(comp.transactionTypesSharedCollection).toContain(transactionType);
      expect(comp.countriesSharedCollection).toContain(txnCountry);
      expect(comp.countriesSharedCollection).toContain(beneficiaryCountry);
      expect(comp.currenciesSharedCollection).toContain(baseCurrency);
      expect(comp.currenciesSharedCollection).toContain(txnCurrency);
      expect(comp.casaAccountsSharedCollection).toContain(chargeAccount);
      expect(comp.customerSegmentsSharedCollection).toContain(customerSegment);
    });
  });

  describe('save', () => {
    it('Should call update service on save for existing entity', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<Charge>>();
      const charge = { id: 123 };
      jest.spyOn(chargeService, 'update').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ charge });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.next(new HttpResponse({ body: charge }));
      saveSubject.complete();

      // THEN
      expect(comp.previousState).toHaveBeenCalled();
      expect(chargeService.update).toHaveBeenCalledWith(charge);
      expect(comp.isSaving).toEqual(false);
    });

    it('Should call create service on save for new entity', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<Charge>>();
      const charge = new Charge();
      jest.spyOn(chargeService, 'create').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ charge });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.next(new HttpResponse({ body: charge }));
      saveSubject.complete();

      // THEN
      expect(chargeService.create).toHaveBeenCalledWith(charge);
      expect(comp.isSaving).toEqual(false);
      expect(comp.previousState).toHaveBeenCalled();
    });

    it('Should set isSaving to false on error', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<Charge>>();
      const charge = { id: 123 };
      jest.spyOn(chargeService, 'update').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ charge });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.error('This is an error!');

      // THEN
      expect(chargeService.update).toHaveBeenCalledWith(charge);
      expect(comp.isSaving).toEqual(false);
      expect(comp.previousState).not.toHaveBeenCalled();
    });
  });

  describe('Tracking relationships identifiers', () => {
    describe('trackChargeTypeById', () => {
      it('Should return tracked ChargeType primary key', () => {
        const entity = { id: 123 };
        const trackResult = comp.trackChargeTypeById(0, entity);
        expect(trackResult).toEqual(entity.id);
      });
    });

    describe('trackTransactionTypeById', () => {
      it('Should return tracked TransactionType primary key', () => {
        const entity = { id: 123 };
        const trackResult = comp.trackTransactionTypeById(0, entity);
        expect(trackResult).toEqual(entity.id);
      });
    });

    describe('trackCountryById', () => {
      it('Should return tracked Country primary key', () => {
        const entity = { id: 123 };
        const trackResult = comp.trackCountryById(0, entity);
        expect(trackResult).toEqual(entity.id);
      });
    });

    describe('trackCurrencyById', () => {
      it('Should return tracked Currency primary key', () => {
        const entity = { id: 123 };
        const trackResult = comp.trackCurrencyById(0, entity);
        expect(trackResult).toEqual(entity.id);
      });
    });

    describe('trackCasaAccountById', () => {
      it('Should return tracked CasaAccount primary key', () => {
        const entity = { id: 123 };
        const trackResult = comp.trackCasaAccountById(0, entity);
        expect(trackResult).toEqual(entity.id);
      });
    });

    describe('trackCustomerSegmentById', () => {
      it('Should return tracked CustomerSegment primary key', () => {
        const entity = { id: 123 };
        const trackResult = comp.trackCustomerSegmentById(0, entity);
        expect(trackResult).toEqual(entity.id);
      });
    });
  });
});
