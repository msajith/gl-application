import { TestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import dayjs from 'dayjs/esm';

import { DATE_FORMAT } from 'app/config/input.constants';
import { RecordStatus } from 'app/entities/enumerations/record-status.model';
import { ICharge, Charge } from '../charge.model';

import { ChargeService } from './charge.service';

describe('Charge Service', () => {
  let service: ChargeService;
  let httpMock: HttpTestingController;
  let elemDefault: ICharge;
  let expectedResult: ICharge | ICharge[] | boolean | null;
  let currentDate: dayjs.Dayjs;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
    });
    expectedResult = null;
    service = TestBed.inject(ChargeService);
    httpMock = TestBed.inject(HttpTestingController);
    currentDate = dayjs();

    elemDefault = {
      id: 0,
      chargeAmountLocalCcy: 0,
      chargeAmountTxnCcy: 0,
      amountSlab: 'AAAAAAA',
      amountSlabValue: 0,
      amountSlabOperator: 'AAAAAAA',
      amountSlabCurrency: 'AAAAAAA',
      recordStatus: RecordStatus.ACTIVE,
      validfrom: currentDate,
      validTo: currentDate,
    };
  });

  describe('Service methods', () => {
    it('should find an element', () => {
      const returnedFromService = Object.assign(
        {
          validfrom: currentDate.format(DATE_FORMAT),
          validTo: currentDate.format(DATE_FORMAT),
        },
        elemDefault
      );

      service.find(123).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'GET' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(elemDefault);
    });

    it('should create a Charge', () => {
      const returnedFromService = Object.assign(
        {
          id: 0,
          validfrom: currentDate.format(DATE_FORMAT),
          validTo: currentDate.format(DATE_FORMAT),
        },
        elemDefault
      );

      const expected = Object.assign(
        {
          validfrom: currentDate,
          validTo: currentDate,
        },
        returnedFromService
      );

      service.create(new Charge()).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'POST' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(expected);
    });

    it('should update a Charge', () => {
      const returnedFromService = Object.assign(
        {
          id: 1,
          chargeAmountLocalCcy: 1,
          chargeAmountTxnCcy: 1,
          amountSlab: 'BBBBBB',
          amountSlabValue: 1,
          amountSlabOperator: 'BBBBBB',
          amountSlabCurrency: 'BBBBBB',
          recordStatus: 'BBBBBB',
          validfrom: currentDate.format(DATE_FORMAT),
          validTo: currentDate.format(DATE_FORMAT),
        },
        elemDefault
      );

      const expected = Object.assign(
        {
          validfrom: currentDate,
          validTo: currentDate,
        },
        returnedFromService
      );

      service.update(expected).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'PUT' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(expected);
    });

    it('should partial update a Charge', () => {
      const patchObject = Object.assign(
        {
          chargeAmountLocalCcy: 1,
          chargeAmountTxnCcy: 1,
          recordStatus: 'BBBBBB',
          validfrom: currentDate.format(DATE_FORMAT),
          validTo: currentDate.format(DATE_FORMAT),
        },
        new Charge()
      );

      const returnedFromService = Object.assign(patchObject, elemDefault);

      const expected = Object.assign(
        {
          validfrom: currentDate,
          validTo: currentDate,
        },
        returnedFromService
      );

      service.partialUpdate(patchObject).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'PATCH' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(expected);
    });

    it('should return a list of Charge', () => {
      const returnedFromService = Object.assign(
        {
          id: 1,
          chargeAmountLocalCcy: 1,
          chargeAmountTxnCcy: 1,
          amountSlab: 'BBBBBB',
          amountSlabValue: 1,
          amountSlabOperator: 'BBBBBB',
          amountSlabCurrency: 'BBBBBB',
          recordStatus: 'BBBBBB',
          validfrom: currentDate.format(DATE_FORMAT),
          validTo: currentDate.format(DATE_FORMAT),
        },
        elemDefault
      );

      const expected = Object.assign(
        {
          validfrom: currentDate,
          validTo: currentDate,
        },
        returnedFromService
      );

      service.query().subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'GET' });
      req.flush([returnedFromService]);
      httpMock.verify();
      expect(expectedResult).toContainEqual(expected);
    });

    it('should delete a Charge', () => {
      service.delete(123).subscribe(resp => (expectedResult = resp.ok));

      const req = httpMock.expectOne({ method: 'DELETE' });
      req.flush({ status: 200 });
      expect(expectedResult);
    });

    describe('addChargeToCollectionIfMissing', () => {
      it('should add a Charge to an empty array', () => {
        const charge: ICharge = { id: 123 };
        expectedResult = service.addChargeToCollectionIfMissing([], charge);
        expect(expectedResult).toHaveLength(1);
        expect(expectedResult).toContain(charge);
      });

      it('should not add a Charge to an array that contains it', () => {
        const charge: ICharge = { id: 123 };
        const chargeCollection: ICharge[] = [
          {
            ...charge,
          },
          { id: 456 },
        ];
        expectedResult = service.addChargeToCollectionIfMissing(chargeCollection, charge);
        expect(expectedResult).toHaveLength(2);
      });

      it("should add a Charge to an array that doesn't contain it", () => {
        const charge: ICharge = { id: 123 };
        const chargeCollection: ICharge[] = [{ id: 456 }];
        expectedResult = service.addChargeToCollectionIfMissing(chargeCollection, charge);
        expect(expectedResult).toHaveLength(2);
        expect(expectedResult).toContain(charge);
      });

      it('should add only unique Charge to an array', () => {
        const chargeArray: ICharge[] = [{ id: 123 }, { id: 456 }, { id: 66245 }];
        const chargeCollection: ICharge[] = [{ id: 123 }];
        expectedResult = service.addChargeToCollectionIfMissing(chargeCollection, ...chargeArray);
        expect(expectedResult).toHaveLength(3);
      });

      it('should accept varargs', () => {
        const charge: ICharge = { id: 123 };
        const charge2: ICharge = { id: 456 };
        expectedResult = service.addChargeToCollectionIfMissing([], charge, charge2);
        expect(expectedResult).toHaveLength(2);
        expect(expectedResult).toContain(charge);
        expect(expectedResult).toContain(charge2);
      });

      it('should accept null and undefined values', () => {
        const charge: ICharge = { id: 123 };
        expectedResult = service.addChargeToCollectionIfMissing([], null, charge, undefined);
        expect(expectedResult).toHaveLength(1);
        expect(expectedResult).toContain(charge);
      });

      it('should return initial array if no Charge is added', () => {
        const chargeCollection: ICharge[] = [{ id: 123 }];
        expectedResult = service.addChargeToCollectionIfMissing(chargeCollection, undefined, null);
        expect(expectedResult).toEqual(chargeCollection);
      });
    });
  });

  afterEach(() => {
    httpMock.verify();
  });
});
