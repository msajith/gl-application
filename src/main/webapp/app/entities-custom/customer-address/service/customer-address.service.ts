import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import dayjs from 'dayjs/esm';

import { isPresent } from 'app/core/util/operators';
import { ApplicationConfigService } from 'app/core/config/application-config.service';
import { createRequestOption } from 'app/core/request/request-util';
import { ICustomerAddress, getCustomerAddressIdentifier } from '../customer-address.model';

export type EntityResponseType = HttpResponse<ICustomerAddress>;
export type EntityArrayResponseType = HttpResponse<ICustomerAddress[]>;

@Injectable({ providedIn: 'root' })
export class CustomerAddressService {
  protected resourceUrl = this.applicationConfigService.getEndpointFor('api/customer-addresses');

  constructor(protected http: HttpClient, protected applicationConfigService: ApplicationConfigService) {}

  create(customerAddress: ICustomerAddress): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(customerAddress);
    return this.http
      .post<ICustomerAddress>(this.resourceUrl, copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  update(customerAddress: ICustomerAddress): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(customerAddress);
    return this.http
      .put<ICustomerAddress>(`${this.resourceUrl}/${getCustomerAddressIdentifier(customerAddress) as number}`, copy, {
        observe: 'response',
      })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  partialUpdate(customerAddress: ICustomerAddress): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(customerAddress);
    return this.http
      .patch<ICustomerAddress>(`${this.resourceUrl}/${getCustomerAddressIdentifier(customerAddress) as number}`, copy, {
        observe: 'response',
      })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http
      .get<ICustomerAddress>(`${this.resourceUrl}/${id}`, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http
      .get<ICustomerAddress[]>(this.resourceUrl, { params: options, observe: 'response' })
      .pipe(map((res: EntityArrayResponseType) => this.convertDateArrayFromServer(res)));
  }

  delete(id: number): Observable<HttpResponse<{}>> {
    return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  addCustomerAddressToCollectionIfMissing(
    customerAddressCollection: ICustomerAddress[],
    ...customerAddressesToCheck: (ICustomerAddress | null | undefined)[]
  ): ICustomerAddress[] {
    const customerAddresses: ICustomerAddress[] = customerAddressesToCheck.filter(isPresent);
    if (customerAddresses.length > 0) {
      const customerAddressCollectionIdentifiers = customerAddressCollection.map(
        customerAddressItem => getCustomerAddressIdentifier(customerAddressItem)!
      );
      const customerAddressesToAdd = customerAddresses.filter(customerAddressItem => {
        const customerAddressIdentifier = getCustomerAddressIdentifier(customerAddressItem);
        if (customerAddressIdentifier == null || customerAddressCollectionIdentifiers.includes(customerAddressIdentifier)) {
          return false;
        }
        customerAddressCollectionIdentifiers.push(customerAddressIdentifier);
        return true;
      });
      return [...customerAddressesToAdd, ...customerAddressCollection];
    }
    return customerAddressCollection;
  }

  protected convertDateFromClient(customerAddress: ICustomerAddress): ICustomerAddress {
    return Object.assign({}, customerAddress, {
      createdOn: customerAddress.createdOn?.isValid() ? customerAddress.createdOn.toJSON() : undefined,
    });
  }

  protected convertDateFromServer(res: EntityResponseType): EntityResponseType {
    if (res.body) {
      res.body.createdOn = res.body.createdOn ? dayjs(res.body.createdOn) : undefined;
    }
    return res;
  }

  protected convertDateArrayFromServer(res: EntityArrayResponseType): EntityArrayResponseType {
    if (res.body) {
      res.body.forEach((customerAddress: ICustomerAddress) => {
        customerAddress.createdOn = customerAddress.createdOn ? dayjs(customerAddress.createdOn) : undefined;
      });
    }
    return res;
  }
}
