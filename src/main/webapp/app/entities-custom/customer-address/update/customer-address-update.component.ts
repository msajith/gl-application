import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { finalize, map } from 'rxjs/operators';

import dayjs from 'dayjs/esm';
import { DATE_TIME_FORMAT } from 'app/config/input.constants';

import { ICustomerAddress, CustomerAddress } from '../customer-address.model';
import { CustomerAddressService } from '../service/customer-address.service';
import { ICountry } from 'app/entities/country/country.model';
import { CountryService } from 'app/entities/country/service/country.service';
import { ICustomer } from 'app/entities/customer/customer.model';
import { CustomerService } from 'app/entities/customer/service/customer.service';

@Component({
  selector: 'jhi-customer-address-update',
  templateUrl: './customer-address-update.component.html',
})
export class CustomerAddressUpdateComponent implements OnInit {
  isSaving = false;

  countriesSharedCollection: ICountry[] = [];
  customersSharedCollection: ICustomer[] = [];

  editForm = this.fb.group({
    id: [],
    title: [null, [Validators.required]],
    cif: [null, [Validators.required]],
    address1: [],
    address2: [],
    address3: [],
    city: [],
    createdOn: [],
    createdBy: [],
    country: [],
    customer: [],
  });

  constructor(
    protected customerAddressService: CustomerAddressService,
    protected countryService: CountryService,
    protected customerService: CustomerService,
    protected activatedRoute: ActivatedRoute,
    protected fb: FormBuilder
  ) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ customerAddress }) => {
      if (customerAddress.id === undefined) {
        const today = dayjs().startOf('day');
        customerAddress.createdOn = today;
      }

      this.updateForm(customerAddress);

      this.loadRelationshipsOptions();
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const customerAddress = this.createFromForm();
    if (customerAddress.id !== undefined) {
      this.subscribeToSaveResponse(this.customerAddressService.update(customerAddress));
    } else {
      this.subscribeToSaveResponse(this.customerAddressService.create(customerAddress));
    }
  }

  trackCountryById(index: number, item: ICountry): number {
    return item.id!;
  }

  trackCustomerById(index: number, item: ICustomer): number {
    return item.id!;
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<ICustomerAddress>>): void {
    result.pipe(finalize(() => this.onSaveFinalize())).subscribe({
      next: () => this.onSaveSuccess(),
      error: () => this.onSaveError(),
    });
  }

  protected onSaveSuccess(): void {
    this.previousState();
  }

  protected onSaveError(): void {
    // Api for inheritance.
  }

  protected onSaveFinalize(): void {
    this.isSaving = false;
  }

  protected updateForm(customerAddress: ICustomerAddress): void {
    this.editForm.patchValue({
      id: customerAddress.id,
      title: customerAddress.title,
      cif: customerAddress.cif,
      address1: customerAddress.address1,
      address2: customerAddress.address2,
      address3: customerAddress.address3,
      city: customerAddress.city,
      createdOn: customerAddress.createdOn ? customerAddress.createdOn.format(DATE_TIME_FORMAT) : null,
      createdBy: customerAddress.createdBy,
      country: customerAddress.country,
      customer: customerAddress.customer,
    });

    this.countriesSharedCollection = this.countryService.addCountryToCollectionIfMissing(
      this.countriesSharedCollection,
      customerAddress.country
    );
    this.customersSharedCollection = this.customerService.addCustomerToCollectionIfMissing(
      this.customersSharedCollection,
      customerAddress.customer
    );
  }

  protected loadRelationshipsOptions(): void {
    this.countryService
      .query()
      .pipe(map((res: HttpResponse<ICountry[]>) => res.body ?? []))
      .pipe(
        map((countries: ICountry[]) => this.countryService.addCountryToCollectionIfMissing(countries, this.editForm.get('country')!.value))
      )
      .subscribe((countries: ICountry[]) => (this.countriesSharedCollection = countries));

    this.customerService
      .query()
      .pipe(map((res: HttpResponse<ICustomer[]>) => res.body ?? []))
      .pipe(
        map((customers: ICustomer[]) =>
          this.customerService.addCustomerToCollectionIfMissing(customers, this.editForm.get('customer')!.value)
        )
      )
      .subscribe((customers: ICustomer[]) => (this.customersSharedCollection = customers));
  }

  protected createFromForm(): ICustomerAddress {
    return {
      ...new CustomerAddress(),
      id: this.editForm.get(['id'])!.value,
      title: this.editForm.get(['title'])!.value,
      cif: this.editForm.get(['cif'])!.value,
      address1: this.editForm.get(['address1'])!.value,
      address2: this.editForm.get(['address2'])!.value,
      address3: this.editForm.get(['address3'])!.value,
      city: this.editForm.get(['city'])!.value,
      createdOn: this.editForm.get(['createdOn'])!.value ? dayjs(this.editForm.get(['createdOn'])!.value, DATE_TIME_FORMAT) : undefined,
      createdBy: this.editForm.get(['createdBy'])!.value,
      country: this.editForm.get(['country'])!.value,
      customer: this.editForm.get(['customer'])!.value,
    };
  }
}
