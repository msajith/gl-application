import { ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpHeaders, HttpResponse } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { of } from 'rxjs';

import { CustomerAddressService } from '../service/customer-address.service';

import { CustomerAddressComponent } from './customer-address.component';

describe('CustomerAddress Management Component', () => {
  let comp: CustomerAddressComponent;
  let fixture: ComponentFixture<CustomerAddressComponent>;
  let service: CustomerAddressService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      declarations: [CustomerAddressComponent],
    })
      .overrideTemplate(CustomerAddressComponent, '')
      .compileComponents();

    fixture = TestBed.createComponent(CustomerAddressComponent);
    comp = fixture.componentInstance;
    service = TestBed.inject(CustomerAddressService);

    const headers = new HttpHeaders();
    jest.spyOn(service, 'query').mockReturnValue(
      of(
        new HttpResponse({
          body: [{ id: 123 }],
          headers,
        })
      )
    );
  });

  it('Should call load all on init', () => {
    // WHEN
    comp.ngOnInit();

    // THEN
    expect(service.query).toHaveBeenCalled();
    expect(comp.customerAddresses?.[0]).toEqual(expect.objectContaining({ id: 123 }));
  });
});
