import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { UserRouteAccessService } from 'app/core/auth/user-route-access.service';
import { RemittanceTransactionComponent } from '../list/remittance-transaction.component';
import { RemittanceTransactionDetailComponent } from '../detail/remittance-transaction-detail.component';
import { RemittanceTransactionUpdateComponent } from '../update/remittance-transaction-update.component';
import { RemittanceTransactionRoutingResolveService } from './remittance-transaction-routing-resolve.service';

const remittanceTransactionRoute: Routes = [
  {
    path: '',
    component: RemittanceTransactionComponent,
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/view',
    component: RemittanceTransactionDetailComponent,
    resolve: {
      remittanceTransaction: RemittanceTransactionRoutingResolveService,
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: 'new',
    component: RemittanceTransactionUpdateComponent,
    resolve: {
      remittanceTransaction: RemittanceTransactionRoutingResolveService,
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/edit',
    component: RemittanceTransactionUpdateComponent,
    resolve: {
      remittanceTransaction: RemittanceTransactionRoutingResolveService,
    },
    canActivate: [UserRouteAccessService],
  },
];

@NgModule({
  imports: [RouterModule.forChild(remittanceTransactionRoute)],
  exports: [RouterModule],
})
export class RemittanceTransactionRoutingModule {}
