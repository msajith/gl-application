import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, Router } from '@angular/router';
import { Observable, of, EMPTY } from 'rxjs';
import { mergeMap } from 'rxjs/operators';

import { IRemittanceTransaction, RemittanceTransaction } from '../remittance-transaction.model';
import { RemittanceTransactionService } from '../service/remittance-transaction.service';

@Injectable({ providedIn: 'root' })
export class RemittanceTransactionRoutingResolveService implements Resolve<IRemittanceTransaction> {
  constructor(protected service: RemittanceTransactionService, protected router: Router) {}

  resolve(route: ActivatedRouteSnapshot): Observable<IRemittanceTransaction> | Observable<never> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        mergeMap((remittanceTransaction: HttpResponse<RemittanceTransaction>) => {
          if (remittanceTransaction.body) {
            return of(remittanceTransaction.body);
          } else {
            this.router.navigate(['404']);
            return EMPTY;
          }
        })
      );
    }
    return of(new RemittanceTransaction());
  }
}
