import { NgModule } from '@angular/core';
import { SharedModule } from 'app/shared/shared.module';
import { RemittanceTransactionComponent } from './list/remittance-transaction.component';
import { RemittanceTransactionDetailComponent } from './detail/remittance-transaction-detail.component';
import { RemittanceTransactionUpdateComponent } from './update/remittance-transaction-update.component';
import { RemittanceTransactionDeleteDialogComponent } from './delete/remittance-transaction-delete-dialog.component';
import { RemittanceTransactionRoutingModule } from './route/remittance-transaction-routing.module';

@NgModule({
  imports: [SharedModule, RemittanceTransactionRoutingModule],
  declarations: [
    RemittanceTransactionComponent,
    RemittanceTransactionDetailComponent,
    RemittanceTransactionUpdateComponent,
    RemittanceTransactionDeleteDialogComponent,
  ],
  entryComponents: [RemittanceTransactionDeleteDialogComponent],
})
export class RemittanceTransactionModule {}
