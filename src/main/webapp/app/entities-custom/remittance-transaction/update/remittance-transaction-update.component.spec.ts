import { ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { of, Subject, from } from 'rxjs';

import { RemittanceTransactionService } from '../service/remittance-transaction.service';
import { IRemittanceTransaction, RemittanceTransaction } from '../remittance-transaction.model';
import { ICountry } from 'app/entities/country/country.model';
import { CountryService } from 'app/entities/country/service/country.service';
import { ICurrency } from 'app/entities/currency/currency.model';
import { CurrencyService } from 'app/entities/currency/service/currency.service';
import { IPurposeOfTxn } from 'app/entities/purpose-of-txn/purpose-of-txn.model';
import { PurposeOfTxnService } from 'app/entities/purpose-of-txn/service/purpose-of-txn.service';
import { IFundingSourceType } from 'app/entities/funding-source-type/funding-source-type.model';
import { FundingSourceTypeService } from 'app/entities/funding-source-type/service/funding-source-type.service';
import { ITransactionType } from 'app/entities/transaction-type/transaction-type.model';
import { TransactionTypeService } from 'app/entities/transaction-type/service/transaction-type.service';

import { RemittanceTransactionUpdateComponent } from './remittance-transaction-update.component';

describe('RemittanceTransaction Management Update Component', () => {
  let comp: RemittanceTransactionUpdateComponent;
  let fixture: ComponentFixture<RemittanceTransactionUpdateComponent>;
  let activatedRoute: ActivatedRoute;
  let remittanceTransactionService: RemittanceTransactionService;
  let countryService: CountryService;
  let currencyService: CurrencyService;
  let purposeOfTxnService: PurposeOfTxnService;
  let fundingSourceTypeService: FundingSourceTypeService;
  let transactionTypeService: TransactionTypeService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule, RouterTestingModule.withRoutes([])],
      declarations: [RemittanceTransactionUpdateComponent],
      providers: [
        FormBuilder,
        {
          provide: ActivatedRoute,
          useValue: {
            params: from([{}]),
          },
        },
      ],
    })
      .overrideTemplate(RemittanceTransactionUpdateComponent, '')
      .compileComponents();

    fixture = TestBed.createComponent(RemittanceTransactionUpdateComponent);
    activatedRoute = TestBed.inject(ActivatedRoute);
    remittanceTransactionService = TestBed.inject(RemittanceTransactionService);
    countryService = TestBed.inject(CountryService);
    currencyService = TestBed.inject(CurrencyService);
    purposeOfTxnService = TestBed.inject(PurposeOfTxnService);
    fundingSourceTypeService = TestBed.inject(FundingSourceTypeService);
    transactionTypeService = TestBed.inject(TransactionTypeService);

    comp = fixture.componentInstance;
  });

  describe('ngOnInit', () => {
    it('Should call Country query and add missing value', () => {
      const remittanceTransaction: IRemittanceTransaction = { id: 456 };
      const country: ICountry = { id: 11601 };
      remittanceTransaction.country = country;

      const countryCollection: ICountry[] = [{ id: 85811 }];
      jest.spyOn(countryService, 'query').mockReturnValue(of(new HttpResponse({ body: countryCollection })));
      const additionalCountries = [country];
      const expectedCollection: ICountry[] = [...additionalCountries, ...countryCollection];
      jest.spyOn(countryService, 'addCountryToCollectionIfMissing').mockReturnValue(expectedCollection);

      activatedRoute.data = of({ remittanceTransaction });
      comp.ngOnInit();

      expect(countryService.query).toHaveBeenCalled();
      expect(countryService.addCountryToCollectionIfMissing).toHaveBeenCalledWith(countryCollection, ...additionalCountries);
      expect(comp.countriesSharedCollection).toEqual(expectedCollection);
    });

    it('Should call Currency query and add missing value', () => {
      const remittanceTransaction: IRemittanceTransaction = { id: 456 };
      const txnCurrency: ICurrency = { id: 60371 };
      remittanceTransaction.txnCurrency = txnCurrency;
      const accountCurrency: ICurrency = { id: 20835 };
      remittanceTransaction.accountCurrency = accountCurrency;
      const instructedCurrency: ICurrency = { id: 15961 };
      remittanceTransaction.instructedCurrency = instructedCurrency;

      const currencyCollection: ICurrency[] = [{ id: 65044 }];
      jest.spyOn(currencyService, 'query').mockReturnValue(of(new HttpResponse({ body: currencyCollection })));
      const additionalCurrencies = [txnCurrency, accountCurrency, instructedCurrency];
      const expectedCollection: ICurrency[] = [...additionalCurrencies, ...currencyCollection];
      jest.spyOn(currencyService, 'addCurrencyToCollectionIfMissing').mockReturnValue(expectedCollection);

      activatedRoute.data = of({ remittanceTransaction });
      comp.ngOnInit();

      expect(currencyService.query).toHaveBeenCalled();
      expect(currencyService.addCurrencyToCollectionIfMissing).toHaveBeenCalledWith(currencyCollection, ...additionalCurrencies);
      expect(comp.currenciesSharedCollection).toEqual(expectedCollection);
    });

    it('Should call PurposeOfTxn query and add missing value', () => {
      const remittanceTransaction: IRemittanceTransaction = { id: 456 };
      const purposeOfTxn: IPurposeOfTxn = { id: 89510 };
      remittanceTransaction.purposeOfTxn = purposeOfTxn;

      const purposeOfTxnCollection: IPurposeOfTxn[] = [{ id: 11076 }];
      jest.spyOn(purposeOfTxnService, 'query').mockReturnValue(of(new HttpResponse({ body: purposeOfTxnCollection })));
      const additionalPurposeOfTxns = [purposeOfTxn];
      const expectedCollection: IPurposeOfTxn[] = [...additionalPurposeOfTxns, ...purposeOfTxnCollection];
      jest.spyOn(purposeOfTxnService, 'addPurposeOfTxnToCollectionIfMissing').mockReturnValue(expectedCollection);

      activatedRoute.data = of({ remittanceTransaction });
      comp.ngOnInit();

      expect(purposeOfTxnService.query).toHaveBeenCalled();
      expect(purposeOfTxnService.addPurposeOfTxnToCollectionIfMissing).toHaveBeenCalledWith(
        purposeOfTxnCollection,
        ...additionalPurposeOfTxns
      );
      expect(comp.purposeOfTxnsSharedCollection).toEqual(expectedCollection);
    });

    it('Should call FundingSourceType query and add missing value', () => {
      const remittanceTransaction: IRemittanceTransaction = { id: 456 };
      const fundingSourceType: IFundingSourceType = { id: 18737 };
      remittanceTransaction.fundingSourceType = fundingSourceType;

      const fundingSourceTypeCollection: IFundingSourceType[] = [{ id: 12861 }];
      jest.spyOn(fundingSourceTypeService, 'query').mockReturnValue(of(new HttpResponse({ body: fundingSourceTypeCollection })));
      const additionalFundingSourceTypes = [fundingSourceType];
      const expectedCollection: IFundingSourceType[] = [...additionalFundingSourceTypes, ...fundingSourceTypeCollection];
      jest.spyOn(fundingSourceTypeService, 'addFundingSourceTypeToCollectionIfMissing').mockReturnValue(expectedCollection);

      activatedRoute.data = of({ remittanceTransaction });
      comp.ngOnInit();

      expect(fundingSourceTypeService.query).toHaveBeenCalled();
      expect(fundingSourceTypeService.addFundingSourceTypeToCollectionIfMissing).toHaveBeenCalledWith(
        fundingSourceTypeCollection,
        ...additionalFundingSourceTypes
      );
      expect(comp.fundingSourceTypesSharedCollection).toEqual(expectedCollection);
    });

    it('Should call TransactionType query and add missing value', () => {
      const remittanceTransaction: IRemittanceTransaction = { id: 456 };
      const transactionType: ITransactionType = { id: 35490 };
      remittanceTransaction.transactionType = transactionType;

      const transactionTypeCollection: ITransactionType[] = [{ id: 48898 }];
      jest.spyOn(transactionTypeService, 'query').mockReturnValue(of(new HttpResponse({ body: transactionTypeCollection })));
      const additionalTransactionTypes = [transactionType];
      const expectedCollection: ITransactionType[] = [...additionalTransactionTypes, ...transactionTypeCollection];
      jest.spyOn(transactionTypeService, 'addTransactionTypeToCollectionIfMissing').mockReturnValue(expectedCollection);

      activatedRoute.data = of({ remittanceTransaction });
      comp.ngOnInit();

      expect(transactionTypeService.query).toHaveBeenCalled();
      expect(transactionTypeService.addTransactionTypeToCollectionIfMissing).toHaveBeenCalledWith(
        transactionTypeCollection,
        ...additionalTransactionTypes
      );
      expect(comp.transactionTypesSharedCollection).toEqual(expectedCollection);
    });

    it('Should update editForm', () => {
      const remittanceTransaction: IRemittanceTransaction = { id: 456 };
      const country: ICountry = { id: 45207 };
      remittanceTransaction.country = country;
      const txnCurrency: ICurrency = { id: 49328 };
      remittanceTransaction.txnCurrency = txnCurrency;
      const accountCurrency: ICurrency = { id: 4147 };
      remittanceTransaction.accountCurrency = accountCurrency;
      const instructedCurrency: ICurrency = { id: 85669 };
      remittanceTransaction.instructedCurrency = instructedCurrency;
      const purposeOfTxn: IPurposeOfTxn = { id: 20197 };
      remittanceTransaction.purposeOfTxn = purposeOfTxn;
      const fundingSourceType: IFundingSourceType = { id: 55426 };
      remittanceTransaction.fundingSourceType = fundingSourceType;
      const transactionType: ITransactionType = { id: 24802 };
      remittanceTransaction.transactionType = transactionType;

      activatedRoute.data = of({ remittanceTransaction });
      comp.ngOnInit();

      expect(comp.editForm.value).toEqual(expect.objectContaining(remittanceTransaction));
      expect(comp.countriesSharedCollection).toContain(country);
      expect(comp.currenciesSharedCollection).toContain(txnCurrency);
      expect(comp.currenciesSharedCollection).toContain(accountCurrency);
      expect(comp.currenciesSharedCollection).toContain(instructedCurrency);
      expect(comp.purposeOfTxnsSharedCollection).toContain(purposeOfTxn);
      expect(comp.fundingSourceTypesSharedCollection).toContain(fundingSourceType);
      expect(comp.transactionTypesSharedCollection).toContain(transactionType);
    });
  });

  describe('save', () => {
    it('Should call update service on save for existing entity', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<RemittanceTransaction>>();
      const remittanceTransaction = { id: 123 };
      jest.spyOn(remittanceTransactionService, 'update').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ remittanceTransaction });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.next(new HttpResponse({ body: remittanceTransaction }));
      saveSubject.complete();

      // THEN
      expect(comp.previousState).toHaveBeenCalled();
      expect(remittanceTransactionService.update).toHaveBeenCalledWith(remittanceTransaction);
      expect(comp.isSaving).toEqual(false);
    });

    it('Should call create service on save for new entity', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<RemittanceTransaction>>();
      const remittanceTransaction = new RemittanceTransaction();
      jest.spyOn(remittanceTransactionService, 'create').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ remittanceTransaction });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.next(new HttpResponse({ body: remittanceTransaction }));
      saveSubject.complete();

      // THEN
      expect(remittanceTransactionService.create).toHaveBeenCalledWith(remittanceTransaction);
      expect(comp.isSaving).toEqual(false);
      expect(comp.previousState).toHaveBeenCalled();
    });

    it('Should set isSaving to false on error', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<RemittanceTransaction>>();
      const remittanceTransaction = { id: 123 };
      jest.spyOn(remittanceTransactionService, 'update').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ remittanceTransaction });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.error('This is an error!');

      // THEN
      expect(remittanceTransactionService.update).toHaveBeenCalledWith(remittanceTransaction);
      expect(comp.isSaving).toEqual(false);
      expect(comp.previousState).not.toHaveBeenCalled();
    });
  });

  describe('Tracking relationships identifiers', () => {
    describe('trackCountryById', () => {
      it('Should return tracked Country primary key', () => {
        const entity = { id: 123 };
        const trackResult = comp.trackCountryById(0, entity);
        expect(trackResult).toEqual(entity.id);
      });
    });

    describe('trackCurrencyById', () => {
      it('Should return tracked Currency primary key', () => {
        const entity = { id: 123 };
        const trackResult = comp.trackCurrencyById(0, entity);
        expect(trackResult).toEqual(entity.id);
      });
    });

    describe('trackPurposeOfTxnById', () => {
      it('Should return tracked PurposeOfTxn primary key', () => {
        const entity = { id: 123 };
        const trackResult = comp.trackPurposeOfTxnById(0, entity);
        expect(trackResult).toEqual(entity.id);
      });
    });

    describe('trackFundingSourceTypeById', () => {
      it('Should return tracked FundingSourceType primary key', () => {
        const entity = { id: 123 };
        const trackResult = comp.trackFundingSourceTypeById(0, entity);
        expect(trackResult).toEqual(entity.id);
      });
    });

    describe('trackTransactionTypeById', () => {
      it('Should return tracked TransactionType primary key', () => {
        const entity = { id: 123 };
        const trackResult = comp.trackTransactionTypeById(0, entity);
        expect(trackResult).toEqual(entity.id);
      });
    });
  });
});
