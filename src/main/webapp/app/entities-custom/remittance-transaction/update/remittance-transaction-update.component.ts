import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { finalize, map } from 'rxjs/operators';

import dayjs from 'dayjs/esm';
import { DATE_TIME_FORMAT } from 'app/config/input.constants';

import { IRemittanceTransaction, RemittanceTransaction } from '../remittance-transaction.model';
import { RemittanceTransactionService } from '../service/remittance-transaction.service';
import { ICountry } from 'app/entities-custom/country/country.model';
import { CountryService } from 'app/entities-custom/country/service/country.service';
import { ICurrency } from 'app/entities-custom/currency/currency.model';
import { CurrencyService } from 'app/entities-custom/currency/service/currency.service';
import { IPurposeOfTxn } from 'app/entities-custom/purpose-of-txn/purpose-of-txn.model';
import { PurposeOfTxnService } from 'app/entities-custom/purpose-of-txn/service/purpose-of-txn.service';
import { IFundingSourceType } from 'app/entities-custom/funding-source-type/funding-source-type.model';
import { FundingSourceTypeService } from 'app/entities-custom/funding-source-type/service/funding-source-type.service';
import { ITransactionType } from 'app/entities-custom/transaction-type/transaction-type.model';
import { TransactionTypeService } from 'app/entities-custom/transaction-type/service/transaction-type.service';
import { TxnStatus } from 'app/entities-custom/enumerations/txn-status.model';
import { ICustomer } from 'app/entities-custom/customer/customer.model';
import { ICasaAccount } from 'app/entities-custom/casa-account/casa-account.model';
import { IBeneficiary } from 'app/entities-custom/beneficiary/beneficiary.model';
import { IFxRate } from 'app/entities-custom/fx-rate/fx-rate.model';
import { ICharge } from 'app/entities-custom/charge/charge.model';

import { CustomerService } from 'app/entities-custom/customer/service/customer.service';
import { CasaAccountService } from 'app/entities-custom/casa-account/service/casa-account.service';
import { BeneficiaryService } from 'app/entities-custom/beneficiary/service/beneficiary.service';

// Modified Fxrate Service
import { FxRateService } from 'app/entities-custom/fx-rate/service/fx-rate.service';
import { ChargeService } from 'app/entities-custom/charge/service/charge.service';

@Component({
  selector: 'jhi-remittance-transaction-update',
  templateUrl: './remittance-transaction-update.component.html',
})
export class RemittanceTransactionUpdateComponent implements OnInit {
  isSaving = false;
  txnStatusValues = Object.keys(TxnStatus);

  countriesSharedCollection: ICountry[] = [];
  currenciesSharedCollection: ICurrency[] = [];
  purposeOfTxnsSharedCollection: IPurposeOfTxn[] = [];
  fundingSourceTypesSharedCollection: IFundingSourceType[] = [];
  transactionTypesSharedCollection: ITransactionType[] = [];

  beneficiariesSharedCollection: IBeneficiary[] = [];
  casaAccountsSharedCollection: ICasaAccount[] = [];
  customersSharedCollection: ICustomer[] = [];

  editForm = this.fb.group({
    id: [],
    txnRefNo: [],
    accountNo: [],
    txnDate: [],
    txnDateInUTC: [],
    txnDateInLocal: [],
    txnDateForSettlement: [],
    valueDate: [],
    valueDateInUTC: [],
    valueDateInLocal: [],
    valueDateForSettlement: [],
    beneficiaryAccountNo: [],
    beneficiaryName: [],
    beneficiarybankSwiftCode: [],
    intermediaryBankSwiftCode: [],
    beneficiaryBankName: [],
    beneficiaryBankBranchName: [],
    beneficiaryAddress1: [],
    beneficiaryAddress2: [],
    beneficiaryAddress3: [],
    beneficiaryState: [],
    beneficiaryPinCode: [],
    txnAmount: [],
    amountInAccountCurrency: [],
    amountInLocalCurrency: [],
    chargeInAccountCurrency: [],
    chargeInLocalCurrency: [],
    fundingSource: [],
    rate: [],
    instructedAmount: [],
    instructedAmountCurrency: [],
    merchantID: [],
    merchantInfo: [],
    externalRefNo: [],
    txnDescription: [],
    txnInternalDescription: [],
    txnAddnlInfo: [],
    createdOn: [],
    createdBy: [],
    txnStatus: [],
    txnDebitAccountNo: [],
    txnCreditAccountNo: [],
    chargeDebitAccountNo: [],
    chargeCreditAccountNo: [],
    country: [],
    txnCurrency: [],
    accountCurrency: [],
    instructedCurrency: [],
    purposeOfTxn: [],
    fundingSourceType: [],
    transactionType: [],
    customer: [],
    casaAccount: [],
    beneficiary: [],
  });

  constructor(
    protected remittanceTransactionService: RemittanceTransactionService,
    protected countryService: CountryService,
    protected currencyService: CurrencyService,
    protected purposeOfTxnService: PurposeOfTxnService,
    protected fundingSourceTypeService: FundingSourceTypeService,
    protected transactionTypeService: TransactionTypeService,
    protected activatedRoute: ActivatedRoute,
    protected fb: FormBuilder,
    protected customerService: CustomerService,
    protected casaAccountService: CasaAccountService,
    protected beneficiaryService: BeneficiaryService,
    protected fxRateService: FxRateService,
    protected chargeService: ChargeService
  ) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ remittanceTransaction }) => {
      if (remittanceTransaction.id === undefined) {
        const today = dayjs().startOf('day');
        remittanceTransaction.txnDateInUTC = today;
        remittanceTransaction.txnDateInLocal = today;
        remittanceTransaction.txnDateForSettlement = today;
        remittanceTransaction.valueDateInUTC = today;
        remittanceTransaction.valueDateInLocal = today;
        remittanceTransaction.valueDateForSettlement = today;
        remittanceTransaction.createdOn = today;
      }

      this.updateForm(remittanceTransaction);

      this.loadRelationshipsOptions();
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const remittanceTransaction = this.createFromForm();
    if (remittanceTransaction.id !== undefined) {
      this.subscribeToSaveResponse(this.remittanceTransactionService.update(remittanceTransaction));
    } else {
      this.subscribeToSaveResponse(this.remittanceTransactionService.create(remittanceTransaction));
    }
  }

  changeCustomer(): void {
    // alert(this.editForm.get('customer')!.value.id);
    this.beneficiariesSharedCollection = [];
    this.casaAccountsSharedCollection = [];
    this.editForm.patchValue({
      casaAccount: this.casaAccountsSharedCollection,
      beneficiary: this.beneficiariesSharedCollection,
      accountNo: '',
      beneficiaryName: '',
      beneficiaryBankBranchName: '',
      beneficiaryAccountNo: '',
      beneficiarybankSwiftCode: '',
      beneficiaryBankName: '',
      beneficiaryAddress1: '',
      beneficiaryAddress2: '',
      beneficiaryState: '',
      country: '',
    });

    //console.log(this.editForm.get('customer')!.value.id);
    this.loadCasaAccountOptions(this.editForm.get('customer')!.value.id);
    this.loadBeneficiaryOptions(this.editForm.get('customer')!.value.id);
  }

  changeCasaAccount(): void {
    const acValue = {
      accountNo: this.editForm.get('casaAccount')!.value.accountNo,
    };
    // console.log(acValue);
    //this.editForm.setValue(acValue);
    this.editForm.patchValue(acValue);
  }
  changeTxnAmount(): void {
    const rate = this.editForm.get('rate')!.value;
    const amount = this.editForm.get('txnAmount')!.value;
    const txnAmnt = ((1 / rate) * amount).toFixed(2);
    console.log(rate);
    console.log(amount);
    console.log(rate * amount);

    const acTxnAmnt = {
      amountInAccountCurrency: txnAmnt,
    };
    //console.log(acValue);
    this.editForm.patchValue(acTxnAmnt);
  }

  changeCurrency(): void {
    this.editForm.patchValue({
      rate: '',
      chargeInAccountCurrency: '',
    });
    const selectedAccount = this.editForm.get('casaAccount')!.value;
    if (selectedAccount != null) {
      // console.log("Account Currency"+JSON.stringify(selectedAccount));

      const quoteCcy = this.editForm.get('txnCurrency')!.value.code;
      const beneCcyId = this.editForm.get('txnCurrency')!.value.id;
      const beneCountryId = this.editForm.get('country')!.value.id;
      this.fxRateService.getRate(selectedAccount.accountCurrency.code, quoteCcy).subscribe((data: IFxRate) => {
        this.editForm.patchValue({
          rate: data.askPrice,
        });
      });
      this.chargeService.getCharge(beneCountryId, beneCcyId).subscribe((data: ICharge) => {
        this.editForm.patchValue({
          chargeInAccountCurrency: data.chargeAmountLocalCcy,
        });
      });
    }
  }
  changeBeneficiary(): void {
    this.editForm.patchValue({
      accountNo: '',
      beneficiaryName: '',
      beneficiaryBankBranchName: '',
      beneficiaryAccountNo: '',
      beneficiarybankSwiftCode: '',
      beneficiaryBankName: '',
      beneficiaryAddress1: '',
      beneficiaryAddress2: '',
      beneficiaryState: '',
      country: '',
    });
    const beneValue = {
      accountNo: this.editForm.get('casaAccount')!.value.accountNo,
      beneficiaryAccountNo: this.editForm.get('beneficiary')!.value.beneficiaryAccountNo,
      beneficiaryName: this.editForm.get('beneficiary')!.value.beneficiaryName,
      beneficiaryBankBranchName: this.editForm.get('beneficiary')!.value.beneficiaryBankBranchName,
      beneficiarybankSwiftCode: this.editForm.get('beneficiary')!.value.beneficiarybankSwiftCode,
      beneficiaryBankName: this.editForm.get('beneficiary')!.value.beneficiaryBankName,
      beneficiaryAddress1: this.editForm.get('beneficiary')!.value.beneficiaryAddress1,
      beneficiaryAddress2: this.editForm.get('beneficiary')!.value.beneficiaryAddress2,
      beneficiaryState: this.editForm.get('beneficiary')!.value.beneficiaryState,
      country: this.editForm.get('beneficiary')!.value.country,
    };
    // console.log(beneValue);
    //this.editForm.setValue(acValue);
    this.editForm.patchValue(beneValue);
  }

  trackCountryById(index: number, item: ICountry): number {
    return item.id!;
  }

  trackCurrencyById(index: number, item: ICurrency): number {
    return item.id!;
  }

  trackPurposeOfTxnById(index: number, item: IPurposeOfTxn): number {
    return item.id!;
  }

  trackFundingSourceTypeById(index: number, item: IFundingSourceType): number {
    return item.id!;
  }

  trackTransactionTypeById(index: number, item: ITransactionType): number {
    return item.id!;
  }

  trackCustomerById(index: number, item: ICustomer): number {
    return item.id!;
  }

  trackCasaAccountById(index: number, item: ICasaAccount): number {
    return item.id!;
  }

  trackBeneficiaryById(index: number, item: IBeneficiary): number {
    return item.id!;
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IRemittanceTransaction>>): void {
    result.pipe(finalize(() => this.onSaveFinalize())).subscribe({
      next: () => this.onSaveSuccess(),
      error: () => this.onSaveError(),
    });
  }

  protected onSaveSuccess(): void {
    this.previousState();
  }

  protected onSaveError(): void {
    // Api for inheritance.
  }

  protected onSaveFinalize(): void {
    this.isSaving = false;
  }

  protected updateForm(remittanceTransaction: IRemittanceTransaction): void {
    this.editForm.patchValue({
      id: remittanceTransaction.id,
      txnRefNo: remittanceTransaction.txnRefNo,
      accountNo: remittanceTransaction.accountNo,
      txnDate: remittanceTransaction.txnDate,
      valueDate: remittanceTransaction.valueDate,
      beneficiaryAccountNo: remittanceTransaction.beneficiaryAccountNo,
      beneficiaryName: remittanceTransaction.beneficiaryName,
      beneficiarybankSwiftCode: remittanceTransaction.beneficiarybankSwiftCode,
      intermediaryBankSwiftCode: remittanceTransaction.intermediaryBankSwiftCode,
      beneficiaryBankName: remittanceTransaction.beneficiaryBankName,
      beneficiaryBankBranchName: remittanceTransaction.beneficiaryBankBranchName,
      txnAmount: remittanceTransaction.txnAmount,
      amountInAccountCurrency: remittanceTransaction.amountInAccountCurrency,
      rate: remittanceTransaction.rate,
      txnDescription: remittanceTransaction.txnDescription,
      country: remittanceTransaction.country,
      txnCurrency: remittanceTransaction.txnCurrency,
      purposeOfTxn: remittanceTransaction.purposeOfTxn,
    });

    this.countriesSharedCollection = this.countryService.addCountryToCollectionIfMissing(
      this.countriesSharedCollection,
      remittanceTransaction.country
    );
    this.currenciesSharedCollection = this.currencyService.addCurrencyToCollectionIfMissing(
      this.currenciesSharedCollection,
      remittanceTransaction.txnCurrency,
      remittanceTransaction.accountCurrency,
      remittanceTransaction.instructedCurrency
    );
    this.purposeOfTxnsSharedCollection = this.purposeOfTxnService.addPurposeOfTxnToCollectionIfMissing(
      this.purposeOfTxnsSharedCollection,
      remittanceTransaction.purposeOfTxn
    );
    this.fundingSourceTypesSharedCollection = this.fundingSourceTypeService.addFundingSourceTypeToCollectionIfMissing(
      this.fundingSourceTypesSharedCollection,
      remittanceTransaction.fundingSourceType
    );
    this.transactionTypesSharedCollection = this.transactionTypeService.addTransactionTypeToCollectionIfMissing(
      this.transactionTypesSharedCollection,
      remittanceTransaction.transactionType
    );

    this.customersSharedCollection = this.customerService.addCustomerToCollectionIfMissing(
      this.customersSharedCollection,
      remittanceTransaction.customer
    );

    this.beneficiariesSharedCollection = this.beneficiaryService.addBeneficiaryToCollectionIfMissing(
      this.beneficiariesSharedCollection,
      remittanceTransaction.beneficiary
    );

    this.casaAccountsSharedCollection = this.casaAccountService.addCasaAccountToCollectionIfMissing(
      this.casaAccountsSharedCollection,
      remittanceTransaction.casaAccount
    );
  }

  protected loadCasaAccountOptions(id: number): void {
    this.casaAccountService
      .queryByCustomerId(id)
      .pipe(map((res: HttpResponse<ICasaAccount[]>) => res.body ?? []))
      .pipe(
        map((casaAccounts1: ICasaAccount[]) =>
          this.casaAccountService.addCasaAccountToCollectionIfMissing(casaAccounts1, this.editForm.get('casaAccount')!.value)
        )
      )
      .subscribe((casaAccounts: ICasaAccount[]) => (this.casaAccountsSharedCollection = casaAccounts));
  }

  protected loadBeneficiaryOptions(id: number): void {
    this.beneficiaryService
      .queryByCustomerId(id)
      .pipe(map((res: HttpResponse<IBeneficiary[]>) => res.body ?? []))
      .pipe(
        map((beneficiaries: IBeneficiary[]) =>
          this.beneficiaryService.addBeneficiaryToCollectionIfMissing(beneficiaries, this.editForm.get('beneficiary')!.value)
        )
      )
      .subscribe((beneficiaries: IBeneficiary[]) => (this.beneficiariesSharedCollection = beneficiaries));
  }
  protected loadRelationshipsOptions(): void {
    this.countryService
      .query()
      .pipe(map((res: HttpResponse<ICountry[]>) => res.body ?? []))
      .pipe(
        map((countries: ICountry[]) => this.countryService.addCountryToCollectionIfMissing(countries, this.editForm.get('country')!.value))
      )
      .subscribe((countries: ICountry[]) => (this.countriesSharedCollection = countries));

    this.currencyService
      .query()
      .pipe(map((res: HttpResponse<ICurrency[]>) => res.body ?? []))
      .pipe(
        map((currencies: ICurrency[]) =>
          this.currencyService.addCurrencyToCollectionIfMissing(
            currencies,
            this.editForm.get('txnCurrency')!.value,
            this.editForm.get('accountCurrency')!.value,
            this.editForm.get('instructedCurrency')!.value
          )
        )
      )
      .subscribe((currencies: ICurrency[]) => (this.currenciesSharedCollection = currencies));

    this.purposeOfTxnService
      .query()
      .pipe(map((res: HttpResponse<IPurposeOfTxn[]>) => res.body ?? []))
      .pipe(
        map((purposeOfTxns: IPurposeOfTxn[]) =>
          this.purposeOfTxnService.addPurposeOfTxnToCollectionIfMissing(purposeOfTxns, this.editForm.get('purposeOfTxn')!.value)
        )
      )
      .subscribe((purposeOfTxns: IPurposeOfTxn[]) => (this.purposeOfTxnsSharedCollection = purposeOfTxns));

    this.fundingSourceTypeService
      .query()
      .pipe(map((res: HttpResponse<IFundingSourceType[]>) => res.body ?? []))
      .pipe(
        map((fundingSourceTypes: IFundingSourceType[]) =>
          this.fundingSourceTypeService.addFundingSourceTypeToCollectionIfMissing(
            fundingSourceTypes,
            this.editForm.get('fundingSourceType')!.value
          )
        )
      )
      .subscribe((fundingSourceTypes: IFundingSourceType[]) => (this.fundingSourceTypesSharedCollection = fundingSourceTypes));

    this.transactionTypeService
      .query()
      .pipe(map((res: HttpResponse<ITransactionType[]>) => res.body ?? []))
      .pipe(
        map((transactionTypes: ITransactionType[]) =>
          this.transactionTypeService.addTransactionTypeToCollectionIfMissing(transactionTypes, this.editForm.get('transactionType')!.value)
        )
      )
      .subscribe((transactionTypes: ITransactionType[]) => (this.transactionTypesSharedCollection = transactionTypes));

    // New items
    this.customerService
      .query()
      .pipe(map((res: HttpResponse<ICustomer[]>) => res.body ?? []))
      .pipe(
        map((customers: ICustomer[]) =>
          this.customerService.addCustomerToCollectionIfMissing(customers, this.editForm.get('customer')!.value)
        )
      )
      .subscribe((customers: ICustomer[]) => (this.customersSharedCollection = customers));
  }

  protected createFromForm(): IRemittanceTransaction {
    const txnObject: IRemittanceTransaction = new RemittanceTransaction();
    txnObject.txnDescription = this.editForm.get(['txnDescription'])!.value;
    txnObject.id = this.editForm.get(['id'])!.value;
    txnObject.txnRefNo = this.editForm.get(['txnRefNo'])!.value;
    txnObject.accountNo = this.editForm.get(['accountNo'])!.value;
    txnObject.txnDate = this.editForm.get(['txnDate'])!.value;
    txnObject.valueDate = this.editForm.get(['valueDate'])!.value;
    txnObject.beneficiaryAccountNo = this.editForm.get(['beneficiaryAccountNo'])!.value;
    txnObject.beneficiaryName = this.editForm.get(['beneficiaryName'])!.value;
    txnObject.beneficiarybankSwiftCode = this.editForm.get(['beneficiarybankSwiftCode'])!.value;
    txnObject.beneficiaryBankName = this.editForm.get(['beneficiaryBankName'])!.value;
    txnObject.beneficiaryBankBranchName = this.editForm.get(['beneficiaryBankBranchName'])!.value;
    txnObject.txnAmount = this.editForm.get(['txnAmount'])!.value;
    txnObject.amountInAccountCurrency = this.editForm.get(['amountInAccountCurrency'])!.value;
    txnObject.rate = this.editForm.get(['rate'])!.value;
    txnObject.txnDescription = this.editForm.get(['txnDescription'])!.value;
    txnObject.country = this.editForm.get(['country'])!.value;
    txnObject.txnCurrency = this.editForm.get(['txnCurrency'])!.value;
    txnObject.purposeOfTxn = this.editForm.get(['purposeOfTxn'])!.value;
    txnObject.customer = this.editForm.get(['customer'])!.value;
    txnObject.casaAccount = this.editForm.get(['casaAccount'])!.value;
    txnObject.beneficiary = this.editForm.get(['beneficiary'])!.value; //
    txnObject.chargeInAccountCurrency = this.editForm.get(['chargeInAccountCurrency'])!.value;
    txnObject.beneficiaryAddress1 = this.editForm.get(['beneficiaryAddress1'])!.value;
    txnObject.beneficiaryAddress2 = this.editForm.get(['beneficiaryAddress2'])!.value;
    txnObject.beneficiaryState = this.editForm.get(['beneficiaryState'])!.value;

    console.log(txnObject);
    return txnObject;
  }
}
