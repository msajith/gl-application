import { ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpHeaders, HttpResponse } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { of } from 'rxjs';

import { RemittanceTransactionService } from '../service/remittance-transaction.service';

import { RemittanceTransactionComponent } from './remittance-transaction.component';

describe('RemittanceTransaction Management Component', () => {
  let comp: RemittanceTransactionComponent;
  let fixture: ComponentFixture<RemittanceTransactionComponent>;
  let service: RemittanceTransactionService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      declarations: [RemittanceTransactionComponent],
    })
      .overrideTemplate(RemittanceTransactionComponent, '')
      .compileComponents();

    fixture = TestBed.createComponent(RemittanceTransactionComponent);
    comp = fixture.componentInstance;
    service = TestBed.inject(RemittanceTransactionService);

    const headers = new HttpHeaders();
    jest.spyOn(service, 'query').mockReturnValue(
      of(
        new HttpResponse({
          body: [{ id: 123 }],
          headers,
        })
      )
    );
  });

  it('Should call load all on init', () => {
    // WHEN
    comp.ngOnInit();

    // THEN
    expect(service.query).toHaveBeenCalled();
    expect(comp.remittanceTransactions?.[0]).toEqual(expect.objectContaining({ id: 123 }));
  });
});
