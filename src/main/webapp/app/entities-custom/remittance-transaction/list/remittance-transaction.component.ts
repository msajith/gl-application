import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

import { IRemittanceTransaction } from '../remittance-transaction.model';
import { RemittanceTransactionService } from '../service/remittance-transaction.service';
import { RemittanceTransactionDeleteDialogComponent } from '../delete/remittance-transaction-delete-dialog.component';

@Component({
  selector: 'jhi-remittance-transaction',
  templateUrl: './remittance-transaction.component.html',
})
export class RemittanceTransactionComponent implements OnInit {
  remittanceTransactions?: IRemittanceTransaction[];
  isLoading = false;

  constructor(protected remittanceTransactionService: RemittanceTransactionService, protected modalService: NgbModal) {}

  loadAll(): void {
    this.isLoading = true;

    this.remittanceTransactionService.query().subscribe({
      next: (res: HttpResponse<IRemittanceTransaction[]>) => {
        this.isLoading = false;
        this.remittanceTransactions = res.body ?? [];
      },
      error: () => {
        this.isLoading = false;
      },
    });
  }

  ngOnInit(): void {
    this.loadAll();
  }

  trackId(index: number, item: IRemittanceTransaction): number {
    return item.id!;
  }

  delete(remittanceTransaction: IRemittanceTransaction): void {
    const modalRef = this.modalService.open(RemittanceTransactionDeleteDialogComponent, { size: 'lg', backdrop: 'static' });
    modalRef.componentInstance.remittanceTransaction = remittanceTransaction;
    // unsubscribe not needed because closed completes on modal close
    modalRef.closed.subscribe(reason => {
      if (reason === 'deleted') {
        this.loadAll();
      }
    });
  }
}
