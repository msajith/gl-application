import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import dayjs from 'dayjs/esm';

import { isPresent } from 'app/core/util/operators';
import { DATE_FORMAT } from 'app/config/input.constants';
import { ApplicationConfigService } from 'app/core/config/application-config.service';
import { createRequestOption } from 'app/core/request/request-util';
import { IRemittanceTransaction, getRemittanceTransactionIdentifier } from '../remittance-transaction.model';

export type EntityResponseType = HttpResponse<IRemittanceTransaction>;
export type EntityArrayResponseType = HttpResponse<IRemittanceTransaction[]>;

@Injectable({ providedIn: 'root' })
export class RemittanceTransactionService {
  protected resourceUrl = this.applicationConfigService.getEndpointFor('api/remittance-transactions');
  protected resourceUrlv1Casa = this.applicationConfigService.getEndpointFor('api/v1/remittance-transactions/casa');
  protected casa = 'casa';

  constructor(protected http: HttpClient, protected applicationConfigService: ApplicationConfigService) {}

  create(remittanceTransaction: IRemittanceTransaction): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(remittanceTransaction);
    return this.http
      .post<IRemittanceTransaction>(this.resourceUrlv1Casa, copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  update(remittanceTransaction: IRemittanceTransaction): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(remittanceTransaction);
    return this.http
      .put<IRemittanceTransaction>(`${this.resourceUrl}/${getRemittanceTransactionIdentifier(remittanceTransaction) as number}`, copy, {
        observe: 'response',
      })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  partialUpdate(remittanceTransaction: IRemittanceTransaction): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(remittanceTransaction);
    return this.http
      .patch<IRemittanceTransaction>(`${this.resourceUrl}/${getRemittanceTransactionIdentifier(remittanceTransaction) as number}`, copy, {
        observe: 'response',
      })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http
      .get<IRemittanceTransaction>(`${this.resourceUrl}/${id}`, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http
      .get<IRemittanceTransaction[]>(this.resourceUrl, { params: options, observe: 'response' })
      .pipe(map((res: EntityArrayResponseType) => this.convertDateArrayFromServer(res)));
  }

  delete(id: number): Observable<HttpResponse<{}>> {
    return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  addRemittanceTransactionToCollectionIfMissing(
    remittanceTransactionCollection: IRemittanceTransaction[],
    ...remittanceTransactionsToCheck: (IRemittanceTransaction | null | undefined)[]
  ): IRemittanceTransaction[] {
    const remittanceTransactions: IRemittanceTransaction[] = remittanceTransactionsToCheck.filter(isPresent);
    if (remittanceTransactions.length > 0) {
      const remittanceTransactionCollectionIdentifiers = remittanceTransactionCollection.map(
        remittanceTransactionItem => getRemittanceTransactionIdentifier(remittanceTransactionItem)!
      );
      const remittanceTransactionsToAdd = remittanceTransactions.filter(remittanceTransactionItem => {
        const remittanceTransactionIdentifier = getRemittanceTransactionIdentifier(remittanceTransactionItem);
        if (
          remittanceTransactionIdentifier == null ||
          remittanceTransactionCollectionIdentifiers.includes(remittanceTransactionIdentifier)
        ) {
          return false;
        }
        remittanceTransactionCollectionIdentifiers.push(remittanceTransactionIdentifier);
        return true;
      });
      return [...remittanceTransactionsToAdd, ...remittanceTransactionCollection];
    }
    return remittanceTransactionCollection;
  }

  protected convertDateFromClient(remittanceTransaction: IRemittanceTransaction): IRemittanceTransaction {
    return Object.assign({}, remittanceTransaction, {
      txnDate: remittanceTransaction.txnDate?.isValid() ? remittanceTransaction.txnDate.format(DATE_FORMAT) : undefined,
      txnDateInUTC: remittanceTransaction.txnDateInUTC?.isValid() ? remittanceTransaction.txnDateInUTC.toJSON() : undefined,
      txnDateInLocal: remittanceTransaction.txnDateInLocal?.isValid() ? remittanceTransaction.txnDateInLocal.toJSON() : undefined,
      txnDateForSettlement: remittanceTransaction.txnDateForSettlement?.isValid()
        ? remittanceTransaction.txnDateForSettlement.toJSON()
        : undefined,
      valueDate: remittanceTransaction.valueDate?.isValid() ? remittanceTransaction.valueDate.format(DATE_FORMAT) : undefined,
      valueDateInUTC: remittanceTransaction.valueDateInUTC?.isValid() ? remittanceTransaction.valueDateInUTC.toJSON() : undefined,
      valueDateInLocal: remittanceTransaction.valueDateInLocal?.isValid() ? remittanceTransaction.valueDateInLocal.toJSON() : undefined,
      valueDateForSettlement: remittanceTransaction.valueDateForSettlement?.isValid()
        ? remittanceTransaction.valueDateForSettlement.toJSON()
        : undefined,
      createdOn: remittanceTransaction.createdOn?.isValid() ? remittanceTransaction.createdOn.toJSON() : undefined,
    });
  }

  protected convertDateFromServer(res: EntityResponseType): EntityResponseType {
    if (res.body) {
      res.body.txnDate = res.body.txnDate ? dayjs(res.body.txnDate) : undefined;
      res.body.txnDateInUTC = res.body.txnDateInUTC ? dayjs(res.body.txnDateInUTC) : undefined;
      res.body.txnDateInLocal = res.body.txnDateInLocal ? dayjs(res.body.txnDateInLocal) : undefined;
      res.body.txnDateForSettlement = res.body.txnDateForSettlement ? dayjs(res.body.txnDateForSettlement) : undefined;
      res.body.valueDate = res.body.valueDate ? dayjs(res.body.valueDate) : undefined;
      res.body.valueDateInUTC = res.body.valueDateInUTC ? dayjs(res.body.valueDateInUTC) : undefined;
      res.body.valueDateInLocal = res.body.valueDateInLocal ? dayjs(res.body.valueDateInLocal) : undefined;
      res.body.valueDateForSettlement = res.body.valueDateForSettlement ? dayjs(res.body.valueDateForSettlement) : undefined;
      res.body.createdOn = res.body.createdOn ? dayjs(res.body.createdOn) : undefined;
    }
    return res;
  }

  protected convertDateArrayFromServer(res: EntityArrayResponseType): EntityArrayResponseType {
    if (res.body) {
      res.body.forEach((remittanceTransaction: IRemittanceTransaction) => {
        remittanceTransaction.txnDate = remittanceTransaction.txnDate ? dayjs(remittanceTransaction.txnDate) : undefined;
        remittanceTransaction.txnDateInUTC = remittanceTransaction.txnDateInUTC ? dayjs(remittanceTransaction.txnDateInUTC) : undefined;
        remittanceTransaction.txnDateInLocal = remittanceTransaction.txnDateInLocal
          ? dayjs(remittanceTransaction.txnDateInLocal)
          : undefined;
        remittanceTransaction.txnDateForSettlement = remittanceTransaction.txnDateForSettlement
          ? dayjs(remittanceTransaction.txnDateForSettlement)
          : undefined;
        remittanceTransaction.valueDate = remittanceTransaction.valueDate ? dayjs(remittanceTransaction.valueDate) : undefined;
        remittanceTransaction.valueDateInUTC = remittanceTransaction.valueDateInUTC
          ? dayjs(remittanceTransaction.valueDateInUTC)
          : undefined;
        remittanceTransaction.valueDateInLocal = remittanceTransaction.valueDateInLocal
          ? dayjs(remittanceTransaction.valueDateInLocal)
          : undefined;
        remittanceTransaction.valueDateForSettlement = remittanceTransaction.valueDateForSettlement
          ? dayjs(remittanceTransaction.valueDateForSettlement)
          : undefined;
        remittanceTransaction.createdOn = remittanceTransaction.createdOn ? dayjs(remittanceTransaction.createdOn) : undefined;
      });
    }
    return res;
  }
}
