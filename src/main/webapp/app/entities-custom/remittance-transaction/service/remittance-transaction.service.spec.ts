import { TestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import dayjs from 'dayjs/esm';

import { DATE_FORMAT, DATE_TIME_FORMAT } from 'app/config/input.constants';
import { TxnStatus } from 'app/entities/enumerations/txn-status.model';
import { IRemittanceTransaction, RemittanceTransaction } from '../remittance-transaction.model';

import { RemittanceTransactionService } from './remittance-transaction.service';

describe('RemittanceTransaction Service', () => {
  let service: RemittanceTransactionService;
  let httpMock: HttpTestingController;
  let elemDefault: IRemittanceTransaction;
  let expectedResult: IRemittanceTransaction | IRemittanceTransaction[] | boolean | null;
  let currentDate: dayjs.Dayjs;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
    });
    expectedResult = null;
    service = TestBed.inject(RemittanceTransactionService);
    httpMock = TestBed.inject(HttpTestingController);
    currentDate = dayjs();

    elemDefault = {
      id: 0,
      txnRefNo: 'AAAAAAA',
      accountNo: 'AAAAAAA',
      txnDate: currentDate,
      txnDateInUTC: currentDate,
      txnDateInLocal: currentDate,
      txnDateForSettlement: currentDate,
      valueDate: currentDate,
      valueDateInUTC: currentDate,
      valueDateInLocal: currentDate,
      valueDateForSettlement: currentDate,
      beneficiaryAccountNo: 'AAAAAAA',
      beneficiaryName: 'AAAAAAA',
      beneficiarybankSwiftCode: 'AAAAAAA',
      intermediaryBankSwiftCode: 'AAAAAAA',
      beneficiaryBankName: 'AAAAAAA',
      beneficiaryBankBranchName: 'AAAAAAA',
      beneficiaryAddress1: 'AAAAAAA',
      beneficiaryAddress2: 'AAAAAAA',
      beneficiaryAddress3: 'AAAAAAA',
      beneficiaryState: 'AAAAAAA',
      beneficiaryPinCode: 'AAAAAAA',
      txnAmount: 0,
      amountInAccountCurrency: 0,
      amountInLocalCurrency: 0,
      chargeInAccountCurrency: 0,
      chargeInLocalCurrency: 0,
      fundingSource: 'AAAAAAA',
      rate: 0,
      instructedAmount: 0,
      instructedAmountCurrency: 'AAAAAAA',
      merchantID: 'AAAAAAA',
      merchantInfo: 'AAAAAAA',
      externalRefNo: 'AAAAAAA',
      txnDescription: 'AAAAAAA',
      txnInternalDescription: 'AAAAAAA',
      txnAddnlInfo: 'AAAAAAA',
      createdOn: currentDate,
      createdBy: 'AAAAAAA',
      txnStatus: TxnStatus.ACTIVE,
      txnDebitAccountNo: 'AAAAAAA',
      txnCreditAccountNo: 'AAAAAAA',
      chargeDebitAccountNo: 'AAAAAAA',
      chargeCreditAccountNo: 'AAAAAAA',
    };
  });

  describe('Service methods', () => {
    it('should find an element', () => {
      const returnedFromService = Object.assign(
        {
          txnDate: currentDate.format(DATE_FORMAT),
          txnDateInUTC: currentDate.format(DATE_TIME_FORMAT),
          txnDateInLocal: currentDate.format(DATE_TIME_FORMAT),
          txnDateForSettlement: currentDate.format(DATE_TIME_FORMAT),
          valueDate: currentDate.format(DATE_FORMAT),
          valueDateInUTC: currentDate.format(DATE_TIME_FORMAT),
          valueDateInLocal: currentDate.format(DATE_TIME_FORMAT),
          valueDateForSettlement: currentDate.format(DATE_TIME_FORMAT),
          createdOn: currentDate.format(DATE_TIME_FORMAT),
        },
        elemDefault
      );

      service.find(123).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'GET' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(elemDefault);
    });

    it('should create a RemittanceTransaction', () => {
      const returnedFromService = Object.assign(
        {
          id: 0,
          txnDate: currentDate.format(DATE_FORMAT),
          txnDateInUTC: currentDate.format(DATE_TIME_FORMAT),
          txnDateInLocal: currentDate.format(DATE_TIME_FORMAT),
          txnDateForSettlement: currentDate.format(DATE_TIME_FORMAT),
          valueDate: currentDate.format(DATE_FORMAT),
          valueDateInUTC: currentDate.format(DATE_TIME_FORMAT),
          valueDateInLocal: currentDate.format(DATE_TIME_FORMAT),
          valueDateForSettlement: currentDate.format(DATE_TIME_FORMAT),
          createdOn: currentDate.format(DATE_TIME_FORMAT),
        },
        elemDefault
      );

      const expected = Object.assign(
        {
          txnDate: currentDate,
          txnDateInUTC: currentDate,
          txnDateInLocal: currentDate,
          txnDateForSettlement: currentDate,
          valueDate: currentDate,
          valueDateInUTC: currentDate,
          valueDateInLocal: currentDate,
          valueDateForSettlement: currentDate,
          createdOn: currentDate,
        },
        returnedFromService
      );

      service.create(new RemittanceTransaction()).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'POST' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(expected);
    });

    it('should update a RemittanceTransaction', () => {
      const returnedFromService = Object.assign(
        {
          id: 1,
          txnRefNo: 'BBBBBB',
          accountNo: 'BBBBBB',
          txnDate: currentDate.format(DATE_FORMAT),
          txnDateInUTC: currentDate.format(DATE_TIME_FORMAT),
          txnDateInLocal: currentDate.format(DATE_TIME_FORMAT),
          txnDateForSettlement: currentDate.format(DATE_TIME_FORMAT),
          valueDate: currentDate.format(DATE_FORMAT),
          valueDateInUTC: currentDate.format(DATE_TIME_FORMAT),
          valueDateInLocal: currentDate.format(DATE_TIME_FORMAT),
          valueDateForSettlement: currentDate.format(DATE_TIME_FORMAT),
          beneficiaryAccountNo: 'BBBBBB',
          beneficiaryName: 'BBBBBB',
          beneficiarybankSwiftCode: 'BBBBBB',
          intermediaryBankSwiftCode: 'BBBBBB',
          beneficiaryBankName: 'BBBBBB',
          beneficiaryBankBranchName: 'BBBBBB',
          beneficiaryAddress1: 'BBBBBB',
          beneficiaryAddress2: 'BBBBBB',
          beneficiaryAddress3: 'BBBBBB',
          beneficiaryState: 'BBBBBB',
          beneficiaryPinCode: 'BBBBBB',
          txnAmount: 1,
          amountInAccountCurrency: 1,
          amountInLocalCurrency: 1,
          chargeInAccountCurrency: 1,
          chargeInLocalCurrency: 1,
          fundingSource: 'BBBBBB',
          rate: 1,
          instructedAmount: 1,
          instructedAmountCurrency: 'BBBBBB',
          merchantID: 'BBBBBB',
          merchantInfo: 'BBBBBB',
          externalRefNo: 'BBBBBB',
          txnDescription: 'BBBBBB',
          txnInternalDescription: 'BBBBBB',
          txnAddnlInfo: 'BBBBBB',
          createdOn: currentDate.format(DATE_TIME_FORMAT),
          createdBy: 'BBBBBB',
          txnStatus: 'BBBBBB',
          txnDebitAccountNo: 'BBBBBB',
          txnCreditAccountNo: 'BBBBBB',
          chargeDebitAccountNo: 'BBBBBB',
          chargeCreditAccountNo: 'BBBBBB',
        },
        elemDefault
      );

      const expected = Object.assign(
        {
          txnDate: currentDate,
          txnDateInUTC: currentDate,
          txnDateInLocal: currentDate,
          txnDateForSettlement: currentDate,
          valueDate: currentDate,
          valueDateInUTC: currentDate,
          valueDateInLocal: currentDate,
          valueDateForSettlement: currentDate,
          createdOn: currentDate,
        },
        returnedFromService
      );

      service.update(expected).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'PUT' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(expected);
    });

    it('should partial update a RemittanceTransaction', () => {
      const patchObject = Object.assign(
        {
          txnRefNo: 'BBBBBB',
          accountNo: 'BBBBBB',
          txnDate: currentDate.format(DATE_FORMAT),
          txnDateInUTC: currentDate.format(DATE_TIME_FORMAT),
          txnDateInLocal: currentDate.format(DATE_TIME_FORMAT),
          txnDateForSettlement: currentDate.format(DATE_TIME_FORMAT),
          valueDate: currentDate.format(DATE_FORMAT),
          valueDateInLocal: currentDate.format(DATE_TIME_FORMAT),
          beneficiaryName: 'BBBBBB',
          intermediaryBankSwiftCode: 'BBBBBB',
          beneficiaryBankName: 'BBBBBB',
          beneficiaryAddress1: 'BBBBBB',
          beneficiaryAddress2: 'BBBBBB',
          beneficiaryAddress3: 'BBBBBB',
          beneficiaryPinCode: 'BBBBBB',
          amountInLocalCurrency: 1,
          chargeInLocalCurrency: 1,
          rate: 1,
          instructedAmount: 1,
          externalRefNo: 'BBBBBB',
          txnInternalDescription: 'BBBBBB',
          txnAddnlInfo: 'BBBBBB',
          createdOn: currentDate.format(DATE_TIME_FORMAT),
          createdBy: 'BBBBBB',
          txnStatus: 'BBBBBB',
          chargeDebitAccountNo: 'BBBBBB',
        },
        new RemittanceTransaction()
      );

      const returnedFromService = Object.assign(patchObject, elemDefault);

      const expected = Object.assign(
        {
          txnDate: currentDate,
          txnDateInUTC: currentDate,
          txnDateInLocal: currentDate,
          txnDateForSettlement: currentDate,
          valueDate: currentDate,
          valueDateInUTC: currentDate,
          valueDateInLocal: currentDate,
          valueDateForSettlement: currentDate,
          createdOn: currentDate,
        },
        returnedFromService
      );

      service.partialUpdate(patchObject).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'PATCH' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(expected);
    });

    it('should return a list of RemittanceTransaction', () => {
      const returnedFromService = Object.assign(
        {
          id: 1,
          txnRefNo: 'BBBBBB',
          accountNo: 'BBBBBB',
          txnDate: currentDate.format(DATE_FORMAT),
          txnDateInUTC: currentDate.format(DATE_TIME_FORMAT),
          txnDateInLocal: currentDate.format(DATE_TIME_FORMAT),
          txnDateForSettlement: currentDate.format(DATE_TIME_FORMAT),
          valueDate: currentDate.format(DATE_FORMAT),
          valueDateInUTC: currentDate.format(DATE_TIME_FORMAT),
          valueDateInLocal: currentDate.format(DATE_TIME_FORMAT),
          valueDateForSettlement: currentDate.format(DATE_TIME_FORMAT),
          beneficiaryAccountNo: 'BBBBBB',
          beneficiaryName: 'BBBBBB',
          beneficiarybankSwiftCode: 'BBBBBB',
          intermediaryBankSwiftCode: 'BBBBBB',
          beneficiaryBankName: 'BBBBBB',
          beneficiaryBankBranchName: 'BBBBBB',
          beneficiaryAddress1: 'BBBBBB',
          beneficiaryAddress2: 'BBBBBB',
          beneficiaryAddress3: 'BBBBBB',
          beneficiaryState: 'BBBBBB',
          beneficiaryPinCode: 'BBBBBB',
          txnAmount: 1,
          amountInAccountCurrency: 1,
          amountInLocalCurrency: 1,
          chargeInAccountCurrency: 1,
          chargeInLocalCurrency: 1,
          fundingSource: 'BBBBBB',
          rate: 1,
          instructedAmount: 1,
          instructedAmountCurrency: 'BBBBBB',
          merchantID: 'BBBBBB',
          merchantInfo: 'BBBBBB',
          externalRefNo: 'BBBBBB',
          txnDescription: 'BBBBBB',
          txnInternalDescription: 'BBBBBB',
          txnAddnlInfo: 'BBBBBB',
          createdOn: currentDate.format(DATE_TIME_FORMAT),
          createdBy: 'BBBBBB',
          txnStatus: 'BBBBBB',
          txnDebitAccountNo: 'BBBBBB',
          txnCreditAccountNo: 'BBBBBB',
          chargeDebitAccountNo: 'BBBBBB',
          chargeCreditAccountNo: 'BBBBBB',
        },
        elemDefault
      );

      const expected = Object.assign(
        {
          txnDate: currentDate,
          txnDateInUTC: currentDate,
          txnDateInLocal: currentDate,
          txnDateForSettlement: currentDate,
          valueDate: currentDate,
          valueDateInUTC: currentDate,
          valueDateInLocal: currentDate,
          valueDateForSettlement: currentDate,
          createdOn: currentDate,
        },
        returnedFromService
      );

      service.query().subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'GET' });
      req.flush([returnedFromService]);
      httpMock.verify();
      expect(expectedResult).toContainEqual(expected);
    });

    it('should delete a RemittanceTransaction', () => {
      service.delete(123).subscribe(resp => (expectedResult = resp.ok));

      const req = httpMock.expectOne({ method: 'DELETE' });
      req.flush({ status: 200 });
      expect(expectedResult);
    });

    describe('addRemittanceTransactionToCollectionIfMissing', () => {
      it('should add a RemittanceTransaction to an empty array', () => {
        const remittanceTransaction: IRemittanceTransaction = { id: 123 };
        expectedResult = service.addRemittanceTransactionToCollectionIfMissing([], remittanceTransaction);
        expect(expectedResult).toHaveLength(1);
        expect(expectedResult).toContain(remittanceTransaction);
      });

      it('should not add a RemittanceTransaction to an array that contains it', () => {
        const remittanceTransaction: IRemittanceTransaction = { id: 123 };
        const remittanceTransactionCollection: IRemittanceTransaction[] = [
          {
            ...remittanceTransaction,
          },
          { id: 456 },
        ];
        expectedResult = service.addRemittanceTransactionToCollectionIfMissing(remittanceTransactionCollection, remittanceTransaction);
        expect(expectedResult).toHaveLength(2);
      });

      it("should add a RemittanceTransaction to an array that doesn't contain it", () => {
        const remittanceTransaction: IRemittanceTransaction = { id: 123 };
        const remittanceTransactionCollection: IRemittanceTransaction[] = [{ id: 456 }];
        expectedResult = service.addRemittanceTransactionToCollectionIfMissing(remittanceTransactionCollection, remittanceTransaction);
        expect(expectedResult).toHaveLength(2);
        expect(expectedResult).toContain(remittanceTransaction);
      });

      it('should add only unique RemittanceTransaction to an array', () => {
        const remittanceTransactionArray: IRemittanceTransaction[] = [{ id: 123 }, { id: 456 }, { id: 33679 }];
        const remittanceTransactionCollection: IRemittanceTransaction[] = [{ id: 123 }];
        expectedResult = service.addRemittanceTransactionToCollectionIfMissing(
          remittanceTransactionCollection,
          ...remittanceTransactionArray
        );
        expect(expectedResult).toHaveLength(3);
      });

      it('should accept varargs', () => {
        const remittanceTransaction: IRemittanceTransaction = { id: 123 };
        const remittanceTransaction2: IRemittanceTransaction = { id: 456 };
        expectedResult = service.addRemittanceTransactionToCollectionIfMissing([], remittanceTransaction, remittanceTransaction2);
        expect(expectedResult).toHaveLength(2);
        expect(expectedResult).toContain(remittanceTransaction);
        expect(expectedResult).toContain(remittanceTransaction2);
      });

      it('should accept null and undefined values', () => {
        const remittanceTransaction: IRemittanceTransaction = { id: 123 };
        expectedResult = service.addRemittanceTransactionToCollectionIfMissing([], null, remittanceTransaction, undefined);
        expect(expectedResult).toHaveLength(1);
        expect(expectedResult).toContain(remittanceTransaction);
      });

      it('should return initial array if no RemittanceTransaction is added', () => {
        const remittanceTransactionCollection: IRemittanceTransaction[] = [{ id: 123 }];
        expectedResult = service.addRemittanceTransactionToCollectionIfMissing(remittanceTransactionCollection, undefined, null);
        expect(expectedResult).toEqual(remittanceTransactionCollection);
      });
    });
  });

  afterEach(() => {
    httpMock.verify();
  });
});
