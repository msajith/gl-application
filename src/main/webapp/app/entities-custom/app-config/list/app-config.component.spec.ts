import { ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpHeaders, HttpResponse } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { of } from 'rxjs';

import { AppConfigService } from '../service/app-config.service';

import { AppConfigComponent } from './app-config.component';

describe('AppConfig Management Component', () => {
  let comp: AppConfigComponent;
  let fixture: ComponentFixture<AppConfigComponent>;
  let service: AppConfigService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      declarations: [AppConfigComponent],
    })
      .overrideTemplate(AppConfigComponent, '')
      .compileComponents();

    fixture = TestBed.createComponent(AppConfigComponent);
    comp = fixture.componentInstance;
    service = TestBed.inject(AppConfigService);

    const headers = new HttpHeaders();
    jest.spyOn(service, 'query').mockReturnValue(
      of(
        new HttpResponse({
          body: [{ id: 123 }],
          headers,
        })
      )
    );
  });

  it('Should call load all on init', () => {
    // WHEN
    comp.ngOnInit();

    // THEN
    expect(service.query).toHaveBeenCalled();
    expect(comp.appConfigs?.[0]).toEqual(expect.objectContaining({ id: 123 }));
  });
});
