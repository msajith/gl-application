import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, Router } from '@angular/router';
import { Observable, of, EMPTY } from 'rxjs';
import { mergeMap } from 'rxjs/operators';

import { IAppConfig, AppConfig } from '../app-config.model';
import { AppConfigService } from '../service/app-config.service';

@Injectable({ providedIn: 'root' })
export class AppConfigRoutingResolveService implements Resolve<IAppConfig> {
  constructor(protected service: AppConfigService, protected router: Router) {}

  resolve(route: ActivatedRouteSnapshot): Observable<IAppConfig> | Observable<never> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        mergeMap((appConfig: HttpResponse<AppConfig>) => {
          if (appConfig.body) {
            return of(appConfig.body);
          } else {
            this.router.navigate(['404']);
            return EMPTY;
          }
        })
      );
    }
    return of(new AppConfig());
  }
}
