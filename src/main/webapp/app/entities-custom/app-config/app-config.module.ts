import { NgModule } from '@angular/core';
import { SharedModule } from 'app/shared/shared.module';
import { AppConfigComponent } from './list/app-config.component';
import { AppConfigDetailComponent } from './detail/app-config-detail.component';
import { AppConfigUpdateComponent } from './update/app-config-update.component';
import { AppConfigDeleteDialogComponent } from './delete/app-config-delete-dialog.component';
import { AppConfigRoutingModule } from './route/app-config-routing.module';

@NgModule({
  imports: [SharedModule, AppConfigRoutingModule],
  declarations: [AppConfigComponent, AppConfigDetailComponent, AppConfigUpdateComponent, AppConfigDeleteDialogComponent],
  entryComponents: [AppConfigDeleteDialogComponent],
})
export class AppConfigModule {}
