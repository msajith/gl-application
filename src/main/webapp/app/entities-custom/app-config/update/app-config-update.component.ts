import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { finalize } from 'rxjs/operators';

import { IAppConfig, AppConfig } from '../app-config.model';
import { AppConfigService } from '../service/app-config.service';

@Component({
  selector: 'jhi-app-config-update',
  templateUrl: './app-config-update.component.html',
})
export class AppConfigUpdateComponent implements OnInit {
  isSaving = false;

  editForm = this.fb.group({
    id: [],
    code: [],
    subCode: [],
    value: [],
    description: [],
  });

  constructor(protected appConfigService: AppConfigService, protected activatedRoute: ActivatedRoute, protected fb: FormBuilder) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ appConfig }) => {
      this.updateForm(appConfig);
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const appConfig = this.createFromForm();
    if (appConfig.id !== undefined) {
      this.subscribeToSaveResponse(this.appConfigService.update(appConfig));
    } else {
      this.subscribeToSaveResponse(this.appConfigService.create(appConfig));
    }
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IAppConfig>>): void {
    result.pipe(finalize(() => this.onSaveFinalize())).subscribe({
      next: () => this.onSaveSuccess(),
      error: () => this.onSaveError(),
    });
  }

  protected onSaveSuccess(): void {
    this.previousState();
  }

  protected onSaveError(): void {
    // Api for inheritance.
  }

  protected onSaveFinalize(): void {
    this.isSaving = false;
  }

  protected updateForm(appConfig: IAppConfig): void {
    this.editForm.patchValue({
      id: appConfig.id,
      code: appConfig.code,
      subCode: appConfig.subCode,
      value: appConfig.value,
      description: appConfig.description,
    });
  }

  protected createFromForm(): IAppConfig {
    return {
      ...new AppConfig(),
      id: this.editForm.get(['id'])!.value,
      code: this.editForm.get(['code'])!.value,
      subCode: this.editForm.get(['subCode'])!.value,
      value: this.editForm.get(['value'])!.value,
      description: this.editForm.get(['description'])!.value,
    };
  }
}
