import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { isPresent } from 'app/core/util/operators';
import { ApplicationConfigService } from 'app/core/config/application-config.service';
import { createRequestOption } from 'app/core/request/request-util';
import { IFundingSourceType, getFundingSourceTypeIdentifier } from '../funding-source-type.model';

export type EntityResponseType = HttpResponse<IFundingSourceType>;
export type EntityArrayResponseType = HttpResponse<IFundingSourceType[]>;

@Injectable({ providedIn: 'root' })
export class FundingSourceTypeService {
  protected resourceUrl = this.applicationConfigService.getEndpointFor('api/funding-source-types');

  constructor(protected http: HttpClient, protected applicationConfigService: ApplicationConfigService) {}

  create(fundingSourceType: IFundingSourceType): Observable<EntityResponseType> {
    return this.http.post<IFundingSourceType>(this.resourceUrl, fundingSourceType, { observe: 'response' });
  }

  update(fundingSourceType: IFundingSourceType): Observable<EntityResponseType> {
    return this.http.put<IFundingSourceType>(
      `${this.resourceUrl}/${getFundingSourceTypeIdentifier(fundingSourceType) as number}`,
      fundingSourceType,
      { observe: 'response' }
    );
  }

  partialUpdate(fundingSourceType: IFundingSourceType): Observable<EntityResponseType> {
    return this.http.patch<IFundingSourceType>(
      `${this.resourceUrl}/${getFundingSourceTypeIdentifier(fundingSourceType) as number}`,
      fundingSourceType,
      { observe: 'response' }
    );
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http.get<IFundingSourceType>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<IFundingSourceType[]>(this.resourceUrl, { params: options, observe: 'response' });
  }

  delete(id: number): Observable<HttpResponse<{}>> {
    return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  addFundingSourceTypeToCollectionIfMissing(
    fundingSourceTypeCollection: IFundingSourceType[],
    ...fundingSourceTypesToCheck: (IFundingSourceType | null | undefined)[]
  ): IFundingSourceType[] {
    const fundingSourceTypes: IFundingSourceType[] = fundingSourceTypesToCheck.filter(isPresent);
    if (fundingSourceTypes.length > 0) {
      const fundingSourceTypeCollectionIdentifiers = fundingSourceTypeCollection.map(
        fundingSourceTypeItem => getFundingSourceTypeIdentifier(fundingSourceTypeItem)!
      );
      const fundingSourceTypesToAdd = fundingSourceTypes.filter(fundingSourceTypeItem => {
        const fundingSourceTypeIdentifier = getFundingSourceTypeIdentifier(fundingSourceTypeItem);
        if (fundingSourceTypeIdentifier == null || fundingSourceTypeCollectionIdentifiers.includes(fundingSourceTypeIdentifier)) {
          return false;
        }
        fundingSourceTypeCollectionIdentifiers.push(fundingSourceTypeIdentifier);
        return true;
      });
      return [...fundingSourceTypesToAdd, ...fundingSourceTypeCollection];
    }
    return fundingSourceTypeCollection;
  }
}
