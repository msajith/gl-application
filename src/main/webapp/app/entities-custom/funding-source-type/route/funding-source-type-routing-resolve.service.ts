import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, Router } from '@angular/router';
import { Observable, of, EMPTY } from 'rxjs';
import { mergeMap } from 'rxjs/operators';

import { IFundingSourceType, FundingSourceType } from '../funding-source-type.model';
import { FundingSourceTypeService } from '../service/funding-source-type.service';

@Injectable({ providedIn: 'root' })
export class FundingSourceTypeRoutingResolveService implements Resolve<IFundingSourceType> {
  constructor(protected service: FundingSourceTypeService, protected router: Router) {}

  resolve(route: ActivatedRouteSnapshot): Observable<IFundingSourceType> | Observable<never> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        mergeMap((fundingSourceType: HttpResponse<FundingSourceType>) => {
          if (fundingSourceType.body) {
            return of(fundingSourceType.body);
          } else {
            this.router.navigate(['404']);
            return EMPTY;
          }
        })
      );
    }
    return of(new FundingSourceType());
  }
}
