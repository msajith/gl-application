import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

import { IFundingSourceType } from '../funding-source-type.model';
import { FundingSourceTypeService } from '../service/funding-source-type.service';
import { FundingSourceTypeDeleteDialogComponent } from '../delete/funding-source-type-delete-dialog.component';

@Component({
  selector: 'jhi-funding-source-type',
  templateUrl: './funding-source-type.component.html',
})
export class FundingSourceTypeComponent implements OnInit {
  fundingSourceTypes?: IFundingSourceType[];
  isLoading = false;

  constructor(protected fundingSourceTypeService: FundingSourceTypeService, protected modalService: NgbModal) {}

  loadAll(): void {
    this.isLoading = true;

    this.fundingSourceTypeService.query().subscribe({
      next: (res: HttpResponse<IFundingSourceType[]>) => {
        this.isLoading = false;
        this.fundingSourceTypes = res.body ?? [];
      },
      error: () => {
        this.isLoading = false;
      },
    });
  }

  ngOnInit(): void {
    this.loadAll();
  }

  trackId(index: number, item: IFundingSourceType): number {
    return item.id!;
  }

  delete(fundingSourceType: IFundingSourceType): void {
    const modalRef = this.modalService.open(FundingSourceTypeDeleteDialogComponent, { size: 'lg', backdrop: 'static' });
    modalRef.componentInstance.fundingSourceType = fundingSourceType;
    // unsubscribe not needed because closed completes on modal close
    modalRef.closed.subscribe(reason => {
      if (reason === 'deleted') {
        this.loadAll();
      }
    });
  }
}
