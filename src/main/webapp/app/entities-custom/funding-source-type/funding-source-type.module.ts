import { NgModule } from '@angular/core';
import { SharedModule } from 'app/shared/shared.module';
import { FundingSourceTypeComponent } from './list/funding-source-type.component';
import { FundingSourceTypeDetailComponent } from './detail/funding-source-type-detail.component';
import { FundingSourceTypeUpdateComponent } from './update/funding-source-type-update.component';
import { FundingSourceTypeDeleteDialogComponent } from './delete/funding-source-type-delete-dialog.component';
import { FundingSourceTypeRoutingModule } from './route/funding-source-type-routing.module';

@NgModule({
  imports: [SharedModule, FundingSourceTypeRoutingModule],
  declarations: [
    FundingSourceTypeComponent,
    FundingSourceTypeDetailComponent,
    FundingSourceTypeUpdateComponent,
    FundingSourceTypeDeleteDialogComponent,
  ],
  entryComponents: [FundingSourceTypeDeleteDialogComponent],
})
export class FundingSourceTypeModule {}
