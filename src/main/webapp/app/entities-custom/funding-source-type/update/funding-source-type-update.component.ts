import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { finalize } from 'rxjs/operators';

import { IFundingSourceType, FundingSourceType } from '../funding-source-type.model';
import { FundingSourceTypeService } from '../service/funding-source-type.service';

@Component({
  selector: 'jhi-funding-source-type-update',
  templateUrl: './funding-source-type-update.component.html',
})
export class FundingSourceTypeUpdateComponent implements OnInit {
  isSaving = false;

  editForm = this.fb.group({
    id: [],
    name: [],
    code: [],
  });

  constructor(
    protected fundingSourceTypeService: FundingSourceTypeService,
    protected activatedRoute: ActivatedRoute,
    protected fb: FormBuilder
  ) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ fundingSourceType }) => {
      this.updateForm(fundingSourceType);
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const fundingSourceType = this.createFromForm();
    if (fundingSourceType.id !== undefined) {
      this.subscribeToSaveResponse(this.fundingSourceTypeService.update(fundingSourceType));
    } else {
      this.subscribeToSaveResponse(this.fundingSourceTypeService.create(fundingSourceType));
    }
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IFundingSourceType>>): void {
    result.pipe(finalize(() => this.onSaveFinalize())).subscribe({
      next: () => this.onSaveSuccess(),
      error: () => this.onSaveError(),
    });
  }

  protected onSaveSuccess(): void {
    this.previousState();
  }

  protected onSaveError(): void {
    // Api for inheritance.
  }

  protected onSaveFinalize(): void {
    this.isSaving = false;
  }

  protected updateForm(fundingSourceType: IFundingSourceType): void {
    this.editForm.patchValue({
      id: fundingSourceType.id,
      name: fundingSourceType.name,
      code: fundingSourceType.code,
    });
  }

  protected createFromForm(): IFundingSourceType {
    return {
      ...new FundingSourceType(),
      id: this.editForm.get(['id'])!.value,
      name: this.editForm.get(['name'])!.value,
      code: this.editForm.get(['code'])!.value,
    };
  }
}
