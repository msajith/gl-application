import { Component } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

import { IFundingSourceType } from '../funding-source-type.model';
import { FundingSourceTypeService } from '../service/funding-source-type.service';

@Component({
  templateUrl: './funding-source-type-delete-dialog.component.html',
})
export class FundingSourceTypeDeleteDialogComponent {
  fundingSourceType?: IFundingSourceType;

  constructor(protected fundingSourceTypeService: FundingSourceTypeService, protected activeModal: NgbActiveModal) {}

  cancel(): void {
    this.activeModal.dismiss();
  }

  confirmDelete(id: number): void {
    this.fundingSourceTypeService.delete(id).subscribe(() => {
      this.activeModal.close('deleted');
    });
  }
}
