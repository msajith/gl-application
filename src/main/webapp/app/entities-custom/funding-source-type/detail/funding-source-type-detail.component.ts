import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IFundingSourceType } from '../funding-source-type.model';

@Component({
  selector: 'jhi-funding-source-type-detail',
  templateUrl: './funding-source-type-detail.component.html',
})
export class FundingSourceTypeDetailComponent implements OnInit {
  fundingSourceType: IFundingSourceType | null = null;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ fundingSourceType }) => {
      this.fundingSourceType = fundingSourceType;
    });
  }

  previousState(): void {
    window.history.back();
  }
}
