import { Component } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

import { IWalletTopupTransaction } from '../wallet-topup-transaction.model';
import { WalletTopupTransactionService } from '../service/wallet-topup-transaction.service';

@Component({
  templateUrl: './wallet-topup-transaction-delete-dialog.component.html',
})
export class WalletTopupTransactionDeleteDialogComponent {
  walletTopupTransaction?: IWalletTopupTransaction;

  constructor(protected walletTopupTransactionService: WalletTopupTransactionService, protected activeModal: NgbActiveModal) {}

  cancel(): void {
    this.activeModal.dismiss();
  }

  confirmDelete(id: number): void {
    this.walletTopupTransactionService.delete(id).subscribe(() => {
      this.activeModal.close('deleted');
    });
  }
}
