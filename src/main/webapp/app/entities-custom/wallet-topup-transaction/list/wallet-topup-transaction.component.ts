import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

import { IWalletTopupTransaction } from '../wallet-topup-transaction.model';
import { WalletTopupTransactionService } from '../service/wallet-topup-transaction.service';
import { WalletTopupTransactionDeleteDialogComponent } from '../delete/wallet-topup-transaction-delete-dialog.component';

@Component({
  selector: 'jhi-wallet-topup-transaction',
  templateUrl: './wallet-topup-transaction.component.html',
})
export class WalletTopupTransactionComponent implements OnInit {
  walletTopupTransactions?: IWalletTopupTransaction[];
  isLoading = false;

  constructor(protected walletTopupTransactionService: WalletTopupTransactionService, protected modalService: NgbModal) {}

  loadAll(): void {
    this.isLoading = true;

    this.walletTopupTransactionService.query().subscribe({
      next: (res: HttpResponse<IWalletTopupTransaction[]>) => {
        this.isLoading = false;
        this.walletTopupTransactions = res.body ?? [];
      },
      error: () => {
        this.isLoading = false;
      },
    });
  }

  ngOnInit(): void {
    this.loadAll();
  }

  trackId(index: number, item: IWalletTopupTransaction): number {
    return item.id!;
  }

  delete(walletTopupTransaction: IWalletTopupTransaction): void {
    const modalRef = this.modalService.open(WalletTopupTransactionDeleteDialogComponent, { size: 'lg', backdrop: 'static' });
    modalRef.componentInstance.walletTopupTransaction = walletTopupTransaction;
    // unsubscribe not needed because closed completes on modal close
    modalRef.closed.subscribe(reason => {
      if (reason === 'deleted') {
        this.loadAll();
      }
    });
  }
}
