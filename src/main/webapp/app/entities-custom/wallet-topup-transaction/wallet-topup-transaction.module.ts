import { NgModule } from '@angular/core';
import { SharedModule } from 'app/shared/shared.module';
import { WalletTopupTransactionComponent } from './list/wallet-topup-transaction.component';
import { WalletTopupTransactionDetailComponent } from './detail/wallet-topup-transaction-detail.component';
import { WalletTopupTransactionUpdateComponent } from './update/wallet-topup-transaction-update.component';
import { WalletTopupTransactionDeleteDialogComponent } from './delete/wallet-topup-transaction-delete-dialog.component';
import { WalletTopupTransactionRoutingModule } from './route/wallet-topup-transaction-routing.module';

@NgModule({
  imports: [SharedModule, WalletTopupTransactionRoutingModule],
  declarations: [
    WalletTopupTransactionComponent,
    WalletTopupTransactionDetailComponent,
    WalletTopupTransactionUpdateComponent,
    WalletTopupTransactionDeleteDialogComponent,
  ],
  entryComponents: [WalletTopupTransactionDeleteDialogComponent],
})
export class WalletTopupTransactionModule {}
