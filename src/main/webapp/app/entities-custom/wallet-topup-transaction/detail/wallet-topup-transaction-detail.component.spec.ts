import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { WalletTopupTransactionDetailComponent } from './wallet-topup-transaction-detail.component';

describe('WalletTopupTransaction Management Detail Component', () => {
  let comp: WalletTopupTransactionDetailComponent;
  let fixture: ComponentFixture<WalletTopupTransactionDetailComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [WalletTopupTransactionDetailComponent],
      providers: [
        {
          provide: ActivatedRoute,
          useValue: { data: of({ walletTopupTransaction: { id: 123 } }) },
        },
      ],
    })
      .overrideTemplate(WalletTopupTransactionDetailComponent, '')
      .compileComponents();
    fixture = TestBed.createComponent(WalletTopupTransactionDetailComponent);
    comp = fixture.componentInstance;
  });

  describe('OnInit', () => {
    it('Should load walletTopupTransaction on init', () => {
      // WHEN
      comp.ngOnInit();

      // THEN
      expect(comp.walletTopupTransaction).toEqual(expect.objectContaining({ id: 123 }));
    });
  });
});
