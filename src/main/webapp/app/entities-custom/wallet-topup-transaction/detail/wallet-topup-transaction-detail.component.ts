import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IWalletTopupTransaction } from '../wallet-topup-transaction.model';

@Component({
  selector: 'jhi-wallet-topup-transaction-detail',
  templateUrl: './wallet-topup-transaction-detail.component.html',
})
export class WalletTopupTransactionDetailComponent implements OnInit {
  walletTopupTransaction: IWalletTopupTransaction | null = null;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ walletTopupTransaction }) => {
      this.walletTopupTransaction = walletTopupTransaction;
    });
  }

  previousState(): void {
    window.history.back();
  }
}
