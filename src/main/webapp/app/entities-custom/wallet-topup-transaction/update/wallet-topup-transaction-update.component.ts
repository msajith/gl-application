import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { finalize, map } from 'rxjs/operators';

import dayjs from 'dayjs/esm';
import { DATE_TIME_FORMAT } from 'app/config/input.constants';

import { IWalletTopupTransaction, WalletTopupTransaction } from '../wallet-topup-transaction.model';
import { WalletTopupTransactionService } from '../service/wallet-topup-transaction.service';
import { ICountry } from 'app/entities/country/country.model';
import { CountryService } from 'app/entities/country/service/country.service';
import { ICurrency } from 'app/entities/currency/currency.model';
import { CurrencyService } from 'app/entities/currency/service/currency.service';
import { IPurposeOfTxn } from 'app/entities/purpose-of-txn/purpose-of-txn.model';
import { PurposeOfTxnService } from 'app/entities/purpose-of-txn/service/purpose-of-txn.service';
import { IFundingSourceType } from 'app/entities/funding-source-type/funding-source-type.model';
import { FundingSourceTypeService } from 'app/entities/funding-source-type/service/funding-source-type.service';
import { TxnStatus } from 'app/entities/enumerations/txn-status.model';

@Component({
  selector: 'jhi-wallet-topup-transaction-update',
  templateUrl: './wallet-topup-transaction-update.component.html',
})
export class WalletTopupTransactionUpdateComponent implements OnInit {
  isSaving = false;
  txnStatusValues = Object.keys(TxnStatus);

  countriesSharedCollection: ICountry[] = [];
  currenciesSharedCollection: ICurrency[] = [];
  purposeOfTxnsSharedCollection: IPurposeOfTxn[] = [];
  fundingSourceTypesSharedCollection: IFundingSourceType[] = [];

  editForm = this.fb.group({
    id: [],
    txnRefNo: [],
    accountNo: [],
    txnDate: [],
    txnDateInUTC: [],
    txnDateInLocal: [],
    txnDateForSettlement: [],
    valueDate: [],
    valueDateInUTC: [],
    valueDateInLocal: [],
    valueDateForSettlement: [],
    txnAmount: [],
    instructedAmount: [],
    amountInAccountCurrency: [],
    amountInLocalCurrency: [],
    chargeInAccountCurrency: [],
    chargeInLocalCurrency: [],
    fundingSource: [],
    merchantID: [],
    merchantInfo: [],
    bankCode: [],
    externalRefNo: [],
    txnDescription: [],
    txnInternalDescription: [],
    txnAddnlInfo: [],
    createdOn: [],
    createdBy: [],
    txnStatus: [],
    txnDebitAccountNo: [],
    txnCreditAccountNo: [],
    chargeDebitAccountNo: [],
    chargeCreditAccountNo: [],
    country: [],
    txnCurrency: [],
    accountCurrency: [],
    instructedCurrency: [],
    purposeOfTxn: [],
    fundingSourceType: [],
  });

  constructor(
    protected walletTopupTransactionService: WalletTopupTransactionService,
    protected countryService: CountryService,
    protected currencyService: CurrencyService,
    protected purposeOfTxnService: PurposeOfTxnService,
    protected fundingSourceTypeService: FundingSourceTypeService,
    protected activatedRoute: ActivatedRoute,
    protected fb: FormBuilder
  ) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ walletTopupTransaction }) => {
      if (walletTopupTransaction.id === undefined) {
        const today = dayjs().startOf('day');
        walletTopupTransaction.txnDateInUTC = today;
        walletTopupTransaction.txnDateInLocal = today;
        walletTopupTransaction.txnDateForSettlement = today;
        walletTopupTransaction.valueDateInUTC = today;
        walletTopupTransaction.valueDateInLocal = today;
        walletTopupTransaction.valueDateForSettlement = today;
        walletTopupTransaction.createdOn = today;
      }

      this.updateForm(walletTopupTransaction);

      this.loadRelationshipsOptions();
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const walletTopupTransaction = this.createFromForm();
    if (walletTopupTransaction.id !== undefined) {
      this.subscribeToSaveResponse(this.walletTopupTransactionService.update(walletTopupTransaction));
    } else {
      this.subscribeToSaveResponse(this.walletTopupTransactionService.create(walletTopupTransaction));
    }
  }

  trackCountryById(index: number, item: ICountry): number {
    return item.id!;
  }

  trackCurrencyById(index: number, item: ICurrency): number {
    return item.id!;
  }

  trackPurposeOfTxnById(index: number, item: IPurposeOfTxn): number {
    return item.id!;
  }

  trackFundingSourceTypeById(index: number, item: IFundingSourceType): number {
    return item.id!;
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IWalletTopupTransaction>>): void {
    result.pipe(finalize(() => this.onSaveFinalize())).subscribe({
      next: () => this.onSaveSuccess(),
      error: () => this.onSaveError(),
    });
  }

  protected onSaveSuccess(): void {
    this.previousState();
  }

  protected onSaveError(): void {
    // Api for inheritance.
  }

  protected onSaveFinalize(): void {
    this.isSaving = false;
  }

  protected updateForm(walletTopupTransaction: IWalletTopupTransaction): void {
    this.editForm.patchValue({
      id: walletTopupTransaction.id,
      txnRefNo: walletTopupTransaction.txnRefNo,
      accountNo: walletTopupTransaction.accountNo,
      txnDate: walletTopupTransaction.txnDate,
      txnDateInUTC: walletTopupTransaction.txnDateInUTC ? walletTopupTransaction.txnDateInUTC.format(DATE_TIME_FORMAT) : null,
      txnDateInLocal: walletTopupTransaction.txnDateInLocal ? walletTopupTransaction.txnDateInLocal.format(DATE_TIME_FORMAT) : null,
      txnDateForSettlement: walletTopupTransaction.txnDateForSettlement
        ? walletTopupTransaction.txnDateForSettlement.format(DATE_TIME_FORMAT)
        : null,
      valueDate: walletTopupTransaction.valueDate,
      valueDateInUTC: walletTopupTransaction.valueDateInUTC ? walletTopupTransaction.valueDateInUTC.format(DATE_TIME_FORMAT) : null,
      valueDateInLocal: walletTopupTransaction.valueDateInLocal ? walletTopupTransaction.valueDateInLocal.format(DATE_TIME_FORMAT) : null,
      valueDateForSettlement: walletTopupTransaction.valueDateForSettlement
        ? walletTopupTransaction.valueDateForSettlement.format(DATE_TIME_FORMAT)
        : null,
      txnAmount: walletTopupTransaction.txnAmount,
      instructedAmount: walletTopupTransaction.instructedAmount,
      amountInAccountCurrency: walletTopupTransaction.amountInAccountCurrency,
      amountInLocalCurrency: walletTopupTransaction.amountInLocalCurrency,
      chargeInAccountCurrency: walletTopupTransaction.chargeInAccountCurrency,
      chargeInLocalCurrency: walletTopupTransaction.chargeInLocalCurrency,
      fundingSource: walletTopupTransaction.fundingSource,
      merchantID: walletTopupTransaction.merchantID,
      merchantInfo: walletTopupTransaction.merchantInfo,
      bankCode: walletTopupTransaction.bankCode,
      externalRefNo: walletTopupTransaction.externalRefNo,
      txnDescription: walletTopupTransaction.txnDescription,
      txnInternalDescription: walletTopupTransaction.txnInternalDescription,
      txnAddnlInfo: walletTopupTransaction.txnAddnlInfo,
      createdOn: walletTopupTransaction.createdOn ? walletTopupTransaction.createdOn.format(DATE_TIME_FORMAT) : null,
      createdBy: walletTopupTransaction.createdBy,
      txnStatus: walletTopupTransaction.txnStatus,
      txnDebitAccountNo: walletTopupTransaction.txnDebitAccountNo,
      txnCreditAccountNo: walletTopupTransaction.txnCreditAccountNo,
      chargeDebitAccountNo: walletTopupTransaction.chargeDebitAccountNo,
      chargeCreditAccountNo: walletTopupTransaction.chargeCreditAccountNo,
      country: walletTopupTransaction.country,
      txnCurrency: walletTopupTransaction.txnCurrency,
      accountCurrency: walletTopupTransaction.accountCurrency,
      instructedCurrency: walletTopupTransaction.instructedCurrency,
      purposeOfTxn: walletTopupTransaction.purposeOfTxn,
      fundingSourceType: walletTopupTransaction.fundingSourceType,
    });

    this.countriesSharedCollection = this.countryService.addCountryToCollectionIfMissing(
      this.countriesSharedCollection,
      walletTopupTransaction.country
    );
    this.currenciesSharedCollection = this.currencyService.addCurrencyToCollectionIfMissing(
      this.currenciesSharedCollection,
      walletTopupTransaction.txnCurrency,
      walletTopupTransaction.accountCurrency,
      walletTopupTransaction.instructedCurrency
    );
    this.purposeOfTxnsSharedCollection = this.purposeOfTxnService.addPurposeOfTxnToCollectionIfMissing(
      this.purposeOfTxnsSharedCollection,
      walletTopupTransaction.purposeOfTxn
    );
    this.fundingSourceTypesSharedCollection = this.fundingSourceTypeService.addFundingSourceTypeToCollectionIfMissing(
      this.fundingSourceTypesSharedCollection,
      walletTopupTransaction.fundingSourceType
    );
  }

  protected loadRelationshipsOptions(): void {
    this.countryService
      .query()
      .pipe(map((res: HttpResponse<ICountry[]>) => res.body ?? []))
      .pipe(
        map((countries: ICountry[]) => this.countryService.addCountryToCollectionIfMissing(countries, this.editForm.get('country')!.value))
      )
      .subscribe((countries: ICountry[]) => (this.countriesSharedCollection = countries));

    this.currencyService
      .query()
      .pipe(map((res: HttpResponse<ICurrency[]>) => res.body ?? []))
      .pipe(
        map((currencies: ICurrency[]) =>
          this.currencyService.addCurrencyToCollectionIfMissing(
            currencies,
            this.editForm.get('txnCurrency')!.value,
            this.editForm.get('accountCurrency')!.value,
            this.editForm.get('instructedCurrency')!.value
          )
        )
      )
      .subscribe((currencies: ICurrency[]) => (this.currenciesSharedCollection = currencies));

    this.purposeOfTxnService
      .query()
      .pipe(map((res: HttpResponse<IPurposeOfTxn[]>) => res.body ?? []))
      .pipe(
        map((purposeOfTxns: IPurposeOfTxn[]) =>
          this.purposeOfTxnService.addPurposeOfTxnToCollectionIfMissing(purposeOfTxns, this.editForm.get('purposeOfTxn')!.value)
        )
      )
      .subscribe((purposeOfTxns: IPurposeOfTxn[]) => (this.purposeOfTxnsSharedCollection = purposeOfTxns));

    this.fundingSourceTypeService
      .query()
      .pipe(map((res: HttpResponse<IFundingSourceType[]>) => res.body ?? []))
      .pipe(
        map((fundingSourceTypes: IFundingSourceType[]) =>
          this.fundingSourceTypeService.addFundingSourceTypeToCollectionIfMissing(
            fundingSourceTypes,
            this.editForm.get('fundingSourceType')!.value
          )
        )
      )
      .subscribe((fundingSourceTypes: IFundingSourceType[]) => (this.fundingSourceTypesSharedCollection = fundingSourceTypes));
  }

  protected createFromForm(): IWalletTopupTransaction {
    return {
      ...new WalletTopupTransaction(),
      id: this.editForm.get(['id'])!.value,
      txnRefNo: this.editForm.get(['txnRefNo'])!.value,
      accountNo: this.editForm.get(['accountNo'])!.value,
      txnDate: this.editForm.get(['txnDate'])!.value,
      txnDateInUTC: this.editForm.get(['txnDateInUTC'])!.value
        ? dayjs(this.editForm.get(['txnDateInUTC'])!.value, DATE_TIME_FORMAT)
        : undefined,
      txnDateInLocal: this.editForm.get(['txnDateInLocal'])!.value
        ? dayjs(this.editForm.get(['txnDateInLocal'])!.value, DATE_TIME_FORMAT)
        : undefined,
      txnDateForSettlement: this.editForm.get(['txnDateForSettlement'])!.value
        ? dayjs(this.editForm.get(['txnDateForSettlement'])!.value, DATE_TIME_FORMAT)
        : undefined,
      valueDate: this.editForm.get(['valueDate'])!.value,
      valueDateInUTC: this.editForm.get(['valueDateInUTC'])!.value
        ? dayjs(this.editForm.get(['valueDateInUTC'])!.value, DATE_TIME_FORMAT)
        : undefined,
      valueDateInLocal: this.editForm.get(['valueDateInLocal'])!.value
        ? dayjs(this.editForm.get(['valueDateInLocal'])!.value, DATE_TIME_FORMAT)
        : undefined,
      valueDateForSettlement: this.editForm.get(['valueDateForSettlement'])!.value
        ? dayjs(this.editForm.get(['valueDateForSettlement'])!.value, DATE_TIME_FORMAT)
        : undefined,
      txnAmount: this.editForm.get(['txnAmount'])!.value,
      instructedAmount: this.editForm.get(['instructedAmount'])!.value,
      amountInAccountCurrency: this.editForm.get(['amountInAccountCurrency'])!.value,
      amountInLocalCurrency: this.editForm.get(['amountInLocalCurrency'])!.value,
      chargeInAccountCurrency: this.editForm.get(['chargeInAccountCurrency'])!.value,
      chargeInLocalCurrency: this.editForm.get(['chargeInLocalCurrency'])!.value,
      fundingSource: this.editForm.get(['fundingSource'])!.value,
      merchantID: this.editForm.get(['merchantID'])!.value,
      merchantInfo: this.editForm.get(['merchantInfo'])!.value,
      bankCode: this.editForm.get(['bankCode'])!.value,
      externalRefNo: this.editForm.get(['externalRefNo'])!.value,
      txnDescription: this.editForm.get(['txnDescription'])!.value,
      txnInternalDescription: this.editForm.get(['txnInternalDescription'])!.value,
      txnAddnlInfo: this.editForm.get(['txnAddnlInfo'])!.value,
      createdOn: this.editForm.get(['createdOn'])!.value ? dayjs(this.editForm.get(['createdOn'])!.value, DATE_TIME_FORMAT) : undefined,
      createdBy: this.editForm.get(['createdBy'])!.value,
      txnStatus: this.editForm.get(['txnStatus'])!.value,
      txnDebitAccountNo: this.editForm.get(['txnDebitAccountNo'])!.value,
      txnCreditAccountNo: this.editForm.get(['txnCreditAccountNo'])!.value,
      chargeDebitAccountNo: this.editForm.get(['chargeDebitAccountNo'])!.value,
      chargeCreditAccountNo: this.editForm.get(['chargeCreditAccountNo'])!.value,
      country: this.editForm.get(['country'])!.value,
      txnCurrency: this.editForm.get(['txnCurrency'])!.value,
      accountCurrency: this.editForm.get(['accountCurrency'])!.value,
      instructedCurrency: this.editForm.get(['instructedCurrency'])!.value,
      purposeOfTxn: this.editForm.get(['purposeOfTxn'])!.value,
      fundingSourceType: this.editForm.get(['fundingSourceType'])!.value,
    };
  }
}
