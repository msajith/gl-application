import { ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { of, Subject, from } from 'rxjs';

import { CustomerService } from '../service/customer.service';
import { ICustomer, Customer } from '../customer.model';
import { ICountry } from 'app/entities/country/country.model';
import { CountryService } from 'app/entities/country/service/country.service';
import { ICustomerType } from 'app/entities/customer-type/customer-type.model';
import { CustomerTypeService } from 'app/entities/customer-type/service/customer-type.service';
import { ICustomerSegment } from 'app/entities/customer-segment/customer-segment.model';
import { CustomerSegmentService } from 'app/entities/customer-segment/service/customer-segment.service';

import { CustomerUpdateComponent } from './customer-update.component';

describe('Customer Management Update Component', () => {
  let comp: CustomerUpdateComponent;
  let fixture: ComponentFixture<CustomerUpdateComponent>;
  let activatedRoute: ActivatedRoute;
  let customerService: CustomerService;
  let countryService: CountryService;
  let customerTypeService: CustomerTypeService;
  let customerSegmentService: CustomerSegmentService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule, RouterTestingModule.withRoutes([])],
      declarations: [CustomerUpdateComponent],
      providers: [
        FormBuilder,
        {
          provide: ActivatedRoute,
          useValue: {
            params: from([{}]),
          },
        },
      ],
    })
      .overrideTemplate(CustomerUpdateComponent, '')
      .compileComponents();

    fixture = TestBed.createComponent(CustomerUpdateComponent);
    activatedRoute = TestBed.inject(ActivatedRoute);
    customerService = TestBed.inject(CustomerService);
    countryService = TestBed.inject(CountryService);
    customerTypeService = TestBed.inject(CustomerTypeService);
    customerSegmentService = TestBed.inject(CustomerSegmentService);

    comp = fixture.componentInstance;
  });

  describe('ngOnInit', () => {
    it('Should call Country query and add missing value', () => {
      const customer: ICustomer = { id: 456 };
      const nationality: ICountry = { id: 60929 };
      customer.nationality = nationality;

      const countryCollection: ICountry[] = [{ id: 74934 }];
      jest.spyOn(countryService, 'query').mockReturnValue(of(new HttpResponse({ body: countryCollection })));
      const additionalCountries = [nationality];
      const expectedCollection: ICountry[] = [...additionalCountries, ...countryCollection];
      jest.spyOn(countryService, 'addCountryToCollectionIfMissing').mockReturnValue(expectedCollection);

      activatedRoute.data = of({ customer });
      comp.ngOnInit();

      expect(countryService.query).toHaveBeenCalled();
      expect(countryService.addCountryToCollectionIfMissing).toHaveBeenCalledWith(countryCollection, ...additionalCountries);
      expect(comp.countriesSharedCollection).toEqual(expectedCollection);
    });

    it('Should call CustomerType query and add missing value', () => {
      const customer: ICustomer = { id: 456 };
      const customerType: ICustomerType = { id: 24699 };
      customer.customerType = customerType;

      const customerTypeCollection: ICustomerType[] = [{ id: 93217 }];
      jest.spyOn(customerTypeService, 'query').mockReturnValue(of(new HttpResponse({ body: customerTypeCollection })));
      const additionalCustomerTypes = [customerType];
      const expectedCollection: ICustomerType[] = [...additionalCustomerTypes, ...customerTypeCollection];
      jest.spyOn(customerTypeService, 'addCustomerTypeToCollectionIfMissing').mockReturnValue(expectedCollection);

      activatedRoute.data = of({ customer });
      comp.ngOnInit();

      expect(customerTypeService.query).toHaveBeenCalled();
      expect(customerTypeService.addCustomerTypeToCollectionIfMissing).toHaveBeenCalledWith(
        customerTypeCollection,
        ...additionalCustomerTypes
      );
      expect(comp.customerTypesSharedCollection).toEqual(expectedCollection);
    });

    it('Should call CustomerSegment query and add missing value', () => {
      const customer: ICustomer = { id: 456 };
      const customerSegment: ICustomerSegment = { id: 30302 };
      customer.customerSegment = customerSegment;

      const customerSegmentCollection: ICustomerSegment[] = [{ id: 39439 }];
      jest.spyOn(customerSegmentService, 'query').mockReturnValue(of(new HttpResponse({ body: customerSegmentCollection })));
      const additionalCustomerSegments = [customerSegment];
      const expectedCollection: ICustomerSegment[] = [...additionalCustomerSegments, ...customerSegmentCollection];
      jest.spyOn(customerSegmentService, 'addCustomerSegmentToCollectionIfMissing').mockReturnValue(expectedCollection);

      activatedRoute.data = of({ customer });
      comp.ngOnInit();

      expect(customerSegmentService.query).toHaveBeenCalled();
      expect(customerSegmentService.addCustomerSegmentToCollectionIfMissing).toHaveBeenCalledWith(
        customerSegmentCollection,
        ...additionalCustomerSegments
      );
      expect(comp.customerSegmentsSharedCollection).toEqual(expectedCollection);
    });

    it('Should update editForm', () => {
      const customer: ICustomer = { id: 456 };
      const nationality: ICountry = { id: 61824 };
      customer.nationality = nationality;
      const customerType: ICustomerType = { id: 98473 };
      customer.customerType = customerType;
      const customerSegment: ICustomerSegment = { id: 38073 };
      customer.customerSegment = customerSegment;

      activatedRoute.data = of({ customer });
      comp.ngOnInit();

      expect(comp.editForm.value).toEqual(expect.objectContaining(customer));
      expect(comp.countriesSharedCollection).toContain(nationality);
      expect(comp.customerTypesSharedCollection).toContain(customerType);
      expect(comp.customerSegmentsSharedCollection).toContain(customerSegment);
    });
  });

  describe('save', () => {
    it('Should call update service on save for existing entity', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<Customer>>();
      const customer = { id: 123 };
      jest.spyOn(customerService, 'update').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ customer });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.next(new HttpResponse({ body: customer }));
      saveSubject.complete();

      // THEN
      expect(comp.previousState).toHaveBeenCalled();
      expect(customerService.update).toHaveBeenCalledWith(customer);
      expect(comp.isSaving).toEqual(false);
    });

    it('Should call create service on save for new entity', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<Customer>>();
      const customer = new Customer();
      jest.spyOn(customerService, 'create').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ customer });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.next(new HttpResponse({ body: customer }));
      saveSubject.complete();

      // THEN
      expect(customerService.create).toHaveBeenCalledWith(customer);
      expect(comp.isSaving).toEqual(false);
      expect(comp.previousState).toHaveBeenCalled();
    });

    it('Should set isSaving to false on error', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<Customer>>();
      const customer = { id: 123 };
      jest.spyOn(customerService, 'update').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ customer });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.error('This is an error!');

      // THEN
      expect(customerService.update).toHaveBeenCalledWith(customer);
      expect(comp.isSaving).toEqual(false);
      expect(comp.previousState).not.toHaveBeenCalled();
    });
  });

  describe('Tracking relationships identifiers', () => {
    describe('trackCountryById', () => {
      it('Should return tracked Country primary key', () => {
        const entity = { id: 123 };
        const trackResult = comp.trackCountryById(0, entity);
        expect(trackResult).toEqual(entity.id);
      });
    });

    describe('trackCustomerTypeById', () => {
      it('Should return tracked CustomerType primary key', () => {
        const entity = { id: 123 };
        const trackResult = comp.trackCustomerTypeById(0, entity);
        expect(trackResult).toEqual(entity.id);
      });
    });

    describe('trackCustomerSegmentById', () => {
      it('Should return tracked CustomerSegment primary key', () => {
        const entity = { id: 123 };
        const trackResult = comp.trackCustomerSegmentById(0, entity);
        expect(trackResult).toEqual(entity.id);
      });
    });
  });
});
