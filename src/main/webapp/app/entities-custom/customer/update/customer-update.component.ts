import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { finalize, map } from 'rxjs/operators';

import dayjs from 'dayjs/esm';
import { DATE_TIME_FORMAT } from 'app/config/input.constants';

import { ICustomer, Customer } from '../customer.model';
import { CustomerService } from '../service/customer.service';
import { ICountry } from 'app/entities/country/country.model';
import { CountryService } from 'app/entities/country/service/country.service';
import { ICustomerType } from 'app/entities/customer-type/customer-type.model';
import { CustomerTypeService } from 'app/entities/customer-type/service/customer-type.service';
import { ICustomerSegment } from 'app/entities/customer-segment/customer-segment.model';
import { CustomerSegmentService } from 'app/entities/customer-segment/service/customer-segment.service';
import { CustomerStatus } from 'app/entities/enumerations/customer-status.model';

@Component({
  selector: 'jhi-customer-update',
  templateUrl: './customer-update.component.html',
})
export class CustomerUpdateComponent implements OnInit {
  isSaving = false;
  customerStatusValues = Object.keys(CustomerStatus);

  countriesSharedCollection: ICountry[] = [];
  customerTypesSharedCollection: ICustomerType[] = [];
  customerSegmentsSharedCollection: ICustomerSegment[] = [];

  editForm = this.fb.group({
    id: [],
    cif: [null, [Validators.required]],
    firstName: [],
    middleName: [],
    lastName: [null, [Validators.required]],
    nameInLocalLang: [],
    telephone: [],
    mobile: [],
    email: [],
    createdOn: [],
    createdBy: [],
    customerStatus: [],
    nationality: [],
    customerType: [],
    customerSegment: [],
  });

  constructor(
    protected customerService: CustomerService,
    protected countryService: CountryService,
    protected customerTypeService: CustomerTypeService,
    protected customerSegmentService: CustomerSegmentService,
    protected activatedRoute: ActivatedRoute,
    protected fb: FormBuilder
  ) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ customer }) => {
      if (customer.id === undefined) {
        const today = dayjs().startOf('day');
        customer.createdOn = today;
      }

      this.updateForm(customer);

      this.loadRelationshipsOptions();
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const customer = this.createFromForm();
    if (customer.id !== undefined) {
      this.subscribeToSaveResponse(this.customerService.update(customer));
    } else {
      this.subscribeToSaveResponse(this.customerService.create(customer));
    }
  }

  trackCountryById(index: number, item: ICountry): number {
    return item.id!;
  }

  trackCustomerTypeById(index: number, item: ICustomerType): number {
    return item.id!;
  }

  trackCustomerSegmentById(index: number, item: ICustomerSegment): number {
    return item.id!;
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<ICustomer>>): void {
    result.pipe(finalize(() => this.onSaveFinalize())).subscribe({
      next: () => this.onSaveSuccess(),
      error: () => this.onSaveError(),
    });
  }

  protected onSaveSuccess(): void {
    this.previousState();
  }

  protected onSaveError(): void {
    // Api for inheritance.
  }

  protected onSaveFinalize(): void {
    this.isSaving = false;
  }

  protected updateForm(customer: ICustomer): void {
    this.editForm.patchValue({
      id: customer.id,
      cif: customer.cif,
      firstName: customer.firstName,
      middleName: customer.middleName,
      lastName: customer.lastName,
      nameInLocalLang: customer.nameInLocalLang,
      telephone: customer.telephone,
      mobile: customer.mobile,
      email: customer.email,
      createdOn: customer.createdOn ? customer.createdOn.format(DATE_TIME_FORMAT) : null,
      createdBy: customer.createdBy,
      customerStatus: customer.customerStatus,
      nationality: customer.nationality,
      customerType: customer.customerType,
      customerSegment: customer.customerSegment,
    });

    this.countriesSharedCollection = this.countryService.addCountryToCollectionIfMissing(
      this.countriesSharedCollection,
      customer.nationality
    );
    this.customerTypesSharedCollection = this.customerTypeService.addCustomerTypeToCollectionIfMissing(
      this.customerTypesSharedCollection,
      customer.customerType
    );
    this.customerSegmentsSharedCollection = this.customerSegmentService.addCustomerSegmentToCollectionIfMissing(
      this.customerSegmentsSharedCollection,
      customer.customerSegment
    );
  }

  protected loadRelationshipsOptions(): void {
    this.countryService
      .query()
      .pipe(map((res: HttpResponse<ICountry[]>) => res.body ?? []))
      .pipe(
        map((countries: ICountry[]) =>
          this.countryService.addCountryToCollectionIfMissing(countries, this.editForm.get('nationality')!.value)
        )
      )
      .subscribe((countries: ICountry[]) => (this.countriesSharedCollection = countries));

    this.customerTypeService
      .query()
      .pipe(map((res: HttpResponse<ICustomerType[]>) => res.body ?? []))
      .pipe(
        map((customerTypes: ICustomerType[]) =>
          this.customerTypeService.addCustomerTypeToCollectionIfMissing(customerTypes, this.editForm.get('customerType')!.value)
        )
      )
      .subscribe((customerTypes: ICustomerType[]) => (this.customerTypesSharedCollection = customerTypes));

    this.customerSegmentService
      .query()
      .pipe(map((res: HttpResponse<ICustomerSegment[]>) => res.body ?? []))
      .pipe(
        map((customerSegments: ICustomerSegment[]) =>
          this.customerSegmentService.addCustomerSegmentToCollectionIfMissing(customerSegments, this.editForm.get('customerSegment')!.value)
        )
      )
      .subscribe((customerSegments: ICustomerSegment[]) => (this.customerSegmentsSharedCollection = customerSegments));
  }

  protected createFromForm(): ICustomer {
    return {
      ...new Customer(),
      id: this.editForm.get(['id'])!.value,
      cif: this.editForm.get(['cif'])!.value,
      firstName: this.editForm.get(['firstName'])!.value,
      middleName: this.editForm.get(['middleName'])!.value,
      lastName: this.editForm.get(['lastName'])!.value,
      nameInLocalLang: this.editForm.get(['nameInLocalLang'])!.value,
      telephone: this.editForm.get(['telephone'])!.value,
      mobile: this.editForm.get(['mobile'])!.value,
      email: this.editForm.get(['email'])!.value,
      createdOn: this.editForm.get(['createdOn'])!.value ? dayjs(this.editForm.get(['createdOn'])!.value, DATE_TIME_FORMAT) : undefined,
      createdBy: this.editForm.get(['createdBy'])!.value,
      customerStatus: this.editForm.get(['customerStatus'])!.value,
      nationality: this.editForm.get(['nationality'])!.value,
      customerType: this.editForm.get(['customerType'])!.value,
      customerSegment: this.editForm.get(['customerSegment'])!.value,
    };
  }
}
