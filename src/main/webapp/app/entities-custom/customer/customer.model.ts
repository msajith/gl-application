import dayjs from 'dayjs/esm';
import { ICustomerAddress } from 'app/entities/customer-address/customer-address.model';
import { IWalletAccount } from 'app/entities/wallet-account/wallet-account.model';
import { ICasaAccount } from 'app/entities/casa-account/casa-account.model';
import { ICountry } from 'app/entities/country/country.model';
import { ICustomerType } from 'app/entities/customer-type/customer-type.model';
import { ICustomerSegment } from 'app/entities/customer-segment/customer-segment.model';
import { CustomerStatus } from 'app/entities/enumerations/customer-status.model';

export interface ICustomer {
  id?: number;
  cif?: string;
  firstName?: string | null;
  middleName?: string | null;
  lastName?: string;
  nameInLocalLang?: string | null;
  telephone?: string | null;
  mobile?: string | null;
  email?: string | null;
  createdOn?: dayjs.Dayjs | null;
  createdBy?: string | null;
  customerStatus?: CustomerStatus | null;
  addresses?: ICustomerAddress[] | null;
  walletAccounts?: IWalletAccount[] | null;
  casaAccounts?: ICasaAccount[] | null;
  nationality?: ICountry | null;
  customerType?: ICustomerType | null;
  customerSegment?: ICustomerSegment | null;
}

export class Customer implements ICustomer {
  constructor(
    public id?: number,
    public cif?: string,
    public firstName?: string | null,
    public middleName?: string | null,
    public lastName?: string,
    public nameInLocalLang?: string | null,
    public telephone?: string | null,
    public mobile?: string | null,
    public email?: string | null,
    public createdOn?: dayjs.Dayjs | null,
    public createdBy?: string | null,
    public customerStatus?: CustomerStatus | null,
    public addresses?: ICustomerAddress[] | null,
    public walletAccounts?: IWalletAccount[] | null,
    public casaAccounts?: ICasaAccount[] | null,
    public nationality?: ICountry | null,
    public customerType?: ICustomerType | null,
    public customerSegment?: ICustomerSegment | null
  ) {}
}

export function getCustomerIdentifier(customer: ICustomer): number | undefined {
  return customer.id;
}
