import { TestBed } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { ActivatedRouteSnapshot, ActivatedRoute, Router, convertToParamMap } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { of } from 'rxjs';

import { ICustomerSegment, CustomerSegment } from '../customer-segment.model';
import { CustomerSegmentService } from '../service/customer-segment.service';

import { CustomerSegmentRoutingResolveService } from './customer-segment-routing-resolve.service';

describe('CustomerSegment routing resolve service', () => {
  let mockRouter: Router;
  let mockActivatedRouteSnapshot: ActivatedRouteSnapshot;
  let routingResolveService: CustomerSegmentRoutingResolveService;
  let service: CustomerSegmentService;
  let resultCustomerSegment: ICustomerSegment | undefined;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule, RouterTestingModule.withRoutes([])],
      providers: [
        {
          provide: ActivatedRoute,
          useValue: {
            snapshot: {
              paramMap: convertToParamMap({}),
            },
          },
        },
      ],
    });
    mockRouter = TestBed.inject(Router);
    jest.spyOn(mockRouter, 'navigate').mockImplementation(() => Promise.resolve(true));
    mockActivatedRouteSnapshot = TestBed.inject(ActivatedRoute).snapshot;
    routingResolveService = TestBed.inject(CustomerSegmentRoutingResolveService);
    service = TestBed.inject(CustomerSegmentService);
    resultCustomerSegment = undefined;
  });

  describe('resolve', () => {
    it('should return ICustomerSegment returned by find', () => {
      // GIVEN
      service.find = jest.fn(id => of(new HttpResponse({ body: { id } })));
      mockActivatedRouteSnapshot.params = { id: 123 };

      // WHEN
      routingResolveService.resolve(mockActivatedRouteSnapshot).subscribe(result => {
        resultCustomerSegment = result;
      });

      // THEN
      expect(service.find).toBeCalledWith(123);
      expect(resultCustomerSegment).toEqual({ id: 123 });
    });

    it('should return new ICustomerSegment if id is not provided', () => {
      // GIVEN
      service.find = jest.fn();
      mockActivatedRouteSnapshot.params = {};

      // WHEN
      routingResolveService.resolve(mockActivatedRouteSnapshot).subscribe(result => {
        resultCustomerSegment = result;
      });

      // THEN
      expect(service.find).not.toBeCalled();
      expect(resultCustomerSegment).toEqual(new CustomerSegment());
    });

    it('should route to 404 page if data not found in server', () => {
      // GIVEN
      jest.spyOn(service, 'find').mockReturnValue(of(new HttpResponse({ body: null as unknown as CustomerSegment })));
      mockActivatedRouteSnapshot.params = { id: 123 };

      // WHEN
      routingResolveService.resolve(mockActivatedRouteSnapshot).subscribe(result => {
        resultCustomerSegment = result;
      });

      // THEN
      expect(service.find).toBeCalledWith(123);
      expect(resultCustomerSegment).toEqual(undefined);
      expect(mockRouter.navigate).toHaveBeenCalledWith(['404']);
    });
  });
});
