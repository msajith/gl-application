import { Component } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

import { ICustomerSegment } from '../customer-segment.model';
import { CustomerSegmentService } from '../service/customer-segment.service';

@Component({
  templateUrl: './customer-segment-delete-dialog.component.html',
})
export class CustomerSegmentDeleteDialogComponent {
  customerSegment?: ICustomerSegment;

  constructor(protected customerSegmentService: CustomerSegmentService, protected activeModal: NgbActiveModal) {}

  cancel(): void {
    this.activeModal.dismiss();
  }

  confirmDelete(id: number): void {
    this.customerSegmentService.delete(id).subscribe(() => {
      this.activeModal.close('deleted');
    });
  }
}
