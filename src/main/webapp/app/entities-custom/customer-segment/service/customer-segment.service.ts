import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { isPresent } from 'app/core/util/operators';
import { ApplicationConfigService } from 'app/core/config/application-config.service';
import { createRequestOption } from 'app/core/request/request-util';
import { ICustomerSegment, getCustomerSegmentIdentifier } from '../customer-segment.model';

export type EntityResponseType = HttpResponse<ICustomerSegment>;
export type EntityArrayResponseType = HttpResponse<ICustomerSegment[]>;

@Injectable({ providedIn: 'root' })
export class CustomerSegmentService {
  protected resourceUrl = this.applicationConfigService.getEndpointFor('api/customer-segments');

  constructor(protected http: HttpClient, protected applicationConfigService: ApplicationConfigService) {}

  create(customerSegment: ICustomerSegment): Observable<EntityResponseType> {
    return this.http.post<ICustomerSegment>(this.resourceUrl, customerSegment, { observe: 'response' });
  }

  update(customerSegment: ICustomerSegment): Observable<EntityResponseType> {
    return this.http.put<ICustomerSegment>(
      `${this.resourceUrl}/${getCustomerSegmentIdentifier(customerSegment) as number}`,
      customerSegment,
      { observe: 'response' }
    );
  }

  partialUpdate(customerSegment: ICustomerSegment): Observable<EntityResponseType> {
    return this.http.patch<ICustomerSegment>(
      `${this.resourceUrl}/${getCustomerSegmentIdentifier(customerSegment) as number}`,
      customerSegment,
      { observe: 'response' }
    );
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http.get<ICustomerSegment>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<ICustomerSegment[]>(this.resourceUrl, { params: options, observe: 'response' });
  }

  delete(id: number): Observable<HttpResponse<{}>> {
    return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  addCustomerSegmentToCollectionIfMissing(
    customerSegmentCollection: ICustomerSegment[],
    ...customerSegmentsToCheck: (ICustomerSegment | null | undefined)[]
  ): ICustomerSegment[] {
    const customerSegments: ICustomerSegment[] = customerSegmentsToCheck.filter(isPresent);
    if (customerSegments.length > 0) {
      const customerSegmentCollectionIdentifiers = customerSegmentCollection.map(
        customerSegmentItem => getCustomerSegmentIdentifier(customerSegmentItem)!
      );
      const customerSegmentsToAdd = customerSegments.filter(customerSegmentItem => {
        const customerSegmentIdentifier = getCustomerSegmentIdentifier(customerSegmentItem);
        if (customerSegmentIdentifier == null || customerSegmentCollectionIdentifiers.includes(customerSegmentIdentifier)) {
          return false;
        }
        customerSegmentCollectionIdentifiers.push(customerSegmentIdentifier);
        return true;
      });
      return [...customerSegmentsToAdd, ...customerSegmentCollection];
    }
    return customerSegmentCollection;
  }
}
