import { ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { of, Subject, from } from 'rxjs';

import { CustomerSegmentService } from '../service/customer-segment.service';
import { ICustomerSegment, CustomerSegment } from '../customer-segment.model';

import { CustomerSegmentUpdateComponent } from './customer-segment-update.component';

describe('CustomerSegment Management Update Component', () => {
  let comp: CustomerSegmentUpdateComponent;
  let fixture: ComponentFixture<CustomerSegmentUpdateComponent>;
  let activatedRoute: ActivatedRoute;
  let customerSegmentService: CustomerSegmentService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule, RouterTestingModule.withRoutes([])],
      declarations: [CustomerSegmentUpdateComponent],
      providers: [
        FormBuilder,
        {
          provide: ActivatedRoute,
          useValue: {
            params: from([{}]),
          },
        },
      ],
    })
      .overrideTemplate(CustomerSegmentUpdateComponent, '')
      .compileComponents();

    fixture = TestBed.createComponent(CustomerSegmentUpdateComponent);
    activatedRoute = TestBed.inject(ActivatedRoute);
    customerSegmentService = TestBed.inject(CustomerSegmentService);

    comp = fixture.componentInstance;
  });

  describe('ngOnInit', () => {
    it('Should update editForm', () => {
      const customerSegment: ICustomerSegment = { id: 456 };

      activatedRoute.data = of({ customerSegment });
      comp.ngOnInit();

      expect(comp.editForm.value).toEqual(expect.objectContaining(customerSegment));
    });
  });

  describe('save', () => {
    it('Should call update service on save for existing entity', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<CustomerSegment>>();
      const customerSegment = { id: 123 };
      jest.spyOn(customerSegmentService, 'update').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ customerSegment });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.next(new HttpResponse({ body: customerSegment }));
      saveSubject.complete();

      // THEN
      expect(comp.previousState).toHaveBeenCalled();
      expect(customerSegmentService.update).toHaveBeenCalledWith(customerSegment);
      expect(comp.isSaving).toEqual(false);
    });

    it('Should call create service on save for new entity', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<CustomerSegment>>();
      const customerSegment = new CustomerSegment();
      jest.spyOn(customerSegmentService, 'create').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ customerSegment });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.next(new HttpResponse({ body: customerSegment }));
      saveSubject.complete();

      // THEN
      expect(customerSegmentService.create).toHaveBeenCalledWith(customerSegment);
      expect(comp.isSaving).toEqual(false);
      expect(comp.previousState).toHaveBeenCalled();
    });

    it('Should set isSaving to false on error', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<CustomerSegment>>();
      const customerSegment = { id: 123 };
      jest.spyOn(customerSegmentService, 'update').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ customerSegment });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.error('This is an error!');

      // THEN
      expect(customerSegmentService.update).toHaveBeenCalledWith(customerSegment);
      expect(comp.isSaving).toEqual(false);
      expect(comp.previousState).not.toHaveBeenCalled();
    });
  });
});
