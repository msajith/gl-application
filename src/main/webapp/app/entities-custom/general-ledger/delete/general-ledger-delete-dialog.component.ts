import { Component } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

import { IGeneralLedger } from '../general-ledger.model';
import { GeneralLedgerService } from '../service/general-ledger.service';

@Component({
  templateUrl: './general-ledger-delete-dialog.component.html',
})
export class GeneralLedgerDeleteDialogComponent {
  generalLedger?: IGeneralLedger;

  constructor(protected generalLedgerService: GeneralLedgerService, protected activeModal: NgbActiveModal) {}

  cancel(): void {
    this.activeModal.dismiss();
  }

  confirmDelete(id: number): void {
    this.generalLedgerService.delete(id).subscribe(() => {
      this.activeModal.close('deleted');
    });
  }
}
