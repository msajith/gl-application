import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import dayjs from 'dayjs/esm';

import { isPresent } from 'app/core/util/operators';
import { DATE_FORMAT } from 'app/config/input.constants';
import { ApplicationConfigService } from 'app/core/config/application-config.service';
import { createRequestOption } from 'app/core/request/request-util';
import { IGeneralLedger, getGeneralLedgerIdentifier } from '../general-ledger.model';

export type EntityResponseType = HttpResponse<IGeneralLedger>;
export type EntityArrayResponseType = HttpResponse<IGeneralLedger[]>;

@Injectable({ providedIn: 'root' })
export class GeneralLedgerService {
  protected resourceUrl = this.applicationConfigService.getEndpointFor('api/general-ledgers');

  constructor(protected http: HttpClient, protected applicationConfigService: ApplicationConfigService) {}

  create(generalLedger: IGeneralLedger): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(generalLedger);
    return this.http
      .post<IGeneralLedger>(this.resourceUrl, copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  update(generalLedger: IGeneralLedger): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(generalLedger);
    return this.http
      .put<IGeneralLedger>(`${this.resourceUrl}/${getGeneralLedgerIdentifier(generalLedger) as number}`, copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  partialUpdate(generalLedger: IGeneralLedger): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(generalLedger);
    return this.http
      .patch<IGeneralLedger>(`${this.resourceUrl}/${getGeneralLedgerIdentifier(generalLedger) as number}`, copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http
      .get<IGeneralLedger>(`${this.resourceUrl}/${id}`, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http
      .get<IGeneralLedger[]>(this.resourceUrl, { params: options, observe: 'response' })
      .pipe(map((res: EntityArrayResponseType) => this.convertDateArrayFromServer(res)));
  }

  delete(id: number): Observable<HttpResponse<{}>> {
    return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  addGeneralLedgerToCollectionIfMissing(
    generalLedgerCollection: IGeneralLedger[],
    ...generalLedgersToCheck: (IGeneralLedger | null | undefined)[]
  ): IGeneralLedger[] {
    const generalLedgers: IGeneralLedger[] = generalLedgersToCheck.filter(isPresent);
    if (generalLedgers.length > 0) {
      const generalLedgerCollectionIdentifiers = generalLedgerCollection.map(
        generalLedgerItem => getGeneralLedgerIdentifier(generalLedgerItem)!
      );
      const generalLedgersToAdd = generalLedgers.filter(generalLedgerItem => {
        const generalLedgerIdentifier = getGeneralLedgerIdentifier(generalLedgerItem);
        if (generalLedgerIdentifier == null || generalLedgerCollectionIdentifiers.includes(generalLedgerIdentifier)) {
          return false;
        }
        generalLedgerCollectionIdentifiers.push(generalLedgerIdentifier);
        return true;
      });
      return [...generalLedgersToAdd, ...generalLedgerCollection];
    }
    return generalLedgerCollection;
  }

  protected convertDateFromClient(generalLedger: IGeneralLedger): IGeneralLedger {
    return Object.assign({}, generalLedger, {
      txnDate: generalLedger.txnDate?.isValid() ? generalLedger.txnDate.format(DATE_FORMAT) : undefined,
      txnDateInUTC: generalLedger.txnDateInUTC?.isValid() ? generalLedger.txnDateInUTC.toJSON() : undefined,
      txnDateInLocal: generalLedger.txnDateInLocal?.isValid() ? generalLedger.txnDateInLocal.toJSON() : undefined,
      txnDateForSettlement: generalLedger.txnDateForSettlement?.isValid() ? generalLedger.txnDateForSettlement.toJSON() : undefined,
      createdOn: generalLedger.createdOn?.isValid() ? generalLedger.createdOn.toJSON() : undefined,
    });
  }

  protected convertDateFromServer(res: EntityResponseType): EntityResponseType {
    if (res.body) {
      res.body.txnDate = res.body.txnDate ? dayjs(res.body.txnDate) : undefined;
      res.body.txnDateInUTC = res.body.txnDateInUTC ? dayjs(res.body.txnDateInUTC) : undefined;
      res.body.txnDateInLocal = res.body.txnDateInLocal ? dayjs(res.body.txnDateInLocal) : undefined;
      res.body.txnDateForSettlement = res.body.txnDateForSettlement ? dayjs(res.body.txnDateForSettlement) : undefined;
      res.body.createdOn = res.body.createdOn ? dayjs(res.body.createdOn) : undefined;
    }
    return res;
  }

  protected convertDateArrayFromServer(res: EntityArrayResponseType): EntityArrayResponseType {
    if (res.body) {
      res.body.forEach((generalLedger: IGeneralLedger) => {
        generalLedger.txnDate = generalLedger.txnDate ? dayjs(generalLedger.txnDate) : undefined;
        generalLedger.txnDateInUTC = generalLedger.txnDateInUTC ? dayjs(generalLedger.txnDateInUTC) : undefined;
        generalLedger.txnDateInLocal = generalLedger.txnDateInLocal ? dayjs(generalLedger.txnDateInLocal) : undefined;
        generalLedger.txnDateForSettlement = generalLedger.txnDateForSettlement ? dayjs(generalLedger.txnDateForSettlement) : undefined;
        generalLedger.createdOn = generalLedger.createdOn ? dayjs(generalLedger.createdOn) : undefined;
      });
    }
    return res;
  }
}
