import { TestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import dayjs from 'dayjs/esm';

import { DATE_FORMAT, DATE_TIME_FORMAT } from 'app/config/input.constants';
import { EntryStatus } from 'app/entities/enumerations/entry-status.model';
import { GlEntryType } from 'app/entities/enumerations/gl-entry-type.model';
import { IGeneralLedger, GeneralLedger } from '../general-ledger.model';

import { GeneralLedgerService } from './general-ledger.service';

describe('GeneralLedger Service', () => {
  let service: GeneralLedgerService;
  let httpMock: HttpTestingController;
  let elemDefault: IGeneralLedger;
  let expectedResult: IGeneralLedger | IGeneralLedger[] | boolean | null;
  let currentDate: dayjs.Dayjs;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
    });
    expectedResult = null;
    service = TestBed.inject(GeneralLedgerService);
    httpMock = TestBed.inject(HttpTestingController);
    currentDate = dayjs();

    elemDefault = {
      id: 0,
      glRefNo: 'AAAAAAA',
      glAccountNo: 'AAAAAAA',
      txnDate: currentDate,
      txnDateInUTC: currentDate,
      txnDateInLocal: currentDate,
      txnDateForSettlement: currentDate,
      amountInAccountCurrency: 0,
      amountInLocalCurrency: 0,
      rate: 0,
      entryCategory: 'AAAAAAA',
      description1: 'AAAAAAA',
      description2: 'AAAAAAA',
      createdOn: currentDate,
      createdBy: 'AAAAAAA',
      entryStatus: EntryStatus.ACTIVE,
      glEntryType: GlEntryType.DR,
    };
  });

  describe('Service methods', () => {
    it('should find an element', () => {
      const returnedFromService = Object.assign(
        {
          txnDate: currentDate.format(DATE_FORMAT),
          txnDateInUTC: currentDate.format(DATE_TIME_FORMAT),
          txnDateInLocal: currentDate.format(DATE_TIME_FORMAT),
          txnDateForSettlement: currentDate.format(DATE_TIME_FORMAT),
          createdOn: currentDate.format(DATE_TIME_FORMAT),
        },
        elemDefault
      );

      service.find(123).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'GET' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(elemDefault);
    });

    it('should create a GeneralLedger', () => {
      const returnedFromService = Object.assign(
        {
          id: 0,
          txnDate: currentDate.format(DATE_FORMAT),
          txnDateInUTC: currentDate.format(DATE_TIME_FORMAT),
          txnDateInLocal: currentDate.format(DATE_TIME_FORMAT),
          txnDateForSettlement: currentDate.format(DATE_TIME_FORMAT),
          createdOn: currentDate.format(DATE_TIME_FORMAT),
        },
        elemDefault
      );

      const expected = Object.assign(
        {
          txnDate: currentDate,
          txnDateInUTC: currentDate,
          txnDateInLocal: currentDate,
          txnDateForSettlement: currentDate,
          createdOn: currentDate,
        },
        returnedFromService
      );

      service.create(new GeneralLedger()).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'POST' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(expected);
    });

    it('should update a GeneralLedger', () => {
      const returnedFromService = Object.assign(
        {
          id: 1,
          glRefNo: 'BBBBBB',
          glAccountNo: 'BBBBBB',
          txnDate: currentDate.format(DATE_FORMAT),
          txnDateInUTC: currentDate.format(DATE_TIME_FORMAT),
          txnDateInLocal: currentDate.format(DATE_TIME_FORMAT),
          txnDateForSettlement: currentDate.format(DATE_TIME_FORMAT),
          amountInAccountCurrency: 1,
          amountInLocalCurrency: 1,
          rate: 1,
          entryCategory: 'BBBBBB',
          description1: 'BBBBBB',
          description2: 'BBBBBB',
          createdOn: currentDate.format(DATE_TIME_FORMAT),
          createdBy: 'BBBBBB',
          entryStatus: 'BBBBBB',
          glEntryType: 'BBBBBB',
        },
        elemDefault
      );

      const expected = Object.assign(
        {
          txnDate: currentDate,
          txnDateInUTC: currentDate,
          txnDateInLocal: currentDate,
          txnDateForSettlement: currentDate,
          createdOn: currentDate,
        },
        returnedFromService
      );

      service.update(expected).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'PUT' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(expected);
    });

    it('should partial update a GeneralLedger', () => {
      const patchObject = Object.assign(
        {
          glRefNo: 'BBBBBB',
          glAccountNo: 'BBBBBB',
          txnDate: currentDate.format(DATE_FORMAT),
          txnDateInUTC: currentDate.format(DATE_TIME_FORMAT),
          txnDateInLocal: currentDate.format(DATE_TIME_FORMAT),
          txnDateForSettlement: currentDate.format(DATE_TIME_FORMAT),
          amountInAccountCurrency: 1,
          amountInLocalCurrency: 1,
          description1: 'BBBBBB',
          description2: 'BBBBBB',
        },
        new GeneralLedger()
      );

      const returnedFromService = Object.assign(patchObject, elemDefault);

      const expected = Object.assign(
        {
          txnDate: currentDate,
          txnDateInUTC: currentDate,
          txnDateInLocal: currentDate,
          txnDateForSettlement: currentDate,
          createdOn: currentDate,
        },
        returnedFromService
      );

      service.partialUpdate(patchObject).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'PATCH' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(expected);
    });

    it('should return a list of GeneralLedger', () => {
      const returnedFromService = Object.assign(
        {
          id: 1,
          glRefNo: 'BBBBBB',
          glAccountNo: 'BBBBBB',
          txnDate: currentDate.format(DATE_FORMAT),
          txnDateInUTC: currentDate.format(DATE_TIME_FORMAT),
          txnDateInLocal: currentDate.format(DATE_TIME_FORMAT),
          txnDateForSettlement: currentDate.format(DATE_TIME_FORMAT),
          amountInAccountCurrency: 1,
          amountInLocalCurrency: 1,
          rate: 1,
          entryCategory: 'BBBBBB',
          description1: 'BBBBBB',
          description2: 'BBBBBB',
          createdOn: currentDate.format(DATE_TIME_FORMAT),
          createdBy: 'BBBBBB',
          entryStatus: 'BBBBBB',
          glEntryType: 'BBBBBB',
        },
        elemDefault
      );

      const expected = Object.assign(
        {
          txnDate: currentDate,
          txnDateInUTC: currentDate,
          txnDateInLocal: currentDate,
          txnDateForSettlement: currentDate,
          createdOn: currentDate,
        },
        returnedFromService
      );

      service.query().subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'GET' });
      req.flush([returnedFromService]);
      httpMock.verify();
      expect(expectedResult).toContainEqual(expected);
    });

    it('should delete a GeneralLedger', () => {
      service.delete(123).subscribe(resp => (expectedResult = resp.ok));

      const req = httpMock.expectOne({ method: 'DELETE' });
      req.flush({ status: 200 });
      expect(expectedResult);
    });

    describe('addGeneralLedgerToCollectionIfMissing', () => {
      it('should add a GeneralLedger to an empty array', () => {
        const generalLedger: IGeneralLedger = { id: 123 };
        expectedResult = service.addGeneralLedgerToCollectionIfMissing([], generalLedger);
        expect(expectedResult).toHaveLength(1);
        expect(expectedResult).toContain(generalLedger);
      });

      it('should not add a GeneralLedger to an array that contains it', () => {
        const generalLedger: IGeneralLedger = { id: 123 };
        const generalLedgerCollection: IGeneralLedger[] = [
          {
            ...generalLedger,
          },
          { id: 456 },
        ];
        expectedResult = service.addGeneralLedgerToCollectionIfMissing(generalLedgerCollection, generalLedger);
        expect(expectedResult).toHaveLength(2);
      });

      it("should add a GeneralLedger to an array that doesn't contain it", () => {
        const generalLedger: IGeneralLedger = { id: 123 };
        const generalLedgerCollection: IGeneralLedger[] = [{ id: 456 }];
        expectedResult = service.addGeneralLedgerToCollectionIfMissing(generalLedgerCollection, generalLedger);
        expect(expectedResult).toHaveLength(2);
        expect(expectedResult).toContain(generalLedger);
      });

      it('should add only unique GeneralLedger to an array', () => {
        const generalLedgerArray: IGeneralLedger[] = [{ id: 123 }, { id: 456 }, { id: 12437 }];
        const generalLedgerCollection: IGeneralLedger[] = [{ id: 123 }];
        expectedResult = service.addGeneralLedgerToCollectionIfMissing(generalLedgerCollection, ...generalLedgerArray);
        expect(expectedResult).toHaveLength(3);
      });

      it('should accept varargs', () => {
        const generalLedger: IGeneralLedger = { id: 123 };
        const generalLedger2: IGeneralLedger = { id: 456 };
        expectedResult = service.addGeneralLedgerToCollectionIfMissing([], generalLedger, generalLedger2);
        expect(expectedResult).toHaveLength(2);
        expect(expectedResult).toContain(generalLedger);
        expect(expectedResult).toContain(generalLedger2);
      });

      it('should accept null and undefined values', () => {
        const generalLedger: IGeneralLedger = { id: 123 };
        expectedResult = service.addGeneralLedgerToCollectionIfMissing([], null, generalLedger, undefined);
        expect(expectedResult).toHaveLength(1);
        expect(expectedResult).toContain(generalLedger);
      });

      it('should return initial array if no GeneralLedger is added', () => {
        const generalLedgerCollection: IGeneralLedger[] = [{ id: 123 }];
        expectedResult = service.addGeneralLedgerToCollectionIfMissing(generalLedgerCollection, undefined, null);
        expect(expectedResult).toEqual(generalLedgerCollection);
      });
    });
  });

  afterEach(() => {
    httpMock.verify();
  });
});
