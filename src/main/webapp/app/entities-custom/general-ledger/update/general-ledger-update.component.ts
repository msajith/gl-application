import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { finalize } from 'rxjs/operators';

import dayjs from 'dayjs/esm';
import { DATE_TIME_FORMAT } from 'app/config/input.constants';

import { IGeneralLedger, GeneralLedger } from '../general-ledger.model';
import { GeneralLedgerService } from '../service/general-ledger.service';
import { EntryStatus } from 'app/entities/enumerations/entry-status.model';
import { GlEntryType } from 'app/entities/enumerations/gl-entry-type.model';

@Component({
  selector: 'jhi-general-ledger-update',
  templateUrl: './general-ledger-update.component.html',
})
export class GeneralLedgerUpdateComponent implements OnInit {
  isSaving = false;
  entryStatusValues = Object.keys(EntryStatus);
  glEntryTypeValues = Object.keys(GlEntryType);

  editForm = this.fb.group({
    id: [],
    glRefNo: [],
    glAccountNo: [],
    txnDate: [],
    txnDateInUTC: [],
    txnDateInLocal: [],
    txnDateForSettlement: [],
    amountInAccountCurrency: [],
    amountInLocalCurrency: [],
    rate: [],
    entryCategory: [],
    description1: [],
    description2: [],
    createdOn: [],
    createdBy: [],
    entryStatus: [],
    glEntryType: [],
  });

  constructor(protected generalLedgerService: GeneralLedgerService, protected activatedRoute: ActivatedRoute, protected fb: FormBuilder) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ generalLedger }) => {
      if (generalLedger.id === undefined) {
        const today = dayjs().startOf('day');
        generalLedger.txnDateInUTC = today;
        generalLedger.txnDateInLocal = today;
        generalLedger.txnDateForSettlement = today;
        generalLedger.createdOn = today;
      }

      this.updateForm(generalLedger);
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const generalLedger = this.createFromForm();
    if (generalLedger.id !== undefined) {
      this.subscribeToSaveResponse(this.generalLedgerService.update(generalLedger));
    } else {
      this.subscribeToSaveResponse(this.generalLedgerService.create(generalLedger));
    }
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IGeneralLedger>>): void {
    result.pipe(finalize(() => this.onSaveFinalize())).subscribe({
      next: () => this.onSaveSuccess(),
      error: () => this.onSaveError(),
    });
  }

  protected onSaveSuccess(): void {
    this.previousState();
  }

  protected onSaveError(): void {
    // Api for inheritance.
  }

  protected onSaveFinalize(): void {
    this.isSaving = false;
  }

  protected updateForm(generalLedger: IGeneralLedger): void {
    this.editForm.patchValue({
      id: generalLedger.id,
      glRefNo: generalLedger.glRefNo,
      glAccountNo: generalLedger.glAccountNo,
      txnDate: generalLedger.txnDate,
      txnDateInUTC: generalLedger.txnDateInUTC ? generalLedger.txnDateInUTC.format(DATE_TIME_FORMAT) : null,
      txnDateInLocal: generalLedger.txnDateInLocal ? generalLedger.txnDateInLocal.format(DATE_TIME_FORMAT) : null,
      txnDateForSettlement: generalLedger.txnDateForSettlement ? generalLedger.txnDateForSettlement.format(DATE_TIME_FORMAT) : null,
      amountInAccountCurrency: generalLedger.amountInAccountCurrency,
      amountInLocalCurrency: generalLedger.amountInLocalCurrency,
      rate: generalLedger.rate,
      entryCategory: generalLedger.entryCategory,
      description1: generalLedger.description1,
      description2: generalLedger.description2,
      createdOn: generalLedger.createdOn ? generalLedger.createdOn.format(DATE_TIME_FORMAT) : null,
      createdBy: generalLedger.createdBy,
      entryStatus: generalLedger.entryStatus,
      glEntryType: generalLedger.glEntryType,
    });
  }

  protected createFromForm(): IGeneralLedger {
    return {
      ...new GeneralLedger(),
      id: this.editForm.get(['id'])!.value,
      glRefNo: this.editForm.get(['glRefNo'])!.value,
      glAccountNo: this.editForm.get(['glAccountNo'])!.value,
      txnDate: this.editForm.get(['txnDate'])!.value,
      txnDateInUTC: this.editForm.get(['txnDateInUTC'])!.value
        ? dayjs(this.editForm.get(['txnDateInUTC'])!.value, DATE_TIME_FORMAT)
        : undefined,
      txnDateInLocal: this.editForm.get(['txnDateInLocal'])!.value
        ? dayjs(this.editForm.get(['txnDateInLocal'])!.value, DATE_TIME_FORMAT)
        : undefined,
      txnDateForSettlement: this.editForm.get(['txnDateForSettlement'])!.value
        ? dayjs(this.editForm.get(['txnDateForSettlement'])!.value, DATE_TIME_FORMAT)
        : undefined,
      amountInAccountCurrency: this.editForm.get(['amountInAccountCurrency'])!.value,
      amountInLocalCurrency: this.editForm.get(['amountInLocalCurrency'])!.value,
      rate: this.editForm.get(['rate'])!.value,
      entryCategory: this.editForm.get(['entryCategory'])!.value,
      description1: this.editForm.get(['description1'])!.value,
      description2: this.editForm.get(['description2'])!.value,
      createdOn: this.editForm.get(['createdOn'])!.value ? dayjs(this.editForm.get(['createdOn'])!.value, DATE_TIME_FORMAT) : undefined,
      createdBy: this.editForm.get(['createdBy'])!.value,
      entryStatus: this.editForm.get(['entryStatus'])!.value,
      glEntryType: this.editForm.get(['glEntryType'])!.value,
    };
  }
}
