import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

import { IGeneralLedger } from '../general-ledger.model';
import { GeneralLedgerService } from '../service/general-ledger.service';
import { GeneralLedgerDeleteDialogComponent } from '../delete/general-ledger-delete-dialog.component';

@Component({
  selector: 'jhi-general-ledger',
  templateUrl: './general-ledger.component.html',
})
export class GeneralLedgerComponent implements OnInit {
  generalLedgers?: IGeneralLedger[];
  isLoading = false;

  constructor(protected generalLedgerService: GeneralLedgerService, protected modalService: NgbModal) {}

  loadAll(): void {
    this.isLoading = true;

    this.generalLedgerService.query().subscribe({
      next: (res: HttpResponse<IGeneralLedger[]>) => {
        this.isLoading = false;
        this.generalLedgers = res.body ?? [];
      },
      error: () => {
        this.isLoading = false;
      },
    });
  }

  ngOnInit(): void {
    this.loadAll();
  }

  trackId(index: number, item: IGeneralLedger): number {
    return item.id!;
  }

  delete(generalLedger: IGeneralLedger): void {
    const modalRef = this.modalService.open(GeneralLedgerDeleteDialogComponent, { size: 'lg', backdrop: 'static' });
    modalRef.componentInstance.generalLedger = generalLedger;
    // unsubscribe not needed because closed completes on modal close
    modalRef.closed.subscribe(reason => {
      if (reason === 'deleted') {
        this.loadAll();
      }
    });
  }
}
