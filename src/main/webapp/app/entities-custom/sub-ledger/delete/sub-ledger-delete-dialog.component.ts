import { Component } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

import { ISubLedger } from '../sub-ledger.model';
import { SubLedgerService } from '../service/sub-ledger.service';

@Component({
  templateUrl: './sub-ledger-delete-dialog.component.html',
})
export class SubLedgerDeleteDialogComponent {
  subLedger?: ISubLedger;

  constructor(protected subLedgerService: SubLedgerService, protected activeModal: NgbActiveModal) {}

  cancel(): void {
    this.activeModal.dismiss();
  }

  confirmDelete(id: number): void {
    this.subLedgerService.delete(id).subscribe(() => {
      this.activeModal.close('deleted');
    });
  }
}
