import { ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { of, Subject, from } from 'rxjs';

import { SubLedgerService } from '../service/sub-ledger.service';
import { ISubLedger, SubLedger } from '../sub-ledger.model';

import { SubLedgerUpdateComponent } from './sub-ledger-update.component';

describe('SubLedger Management Update Component', () => {
  let comp: SubLedgerUpdateComponent;
  let fixture: ComponentFixture<SubLedgerUpdateComponent>;
  let activatedRoute: ActivatedRoute;
  let subLedgerService: SubLedgerService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule, RouterTestingModule.withRoutes([])],
      declarations: [SubLedgerUpdateComponent],
      providers: [
        FormBuilder,
        {
          provide: ActivatedRoute,
          useValue: {
            params: from([{}]),
          },
        },
      ],
    })
      .overrideTemplate(SubLedgerUpdateComponent, '')
      .compileComponents();

    fixture = TestBed.createComponent(SubLedgerUpdateComponent);
    activatedRoute = TestBed.inject(ActivatedRoute);
    subLedgerService = TestBed.inject(SubLedgerService);

    comp = fixture.componentInstance;
  });

  describe('ngOnInit', () => {
    it('Should update editForm', () => {
      const subLedger: ISubLedger = { id: 456 };

      activatedRoute.data = of({ subLedger });
      comp.ngOnInit();

      expect(comp.editForm.value).toEqual(expect.objectContaining(subLedger));
    });
  });

  describe('save', () => {
    it('Should call update service on save for existing entity', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<SubLedger>>();
      const subLedger = { id: 123 };
      jest.spyOn(subLedgerService, 'update').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ subLedger });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.next(new HttpResponse({ body: subLedger }));
      saveSubject.complete();

      // THEN
      expect(comp.previousState).toHaveBeenCalled();
      expect(subLedgerService.update).toHaveBeenCalledWith(subLedger);
      expect(comp.isSaving).toEqual(false);
    });

    it('Should call create service on save for new entity', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<SubLedger>>();
      const subLedger = new SubLedger();
      jest.spyOn(subLedgerService, 'create').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ subLedger });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.next(new HttpResponse({ body: subLedger }));
      saveSubject.complete();

      // THEN
      expect(subLedgerService.create).toHaveBeenCalledWith(subLedger);
      expect(comp.isSaving).toEqual(false);
      expect(comp.previousState).toHaveBeenCalled();
    });

    it('Should set isSaving to false on error', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<SubLedger>>();
      const subLedger = { id: 123 };
      jest.spyOn(subLedgerService, 'update').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ subLedger });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.error('This is an error!');

      // THEN
      expect(subLedgerService.update).toHaveBeenCalledWith(subLedger);
      expect(comp.isSaving).toEqual(false);
      expect(comp.previousState).not.toHaveBeenCalled();
    });
  });
});
