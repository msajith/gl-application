import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { finalize } from 'rxjs/operators';

import dayjs from 'dayjs/esm';
import { DATE_TIME_FORMAT } from 'app/config/input.constants';

import { ISubLedger, SubLedger } from '../sub-ledger.model';
import { SubLedgerService } from '../service/sub-ledger.service';
import { EntryStatus } from 'app/entities/enumerations/entry-status.model';
import { GlEntryType } from 'app/entities/enumerations/gl-entry-type.model';

@Component({
  selector: 'jhi-sub-ledger-update',
  templateUrl: './sub-ledger-update.component.html',
})
export class SubLedgerUpdateComponent implements OnInit {
  isSaving = false;
  entryStatusValues = Object.keys(EntryStatus);
  glEntryTypeValues = Object.keys(GlEntryType);

  editForm = this.fb.group({
    id: [],
    subLedgerRefNo: [],
    accountNo: [],
    txnDate: [],
    txnDateInUTC: [],
    txnDateInLocal: [],
    txnDateForSettlement: [],
    amountInAccountCurrency: [],
    amountInLocalCurrency: [],
    rate: [],
    entryCategory: [],
    description1: [],
    description2: [],
    createdOn: [],
    createdBy: [],
    entryStatus: [],
    glEntryType: [],
  });

  constructor(protected subLedgerService: SubLedgerService, protected activatedRoute: ActivatedRoute, protected fb: FormBuilder) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ subLedger }) => {
      if (subLedger.id === undefined) {
        const today = dayjs().startOf('day');
        subLedger.txnDateInUTC = today;
        subLedger.txnDateInLocal = today;
        subLedger.txnDateForSettlement = today;
        subLedger.createdOn = today;
      }

      this.updateForm(subLedger);
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const subLedger = this.createFromForm();
    if (subLedger.id !== undefined) {
      this.subscribeToSaveResponse(this.subLedgerService.update(subLedger));
    } else {
      this.subscribeToSaveResponse(this.subLedgerService.create(subLedger));
    }
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<ISubLedger>>): void {
    result.pipe(finalize(() => this.onSaveFinalize())).subscribe({
      next: () => this.onSaveSuccess(),
      error: () => this.onSaveError(),
    });
  }

  protected onSaveSuccess(): void {
    this.previousState();
  }

  protected onSaveError(): void {
    // Api for inheritance.
  }

  protected onSaveFinalize(): void {
    this.isSaving = false;
  }

  protected updateForm(subLedger: ISubLedger): void {
    this.editForm.patchValue({
      id: subLedger.id,
      subLedgerRefNo: subLedger.subLedgerRefNo,
      accountNo: subLedger.accountNo,
      txnDate: subLedger.txnDate,
      txnDateInUTC: subLedger.txnDateInUTC ? subLedger.txnDateInUTC.format(DATE_TIME_FORMAT) : null,
      txnDateInLocal: subLedger.txnDateInLocal ? subLedger.txnDateInLocal.format(DATE_TIME_FORMAT) : null,
      txnDateForSettlement: subLedger.txnDateForSettlement ? subLedger.txnDateForSettlement.format(DATE_TIME_FORMAT) : null,
      amountInAccountCurrency: subLedger.amountInAccountCurrency,
      amountInLocalCurrency: subLedger.amountInLocalCurrency,
      rate: subLedger.rate,
      entryCategory: subLedger.entryCategory,
      description1: subLedger.description1,
      description2: subLedger.description2,
      createdOn: subLedger.createdOn ? subLedger.createdOn.format(DATE_TIME_FORMAT) : null,
      createdBy: subLedger.createdBy,
      entryStatus: subLedger.entryStatus,
      glEntryType: subLedger.glEntryType,
    });
  }

  protected createFromForm(): ISubLedger {
    return {
      ...new SubLedger(),
      id: this.editForm.get(['id'])!.value,
      subLedgerRefNo: this.editForm.get(['subLedgerRefNo'])!.value,
      accountNo: this.editForm.get(['accountNo'])!.value,
      txnDate: this.editForm.get(['txnDate'])!.value,
      txnDateInUTC: this.editForm.get(['txnDateInUTC'])!.value
        ? dayjs(this.editForm.get(['txnDateInUTC'])!.value, DATE_TIME_FORMAT)
        : undefined,
      txnDateInLocal: this.editForm.get(['txnDateInLocal'])!.value
        ? dayjs(this.editForm.get(['txnDateInLocal'])!.value, DATE_TIME_FORMAT)
        : undefined,
      txnDateForSettlement: this.editForm.get(['txnDateForSettlement'])!.value
        ? dayjs(this.editForm.get(['txnDateForSettlement'])!.value, DATE_TIME_FORMAT)
        : undefined,
      amountInAccountCurrency: this.editForm.get(['amountInAccountCurrency'])!.value,
      amountInLocalCurrency: this.editForm.get(['amountInLocalCurrency'])!.value,
      rate: this.editForm.get(['rate'])!.value,
      entryCategory: this.editForm.get(['entryCategory'])!.value,
      description1: this.editForm.get(['description1'])!.value,
      description2: this.editForm.get(['description2'])!.value,
      createdOn: this.editForm.get(['createdOn'])!.value ? dayjs(this.editForm.get(['createdOn'])!.value, DATE_TIME_FORMAT) : undefined,
      createdBy: this.editForm.get(['createdBy'])!.value,
      entryStatus: this.editForm.get(['entryStatus'])!.value,
      glEntryType: this.editForm.get(['glEntryType'])!.value,
    };
  }
}
