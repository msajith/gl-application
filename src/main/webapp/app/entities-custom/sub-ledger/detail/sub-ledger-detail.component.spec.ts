import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { SubLedgerDetailComponent } from './sub-ledger-detail.component';

describe('SubLedger Management Detail Component', () => {
  let comp: SubLedgerDetailComponent;
  let fixture: ComponentFixture<SubLedgerDetailComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [SubLedgerDetailComponent],
      providers: [
        {
          provide: ActivatedRoute,
          useValue: { data: of({ subLedger: { id: 123 } }) },
        },
      ],
    })
      .overrideTemplate(SubLedgerDetailComponent, '')
      .compileComponents();
    fixture = TestBed.createComponent(SubLedgerDetailComponent);
    comp = fixture.componentInstance;
  });

  describe('OnInit', () => {
    it('Should load subLedger on init', () => {
      // WHEN
      comp.ngOnInit();

      // THEN
      expect(comp.subLedger).toEqual(expect.objectContaining({ id: 123 }));
    });
  });
});
