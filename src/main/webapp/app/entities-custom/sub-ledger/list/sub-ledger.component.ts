import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

import { ISubLedger } from '../sub-ledger.model';
import { SubLedgerService } from '../service/sub-ledger.service';
import { SubLedgerDeleteDialogComponent } from '../delete/sub-ledger-delete-dialog.component';

@Component({
  selector: 'jhi-sub-ledger',
  templateUrl: './sub-ledger.component.html',
})
export class SubLedgerComponent implements OnInit {
  subLedgers?: ISubLedger[];
  isLoading = false;

  constructor(protected subLedgerService: SubLedgerService, protected modalService: NgbModal) {}

  loadAll(): void {
    this.isLoading = true;

    this.subLedgerService.query().subscribe({
      next: (res: HttpResponse<ISubLedger[]>) => {
        this.isLoading = false;
        this.subLedgers = res.body ?? [];
      },
      error: () => {
        this.isLoading = false;
      },
    });
  }

  ngOnInit(): void {
    this.loadAll();
  }

  trackId(index: number, item: ISubLedger): number {
    return item.id!;
  }

  delete(subLedger: ISubLedger): void {
    const modalRef = this.modalService.open(SubLedgerDeleteDialogComponent, { size: 'lg', backdrop: 'static' });
    modalRef.componentInstance.subLedger = subLedger;
    // unsubscribe not needed because closed completes on modal close
    modalRef.closed.subscribe(reason => {
      if (reason === 'deleted') {
        this.loadAll();
      }
    });
  }
}
