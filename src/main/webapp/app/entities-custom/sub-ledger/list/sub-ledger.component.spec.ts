import { ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpHeaders, HttpResponse } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { of } from 'rxjs';

import { SubLedgerService } from '../service/sub-ledger.service';

import { SubLedgerComponent } from './sub-ledger.component';

describe('SubLedger Management Component', () => {
  let comp: SubLedgerComponent;
  let fixture: ComponentFixture<SubLedgerComponent>;
  let service: SubLedgerService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      declarations: [SubLedgerComponent],
    })
      .overrideTemplate(SubLedgerComponent, '')
      .compileComponents();

    fixture = TestBed.createComponent(SubLedgerComponent);
    comp = fixture.componentInstance;
    service = TestBed.inject(SubLedgerService);

    const headers = new HttpHeaders();
    jest.spyOn(service, 'query').mockReturnValue(
      of(
        new HttpResponse({
          body: [{ id: 123 }],
          headers,
        })
      )
    );
  });

  it('Should call load all on init', () => {
    // WHEN
    comp.ngOnInit();

    // THEN
    expect(service.query).toHaveBeenCalled();
    expect(comp.subLedgers?.[0]).toEqual(expect.objectContaining({ id: 123 }));
  });
});
