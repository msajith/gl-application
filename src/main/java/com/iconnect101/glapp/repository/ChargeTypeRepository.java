package com.iconnect101.glapp.repository;

import com.iconnect101.glapp.domain.ChargeType;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data SQL repository for the ChargeType entity.
 */
@SuppressWarnings("unused")
@Repository
public interface ChargeTypeRepository extends JpaRepository<ChargeType, Long> {}
