package com.iconnect101.glapp.repository;

import com.iconnect101.glapp.domain.GlAccount;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data SQL repository for the GlAccount entity.
 */
@SuppressWarnings("unused")
@Repository
public interface GlAccountRepository extends JpaRepository<GlAccount, Long> {}
