package com.iconnect101.glapp.repository;

import com.iconnect101.glapp.domain.CasaAccountDayBalance;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data SQL repository for the CasaAccountDayBalance entity.
 */
@SuppressWarnings("unused")
@Repository
public interface CasaAccountDayBalanceRepository extends JpaRepository<CasaAccountDayBalance, Long> {}
