package com.iconnect101.glapp.repository;

import com.iconnect101.glapp.domain.SubLedger;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data SQL repository for the SubLedger entity.
 */
@SuppressWarnings("unused")
@Repository
public interface SubLedgerRepository extends JpaRepository<SubLedger, Long> {}
