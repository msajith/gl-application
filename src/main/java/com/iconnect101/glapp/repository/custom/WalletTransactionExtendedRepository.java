package com.iconnect101.glapp.repository.custom;

import com.iconnect101.glapp.repository.WalletTransactionRepository;
import org.springframework.stereotype.Repository;

@SuppressWarnings("unused")
@Repository
public interface WalletTransactionExtendedRepository extends WalletTransactionRepository {}
