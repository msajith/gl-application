package com.iconnect101.glapp.repository.custom;

import com.iconnect101.glapp.domain.AppConfig;
import com.iconnect101.glapp.repository.AppConfigRepository;
import org.springframework.data.jpa.repository.Query;

public interface AppConfigExtendedRepository extends AppConfigRepository {
    @Query(value = " select * from app_config a where a.code =?1 and a.sub_code=?2", nativeQuery = true)
    AppConfig findByCodeAndSubCode(String code, String subCode);
}
