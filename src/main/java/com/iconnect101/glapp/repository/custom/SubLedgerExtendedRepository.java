package com.iconnect101.glapp.repository.custom;

import com.iconnect101.glapp.repository.SubLedgerRepository;
import org.springframework.stereotype.Repository;

@SuppressWarnings("unused")
@Repository
public interface SubLedgerExtendedRepository extends SubLedgerRepository {}
