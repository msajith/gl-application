package com.iconnect101.glapp.repository.custom;

import com.iconnect101.glapp.domain.CasaAccount;
import com.iconnect101.glapp.domain.Customer;
import com.iconnect101.glapp.repository.CasaAccountRepository;
import java.util.List;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@SuppressWarnings("unused")
@Repository
public interface CasaAccountExtendedRepository extends CasaAccountRepository {
    CasaAccount findByAccountNo(String accountNo);

    @Query(value = " select * from casa_account a where a.customer_id=?1 ", nativeQuery = true)
    List<CasaAccount> findByCustomerId(Long customerId);
}
