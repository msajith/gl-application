package com.iconnect101.glapp.repository.custom;

import com.iconnect101.glapp.domain.Charge;
import com.iconnect101.glapp.repository.ChargeRepository;
import java.util.Optional;
import org.springframework.data.jpa.repository.Query;

public interface ChargeExtendedRepository extends ChargeRepository {
    @Query(value = "select * from charge a where a.beneficiary_country_id=?1 and a.txn_currency_id=?2 ", nativeQuery = true)
    Optional<Charge> getCharge(Long beneCountryId, Long beneCurrencyId);
}
