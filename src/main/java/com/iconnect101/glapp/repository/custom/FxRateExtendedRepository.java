package com.iconnect101.glapp.repository.custom;

import com.iconnect101.glapp.domain.FxRate;
import com.iconnect101.glapp.repository.FxRateRepository;
import java.util.Optional;
import org.springframework.data.jpa.repository.Query;

public interface FxRateExtendedRepository extends FxRateRepository {
    @Query(value = "  select * from fx_rate a where a.base_currency=?1 and a.quote_currency=?2 ", nativeQuery = true)
    Optional<FxRate> getRate(String baseCurrency, String quoteCurrency);
}
