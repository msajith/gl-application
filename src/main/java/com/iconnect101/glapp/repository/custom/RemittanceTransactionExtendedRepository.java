package com.iconnect101.glapp.repository.custom;

import com.iconnect101.glapp.repository.RemittanceTransactionRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface RemittanceTransactionExtendedRepository extends RemittanceTransactionRepository {}
