package com.iconnect101.glapp.repository.custom;

import com.iconnect101.glapp.domain.CasaAccountDayBalance;
import com.iconnect101.glapp.repository.CasaAccountDayBalanceRepository;
import java.time.LocalDate;
import org.springframework.data.jpa.repository.Query;

public interface CasaAccountDayBalanceExtendedRepository extends CasaAccountDayBalanceRepository {
    //

    @Query(value = " select * from casa_account_day_balance where casa_account_id=?1 and date=?2 ", nativeQuery = true)
    CasaAccountDayBalance getDaybalance(Long casaAccountId, LocalDate date);
}
