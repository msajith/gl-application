package com.iconnect101.glapp.repository.custom;

import com.iconnect101.glapp.repository.CustomerRepository;
import org.springframework.stereotype.Repository;

@SuppressWarnings("unused")
@Repository
public interface CustomerExtendedRepository extends CustomerRepository {}
