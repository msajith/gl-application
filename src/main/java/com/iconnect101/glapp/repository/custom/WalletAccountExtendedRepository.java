package com.iconnect101.glapp.repository.custom;

import com.iconnect101.glapp.domain.WalletAccount;
import com.iconnect101.glapp.repository.WalletAccountRepository;
import org.springframework.stereotype.Repository;

@SuppressWarnings("unused")
@Repository
public interface WalletAccountExtendedRepository extends WalletAccountRepository {
    WalletAccount findByAccountNo(String accountNo);
}
