package com.iconnect101.glapp.repository.custom;

import com.iconnect101.glapp.domain.Beneficiary;
import com.iconnect101.glapp.repository.BeneficiaryRepository;
import java.util.List;
import org.springframework.data.jpa.repository.Query;

public interface BeneficiaryExtendedRepository extends BeneficiaryRepository {
    @Query(value = " select * from beneficiary a where a.customer_id=?1 ", nativeQuery = true)
    List<Beneficiary> findByCustomerId(Long customerId);
}
