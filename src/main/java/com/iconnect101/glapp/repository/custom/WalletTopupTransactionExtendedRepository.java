package com.iconnect101.glapp.repository.custom;

import com.iconnect101.glapp.repository.WalletTopupTransactionRepository;
import org.springframework.stereotype.Repository;

@SuppressWarnings("unused")
@Repository
public interface WalletTopupTransactionExtendedRepository extends WalletTopupTransactionRepository {}
