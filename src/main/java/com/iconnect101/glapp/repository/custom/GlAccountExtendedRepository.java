package com.iconnect101.glapp.repository.custom;

import com.iconnect101.glapp.repository.GlAccountRepository;
import org.springframework.stereotype.Repository;

@SuppressWarnings("unused")
@Repository
public interface GlAccountExtendedRepository extends GlAccountRepository {}
