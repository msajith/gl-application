package com.iconnect101.glapp.repository;

import com.iconnect101.glapp.domain.WalletAccount;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data SQL repository for the WalletAccount entity.
 */
@SuppressWarnings("unused")
@Repository
public interface WalletAccountRepository extends JpaRepository<WalletAccount, Long> {}
