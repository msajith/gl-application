package com.iconnect101.glapp.repository;

import com.iconnect101.glapp.domain.GlEntryType;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data SQL repository for the GlEntryType entity.
 */
@SuppressWarnings("unused")
@Repository
public interface GlEntryTypeRepository extends JpaRepository<GlEntryType, Long> {}
