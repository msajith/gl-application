package com.iconnect101.glapp.repository;

import com.iconnect101.glapp.domain.CasaAccount;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data SQL repository for the CasaAccount entity.
 */
@SuppressWarnings("unused")
@Repository
public interface CasaAccountRepository extends JpaRepository<CasaAccount, Long> {}
