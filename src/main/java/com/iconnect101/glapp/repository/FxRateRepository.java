package com.iconnect101.glapp.repository;

import com.iconnect101.glapp.domain.FxRate;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data SQL repository for the FxRate entity.
 */
@SuppressWarnings("unused")
@Repository
public interface FxRateRepository extends JpaRepository<FxRate, Long> {}
