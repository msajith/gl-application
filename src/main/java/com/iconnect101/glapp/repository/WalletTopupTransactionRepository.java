package com.iconnect101.glapp.repository;

import com.iconnect101.glapp.domain.WalletTopupTransaction;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data SQL repository for the WalletTopupTransaction entity.
 */
@SuppressWarnings("unused")
@Repository
public interface WalletTopupTransactionRepository extends JpaRepository<WalletTopupTransaction, Long> {}
