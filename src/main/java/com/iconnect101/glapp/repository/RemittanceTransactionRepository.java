package com.iconnect101.glapp.repository;

import com.iconnect101.glapp.domain.RemittanceTransaction;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data SQL repository for the RemittanceTransaction entity.
 */
@SuppressWarnings("unused")
@Repository
public interface RemittanceTransactionRepository extends JpaRepository<RemittanceTransaction, Long> {}
