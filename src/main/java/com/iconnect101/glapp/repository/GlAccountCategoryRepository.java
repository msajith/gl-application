package com.iconnect101.glapp.repository;

import com.iconnect101.glapp.domain.GlAccountCategory;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data SQL repository for the GlAccountCategory entity.
 */
@SuppressWarnings("unused")
@Repository
public interface GlAccountCategoryRepository extends JpaRepository<GlAccountCategory, Long> {}
