package com.iconnect101.glapp.repository;

import com.iconnect101.glapp.domain.AccountCategory;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data SQL repository for the AccountCategory entity.
 */
@SuppressWarnings("unused")
@Repository
public interface AccountCategoryRepository extends JpaRepository<AccountCategory, Long> {}
