package com.iconnect101.glapp.repository;

import com.iconnect101.glapp.domain.GeneralLedger;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data SQL repository for the GeneralLedger entity.
 */
@SuppressWarnings("unused")
@Repository
public interface GeneralLedgerRepository extends JpaRepository<GeneralLedger, Long> {}
