package com.iconnect101.glapp.repository;

import com.iconnect101.glapp.domain.CustomerSegment;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data SQL repository for the CustomerSegment entity.
 */
@SuppressWarnings("unused")
@Repository
public interface CustomerSegmentRepository extends JpaRepository<CustomerSegment, Long> {}
