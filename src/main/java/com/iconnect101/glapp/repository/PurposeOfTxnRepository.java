package com.iconnect101.glapp.repository;

import com.iconnect101.glapp.domain.PurposeOfTxn;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data SQL repository for the PurposeOfTxn entity.
 */
@SuppressWarnings("unused")
@Repository
public interface PurposeOfTxnRepository extends JpaRepository<PurposeOfTxn, Long> {}
