package com.iconnect101.glapp.repository;

import com.iconnect101.glapp.domain.FundingSourceType;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data SQL repository for the FundingSourceType entity.
 */
@SuppressWarnings("unused")
@Repository
public interface FundingSourceTypeRepository extends JpaRepository<FundingSourceType, Long> {}
