package com.iconnect101.glapp.web.rest.custom;

import com.iconnect101.glapp.config.custom.AppConstants;
import com.iconnect101.glapp.domain.SubLedger;
import com.iconnect101.glapp.domain.enumeration.EntryStatus;
import com.iconnect101.glapp.domain.enumeration.GlEntryType;
import com.iconnect101.glapp.repository.custom.SubLedgerExtendedRepository;
import com.iconnect101.glapp.web.rest.SubLedgerResource;
import java.math.BigDecimal;
import java.time.Instant;
import java.time.LocalDate;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/v1")
public class SubLedgerExtendedResource extends SubLedgerResource {

    private final SubLedgerExtendedRepository subLedgerExtendedRepository;

    public SubLedgerExtendedResource(SubLedgerExtendedRepository subLedgerExtendedRepository) {
        super(subLedgerExtendedRepository);
        this.subLedgerExtendedRepository = subLedgerExtendedRepository;
        // TODO Auto-generated constructor stub
    }

    //RefNumber,Desc1, Desc2, Txndate, AmountInAcccy, AmountInLocalCcy, rate,AcNo , GL Entry tpe

    public SubLedger saveGLEntry(
        String txnRefnumber,
        String desc1,
        String desc2,
        LocalDate txnDate,
        BigDecimal amountInAcCcy,
        BigDecimal amountInLocalCcy,
        BigDecimal rate,
        String accountNumber,
        GlEntryType glEntryType
    ) {
        SubLedger subLedgerEntry = new SubLedger();
        subLedgerEntry.setSubLedgerRefNo(txnRefnumber);
        subLedgerEntry.accountNo(accountNumber);
        if (txnDate != null) {
            subLedgerEntry.setTxnDate(txnDate);
        } else {
            subLedgerEntry.setTxnDate(LocalDate.now());
        }

        subLedgerEntry.setAmountInAccountCurrency(amountInAcCcy);
        subLedgerEntry.setAmountInLocalCurrency(amountInLocalCcy);
        subLedgerEntry.setRate(rate);
        subLedgerEntry.setDescription1(desc1);
        subLedgerEntry.setDescription2(desc2);
        subLedgerEntry.setCreatedOn(Instant.now());
        subLedgerEntry.setCreatedBy(AppConstants.CREATED_BY);
        subLedgerEntry.setEntryStatus(EntryStatus.ACTIVE);
        subLedgerEntry.setGlEntryType(glEntryType);
        return this.subLedgerExtendedRepository.save(subLedgerEntry);
    }
}
