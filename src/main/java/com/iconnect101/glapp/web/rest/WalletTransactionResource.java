package com.iconnect101.glapp.web.rest;

import com.iconnect101.glapp.domain.WalletTransaction;
import com.iconnect101.glapp.repository.WalletTransactionRepository;
import com.iconnect101.glapp.web.rest.errors.BadRequestAlertException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;
import tech.jhipster.web.util.HeaderUtil;
import tech.jhipster.web.util.ResponseUtil;

/**
 * REST controller for managing {@link com.iconnect101.glapp.domain.WalletTransaction}.
 */
@RestController
@RequestMapping("/api")
@Transactional
public class WalletTransactionResource {

    private final Logger log = LoggerFactory.getLogger(WalletTransactionResource.class);

    private static final String ENTITY_NAME = "walletTransaction";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final WalletTransactionRepository walletTransactionRepository;

    public WalletTransactionResource(WalletTransactionRepository walletTransactionRepository) {
        this.walletTransactionRepository = walletTransactionRepository;
    }

    /**
     * {@code POST  /wallet-transactions} : Create a new walletTransaction.
     *
     * @param walletTransaction the walletTransaction to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new walletTransaction, or with status {@code 400 (Bad Request)} if the walletTransaction has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/wallet-transactions")
    public ResponseEntity<WalletTransaction> createWalletTransaction(@RequestBody WalletTransaction walletTransaction)
        throws URISyntaxException {
        log.debug("REST request to save WalletTransaction : {}", walletTransaction);
        if (walletTransaction.getId() != null) {
            throw new BadRequestAlertException("A new walletTransaction cannot already have an ID", ENTITY_NAME, "idexists");
        }
        WalletTransaction result = walletTransactionRepository.save(walletTransaction);
        return ResponseEntity
            .created(new URI("/api/wallet-transactions/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /wallet-transactions/:id} : Updates an existing walletTransaction.
     *
     * @param id the id of the walletTransaction to save.
     * @param walletTransaction the walletTransaction to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated walletTransaction,
     * or with status {@code 400 (Bad Request)} if the walletTransaction is not valid,
     * or with status {@code 500 (Internal Server Error)} if the walletTransaction couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/wallet-transactions/{id}")
    public ResponseEntity<WalletTransaction> updateWalletTransaction(
        @PathVariable(value = "id", required = false) final Long id,
        @RequestBody WalletTransaction walletTransaction
    ) throws URISyntaxException {
        log.debug("REST request to update WalletTransaction : {}, {}", id, walletTransaction);
        if (walletTransaction.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, walletTransaction.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!walletTransactionRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        WalletTransaction result = walletTransactionRepository.save(walletTransaction);
        return ResponseEntity
            .ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, walletTransaction.getId().toString()))
            .body(result);
    }

    /**
     * {@code PATCH  /wallet-transactions/:id} : Partial updates given fields of an existing walletTransaction, field will ignore if it is null
     *
     * @param id the id of the walletTransaction to save.
     * @param walletTransaction the walletTransaction to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated walletTransaction,
     * or with status {@code 400 (Bad Request)} if the walletTransaction is not valid,
     * or with status {@code 404 (Not Found)} if the walletTransaction is not found,
     * or with status {@code 500 (Internal Server Error)} if the walletTransaction couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PatchMapping(value = "/wallet-transactions/{id}", consumes = { "application/json", "application/merge-patch+json" })
    public ResponseEntity<WalletTransaction> partialUpdateWalletTransaction(
        @PathVariable(value = "id", required = false) final Long id,
        @RequestBody WalletTransaction walletTransaction
    ) throws URISyntaxException {
        log.debug("REST request to partial update WalletTransaction partially : {}, {}", id, walletTransaction);
        if (walletTransaction.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, walletTransaction.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!walletTransactionRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        Optional<WalletTransaction> result = walletTransactionRepository
            .findById(walletTransaction.getId())
            .map(existingWalletTransaction -> {
                if (walletTransaction.getTxnRefNo() != null) {
                    existingWalletTransaction.setTxnRefNo(walletTransaction.getTxnRefNo());
                }
                if (walletTransaction.getAccountNo() != null) {
                    existingWalletTransaction.setAccountNo(walletTransaction.getAccountNo());
                }
                if (walletTransaction.getTxnDate() != null) {
                    existingWalletTransaction.setTxnDate(walletTransaction.getTxnDate());
                }
                if (walletTransaction.getTxnDateInUTC() != null) {
                    existingWalletTransaction.setTxnDateInUTC(walletTransaction.getTxnDateInUTC());
                }
                if (walletTransaction.getTxnDateInLocal() != null) {
                    existingWalletTransaction.setTxnDateInLocal(walletTransaction.getTxnDateInLocal());
                }
                if (walletTransaction.getTxnDateForSettlement() != null) {
                    existingWalletTransaction.setTxnDateForSettlement(walletTransaction.getTxnDateForSettlement());
                }
                if (walletTransaction.getValueDate() != null) {
                    existingWalletTransaction.setValueDate(walletTransaction.getValueDate());
                }
                if (walletTransaction.getValueDateInUTC() != null) {
                    existingWalletTransaction.setValueDateInUTC(walletTransaction.getValueDateInUTC());
                }
                if (walletTransaction.getValueDateInLocal() != null) {
                    existingWalletTransaction.setValueDateInLocal(walletTransaction.getValueDateInLocal());
                }
                if (walletTransaction.getValueDateForSettlement() != null) {
                    existingWalletTransaction.setValueDateForSettlement(walletTransaction.getValueDateForSettlement());
                }
                if (walletTransaction.getTxnAmount() != null) {
                    existingWalletTransaction.setTxnAmount(walletTransaction.getTxnAmount());
                }
                if (walletTransaction.getInstructedAmount() != null) {
                    existingWalletTransaction.setInstructedAmount(walletTransaction.getInstructedAmount());
                }
                if (walletTransaction.getAmountInAccountCurrency() != null) {
                    existingWalletTransaction.setAmountInAccountCurrency(walletTransaction.getAmountInAccountCurrency());
                }
                if (walletTransaction.getAmountInLocalCurrency() != null) {
                    existingWalletTransaction.setAmountInLocalCurrency(walletTransaction.getAmountInLocalCurrency());
                }
                if (walletTransaction.getChargeInAccountCurrency() != null) {
                    existingWalletTransaction.setChargeInAccountCurrency(walletTransaction.getChargeInAccountCurrency());
                }
                if (walletTransaction.getChargeInLocalCurrency() != null) {
                    existingWalletTransaction.setChargeInLocalCurrency(walletTransaction.getChargeInLocalCurrency());
                }
                if (walletTransaction.getFundingSource() != null) {
                    existingWalletTransaction.setFundingSource(walletTransaction.getFundingSource());
                }
                if (walletTransaction.getMerchantID() != null) {
                    existingWalletTransaction.setMerchantID(walletTransaction.getMerchantID());
                }
                if (walletTransaction.getMerchantInfo() != null) {
                    existingWalletTransaction.setMerchantInfo(walletTransaction.getMerchantInfo());
                }
                if (walletTransaction.getExternalRefNo() != null) {
                    existingWalletTransaction.setExternalRefNo(walletTransaction.getExternalRefNo());
                }
                if (walletTransaction.getTxnDescription() != null) {
                    existingWalletTransaction.setTxnDescription(walletTransaction.getTxnDescription());
                }
                if (walletTransaction.getTxnInternalDescription() != null) {
                    existingWalletTransaction.setTxnInternalDescription(walletTransaction.getTxnInternalDescription());
                }
                if (walletTransaction.getCreatedOn() != null) {
                    existingWalletTransaction.setCreatedOn(walletTransaction.getCreatedOn());
                }
                if (walletTransaction.getCreatedBy() != null) {
                    existingWalletTransaction.setCreatedBy(walletTransaction.getCreatedBy());
                }
                if (walletTransaction.getTxnStatus() != null) {
                    existingWalletTransaction.setTxnStatus(walletTransaction.getTxnStatus());
                }
                if (walletTransaction.getTxnDebitAccountNo() != null) {
                    existingWalletTransaction.setTxnDebitAccountNo(walletTransaction.getTxnDebitAccountNo());
                }
                if (walletTransaction.getTxnCreditAccountNo() != null) {
                    existingWalletTransaction.setTxnCreditAccountNo(walletTransaction.getTxnCreditAccountNo());
                }
                if (walletTransaction.getChargeDebitAccountNo() != null) {
                    existingWalletTransaction.setChargeDebitAccountNo(walletTransaction.getChargeDebitAccountNo());
                }
                if (walletTransaction.getChargeCreditAccountNo() != null) {
                    existingWalletTransaction.setChargeCreditAccountNo(walletTransaction.getChargeCreditAccountNo());
                }

                return existingWalletTransaction;
            })
            .map(walletTransactionRepository::save);

        return ResponseUtil.wrapOrNotFound(
            result,
            HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, walletTransaction.getId().toString())
        );
    }

    /**
     * {@code GET  /wallet-transactions} : get all the walletTransactions.
     *
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of walletTransactions in body.
     */
    @GetMapping("/wallet-transactions")
    public List<WalletTransaction> getAllWalletTransactions() {
        log.debug("REST request to get all WalletTransactions");
        return walletTransactionRepository.findAll();
    }

    /**
     * {@code GET  /wallet-transactions/:id} : get the "id" walletTransaction.
     *
     * @param id the id of the walletTransaction to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the walletTransaction, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/wallet-transactions/{id}")
    public ResponseEntity<WalletTransaction> getWalletTransaction(@PathVariable Long id) {
        log.debug("REST request to get WalletTransaction : {}", id);
        Optional<WalletTransaction> walletTransaction = walletTransactionRepository.findById(id);
        return ResponseUtil.wrapOrNotFound(walletTransaction);
    }

    /**
     * {@code DELETE  /wallet-transactions/:id} : delete the "id" walletTransaction.
     *
     * @param id the id of the walletTransaction to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/wallet-transactions/{id}")
    public ResponseEntity<Void> deleteWalletTransaction(@PathVariable Long id) {
        log.debug("REST request to delete WalletTransaction : {}", id);
        walletTransactionRepository.deleteById(id);
        return ResponseEntity
            .noContent()
            .headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id.toString()))
            .build();
    }
}
