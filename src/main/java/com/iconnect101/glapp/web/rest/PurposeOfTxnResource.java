package com.iconnect101.glapp.web.rest;

import com.iconnect101.glapp.domain.PurposeOfTxn;
import com.iconnect101.glapp.repository.PurposeOfTxnRepository;
import com.iconnect101.glapp.web.rest.errors.BadRequestAlertException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;
import tech.jhipster.web.util.HeaderUtil;
import tech.jhipster.web.util.ResponseUtil;

/**
 * REST controller for managing {@link com.iconnect101.glapp.domain.PurposeOfTxn}.
 */
@RestController
@RequestMapping("/api")
@Transactional
public class PurposeOfTxnResource {

    private final Logger log = LoggerFactory.getLogger(PurposeOfTxnResource.class);

    private static final String ENTITY_NAME = "purposeOfTxn";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final PurposeOfTxnRepository purposeOfTxnRepository;

    public PurposeOfTxnResource(PurposeOfTxnRepository purposeOfTxnRepository) {
        this.purposeOfTxnRepository = purposeOfTxnRepository;
    }

    /**
     * {@code POST  /purpose-of-txns} : Create a new purposeOfTxn.
     *
     * @param purposeOfTxn the purposeOfTxn to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new purposeOfTxn, or with status {@code 400 (Bad Request)} if the purposeOfTxn has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/purpose-of-txns")
    public ResponseEntity<PurposeOfTxn> createPurposeOfTxn(@RequestBody PurposeOfTxn purposeOfTxn) throws URISyntaxException {
        log.debug("REST request to save PurposeOfTxn : {}", purposeOfTxn);
        if (purposeOfTxn.getId() != null) {
            throw new BadRequestAlertException("A new purposeOfTxn cannot already have an ID", ENTITY_NAME, "idexists");
        }
        PurposeOfTxn result = purposeOfTxnRepository.save(purposeOfTxn);
        return ResponseEntity
            .created(new URI("/api/purpose-of-txns/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /purpose-of-txns/:id} : Updates an existing purposeOfTxn.
     *
     * @param id the id of the purposeOfTxn to save.
     * @param purposeOfTxn the purposeOfTxn to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated purposeOfTxn,
     * or with status {@code 400 (Bad Request)} if the purposeOfTxn is not valid,
     * or with status {@code 500 (Internal Server Error)} if the purposeOfTxn couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/purpose-of-txns/{id}")
    public ResponseEntity<PurposeOfTxn> updatePurposeOfTxn(
        @PathVariable(value = "id", required = false) final Long id,
        @RequestBody PurposeOfTxn purposeOfTxn
    ) throws URISyntaxException {
        log.debug("REST request to update PurposeOfTxn : {}, {}", id, purposeOfTxn);
        if (purposeOfTxn.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, purposeOfTxn.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!purposeOfTxnRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        PurposeOfTxn result = purposeOfTxnRepository.save(purposeOfTxn);
        return ResponseEntity
            .ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, purposeOfTxn.getId().toString()))
            .body(result);
    }

    /**
     * {@code PATCH  /purpose-of-txns/:id} : Partial updates given fields of an existing purposeOfTxn, field will ignore if it is null
     *
     * @param id the id of the purposeOfTxn to save.
     * @param purposeOfTxn the purposeOfTxn to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated purposeOfTxn,
     * or with status {@code 400 (Bad Request)} if the purposeOfTxn is not valid,
     * or with status {@code 404 (Not Found)} if the purposeOfTxn is not found,
     * or with status {@code 500 (Internal Server Error)} if the purposeOfTxn couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PatchMapping(value = "/purpose-of-txns/{id}", consumes = { "application/json", "application/merge-patch+json" })
    public ResponseEntity<PurposeOfTxn> partialUpdatePurposeOfTxn(
        @PathVariable(value = "id", required = false) final Long id,
        @RequestBody PurposeOfTxn purposeOfTxn
    ) throws URISyntaxException {
        log.debug("REST request to partial update PurposeOfTxn partially : {}, {}", id, purposeOfTxn);
        if (purposeOfTxn.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, purposeOfTxn.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!purposeOfTxnRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        Optional<PurposeOfTxn> result = purposeOfTxnRepository
            .findById(purposeOfTxn.getId())
            .map(existingPurposeOfTxn -> {
                if (purposeOfTxn.getName() != null) {
                    existingPurposeOfTxn.setName(purposeOfTxn.getName());
                }
                if (purposeOfTxn.getCode() != null) {
                    existingPurposeOfTxn.setCode(purposeOfTxn.getCode());
                }

                return existingPurposeOfTxn;
            })
            .map(purposeOfTxnRepository::save);

        return ResponseUtil.wrapOrNotFound(
            result,
            HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, purposeOfTxn.getId().toString())
        );
    }

    /**
     * {@code GET  /purpose-of-txns} : get all the purposeOfTxns.
     *
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of purposeOfTxns in body.
     */
    @GetMapping("/purpose-of-txns")
    public List<PurposeOfTxn> getAllPurposeOfTxns() {
        log.debug("REST request to get all PurposeOfTxns");
        return purposeOfTxnRepository.findAll();
    }

    /**
     * {@code GET  /purpose-of-txns/:id} : get the "id" purposeOfTxn.
     *
     * @param id the id of the purposeOfTxn to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the purposeOfTxn, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/purpose-of-txns/{id}")
    public ResponseEntity<PurposeOfTxn> getPurposeOfTxn(@PathVariable Long id) {
        log.debug("REST request to get PurposeOfTxn : {}", id);
        Optional<PurposeOfTxn> purposeOfTxn = purposeOfTxnRepository.findById(id);
        return ResponseUtil.wrapOrNotFound(purposeOfTxn);
    }

    /**
     * {@code DELETE  /purpose-of-txns/:id} : delete the "id" purposeOfTxn.
     *
     * @param id the id of the purposeOfTxn to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/purpose-of-txns/{id}")
    public ResponseEntity<Void> deletePurposeOfTxn(@PathVariable Long id) {
        log.debug("REST request to delete PurposeOfTxn : {}", id);
        purposeOfTxnRepository.deleteById(id);
        return ResponseEntity
            .noContent()
            .headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id.toString()))
            .build();
    }
}
