package com.iconnect101.glapp.web.rest;

import com.iconnect101.glapp.domain.AccountCategory;
import com.iconnect101.glapp.repository.AccountCategoryRepository;
import com.iconnect101.glapp.web.rest.errors.BadRequestAlertException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;
import tech.jhipster.web.util.HeaderUtil;
import tech.jhipster.web.util.ResponseUtil;

/**
 * REST controller for managing {@link com.iconnect101.glapp.domain.AccountCategory}.
 */
@RestController
@RequestMapping("/api")
@Transactional
public class AccountCategoryResource {

    private final Logger log = LoggerFactory.getLogger(AccountCategoryResource.class);

    private static final String ENTITY_NAME = "accountCategory";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final AccountCategoryRepository accountCategoryRepository;

    public AccountCategoryResource(AccountCategoryRepository accountCategoryRepository) {
        this.accountCategoryRepository = accountCategoryRepository;
    }

    /**
     * {@code POST  /account-categories} : Create a new accountCategory.
     *
     * @param accountCategory the accountCategory to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new accountCategory, or with status {@code 400 (Bad Request)} if the accountCategory has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/account-categories")
    public ResponseEntity<AccountCategory> createAccountCategory(@RequestBody AccountCategory accountCategory) throws URISyntaxException {
        log.debug("REST request to save AccountCategory : {}", accountCategory);
        if (accountCategory.getId() != null) {
            throw new BadRequestAlertException("A new accountCategory cannot already have an ID", ENTITY_NAME, "idexists");
        }
        AccountCategory result = accountCategoryRepository.save(accountCategory);
        return ResponseEntity
            .created(new URI("/api/account-categories/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /account-categories/:id} : Updates an existing accountCategory.
     *
     * @param id the id of the accountCategory to save.
     * @param accountCategory the accountCategory to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated accountCategory,
     * or with status {@code 400 (Bad Request)} if the accountCategory is not valid,
     * or with status {@code 500 (Internal Server Error)} if the accountCategory couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/account-categories/{id}")
    public ResponseEntity<AccountCategory> updateAccountCategory(
        @PathVariable(value = "id", required = false) final Long id,
        @RequestBody AccountCategory accountCategory
    ) throws URISyntaxException {
        log.debug("REST request to update AccountCategory : {}, {}", id, accountCategory);
        if (accountCategory.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, accountCategory.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!accountCategoryRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        AccountCategory result = accountCategoryRepository.save(accountCategory);
        return ResponseEntity
            .ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, accountCategory.getId().toString()))
            .body(result);
    }

    /**
     * {@code PATCH  /account-categories/:id} : Partial updates given fields of an existing accountCategory, field will ignore if it is null
     *
     * @param id the id of the accountCategory to save.
     * @param accountCategory the accountCategory to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated accountCategory,
     * or with status {@code 400 (Bad Request)} if the accountCategory is not valid,
     * or with status {@code 404 (Not Found)} if the accountCategory is not found,
     * or with status {@code 500 (Internal Server Error)} if the accountCategory couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PatchMapping(value = "/account-categories/{id}", consumes = { "application/json", "application/merge-patch+json" })
    public ResponseEntity<AccountCategory> partialUpdateAccountCategory(
        @PathVariable(value = "id", required = false) final Long id,
        @RequestBody AccountCategory accountCategory
    ) throws URISyntaxException {
        log.debug("REST request to partial update AccountCategory partially : {}, {}", id, accountCategory);
        if (accountCategory.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, accountCategory.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!accountCategoryRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        Optional<AccountCategory> result = accountCategoryRepository
            .findById(accountCategory.getId())
            .map(existingAccountCategory -> {
                if (accountCategory.getName() != null) {
                    existingAccountCategory.setName(accountCategory.getName());
                }
                if (accountCategory.getCode() != null) {
                    existingAccountCategory.setCode(accountCategory.getCode());
                }

                return existingAccountCategory;
            })
            .map(accountCategoryRepository::save);

        return ResponseUtil.wrapOrNotFound(
            result,
            HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, accountCategory.getId().toString())
        );
    }

    /**
     * {@code GET  /account-categories} : get all the accountCategories.
     *
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of accountCategories in body.
     */
    @GetMapping("/account-categories")
    public List<AccountCategory> getAllAccountCategories() {
        log.debug("REST request to get all AccountCategories");
        return accountCategoryRepository.findAll();
    }

    /**
     * {@code GET  /account-categories/:id} : get the "id" accountCategory.
     *
     * @param id the id of the accountCategory to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the accountCategory, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/account-categories/{id}")
    public ResponseEntity<AccountCategory> getAccountCategory(@PathVariable Long id) {
        log.debug("REST request to get AccountCategory : {}", id);
        Optional<AccountCategory> accountCategory = accountCategoryRepository.findById(id);
        return ResponseUtil.wrapOrNotFound(accountCategory);
    }

    /**
     * {@code DELETE  /account-categories/:id} : delete the "id" accountCategory.
     *
     * @param id the id of the accountCategory to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/account-categories/{id}")
    public ResponseEntity<Void> deleteAccountCategory(@PathVariable Long id) {
        log.debug("REST request to delete AccountCategory : {}", id);
        accountCategoryRepository.deleteById(id);
        return ResponseEntity
            .noContent()
            .headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id.toString()))
            .build();
    }
}
