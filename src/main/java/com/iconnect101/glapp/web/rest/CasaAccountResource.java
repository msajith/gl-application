package com.iconnect101.glapp.web.rest;

import com.iconnect101.glapp.domain.CasaAccount;
import com.iconnect101.glapp.repository.CasaAccountRepository;
import com.iconnect101.glapp.web.rest.errors.BadRequestAlertException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;
import tech.jhipster.web.util.HeaderUtil;
import tech.jhipster.web.util.ResponseUtil;

/**
 * REST controller for managing {@link com.iconnect101.glapp.domain.CasaAccount}.
 */
@RestController
@RequestMapping("/api")
@Transactional
public class CasaAccountResource {

    private final Logger log = LoggerFactory.getLogger(CasaAccountResource.class);

    private static final String ENTITY_NAME = "casaAccount";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final CasaAccountRepository casaAccountRepository;

    public CasaAccountResource(CasaAccountRepository casaAccountRepository) {
        this.casaAccountRepository = casaAccountRepository;
    }

    /**
     * {@code POST  /casa-accounts} : Create a new casaAccount.
     *
     * @param casaAccount the casaAccount to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new casaAccount, or with status {@code 400 (Bad Request)} if the casaAccount has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/casa-accounts")
    public ResponseEntity<CasaAccount> createCasaAccount(@Valid @RequestBody CasaAccount casaAccount) throws URISyntaxException {
        log.debug("REST request to save CasaAccount : {}", casaAccount);
        if (casaAccount.getId() != null) {
            throw new BadRequestAlertException("A new casaAccount cannot already have an ID", ENTITY_NAME, "idexists");
        }
        CasaAccount result = casaAccountRepository.save(casaAccount);
        return ResponseEntity
            .created(new URI("/api/casa-accounts/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /casa-accounts/:id} : Updates an existing casaAccount.
     *
     * @param id the id of the casaAccount to save.
     * @param casaAccount the casaAccount to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated casaAccount,
     * or with status {@code 400 (Bad Request)} if the casaAccount is not valid,
     * or with status {@code 500 (Internal Server Error)} if the casaAccount couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/casa-accounts/{id}")
    public ResponseEntity<CasaAccount> updateCasaAccount(
        @PathVariable(value = "id", required = false) final Long id,
        @Valid @RequestBody CasaAccount casaAccount
    ) throws URISyntaxException {
        log.debug("REST request to update CasaAccount : {}, {}", id, casaAccount);
        if (casaAccount.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, casaAccount.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!casaAccountRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        CasaAccount result = casaAccountRepository.save(casaAccount);
        return ResponseEntity
            .ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, casaAccount.getId().toString()))
            .body(result);
    }

    /**
     * {@code PATCH  /casa-accounts/:id} : Partial updates given fields of an existing casaAccount, field will ignore if it is null
     *
     * @param id the id of the casaAccount to save.
     * @param casaAccount the casaAccount to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated casaAccount,
     * or with status {@code 400 (Bad Request)} if the casaAccount is not valid,
     * or with status {@code 404 (Not Found)} if the casaAccount is not found,
     * or with status {@code 500 (Internal Server Error)} if the casaAccount couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PatchMapping(value = "/casa-accounts/{id}", consumes = { "application/json", "application/merge-patch+json" })
    public ResponseEntity<CasaAccount> partialUpdateCasaAccount(
        @PathVariable(value = "id", required = false) final Long id,
        @NotNull @RequestBody CasaAccount casaAccount
    ) throws URISyntaxException {
        log.debug("REST request to partial update CasaAccount partially : {}, {}", id, casaAccount);
        if (casaAccount.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, casaAccount.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!casaAccountRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        Optional<CasaAccount> result = casaAccountRepository
            .findById(casaAccount.getId())
            .map(existingCasaAccount -> {
                if (casaAccount.getAccountNo() != null) {
                    existingCasaAccount.setAccountNo(casaAccount.getAccountNo());
                }
                if (casaAccount.getAccountName() != null) {
                    existingCasaAccount.setAccountName(casaAccount.getAccountName());
                }
                if (casaAccount.getDate() != null) {
                    existingCasaAccount.setDate(casaAccount.getDate());
                }
                if (casaAccount.getClosingBalance() != null) {
                    existingCasaAccount.setClosingBalance(casaAccount.getClosingBalance());
                }
                if (casaAccount.getOpeningBalance() != null) {
                    existingCasaAccount.setOpeningBalance(casaAccount.getOpeningBalance());
                }
                if (casaAccount.getTotalDebit() != null) {
                    existingCasaAccount.setTotalDebit(casaAccount.getTotalDebit());
                }
                if (casaAccount.getTotalCredit() != null) {
                    existingCasaAccount.setTotalCredit(casaAccount.getTotalCredit());
                }
                if (casaAccount.getAvailableBalance() != null) {
                    existingCasaAccount.setAvailableBalance(casaAccount.getAvailableBalance());
                }
                if (casaAccount.getCurrentBalance() != null) {
                    existingCasaAccount.setCurrentBalance(casaAccount.getCurrentBalance());
                }
                if (casaAccount.getCreatedOn() != null) {
                    existingCasaAccount.setCreatedOn(casaAccount.getCreatedOn());
                }
                if (casaAccount.getCreatedBy() != null) {
                    existingCasaAccount.setCreatedBy(casaAccount.getCreatedBy());
                }
                if (casaAccount.getAccountStatus() != null) {
                    existingCasaAccount.setAccountStatus(casaAccount.getAccountStatus());
                }

                return existingCasaAccount;
            })
            .map(casaAccountRepository::save);

        return ResponseUtil.wrapOrNotFound(
            result,
            HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, casaAccount.getId().toString())
        );
    }

    /**
     * {@code GET  /casa-accounts} : get all the casaAccounts.
     *
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of casaAccounts in body.
     */
    @GetMapping("/casa-accounts")
    public List<CasaAccount> getAllCasaAccounts() {
        log.debug("REST request to get all CasaAccounts");
        return casaAccountRepository.findAll();
    }

    /**
     * {@code GET  /casa-accounts/:id} : get the "id" casaAccount.
     *
     * @param id the id of the casaAccount to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the casaAccount, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/casa-accounts/{id}")
    public ResponseEntity<CasaAccount> getCasaAccount(@PathVariable Long id) {
        log.debug("REST request to get CasaAccount : {}", id);
        Optional<CasaAccount> casaAccount = casaAccountRepository.findById(id);
        return ResponseUtil.wrapOrNotFound(casaAccount);
    }

    /**
     * {@code DELETE  /casa-accounts/:id} : delete the "id" casaAccount.
     *
     * @param id the id of the casaAccount to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/casa-accounts/{id}")
    public ResponseEntity<Void> deleteCasaAccount(@PathVariable Long id) {
        log.debug("REST request to delete CasaAccount : {}", id);
        casaAccountRepository.deleteById(id);
        return ResponseEntity
            .noContent()
            .headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id.toString()))
            .build();
    }
}
