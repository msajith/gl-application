package com.iconnect101.glapp.web.rest;

import com.iconnect101.glapp.domain.WalletTopupTransaction;
import com.iconnect101.glapp.repository.WalletTopupTransactionRepository;
import com.iconnect101.glapp.web.rest.errors.BadRequestAlertException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;
import tech.jhipster.web.util.HeaderUtil;
import tech.jhipster.web.util.ResponseUtil;

/**
 * REST controller for managing {@link com.iconnect101.glapp.domain.WalletTopupTransaction}.
 */
@RestController
@RequestMapping("/api")
@Transactional
public class WalletTopupTransactionResource {

    private final Logger log = LoggerFactory.getLogger(WalletTopupTransactionResource.class);

    private static final String ENTITY_NAME = "walletTopupTransaction";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final WalletTopupTransactionRepository walletTopupTransactionRepository;

    public WalletTopupTransactionResource(WalletTopupTransactionRepository walletTopupTransactionRepository) {
        this.walletTopupTransactionRepository = walletTopupTransactionRepository;
    }

    /**
     * {@code POST  /wallet-topup-transactions} : Create a new walletTopupTransaction.
     *
     * @param walletTopupTransaction the walletTopupTransaction to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new walletTopupTransaction, or with status {@code 400 (Bad Request)} if the walletTopupTransaction has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/wallet-topup-transactions")
    public ResponseEntity<WalletTopupTransaction> createWalletTopupTransaction(@RequestBody WalletTopupTransaction walletTopupTransaction)
        throws URISyntaxException {
        log.debug("REST request to save WalletTopupTransaction : {}", walletTopupTransaction);
        if (walletTopupTransaction.getId() != null) {
            throw new BadRequestAlertException("A new walletTopupTransaction cannot already have an ID", ENTITY_NAME, "idexists");
        }
        WalletTopupTransaction result = walletTopupTransactionRepository.save(walletTopupTransaction);
        return ResponseEntity
            .created(new URI("/api/wallet-topup-transactions/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /wallet-topup-transactions/:id} : Updates an existing walletTopupTransaction.
     *
     * @param id the id of the walletTopupTransaction to save.
     * @param walletTopupTransaction the walletTopupTransaction to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated walletTopupTransaction,
     * or with status {@code 400 (Bad Request)} if the walletTopupTransaction is not valid,
     * or with status {@code 500 (Internal Server Error)} if the walletTopupTransaction couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/wallet-topup-transactions/{id}")
    public ResponseEntity<WalletTopupTransaction> updateWalletTopupTransaction(
        @PathVariable(value = "id", required = false) final Long id,
        @RequestBody WalletTopupTransaction walletTopupTransaction
    ) throws URISyntaxException {
        log.debug("REST request to update WalletTopupTransaction : {}, {}", id, walletTopupTransaction);
        if (walletTopupTransaction.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, walletTopupTransaction.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!walletTopupTransactionRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        WalletTopupTransaction result = walletTopupTransactionRepository.save(walletTopupTransaction);
        return ResponseEntity
            .ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, walletTopupTransaction.getId().toString()))
            .body(result);
    }

    /**
     * {@code PATCH  /wallet-topup-transactions/:id} : Partial updates given fields of an existing walletTopupTransaction, field will ignore if it is null
     *
     * @param id the id of the walletTopupTransaction to save.
     * @param walletTopupTransaction the walletTopupTransaction to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated walletTopupTransaction,
     * or with status {@code 400 (Bad Request)} if the walletTopupTransaction is not valid,
     * or with status {@code 404 (Not Found)} if the walletTopupTransaction is not found,
     * or with status {@code 500 (Internal Server Error)} if the walletTopupTransaction couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PatchMapping(value = "/wallet-topup-transactions/{id}", consumes = { "application/json", "application/merge-patch+json" })
    public ResponseEntity<WalletTopupTransaction> partialUpdateWalletTopupTransaction(
        @PathVariable(value = "id", required = false) final Long id,
        @RequestBody WalletTopupTransaction walletTopupTransaction
    ) throws URISyntaxException {
        log.debug("REST request to partial update WalletTopupTransaction partially : {}, {}", id, walletTopupTransaction);
        if (walletTopupTransaction.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, walletTopupTransaction.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!walletTopupTransactionRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        Optional<WalletTopupTransaction> result = walletTopupTransactionRepository
            .findById(walletTopupTransaction.getId())
            .map(existingWalletTopupTransaction -> {
                if (walletTopupTransaction.getTxnRefNo() != null) {
                    existingWalletTopupTransaction.setTxnRefNo(walletTopupTransaction.getTxnRefNo());
                }
                if (walletTopupTransaction.getAccountNo() != null) {
                    existingWalletTopupTransaction.setAccountNo(walletTopupTransaction.getAccountNo());
                }
                if (walletTopupTransaction.getTxnDate() != null) {
                    existingWalletTopupTransaction.setTxnDate(walletTopupTransaction.getTxnDate());
                }
                if (walletTopupTransaction.getTxnDateInUTC() != null) {
                    existingWalletTopupTransaction.setTxnDateInUTC(walletTopupTransaction.getTxnDateInUTC());
                }
                if (walletTopupTransaction.getTxnDateInLocal() != null) {
                    existingWalletTopupTransaction.setTxnDateInLocal(walletTopupTransaction.getTxnDateInLocal());
                }
                if (walletTopupTransaction.getTxnDateForSettlement() != null) {
                    existingWalletTopupTransaction.setTxnDateForSettlement(walletTopupTransaction.getTxnDateForSettlement());
                }
                if (walletTopupTransaction.getValueDate() != null) {
                    existingWalletTopupTransaction.setValueDate(walletTopupTransaction.getValueDate());
                }
                if (walletTopupTransaction.getValueDateInUTC() != null) {
                    existingWalletTopupTransaction.setValueDateInUTC(walletTopupTransaction.getValueDateInUTC());
                }
                if (walletTopupTransaction.getValueDateInLocal() != null) {
                    existingWalletTopupTransaction.setValueDateInLocal(walletTopupTransaction.getValueDateInLocal());
                }
                if (walletTopupTransaction.getValueDateForSettlement() != null) {
                    existingWalletTopupTransaction.setValueDateForSettlement(walletTopupTransaction.getValueDateForSettlement());
                }
                if (walletTopupTransaction.getTxnAmount() != null) {
                    existingWalletTopupTransaction.setTxnAmount(walletTopupTransaction.getTxnAmount());
                }
                if (walletTopupTransaction.getInstructedAmount() != null) {
                    existingWalletTopupTransaction.setInstructedAmount(walletTopupTransaction.getInstructedAmount());
                }
                if (walletTopupTransaction.getAmountInAccountCurrency() != null) {
                    existingWalletTopupTransaction.setAmountInAccountCurrency(walletTopupTransaction.getAmountInAccountCurrency());
                }
                if (walletTopupTransaction.getAmountInLocalCurrency() != null) {
                    existingWalletTopupTransaction.setAmountInLocalCurrency(walletTopupTransaction.getAmountInLocalCurrency());
                }
                if (walletTopupTransaction.getChargeInAccountCurrency() != null) {
                    existingWalletTopupTransaction.setChargeInAccountCurrency(walletTopupTransaction.getChargeInAccountCurrency());
                }
                if (walletTopupTransaction.getChargeInLocalCurrency() != null) {
                    existingWalletTopupTransaction.setChargeInLocalCurrency(walletTopupTransaction.getChargeInLocalCurrency());
                }
                if (walletTopupTransaction.getFundingSource() != null) {
                    existingWalletTopupTransaction.setFundingSource(walletTopupTransaction.getFundingSource());
                }
                if (walletTopupTransaction.getMerchantID() != null) {
                    existingWalletTopupTransaction.setMerchantID(walletTopupTransaction.getMerchantID());
                }
                if (walletTopupTransaction.getMerchantInfo() != null) {
                    existingWalletTopupTransaction.setMerchantInfo(walletTopupTransaction.getMerchantInfo());
                }
                if (walletTopupTransaction.getBankCode() != null) {
                    existingWalletTopupTransaction.setBankCode(walletTopupTransaction.getBankCode());
                }
                if (walletTopupTransaction.getExternalRefNo() != null) {
                    existingWalletTopupTransaction.setExternalRefNo(walletTopupTransaction.getExternalRefNo());
                }
                if (walletTopupTransaction.getTxnDescription() != null) {
                    existingWalletTopupTransaction.setTxnDescription(walletTopupTransaction.getTxnDescription());
                }
                if (walletTopupTransaction.getTxnInternalDescription() != null) {
                    existingWalletTopupTransaction.setTxnInternalDescription(walletTopupTransaction.getTxnInternalDescription());
                }
                if (walletTopupTransaction.getTxnAddnlInfo() != null) {
                    existingWalletTopupTransaction.setTxnAddnlInfo(walletTopupTransaction.getTxnAddnlInfo());
                }
                if (walletTopupTransaction.getCreatedOn() != null) {
                    existingWalletTopupTransaction.setCreatedOn(walletTopupTransaction.getCreatedOn());
                }
                if (walletTopupTransaction.getCreatedBy() != null) {
                    existingWalletTopupTransaction.setCreatedBy(walletTopupTransaction.getCreatedBy());
                }
                if (walletTopupTransaction.getTxnStatus() != null) {
                    existingWalletTopupTransaction.setTxnStatus(walletTopupTransaction.getTxnStatus());
                }
                if (walletTopupTransaction.getTxnDebitAccountNo() != null) {
                    existingWalletTopupTransaction.setTxnDebitAccountNo(walletTopupTransaction.getTxnDebitAccountNo());
                }
                if (walletTopupTransaction.getTxnCreditAccountNo() != null) {
                    existingWalletTopupTransaction.setTxnCreditAccountNo(walletTopupTransaction.getTxnCreditAccountNo());
                }
                if (walletTopupTransaction.getChargeDebitAccountNo() != null) {
                    existingWalletTopupTransaction.setChargeDebitAccountNo(walletTopupTransaction.getChargeDebitAccountNo());
                }
                if (walletTopupTransaction.getChargeCreditAccountNo() != null) {
                    existingWalletTopupTransaction.setChargeCreditAccountNo(walletTopupTransaction.getChargeCreditAccountNo());
                }

                return existingWalletTopupTransaction;
            })
            .map(walletTopupTransactionRepository::save);

        return ResponseUtil.wrapOrNotFound(
            result,
            HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, walletTopupTransaction.getId().toString())
        );
    }

    /**
     * {@code GET  /wallet-topup-transactions} : get all the walletTopupTransactions.
     *
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of walletTopupTransactions in body.
     */
    @GetMapping("/wallet-topup-transactions")
    public List<WalletTopupTransaction> getAllWalletTopupTransactions() {
        log.debug("REST request to get all WalletTopupTransactions");
        return walletTopupTransactionRepository.findAll();
    }

    /**
     * {@code GET  /wallet-topup-transactions/:id} : get the "id" walletTopupTransaction.
     *
     * @param id the id of the walletTopupTransaction to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the walletTopupTransaction, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/wallet-topup-transactions/{id}")
    public ResponseEntity<WalletTopupTransaction> getWalletTopupTransaction(@PathVariable Long id) {
        log.debug("REST request to get WalletTopupTransaction : {}", id);
        Optional<WalletTopupTransaction> walletTopupTransaction = walletTopupTransactionRepository.findById(id);
        return ResponseUtil.wrapOrNotFound(walletTopupTransaction);
    }

    /**
     * {@code DELETE  /wallet-topup-transactions/:id} : delete the "id" walletTopupTransaction.
     *
     * @param id the id of the walletTopupTransaction to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/wallet-topup-transactions/{id}")
    public ResponseEntity<Void> deleteWalletTopupTransaction(@PathVariable Long id) {
        log.debug("REST request to delete WalletTopupTransaction : {}", id);
        walletTopupTransactionRepository.deleteById(id);
        return ResponseEntity
            .noContent()
            .headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id.toString()))
            .build();
    }
}
