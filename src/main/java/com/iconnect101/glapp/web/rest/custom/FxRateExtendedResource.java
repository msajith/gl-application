package com.iconnect101.glapp.web.rest.custom;

import com.iconnect101.glapp.domain.FxRate;
import com.iconnect101.glapp.repository.FxRateRepository;
import com.iconnect101.glapp.repository.custom.FxRateExtendedRepository;
import com.iconnect101.glapp.web.rest.FxRateResource;
import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import tech.jhipster.web.util.ResponseUtil;

@RestController
@RequestMapping("/api/v1")
public class FxRateExtendedResource extends FxRateResource {

    private final Logger log = LoggerFactory.getLogger(FxRateExtendedResource.class);

    private static final String ENTITY_NAME = "fxRate";

    private final FxRateExtendedRepository fxRateExtendedRepository;

    public FxRateExtendedResource(FxRateRepository fxRateRepository, FxRateExtendedRepository fxRateExtendedRepository) {
        super(fxRateRepository);
        this.fxRateExtendedRepository = fxRateExtendedRepository;
        // TODO Auto-generated constructor stub
    }

    @GetMapping("/fx-rates/{baseccy}/{quoteccy}")
    public ResponseEntity<FxRate> getFxRate(@PathVariable String baseccy, @PathVariable String quoteccy) {
        log.debug("REST request to get FxRate : {}", baseccy);
        Optional<FxRate> fxRate = fxRateExtendedRepository.getRate(baseccy, quoteccy);
        return ResponseUtil.wrapOrNotFound(fxRate);
    }
}
