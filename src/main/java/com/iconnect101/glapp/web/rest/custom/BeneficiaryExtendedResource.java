package com.iconnect101.glapp.web.rest.custom;

import com.iconnect101.glapp.domain.Beneficiary;
import com.iconnect101.glapp.domain.CasaAccount;
import com.iconnect101.glapp.repository.BeneficiaryRepository;
import com.iconnect101.glapp.repository.custom.BeneficiaryExtendedRepository;
import com.iconnect101.glapp.web.rest.BeneficiaryResource;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/v1")
public class BeneficiaryExtendedResource extends BeneficiaryResource {

    private final Logger log = LoggerFactory.getLogger(BeneficiaryExtendedResource.class);

    private static final String ENTITY_NAME = "beneficiary";

    private final BeneficiaryExtendedRepository beneficiaryExtendedRepository;

    public BeneficiaryExtendedResource(
        BeneficiaryRepository beneficiaryRepository,
        BeneficiaryExtendedRepository beneficiaryExtendedRepository
    ) {
        super(beneficiaryRepository);
        // TODO Auto-generated constructor stub
        this.beneficiaryExtendedRepository = beneficiaryExtendedRepository;
    }

    @GetMapping("/beneficiaries/customer/{id}")
    public List<Beneficiary> getBeneficiaries(@PathVariable Long id) {
        log.debug("REST request to get CasaAccount : {}", id);
        List<Beneficiary> beneficiaries = beneficiaryExtendedRepository.findByCustomerId(id);
        return beneficiaries;
    }
}
