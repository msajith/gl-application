package com.iconnect101.glapp.web.rest;

import com.iconnect101.glapp.domain.CustomerSegment;
import com.iconnect101.glapp.repository.CustomerSegmentRepository;
import com.iconnect101.glapp.web.rest.errors.BadRequestAlertException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;
import tech.jhipster.web.util.HeaderUtil;
import tech.jhipster.web.util.ResponseUtil;

/**
 * REST controller for managing {@link com.iconnect101.glapp.domain.CustomerSegment}.
 */
@RestController
@RequestMapping("/api")
@Transactional
public class CustomerSegmentResource {

    private final Logger log = LoggerFactory.getLogger(CustomerSegmentResource.class);

    private static final String ENTITY_NAME = "customerSegment";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final CustomerSegmentRepository customerSegmentRepository;

    public CustomerSegmentResource(CustomerSegmentRepository customerSegmentRepository) {
        this.customerSegmentRepository = customerSegmentRepository;
    }

    /**
     * {@code POST  /customer-segments} : Create a new customerSegment.
     *
     * @param customerSegment the customerSegment to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new customerSegment, or with status {@code 400 (Bad Request)} if the customerSegment has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/customer-segments")
    public ResponseEntity<CustomerSegment> createCustomerSegment(@RequestBody CustomerSegment customerSegment) throws URISyntaxException {
        log.debug("REST request to save CustomerSegment : {}", customerSegment);
        if (customerSegment.getId() != null) {
            throw new BadRequestAlertException("A new customerSegment cannot already have an ID", ENTITY_NAME, "idexists");
        }
        CustomerSegment result = customerSegmentRepository.save(customerSegment);
        return ResponseEntity
            .created(new URI("/api/customer-segments/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /customer-segments/:id} : Updates an existing customerSegment.
     *
     * @param id the id of the customerSegment to save.
     * @param customerSegment the customerSegment to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated customerSegment,
     * or with status {@code 400 (Bad Request)} if the customerSegment is not valid,
     * or with status {@code 500 (Internal Server Error)} if the customerSegment couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/customer-segments/{id}")
    public ResponseEntity<CustomerSegment> updateCustomerSegment(
        @PathVariable(value = "id", required = false) final Long id,
        @RequestBody CustomerSegment customerSegment
    ) throws URISyntaxException {
        log.debug("REST request to update CustomerSegment : {}, {}", id, customerSegment);
        if (customerSegment.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, customerSegment.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!customerSegmentRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        CustomerSegment result = customerSegmentRepository.save(customerSegment);
        return ResponseEntity
            .ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, customerSegment.getId().toString()))
            .body(result);
    }

    /**
     * {@code PATCH  /customer-segments/:id} : Partial updates given fields of an existing customerSegment, field will ignore if it is null
     *
     * @param id the id of the customerSegment to save.
     * @param customerSegment the customerSegment to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated customerSegment,
     * or with status {@code 400 (Bad Request)} if the customerSegment is not valid,
     * or with status {@code 404 (Not Found)} if the customerSegment is not found,
     * or with status {@code 500 (Internal Server Error)} if the customerSegment couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PatchMapping(value = "/customer-segments/{id}", consumes = { "application/json", "application/merge-patch+json" })
    public ResponseEntity<CustomerSegment> partialUpdateCustomerSegment(
        @PathVariable(value = "id", required = false) final Long id,
        @RequestBody CustomerSegment customerSegment
    ) throws URISyntaxException {
        log.debug("REST request to partial update CustomerSegment partially : {}, {}", id, customerSegment);
        if (customerSegment.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, customerSegment.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!customerSegmentRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        Optional<CustomerSegment> result = customerSegmentRepository
            .findById(customerSegment.getId())
            .map(existingCustomerSegment -> {
                if (customerSegment.getName() != null) {
                    existingCustomerSegment.setName(customerSegment.getName());
                }
                if (customerSegment.getCode() != null) {
                    existingCustomerSegment.setCode(customerSegment.getCode());
                }

                return existingCustomerSegment;
            })
            .map(customerSegmentRepository::save);

        return ResponseUtil.wrapOrNotFound(
            result,
            HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, customerSegment.getId().toString())
        );
    }

    /**
     * {@code GET  /customer-segments} : get all the customerSegments.
     *
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of customerSegments in body.
     */
    @GetMapping("/customer-segments")
    public List<CustomerSegment> getAllCustomerSegments() {
        log.debug("REST request to get all CustomerSegments");
        return customerSegmentRepository.findAll();
    }

    /**
     * {@code GET  /customer-segments/:id} : get the "id" customerSegment.
     *
     * @param id the id of the customerSegment to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the customerSegment, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/customer-segments/{id}")
    public ResponseEntity<CustomerSegment> getCustomerSegment(@PathVariable Long id) {
        log.debug("REST request to get CustomerSegment : {}", id);
        Optional<CustomerSegment> customerSegment = customerSegmentRepository.findById(id);
        return ResponseUtil.wrapOrNotFound(customerSegment);
    }

    /**
     * {@code DELETE  /customer-segments/:id} : delete the "id" customerSegment.
     *
     * @param id the id of the customerSegment to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/customer-segments/{id}")
    public ResponseEntity<Void> deleteCustomerSegment(@PathVariable Long id) {
        log.debug("REST request to delete CustomerSegment : {}", id);
        customerSegmentRepository.deleteById(id);
        return ResponseEntity
            .noContent()
            .headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id.toString()))
            .build();
    }
}
