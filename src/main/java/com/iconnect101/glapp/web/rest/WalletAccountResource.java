package com.iconnect101.glapp.web.rest;

import com.iconnect101.glapp.domain.WalletAccount;
import com.iconnect101.glapp.repository.WalletAccountRepository;
import com.iconnect101.glapp.web.rest.errors.BadRequestAlertException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;
import tech.jhipster.web.util.HeaderUtil;
import tech.jhipster.web.util.ResponseUtil;

/**
 * REST controller for managing {@link com.iconnect101.glapp.domain.WalletAccount}.
 */
@RestController
@RequestMapping("/api")
@Transactional
public class WalletAccountResource {

    private final Logger log = LoggerFactory.getLogger(WalletAccountResource.class);

    private static final String ENTITY_NAME = "walletAccount";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final WalletAccountRepository walletAccountRepository;

    public WalletAccountResource(WalletAccountRepository walletAccountRepository) {
        this.walletAccountRepository = walletAccountRepository;
    }

    /**
     * {@code POST  /wallet-accounts} : Create a new walletAccount.
     *
     * @param walletAccount the walletAccount to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new walletAccount, or with status {@code 400 (Bad Request)} if the walletAccount has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/wallet-accounts")
    public ResponseEntity<WalletAccount> createWalletAccount(@Valid @RequestBody WalletAccount walletAccount) throws URISyntaxException {
        log.debug("REST request to save WalletAccount : {}", walletAccount);
        if (walletAccount.getId() != null) {
            throw new BadRequestAlertException("A new walletAccount cannot already have an ID", ENTITY_NAME, "idexists");
        }
        WalletAccount result = walletAccountRepository.save(walletAccount);
        return ResponseEntity
            .created(new URI("/api/wallet-accounts/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /wallet-accounts/:id} : Updates an existing walletAccount.
     *
     * @param id the id of the walletAccount to save.
     * @param walletAccount the walletAccount to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated walletAccount,
     * or with status {@code 400 (Bad Request)} if the walletAccount is not valid,
     * or with status {@code 500 (Internal Server Error)} if the walletAccount couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/wallet-accounts/{id}")
    public ResponseEntity<WalletAccount> updateWalletAccount(
        @PathVariable(value = "id", required = false) final Long id,
        @Valid @RequestBody WalletAccount walletAccount
    ) throws URISyntaxException {
        log.debug("REST request to update WalletAccount : {}, {}", id, walletAccount);
        if (walletAccount.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, walletAccount.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!walletAccountRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        WalletAccount result = walletAccountRepository.save(walletAccount);
        return ResponseEntity
            .ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, walletAccount.getId().toString()))
            .body(result);
    }

    /**
     * {@code PATCH  /wallet-accounts/:id} : Partial updates given fields of an existing walletAccount, field will ignore if it is null
     *
     * @param id the id of the walletAccount to save.
     * @param walletAccount the walletAccount to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated walletAccount,
     * or with status {@code 400 (Bad Request)} if the walletAccount is not valid,
     * or with status {@code 404 (Not Found)} if the walletAccount is not found,
     * or with status {@code 500 (Internal Server Error)} if the walletAccount couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PatchMapping(value = "/wallet-accounts/{id}", consumes = { "application/json", "application/merge-patch+json" })
    public ResponseEntity<WalletAccount> partialUpdateWalletAccount(
        @PathVariable(value = "id", required = false) final Long id,
        @NotNull @RequestBody WalletAccount walletAccount
    ) throws URISyntaxException {
        log.debug("REST request to partial update WalletAccount partially : {}, {}", id, walletAccount);
        if (walletAccount.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, walletAccount.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!walletAccountRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        Optional<WalletAccount> result = walletAccountRepository
            .findById(walletAccount.getId())
            .map(existingWalletAccount -> {
                if (walletAccount.getAccountNo() != null) {
                    existingWalletAccount.setAccountNo(walletAccount.getAccountNo());
                }
                if (walletAccount.getCif() != null) {
                    existingWalletAccount.setCif(walletAccount.getCif());
                }
                if (walletAccount.getAccountName() != null) {
                    existingWalletAccount.setAccountName(walletAccount.getAccountName());
                }
                if (walletAccount.getDate() != null) {
                    existingWalletAccount.setDate(walletAccount.getDate());
                }
                if (walletAccount.getClosingBalance() != null) {
                    existingWalletAccount.setClosingBalance(walletAccount.getClosingBalance());
                }
                if (walletAccount.getOpeningBalance() != null) {
                    existingWalletAccount.setOpeningBalance(walletAccount.getOpeningBalance());
                }
                if (walletAccount.getTotalDebit() != null) {
                    existingWalletAccount.setTotalDebit(walletAccount.getTotalDebit());
                }
                if (walletAccount.getTotalCredit() != null) {
                    existingWalletAccount.setTotalCredit(walletAccount.getTotalCredit());
                }
                if (walletAccount.getAvailableBalance() != null) {
                    existingWalletAccount.setAvailableBalance(walletAccount.getAvailableBalance());
                }
                if (walletAccount.getCurrentBalance() != null) {
                    existingWalletAccount.setCurrentBalance(walletAccount.getCurrentBalance());
                }
                if (walletAccount.getCreatedOn() != null) {
                    existingWalletAccount.setCreatedOn(walletAccount.getCreatedOn());
                }
                if (walletAccount.getCreatedBy() != null) {
                    existingWalletAccount.setCreatedBy(walletAccount.getCreatedBy());
                }
                if (walletAccount.getAccountStatus() != null) {
                    existingWalletAccount.setAccountStatus(walletAccount.getAccountStatus());
                }

                return existingWalletAccount;
            })
            .map(walletAccountRepository::save);

        return ResponseUtil.wrapOrNotFound(
            result,
            HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, walletAccount.getId().toString())
        );
    }

    /**
     * {@code GET  /wallet-accounts} : get all the walletAccounts.
     *
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of walletAccounts in body.
     */
    @GetMapping("/wallet-accounts")
    public List<WalletAccount> getAllWalletAccounts() {
        log.debug("REST request to get all WalletAccounts");
        return walletAccountRepository.findAll();
    }

    /**
     * {@code GET  /wallet-accounts/:id} : get the "id" walletAccount.
     *
     * @param id the id of the walletAccount to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the walletAccount, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/wallet-accounts/{id}")
    public ResponseEntity<WalletAccount> getWalletAccount(@PathVariable Long id) {
        log.debug("REST request to get WalletAccount : {}", id);
        Optional<WalletAccount> walletAccount = walletAccountRepository.findById(id);
        return ResponseUtil.wrapOrNotFound(walletAccount);
    }

    /**
     * {@code DELETE  /wallet-accounts/:id} : delete the "id" walletAccount.
     *
     * @param id the id of the walletAccount to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/wallet-accounts/{id}")
    public ResponseEntity<Void> deleteWalletAccount(@PathVariable Long id) {
        log.debug("REST request to delete WalletAccount : {}", id);
        walletAccountRepository.deleteById(id);
        return ResponseEntity
            .noContent()
            .headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id.toString()))
            .build();
    }
}
