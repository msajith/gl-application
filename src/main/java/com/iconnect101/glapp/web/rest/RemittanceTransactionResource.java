package com.iconnect101.glapp.web.rest;

import com.iconnect101.glapp.domain.RemittanceTransaction;
import com.iconnect101.glapp.repository.RemittanceTransactionRepository;
import com.iconnect101.glapp.web.rest.errors.BadRequestAlertException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;
import tech.jhipster.web.util.HeaderUtil;
import tech.jhipster.web.util.ResponseUtil;

/**
 * REST controller for managing {@link com.iconnect101.glapp.domain.RemittanceTransaction}.
 */
@RestController
@RequestMapping("/api")
@Transactional
public class RemittanceTransactionResource {

    private final Logger log = LoggerFactory.getLogger(RemittanceTransactionResource.class);

    private static final String ENTITY_NAME = "remittanceTransaction";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final RemittanceTransactionRepository remittanceTransactionRepository;

    public RemittanceTransactionResource(RemittanceTransactionRepository remittanceTransactionRepository) {
        this.remittanceTransactionRepository = remittanceTransactionRepository;
    }

    /**
     * {@code POST  /remittance-transactions} : Create a new remittanceTransaction.
     *
     * @param remittanceTransaction the remittanceTransaction to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new remittanceTransaction, or with status {@code 400 (Bad Request)} if the remittanceTransaction has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/remittance-transactions")
    public ResponseEntity<RemittanceTransaction> createRemittanceTransaction(@RequestBody RemittanceTransaction remittanceTransaction)
        throws URISyntaxException {
        log.debug("REST request to save RemittanceTransaction : {}", remittanceTransaction);
        if (remittanceTransaction.getId() != null) {
            throw new BadRequestAlertException("A new remittanceTransaction cannot already have an ID", ENTITY_NAME, "idexists");
        }
        RemittanceTransaction result = remittanceTransactionRepository.save(remittanceTransaction);
        return ResponseEntity
            .created(new URI("/api/remittance-transactions/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /remittance-transactions/:id} : Updates an existing remittanceTransaction.
     *
     * @param id the id of the remittanceTransaction to save.
     * @param remittanceTransaction the remittanceTransaction to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated remittanceTransaction,
     * or with status {@code 400 (Bad Request)} if the remittanceTransaction is not valid,
     * or with status {@code 500 (Internal Server Error)} if the remittanceTransaction couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/remittance-transactions/{id}")
    public ResponseEntity<RemittanceTransaction> updateRemittanceTransaction(
        @PathVariable(value = "id", required = false) final Long id,
        @RequestBody RemittanceTransaction remittanceTransaction
    ) throws URISyntaxException {
        log.debug("REST request to update RemittanceTransaction : {}, {}", id, remittanceTransaction);
        if (remittanceTransaction.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, remittanceTransaction.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!remittanceTransactionRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        RemittanceTransaction result = remittanceTransactionRepository.save(remittanceTransaction);
        return ResponseEntity
            .ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, remittanceTransaction.getId().toString()))
            .body(result);
    }

    /**
     * {@code PATCH  /remittance-transactions/:id} : Partial updates given fields of an existing remittanceTransaction, field will ignore if it is null
     *
     * @param id the id of the remittanceTransaction to save.
     * @param remittanceTransaction the remittanceTransaction to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated remittanceTransaction,
     * or with status {@code 400 (Bad Request)} if the remittanceTransaction is not valid,
     * or with status {@code 404 (Not Found)} if the remittanceTransaction is not found,
     * or with status {@code 500 (Internal Server Error)} if the remittanceTransaction couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PatchMapping(value = "/remittance-transactions/{id}", consumes = { "application/json", "application/merge-patch+json" })
    public ResponseEntity<RemittanceTransaction> partialUpdateRemittanceTransaction(
        @PathVariable(value = "id", required = false) final Long id,
        @RequestBody RemittanceTransaction remittanceTransaction
    ) throws URISyntaxException {
        log.debug("REST request to partial update RemittanceTransaction partially : {}, {}", id, remittanceTransaction);
        if (remittanceTransaction.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, remittanceTransaction.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!remittanceTransactionRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        Optional<RemittanceTransaction> result = remittanceTransactionRepository
            .findById(remittanceTransaction.getId())
            .map(existingRemittanceTransaction -> {
                if (remittanceTransaction.getTxnRefNo() != null) {
                    existingRemittanceTransaction.setTxnRefNo(remittanceTransaction.getTxnRefNo());
                }
                if (remittanceTransaction.getAccountNo() != null) {
                    existingRemittanceTransaction.setAccountNo(remittanceTransaction.getAccountNo());
                }
                if (remittanceTransaction.getTxnDate() != null) {
                    existingRemittanceTransaction.setTxnDate(remittanceTransaction.getTxnDate());
                }
                if (remittanceTransaction.getTxnDateInUTC() != null) {
                    existingRemittanceTransaction.setTxnDateInUTC(remittanceTransaction.getTxnDateInUTC());
                }
                if (remittanceTransaction.getTxnDateInLocal() != null) {
                    existingRemittanceTransaction.setTxnDateInLocal(remittanceTransaction.getTxnDateInLocal());
                }
                if (remittanceTransaction.getTxnDateForSettlement() != null) {
                    existingRemittanceTransaction.setTxnDateForSettlement(remittanceTransaction.getTxnDateForSettlement());
                }
                if (remittanceTransaction.getValueDate() != null) {
                    existingRemittanceTransaction.setValueDate(remittanceTransaction.getValueDate());
                }
                if (remittanceTransaction.getValueDateInUTC() != null) {
                    existingRemittanceTransaction.setValueDateInUTC(remittanceTransaction.getValueDateInUTC());
                }
                if (remittanceTransaction.getValueDateInLocal() != null) {
                    existingRemittanceTransaction.setValueDateInLocal(remittanceTransaction.getValueDateInLocal());
                }
                if (remittanceTransaction.getValueDateForSettlement() != null) {
                    existingRemittanceTransaction.setValueDateForSettlement(remittanceTransaction.getValueDateForSettlement());
                }
                if (remittanceTransaction.getBeneficiaryAccountNo() != null) {
                    existingRemittanceTransaction.setBeneficiaryAccountNo(remittanceTransaction.getBeneficiaryAccountNo());
                }
                if (remittanceTransaction.getBeneficiaryName() != null) {
                    existingRemittanceTransaction.setBeneficiaryName(remittanceTransaction.getBeneficiaryName());
                }
                if (remittanceTransaction.getBeneficiarybankSwiftCode() != null) {
                    existingRemittanceTransaction.setBeneficiarybankSwiftCode(remittanceTransaction.getBeneficiarybankSwiftCode());
                }
                if (remittanceTransaction.getIntermediaryBankSwiftCode() != null) {
                    existingRemittanceTransaction.setIntermediaryBankSwiftCode(remittanceTransaction.getIntermediaryBankSwiftCode());
                }
                if (remittanceTransaction.getBeneficiaryBankName() != null) {
                    existingRemittanceTransaction.setBeneficiaryBankName(remittanceTransaction.getBeneficiaryBankName());
                }
                if (remittanceTransaction.getBeneficiaryBankBranchName() != null) {
                    existingRemittanceTransaction.setBeneficiaryBankBranchName(remittanceTransaction.getBeneficiaryBankBranchName());
                }
                if (remittanceTransaction.getBeneficiaryAddress1() != null) {
                    existingRemittanceTransaction.setBeneficiaryAddress1(remittanceTransaction.getBeneficiaryAddress1());
                }
                if (remittanceTransaction.getBeneficiaryAddress2() != null) {
                    existingRemittanceTransaction.setBeneficiaryAddress2(remittanceTransaction.getBeneficiaryAddress2());
                }
                if (remittanceTransaction.getBeneficiaryAddress3() != null) {
                    existingRemittanceTransaction.setBeneficiaryAddress3(remittanceTransaction.getBeneficiaryAddress3());
                }
                if (remittanceTransaction.getBeneficiaryState() != null) {
                    existingRemittanceTransaction.setBeneficiaryState(remittanceTransaction.getBeneficiaryState());
                }
                if (remittanceTransaction.getBeneficiaryPinCode() != null) {
                    existingRemittanceTransaction.setBeneficiaryPinCode(remittanceTransaction.getBeneficiaryPinCode());
                }
                if (remittanceTransaction.getTxnAmount() != null) {
                    existingRemittanceTransaction.setTxnAmount(remittanceTransaction.getTxnAmount());
                }
                if (remittanceTransaction.getAmountInAccountCurrency() != null) {
                    existingRemittanceTransaction.setAmountInAccountCurrency(remittanceTransaction.getAmountInAccountCurrency());
                }
                if (remittanceTransaction.getAmountInLocalCurrency() != null) {
                    existingRemittanceTransaction.setAmountInLocalCurrency(remittanceTransaction.getAmountInLocalCurrency());
                }
                if (remittanceTransaction.getChargeInAccountCurrency() != null) {
                    existingRemittanceTransaction.setChargeInAccountCurrency(remittanceTransaction.getChargeInAccountCurrency());
                }
                if (remittanceTransaction.getChargeInLocalCurrency() != null) {
                    existingRemittanceTransaction.setChargeInLocalCurrency(remittanceTransaction.getChargeInLocalCurrency());
                }
                if (remittanceTransaction.getFundingSource() != null) {
                    existingRemittanceTransaction.setFundingSource(remittanceTransaction.getFundingSource());
                }
                if (remittanceTransaction.getRate() != null) {
                    existingRemittanceTransaction.setRate(remittanceTransaction.getRate());
                }
                if (remittanceTransaction.getInstructedAmount() != null) {
                    existingRemittanceTransaction.setInstructedAmount(remittanceTransaction.getInstructedAmount());
                }
                if (remittanceTransaction.getInstructedAmountCurrency() != null) {
                    existingRemittanceTransaction.setInstructedAmountCurrency(remittanceTransaction.getInstructedAmountCurrency());
                }
                if (remittanceTransaction.getMerchantID() != null) {
                    existingRemittanceTransaction.setMerchantID(remittanceTransaction.getMerchantID());
                }
                if (remittanceTransaction.getMerchantInfo() != null) {
                    existingRemittanceTransaction.setMerchantInfo(remittanceTransaction.getMerchantInfo());
                }
                if (remittanceTransaction.getExternalRefNo() != null) {
                    existingRemittanceTransaction.setExternalRefNo(remittanceTransaction.getExternalRefNo());
                }
                if (remittanceTransaction.getTxnDescription() != null) {
                    existingRemittanceTransaction.setTxnDescription(remittanceTransaction.getTxnDescription());
                }
                if (remittanceTransaction.getTxnInternalDescription() != null) {
                    existingRemittanceTransaction.setTxnInternalDescription(remittanceTransaction.getTxnInternalDescription());
                }
                if (remittanceTransaction.getTxnAddnlInfo() != null) {
                    existingRemittanceTransaction.setTxnAddnlInfo(remittanceTransaction.getTxnAddnlInfo());
                }
                if (remittanceTransaction.getCreatedOn() != null) {
                    existingRemittanceTransaction.setCreatedOn(remittanceTransaction.getCreatedOn());
                }
                if (remittanceTransaction.getCreatedBy() != null) {
                    existingRemittanceTransaction.setCreatedBy(remittanceTransaction.getCreatedBy());
                }
                if (remittanceTransaction.getTxnStatus() != null) {
                    existingRemittanceTransaction.setTxnStatus(remittanceTransaction.getTxnStatus());
                }
                if (remittanceTransaction.getTxnDebitAccountNo() != null) {
                    existingRemittanceTransaction.setTxnDebitAccountNo(remittanceTransaction.getTxnDebitAccountNo());
                }
                if (remittanceTransaction.getTxnCreditAccountNo() != null) {
                    existingRemittanceTransaction.setTxnCreditAccountNo(remittanceTransaction.getTxnCreditAccountNo());
                }
                if (remittanceTransaction.getChargeDebitAccountNo() != null) {
                    existingRemittanceTransaction.setChargeDebitAccountNo(remittanceTransaction.getChargeDebitAccountNo());
                }
                if (remittanceTransaction.getChargeCreditAccountNo() != null) {
                    existingRemittanceTransaction.setChargeCreditAccountNo(remittanceTransaction.getChargeCreditAccountNo());
                }

                return existingRemittanceTransaction;
            })
            .map(remittanceTransactionRepository::save);

        return ResponseUtil.wrapOrNotFound(
            result,
            HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, remittanceTransaction.getId().toString())
        );
    }

    /**
     * {@code GET  /remittance-transactions} : get all the remittanceTransactions.
     *
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of remittanceTransactions in body.
     */
    @GetMapping("/remittance-transactions")
    public List<RemittanceTransaction> getAllRemittanceTransactions() {
        log.debug("REST request to get all RemittanceTransactions");
        return remittanceTransactionRepository.findAll();
    }

    /**
     * {@code GET  /remittance-transactions/:id} : get the "id" remittanceTransaction.
     *
     * @param id the id of the remittanceTransaction to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the remittanceTransaction, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/remittance-transactions/{id}")
    public ResponseEntity<RemittanceTransaction> getRemittanceTransaction(@PathVariable Long id) {
        log.debug("REST request to get RemittanceTransaction : {}", id);
        Optional<RemittanceTransaction> remittanceTransaction = remittanceTransactionRepository.findById(id);
        return ResponseUtil.wrapOrNotFound(remittanceTransaction);
    }

    /**
     * {@code DELETE  /remittance-transactions/:id} : delete the "id" remittanceTransaction.
     *
     * @param id the id of the remittanceTransaction to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/remittance-transactions/{id}")
    public ResponseEntity<Void> deleteRemittanceTransaction(@PathVariable Long id) {
        log.debug("REST request to delete RemittanceTransaction : {}", id);
        remittanceTransactionRepository.deleteById(id);
        return ResponseEntity
            .noContent()
            .headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id.toString()))
            .build();
    }
}
