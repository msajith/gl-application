package com.iconnect101.glapp.web.rest;

import com.iconnect101.glapp.domain.CustomerType;
import com.iconnect101.glapp.repository.CustomerTypeRepository;
import com.iconnect101.glapp.web.rest.errors.BadRequestAlertException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;
import tech.jhipster.web.util.HeaderUtil;
import tech.jhipster.web.util.ResponseUtil;

/**
 * REST controller for managing {@link com.iconnect101.glapp.domain.CustomerType}.
 */
@RestController
@RequestMapping("/api")
@Transactional
public class CustomerTypeResource {

    private final Logger log = LoggerFactory.getLogger(CustomerTypeResource.class);

    private static final String ENTITY_NAME = "customerType";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final CustomerTypeRepository customerTypeRepository;

    public CustomerTypeResource(CustomerTypeRepository customerTypeRepository) {
        this.customerTypeRepository = customerTypeRepository;
    }

    /**
     * {@code POST  /customer-types} : Create a new customerType.
     *
     * @param customerType the customerType to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new customerType, or with status {@code 400 (Bad Request)} if the customerType has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/customer-types")
    public ResponseEntity<CustomerType> createCustomerType(@RequestBody CustomerType customerType) throws URISyntaxException {
        log.debug("REST request to save CustomerType : {}", customerType);
        if (customerType.getId() != null) {
            throw new BadRequestAlertException("A new customerType cannot already have an ID", ENTITY_NAME, "idexists");
        }
        CustomerType result = customerTypeRepository.save(customerType);
        return ResponseEntity
            .created(new URI("/api/customer-types/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /customer-types/:id} : Updates an existing customerType.
     *
     * @param id the id of the customerType to save.
     * @param customerType the customerType to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated customerType,
     * or with status {@code 400 (Bad Request)} if the customerType is not valid,
     * or with status {@code 500 (Internal Server Error)} if the customerType couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/customer-types/{id}")
    public ResponseEntity<CustomerType> updateCustomerType(
        @PathVariable(value = "id", required = false) final Long id,
        @RequestBody CustomerType customerType
    ) throws URISyntaxException {
        log.debug("REST request to update CustomerType : {}, {}", id, customerType);
        if (customerType.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, customerType.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!customerTypeRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        CustomerType result = customerTypeRepository.save(customerType);
        return ResponseEntity
            .ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, customerType.getId().toString()))
            .body(result);
    }

    /**
     * {@code PATCH  /customer-types/:id} : Partial updates given fields of an existing customerType, field will ignore if it is null
     *
     * @param id the id of the customerType to save.
     * @param customerType the customerType to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated customerType,
     * or with status {@code 400 (Bad Request)} if the customerType is not valid,
     * or with status {@code 404 (Not Found)} if the customerType is not found,
     * or with status {@code 500 (Internal Server Error)} if the customerType couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PatchMapping(value = "/customer-types/{id}", consumes = { "application/json", "application/merge-patch+json" })
    public ResponseEntity<CustomerType> partialUpdateCustomerType(
        @PathVariable(value = "id", required = false) final Long id,
        @RequestBody CustomerType customerType
    ) throws URISyntaxException {
        log.debug("REST request to partial update CustomerType partially : {}, {}", id, customerType);
        if (customerType.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, customerType.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!customerTypeRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        Optional<CustomerType> result = customerTypeRepository
            .findById(customerType.getId())
            .map(existingCustomerType -> {
                if (customerType.getName() != null) {
                    existingCustomerType.setName(customerType.getName());
                }
                if (customerType.getCode() != null) {
                    existingCustomerType.setCode(customerType.getCode());
                }

                return existingCustomerType;
            })
            .map(customerTypeRepository::save);

        return ResponseUtil.wrapOrNotFound(
            result,
            HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, customerType.getId().toString())
        );
    }

    /**
     * {@code GET  /customer-types} : get all the customerTypes.
     *
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of customerTypes in body.
     */
    @GetMapping("/customer-types")
    public List<CustomerType> getAllCustomerTypes() {
        log.debug("REST request to get all CustomerTypes");
        return customerTypeRepository.findAll();
    }

    /**
     * {@code GET  /customer-types/:id} : get the "id" customerType.
     *
     * @param id the id of the customerType to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the customerType, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/customer-types/{id}")
    public ResponseEntity<CustomerType> getCustomerType(@PathVariable Long id) {
        log.debug("REST request to get CustomerType : {}", id);
        Optional<CustomerType> customerType = customerTypeRepository.findById(id);
        return ResponseUtil.wrapOrNotFound(customerType);
    }

    /**
     * {@code DELETE  /customer-types/:id} : delete the "id" customerType.
     *
     * @param id the id of the customerType to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/customer-types/{id}")
    public ResponseEntity<Void> deleteCustomerType(@PathVariable Long id) {
        log.debug("REST request to delete CustomerType : {}", id);
        customerTypeRepository.deleteById(id);
        return ResponseEntity
            .noContent()
            .headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id.toString()))
            .build();
    }
}
