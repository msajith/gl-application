/**
 * View Models used by Spring MVC REST controllers.
 */
package com.iconnect101.glapp.web.rest.vm;
