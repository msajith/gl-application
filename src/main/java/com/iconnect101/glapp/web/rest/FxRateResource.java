package com.iconnect101.glapp.web.rest;

import com.iconnect101.glapp.domain.FxRate;
import com.iconnect101.glapp.repository.FxRateRepository;
import com.iconnect101.glapp.web.rest.errors.BadRequestAlertException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;
import tech.jhipster.web.util.HeaderUtil;
import tech.jhipster.web.util.ResponseUtil;

/**
 * REST controller for managing {@link com.iconnect101.glapp.domain.FxRate}.
 */
@RestController
@RequestMapping("/api")
@Transactional
public class FxRateResource {

    private final Logger log = LoggerFactory.getLogger(FxRateResource.class);

    private static final String ENTITY_NAME = "fxRate";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final FxRateRepository fxRateRepository;

    public FxRateResource(FxRateRepository fxRateRepository) {
        this.fxRateRepository = fxRateRepository;
    }

    /**
     * {@code POST  /fx-rates} : Create a new fxRate.
     *
     * @param fxRate the fxRate to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new fxRate, or with status {@code 400 (Bad Request)} if the fxRate has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/fx-rates")
    public ResponseEntity<FxRate> createFxRate(@RequestBody FxRate fxRate) throws URISyntaxException {
        log.debug("REST request to save FxRate : {}", fxRate);
        if (fxRate.getId() != null) {
            throw new BadRequestAlertException("A new fxRate cannot already have an ID", ENTITY_NAME, "idexists");
        }
        FxRate result = fxRateRepository.save(fxRate);
        return ResponseEntity
            .created(new URI("/api/fx-rates/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /fx-rates/:id} : Updates an existing fxRate.
     *
     * @param id the id of the fxRate to save.
     * @param fxRate the fxRate to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated fxRate,
     * or with status {@code 400 (Bad Request)} if the fxRate is not valid,
     * or with status {@code 500 (Internal Server Error)} if the fxRate couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/fx-rates/{id}")
    public ResponseEntity<FxRate> updateFxRate(@PathVariable(value = "id", required = false) final Long id, @RequestBody FxRate fxRate)
        throws URISyntaxException {
        log.debug("REST request to update FxRate : {}, {}", id, fxRate);
        if (fxRate.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, fxRate.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!fxRateRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        FxRate result = fxRateRepository.save(fxRate);
        return ResponseEntity
            .ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, fxRate.getId().toString()))
            .body(result);
    }

    /**
     * {@code PATCH  /fx-rates/:id} : Partial updates given fields of an existing fxRate, field will ignore if it is null
     *
     * @param id the id of the fxRate to save.
     * @param fxRate the fxRate to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated fxRate,
     * or with status {@code 400 (Bad Request)} if the fxRate is not valid,
     * or with status {@code 404 (Not Found)} if the fxRate is not found,
     * or with status {@code 500 (Internal Server Error)} if the fxRate couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PatchMapping(value = "/fx-rates/{id}", consumes = { "application/json", "application/merge-patch+json" })
    public ResponseEntity<FxRate> partialUpdateFxRate(
        @PathVariable(value = "id", required = false) final Long id,
        @RequestBody FxRate fxRate
    ) throws URISyntaxException {
        log.debug("REST request to partial update FxRate partially : {}, {}", id, fxRate);
        if (fxRate.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, fxRate.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!fxRateRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        Optional<FxRate> result = fxRateRepository
            .findById(fxRate.getId())
            .map(existingFxRate -> {
                if (fxRate.getType() != null) {
                    existingFxRate.setType(fxRate.getType());
                }
                if (fxRate.getBaseCurrency() != null) {
                    existingFxRate.setBaseCurrency(fxRate.getBaseCurrency());
                }
                if (fxRate.getQuoteCurrency() != null) {
                    existingFxRate.setQuoteCurrency(fxRate.getQuoteCurrency());
                }
                if (fxRate.getAskPrice() != null) {
                    existingFxRate.setAskPrice(fxRate.getAskPrice());
                }
                if (fxRate.getBidPrice() != null) {
                    existingFxRate.setBidPrice(fxRate.getBidPrice());
                }
                if (fxRate.getMidPrice() != null) {
                    existingFxRate.setMidPrice(fxRate.getMidPrice());
                }
                if (fxRate.getAskMargin() != null) {
                    existingFxRate.setAskMargin(fxRate.getAskMargin());
                }
                if (fxRate.getBidMargin() != null) {
                    existingFxRate.setBidMargin(fxRate.getBidMargin());
                }
                if (fxRate.getSegment() != null) {
                    existingFxRate.setSegment(fxRate.getSegment());
                }
                if (fxRate.getAmountSlab() != null) {
                    existingFxRate.setAmountSlab(fxRate.getAmountSlab());
                }
                if (fxRate.getAmountSlabValue() != null) {
                    existingFxRate.setAmountSlabValue(fxRate.getAmountSlabValue());
                }
                if (fxRate.getAmountSlabOperator() != null) {
                    existingFxRate.setAmountSlabOperator(fxRate.getAmountSlabOperator());
                }
                if (fxRate.getAmountSlabCurrency() != null) {
                    existingFxRate.setAmountSlabCurrency(fxRate.getAmountSlabCurrency());
                }
                if (fxRate.getRecordStatus() != null) {
                    existingFxRate.setRecordStatus(fxRate.getRecordStatus());
                }
                if (fxRate.getValidfrom() != null) {
                    existingFxRate.setValidfrom(fxRate.getValidfrom());
                }
                if (fxRate.getValidTo() != null) {
                    existingFxRate.setValidTo(fxRate.getValidTo());
                }

                return existingFxRate;
            })
            .map(fxRateRepository::save);

        return ResponseUtil.wrapOrNotFound(
            result,
            HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, fxRate.getId().toString())
        );
    }

    /**
     * {@code GET  /fx-rates} : get all the fxRates.
     *
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of fxRates in body.
     */
    @GetMapping("/fx-rates")
    public List<FxRate> getAllFxRates() {
        log.debug("REST request to get all FxRates");
        return fxRateRepository.findAll();
    }

    /**
     * {@code GET  /fx-rates/:id} : get the "id" fxRate.
     *
     * @param id the id of the fxRate to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the fxRate, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/fx-rates/{id}")
    public ResponseEntity<FxRate> getFxRate(@PathVariable Long id) {
        log.debug("REST request to get FxRate : {}", id);
        Optional<FxRate> fxRate = fxRateRepository.findById(id);
        return ResponseUtil.wrapOrNotFound(fxRate);
    }

    /**
     * {@code DELETE  /fx-rates/:id} : delete the "id" fxRate.
     *
     * @param id the id of the fxRate to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/fx-rates/{id}")
    public ResponseEntity<Void> deleteFxRate(@PathVariable Long id) {
        log.debug("REST request to delete FxRate : {}", id);
        fxRateRepository.deleteById(id);
        return ResponseEntity
            .noContent()
            .headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id.toString()))
            .build();
    }
}
