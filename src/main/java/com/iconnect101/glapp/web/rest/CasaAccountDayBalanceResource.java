package com.iconnect101.glapp.web.rest;

import com.iconnect101.glapp.domain.CasaAccountDayBalance;
import com.iconnect101.glapp.repository.CasaAccountDayBalanceRepository;
import com.iconnect101.glapp.web.rest.errors.BadRequestAlertException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;
import tech.jhipster.web.util.HeaderUtil;
import tech.jhipster.web.util.ResponseUtil;

/**
 * REST controller for managing {@link com.iconnect101.glapp.domain.CasaAccountDayBalance}.
 */
@RestController
@RequestMapping("/api")
@Transactional
public class CasaAccountDayBalanceResource {

    private final Logger log = LoggerFactory.getLogger(CasaAccountDayBalanceResource.class);

    private static final String ENTITY_NAME = "casaAccountDayBalance";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final CasaAccountDayBalanceRepository casaAccountDayBalanceRepository;

    public CasaAccountDayBalanceResource(CasaAccountDayBalanceRepository casaAccountDayBalanceRepository) {
        this.casaAccountDayBalanceRepository = casaAccountDayBalanceRepository;
    }

    /**
     * {@code POST  /casa-account-day-balances} : Create a new casaAccountDayBalance.
     *
     * @param casaAccountDayBalance the casaAccountDayBalance to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new casaAccountDayBalance, or with status {@code 400 (Bad Request)} if the casaAccountDayBalance has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/casa-account-day-balances")
    public ResponseEntity<CasaAccountDayBalance> createCasaAccountDayBalance(
        @Valid @RequestBody CasaAccountDayBalance casaAccountDayBalance
    ) throws URISyntaxException {
        log.debug("REST request to save CasaAccountDayBalance : {}", casaAccountDayBalance);
        if (casaAccountDayBalance.getId() != null) {
            throw new BadRequestAlertException("A new casaAccountDayBalance cannot already have an ID", ENTITY_NAME, "idexists");
        }
        CasaAccountDayBalance result = casaAccountDayBalanceRepository.save(casaAccountDayBalance);
        return ResponseEntity
            .created(new URI("/api/casa-account-day-balances/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /casa-account-day-balances/:id} : Updates an existing casaAccountDayBalance.
     *
     * @param id the id of the casaAccountDayBalance to save.
     * @param casaAccountDayBalance the casaAccountDayBalance to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated casaAccountDayBalance,
     * or with status {@code 400 (Bad Request)} if the casaAccountDayBalance is not valid,
     * or with status {@code 500 (Internal Server Error)} if the casaAccountDayBalance couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/casa-account-day-balances/{id}")
    public ResponseEntity<CasaAccountDayBalance> updateCasaAccountDayBalance(
        @PathVariable(value = "id", required = false) final Long id,
        @Valid @RequestBody CasaAccountDayBalance casaAccountDayBalance
    ) throws URISyntaxException {
        log.debug("REST request to update CasaAccountDayBalance : {}, {}", id, casaAccountDayBalance);
        if (casaAccountDayBalance.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, casaAccountDayBalance.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!casaAccountDayBalanceRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        CasaAccountDayBalance result = casaAccountDayBalanceRepository.save(casaAccountDayBalance);
        return ResponseEntity
            .ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, casaAccountDayBalance.getId().toString()))
            .body(result);
    }

    /**
     * {@code PATCH  /casa-account-day-balances/:id} : Partial updates given fields of an existing casaAccountDayBalance, field will ignore if it is null
     *
     * @param id the id of the casaAccountDayBalance to save.
     * @param casaAccountDayBalance the casaAccountDayBalance to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated casaAccountDayBalance,
     * or with status {@code 400 (Bad Request)} if the casaAccountDayBalance is not valid,
     * or with status {@code 404 (Not Found)} if the casaAccountDayBalance is not found,
     * or with status {@code 500 (Internal Server Error)} if the casaAccountDayBalance couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PatchMapping(value = "/casa-account-day-balances/{id}", consumes = { "application/json", "application/merge-patch+json" })
    public ResponseEntity<CasaAccountDayBalance> partialUpdateCasaAccountDayBalance(
        @PathVariable(value = "id", required = false) final Long id,
        @NotNull @RequestBody CasaAccountDayBalance casaAccountDayBalance
    ) throws URISyntaxException {
        log.debug("REST request to partial update CasaAccountDayBalance partially : {}, {}", id, casaAccountDayBalance);
        if (casaAccountDayBalance.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, casaAccountDayBalance.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!casaAccountDayBalanceRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        Optional<CasaAccountDayBalance> result = casaAccountDayBalanceRepository
            .findById(casaAccountDayBalance.getId())
            .map(existingCasaAccountDayBalance -> {
                if (casaAccountDayBalance.getAccountNo() != null) {
                    existingCasaAccountDayBalance.setAccountNo(casaAccountDayBalance.getAccountNo());
                }
                if (casaAccountDayBalance.getDate() != null) {
                    existingCasaAccountDayBalance.setDate(casaAccountDayBalance.getDate());
                }
                if (casaAccountDayBalance.getClosingBalance() != null) {
                    existingCasaAccountDayBalance.setClosingBalance(casaAccountDayBalance.getClosingBalance());
                }
                if (casaAccountDayBalance.getOpeningBalance() != null) {
                    existingCasaAccountDayBalance.setOpeningBalance(casaAccountDayBalance.getOpeningBalance());
                }
                if (casaAccountDayBalance.getTotalDebit() != null) {
                    existingCasaAccountDayBalance.setTotalDebit(casaAccountDayBalance.getTotalDebit());
                }
                if (casaAccountDayBalance.getTotalCredit() != null) {
                    existingCasaAccountDayBalance.setTotalCredit(casaAccountDayBalance.getTotalCredit());
                }
                if (casaAccountDayBalance.getAvailableBalance() != null) {
                    existingCasaAccountDayBalance.setAvailableBalance(casaAccountDayBalance.getAvailableBalance());
                }
                if (casaAccountDayBalance.getCurrentBalance() != null) {
                    existingCasaAccountDayBalance.setCurrentBalance(casaAccountDayBalance.getCurrentBalance());
                }
                if (casaAccountDayBalance.getCreatedOn() != null) {
                    existingCasaAccountDayBalance.setCreatedOn(casaAccountDayBalance.getCreatedOn());
                }
                if (casaAccountDayBalance.getCreatedBy() != null) {
                    existingCasaAccountDayBalance.setCreatedBy(casaAccountDayBalance.getCreatedBy());
                }

                return existingCasaAccountDayBalance;
            })
            .map(casaAccountDayBalanceRepository::save);

        return ResponseUtil.wrapOrNotFound(
            result,
            HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, casaAccountDayBalance.getId().toString())
        );
    }

    /**
     * {@code GET  /casa-account-day-balances} : get all the casaAccountDayBalances.
     *
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of casaAccountDayBalances in body.
     */
    @GetMapping("/casa-account-day-balances")
    public List<CasaAccountDayBalance> getAllCasaAccountDayBalances() {
        log.debug("REST request to get all CasaAccountDayBalances");
        return casaAccountDayBalanceRepository.findAll();
    }

    /**
     * {@code GET  /casa-account-day-balances/:id} : get the "id" casaAccountDayBalance.
     *
     * @param id the id of the casaAccountDayBalance to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the casaAccountDayBalance, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/casa-account-day-balances/{id}")
    public ResponseEntity<CasaAccountDayBalance> getCasaAccountDayBalance(@PathVariable Long id) {
        log.debug("REST request to get CasaAccountDayBalance : {}", id);
        Optional<CasaAccountDayBalance> casaAccountDayBalance = casaAccountDayBalanceRepository.findById(id);
        return ResponseUtil.wrapOrNotFound(casaAccountDayBalance);
    }

    /**
     * {@code DELETE  /casa-account-day-balances/:id} : delete the "id" casaAccountDayBalance.
     *
     * @param id the id of the casaAccountDayBalance to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/casa-account-day-balances/{id}")
    public ResponseEntity<Void> deleteCasaAccountDayBalance(@PathVariable Long id) {
        log.debug("REST request to delete CasaAccountDayBalance : {}", id);
        casaAccountDayBalanceRepository.deleteById(id);
        return ResponseEntity
            .noContent()
            .headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id.toString()))
            .build();
    }
}
