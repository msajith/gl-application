package com.iconnect101.glapp.web.rest;

import com.iconnect101.glapp.domain.GlAccount;
import com.iconnect101.glapp.repository.GlAccountRepository;
import com.iconnect101.glapp.web.rest.errors.BadRequestAlertException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;
import tech.jhipster.web.util.HeaderUtil;
import tech.jhipster.web.util.ResponseUtil;

/**
 * REST controller for managing {@link com.iconnect101.glapp.domain.GlAccount}.
 */
@RestController
@RequestMapping("/api")
@Transactional
public class GlAccountResource {

    private final Logger log = LoggerFactory.getLogger(GlAccountResource.class);

    private static final String ENTITY_NAME = "glAccount";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final GlAccountRepository glAccountRepository;

    public GlAccountResource(GlAccountRepository glAccountRepository) {
        this.glAccountRepository = glAccountRepository;
    }

    /**
     * {@code POST  /gl-accounts} : Create a new glAccount.
     *
     * @param glAccount the glAccount to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new glAccount, or with status {@code 400 (Bad Request)} if the glAccount has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/gl-accounts")
    public ResponseEntity<GlAccount> createGlAccount(@Valid @RequestBody GlAccount glAccount) throws URISyntaxException {
        log.debug("REST request to save GlAccount : {}", glAccount);
        if (glAccount.getId() != null) {
            throw new BadRequestAlertException("A new glAccount cannot already have an ID", ENTITY_NAME, "idexists");
        }
        GlAccount result = glAccountRepository.save(glAccount);
        return ResponseEntity
            .created(new URI("/api/gl-accounts/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /gl-accounts/:id} : Updates an existing glAccount.
     *
     * @param id the id of the glAccount to save.
     * @param glAccount the glAccount to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated glAccount,
     * or with status {@code 400 (Bad Request)} if the glAccount is not valid,
     * or with status {@code 500 (Internal Server Error)} if the glAccount couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/gl-accounts/{id}")
    public ResponseEntity<GlAccount> updateGlAccount(
        @PathVariable(value = "id", required = false) final Long id,
        @Valid @RequestBody GlAccount glAccount
    ) throws URISyntaxException {
        log.debug("REST request to update GlAccount : {}, {}", id, glAccount);
        if (glAccount.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, glAccount.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!glAccountRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        GlAccount result = glAccountRepository.save(glAccount);
        return ResponseEntity
            .ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, glAccount.getId().toString()))
            .body(result);
    }

    /**
     * {@code PATCH  /gl-accounts/:id} : Partial updates given fields of an existing glAccount, field will ignore if it is null
     *
     * @param id the id of the glAccount to save.
     * @param glAccount the glAccount to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated glAccount,
     * or with status {@code 400 (Bad Request)} if the glAccount is not valid,
     * or with status {@code 404 (Not Found)} if the glAccount is not found,
     * or with status {@code 500 (Internal Server Error)} if the glAccount couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PatchMapping(value = "/gl-accounts/{id}", consumes = { "application/json", "application/merge-patch+json" })
    public ResponseEntity<GlAccount> partialUpdateGlAccount(
        @PathVariable(value = "id", required = false) final Long id,
        @NotNull @RequestBody GlAccount glAccount
    ) throws URISyntaxException {
        log.debug("REST request to partial update GlAccount partially : {}, {}", id, glAccount);
        if (glAccount.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, glAccount.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!glAccountRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        Optional<GlAccount> result = glAccountRepository
            .findById(glAccount.getId())
            .map(existingGlAccount -> {
                if (glAccount.getAccountNo() != null) {
                    existingGlAccount.setAccountNo(glAccount.getAccountNo());
                }
                if (glAccount.getAccountName() != null) {
                    existingGlAccount.setAccountName(glAccount.getAccountName());
                }
                if (glAccount.getDate() != null) {
                    existingGlAccount.setDate(glAccount.getDate());
                }
                if (glAccount.getClosingBalance() != null) {
                    existingGlAccount.setClosingBalance(glAccount.getClosingBalance());
                }
                if (glAccount.getOpeningBalance() != null) {
                    existingGlAccount.setOpeningBalance(glAccount.getOpeningBalance());
                }
                if (glAccount.getTotalDebit() != null) {
                    existingGlAccount.setTotalDebit(glAccount.getTotalDebit());
                }
                if (glAccount.getTotalCredit() != null) {
                    existingGlAccount.setTotalCredit(glAccount.getTotalCredit());
                }
                if (glAccount.getAvailableBalance() != null) {
                    existingGlAccount.setAvailableBalance(glAccount.getAvailableBalance());
                }
                if (glAccount.getCurrentBalance() != null) {
                    existingGlAccount.setCurrentBalance(glAccount.getCurrentBalance());
                }
                if (glAccount.getCreatedOn() != null) {
                    existingGlAccount.setCreatedOn(glAccount.getCreatedOn());
                }
                if (glAccount.getCreatedBy() != null) {
                    existingGlAccount.setCreatedBy(glAccount.getCreatedBy());
                }
                if (glAccount.getGlAccountStatus() != null) {
                    existingGlAccount.setGlAccountStatus(glAccount.getGlAccountStatus());
                }
                if (glAccount.getGlAccountType() != null) {
                    existingGlAccount.setGlAccountType(glAccount.getGlAccountType());
                }

                return existingGlAccount;
            })
            .map(glAccountRepository::save);

        return ResponseUtil.wrapOrNotFound(
            result,
            HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, glAccount.getId().toString())
        );
    }

    /**
     * {@code GET  /gl-accounts} : get all the glAccounts.
     *
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of glAccounts in body.
     */
    @GetMapping("/gl-accounts")
    public List<GlAccount> getAllGlAccounts() {
        log.debug("REST request to get all GlAccounts");
        return glAccountRepository.findAll();
    }

    /**
     * {@code GET  /gl-accounts/:id} : get the "id" glAccount.
     *
     * @param id the id of the glAccount to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the glAccount, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/gl-accounts/{id}")
    public ResponseEntity<GlAccount> getGlAccount(@PathVariable Long id) {
        log.debug("REST request to get GlAccount : {}", id);
        Optional<GlAccount> glAccount = glAccountRepository.findById(id);
        return ResponseUtil.wrapOrNotFound(glAccount);
    }

    /**
     * {@code DELETE  /gl-accounts/:id} : delete the "id" glAccount.
     *
     * @param id the id of the glAccount to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/gl-accounts/{id}")
    public ResponseEntity<Void> deleteGlAccount(@PathVariable Long id) {
        log.debug("REST request to delete GlAccount : {}", id);
        glAccountRepository.deleteById(id);
        return ResponseEntity
            .noContent()
            .headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id.toString()))
            .build();
    }
}
