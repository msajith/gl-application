package com.iconnect101.glapp.web.rest;

import com.iconnect101.glapp.domain.GeneralLedger;
import com.iconnect101.glapp.repository.GeneralLedgerRepository;
import com.iconnect101.glapp.web.rest.errors.BadRequestAlertException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;
import tech.jhipster.web.util.HeaderUtil;
import tech.jhipster.web.util.ResponseUtil;

/**
 * REST controller for managing {@link com.iconnect101.glapp.domain.GeneralLedger}.
 */
@RestController
@RequestMapping("/api")
@Transactional
public class GeneralLedgerResource {

    private final Logger log = LoggerFactory.getLogger(GeneralLedgerResource.class);

    private static final String ENTITY_NAME = "generalLedger";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final GeneralLedgerRepository generalLedgerRepository;

    public GeneralLedgerResource(GeneralLedgerRepository generalLedgerRepository) {
        this.generalLedgerRepository = generalLedgerRepository;
    }

    /**
     * {@code POST  /general-ledgers} : Create a new generalLedger.
     *
     * @param generalLedger the generalLedger to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new generalLedger, or with status {@code 400 (Bad Request)} if the generalLedger has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/general-ledgers")
    public ResponseEntity<GeneralLedger> createGeneralLedger(@RequestBody GeneralLedger generalLedger) throws URISyntaxException {
        log.debug("REST request to save GeneralLedger : {}", generalLedger);
        if (generalLedger.getId() != null) {
            throw new BadRequestAlertException("A new generalLedger cannot already have an ID", ENTITY_NAME, "idexists");
        }
        GeneralLedger result = generalLedgerRepository.save(generalLedger);
        return ResponseEntity
            .created(new URI("/api/general-ledgers/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /general-ledgers/:id} : Updates an existing generalLedger.
     *
     * @param id the id of the generalLedger to save.
     * @param generalLedger the generalLedger to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated generalLedger,
     * or with status {@code 400 (Bad Request)} if the generalLedger is not valid,
     * or with status {@code 500 (Internal Server Error)} if the generalLedger couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/general-ledgers/{id}")
    public ResponseEntity<GeneralLedger> updateGeneralLedger(
        @PathVariable(value = "id", required = false) final Long id,
        @RequestBody GeneralLedger generalLedger
    ) throws URISyntaxException {
        log.debug("REST request to update GeneralLedger : {}, {}", id, generalLedger);
        if (generalLedger.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, generalLedger.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!generalLedgerRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        GeneralLedger result = generalLedgerRepository.save(generalLedger);
        return ResponseEntity
            .ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, generalLedger.getId().toString()))
            .body(result);
    }

    /**
     * {@code PATCH  /general-ledgers/:id} : Partial updates given fields of an existing generalLedger, field will ignore if it is null
     *
     * @param id the id of the generalLedger to save.
     * @param generalLedger the generalLedger to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated generalLedger,
     * or with status {@code 400 (Bad Request)} if the generalLedger is not valid,
     * or with status {@code 404 (Not Found)} if the generalLedger is not found,
     * or with status {@code 500 (Internal Server Error)} if the generalLedger couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PatchMapping(value = "/general-ledgers/{id}", consumes = { "application/json", "application/merge-patch+json" })
    public ResponseEntity<GeneralLedger> partialUpdateGeneralLedger(
        @PathVariable(value = "id", required = false) final Long id,
        @RequestBody GeneralLedger generalLedger
    ) throws URISyntaxException {
        log.debug("REST request to partial update GeneralLedger partially : {}, {}", id, generalLedger);
        if (generalLedger.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, generalLedger.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!generalLedgerRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        Optional<GeneralLedger> result = generalLedgerRepository
            .findById(generalLedger.getId())
            .map(existingGeneralLedger -> {
                if (generalLedger.getGlRefNo() != null) {
                    existingGeneralLedger.setGlRefNo(generalLedger.getGlRefNo());
                }
                if (generalLedger.getGlAccountNo() != null) {
                    existingGeneralLedger.setGlAccountNo(generalLedger.getGlAccountNo());
                }
                if (generalLedger.getTxnDate() != null) {
                    existingGeneralLedger.setTxnDate(generalLedger.getTxnDate());
                }
                if (generalLedger.getTxnDateInUTC() != null) {
                    existingGeneralLedger.setTxnDateInUTC(generalLedger.getTxnDateInUTC());
                }
                if (generalLedger.getTxnDateInLocal() != null) {
                    existingGeneralLedger.setTxnDateInLocal(generalLedger.getTxnDateInLocal());
                }
                if (generalLedger.getTxnDateForSettlement() != null) {
                    existingGeneralLedger.setTxnDateForSettlement(generalLedger.getTxnDateForSettlement());
                }
                if (generalLedger.getAmountInAccountCurrency() != null) {
                    existingGeneralLedger.setAmountInAccountCurrency(generalLedger.getAmountInAccountCurrency());
                }
                if (generalLedger.getAmountInLocalCurrency() != null) {
                    existingGeneralLedger.setAmountInLocalCurrency(generalLedger.getAmountInLocalCurrency());
                }
                if (generalLedger.getRate() != null) {
                    existingGeneralLedger.setRate(generalLedger.getRate());
                }
                if (generalLedger.getEntryCategory() != null) {
                    existingGeneralLedger.setEntryCategory(generalLedger.getEntryCategory());
                }
                if (generalLedger.getDescription1() != null) {
                    existingGeneralLedger.setDescription1(generalLedger.getDescription1());
                }
                if (generalLedger.getDescription2() != null) {
                    existingGeneralLedger.setDescription2(generalLedger.getDescription2());
                }
                if (generalLedger.getCreatedOn() != null) {
                    existingGeneralLedger.setCreatedOn(generalLedger.getCreatedOn());
                }
                if (generalLedger.getCreatedBy() != null) {
                    existingGeneralLedger.setCreatedBy(generalLedger.getCreatedBy());
                }
                if (generalLedger.getEntryStatus() != null) {
                    existingGeneralLedger.setEntryStatus(generalLedger.getEntryStatus());
                }
                if (generalLedger.getGlEntryType() != null) {
                    existingGeneralLedger.setGlEntryType(generalLedger.getGlEntryType());
                }

                return existingGeneralLedger;
            })
            .map(generalLedgerRepository::save);

        return ResponseUtil.wrapOrNotFound(
            result,
            HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, generalLedger.getId().toString())
        );
    }

    /**
     * {@code GET  /general-ledgers} : get all the generalLedgers.
     *
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of generalLedgers in body.
     */
    @GetMapping("/general-ledgers")
    public List<GeneralLedger> getAllGeneralLedgers() {
        log.debug("REST request to get all GeneralLedgers");
        return generalLedgerRepository.findAll();
    }

    /**
     * {@code GET  /general-ledgers/:id} : get the "id" generalLedger.
     *
     * @param id the id of the generalLedger to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the generalLedger, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/general-ledgers/{id}")
    public ResponseEntity<GeneralLedger> getGeneralLedger(@PathVariable Long id) {
        log.debug("REST request to get GeneralLedger : {}", id);
        Optional<GeneralLedger> generalLedger = generalLedgerRepository.findById(id);
        return ResponseUtil.wrapOrNotFound(generalLedger);
    }

    /**
     * {@code DELETE  /general-ledgers/:id} : delete the "id" generalLedger.
     *
     * @param id the id of the generalLedger to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/general-ledgers/{id}")
    public ResponseEntity<Void> deleteGeneralLedger(@PathVariable Long id) {
        log.debug("REST request to delete GeneralLedger : {}", id);
        generalLedgerRepository.deleteById(id);
        return ResponseEntity
            .noContent()
            .headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id.toString()))
            .build();
    }
}
