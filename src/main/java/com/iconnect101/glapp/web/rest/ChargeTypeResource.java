package com.iconnect101.glapp.web.rest;

import com.iconnect101.glapp.domain.ChargeType;
import com.iconnect101.glapp.repository.ChargeTypeRepository;
import com.iconnect101.glapp.web.rest.errors.BadRequestAlertException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;
import tech.jhipster.web.util.HeaderUtil;
import tech.jhipster.web.util.ResponseUtil;

/**
 * REST controller for managing {@link com.iconnect101.glapp.domain.ChargeType}.
 */
@RestController
@RequestMapping("/api")
@Transactional
public class ChargeTypeResource {

    private final Logger log = LoggerFactory.getLogger(ChargeTypeResource.class);

    private static final String ENTITY_NAME = "chargeType";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final ChargeTypeRepository chargeTypeRepository;

    public ChargeTypeResource(ChargeTypeRepository chargeTypeRepository) {
        this.chargeTypeRepository = chargeTypeRepository;
    }

    /**
     * {@code POST  /charge-types} : Create a new chargeType.
     *
     * @param chargeType the chargeType to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new chargeType, or with status {@code 400 (Bad Request)} if the chargeType has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/charge-types")
    public ResponseEntity<ChargeType> createChargeType(@RequestBody ChargeType chargeType) throws URISyntaxException {
        log.debug("REST request to save ChargeType : {}", chargeType);
        if (chargeType.getId() != null) {
            throw new BadRequestAlertException("A new chargeType cannot already have an ID", ENTITY_NAME, "idexists");
        }
        ChargeType result = chargeTypeRepository.save(chargeType);
        return ResponseEntity
            .created(new URI("/api/charge-types/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /charge-types/:id} : Updates an existing chargeType.
     *
     * @param id the id of the chargeType to save.
     * @param chargeType the chargeType to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated chargeType,
     * or with status {@code 400 (Bad Request)} if the chargeType is not valid,
     * or with status {@code 500 (Internal Server Error)} if the chargeType couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/charge-types/{id}")
    public ResponseEntity<ChargeType> updateChargeType(
        @PathVariable(value = "id", required = false) final Long id,
        @RequestBody ChargeType chargeType
    ) throws URISyntaxException {
        log.debug("REST request to update ChargeType : {}, {}", id, chargeType);
        if (chargeType.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, chargeType.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!chargeTypeRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        ChargeType result = chargeTypeRepository.save(chargeType);
        return ResponseEntity
            .ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, chargeType.getId().toString()))
            .body(result);
    }

    /**
     * {@code PATCH  /charge-types/:id} : Partial updates given fields of an existing chargeType, field will ignore if it is null
     *
     * @param id the id of the chargeType to save.
     * @param chargeType the chargeType to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated chargeType,
     * or with status {@code 400 (Bad Request)} if the chargeType is not valid,
     * or with status {@code 404 (Not Found)} if the chargeType is not found,
     * or with status {@code 500 (Internal Server Error)} if the chargeType couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PatchMapping(value = "/charge-types/{id}", consumes = { "application/json", "application/merge-patch+json" })
    public ResponseEntity<ChargeType> partialUpdateChargeType(
        @PathVariable(value = "id", required = false) final Long id,
        @RequestBody ChargeType chargeType
    ) throws URISyntaxException {
        log.debug("REST request to partial update ChargeType partially : {}, {}", id, chargeType);
        if (chargeType.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, chargeType.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!chargeTypeRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        Optional<ChargeType> result = chargeTypeRepository
            .findById(chargeType.getId())
            .map(existingChargeType -> {
                if (chargeType.getName() != null) {
                    existingChargeType.setName(chargeType.getName());
                }
                if (chargeType.getCode() != null) {
                    existingChargeType.setCode(chargeType.getCode());
                }

                return existingChargeType;
            })
            .map(chargeTypeRepository::save);

        return ResponseUtil.wrapOrNotFound(
            result,
            HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, chargeType.getId().toString())
        );
    }

    /**
     * {@code GET  /charge-types} : get all the chargeTypes.
     *
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of chargeTypes in body.
     */
    @GetMapping("/charge-types")
    public List<ChargeType> getAllChargeTypes() {
        log.debug("REST request to get all ChargeTypes");
        return chargeTypeRepository.findAll();
    }

    /**
     * {@code GET  /charge-types/:id} : get the "id" chargeType.
     *
     * @param id the id of the chargeType to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the chargeType, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/charge-types/{id}")
    public ResponseEntity<ChargeType> getChargeType(@PathVariable Long id) {
        log.debug("REST request to get ChargeType : {}", id);
        Optional<ChargeType> chargeType = chargeTypeRepository.findById(id);
        return ResponseUtil.wrapOrNotFound(chargeType);
    }

    /**
     * {@code DELETE  /charge-types/:id} : delete the "id" chargeType.
     *
     * @param id the id of the chargeType to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/charge-types/{id}")
    public ResponseEntity<Void> deleteChargeType(@PathVariable Long id) {
        log.debug("REST request to delete ChargeType : {}", id);
        chargeTypeRepository.deleteById(id);
        return ResponseEntity
            .noContent()
            .headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id.toString()))
            .build();
    }
}
