package com.iconnect101.glapp.web.rest;

import com.iconnect101.glapp.domain.GlEntryType;
import com.iconnect101.glapp.repository.GlEntryTypeRepository;
import com.iconnect101.glapp.web.rest.errors.BadRequestAlertException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;
import tech.jhipster.web.util.HeaderUtil;
import tech.jhipster.web.util.ResponseUtil;

/**
 * REST controller for managing {@link com.iconnect101.glapp.domain.GlEntryType}.
 */
@RestController
@RequestMapping("/api")
@Transactional
public class GlEntryTypeResource {

    private final Logger log = LoggerFactory.getLogger(GlEntryTypeResource.class);

    private static final String ENTITY_NAME = "glEntryType";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final GlEntryTypeRepository glEntryTypeRepository;

    public GlEntryTypeResource(GlEntryTypeRepository glEntryTypeRepository) {
        this.glEntryTypeRepository = glEntryTypeRepository;
    }

    /**
     * {@code POST  /gl-entry-types} : Create a new glEntryType.
     *
     * @param glEntryType the glEntryType to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new glEntryType, or with status {@code 400 (Bad Request)} if the glEntryType has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/gl-entry-types")
    public ResponseEntity<GlEntryType> createGlEntryType(@RequestBody GlEntryType glEntryType) throws URISyntaxException {
        log.debug("REST request to save GlEntryType : {}", glEntryType);
        if (glEntryType.getId() != null) {
            throw new BadRequestAlertException("A new glEntryType cannot already have an ID", ENTITY_NAME, "idexists");
        }
        GlEntryType result = glEntryTypeRepository.save(glEntryType);
        return ResponseEntity
            .created(new URI("/api/gl-entry-types/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /gl-entry-types/:id} : Updates an existing glEntryType.
     *
     * @param id the id of the glEntryType to save.
     * @param glEntryType the glEntryType to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated glEntryType,
     * or with status {@code 400 (Bad Request)} if the glEntryType is not valid,
     * or with status {@code 500 (Internal Server Error)} if the glEntryType couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/gl-entry-types/{id}")
    public ResponseEntity<GlEntryType> updateGlEntryType(
        @PathVariable(value = "id", required = false) final Long id,
        @RequestBody GlEntryType glEntryType
    ) throws URISyntaxException {
        log.debug("REST request to update GlEntryType : {}, {}", id, glEntryType);
        if (glEntryType.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, glEntryType.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!glEntryTypeRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        GlEntryType result = glEntryTypeRepository.save(glEntryType);
        return ResponseEntity
            .ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, glEntryType.getId().toString()))
            .body(result);
    }

    /**
     * {@code PATCH  /gl-entry-types/:id} : Partial updates given fields of an existing glEntryType, field will ignore if it is null
     *
     * @param id the id of the glEntryType to save.
     * @param glEntryType the glEntryType to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated glEntryType,
     * or with status {@code 400 (Bad Request)} if the glEntryType is not valid,
     * or with status {@code 404 (Not Found)} if the glEntryType is not found,
     * or with status {@code 500 (Internal Server Error)} if the glEntryType couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PatchMapping(value = "/gl-entry-types/{id}", consumes = { "application/json", "application/merge-patch+json" })
    public ResponseEntity<GlEntryType> partialUpdateGlEntryType(
        @PathVariable(value = "id", required = false) final Long id,
        @RequestBody GlEntryType glEntryType
    ) throws URISyntaxException {
        log.debug("REST request to partial update GlEntryType partially : {}, {}", id, glEntryType);
        if (glEntryType.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, glEntryType.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!glEntryTypeRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        Optional<GlEntryType> result = glEntryTypeRepository
            .findById(glEntryType.getId())
            .map(existingGlEntryType -> {
                if (glEntryType.getName() != null) {
                    existingGlEntryType.setName(glEntryType.getName());
                }
                if (glEntryType.getCode() != null) {
                    existingGlEntryType.setCode(glEntryType.getCode());
                }

                return existingGlEntryType;
            })
            .map(glEntryTypeRepository::save);

        return ResponseUtil.wrapOrNotFound(
            result,
            HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, glEntryType.getId().toString())
        );
    }

    /**
     * {@code GET  /gl-entry-types} : get all the glEntryTypes.
     *
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of glEntryTypes in body.
     */
    @GetMapping("/gl-entry-types")
    public List<GlEntryType> getAllGlEntryTypes() {
        log.debug("REST request to get all GlEntryTypes");
        return glEntryTypeRepository.findAll();
    }

    /**
     * {@code GET  /gl-entry-types/:id} : get the "id" glEntryType.
     *
     * @param id the id of the glEntryType to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the glEntryType, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/gl-entry-types/{id}")
    public ResponseEntity<GlEntryType> getGlEntryType(@PathVariable Long id) {
        log.debug("REST request to get GlEntryType : {}", id);
        Optional<GlEntryType> glEntryType = glEntryTypeRepository.findById(id);
        return ResponseUtil.wrapOrNotFound(glEntryType);
    }

    /**
     * {@code DELETE  /gl-entry-types/:id} : delete the "id" glEntryType.
     *
     * @param id the id of the glEntryType to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/gl-entry-types/{id}")
    public ResponseEntity<Void> deleteGlEntryType(@PathVariable Long id) {
        log.debug("REST request to delete GlEntryType : {}", id);
        glEntryTypeRepository.deleteById(id);
        return ResponseEntity
            .noContent()
            .headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id.toString()))
            .build();
    }
}
