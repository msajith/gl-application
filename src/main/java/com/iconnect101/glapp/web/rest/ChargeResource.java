package com.iconnect101.glapp.web.rest;

import com.iconnect101.glapp.domain.Charge;
import com.iconnect101.glapp.repository.ChargeRepository;
import com.iconnect101.glapp.web.rest.errors.BadRequestAlertException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;
import tech.jhipster.web.util.HeaderUtil;
import tech.jhipster.web.util.ResponseUtil;

/**
 * REST controller for managing {@link com.iconnect101.glapp.domain.Charge}.
 */
@RestController
@RequestMapping("/api")
@Transactional
public class ChargeResource {

    private final Logger log = LoggerFactory.getLogger(ChargeResource.class);

    private static final String ENTITY_NAME = "charge";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final ChargeRepository chargeRepository;

    public ChargeResource(ChargeRepository chargeRepository) {
        this.chargeRepository = chargeRepository;
    }

    /**
     * {@code POST  /charges} : Create a new charge.
     *
     * @param charge the charge to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new charge, or with status {@code 400 (Bad Request)} if the charge has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/charges")
    public ResponseEntity<Charge> createCharge(@RequestBody Charge charge) throws URISyntaxException {
        log.debug("REST request to save Charge : {}", charge);
        if (charge.getId() != null) {
            throw new BadRequestAlertException("A new charge cannot already have an ID", ENTITY_NAME, "idexists");
        }
        Charge result = chargeRepository.save(charge);
        return ResponseEntity
            .created(new URI("/api/charges/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /charges/:id} : Updates an existing charge.
     *
     * @param id the id of the charge to save.
     * @param charge the charge to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated charge,
     * or with status {@code 400 (Bad Request)} if the charge is not valid,
     * or with status {@code 500 (Internal Server Error)} if the charge couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/charges/{id}")
    public ResponseEntity<Charge> updateCharge(@PathVariable(value = "id", required = false) final Long id, @RequestBody Charge charge)
        throws URISyntaxException {
        log.debug("REST request to update Charge : {}, {}", id, charge);
        if (charge.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, charge.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!chargeRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        Charge result = chargeRepository.save(charge);
        return ResponseEntity
            .ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, charge.getId().toString()))
            .body(result);
    }

    /**
     * {@code PATCH  /charges/:id} : Partial updates given fields of an existing charge, field will ignore if it is null
     *
     * @param id the id of the charge to save.
     * @param charge the charge to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated charge,
     * or with status {@code 400 (Bad Request)} if the charge is not valid,
     * or with status {@code 404 (Not Found)} if the charge is not found,
     * or with status {@code 500 (Internal Server Error)} if the charge couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PatchMapping(value = "/charges/{id}", consumes = { "application/json", "application/merge-patch+json" })
    public ResponseEntity<Charge> partialUpdateCharge(
        @PathVariable(value = "id", required = false) final Long id,
        @RequestBody Charge charge
    ) throws URISyntaxException {
        log.debug("REST request to partial update Charge partially : {}, {}", id, charge);
        if (charge.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, charge.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!chargeRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        Optional<Charge> result = chargeRepository
            .findById(charge.getId())
            .map(existingCharge -> {
                if (charge.getChargeAmountLocalCcy() != null) {
                    existingCharge.setChargeAmountLocalCcy(charge.getChargeAmountLocalCcy());
                }
                if (charge.getChargeAmountTxnCcy() != null) {
                    existingCharge.setChargeAmountTxnCcy(charge.getChargeAmountTxnCcy());
                }
                if (charge.getAmountSlab() != null) {
                    existingCharge.setAmountSlab(charge.getAmountSlab());
                }
                if (charge.getAmountSlabValue() != null) {
                    existingCharge.setAmountSlabValue(charge.getAmountSlabValue());
                }
                if (charge.getAmountSlabOperator() != null) {
                    existingCharge.setAmountSlabOperator(charge.getAmountSlabOperator());
                }
                if (charge.getAmountSlabCurrency() != null) {
                    existingCharge.setAmountSlabCurrency(charge.getAmountSlabCurrency());
                }
                if (charge.getRecordStatus() != null) {
                    existingCharge.setRecordStatus(charge.getRecordStatus());
                }
                if (charge.getValidfrom() != null) {
                    existingCharge.setValidfrom(charge.getValidfrom());
                }
                if (charge.getValidTo() != null) {
                    existingCharge.setValidTo(charge.getValidTo());
                }

                return existingCharge;
            })
            .map(chargeRepository::save);

        return ResponseUtil.wrapOrNotFound(
            result,
            HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, charge.getId().toString())
        );
    }

    /**
     * {@code GET  /charges} : get all the charges.
     *
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of charges in body.
     */
    @GetMapping("/charges")
    public List<Charge> getAllCharges() {
        log.debug("REST request to get all Charges");
        return chargeRepository.findAll();
    }

    /**
     * {@code GET  /charges/:id} : get the "id" charge.
     *
     * @param id the id of the charge to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the charge, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/charges/{id}")
    public ResponseEntity<Charge> getCharge(@PathVariable Long id) {
        log.debug("REST request to get Charge : {}", id);
        Optional<Charge> charge = chargeRepository.findById(id);
        return ResponseUtil.wrapOrNotFound(charge);
    }

    /**
     * {@code DELETE  /charges/:id} : delete the "id" charge.
     *
     * @param id the id of the charge to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/charges/{id}")
    public ResponseEntity<Void> deleteCharge(@PathVariable Long id) {
        log.debug("REST request to delete Charge : {}", id);
        chargeRepository.deleteById(id);
        return ResponseEntity
            .noContent()
            .headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id.toString()))
            .build();
    }
}
