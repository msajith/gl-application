package com.iconnect101.glapp.web.rest;

import com.iconnect101.glapp.domain.GlAccountCategory;
import com.iconnect101.glapp.repository.GlAccountCategoryRepository;
import com.iconnect101.glapp.web.rest.errors.BadRequestAlertException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;
import tech.jhipster.web.util.HeaderUtil;
import tech.jhipster.web.util.ResponseUtil;

/**
 * REST controller for managing {@link com.iconnect101.glapp.domain.GlAccountCategory}.
 */
@RestController
@RequestMapping("/api")
@Transactional
public class GlAccountCategoryResource {

    private final Logger log = LoggerFactory.getLogger(GlAccountCategoryResource.class);

    private static final String ENTITY_NAME = "glAccountCategory";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final GlAccountCategoryRepository glAccountCategoryRepository;

    public GlAccountCategoryResource(GlAccountCategoryRepository glAccountCategoryRepository) {
        this.glAccountCategoryRepository = glAccountCategoryRepository;
    }

    /**
     * {@code POST  /gl-account-categories} : Create a new glAccountCategory.
     *
     * @param glAccountCategory the glAccountCategory to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new glAccountCategory, or with status {@code 400 (Bad Request)} if the glAccountCategory has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/gl-account-categories")
    public ResponseEntity<GlAccountCategory> createGlAccountCategory(@RequestBody GlAccountCategory glAccountCategory)
        throws URISyntaxException {
        log.debug("REST request to save GlAccountCategory : {}", glAccountCategory);
        if (glAccountCategory.getId() != null) {
            throw new BadRequestAlertException("A new glAccountCategory cannot already have an ID", ENTITY_NAME, "idexists");
        }
        GlAccountCategory result = glAccountCategoryRepository.save(glAccountCategory);
        return ResponseEntity
            .created(new URI("/api/gl-account-categories/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /gl-account-categories/:id} : Updates an existing glAccountCategory.
     *
     * @param id the id of the glAccountCategory to save.
     * @param glAccountCategory the glAccountCategory to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated glAccountCategory,
     * or with status {@code 400 (Bad Request)} if the glAccountCategory is not valid,
     * or with status {@code 500 (Internal Server Error)} if the glAccountCategory couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/gl-account-categories/{id}")
    public ResponseEntity<GlAccountCategory> updateGlAccountCategory(
        @PathVariable(value = "id", required = false) final Long id,
        @RequestBody GlAccountCategory glAccountCategory
    ) throws URISyntaxException {
        log.debug("REST request to update GlAccountCategory : {}, {}", id, glAccountCategory);
        if (glAccountCategory.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, glAccountCategory.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!glAccountCategoryRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        GlAccountCategory result = glAccountCategoryRepository.save(glAccountCategory);
        return ResponseEntity
            .ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, glAccountCategory.getId().toString()))
            .body(result);
    }

    /**
     * {@code PATCH  /gl-account-categories/:id} : Partial updates given fields of an existing glAccountCategory, field will ignore if it is null
     *
     * @param id the id of the glAccountCategory to save.
     * @param glAccountCategory the glAccountCategory to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated glAccountCategory,
     * or with status {@code 400 (Bad Request)} if the glAccountCategory is not valid,
     * or with status {@code 404 (Not Found)} if the glAccountCategory is not found,
     * or with status {@code 500 (Internal Server Error)} if the glAccountCategory couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PatchMapping(value = "/gl-account-categories/{id}", consumes = { "application/json", "application/merge-patch+json" })
    public ResponseEntity<GlAccountCategory> partialUpdateGlAccountCategory(
        @PathVariable(value = "id", required = false) final Long id,
        @RequestBody GlAccountCategory glAccountCategory
    ) throws URISyntaxException {
        log.debug("REST request to partial update GlAccountCategory partially : {}, {}", id, glAccountCategory);
        if (glAccountCategory.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, glAccountCategory.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!glAccountCategoryRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        Optional<GlAccountCategory> result = glAccountCategoryRepository
            .findById(glAccountCategory.getId())
            .map(existingGlAccountCategory -> {
                if (glAccountCategory.getName() != null) {
                    existingGlAccountCategory.setName(glAccountCategory.getName());
                }
                if (glAccountCategory.getCode() != null) {
                    existingGlAccountCategory.setCode(glAccountCategory.getCode());
                }

                return existingGlAccountCategory;
            })
            .map(glAccountCategoryRepository::save);

        return ResponseUtil.wrapOrNotFound(
            result,
            HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, glAccountCategory.getId().toString())
        );
    }

    /**
     * {@code GET  /gl-account-categories} : get all the glAccountCategories.
     *
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of glAccountCategories in body.
     */
    @GetMapping("/gl-account-categories")
    public List<GlAccountCategory> getAllGlAccountCategories() {
        log.debug("REST request to get all GlAccountCategories");
        return glAccountCategoryRepository.findAll();
    }

    /**
     * {@code GET  /gl-account-categories/:id} : get the "id" glAccountCategory.
     *
     * @param id the id of the glAccountCategory to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the glAccountCategory, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/gl-account-categories/{id}")
    public ResponseEntity<GlAccountCategory> getGlAccountCategory(@PathVariable Long id) {
        log.debug("REST request to get GlAccountCategory : {}", id);
        Optional<GlAccountCategory> glAccountCategory = glAccountCategoryRepository.findById(id);
        return ResponseUtil.wrapOrNotFound(glAccountCategory);
    }

    /**
     * {@code DELETE  /gl-account-categories/:id} : delete the "id" glAccountCategory.
     *
     * @param id the id of the glAccountCategory to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/gl-account-categories/{id}")
    public ResponseEntity<Void> deleteGlAccountCategory(@PathVariable Long id) {
        log.debug("REST request to delete GlAccountCategory : {}", id);
        glAccountCategoryRepository.deleteById(id);
        return ResponseEntity
            .noContent()
            .headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id.toString()))
            .build();
    }
}
