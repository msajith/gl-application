package com.iconnect101.glapp.web.rest;

import com.iconnect101.glapp.domain.Merchant;
import com.iconnect101.glapp.repository.MerchantRepository;
import com.iconnect101.glapp.web.rest.errors.BadRequestAlertException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;
import tech.jhipster.web.util.HeaderUtil;
import tech.jhipster.web.util.ResponseUtil;

/**
 * REST controller for managing {@link com.iconnect101.glapp.domain.Merchant}.
 */
@RestController
@RequestMapping("/api")
@Transactional
public class MerchantResource {

    private final Logger log = LoggerFactory.getLogger(MerchantResource.class);

    private static final String ENTITY_NAME = "merchant";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final MerchantRepository merchantRepository;

    public MerchantResource(MerchantRepository merchantRepository) {
        this.merchantRepository = merchantRepository;
    }

    /**
     * {@code POST  /merchants} : Create a new merchant.
     *
     * @param merchant the merchant to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new merchant, or with status {@code 400 (Bad Request)} if the merchant has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/merchants")
    public ResponseEntity<Merchant> createMerchant(@RequestBody Merchant merchant) throws URISyntaxException {
        log.debug("REST request to save Merchant : {}", merchant);
        if (merchant.getId() != null) {
            throw new BadRequestAlertException("A new merchant cannot already have an ID", ENTITY_NAME, "idexists");
        }
        Merchant result = merchantRepository.save(merchant);
        return ResponseEntity
            .created(new URI("/api/merchants/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /merchants/:id} : Updates an existing merchant.
     *
     * @param id the id of the merchant to save.
     * @param merchant the merchant to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated merchant,
     * or with status {@code 400 (Bad Request)} if the merchant is not valid,
     * or with status {@code 500 (Internal Server Error)} if the merchant couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/merchants/{id}")
    public ResponseEntity<Merchant> updateMerchant(
        @PathVariable(value = "id", required = false) final Long id,
        @RequestBody Merchant merchant
    ) throws URISyntaxException {
        log.debug("REST request to update Merchant : {}, {}", id, merchant);
        if (merchant.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, merchant.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!merchantRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        Merchant result = merchantRepository.save(merchant);
        return ResponseEntity
            .ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, merchant.getId().toString()))
            .body(result);
    }

    /**
     * {@code PATCH  /merchants/:id} : Partial updates given fields of an existing merchant, field will ignore if it is null
     *
     * @param id the id of the merchant to save.
     * @param merchant the merchant to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated merchant,
     * or with status {@code 400 (Bad Request)} if the merchant is not valid,
     * or with status {@code 404 (Not Found)} if the merchant is not found,
     * or with status {@code 500 (Internal Server Error)} if the merchant couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PatchMapping(value = "/merchants/{id}", consumes = { "application/json", "application/merge-patch+json" })
    public ResponseEntity<Merchant> partialUpdateMerchant(
        @PathVariable(value = "id", required = false) final Long id,
        @RequestBody Merchant merchant
    ) throws URISyntaxException {
        log.debug("REST request to partial update Merchant partially : {}, {}", id, merchant);
        if (merchant.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, merchant.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!merchantRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        Optional<Merchant> result = merchantRepository
            .findById(merchant.getId())
            .map(existingMerchant -> {
                if (merchant.getMerchantID() != null) {
                    existingMerchant.setMerchantID(merchant.getMerchantID());
                }
                if (merchant.getMerchantName() != null) {
                    existingMerchant.setMerchantName(merchant.getMerchantName());
                }
                if (merchant.getMerchantCategory() != null) {
                    existingMerchant.setMerchantCategory(merchant.getMerchantCategory());
                }
                if (merchant.getMerchantGLAccountNo() != null) {
                    existingMerchant.setMerchantGLAccountNo(merchant.getMerchantGLAccountNo());
                }
                if (merchant.getMerchantBankAccountNo() != null) {
                    existingMerchant.setMerchantBankAccountNo(merchant.getMerchantBankAccountNo());
                }
                if (merchant.getMerchantBankAccountCurrency() != null) {
                    existingMerchant.setMerchantBankAccountCurrency(merchant.getMerchantBankAccountCurrency());
                }
                if (merchant.getMerchantBankCode() != null) {
                    existingMerchant.setMerchantBankCode(merchant.getMerchantBankCode());
                }
                if (merchant.getMerchantBankName() != null) {
                    existingMerchant.setMerchantBankName(merchant.getMerchantBankName());
                }
                if (merchant.getMerchantAddress() != null) {
                    existingMerchant.setMerchantAddress(merchant.getMerchantAddress());
                }
                if (merchant.getMerchantCity() != null) {
                    existingMerchant.setMerchantCity(merchant.getMerchantCity());
                }
                if (merchant.getCreatedOn() != null) {
                    existingMerchant.setCreatedOn(merchant.getCreatedOn());
                }
                if (merchant.getCreatedBy() != null) {
                    existingMerchant.setCreatedBy(merchant.getCreatedBy());
                }
                if (merchant.getMerchantStataus() != null) {
                    existingMerchant.setMerchantStataus(merchant.getMerchantStataus());
                }

                return existingMerchant;
            })
            .map(merchantRepository::save);

        return ResponseUtil.wrapOrNotFound(
            result,
            HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, merchant.getId().toString())
        );
    }

    /**
     * {@code GET  /merchants} : get all the merchants.
     *
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of merchants in body.
     */
    @GetMapping("/merchants")
    public List<Merchant> getAllMerchants() {
        log.debug("REST request to get all Merchants");
        return merchantRepository.findAll();
    }

    /**
     * {@code GET  /merchants/:id} : get the "id" merchant.
     *
     * @param id the id of the merchant to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the merchant, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/merchants/{id}")
    public ResponseEntity<Merchant> getMerchant(@PathVariable Long id) {
        log.debug("REST request to get Merchant : {}", id);
        Optional<Merchant> merchant = merchantRepository.findById(id);
        return ResponseUtil.wrapOrNotFound(merchant);
    }

    /**
     * {@code DELETE  /merchants/:id} : delete the "id" merchant.
     *
     * @param id the id of the merchant to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/merchants/{id}")
    public ResponseEntity<Void> deleteMerchant(@PathVariable Long id) {
        log.debug("REST request to delete Merchant : {}", id);
        merchantRepository.deleteById(id);
        return ResponseEntity
            .noContent()
            .headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id.toString()))
            .build();
    }
}
