package com.iconnect101.glapp.web.rest.custom;

import com.iconnect101.glapp.domain.Charge;
import com.iconnect101.glapp.repository.ChargeRepository;
import com.iconnect101.glapp.repository.custom.ChargeExtendedRepository;
import com.iconnect101.glapp.web.rest.ChargeResource;
import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import tech.jhipster.web.util.ResponseUtil;

@RestController
@RequestMapping("/api/v1")
public class ChargeExtendedResource extends ChargeResource {

    private final Logger log = LoggerFactory.getLogger(RemittanceTransactionExtendedResource.class);

    private static final String ENTITY_NAME = "charge";
    private final ChargeExtendedRepository chargeExtendedRepository;

    public ChargeExtendedResource(ChargeRepository chargeRepository, ChargeExtendedRepository chargeExtendedRepository) {
        super(chargeRepository);
        this.chargeExtendedRepository = chargeExtendedRepository;
        // TODO Auto-generated constructor stub
    }

    @GetMapping("/charges/{bencountry}/{benccy}")
    public ResponseEntity<Charge> getFxRate(@PathVariable Long bencountry, @PathVariable Long benccy) {
        log.debug("REST request to get charges : {}", bencountry);
        Optional<Charge> charge = chargeExtendedRepository.getCharge(bencountry, benccy);
        return ResponseUtil.wrapOrNotFound(charge);
    }
}
