package com.iconnect101.glapp.web.rest.custom;

import com.iconnect101.glapp.domain.CasaAccount;
import com.iconnect101.glapp.repository.CasaAccountRepository;
import com.iconnect101.glapp.repository.custom.CasaAccountExtendedRepository;
import com.iconnect101.glapp.web.rest.CasaAccountResource;
import java.util.List;
import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import tech.jhipster.web.util.ResponseUtil;

@RestController
@RequestMapping("/api/v1")
public class CasaAccountExtendedResource extends CasaAccountResource {

    private final Logger log = LoggerFactory.getLogger(CasaAccountExtendedResource.class);

    private static final String ENTITY_NAME = "casaAccount";
    private final CasaAccountExtendedRepository casaAccountExtendedRepository;

    public CasaAccountExtendedResource(
        CasaAccountRepository casaAccountRepository,
        CasaAccountExtendedRepository casaAccountExtendedRepository
    ) {
        super(casaAccountRepository);
        this.casaAccountExtendedRepository = casaAccountExtendedRepository;
        // TODO Auto-generated constructor stub
    }

    @GetMapping("/casa-accounts/customer/{id}")
    public List<CasaAccount> getCasaAccounts(@PathVariable Long id) {
        log.debug("REST request to get CasaAccount : {}", id);
        List<CasaAccount> casaAccounts = casaAccountExtendedRepository.findByCustomerId(id);
        return casaAccounts;
    }
}
