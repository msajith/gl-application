package com.iconnect101.glapp.web.rest;

import com.iconnect101.glapp.domain.FundingSourceType;
import com.iconnect101.glapp.repository.FundingSourceTypeRepository;
import com.iconnect101.glapp.web.rest.errors.BadRequestAlertException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;
import tech.jhipster.web.util.HeaderUtil;
import tech.jhipster.web.util.ResponseUtil;

/**
 * REST controller for managing {@link com.iconnect101.glapp.domain.FundingSourceType}.
 */
@RestController
@RequestMapping("/api")
@Transactional
public class FundingSourceTypeResource {

    private final Logger log = LoggerFactory.getLogger(FundingSourceTypeResource.class);

    private static final String ENTITY_NAME = "fundingSourceType";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final FundingSourceTypeRepository fundingSourceTypeRepository;

    public FundingSourceTypeResource(FundingSourceTypeRepository fundingSourceTypeRepository) {
        this.fundingSourceTypeRepository = fundingSourceTypeRepository;
    }

    /**
     * {@code POST  /funding-source-types} : Create a new fundingSourceType.
     *
     * @param fundingSourceType the fundingSourceType to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new fundingSourceType, or with status {@code 400 (Bad Request)} if the fundingSourceType has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/funding-source-types")
    public ResponseEntity<FundingSourceType> createFundingSourceType(@RequestBody FundingSourceType fundingSourceType)
        throws URISyntaxException {
        log.debug("REST request to save FundingSourceType : {}", fundingSourceType);
        if (fundingSourceType.getId() != null) {
            throw new BadRequestAlertException("A new fundingSourceType cannot already have an ID", ENTITY_NAME, "idexists");
        }
        FundingSourceType result = fundingSourceTypeRepository.save(fundingSourceType);
        return ResponseEntity
            .created(new URI("/api/funding-source-types/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /funding-source-types/:id} : Updates an existing fundingSourceType.
     *
     * @param id the id of the fundingSourceType to save.
     * @param fundingSourceType the fundingSourceType to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated fundingSourceType,
     * or with status {@code 400 (Bad Request)} if the fundingSourceType is not valid,
     * or with status {@code 500 (Internal Server Error)} if the fundingSourceType couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/funding-source-types/{id}")
    public ResponseEntity<FundingSourceType> updateFundingSourceType(
        @PathVariable(value = "id", required = false) final Long id,
        @RequestBody FundingSourceType fundingSourceType
    ) throws URISyntaxException {
        log.debug("REST request to update FundingSourceType : {}, {}", id, fundingSourceType);
        if (fundingSourceType.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, fundingSourceType.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!fundingSourceTypeRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        FundingSourceType result = fundingSourceTypeRepository.save(fundingSourceType);
        return ResponseEntity
            .ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, fundingSourceType.getId().toString()))
            .body(result);
    }

    /**
     * {@code PATCH  /funding-source-types/:id} : Partial updates given fields of an existing fundingSourceType, field will ignore if it is null
     *
     * @param id the id of the fundingSourceType to save.
     * @param fundingSourceType the fundingSourceType to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated fundingSourceType,
     * or with status {@code 400 (Bad Request)} if the fundingSourceType is not valid,
     * or with status {@code 404 (Not Found)} if the fundingSourceType is not found,
     * or with status {@code 500 (Internal Server Error)} if the fundingSourceType couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PatchMapping(value = "/funding-source-types/{id}", consumes = { "application/json", "application/merge-patch+json" })
    public ResponseEntity<FundingSourceType> partialUpdateFundingSourceType(
        @PathVariable(value = "id", required = false) final Long id,
        @RequestBody FundingSourceType fundingSourceType
    ) throws URISyntaxException {
        log.debug("REST request to partial update FundingSourceType partially : {}, {}", id, fundingSourceType);
        if (fundingSourceType.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, fundingSourceType.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!fundingSourceTypeRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        Optional<FundingSourceType> result = fundingSourceTypeRepository
            .findById(fundingSourceType.getId())
            .map(existingFundingSourceType -> {
                if (fundingSourceType.getName() != null) {
                    existingFundingSourceType.setName(fundingSourceType.getName());
                }
                if (fundingSourceType.getCode() != null) {
                    existingFundingSourceType.setCode(fundingSourceType.getCode());
                }

                return existingFundingSourceType;
            })
            .map(fundingSourceTypeRepository::save);

        return ResponseUtil.wrapOrNotFound(
            result,
            HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, fundingSourceType.getId().toString())
        );
    }

    /**
     * {@code GET  /funding-source-types} : get all the fundingSourceTypes.
     *
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of fundingSourceTypes in body.
     */
    @GetMapping("/funding-source-types")
    public List<FundingSourceType> getAllFundingSourceTypes() {
        log.debug("REST request to get all FundingSourceTypes");
        return fundingSourceTypeRepository.findAll();
    }

    /**
     * {@code GET  /funding-source-types/:id} : get the "id" fundingSourceType.
     *
     * @param id the id of the fundingSourceType to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the fundingSourceType, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/funding-source-types/{id}")
    public ResponseEntity<FundingSourceType> getFundingSourceType(@PathVariable Long id) {
        log.debug("REST request to get FundingSourceType : {}", id);
        Optional<FundingSourceType> fundingSourceType = fundingSourceTypeRepository.findById(id);
        return ResponseUtil.wrapOrNotFound(fundingSourceType);
    }

    /**
     * {@code DELETE  /funding-source-types/:id} : delete the "id" fundingSourceType.
     *
     * @param id the id of the fundingSourceType to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/funding-source-types/{id}")
    public ResponseEntity<Void> deleteFundingSourceType(@PathVariable Long id) {
        log.debug("REST request to delete FundingSourceType : {}", id);
        fundingSourceTypeRepository.deleteById(id);
        return ResponseEntity
            .noContent()
            .headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id.toString()))
            .build();
    }
}
