package com.iconnect101.glapp.web.rest.custom;

import com.iconnect101.glapp.config.custom.AppConstants;
import com.iconnect101.glapp.config.custom.RandomGenerator;
import com.iconnect101.glapp.domain.AppConfig;
import com.iconnect101.glapp.domain.CasaAccount;
import com.iconnect101.glapp.domain.CasaAccountDayBalance;
import com.iconnect101.glapp.domain.FundingSourceType;
import com.iconnect101.glapp.domain.RemittanceTransaction;
import com.iconnect101.glapp.domain.TransactionType;
import com.iconnect101.glapp.domain.WalletAccount;
import com.iconnect101.glapp.domain.enumeration.GlEntryType;
import com.iconnect101.glapp.domain.enumeration.TxnStatus;
import com.iconnect101.glapp.repository.custom.AppConfigExtendedRepository;
import com.iconnect101.glapp.repository.custom.CasaAccountDayBalanceExtendedRepository;
import com.iconnect101.glapp.repository.custom.CasaAccountExtendedRepository;
import com.iconnect101.glapp.repository.custom.RemittanceTransactionExtendedRepository;
import com.iconnect101.glapp.repository.custom.SubLedgerExtendedRepository;
import com.iconnect101.glapp.repository.custom.WalletAccountExtendedRepository;
import com.iconnect101.glapp.web.rest.RemittanceTransactionResource;
import com.iconnect101.glapp.web.rest.errors.BadRequestAlertException;
import java.math.BigDecimal;
import java.net.URI;
import java.net.URISyntaxException;
import java.time.LocalDate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import tech.jhipster.web.util.HeaderUtil;

@RestController
@RequestMapping("/api/v1")
public class RemittanceTransactionExtendedResource extends RemittanceTransactionResource {

    private final Logger log = LoggerFactory.getLogger(RemittanceTransactionExtendedResource.class);

    private static final String ENTITY_NAME = "remittanceTransaction";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final RemittanceTransactionExtendedRepository remittanceTransactionExtendedRepository;
    private final WalletAccountExtendedRepository walletAccountExtendedRepository;
    private final SubLedgerExtendedResource subLedgerExtendedResource;
    private final AppConfigExtendedRepository appConfigExtendedRepository;
    private final CasaAccountExtendedRepository casaAccountExtendedRepository;
    private final CasaAccountDayBalanceExtendedRepository casaAccountDayBalanceExtendedRepository;

    public RemittanceTransactionExtendedResource(
        RemittanceTransactionExtendedRepository remittanceTransactionExtendedRepository,
        WalletAccountExtendedRepository walletAccountExtendedRepository,
        SubLedgerExtendedRepository subLedgerExtendedRepository,
        SubLedgerExtendedResource subLedgerExtendedResource,
        AppConfigExtendedRepository appConfigExtendedRepository,
        CasaAccountExtendedRepository casaAccountExtendedRepository,
        CasaAccountDayBalanceExtendedRepository casaAccountDayBalanceExtendedRepository
    ) {
        super(remittanceTransactionExtendedRepository);
        this.remittanceTransactionExtendedRepository = remittanceTransactionExtendedRepository;
        this.walletAccountExtendedRepository = walletAccountExtendedRepository;
        this.subLedgerExtendedResource = subLedgerExtendedResource;
        this.appConfigExtendedRepository = appConfigExtendedRepository;
        this.casaAccountExtendedRepository = casaAccountExtendedRepository;
        this.casaAccountDayBalanceExtendedRepository = casaAccountDayBalanceExtendedRepository;
    }

    /**
     * {@code POST  /remittance-transactions} : Create a new remittanceTransaction.
     *
     * @param remittanceTransaction the remittanceTransaction to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new remittanceTransaction, or with status {@code 400 (Bad Request)} if the remittanceTransaction has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/remittance-transactions/casa")
    public ResponseEntity<RemittanceTransaction> sendRemittanceFromCasa(@RequestBody RemittanceTransaction remittanceTransaction)
        throws URISyntaxException {
        log.debug("REST request to save RemittanceTransaction : {}", remittanceTransaction);
        if (remittanceTransaction.getId() != null) {
            throw new BadRequestAlertException("A new remittanceTransaction cannot already have an ID", ENTITY_NAME, "idexists");
        }
        FundingSourceType fsType = new FundingSourceType();
        fsType.setId(AppConstants.FUNDING_SOURCE_TYPE_CASA);

        TransactionType txnType = new TransactionType();
        txnType.setId(AppConstants.TXN_TYPE_INL_REM_CASA);

        remittanceTransaction.setTxnRefNo(RandomGenerator.generateRandom(10));
        remittanceTransaction.setFundingSourceType(fsType);
        remittanceTransaction.setTransactionType(txnType);
        remittanceTransaction.setTxnStatus(TxnStatus.ACTIVE);
        //
        RemittanceTransaction result = remittanceTransactionExtendedRepository.save(remittanceTransaction);

        // Step1 - Adjust Customer Account balance for txnAmount and charge.
        // Step 1.1  Update the CASA account table
        CasaAccount customerAccount = casaAccountExtendedRepository.findByAccountNo(result.getAccountNo());
        BigDecimal totalDebitAmount = result.getAmountInAccountCurrency().add(result.getChargeInAccountCurrency());
        customerAccount.setAvailableBalance(customerAccount.getAvailableBalance().subtract(totalDebitAmount));
        customerAccount.setCurrentBalance(customerAccount.getCurrentBalance().subtract(totalDebitAmount));
        customerAccount.setTotalDebit(customerAccount.getTotalDebit().add(totalDebitAmount));
        casaAccountExtendedRepository.save(customerAccount);

        // Step 1.2  Update the CASA account Daily balance table
        LocalDate today = LocalDate.now();
        CasaAccountDayBalance customerAccdayBalance = casaAccountDayBalanceExtendedRepository.getDaybalance(customerAccount.getId(), today);
        if (customerAccdayBalance != null) {
            // Update the balance here.
            customerAccdayBalance.setAvailableBalance(customerAccdayBalance.getAvailableBalance().subtract(totalDebitAmount));
            customerAccdayBalance.setCurrentBalance(customerAccdayBalance.getCurrentBalance().subtract(totalDebitAmount));
            customerAccdayBalance.setTotalDebit(customerAccdayBalance.getTotalDebit().add(totalDebitAmount));
            casaAccountDayBalanceExtendedRepository.save(customerAccdayBalance);
        } else {
            // Create a new cord and save.
            CasaAccountDayBalance newCustomerDaybalance = new CasaAccountDayBalance();
            newCustomerDaybalance.setDate(today);
            newCustomerDaybalance.setOpeningBalance(BigDecimal.ZERO);
            newCustomerDaybalance.setClosingBalance(BigDecimal.ZERO);
            newCustomerDaybalance.setCasaAccount(customerAccount);
            newCustomerDaybalance.setAccountNo(customerAccount.getAccountNo());
            newCustomerDaybalance.setAvailableBalance(totalDebitAmount);
            newCustomerDaybalance.setCurrentBalance(totalDebitAmount);
            newCustomerDaybalance.setTotalDebit(totalDebitAmount);
            casaAccountDayBalanceExtendedRepository.save(newCustomerDaybalance);
        }

        // Step2 Credit the transaction amount to the correspondent banking account
        //Get the account number of correspondent bank
        AppConfig remittanceCollectionAcConfig = appConfigExtendedRepository.findByCodeAndSubCode(
            AppConstants.REMITTANCE_CORR_BANK_AC,
            result.getTxnCurrency().getCode()
        );
        // Step 2.1  Update the CASA account table
        CasaAccount correspondentBankAccount = casaAccountExtendedRepository.findByAccountNo(remittanceCollectionAcConfig.getValue());
        correspondentBankAccount.setAvailableBalance(customerAccount.getAvailableBalance().add(result.getAmountInAccountCurrency()));
        correspondentBankAccount.setCurrentBalance(customerAccount.getCurrentBalance().add(result.getAmountInAccountCurrency()));
        correspondentBankAccount.setTotalCredit(customerAccount.getTotalDebit().add(result.getAmountInAccountCurrency()));
        casaAccountExtendedRepository.save(correspondentBankAccount);

        // Step 2.2  Update the CASA account Daily balance table
        CasaAccountDayBalance corrBankAccdayBalance = casaAccountDayBalanceExtendedRepository.getDaybalance(
            correspondentBankAccount.getId(),
            today
        );
        if (corrBankAccdayBalance != null) {
            // Update the balance here.
            corrBankAccdayBalance.setAvailableBalance(corrBankAccdayBalance.getAvailableBalance().add(result.getAmountInAccountCurrency()));
            corrBankAccdayBalance.setCurrentBalance(corrBankAccdayBalance.getCurrentBalance().add(result.getAmountInAccountCurrency()));
            corrBankAccdayBalance.setTotalCredit(corrBankAccdayBalance.getTotalDebit().add(result.getAmountInAccountCurrency()));
            casaAccountDayBalanceExtendedRepository.save(corrBankAccdayBalance);
        } else {
            // Create a new cord and save.
            CasaAccountDayBalance newCorrBankAccdayBalance = new CasaAccountDayBalance();
            newCorrBankAccdayBalance.setDate(today);
            newCorrBankAccdayBalance.setOpeningBalance(BigDecimal.ZERO);
            newCorrBankAccdayBalance.setClosingBalance(BigDecimal.ZERO);
            newCorrBankAccdayBalance.setCasaAccount(correspondentBankAccount);
            newCorrBankAccdayBalance.setAccountNo(correspondentBankAccount.getAccountNo());
            newCorrBankAccdayBalance.setAvailableBalance(result.getAmountInAccountCurrency());
            newCorrBankAccdayBalance.setCurrentBalance(result.getAmountInAccountCurrency());
            newCorrBankAccdayBalance.setTotalCredit(result.getAmountInAccountCurrency());
            casaAccountDayBalanceExtendedRepository.save(newCorrBankAccdayBalance);
        }

        // Step3 Credit the charge amount to the charge account
        AppConfig remittanceChargeAcConfig = appConfigExtendedRepository.findByCodeAndSubCode(
            AppConstants.REMITTANCE_CHARGE_AC,
            result.getTxnCurrency().getCode()
        );
        // Step 3.1  Update the CASA account table
        CasaAccount chargeAccount = casaAccountExtendedRepository.findByAccountNo(remittanceChargeAcConfig.getValue());
        chargeAccount.setAvailableBalance(chargeAccount.getAvailableBalance().add(result.getChargeInAccountCurrency()));
        chargeAccount.setCurrentBalance(chargeAccount.getCurrentBalance().add(result.getChargeInAccountCurrency()));
        chargeAccount.setTotalCredit(chargeAccount.getTotalDebit().add(result.getChargeInAccountCurrency()));
        casaAccountExtendedRepository.save(chargeAccount);

        // Step 3.2  Update the CASA account Daily balance table
        CasaAccountDayBalance chargeAccountDayBalance = casaAccountDayBalanceExtendedRepository.getDaybalance(chargeAccount.getId(), today);
        if (chargeAccountDayBalance != null) {
            // Update the balance here.
            chargeAccountDayBalance.setAvailableBalance(
                corrBankAccdayBalance.getAvailableBalance().add(result.getChargeInAccountCurrency())
            );
            chargeAccountDayBalance.setCurrentBalance(corrBankAccdayBalance.getCurrentBalance().add(result.getChargeInAccountCurrency()));
            chargeAccountDayBalance.setTotalCredit(corrBankAccdayBalance.getTotalDebit().add(result.getChargeInAccountCurrency()));
            casaAccountDayBalanceExtendedRepository.save(corrBankAccdayBalance);
        } else {
            // Create a new cord and save.
            CasaAccountDayBalance newChargeAccountDayBalance = new CasaAccountDayBalance();
            newChargeAccountDayBalance.setDate(today);
            newChargeAccountDayBalance.setOpeningBalance(BigDecimal.ZERO);
            newChargeAccountDayBalance.setClosingBalance(BigDecimal.ZERO);
            newChargeAccountDayBalance.setCasaAccount(chargeAccount);
            newChargeAccountDayBalance.setAccountNo(chargeAccount.getAccountNo());
            newChargeAccountDayBalance.setAvailableBalance(result.getChargeInAccountCurrency());
            newChargeAccountDayBalance.setCurrentBalance(result.getChargeInAccountCurrency());
            newChargeAccountDayBalance.setTotalCredit(result.getChargeInAccountCurrency());
            casaAccountDayBalanceExtendedRepository.save(newChargeAccountDayBalance);
        }

        return ResponseEntity
            .created(new URI("/api/v1/remittance-transactions/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    public ResponseEntity<RemittanceTransaction> createRemittanceTransactionBackup(
        @RequestBody RemittanceTransaction remittanceTransaction
    ) throws URISyntaxException {
        log.debug("REST request to save RemittanceTransaction : {}", remittanceTransaction);
        if (remittanceTransaction.getId() != null) {
            throw new BadRequestAlertException("A new remittanceTransaction cannot already have an ID", ENTITY_NAME, "idexists");
        }
        RemittanceTransaction result = remittanceTransactionExtendedRepository.save(remittanceTransaction);

        if (result.getFundingSourceType().getCode().equalsIgnoreCase(AppConstants.WALLET_ACCOUNT)) {
            //Get the source account details form WalletAccount.
            WalletAccount walletAccount = walletAccountExtendedRepository.findByAccountNo(result.getAccountNo());

            // Source is Wallet Account
            if (result.getTransactionType().getCode().equalsIgnoreCase(AppConstants.INL_REM_WAL)) {
                /* This is an international remittance to beneficiary bank account . We need to do the following this here.
                 *  Step 1. Adjust the wallet account balances - availBal,curBal,totalDebit/totalCredit,closingBal,openingBal with txn & charge amount.
                 *  Step 2. Post the  DR (debit)  entry in the WALLET Collection Sub GL Account for transaction amount
                 *  Step 3. Post the CR (credit) entry in the Corresponding partner (Bank) Account for transaction amount
                 *  Step 4. Post the DR  (debit)  entry in the WALLET Collection Sub GL Account for charge amount
                 *  Step 5. Post the CR (credit) entry in the Charge account Account for charge amount
                 */

                String walletCollectionAccount = null;
                if (result.getInstructedCurrency().getCode().equalsIgnoreCase(AppConstants.AED)) {
                    walletCollectionAccount = AppConstants.WALLET_COLLECTION_ACCONUT_AED;
                } else if (result.getInstructedCurrency().getCode().equalsIgnoreCase(AppConstants.USD)) {
                    walletCollectionAccount = AppConstants.WALLET_COLLECTION_ACCONUT_USD;
                } else if (result.getInstructedCurrency().getCode().equalsIgnoreCase(AppConstants.RND)) {
                    walletCollectionAccount = AppConstants.WALLET_COLLECTION_ACCONUT_RND;
                }

                /*Step 1 - Adjust the wallet account balances - subtract transaction amount + charges.
                 **----------------------------------------------------------------------------
                 */

                BigDecimal totalDebitAmount = result.getAmountInAccountCurrency().add(result.getChargeInAccountCurrency());
                walletAccount.setAvailableBalance(walletAccount.getAvailableBalance().subtract(totalDebitAmount));
                walletAccount.setCurrentBalance(walletAccount.getCurrentBalance().subtract(totalDebitAmount));
                walletAccount.setTotalDebit(walletAccount.getTotalDebit().subtract(totalDebitAmount));
                walletAccountExtendedRepository.save(walletAccount);

                /*Step 2. Post the  DR (debit)  entry in the WALLET Collection Sub GL Account subtract transaction amount
                 **---------------------------------------------------------------------------------------------------------
                 */

                subLedgerExtendedResource.saveGLEntry(
                    result.getTxnRefNo(),
                    result.getTxnDescription(),
                    result.getTxnInternalDescription(),
                    result.getTxnDate(),
                    result.getAmountInAccountCurrency(),
                    result.getAmountInLocalCurrency(),
                    result.getRate(),
                    walletCollectionAccount,
                    GlEntryType.DR
                );

                /*Step 3. Post the CR (credit) entry in the Corresponding partner (Bank) Account - add transaction amount
                 * **------------------------------------------------------------------------------------------------------
                 */
                //Get the account number of correspondent bank
                AppConfig remittanceCollectionAcConfig = appConfigExtendedRepository.findByCodeAndSubCode(
                    AppConstants.REMITTANCE_CORR_BANK_AC,
                    result.getTxnCurrency().getCode()
                );
                subLedgerExtendedResource.saveGLEntry(
                    result.getTxnRefNo(),
                    result.getTxnDescription(),
                    result.getTxnInternalDescription(),
                    result.getTxnDate(),
                    result.getAmountInAccountCurrency(),
                    result.getAmountInLocalCurrency(),
                    result.getRate(),
                    remittanceCollectionAcConfig.getValue(),
                    GlEntryType.CR
                );

                /*Step 4. Post the  DR (debit)  entry in the WALLET Collection Sub GL Account subtract charge amount
                 * **------------------------------------------------------------------------------------------------------
                 */
                subLedgerExtendedResource.saveGLEntry(
                    AppConstants.CHARGE_REF_PREFEIX + result.getTxnRefNo(),
                    AppConstants.CHARGE_DESC_PREFEIX + result.getTxnDescription(),
                    AppConstants.CHARGE_DESC_PREFEIX + result.getTxnInternalDescription(),
                    result.getTxnDate(),
                    result.getChargeInAccountCurrency(),
                    result.getChargeInLocalCurrency(),
                    result.getRate(),
                    walletCollectionAccount,
                    GlEntryType.DR
                );

                /*Step 3. Post the CR (credit) entry in the Charge Account - add charge amount
                 * **------------------------------------------------------------------------------------------------------
                 */
                //Get the charge account number of  remittances
                AppConfig remittanceChargeAcConfig = appConfigExtendedRepository.findByCodeAndSubCode(
                    AppConstants.REMITTANCE_CHARGE_AC,
                    result.getTxnCurrency().getCode()
                );
                subLedgerExtendedResource.saveGLEntry(
                    AppConstants.CHARGE_REF_PREFEIX + result.getTxnRefNo(),
                    AppConstants.CHARGE_DESC_PREFEIX + result.getTxnDescription(),
                    AppConstants.CHARGE_DESC_PREFEIX + result.getTxnInternalDescription(),
                    result.getTxnDate(),
                    result.getChargeInAccountCurrency(),
                    result.getChargeInLocalCurrency(),
                    result.getRate(),
                    remittanceChargeAcConfig.getValue(),
                    GlEntryType.CR
                );
            }
        }

        return ResponseEntity
            .created(new URI("/api/v1/remittance-transactions/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }
}
