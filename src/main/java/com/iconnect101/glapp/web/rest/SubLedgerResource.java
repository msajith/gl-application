package com.iconnect101.glapp.web.rest;

import com.iconnect101.glapp.domain.SubLedger;
import com.iconnect101.glapp.repository.SubLedgerRepository;
import com.iconnect101.glapp.web.rest.errors.BadRequestAlertException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;
import tech.jhipster.web.util.HeaderUtil;
import tech.jhipster.web.util.ResponseUtil;

/**
 * REST controller for managing {@link com.iconnect101.glapp.domain.SubLedger}.
 */
@RestController
@RequestMapping("/api")
@Transactional
public class SubLedgerResource {

    private final Logger log = LoggerFactory.getLogger(SubLedgerResource.class);

    private static final String ENTITY_NAME = "subLedger";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final SubLedgerRepository subLedgerRepository;

    public SubLedgerResource(SubLedgerRepository subLedgerRepository) {
        this.subLedgerRepository = subLedgerRepository;
    }

    /**
     * {@code POST  /sub-ledgers} : Create a new subLedger.
     *
     * @param subLedger the subLedger to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new subLedger, or with status {@code 400 (Bad Request)} if the subLedger has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/sub-ledgers")
    public ResponseEntity<SubLedger> createSubLedger(@RequestBody SubLedger subLedger) throws URISyntaxException {
        log.debug("REST request to save SubLedger : {}", subLedger);
        if (subLedger.getId() != null) {
            throw new BadRequestAlertException("A new subLedger cannot already have an ID", ENTITY_NAME, "idexists");
        }
        SubLedger result = subLedgerRepository.save(subLedger);
        return ResponseEntity
            .created(new URI("/api/sub-ledgers/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /sub-ledgers/:id} : Updates an existing subLedger.
     *
     * @param id the id of the subLedger to save.
     * @param subLedger the subLedger to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated subLedger,
     * or with status {@code 400 (Bad Request)} if the subLedger is not valid,
     * or with status {@code 500 (Internal Server Error)} if the subLedger couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/sub-ledgers/{id}")
    public ResponseEntity<SubLedger> updateSubLedger(
        @PathVariable(value = "id", required = false) final Long id,
        @RequestBody SubLedger subLedger
    ) throws URISyntaxException {
        log.debug("REST request to update SubLedger : {}, {}", id, subLedger);
        if (subLedger.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, subLedger.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!subLedgerRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        SubLedger result = subLedgerRepository.save(subLedger);
        return ResponseEntity
            .ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, subLedger.getId().toString()))
            .body(result);
    }

    /**
     * {@code PATCH  /sub-ledgers/:id} : Partial updates given fields of an existing subLedger, field will ignore if it is null
     *
     * @param id the id of the subLedger to save.
     * @param subLedger the subLedger to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated subLedger,
     * or with status {@code 400 (Bad Request)} if the subLedger is not valid,
     * or with status {@code 404 (Not Found)} if the subLedger is not found,
     * or with status {@code 500 (Internal Server Error)} if the subLedger couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PatchMapping(value = "/sub-ledgers/{id}", consumes = { "application/json", "application/merge-patch+json" })
    public ResponseEntity<SubLedger> partialUpdateSubLedger(
        @PathVariable(value = "id", required = false) final Long id,
        @RequestBody SubLedger subLedger
    ) throws URISyntaxException {
        log.debug("REST request to partial update SubLedger partially : {}, {}", id, subLedger);
        if (subLedger.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, subLedger.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!subLedgerRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        Optional<SubLedger> result = subLedgerRepository
            .findById(subLedger.getId())
            .map(existingSubLedger -> {
                if (subLedger.getSubLedgerRefNo() != null) {
                    existingSubLedger.setSubLedgerRefNo(subLedger.getSubLedgerRefNo());
                }
                if (subLedger.getAccountNo() != null) {
                    existingSubLedger.setAccountNo(subLedger.getAccountNo());
                }
                if (subLedger.getTxnDate() != null) {
                    existingSubLedger.setTxnDate(subLedger.getTxnDate());
                }
                if (subLedger.getTxnDateInUTC() != null) {
                    existingSubLedger.setTxnDateInUTC(subLedger.getTxnDateInUTC());
                }
                if (subLedger.getTxnDateInLocal() != null) {
                    existingSubLedger.setTxnDateInLocal(subLedger.getTxnDateInLocal());
                }
                if (subLedger.getTxnDateForSettlement() != null) {
                    existingSubLedger.setTxnDateForSettlement(subLedger.getTxnDateForSettlement());
                }
                if (subLedger.getAmountInAccountCurrency() != null) {
                    existingSubLedger.setAmountInAccountCurrency(subLedger.getAmountInAccountCurrency());
                }
                if (subLedger.getAmountInLocalCurrency() != null) {
                    existingSubLedger.setAmountInLocalCurrency(subLedger.getAmountInLocalCurrency());
                }
                if (subLedger.getRate() != null) {
                    existingSubLedger.setRate(subLedger.getRate());
                }
                if (subLedger.getEntryCategory() != null) {
                    existingSubLedger.setEntryCategory(subLedger.getEntryCategory());
                }
                if (subLedger.getDescription1() != null) {
                    existingSubLedger.setDescription1(subLedger.getDescription1());
                }
                if (subLedger.getDescription2() != null) {
                    existingSubLedger.setDescription2(subLedger.getDescription2());
                }
                if (subLedger.getCreatedOn() != null) {
                    existingSubLedger.setCreatedOn(subLedger.getCreatedOn());
                }
                if (subLedger.getCreatedBy() != null) {
                    existingSubLedger.setCreatedBy(subLedger.getCreatedBy());
                }
                if (subLedger.getEntryStatus() != null) {
                    existingSubLedger.setEntryStatus(subLedger.getEntryStatus());
                }
                if (subLedger.getGlEntryType() != null) {
                    existingSubLedger.setGlEntryType(subLedger.getGlEntryType());
                }

                return existingSubLedger;
            })
            .map(subLedgerRepository::save);

        return ResponseUtil.wrapOrNotFound(
            result,
            HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, subLedger.getId().toString())
        );
    }

    /**
     * {@code GET  /sub-ledgers} : get all the subLedgers.
     *
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of subLedgers in body.
     */
    @GetMapping("/sub-ledgers")
    public List<SubLedger> getAllSubLedgers() {
        log.debug("REST request to get all SubLedgers");
        return subLedgerRepository.findAll();
    }

    /**
     * {@code GET  /sub-ledgers/:id} : get the "id" subLedger.
     *
     * @param id the id of the subLedger to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the subLedger, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/sub-ledgers/{id}")
    public ResponseEntity<SubLedger> getSubLedger(@PathVariable Long id) {
        log.debug("REST request to get SubLedger : {}", id);
        Optional<SubLedger> subLedger = subLedgerRepository.findById(id);
        return ResponseUtil.wrapOrNotFound(subLedger);
    }

    /**
     * {@code DELETE  /sub-ledgers/:id} : delete the "id" subLedger.
     *
     * @param id the id of the subLedger to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/sub-ledgers/{id}")
    public ResponseEntity<Void> deleteSubLedger(@PathVariable Long id) {
        log.debug("REST request to delete SubLedger : {}", id);
        subLedgerRepository.deleteById(id);
        return ResponseEntity
            .noContent()
            .headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id.toString()))
            .build();
    }
}
