package com.iconnect101.glapp.config.custom;

import java.security.SecureRandom;
import java.util.Random;

public class RandomGenerator {

    private static final String characters = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";

    public static String generateRandom(int length) {
        Random random = new SecureRandom();
        if (length <= 0) {
            throw new IllegalArgumentException("String length must be a positive integer");
        }

        StringBuilder sb = new StringBuilder(length);
        for (int i = 0; i < length; i++) {
            sb.append(characters.charAt(random.nextInt(characters.length())));
        }

        Random r = new Random();
        int low = 1000;
        int high = 9999;
        int result = r.nextInt(high - low) + low;

        return sb.toString() + result;
    }

    public static void main(String args[]) {
        System.out.println(RandomGenerator.generateRandom(10));
    }
}
