package com.iconnect101.glapp.config.custom;

/**
 * Application constants.
 */
public final class AppConstants {

    // Various transaction types
    public static final String INT_ACC_ACC = "INT_ACC_ACC";
    public static final String INT_CARD_ACC = "INT_CARD_ACC";
    public static final String EXT_CARD_ACC = "EXT_CARD_ACC";
    public static final String INL_REM_ACC = "INL_REM_ACC";
    public static final String LCL_REM_ACC = "LCL_REM_ACC";
    public static final String INT_ACC_WAL = "INT_ACC_WAL";
    public static final String INT_CARD_WAL = "INT_CARD_WAL";
    public static final String EXT_CARD_WAL = "EXT_CARD_WAL";
    public static final String INL_REM_WAL = "INL_REM_WAL";
    public static final String LCL_REM_WAL = "LCL_REM_WAL";
    public static final String INL_GLD_WAL = "INL_GLD_WAL";
    public static final String INL_CSH_WAL = "INL_CSH_WAL";

    // Various funding source
    public static final String CASA_ACCOUNT = "CASA_ACCOUNT";
    public static final String WALLET_ACCOUNT = "WALLET_ACCOUNT";
    public static final String GOLD = "GOLD";
    public static final String CASH = "CASH";

    // Created By
    public static final String CREATED_BY = "SYSTEM";

    // Wallet collection accounts
    public static final String WALLET_COLLECTION_ACCONUT_USD = "999-9910101010";
    public static final String WALLET_COLLECTION_ACCONUT_RND = "999-9910101011";
    public static final String WALLET_COLLECTION_ACCONUT_AED = "999-9910101012";

    // Currencies
    public static final String AED = "AED";
    public static final String USD = "USD";
    public static final String RND = "RND";

    //Remittance
    public static final String REMITTANCE_CORR_BANK_AC = "REMITTANCE_CORR_BANK_AC";
    public static final String REMITTANCE_CHARGE_AC = "REMITTANCE_CHARGE_AC";
    public static final String CHARGE_REF_PREFEIX = "CHR_";
    public static final String CHARGE_DESC_PREFEIX = "CHARGE : ";

    public static final Long FUNDING_SOURCE_TYPE_CASA = 1L;
    public static final Long TXN_TYPE_INL_REM_CASA = 4L;

    private AppConstants() {}
}
