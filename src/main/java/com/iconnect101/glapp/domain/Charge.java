package com.iconnect101.glapp.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.iconnect101.glapp.domain.enumeration.RecordStatus;
import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDate;
import javax.persistence.*;

/**
 * A Charge.
 */
@Entity
@Table(name = "charge")
public class Charge implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    @Column(name = "id")
    private Long id;

    @Column(name = "charge_amount_local_ccy", precision = 21, scale = 2)
    private BigDecimal chargeAmountLocalCcy;

    @Column(name = "charge_amount_txn_ccy", precision = 21, scale = 2)
    private BigDecimal chargeAmountTxnCcy;

    @Column(name = "amount_slab")
    private String amountSlab;

    @Column(name = "amount_slab_value", precision = 21, scale = 2)
    private BigDecimal amountSlabValue;

    @Column(name = "amount_slab_operator")
    private String amountSlabOperator;

    @Column(name = "amount_slab_currency")
    private String amountSlabCurrency;

    @Enumerated(EnumType.STRING)
    @Column(name = "record_status")
    private RecordStatus recordStatus;

    @Column(name = "validfrom")
    private LocalDate validfrom;

    @Column(name = "valid_to")
    private LocalDate validTo;

    @ManyToOne
    private ChargeType chargeType;

    @ManyToOne
    private TransactionType transactionType;

    @ManyToOne
    private Country txnCountry;

    @ManyToOne
    private Country beneficiaryCountry;

    @ManyToOne
    private Currency baseCurrency;

    @ManyToOne
    private Currency txnCurrency;

    @ManyToOne
    @JsonIgnoreProperties(value = { "accountType", "accountCategory", "accountCurrency", "customer" }, allowSetters = true)
    private CasaAccount chargeAccount;

    @ManyToOne
    private CustomerSegment customerSegment;

    // jhipster-needle-entity-add-field - JHipster will add fields here

    public Long getId() {
        return this.id;
    }

    public Charge id(Long id) {
        this.setId(id);
        return this;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public BigDecimal getChargeAmountLocalCcy() {
        return this.chargeAmountLocalCcy;
    }

    public Charge chargeAmountLocalCcy(BigDecimal chargeAmountLocalCcy) {
        this.setChargeAmountLocalCcy(chargeAmountLocalCcy);
        return this;
    }

    public void setChargeAmountLocalCcy(BigDecimal chargeAmountLocalCcy) {
        this.chargeAmountLocalCcy = chargeAmountLocalCcy;
    }

    public BigDecimal getChargeAmountTxnCcy() {
        return this.chargeAmountTxnCcy;
    }

    public Charge chargeAmountTxnCcy(BigDecimal chargeAmountTxnCcy) {
        this.setChargeAmountTxnCcy(chargeAmountTxnCcy);
        return this;
    }

    public void setChargeAmountTxnCcy(BigDecimal chargeAmountTxnCcy) {
        this.chargeAmountTxnCcy = chargeAmountTxnCcy;
    }

    public String getAmountSlab() {
        return this.amountSlab;
    }

    public Charge amountSlab(String amountSlab) {
        this.setAmountSlab(amountSlab);
        return this;
    }

    public void setAmountSlab(String amountSlab) {
        this.amountSlab = amountSlab;
    }

    public BigDecimal getAmountSlabValue() {
        return this.amountSlabValue;
    }

    public Charge amountSlabValue(BigDecimal amountSlabValue) {
        this.setAmountSlabValue(amountSlabValue);
        return this;
    }

    public void setAmountSlabValue(BigDecimal amountSlabValue) {
        this.amountSlabValue = amountSlabValue;
    }

    public String getAmountSlabOperator() {
        return this.amountSlabOperator;
    }

    public Charge amountSlabOperator(String amountSlabOperator) {
        this.setAmountSlabOperator(amountSlabOperator);
        return this;
    }

    public void setAmountSlabOperator(String amountSlabOperator) {
        this.amountSlabOperator = amountSlabOperator;
    }

    public String getAmountSlabCurrency() {
        return this.amountSlabCurrency;
    }

    public Charge amountSlabCurrency(String amountSlabCurrency) {
        this.setAmountSlabCurrency(amountSlabCurrency);
        return this;
    }

    public void setAmountSlabCurrency(String amountSlabCurrency) {
        this.amountSlabCurrency = amountSlabCurrency;
    }

    public RecordStatus getRecordStatus() {
        return this.recordStatus;
    }

    public Charge recordStatus(RecordStatus recordStatus) {
        this.setRecordStatus(recordStatus);
        return this;
    }

    public void setRecordStatus(RecordStatus recordStatus) {
        this.recordStatus = recordStatus;
    }

    public LocalDate getValidfrom() {
        return this.validfrom;
    }

    public Charge validfrom(LocalDate validfrom) {
        this.setValidfrom(validfrom);
        return this;
    }

    public void setValidfrom(LocalDate validfrom) {
        this.validfrom = validfrom;
    }

    public LocalDate getValidTo() {
        return this.validTo;
    }

    public Charge validTo(LocalDate validTo) {
        this.setValidTo(validTo);
        return this;
    }

    public void setValidTo(LocalDate validTo) {
        this.validTo = validTo;
    }

    public ChargeType getChargeType() {
        return this.chargeType;
    }

    public void setChargeType(ChargeType chargeType) {
        this.chargeType = chargeType;
    }

    public Charge chargeType(ChargeType chargeType) {
        this.setChargeType(chargeType);
        return this;
    }

    public TransactionType getTransactionType() {
        return this.transactionType;
    }

    public void setTransactionType(TransactionType transactionType) {
        this.transactionType = transactionType;
    }

    public Charge transactionType(TransactionType transactionType) {
        this.setTransactionType(transactionType);
        return this;
    }

    public Country getTxnCountry() {
        return this.txnCountry;
    }

    public void setTxnCountry(Country country) {
        this.txnCountry = country;
    }

    public Charge txnCountry(Country country) {
        this.setTxnCountry(country);
        return this;
    }

    public Country getBeneficiaryCountry() {
        return this.beneficiaryCountry;
    }

    public void setBeneficiaryCountry(Country country) {
        this.beneficiaryCountry = country;
    }

    public Charge beneficiaryCountry(Country country) {
        this.setBeneficiaryCountry(country);
        return this;
    }

    public Currency getBaseCurrency() {
        return this.baseCurrency;
    }

    public void setBaseCurrency(Currency currency) {
        this.baseCurrency = currency;
    }

    public Charge baseCurrency(Currency currency) {
        this.setBaseCurrency(currency);
        return this;
    }

    public Currency getTxnCurrency() {
        return this.txnCurrency;
    }

    public void setTxnCurrency(Currency currency) {
        this.txnCurrency = currency;
    }

    public Charge txnCurrency(Currency currency) {
        this.setTxnCurrency(currency);
        return this;
    }

    public CasaAccount getChargeAccount() {
        return this.chargeAccount;
    }

    public void setChargeAccount(CasaAccount casaAccount) {
        this.chargeAccount = casaAccount;
    }

    public Charge chargeAccount(CasaAccount casaAccount) {
        this.setChargeAccount(casaAccount);
        return this;
    }

    public CustomerSegment getCustomerSegment() {
        return this.customerSegment;
    }

    public void setCustomerSegment(CustomerSegment customerSegment) {
        this.customerSegment = customerSegment;
    }

    public Charge customerSegment(CustomerSegment customerSegment) {
        this.setCustomerSegment(customerSegment);
        return this;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Charge)) {
            return false;
        }
        return id != null && id.equals(((Charge) o).id);
    }

    @Override
    public int hashCode() {
        // see https://vladmihalcea.com/how-to-implement-equals-and-hashcode-using-the-jpa-entity-identifier/
        return getClass().hashCode();
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "Charge{" +
            "id=" + getId() +
            ", chargeAmountLocalCcy=" + getChargeAmountLocalCcy() +
            ", chargeAmountTxnCcy=" + getChargeAmountTxnCcy() +
            ", amountSlab='" + getAmountSlab() + "'" +
            ", amountSlabValue=" + getAmountSlabValue() +
            ", amountSlabOperator='" + getAmountSlabOperator() + "'" +
            ", amountSlabCurrency='" + getAmountSlabCurrency() + "'" +
            ", recordStatus='" + getRecordStatus() + "'" +
            ", validfrom='" + getValidfrom() + "'" +
            ", validTo='" + getValidTo() + "'" +
            "}";
    }
}
