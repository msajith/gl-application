package com.iconnect101.glapp.domain.enumeration;

/**
 * The GlAccountType enumeration.
 */
public enum GlAccountType {
    ASSET,
    LIABILITY,
    REVENUE,
    EXPENSE,
    GAIN,
    LOSS,
}
