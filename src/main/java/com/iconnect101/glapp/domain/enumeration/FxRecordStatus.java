package com.iconnect101.glapp.domain.enumeration;

/**
 * The FxRecordStatus enumeration.
 */
public enum FxRecordStatus {
    ACTIVE,
    INACTIVE,
}
