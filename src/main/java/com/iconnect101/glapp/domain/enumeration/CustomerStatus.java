package com.iconnect101.glapp.domain.enumeration;

/**
 * The CustomerStatus enumeration.
 */
public enum CustomerStatus {
    ACTIVE,
    INACTIVE,
    CLOSED,
}
