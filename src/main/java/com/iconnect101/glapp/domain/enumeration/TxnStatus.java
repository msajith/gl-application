package com.iconnect101.glapp.domain.enumeration;

/**
 * The TxnStatus enumeration.
 */
public enum TxnStatus {
    ACTIVE,
    CANCELLED,
}
