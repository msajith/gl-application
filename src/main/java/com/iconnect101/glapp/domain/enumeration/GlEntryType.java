package com.iconnect101.glapp.domain.enumeration;

/**
 * The GlEntryType enumeration.
 */
public enum GlEntryType {
    DR,
    CR,
}
