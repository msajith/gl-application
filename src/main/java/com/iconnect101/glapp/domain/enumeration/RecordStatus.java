package com.iconnect101.glapp.domain.enumeration;

/**
 * The RecordStatus enumeration.
 */
public enum RecordStatus {
    ACTIVE,
    INACTIVE,
}
