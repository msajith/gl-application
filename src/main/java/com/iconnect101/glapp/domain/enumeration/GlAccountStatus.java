package com.iconnect101.glapp.domain.enumeration;

/**
 * The GlAccountStatus enumeration.
 */
public enum GlAccountStatus {
    ACTIVE,
    INACTIVE,
    CLOSED,
}
