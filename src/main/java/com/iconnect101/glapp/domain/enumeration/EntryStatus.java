package com.iconnect101.glapp.domain.enumeration;

/**
 * The EntryStatus enumeration.
 */
public enum EntryStatus {
    ACTIVE,
    CANCELLED,
}
