package com.iconnect101.glapp.domain.enumeration;

/**
 * The AccountStatus enumeration.
 */
public enum AccountStatus {
    ACTIVE,
    INACTIVE,
    CLOSED,
}
