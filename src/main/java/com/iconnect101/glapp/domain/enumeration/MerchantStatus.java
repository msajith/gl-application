package com.iconnect101.glapp.domain.enumeration;

/**
 * The MerchantStatus enumeration.
 */
public enum MerchantStatus {
    ACTIVE,
    INACTIVE,
}
