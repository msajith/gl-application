package com.iconnect101.glapp.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.io.Serializable;
import java.time.Instant;
import javax.persistence.*;

/**
 * A Beneficiary.
 */
@Entity
@Table(name = "beneficiary")
public class Beneficiary implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    @Column(name = "id")
    private Long id;

    @Column(name = "title")
    private String title;

    @Column(name = "beneficiary_name")
    private String beneficiaryName;

    @Column(name = "beneficiary_account_no")
    private String beneficiaryAccountNo;

    @Column(name = "beneficiarybank_swift_code")
    private String beneficiarybankSwiftCode;

    @Column(name = "intermediary_bank_swift_code")
    private String intermediaryBankSwiftCode;

    @Column(name = "beneficiary_bank_name")
    private String beneficiaryBankName;

    @Column(name = "beneficiary_bank_branch_name")
    private String beneficiaryBankBranchName;

    @Column(name = "beneficiary_address_1")
    private String beneficiaryAddress1;

    @Column(name = "beneficiary_address_2")
    private String beneficiaryAddress2;

    @Column(name = "beneficiary_address_3")
    private String beneficiaryAddress3;

    @Column(name = "beneficiary_state")
    private String beneficiaryState;

    @Column(name = "beneficiary_pin_code")
    private String beneficiaryPinCode;

    @Column(name = "created_on")
    private Instant createdOn;

    @Column(name = "created_by")
    private String createdBy;

    @ManyToOne
    @JsonIgnoreProperties(
        value = { "addresses", "walletAccounts", "casaAccounts", "nationality", "customerType", "customerSegment" },
        allowSetters = true
    )
    private Customer customer;

    @ManyToOne
    private Country country;

    @ManyToOne
    private Currency currency;

    // jhipster-needle-entity-add-field - JHipster will add fields here

    public Long getId() {
        return this.id;
    }

    public Beneficiary id(Long id) {
        this.setId(id);
        return this;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitle() {
        return this.title;
    }

    public Beneficiary title(String title) {
        this.setTitle(title);
        return this;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getBeneficiaryName() {
        return this.beneficiaryName;
    }

    public Beneficiary beneficiaryName(String beneficiaryName) {
        this.setBeneficiaryName(beneficiaryName);
        return this;
    }

    public void setBeneficiaryName(String beneficiaryName) {
        this.beneficiaryName = beneficiaryName;
    }

    public String getBeneficiaryAccountNo() {
        return this.beneficiaryAccountNo;
    }

    public Beneficiary beneficiaryAccountNo(String beneficiaryAccountNo) {
        this.setBeneficiaryAccountNo(beneficiaryAccountNo);
        return this;
    }

    public void setBeneficiaryAccountNo(String beneficiaryAccountNo) {
        this.beneficiaryAccountNo = beneficiaryAccountNo;
    }

    public String getBeneficiarybankSwiftCode() {
        return this.beneficiarybankSwiftCode;
    }

    public Beneficiary beneficiarybankSwiftCode(String beneficiarybankSwiftCode) {
        this.setBeneficiarybankSwiftCode(beneficiarybankSwiftCode);
        return this;
    }

    public void setBeneficiarybankSwiftCode(String beneficiarybankSwiftCode) {
        this.beneficiarybankSwiftCode = beneficiarybankSwiftCode;
    }

    public String getIntermediaryBankSwiftCode() {
        return this.intermediaryBankSwiftCode;
    }

    public Beneficiary intermediaryBankSwiftCode(String intermediaryBankSwiftCode) {
        this.setIntermediaryBankSwiftCode(intermediaryBankSwiftCode);
        return this;
    }

    public void setIntermediaryBankSwiftCode(String intermediaryBankSwiftCode) {
        this.intermediaryBankSwiftCode = intermediaryBankSwiftCode;
    }

    public String getBeneficiaryBankName() {
        return this.beneficiaryBankName;
    }

    public Beneficiary beneficiaryBankName(String beneficiaryBankName) {
        this.setBeneficiaryBankName(beneficiaryBankName);
        return this;
    }

    public void setBeneficiaryBankName(String beneficiaryBankName) {
        this.beneficiaryBankName = beneficiaryBankName;
    }

    public String getBeneficiaryBankBranchName() {
        return this.beneficiaryBankBranchName;
    }

    public Beneficiary beneficiaryBankBranchName(String beneficiaryBankBranchName) {
        this.setBeneficiaryBankBranchName(beneficiaryBankBranchName);
        return this;
    }

    public void setBeneficiaryBankBranchName(String beneficiaryBankBranchName) {
        this.beneficiaryBankBranchName = beneficiaryBankBranchName;
    }

    public String getBeneficiaryAddress1() {
        return this.beneficiaryAddress1;
    }

    public Beneficiary beneficiaryAddress1(String beneficiaryAddress1) {
        this.setBeneficiaryAddress1(beneficiaryAddress1);
        return this;
    }

    public void setBeneficiaryAddress1(String beneficiaryAddress1) {
        this.beneficiaryAddress1 = beneficiaryAddress1;
    }

    public String getBeneficiaryAddress2() {
        return this.beneficiaryAddress2;
    }

    public Beneficiary beneficiaryAddress2(String beneficiaryAddress2) {
        this.setBeneficiaryAddress2(beneficiaryAddress2);
        return this;
    }

    public void setBeneficiaryAddress2(String beneficiaryAddress2) {
        this.beneficiaryAddress2 = beneficiaryAddress2;
    }

    public String getBeneficiaryAddress3() {
        return this.beneficiaryAddress3;
    }

    public Beneficiary beneficiaryAddress3(String beneficiaryAddress3) {
        this.setBeneficiaryAddress3(beneficiaryAddress3);
        return this;
    }

    public void setBeneficiaryAddress3(String beneficiaryAddress3) {
        this.beneficiaryAddress3 = beneficiaryAddress3;
    }

    public String getBeneficiaryState() {
        return this.beneficiaryState;
    }

    public Beneficiary beneficiaryState(String beneficiaryState) {
        this.setBeneficiaryState(beneficiaryState);
        return this;
    }

    public void setBeneficiaryState(String beneficiaryState) {
        this.beneficiaryState = beneficiaryState;
    }

    public String getBeneficiaryPinCode() {
        return this.beneficiaryPinCode;
    }

    public Beneficiary beneficiaryPinCode(String beneficiaryPinCode) {
        this.setBeneficiaryPinCode(beneficiaryPinCode);
        return this;
    }

    public void setBeneficiaryPinCode(String beneficiaryPinCode) {
        this.beneficiaryPinCode = beneficiaryPinCode;
    }

    public Instant getCreatedOn() {
        return this.createdOn;
    }

    public Beneficiary createdOn(Instant createdOn) {
        this.setCreatedOn(createdOn);
        return this;
    }

    public void setCreatedOn(Instant createdOn) {
        this.createdOn = createdOn;
    }

    public String getCreatedBy() {
        return this.createdBy;
    }

    public Beneficiary createdBy(String createdBy) {
        this.setCreatedBy(createdBy);
        return this;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public Customer getCustomer() {
        return this.customer;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }

    public Beneficiary customer(Customer customer) {
        this.setCustomer(customer);
        return this;
    }

    public Country getCountry() {
        return this.country;
    }

    public void setCountry(Country country) {
        this.country = country;
    }

    public Beneficiary country(Country country) {
        this.setCountry(country);
        return this;
    }

    public Currency getCurrency() {
        return this.currency;
    }

    public void setCurrency(Currency currency) {
        this.currency = currency;
    }

    public Beneficiary currency(Currency currency) {
        this.setCurrency(currency);
        return this;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Beneficiary)) {
            return false;
        }
        return id != null && id.equals(((Beneficiary) o).id);
    }

    @Override
    public int hashCode() {
        // see https://vladmihalcea.com/how-to-implement-equals-and-hashcode-using-the-jpa-entity-identifier/
        return getClass().hashCode();
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "Beneficiary{" +
            "id=" + getId() +
            ", title='" + getTitle() + "'" +
            ", beneficiaryName='" + getBeneficiaryName() + "'" +
            ", beneficiaryAccountNo='" + getBeneficiaryAccountNo() + "'" +
            ", beneficiarybankSwiftCode='" + getBeneficiarybankSwiftCode() + "'" +
            ", intermediaryBankSwiftCode='" + getIntermediaryBankSwiftCode() + "'" +
            ", beneficiaryBankName='" + getBeneficiaryBankName() + "'" +
            ", beneficiaryBankBranchName='" + getBeneficiaryBankBranchName() + "'" +
            ", beneficiaryAddress1='" + getBeneficiaryAddress1() + "'" +
            ", beneficiaryAddress2='" + getBeneficiaryAddress2() + "'" +
            ", beneficiaryAddress3='" + getBeneficiaryAddress3() + "'" +
            ", beneficiaryState='" + getBeneficiaryState() + "'" +
            ", beneficiaryPinCode='" + getBeneficiaryPinCode() + "'" +
            ", createdOn='" + getCreatedOn() + "'" +
            ", createdBy='" + getCreatedBy() + "'" +
            "}";
    }
}
