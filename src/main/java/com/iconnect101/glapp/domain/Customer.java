package com.iconnect101.glapp.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.iconnect101.glapp.domain.enumeration.CustomerStatus;
import java.io.Serializable;
import java.time.Instant;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.*;
import javax.validation.constraints.*;

/**
 * A Customer.
 */
@Entity
@Table(name = "customer")
public class Customer implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    @Column(name = "id")
    private Long id;

    @NotNull
    @Column(name = "cif", nullable = false)
    private String cif;

    @Column(name = "first_name")
    private String firstName;

    @Column(name = "middle_name")
    private String middleName;

    @NotNull
    @Column(name = "last_name", nullable = false)
    private String lastName;

    @Column(name = "name_in_local_lang")
    private String nameInLocalLang;

    @Column(name = "telephone")
    private String telephone;

    @Column(name = "mobile")
    private String mobile;

    @Column(name = "email")
    private String email;

    @Column(name = "created_on")
    private Instant createdOn;

    @Column(name = "created_by")
    private String createdBy;

    @Enumerated(EnumType.STRING)
    @Column(name = "customer_status")
    private CustomerStatus customerStatus;

    @OneToMany(mappedBy = "customer")
    @JsonIgnoreProperties(value = { "country", "customer" }, allowSetters = true)
    private Set<CustomerAddress> addresses = new HashSet<>();

    @OneToMany(mappedBy = "customer")
    @JsonIgnoreProperties(value = { "accountType", "accountCategory", "accountCurrency", "customer" }, allowSetters = true)
    private Set<WalletAccount> walletAccounts = new HashSet<>();

    @OneToMany(mappedBy = "customer")
    @JsonIgnoreProperties(value = { "accountType", "accountCategory", "accountCurrency", "customer" }, allowSetters = true)
    private Set<CasaAccount> casaAccounts = new HashSet<>();

    @ManyToOne
    private Country nationality;

    @ManyToOne
    private CustomerType customerType;

    @ManyToOne
    private CustomerSegment customerSegment;

    // jhipster-needle-entity-add-field - JHipster will add fields here

    public Long getId() {
        return this.id;
    }

    public Customer id(Long id) {
        this.setId(id);
        return this;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCif() {
        return this.cif;
    }

    public Customer cif(String cif) {
        this.setCif(cif);
        return this;
    }

    public void setCif(String cif) {
        this.cif = cif;
    }

    public String getFirstName() {
        return this.firstName;
    }

    public Customer firstName(String firstName) {
        this.setFirstName(firstName);
        return this;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getMiddleName() {
        return this.middleName;
    }

    public Customer middleName(String middleName) {
        this.setMiddleName(middleName);
        return this;
    }

    public void setMiddleName(String middleName) {
        this.middleName = middleName;
    }

    public String getLastName() {
        return this.lastName;
    }

    public Customer lastName(String lastName) {
        this.setLastName(lastName);
        return this;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getNameInLocalLang() {
        return this.nameInLocalLang;
    }

    public Customer nameInLocalLang(String nameInLocalLang) {
        this.setNameInLocalLang(nameInLocalLang);
        return this;
    }

    public void setNameInLocalLang(String nameInLocalLang) {
        this.nameInLocalLang = nameInLocalLang;
    }

    public String getTelephone() {
        return this.telephone;
    }

    public Customer telephone(String telephone) {
        this.setTelephone(telephone);
        return this;
    }

    public void setTelephone(String telephone) {
        this.telephone = telephone;
    }

    public String getMobile() {
        return this.mobile;
    }

    public Customer mobile(String mobile) {
        this.setMobile(mobile);
        return this;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getEmail() {
        return this.email;
    }

    public Customer email(String email) {
        this.setEmail(email);
        return this;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Instant getCreatedOn() {
        return this.createdOn;
    }

    public Customer createdOn(Instant createdOn) {
        this.setCreatedOn(createdOn);
        return this;
    }

    public void setCreatedOn(Instant createdOn) {
        this.createdOn = createdOn;
    }

    public String getCreatedBy() {
        return this.createdBy;
    }

    public Customer createdBy(String createdBy) {
        this.setCreatedBy(createdBy);
        return this;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public CustomerStatus getCustomerStatus() {
        return this.customerStatus;
    }

    public Customer customerStatus(CustomerStatus customerStatus) {
        this.setCustomerStatus(customerStatus);
        return this;
    }

    public void setCustomerStatus(CustomerStatus customerStatus) {
        this.customerStatus = customerStatus;
    }

    public Set<CustomerAddress> getAddresses() {
        return this.addresses;
    }

    public void setAddresses(Set<CustomerAddress> customerAddresses) {
        if (this.addresses != null) {
            this.addresses.forEach(i -> i.setCustomer(null));
        }
        if (customerAddresses != null) {
            customerAddresses.forEach(i -> i.setCustomer(this));
        }
        this.addresses = customerAddresses;
    }

    public Customer addresses(Set<CustomerAddress> customerAddresses) {
        this.setAddresses(customerAddresses);
        return this;
    }

    public Customer addAddress(CustomerAddress customerAddress) {
        this.addresses.add(customerAddress);
        customerAddress.setCustomer(this);
        return this;
    }

    public Customer removeAddress(CustomerAddress customerAddress) {
        this.addresses.remove(customerAddress);
        customerAddress.setCustomer(null);
        return this;
    }

    public Set<WalletAccount> getWalletAccounts() {
        return this.walletAccounts;
    }

    public void setWalletAccounts(Set<WalletAccount> walletAccounts) {
        if (this.walletAccounts != null) {
            this.walletAccounts.forEach(i -> i.setCustomer(null));
        }
        if (walletAccounts != null) {
            walletAccounts.forEach(i -> i.setCustomer(this));
        }
        this.walletAccounts = walletAccounts;
    }

    public Customer walletAccounts(Set<WalletAccount> walletAccounts) {
        this.setWalletAccounts(walletAccounts);
        return this;
    }

    public Customer addWalletAccount(WalletAccount walletAccount) {
        this.walletAccounts.add(walletAccount);
        walletAccount.setCustomer(this);
        return this;
    }

    public Customer removeWalletAccount(WalletAccount walletAccount) {
        this.walletAccounts.remove(walletAccount);
        walletAccount.setCustomer(null);
        return this;
    }

    public Set<CasaAccount> getCasaAccounts() {
        return this.casaAccounts;
    }

    public void setCasaAccounts(Set<CasaAccount> casaAccounts) {
        if (this.casaAccounts != null) {
            this.casaAccounts.forEach(i -> i.setCustomer(null));
        }
        if (casaAccounts != null) {
            casaAccounts.forEach(i -> i.setCustomer(this));
        }
        this.casaAccounts = casaAccounts;
    }

    public Customer casaAccounts(Set<CasaAccount> casaAccounts) {
        this.setCasaAccounts(casaAccounts);
        return this;
    }

    public Customer addCasaAccount(CasaAccount casaAccount) {
        this.casaAccounts.add(casaAccount);
        casaAccount.setCustomer(this);
        return this;
    }

    public Customer removeCasaAccount(CasaAccount casaAccount) {
        this.casaAccounts.remove(casaAccount);
        casaAccount.setCustomer(null);
        return this;
    }

    public Country getNationality() {
        return this.nationality;
    }

    public void setNationality(Country country) {
        this.nationality = country;
    }

    public Customer nationality(Country country) {
        this.setNationality(country);
        return this;
    }

    public CustomerType getCustomerType() {
        return this.customerType;
    }

    public void setCustomerType(CustomerType customerType) {
        this.customerType = customerType;
    }

    public Customer customerType(CustomerType customerType) {
        this.setCustomerType(customerType);
        return this;
    }

    public CustomerSegment getCustomerSegment() {
        return this.customerSegment;
    }

    public void setCustomerSegment(CustomerSegment customerSegment) {
        this.customerSegment = customerSegment;
    }

    public Customer customerSegment(CustomerSegment customerSegment) {
        this.setCustomerSegment(customerSegment);
        return this;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Customer)) {
            return false;
        }
        return id != null && id.equals(((Customer) o).id);
    }

    @Override
    public int hashCode() {
        // see https://vladmihalcea.com/how-to-implement-equals-and-hashcode-using-the-jpa-entity-identifier/
        return getClass().hashCode();
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "Customer{" +
            "id=" + getId() +
            ", cif='" + getCif() + "'" +
            ", firstName='" + getFirstName() + "'" +
            ", middleName='" + getMiddleName() + "'" +
            ", lastName='" + getLastName() + "'" +
            ", nameInLocalLang='" + getNameInLocalLang() + "'" +
            ", telephone='" + getTelephone() + "'" +
            ", mobile='" + getMobile() + "'" +
            ", email='" + getEmail() + "'" +
            ", createdOn='" + getCreatedOn() + "'" +
            ", createdBy='" + getCreatedBy() + "'" +
            ", customerStatus='" + getCustomerStatus() + "'" +
            "}";
    }
}
