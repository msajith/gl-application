package com.iconnect101.glapp.domain;

import com.iconnect101.glapp.domain.enumeration.EntryStatus;
import com.iconnect101.glapp.domain.enumeration.GlEntryType;
import java.io.Serializable;
import java.math.BigDecimal;
import java.time.Instant;
import java.time.LocalDate;
import java.time.ZonedDateTime;
import javax.persistence.*;

/**
 * A GeneralLedger.
 */
@Entity
@Table(name = "general_ledger")
public class GeneralLedger implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    @Column(name = "id")
    private Long id;

    @Column(name = "gl_ref_no")
    private String glRefNo;

    @Column(name = "gl_account_no")
    private String glAccountNo;

    @Column(name = "txn_date")
    private LocalDate txnDate;

    @Column(name = "txn_date_in_utc")
    private Instant txnDateInUTC;

    @Column(name = "txn_date_in_local")
    private ZonedDateTime txnDateInLocal;

    @Column(name = "txn_date_for_settlement")
    private ZonedDateTime txnDateForSettlement;

    @Column(name = "amount_in_account_currency", precision = 21, scale = 2)
    private BigDecimal amountInAccountCurrency;

    @Column(name = "amount_in_local_currency", precision = 21, scale = 2)
    private BigDecimal amountInLocalCurrency;

    @Column(name = "rate", precision = 21, scale = 2)
    private BigDecimal rate;

    @Column(name = "entry_category")
    private String entryCategory;

    @Column(name = "description_1")
    private String description1;

    @Column(name = "description_2")
    private String description2;

    @Column(name = "created_on")
    private Instant createdOn;

    @Column(name = "created_by")
    private String createdBy;

    @Enumerated(EnumType.STRING)
    @Column(name = "entry_status")
    private EntryStatus entryStatus;

    @Enumerated(EnumType.STRING)
    @Column(name = "gl_entry_type")
    private GlEntryType glEntryType;

    // jhipster-needle-entity-add-field - JHipster will add fields here

    public Long getId() {
        return this.id;
    }

    public GeneralLedger id(Long id) {
        this.setId(id);
        return this;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getGlRefNo() {
        return this.glRefNo;
    }

    public GeneralLedger glRefNo(String glRefNo) {
        this.setGlRefNo(glRefNo);
        return this;
    }

    public void setGlRefNo(String glRefNo) {
        this.glRefNo = glRefNo;
    }

    public String getGlAccountNo() {
        return this.glAccountNo;
    }

    public GeneralLedger glAccountNo(String glAccountNo) {
        this.setGlAccountNo(glAccountNo);
        return this;
    }

    public void setGlAccountNo(String glAccountNo) {
        this.glAccountNo = glAccountNo;
    }

    public LocalDate getTxnDate() {
        return this.txnDate;
    }

    public GeneralLedger txnDate(LocalDate txnDate) {
        this.setTxnDate(txnDate);
        return this;
    }

    public void setTxnDate(LocalDate txnDate) {
        this.txnDate = txnDate;
    }

    public Instant getTxnDateInUTC() {
        return this.txnDateInUTC;
    }

    public GeneralLedger txnDateInUTC(Instant txnDateInUTC) {
        this.setTxnDateInUTC(txnDateInUTC);
        return this;
    }

    public void setTxnDateInUTC(Instant txnDateInUTC) {
        this.txnDateInUTC = txnDateInUTC;
    }

    public ZonedDateTime getTxnDateInLocal() {
        return this.txnDateInLocal;
    }

    public GeneralLedger txnDateInLocal(ZonedDateTime txnDateInLocal) {
        this.setTxnDateInLocal(txnDateInLocal);
        return this;
    }

    public void setTxnDateInLocal(ZonedDateTime txnDateInLocal) {
        this.txnDateInLocal = txnDateInLocal;
    }

    public ZonedDateTime getTxnDateForSettlement() {
        return this.txnDateForSettlement;
    }

    public GeneralLedger txnDateForSettlement(ZonedDateTime txnDateForSettlement) {
        this.setTxnDateForSettlement(txnDateForSettlement);
        return this;
    }

    public void setTxnDateForSettlement(ZonedDateTime txnDateForSettlement) {
        this.txnDateForSettlement = txnDateForSettlement;
    }

    public BigDecimal getAmountInAccountCurrency() {
        return this.amountInAccountCurrency;
    }

    public GeneralLedger amountInAccountCurrency(BigDecimal amountInAccountCurrency) {
        this.setAmountInAccountCurrency(amountInAccountCurrency);
        return this;
    }

    public void setAmountInAccountCurrency(BigDecimal amountInAccountCurrency) {
        this.amountInAccountCurrency = amountInAccountCurrency;
    }

    public BigDecimal getAmountInLocalCurrency() {
        return this.amountInLocalCurrency;
    }

    public GeneralLedger amountInLocalCurrency(BigDecimal amountInLocalCurrency) {
        this.setAmountInLocalCurrency(amountInLocalCurrency);
        return this;
    }

    public void setAmountInLocalCurrency(BigDecimal amountInLocalCurrency) {
        this.amountInLocalCurrency = amountInLocalCurrency;
    }

    public BigDecimal getRate() {
        return this.rate;
    }

    public GeneralLedger rate(BigDecimal rate) {
        this.setRate(rate);
        return this;
    }

    public void setRate(BigDecimal rate) {
        this.rate = rate;
    }

    public String getEntryCategory() {
        return this.entryCategory;
    }

    public GeneralLedger entryCategory(String entryCategory) {
        this.setEntryCategory(entryCategory);
        return this;
    }

    public void setEntryCategory(String entryCategory) {
        this.entryCategory = entryCategory;
    }

    public String getDescription1() {
        return this.description1;
    }

    public GeneralLedger description1(String description1) {
        this.setDescription1(description1);
        return this;
    }

    public void setDescription1(String description1) {
        this.description1 = description1;
    }

    public String getDescription2() {
        return this.description2;
    }

    public GeneralLedger description2(String description2) {
        this.setDescription2(description2);
        return this;
    }

    public void setDescription2(String description2) {
        this.description2 = description2;
    }

    public Instant getCreatedOn() {
        return this.createdOn;
    }

    public GeneralLedger createdOn(Instant createdOn) {
        this.setCreatedOn(createdOn);
        return this;
    }

    public void setCreatedOn(Instant createdOn) {
        this.createdOn = createdOn;
    }

    public String getCreatedBy() {
        return this.createdBy;
    }

    public GeneralLedger createdBy(String createdBy) {
        this.setCreatedBy(createdBy);
        return this;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public EntryStatus getEntryStatus() {
        return this.entryStatus;
    }

    public GeneralLedger entryStatus(EntryStatus entryStatus) {
        this.setEntryStatus(entryStatus);
        return this;
    }

    public void setEntryStatus(EntryStatus entryStatus) {
        this.entryStatus = entryStatus;
    }

    public GlEntryType getGlEntryType() {
        return this.glEntryType;
    }

    public GeneralLedger glEntryType(GlEntryType glEntryType) {
        this.setGlEntryType(glEntryType);
        return this;
    }

    public void setGlEntryType(GlEntryType glEntryType) {
        this.glEntryType = glEntryType;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof GeneralLedger)) {
            return false;
        }
        return id != null && id.equals(((GeneralLedger) o).id);
    }

    @Override
    public int hashCode() {
        // see https://vladmihalcea.com/how-to-implement-equals-and-hashcode-using-the-jpa-entity-identifier/
        return getClass().hashCode();
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "GeneralLedger{" +
            "id=" + getId() +
            ", glRefNo='" + getGlRefNo() + "'" +
            ", glAccountNo='" + getGlAccountNo() + "'" +
            ", txnDate='" + getTxnDate() + "'" +
            ", txnDateInUTC='" + getTxnDateInUTC() + "'" +
            ", txnDateInLocal='" + getTxnDateInLocal() + "'" +
            ", txnDateForSettlement='" + getTxnDateForSettlement() + "'" +
            ", amountInAccountCurrency=" + getAmountInAccountCurrency() +
            ", amountInLocalCurrency=" + getAmountInLocalCurrency() +
            ", rate=" + getRate() +
            ", entryCategory='" + getEntryCategory() + "'" +
            ", description1='" + getDescription1() + "'" +
            ", description2='" + getDescription2() + "'" +
            ", createdOn='" + getCreatedOn() + "'" +
            ", createdBy='" + getCreatedBy() + "'" +
            ", entryStatus='" + getEntryStatus() + "'" +
            ", glEntryType='" + getGlEntryType() + "'" +
            "}";
    }
}
