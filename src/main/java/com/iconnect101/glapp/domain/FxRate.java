package com.iconnect101.glapp.domain;

import com.iconnect101.glapp.domain.enumeration.FxRecordStatus;
import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDate;
import javax.persistence.*;

/**
 * A FxRate.
 */
@Entity
@Table(name = "fx_rate")
public class FxRate implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    @Column(name = "id")
    private Long id;

    @Column(name = "type")
    private String type;

    @Column(name = "base_currency")
    private String baseCurrency;

    @Column(name = "quote_currency")
    private String quoteCurrency;

    @Column(name = "ask_price", precision = 21, scale = 2)
    private BigDecimal askPrice;

    @Column(name = "bid_price", precision = 21, scale = 2)
    private BigDecimal bidPrice;

    @Column(name = "mid_price", precision = 21, scale = 2)
    private BigDecimal midPrice;

    @Column(name = "ask_margin", precision = 21, scale = 2)
    private BigDecimal askMargin;

    @Column(name = "bid_margin", precision = 21, scale = 2)
    private BigDecimal bidMargin;

    @Column(name = "segment")
    private String segment;

    @Column(name = "amount_slab")
    private String amountSlab;

    @Column(name = "amount_slab_value", precision = 21, scale = 2)
    private BigDecimal amountSlabValue;

    @Column(name = "amount_slab_operator")
    private String amountSlabOperator;

    @Column(name = "amount_slab_currency")
    private String amountSlabCurrency;

    @Enumerated(EnumType.STRING)
    @Column(name = "record_status")
    private FxRecordStatus recordStatus;

    @Column(name = "validfrom")
    private LocalDate validfrom;

    @Column(name = "valid_to")
    private LocalDate validTo;

    // jhipster-needle-entity-add-field - JHipster will add fields here

    public Long getId() {
        return this.id;
    }

    public FxRate id(Long id) {
        this.setId(id);
        return this;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getType() {
        return this.type;
    }

    public FxRate type(String type) {
        this.setType(type);
        return this;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getBaseCurrency() {
        return this.baseCurrency;
    }

    public FxRate baseCurrency(String baseCurrency) {
        this.setBaseCurrency(baseCurrency);
        return this;
    }

    public void setBaseCurrency(String baseCurrency) {
        this.baseCurrency = baseCurrency;
    }

    public String getQuoteCurrency() {
        return this.quoteCurrency;
    }

    public FxRate quoteCurrency(String quoteCurrency) {
        this.setQuoteCurrency(quoteCurrency);
        return this;
    }

    public void setQuoteCurrency(String quoteCurrency) {
        this.quoteCurrency = quoteCurrency;
    }

    public BigDecimal getAskPrice() {
        return this.askPrice;
    }

    public FxRate askPrice(BigDecimal askPrice) {
        this.setAskPrice(askPrice);
        return this;
    }

    public void setAskPrice(BigDecimal askPrice) {
        this.askPrice = askPrice;
    }

    public BigDecimal getBidPrice() {
        return this.bidPrice;
    }

    public FxRate bidPrice(BigDecimal bidPrice) {
        this.setBidPrice(bidPrice);
        return this;
    }

    public void setBidPrice(BigDecimal bidPrice) {
        this.bidPrice = bidPrice;
    }

    public BigDecimal getMidPrice() {
        return this.midPrice;
    }

    public FxRate midPrice(BigDecimal midPrice) {
        this.setMidPrice(midPrice);
        return this;
    }

    public void setMidPrice(BigDecimal midPrice) {
        this.midPrice = midPrice;
    }

    public BigDecimal getAskMargin() {
        return this.askMargin;
    }

    public FxRate askMargin(BigDecimal askMargin) {
        this.setAskMargin(askMargin);
        return this;
    }

    public void setAskMargin(BigDecimal askMargin) {
        this.askMargin = askMargin;
    }

    public BigDecimal getBidMargin() {
        return this.bidMargin;
    }

    public FxRate bidMargin(BigDecimal bidMargin) {
        this.setBidMargin(bidMargin);
        return this;
    }

    public void setBidMargin(BigDecimal bidMargin) {
        this.bidMargin = bidMargin;
    }

    public String getSegment() {
        return this.segment;
    }

    public FxRate segment(String segment) {
        this.setSegment(segment);
        return this;
    }

    public void setSegment(String segment) {
        this.segment = segment;
    }

    public String getAmountSlab() {
        return this.amountSlab;
    }

    public FxRate amountSlab(String amountSlab) {
        this.setAmountSlab(amountSlab);
        return this;
    }

    public void setAmountSlab(String amountSlab) {
        this.amountSlab = amountSlab;
    }

    public BigDecimal getAmountSlabValue() {
        return this.amountSlabValue;
    }

    public FxRate amountSlabValue(BigDecimal amountSlabValue) {
        this.setAmountSlabValue(amountSlabValue);
        return this;
    }

    public void setAmountSlabValue(BigDecimal amountSlabValue) {
        this.amountSlabValue = amountSlabValue;
    }

    public String getAmountSlabOperator() {
        return this.amountSlabOperator;
    }

    public FxRate amountSlabOperator(String amountSlabOperator) {
        this.setAmountSlabOperator(amountSlabOperator);
        return this;
    }

    public void setAmountSlabOperator(String amountSlabOperator) {
        this.amountSlabOperator = amountSlabOperator;
    }

    public String getAmountSlabCurrency() {
        return this.amountSlabCurrency;
    }

    public FxRate amountSlabCurrency(String amountSlabCurrency) {
        this.setAmountSlabCurrency(amountSlabCurrency);
        return this;
    }

    public void setAmountSlabCurrency(String amountSlabCurrency) {
        this.amountSlabCurrency = amountSlabCurrency;
    }

    public FxRecordStatus getRecordStatus() {
        return this.recordStatus;
    }

    public FxRate recordStatus(FxRecordStatus recordStatus) {
        this.setRecordStatus(recordStatus);
        return this;
    }

    public void setRecordStatus(FxRecordStatus recordStatus) {
        this.recordStatus = recordStatus;
    }

    public LocalDate getValidfrom() {
        return this.validfrom;
    }

    public FxRate validfrom(LocalDate validfrom) {
        this.setValidfrom(validfrom);
        return this;
    }

    public void setValidfrom(LocalDate validfrom) {
        this.validfrom = validfrom;
    }

    public LocalDate getValidTo() {
        return this.validTo;
    }

    public FxRate validTo(LocalDate validTo) {
        this.setValidTo(validTo);
        return this;
    }

    public void setValidTo(LocalDate validTo) {
        this.validTo = validTo;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof FxRate)) {
            return false;
        }
        return id != null && id.equals(((FxRate) o).id);
    }

    @Override
    public int hashCode() {
        // see https://vladmihalcea.com/how-to-implement-equals-and-hashcode-using-the-jpa-entity-identifier/
        return getClass().hashCode();
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "FxRate{" +
            "id=" + getId() +
            ", type='" + getType() + "'" +
            ", baseCurrency='" + getBaseCurrency() + "'" +
            ", quoteCurrency='" + getQuoteCurrency() + "'" +
            ", askPrice=" + getAskPrice() +
            ", bidPrice=" + getBidPrice() +
            ", midPrice=" + getMidPrice() +
            ", askMargin=" + getAskMargin() +
            ", bidMargin=" + getBidMargin() +
            ", segment='" + getSegment() + "'" +
            ", amountSlab='" + getAmountSlab() + "'" +
            ", amountSlabValue=" + getAmountSlabValue() +
            ", amountSlabOperator='" + getAmountSlabOperator() + "'" +
            ", amountSlabCurrency='" + getAmountSlabCurrency() + "'" +
            ", recordStatus='" + getRecordStatus() + "'" +
            ", validfrom='" + getValidfrom() + "'" +
            ", validTo='" + getValidTo() + "'" +
            "}";
    }
}
