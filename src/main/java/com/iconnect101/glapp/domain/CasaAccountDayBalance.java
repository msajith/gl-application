package com.iconnect101.glapp.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.io.Serializable;
import java.math.BigDecimal;
import java.time.Instant;
import java.time.LocalDate;
import javax.persistence.*;
import javax.validation.constraints.*;

/**
 * A CasaAccountDayBalance.
 */
@Entity
@Table(name = "casa_account_day_balance")
public class CasaAccountDayBalance implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    @Column(name = "id")
    private Long id;

    @NotNull
    @Column(name = "account_no", nullable = false)
    private String accountNo;

    @Column(name = "date")
    private LocalDate date;

    @Column(name = "closing_balance", precision = 21, scale = 2)
    private BigDecimal closingBalance;

    @Column(name = "opening_balance", precision = 21, scale = 2)
    private BigDecimal openingBalance;

    @Column(name = "total_debit", precision = 21, scale = 2)
    private BigDecimal totalDebit;

    @Column(name = "total_credit", precision = 21, scale = 2)
    private BigDecimal totalCredit;

    @Column(name = "available_balance", precision = 21, scale = 2)
    private BigDecimal availableBalance;

    @Column(name = "current_balance", precision = 21, scale = 2)
    private BigDecimal currentBalance;

    @Column(name = "created_on")
    private Instant createdOn;

    @Column(name = "created_by")
    private String createdBy;

    @ManyToOne
    @JsonIgnoreProperties(value = { "accountType", "accountCategory", "accountCurrency", "customer" }, allowSetters = true)
    private CasaAccount casaAccount;

    // jhipster-needle-entity-add-field - JHipster will add fields here

    public Long getId() {
        return this.id;
    }

    public CasaAccountDayBalance id(Long id) {
        this.setId(id);
        return this;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getAccountNo() {
        return this.accountNo;
    }

    public CasaAccountDayBalance accountNo(String accountNo) {
        this.setAccountNo(accountNo);
        return this;
    }

    public void setAccountNo(String accountNo) {
        this.accountNo = accountNo;
    }

    public LocalDate getDate() {
        return this.date;
    }

    public CasaAccountDayBalance date(LocalDate date) {
        this.setDate(date);
        return this;
    }

    public void setDate(LocalDate date) {
        this.date = date;
    }

    public BigDecimal getClosingBalance() {
        return this.closingBalance;
    }

    public CasaAccountDayBalance closingBalance(BigDecimal closingBalance) {
        this.setClosingBalance(closingBalance);
        return this;
    }

    public void setClosingBalance(BigDecimal closingBalance) {
        this.closingBalance = closingBalance;
    }

    public BigDecimal getOpeningBalance() {
        return this.openingBalance;
    }

    public CasaAccountDayBalance openingBalance(BigDecimal openingBalance) {
        this.setOpeningBalance(openingBalance);
        return this;
    }

    public void setOpeningBalance(BigDecimal openingBalance) {
        this.openingBalance = openingBalance;
    }

    public BigDecimal getTotalDebit() {
        return this.totalDebit;
    }

    public CasaAccountDayBalance totalDebit(BigDecimal totalDebit) {
        this.setTotalDebit(totalDebit);
        return this;
    }

    public void setTotalDebit(BigDecimal totalDebit) {
        this.totalDebit = totalDebit;
    }

    public BigDecimal getTotalCredit() {
        return this.totalCredit;
    }

    public CasaAccountDayBalance totalCredit(BigDecimal totalCredit) {
        this.setTotalCredit(totalCredit);
        return this;
    }

    public void setTotalCredit(BigDecimal totalCredit) {
        this.totalCredit = totalCredit;
    }

    public BigDecimal getAvailableBalance() {
        return this.availableBalance;
    }

    public CasaAccountDayBalance availableBalance(BigDecimal availableBalance) {
        this.setAvailableBalance(availableBalance);
        return this;
    }

    public void setAvailableBalance(BigDecimal availableBalance) {
        this.availableBalance = availableBalance;
    }

    public BigDecimal getCurrentBalance() {
        return this.currentBalance;
    }

    public CasaAccountDayBalance currentBalance(BigDecimal currentBalance) {
        this.setCurrentBalance(currentBalance);
        return this;
    }

    public void setCurrentBalance(BigDecimal currentBalance) {
        this.currentBalance = currentBalance;
    }

    public Instant getCreatedOn() {
        return this.createdOn;
    }

    public CasaAccountDayBalance createdOn(Instant createdOn) {
        this.setCreatedOn(createdOn);
        return this;
    }

    public void setCreatedOn(Instant createdOn) {
        this.createdOn = createdOn;
    }

    public String getCreatedBy() {
        return this.createdBy;
    }

    public CasaAccountDayBalance createdBy(String createdBy) {
        this.setCreatedBy(createdBy);
        return this;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public CasaAccount getCasaAccount() {
        return this.casaAccount;
    }

    public void setCasaAccount(CasaAccount casaAccount) {
        this.casaAccount = casaAccount;
    }

    public CasaAccountDayBalance casaAccount(CasaAccount casaAccount) {
        this.setCasaAccount(casaAccount);
        return this;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof CasaAccountDayBalance)) {
            return false;
        }
        return id != null && id.equals(((CasaAccountDayBalance) o).id);
    }

    @Override
    public int hashCode() {
        // see https://vladmihalcea.com/how-to-implement-equals-and-hashcode-using-the-jpa-entity-identifier/
        return getClass().hashCode();
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "CasaAccountDayBalance{" +
            "id=" + getId() +
            ", accountNo='" + getAccountNo() + "'" +
            ", date='" + getDate() + "'" +
            ", closingBalance=" + getClosingBalance() +
            ", openingBalance=" + getOpeningBalance() +
            ", totalDebit=" + getTotalDebit() +
            ", totalCredit=" + getTotalCredit() +
            ", availableBalance=" + getAvailableBalance() +
            ", currentBalance=" + getCurrentBalance() +
            ", createdOn='" + getCreatedOn() + "'" +
            ", createdBy='" + getCreatedBy() + "'" +
            "}";
    }
}
