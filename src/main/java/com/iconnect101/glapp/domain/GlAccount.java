package com.iconnect101.glapp.domain;

import com.iconnect101.glapp.domain.enumeration.GlAccountStatus;
import com.iconnect101.glapp.domain.enumeration.GlAccountType;
import java.io.Serializable;
import java.math.BigDecimal;
import java.time.Instant;
import java.time.LocalDate;
import javax.persistence.*;
import javax.validation.constraints.*;

/**
 * A GlAccount.
 */
@Entity
@Table(name = "gl_account")
public class GlAccount implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    @Column(name = "id")
    private Long id;

    @NotNull
    @Column(name = "account_no", nullable = false)
    private String accountNo;

    @NotNull
    @Column(name = "account_name", nullable = false)
    private String accountName;

    @Column(name = "date")
    private LocalDate date;

    @Column(name = "closing_balance", precision = 21, scale = 2)
    private BigDecimal closingBalance;

    @Column(name = "opening_balance", precision = 21, scale = 2)
    private BigDecimal openingBalance;

    @Column(name = "total_debit", precision = 21, scale = 2)
    private BigDecimal totalDebit;

    @Column(name = "total_credit", precision = 21, scale = 2)
    private BigDecimal totalCredit;

    @Column(name = "available_balance", precision = 21, scale = 2)
    private BigDecimal availableBalance;

    @Column(name = "current_balance", precision = 21, scale = 2)
    private BigDecimal currentBalance;

    @Column(name = "created_on")
    private Instant createdOn;

    @Column(name = "created_by")
    private String createdBy;

    @Enumerated(EnumType.STRING)
    @Column(name = "gl_account_status")
    private GlAccountStatus glAccountStatus;

    @Enumerated(EnumType.STRING)
    @Column(name = "gl_account_type")
    private GlAccountType glAccountType;

    @ManyToOne
    private Currency accountCurrency;

    @ManyToOne
    private GlAccountCategory glAccountCategory;

    // jhipster-needle-entity-add-field - JHipster will add fields here

    public Long getId() {
        return this.id;
    }

    public GlAccount id(Long id) {
        this.setId(id);
        return this;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getAccountNo() {
        return this.accountNo;
    }

    public GlAccount accountNo(String accountNo) {
        this.setAccountNo(accountNo);
        return this;
    }

    public void setAccountNo(String accountNo) {
        this.accountNo = accountNo;
    }

    public String getAccountName() {
        return this.accountName;
    }

    public GlAccount accountName(String accountName) {
        this.setAccountName(accountName);
        return this;
    }

    public void setAccountName(String accountName) {
        this.accountName = accountName;
    }

    public LocalDate getDate() {
        return this.date;
    }

    public GlAccount date(LocalDate date) {
        this.setDate(date);
        return this;
    }

    public void setDate(LocalDate date) {
        this.date = date;
    }

    public BigDecimal getClosingBalance() {
        return this.closingBalance;
    }

    public GlAccount closingBalance(BigDecimal closingBalance) {
        this.setClosingBalance(closingBalance);
        return this;
    }

    public void setClosingBalance(BigDecimal closingBalance) {
        this.closingBalance = closingBalance;
    }

    public BigDecimal getOpeningBalance() {
        return this.openingBalance;
    }

    public GlAccount openingBalance(BigDecimal openingBalance) {
        this.setOpeningBalance(openingBalance);
        return this;
    }

    public void setOpeningBalance(BigDecimal openingBalance) {
        this.openingBalance = openingBalance;
    }

    public BigDecimal getTotalDebit() {
        return this.totalDebit;
    }

    public GlAccount totalDebit(BigDecimal totalDebit) {
        this.setTotalDebit(totalDebit);
        return this;
    }

    public void setTotalDebit(BigDecimal totalDebit) {
        this.totalDebit = totalDebit;
    }

    public BigDecimal getTotalCredit() {
        return this.totalCredit;
    }

    public GlAccount totalCredit(BigDecimal totalCredit) {
        this.setTotalCredit(totalCredit);
        return this;
    }

    public void setTotalCredit(BigDecimal totalCredit) {
        this.totalCredit = totalCredit;
    }

    public BigDecimal getAvailableBalance() {
        return this.availableBalance;
    }

    public GlAccount availableBalance(BigDecimal availableBalance) {
        this.setAvailableBalance(availableBalance);
        return this;
    }

    public void setAvailableBalance(BigDecimal availableBalance) {
        this.availableBalance = availableBalance;
    }

    public BigDecimal getCurrentBalance() {
        return this.currentBalance;
    }

    public GlAccount currentBalance(BigDecimal currentBalance) {
        this.setCurrentBalance(currentBalance);
        return this;
    }

    public void setCurrentBalance(BigDecimal currentBalance) {
        this.currentBalance = currentBalance;
    }

    public Instant getCreatedOn() {
        return this.createdOn;
    }

    public GlAccount createdOn(Instant createdOn) {
        this.setCreatedOn(createdOn);
        return this;
    }

    public void setCreatedOn(Instant createdOn) {
        this.createdOn = createdOn;
    }

    public String getCreatedBy() {
        return this.createdBy;
    }

    public GlAccount createdBy(String createdBy) {
        this.setCreatedBy(createdBy);
        return this;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public GlAccountStatus getGlAccountStatus() {
        return this.glAccountStatus;
    }

    public GlAccount glAccountStatus(GlAccountStatus glAccountStatus) {
        this.setGlAccountStatus(glAccountStatus);
        return this;
    }

    public void setGlAccountStatus(GlAccountStatus glAccountStatus) {
        this.glAccountStatus = glAccountStatus;
    }

    public GlAccountType getGlAccountType() {
        return this.glAccountType;
    }

    public GlAccount glAccountType(GlAccountType glAccountType) {
        this.setGlAccountType(glAccountType);
        return this;
    }

    public void setGlAccountType(GlAccountType glAccountType) {
        this.glAccountType = glAccountType;
    }

    public Currency getAccountCurrency() {
        return this.accountCurrency;
    }

    public void setAccountCurrency(Currency currency) {
        this.accountCurrency = currency;
    }

    public GlAccount accountCurrency(Currency currency) {
        this.setAccountCurrency(currency);
        return this;
    }

    public GlAccountCategory getGlAccountCategory() {
        return this.glAccountCategory;
    }

    public void setGlAccountCategory(GlAccountCategory glAccountCategory) {
        this.glAccountCategory = glAccountCategory;
    }

    public GlAccount glAccountCategory(GlAccountCategory glAccountCategory) {
        this.setGlAccountCategory(glAccountCategory);
        return this;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof GlAccount)) {
            return false;
        }
        return id != null && id.equals(((GlAccount) o).id);
    }

    @Override
    public int hashCode() {
        // see https://vladmihalcea.com/how-to-implement-equals-and-hashcode-using-the-jpa-entity-identifier/
        return getClass().hashCode();
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "GlAccount{" +
            "id=" + getId() +
            ", accountNo='" + getAccountNo() + "'" +
            ", accountName='" + getAccountName() + "'" +
            ", date='" + getDate() + "'" +
            ", closingBalance=" + getClosingBalance() +
            ", openingBalance=" + getOpeningBalance() +
            ", totalDebit=" + getTotalDebit() +
            ", totalCredit=" + getTotalCredit() +
            ", availableBalance=" + getAvailableBalance() +
            ", currentBalance=" + getCurrentBalance() +
            ", createdOn='" + getCreatedOn() + "'" +
            ", createdBy='" + getCreatedBy() + "'" +
            ", glAccountStatus='" + getGlAccountStatus() + "'" +
            ", glAccountType='" + getGlAccountType() + "'" +
            "}";
    }
}
