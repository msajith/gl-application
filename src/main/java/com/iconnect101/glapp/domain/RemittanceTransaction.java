package com.iconnect101.glapp.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.iconnect101.glapp.domain.enumeration.TxnStatus;
import java.io.Serializable;
import java.math.BigDecimal;
import java.time.Instant;
import java.time.LocalDate;
import java.time.ZonedDateTime;
import javax.persistence.*;

/**
 * A RemittanceTransaction.
 */
@Entity
@Table(name = "remittance_transaction")
public class RemittanceTransaction implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    @Column(name = "id")
    private Long id;

    @Column(name = "txn_ref_no")
    private String txnRefNo;

    @Column(name = "account_no")
    private String accountNo;

    @Column(name = "txn_date")
    private LocalDate txnDate;

    @Column(name = "txn_date_in_utc")
    private Instant txnDateInUTC;

    @Column(name = "txn_date_in_local")
    private ZonedDateTime txnDateInLocal;

    @Column(name = "txn_date_for_settlement")
    private ZonedDateTime txnDateForSettlement;

    @Column(name = "value_date")
    private LocalDate valueDate;

    @Column(name = "value_date_in_utc")
    private Instant valueDateInUTC;

    @Column(name = "value_date_in_local")
    private ZonedDateTime valueDateInLocal;

    @Column(name = "value_date_for_settlement")
    private ZonedDateTime valueDateForSettlement;

    @Column(name = "beneficiary_account_no")
    private String beneficiaryAccountNo;

    @Column(name = "beneficiary_name")
    private String beneficiaryName;

    @Column(name = "beneficiarybank_swift_code")
    private String beneficiarybankSwiftCode;

    @Column(name = "intermediary_bank_swift_code")
    private String intermediaryBankSwiftCode;

    @Column(name = "beneficiary_bank_name")
    private String beneficiaryBankName;

    @Column(name = "beneficiary_bank_branch_name")
    private String beneficiaryBankBranchName;

    @Column(name = "beneficiary_address_1")
    private String beneficiaryAddress1;

    @Column(name = "beneficiary_address_2")
    private String beneficiaryAddress2;

    @Column(name = "beneficiary_address_3")
    private String beneficiaryAddress3;

    @Column(name = "beneficiary_state")
    private String beneficiaryState;

    @Column(name = "beneficiary_pin_code")
    private String beneficiaryPinCode;

    @Column(name = "txn_amount", precision = 21, scale = 2)
    private BigDecimal txnAmount;

    @Column(name = "amount_in_account_currency", precision = 21, scale = 2)
    private BigDecimal amountInAccountCurrency;

    @Column(name = "amount_in_local_currency", precision = 21, scale = 2)
    private BigDecimal amountInLocalCurrency;

    @Column(name = "charge_in_account_currency", precision = 21, scale = 2)
    private BigDecimal chargeInAccountCurrency;

    @Column(name = "charge_in_local_currency", precision = 21, scale = 2)
    private BigDecimal chargeInLocalCurrency;

    @Column(name = "funding_source")
    private String fundingSource;

    @Column(name = "rate", precision = 21, scale = 2)
    private BigDecimal rate;

    @Column(name = "instructed_amount", precision = 21, scale = 2)
    private BigDecimal instructedAmount;

    @Column(name = "instructed_amount_currency")
    private String instructedAmountCurrency;

    @Column(name = "merchant_id")
    private String merchantID;

    @Column(name = "merchant_info")
    private String merchantInfo;

    @Column(name = "external_ref_no")
    private String externalRefNo;

    @Column(name = "txn_description")
    private String txnDescription;

    @Column(name = "txn_internal_description")
    private String txnInternalDescription;

    @Column(name = "txn_addnl_info")
    private String txnAddnlInfo;

    @Column(name = "created_on")
    private Instant createdOn;

    @Column(name = "created_by")
    private String createdBy;

    @Enumerated(EnumType.STRING)
    @Column(name = "txn_status")
    private TxnStatus txnStatus;

    @Column(name = "txn_debit_account_no")
    private String txnDebitAccountNo;

    @Column(name = "txn_credit_account_no")
    private String txnCreditAccountNo;

    @Column(name = "charge_debit_account_no")
    private String chargeDebitAccountNo;

    @Column(name = "charge_credit_account_no")
    private String chargeCreditAccountNo;

    @ManyToOne
    private Country country;

    @ManyToOne
    private Currency txnCurrency;

    @ManyToOne
    private Currency accountCurrency;

    @ManyToOne
    private Currency instructedCurrency;

    @ManyToOne
    private PurposeOfTxn purposeOfTxn;

    @ManyToOne
    private FundingSourceType fundingSourceType;

    @ManyToOne
    private TransactionType transactionType;

    @ManyToOne
    @JsonIgnoreProperties(
        value = { "addresses", "walletAccounts", "casaAccounts", "nationality", "customerType", "customerSegment" },
        allowSetters = true
    )
    private Customer customer;

    @ManyToOne
    @JsonIgnoreProperties(value = { "accountType", "accountCategory", "accountCurrency", "customer" }, allowSetters = true)
    private CasaAccount casaAccount;

    @ManyToOne
    @JsonIgnoreProperties(value = { "customer", "country", "currency" }, allowSetters = true)
    private Beneficiary beneficiary;

    // jhipster-needle-entity-add-field - JHipster will add fields here

    public Long getId() {
        return this.id;
    }

    public RemittanceTransaction id(Long id) {
        this.setId(id);
        return this;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTxnRefNo() {
        return this.txnRefNo;
    }

    public RemittanceTransaction txnRefNo(String txnRefNo) {
        this.setTxnRefNo(txnRefNo);
        return this;
    }

    public void setTxnRefNo(String txnRefNo) {
        this.txnRefNo = txnRefNo;
    }

    public String getAccountNo() {
        return this.accountNo;
    }

    public RemittanceTransaction accountNo(String accountNo) {
        this.setAccountNo(accountNo);
        return this;
    }

    public void setAccountNo(String accountNo) {
        this.accountNo = accountNo;
    }

    public LocalDate getTxnDate() {
        return this.txnDate;
    }

    public RemittanceTransaction txnDate(LocalDate txnDate) {
        this.setTxnDate(txnDate);
        return this;
    }

    public void setTxnDate(LocalDate txnDate) {
        this.txnDate = txnDate;
    }

    public Instant getTxnDateInUTC() {
        return this.txnDateInUTC;
    }

    public RemittanceTransaction txnDateInUTC(Instant txnDateInUTC) {
        this.setTxnDateInUTC(txnDateInUTC);
        return this;
    }

    public void setTxnDateInUTC(Instant txnDateInUTC) {
        this.txnDateInUTC = txnDateInUTC;
    }

    public ZonedDateTime getTxnDateInLocal() {
        return this.txnDateInLocal;
    }

    public RemittanceTransaction txnDateInLocal(ZonedDateTime txnDateInLocal) {
        this.setTxnDateInLocal(txnDateInLocal);
        return this;
    }

    public void setTxnDateInLocal(ZonedDateTime txnDateInLocal) {
        this.txnDateInLocal = txnDateInLocal;
    }

    public ZonedDateTime getTxnDateForSettlement() {
        return this.txnDateForSettlement;
    }

    public RemittanceTransaction txnDateForSettlement(ZonedDateTime txnDateForSettlement) {
        this.setTxnDateForSettlement(txnDateForSettlement);
        return this;
    }

    public void setTxnDateForSettlement(ZonedDateTime txnDateForSettlement) {
        this.txnDateForSettlement = txnDateForSettlement;
    }

    public LocalDate getValueDate() {
        return this.valueDate;
    }

    public RemittanceTransaction valueDate(LocalDate valueDate) {
        this.setValueDate(valueDate);
        return this;
    }

    public void setValueDate(LocalDate valueDate) {
        this.valueDate = valueDate;
    }

    public Instant getValueDateInUTC() {
        return this.valueDateInUTC;
    }

    public RemittanceTransaction valueDateInUTC(Instant valueDateInUTC) {
        this.setValueDateInUTC(valueDateInUTC);
        return this;
    }

    public void setValueDateInUTC(Instant valueDateInUTC) {
        this.valueDateInUTC = valueDateInUTC;
    }

    public ZonedDateTime getValueDateInLocal() {
        return this.valueDateInLocal;
    }

    public RemittanceTransaction valueDateInLocal(ZonedDateTime valueDateInLocal) {
        this.setValueDateInLocal(valueDateInLocal);
        return this;
    }

    public void setValueDateInLocal(ZonedDateTime valueDateInLocal) {
        this.valueDateInLocal = valueDateInLocal;
    }

    public ZonedDateTime getValueDateForSettlement() {
        return this.valueDateForSettlement;
    }

    public RemittanceTransaction valueDateForSettlement(ZonedDateTime valueDateForSettlement) {
        this.setValueDateForSettlement(valueDateForSettlement);
        return this;
    }

    public void setValueDateForSettlement(ZonedDateTime valueDateForSettlement) {
        this.valueDateForSettlement = valueDateForSettlement;
    }

    public String getBeneficiaryAccountNo() {
        return this.beneficiaryAccountNo;
    }

    public RemittanceTransaction beneficiaryAccountNo(String beneficiaryAccountNo) {
        this.setBeneficiaryAccountNo(beneficiaryAccountNo);
        return this;
    }

    public void setBeneficiaryAccountNo(String beneficiaryAccountNo) {
        this.beneficiaryAccountNo = beneficiaryAccountNo;
    }

    public String getBeneficiaryName() {
        return this.beneficiaryName;
    }

    public RemittanceTransaction beneficiaryName(String beneficiaryName) {
        this.setBeneficiaryName(beneficiaryName);
        return this;
    }

    public void setBeneficiaryName(String beneficiaryName) {
        this.beneficiaryName = beneficiaryName;
    }

    public String getBeneficiarybankSwiftCode() {
        return this.beneficiarybankSwiftCode;
    }

    public RemittanceTransaction beneficiarybankSwiftCode(String beneficiarybankSwiftCode) {
        this.setBeneficiarybankSwiftCode(beneficiarybankSwiftCode);
        return this;
    }

    public void setBeneficiarybankSwiftCode(String beneficiarybankSwiftCode) {
        this.beneficiarybankSwiftCode = beneficiarybankSwiftCode;
    }

    public String getIntermediaryBankSwiftCode() {
        return this.intermediaryBankSwiftCode;
    }

    public RemittanceTransaction intermediaryBankSwiftCode(String intermediaryBankSwiftCode) {
        this.setIntermediaryBankSwiftCode(intermediaryBankSwiftCode);
        return this;
    }

    public void setIntermediaryBankSwiftCode(String intermediaryBankSwiftCode) {
        this.intermediaryBankSwiftCode = intermediaryBankSwiftCode;
    }

    public String getBeneficiaryBankName() {
        return this.beneficiaryBankName;
    }

    public RemittanceTransaction beneficiaryBankName(String beneficiaryBankName) {
        this.setBeneficiaryBankName(beneficiaryBankName);
        return this;
    }

    public void setBeneficiaryBankName(String beneficiaryBankName) {
        this.beneficiaryBankName = beneficiaryBankName;
    }

    public String getBeneficiaryBankBranchName() {
        return this.beneficiaryBankBranchName;
    }

    public RemittanceTransaction beneficiaryBankBranchName(String beneficiaryBankBranchName) {
        this.setBeneficiaryBankBranchName(beneficiaryBankBranchName);
        return this;
    }

    public void setBeneficiaryBankBranchName(String beneficiaryBankBranchName) {
        this.beneficiaryBankBranchName = beneficiaryBankBranchName;
    }

    public String getBeneficiaryAddress1() {
        return this.beneficiaryAddress1;
    }

    public RemittanceTransaction beneficiaryAddress1(String beneficiaryAddress1) {
        this.setBeneficiaryAddress1(beneficiaryAddress1);
        return this;
    }

    public void setBeneficiaryAddress1(String beneficiaryAddress1) {
        this.beneficiaryAddress1 = beneficiaryAddress1;
    }

    public String getBeneficiaryAddress2() {
        return this.beneficiaryAddress2;
    }

    public RemittanceTransaction beneficiaryAddress2(String beneficiaryAddress2) {
        this.setBeneficiaryAddress2(beneficiaryAddress2);
        return this;
    }

    public void setBeneficiaryAddress2(String beneficiaryAddress2) {
        this.beneficiaryAddress2 = beneficiaryAddress2;
    }

    public String getBeneficiaryAddress3() {
        return this.beneficiaryAddress3;
    }

    public RemittanceTransaction beneficiaryAddress3(String beneficiaryAddress3) {
        this.setBeneficiaryAddress3(beneficiaryAddress3);
        return this;
    }

    public void setBeneficiaryAddress3(String beneficiaryAddress3) {
        this.beneficiaryAddress3 = beneficiaryAddress3;
    }

    public String getBeneficiaryState() {
        return this.beneficiaryState;
    }

    public RemittanceTransaction beneficiaryState(String beneficiaryState) {
        this.setBeneficiaryState(beneficiaryState);
        return this;
    }

    public void setBeneficiaryState(String beneficiaryState) {
        this.beneficiaryState = beneficiaryState;
    }

    public String getBeneficiaryPinCode() {
        return this.beneficiaryPinCode;
    }

    public RemittanceTransaction beneficiaryPinCode(String beneficiaryPinCode) {
        this.setBeneficiaryPinCode(beneficiaryPinCode);
        return this;
    }

    public void setBeneficiaryPinCode(String beneficiaryPinCode) {
        this.beneficiaryPinCode = beneficiaryPinCode;
    }

    public BigDecimal getTxnAmount() {
        return this.txnAmount;
    }

    public RemittanceTransaction txnAmount(BigDecimal txnAmount) {
        this.setTxnAmount(txnAmount);
        return this;
    }

    public void setTxnAmount(BigDecimal txnAmount) {
        this.txnAmount = txnAmount;
    }

    public BigDecimal getAmountInAccountCurrency() {
        return this.amountInAccountCurrency;
    }

    public RemittanceTransaction amountInAccountCurrency(BigDecimal amountInAccountCurrency) {
        this.setAmountInAccountCurrency(amountInAccountCurrency);
        return this;
    }

    public void setAmountInAccountCurrency(BigDecimal amountInAccountCurrency) {
        this.amountInAccountCurrency = amountInAccountCurrency;
    }

    public BigDecimal getAmountInLocalCurrency() {
        return this.amountInLocalCurrency;
    }

    public RemittanceTransaction amountInLocalCurrency(BigDecimal amountInLocalCurrency) {
        this.setAmountInLocalCurrency(amountInLocalCurrency);
        return this;
    }

    public void setAmountInLocalCurrency(BigDecimal amountInLocalCurrency) {
        this.amountInLocalCurrency = amountInLocalCurrency;
    }

    public BigDecimal getChargeInAccountCurrency() {
        return this.chargeInAccountCurrency;
    }

    public RemittanceTransaction chargeInAccountCurrency(BigDecimal chargeInAccountCurrency) {
        this.setChargeInAccountCurrency(chargeInAccountCurrency);
        return this;
    }

    public void setChargeInAccountCurrency(BigDecimal chargeInAccountCurrency) {
        this.chargeInAccountCurrency = chargeInAccountCurrency;
    }

    public BigDecimal getChargeInLocalCurrency() {
        return this.chargeInLocalCurrency;
    }

    public RemittanceTransaction chargeInLocalCurrency(BigDecimal chargeInLocalCurrency) {
        this.setChargeInLocalCurrency(chargeInLocalCurrency);
        return this;
    }

    public void setChargeInLocalCurrency(BigDecimal chargeInLocalCurrency) {
        this.chargeInLocalCurrency = chargeInLocalCurrency;
    }

    public String getFundingSource() {
        return this.fundingSource;
    }

    public RemittanceTransaction fundingSource(String fundingSource) {
        this.setFundingSource(fundingSource);
        return this;
    }

    public void setFundingSource(String fundingSource) {
        this.fundingSource = fundingSource;
    }

    public BigDecimal getRate() {
        return this.rate;
    }

    public RemittanceTransaction rate(BigDecimal rate) {
        this.setRate(rate);
        return this;
    }

    public void setRate(BigDecimal rate) {
        this.rate = rate;
    }

    public BigDecimal getInstructedAmount() {
        return this.instructedAmount;
    }

    public RemittanceTransaction instructedAmount(BigDecimal instructedAmount) {
        this.setInstructedAmount(instructedAmount);
        return this;
    }

    public void setInstructedAmount(BigDecimal instructedAmount) {
        this.instructedAmount = instructedAmount;
    }

    public String getInstructedAmountCurrency() {
        return this.instructedAmountCurrency;
    }

    public RemittanceTransaction instructedAmountCurrency(String instructedAmountCurrency) {
        this.setInstructedAmountCurrency(instructedAmountCurrency);
        return this;
    }

    public void setInstructedAmountCurrency(String instructedAmountCurrency) {
        this.instructedAmountCurrency = instructedAmountCurrency;
    }

    public String getMerchantID() {
        return this.merchantID;
    }

    public RemittanceTransaction merchantID(String merchantID) {
        this.setMerchantID(merchantID);
        return this;
    }

    public void setMerchantID(String merchantID) {
        this.merchantID = merchantID;
    }

    public String getMerchantInfo() {
        return this.merchantInfo;
    }

    public RemittanceTransaction merchantInfo(String merchantInfo) {
        this.setMerchantInfo(merchantInfo);
        return this;
    }

    public void setMerchantInfo(String merchantInfo) {
        this.merchantInfo = merchantInfo;
    }

    public String getExternalRefNo() {
        return this.externalRefNo;
    }

    public RemittanceTransaction externalRefNo(String externalRefNo) {
        this.setExternalRefNo(externalRefNo);
        return this;
    }

    public void setExternalRefNo(String externalRefNo) {
        this.externalRefNo = externalRefNo;
    }

    public String getTxnDescription() {
        return this.txnDescription;
    }

    public RemittanceTransaction txnDescription(String txnDescription) {
        this.setTxnDescription(txnDescription);
        return this;
    }

    public void setTxnDescription(String txnDescription) {
        this.txnDescription = txnDescription;
    }

    public String getTxnInternalDescription() {
        return this.txnInternalDescription;
    }

    public RemittanceTransaction txnInternalDescription(String txnInternalDescription) {
        this.setTxnInternalDescription(txnInternalDescription);
        return this;
    }

    public void setTxnInternalDescription(String txnInternalDescription) {
        this.txnInternalDescription = txnInternalDescription;
    }

    public String getTxnAddnlInfo() {
        return this.txnAddnlInfo;
    }

    public RemittanceTransaction txnAddnlInfo(String txnAddnlInfo) {
        this.setTxnAddnlInfo(txnAddnlInfo);
        return this;
    }

    public void setTxnAddnlInfo(String txnAddnlInfo) {
        this.txnAddnlInfo = txnAddnlInfo;
    }

    public Instant getCreatedOn() {
        return this.createdOn;
    }

    public RemittanceTransaction createdOn(Instant createdOn) {
        this.setCreatedOn(createdOn);
        return this;
    }

    public void setCreatedOn(Instant createdOn) {
        this.createdOn = createdOn;
    }

    public String getCreatedBy() {
        return this.createdBy;
    }

    public RemittanceTransaction createdBy(String createdBy) {
        this.setCreatedBy(createdBy);
        return this;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public TxnStatus getTxnStatus() {
        return this.txnStatus;
    }

    public RemittanceTransaction txnStatus(TxnStatus txnStatus) {
        this.setTxnStatus(txnStatus);
        return this;
    }

    public void setTxnStatus(TxnStatus txnStatus) {
        this.txnStatus = txnStatus;
    }

    public String getTxnDebitAccountNo() {
        return this.txnDebitAccountNo;
    }

    public RemittanceTransaction txnDebitAccountNo(String txnDebitAccountNo) {
        this.setTxnDebitAccountNo(txnDebitAccountNo);
        return this;
    }

    public void setTxnDebitAccountNo(String txnDebitAccountNo) {
        this.txnDebitAccountNo = txnDebitAccountNo;
    }

    public String getTxnCreditAccountNo() {
        return this.txnCreditAccountNo;
    }

    public RemittanceTransaction txnCreditAccountNo(String txnCreditAccountNo) {
        this.setTxnCreditAccountNo(txnCreditAccountNo);
        return this;
    }

    public void setTxnCreditAccountNo(String txnCreditAccountNo) {
        this.txnCreditAccountNo = txnCreditAccountNo;
    }

    public String getChargeDebitAccountNo() {
        return this.chargeDebitAccountNo;
    }

    public RemittanceTransaction chargeDebitAccountNo(String chargeDebitAccountNo) {
        this.setChargeDebitAccountNo(chargeDebitAccountNo);
        return this;
    }

    public void setChargeDebitAccountNo(String chargeDebitAccountNo) {
        this.chargeDebitAccountNo = chargeDebitAccountNo;
    }

    public String getChargeCreditAccountNo() {
        return this.chargeCreditAccountNo;
    }

    public RemittanceTransaction chargeCreditAccountNo(String chargeCreditAccountNo) {
        this.setChargeCreditAccountNo(chargeCreditAccountNo);
        return this;
    }

    public void setChargeCreditAccountNo(String chargeCreditAccountNo) {
        this.chargeCreditAccountNo = chargeCreditAccountNo;
    }

    public Country getCountry() {
        return this.country;
    }

    public void setCountry(Country country) {
        this.country = country;
    }

    public RemittanceTransaction country(Country country) {
        this.setCountry(country);
        return this;
    }

    public Currency getTxnCurrency() {
        return this.txnCurrency;
    }

    public void setTxnCurrency(Currency currency) {
        this.txnCurrency = currency;
    }

    public RemittanceTransaction txnCurrency(Currency currency) {
        this.setTxnCurrency(currency);
        return this;
    }

    public Currency getAccountCurrency() {
        return this.accountCurrency;
    }

    public void setAccountCurrency(Currency currency) {
        this.accountCurrency = currency;
    }

    public RemittanceTransaction accountCurrency(Currency currency) {
        this.setAccountCurrency(currency);
        return this;
    }

    public Currency getInstructedCurrency() {
        return this.instructedCurrency;
    }

    public void setInstructedCurrency(Currency currency) {
        this.instructedCurrency = currency;
    }

    public RemittanceTransaction instructedCurrency(Currency currency) {
        this.setInstructedCurrency(currency);
        return this;
    }

    public PurposeOfTxn getPurposeOfTxn() {
        return this.purposeOfTxn;
    }

    public void setPurposeOfTxn(PurposeOfTxn purposeOfTxn) {
        this.purposeOfTxn = purposeOfTxn;
    }

    public RemittanceTransaction purposeOfTxn(PurposeOfTxn purposeOfTxn) {
        this.setPurposeOfTxn(purposeOfTxn);
        return this;
    }

    public FundingSourceType getFundingSourceType() {
        return this.fundingSourceType;
    }

    public void setFundingSourceType(FundingSourceType fundingSourceType) {
        this.fundingSourceType = fundingSourceType;
    }

    public RemittanceTransaction fundingSourceType(FundingSourceType fundingSourceType) {
        this.setFundingSourceType(fundingSourceType);
        return this;
    }

    public TransactionType getTransactionType() {
        return this.transactionType;
    }

    public void setTransactionType(TransactionType transactionType) {
        this.transactionType = transactionType;
    }

    public RemittanceTransaction transactionType(TransactionType transactionType) {
        this.setTransactionType(transactionType);
        return this;
    }

    public Customer getCustomer() {
        return this.customer;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }

    public RemittanceTransaction customer(Customer customer) {
        this.setCustomer(customer);
        return this;
    }

    public CasaAccount getCasaAccount() {
        return this.casaAccount;
    }

    public void setCasaAccount(CasaAccount casaAccount) {
        this.casaAccount = casaAccount;
    }

    public RemittanceTransaction casaAccount(CasaAccount casaAccount) {
        this.setCasaAccount(casaAccount);
        return this;
    }

    public Beneficiary getBeneficiary() {
        return this.beneficiary;
    }

    public void setBeneficiary(Beneficiary beneficiary) {
        this.beneficiary = beneficiary;
    }

    public RemittanceTransaction beneficiary(Beneficiary beneficiary) {
        this.setBeneficiary(beneficiary);
        return this;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof RemittanceTransaction)) {
            return false;
        }
        return id != null && id.equals(((RemittanceTransaction) o).id);
    }

    @Override
    public int hashCode() {
        // see https://vladmihalcea.com/how-to-implement-equals-and-hashcode-using-the-jpa-entity-identifier/
        return getClass().hashCode();
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "RemittanceTransaction{" +
            "id=" + getId() +
            ", txnRefNo='" + getTxnRefNo() + "'" +
            ", accountNo='" + getAccountNo() + "'" +
            ", txnDate='" + getTxnDate() + "'" +
            ", txnDateInUTC='" + getTxnDateInUTC() + "'" +
            ", txnDateInLocal='" + getTxnDateInLocal() + "'" +
            ", txnDateForSettlement='" + getTxnDateForSettlement() + "'" +
            ", valueDate='" + getValueDate() + "'" +
            ", valueDateInUTC='" + getValueDateInUTC() + "'" +
            ", valueDateInLocal='" + getValueDateInLocal() + "'" +
            ", valueDateForSettlement='" + getValueDateForSettlement() + "'" +
            ", beneficiaryAccountNo='" + getBeneficiaryAccountNo() + "'" +
            ", beneficiaryName='" + getBeneficiaryName() + "'" +
            ", beneficiarybankSwiftCode='" + getBeneficiarybankSwiftCode() + "'" +
            ", intermediaryBankSwiftCode='" + getIntermediaryBankSwiftCode() + "'" +
            ", beneficiaryBankName='" + getBeneficiaryBankName() + "'" +
            ", beneficiaryBankBranchName='" + getBeneficiaryBankBranchName() + "'" +
            ", beneficiaryAddress1='" + getBeneficiaryAddress1() + "'" +
            ", beneficiaryAddress2='" + getBeneficiaryAddress2() + "'" +
            ", beneficiaryAddress3='" + getBeneficiaryAddress3() + "'" +
            ", beneficiaryState='" + getBeneficiaryState() + "'" +
            ", beneficiaryPinCode='" + getBeneficiaryPinCode() + "'" +
            ", txnAmount=" + getTxnAmount() +
            ", amountInAccountCurrency=" + getAmountInAccountCurrency() +
            ", amountInLocalCurrency=" + getAmountInLocalCurrency() +
            ", chargeInAccountCurrency=" + getChargeInAccountCurrency() +
            ", chargeInLocalCurrency=" + getChargeInLocalCurrency() +
            ", fundingSource='" + getFundingSource() + "'" +
            ", rate=" + getRate() +
            ", instructedAmount=" + getInstructedAmount() +
            ", instructedAmountCurrency='" + getInstructedAmountCurrency() + "'" +
            ", merchantID='" + getMerchantID() + "'" +
            ", merchantInfo='" + getMerchantInfo() + "'" +
            ", externalRefNo='" + getExternalRefNo() + "'" +
            ", txnDescription='" + getTxnDescription() + "'" +
            ", txnInternalDescription='" + getTxnInternalDescription() + "'" +
            ", txnAddnlInfo='" + getTxnAddnlInfo() + "'" +
            ", createdOn='" + getCreatedOn() + "'" +
            ", createdBy='" + getCreatedBy() + "'" +
            ", txnStatus='" + getTxnStatus() + "'" +
            ", txnDebitAccountNo='" + getTxnDebitAccountNo() + "'" +
            ", txnCreditAccountNo='" + getTxnCreditAccountNo() + "'" +
            ", chargeDebitAccountNo='" + getChargeDebitAccountNo() + "'" +
            ", chargeCreditAccountNo='" + getChargeCreditAccountNo() + "'" +
            "}";
    }
}
