package com.iconnect101.glapp.domain;

import com.iconnect101.glapp.domain.enumeration.TxnStatus;
import java.io.Serializable;
import java.math.BigDecimal;
import java.time.Instant;
import java.time.LocalDate;
import java.time.ZonedDateTime;
import javax.persistence.*;

/**
 * A WalletTransaction.
 */
@Entity
@Table(name = "wallet_transaction")
public class WalletTransaction implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    @Column(name = "id")
    private Long id;

    @Column(name = "txn_ref_no")
    private String txnRefNo;

    @Column(name = "account_no")
    private String accountNo;

    @Column(name = "txn_date")
    private LocalDate txnDate;

    @Column(name = "txn_date_in_utc")
    private Instant txnDateInUTC;

    @Column(name = "txn_date_in_local")
    private ZonedDateTime txnDateInLocal;

    @Column(name = "txn_date_for_settlement")
    private ZonedDateTime txnDateForSettlement;

    @Column(name = "value_date")
    private LocalDate valueDate;

    @Column(name = "value_date_in_utc")
    private Instant valueDateInUTC;

    @Column(name = "value_date_in_local")
    private ZonedDateTime valueDateInLocal;

    @Column(name = "value_date_for_settlement")
    private ZonedDateTime valueDateForSettlement;

    @Column(name = "txn_amount", precision = 21, scale = 2)
    private BigDecimal txnAmount;

    @Column(name = "instructed_amount", precision = 21, scale = 2)
    private BigDecimal instructedAmount;

    @Column(name = "amount_in_account_currency", precision = 21, scale = 2)
    private BigDecimal amountInAccountCurrency;

    @Column(name = "amount_in_local_currency", precision = 21, scale = 2)
    private BigDecimal amountInLocalCurrency;

    @Column(name = "charge_in_account_currency", precision = 21, scale = 2)
    private BigDecimal chargeInAccountCurrency;

    @Column(name = "charge_in_local_currency", precision = 21, scale = 2)
    private BigDecimal chargeInLocalCurrency;

    @Column(name = "funding_source")
    private String fundingSource;

    @Column(name = "merchant_id")
    private String merchantID;

    @Column(name = "merchant_info")
    private String merchantInfo;

    @Column(name = "external_ref_no")
    private String externalRefNo;

    @Column(name = "txn_description")
    private String txnDescription;

    @Column(name = "txn_internal_description")
    private String txnInternalDescription;

    @Column(name = "created_on")
    private Instant createdOn;

    @Column(name = "created_by")
    private String createdBy;

    @Enumerated(EnumType.STRING)
    @Column(name = "txn_status")
    private TxnStatus txnStatus;

    @Column(name = "txn_debit_account_no")
    private String txnDebitAccountNo;

    @Column(name = "txn_credit_account_no")
    private String txnCreditAccountNo;

    @Column(name = "charge_debit_account_no")
    private String chargeDebitAccountNo;

    @Column(name = "charge_credit_account_no")
    private String chargeCreditAccountNo;

    @ManyToOne
    private Country country;

    @ManyToOne
    private Currency txnCurrency;

    @ManyToOne
    private Currency accountCurrency;

    @ManyToOne
    private Currency instructedCurrency;

    @ManyToOne
    private PurposeOfTxn purposeOfTxn;

    @ManyToOne
    private FundingSourceType fundingSourceType;

    // jhipster-needle-entity-add-field - JHipster will add fields here

    public Long getId() {
        return this.id;
    }

    public WalletTransaction id(Long id) {
        this.setId(id);
        return this;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTxnRefNo() {
        return this.txnRefNo;
    }

    public WalletTransaction txnRefNo(String txnRefNo) {
        this.setTxnRefNo(txnRefNo);
        return this;
    }

    public void setTxnRefNo(String txnRefNo) {
        this.txnRefNo = txnRefNo;
    }

    public String getAccountNo() {
        return this.accountNo;
    }

    public WalletTransaction accountNo(String accountNo) {
        this.setAccountNo(accountNo);
        return this;
    }

    public void setAccountNo(String accountNo) {
        this.accountNo = accountNo;
    }

    public LocalDate getTxnDate() {
        return this.txnDate;
    }

    public WalletTransaction txnDate(LocalDate txnDate) {
        this.setTxnDate(txnDate);
        return this;
    }

    public void setTxnDate(LocalDate txnDate) {
        this.txnDate = txnDate;
    }

    public Instant getTxnDateInUTC() {
        return this.txnDateInUTC;
    }

    public WalletTransaction txnDateInUTC(Instant txnDateInUTC) {
        this.setTxnDateInUTC(txnDateInUTC);
        return this;
    }

    public void setTxnDateInUTC(Instant txnDateInUTC) {
        this.txnDateInUTC = txnDateInUTC;
    }

    public ZonedDateTime getTxnDateInLocal() {
        return this.txnDateInLocal;
    }

    public WalletTransaction txnDateInLocal(ZonedDateTime txnDateInLocal) {
        this.setTxnDateInLocal(txnDateInLocal);
        return this;
    }

    public void setTxnDateInLocal(ZonedDateTime txnDateInLocal) {
        this.txnDateInLocal = txnDateInLocal;
    }

    public ZonedDateTime getTxnDateForSettlement() {
        return this.txnDateForSettlement;
    }

    public WalletTransaction txnDateForSettlement(ZonedDateTime txnDateForSettlement) {
        this.setTxnDateForSettlement(txnDateForSettlement);
        return this;
    }

    public void setTxnDateForSettlement(ZonedDateTime txnDateForSettlement) {
        this.txnDateForSettlement = txnDateForSettlement;
    }

    public LocalDate getValueDate() {
        return this.valueDate;
    }

    public WalletTransaction valueDate(LocalDate valueDate) {
        this.setValueDate(valueDate);
        return this;
    }

    public void setValueDate(LocalDate valueDate) {
        this.valueDate = valueDate;
    }

    public Instant getValueDateInUTC() {
        return this.valueDateInUTC;
    }

    public WalletTransaction valueDateInUTC(Instant valueDateInUTC) {
        this.setValueDateInUTC(valueDateInUTC);
        return this;
    }

    public void setValueDateInUTC(Instant valueDateInUTC) {
        this.valueDateInUTC = valueDateInUTC;
    }

    public ZonedDateTime getValueDateInLocal() {
        return this.valueDateInLocal;
    }

    public WalletTransaction valueDateInLocal(ZonedDateTime valueDateInLocal) {
        this.setValueDateInLocal(valueDateInLocal);
        return this;
    }

    public void setValueDateInLocal(ZonedDateTime valueDateInLocal) {
        this.valueDateInLocal = valueDateInLocal;
    }

    public ZonedDateTime getValueDateForSettlement() {
        return this.valueDateForSettlement;
    }

    public WalletTransaction valueDateForSettlement(ZonedDateTime valueDateForSettlement) {
        this.setValueDateForSettlement(valueDateForSettlement);
        return this;
    }

    public void setValueDateForSettlement(ZonedDateTime valueDateForSettlement) {
        this.valueDateForSettlement = valueDateForSettlement;
    }

    public BigDecimal getTxnAmount() {
        return this.txnAmount;
    }

    public WalletTransaction txnAmount(BigDecimal txnAmount) {
        this.setTxnAmount(txnAmount);
        return this;
    }

    public void setTxnAmount(BigDecimal txnAmount) {
        this.txnAmount = txnAmount;
    }

    public BigDecimal getInstructedAmount() {
        return this.instructedAmount;
    }

    public WalletTransaction instructedAmount(BigDecimal instructedAmount) {
        this.setInstructedAmount(instructedAmount);
        return this;
    }

    public void setInstructedAmount(BigDecimal instructedAmount) {
        this.instructedAmount = instructedAmount;
    }

    public BigDecimal getAmountInAccountCurrency() {
        return this.amountInAccountCurrency;
    }

    public WalletTransaction amountInAccountCurrency(BigDecimal amountInAccountCurrency) {
        this.setAmountInAccountCurrency(amountInAccountCurrency);
        return this;
    }

    public void setAmountInAccountCurrency(BigDecimal amountInAccountCurrency) {
        this.amountInAccountCurrency = amountInAccountCurrency;
    }

    public BigDecimal getAmountInLocalCurrency() {
        return this.amountInLocalCurrency;
    }

    public WalletTransaction amountInLocalCurrency(BigDecimal amountInLocalCurrency) {
        this.setAmountInLocalCurrency(amountInLocalCurrency);
        return this;
    }

    public void setAmountInLocalCurrency(BigDecimal amountInLocalCurrency) {
        this.amountInLocalCurrency = amountInLocalCurrency;
    }

    public BigDecimal getChargeInAccountCurrency() {
        return this.chargeInAccountCurrency;
    }

    public WalletTransaction chargeInAccountCurrency(BigDecimal chargeInAccountCurrency) {
        this.setChargeInAccountCurrency(chargeInAccountCurrency);
        return this;
    }

    public void setChargeInAccountCurrency(BigDecimal chargeInAccountCurrency) {
        this.chargeInAccountCurrency = chargeInAccountCurrency;
    }

    public BigDecimal getChargeInLocalCurrency() {
        return this.chargeInLocalCurrency;
    }

    public WalletTransaction chargeInLocalCurrency(BigDecimal chargeInLocalCurrency) {
        this.setChargeInLocalCurrency(chargeInLocalCurrency);
        return this;
    }

    public void setChargeInLocalCurrency(BigDecimal chargeInLocalCurrency) {
        this.chargeInLocalCurrency = chargeInLocalCurrency;
    }

    public String getFundingSource() {
        return this.fundingSource;
    }

    public WalletTransaction fundingSource(String fundingSource) {
        this.setFundingSource(fundingSource);
        return this;
    }

    public void setFundingSource(String fundingSource) {
        this.fundingSource = fundingSource;
    }

    public String getMerchantID() {
        return this.merchantID;
    }

    public WalletTransaction merchantID(String merchantID) {
        this.setMerchantID(merchantID);
        return this;
    }

    public void setMerchantID(String merchantID) {
        this.merchantID = merchantID;
    }

    public String getMerchantInfo() {
        return this.merchantInfo;
    }

    public WalletTransaction merchantInfo(String merchantInfo) {
        this.setMerchantInfo(merchantInfo);
        return this;
    }

    public void setMerchantInfo(String merchantInfo) {
        this.merchantInfo = merchantInfo;
    }

    public String getExternalRefNo() {
        return this.externalRefNo;
    }

    public WalletTransaction externalRefNo(String externalRefNo) {
        this.setExternalRefNo(externalRefNo);
        return this;
    }

    public void setExternalRefNo(String externalRefNo) {
        this.externalRefNo = externalRefNo;
    }

    public String getTxnDescription() {
        return this.txnDescription;
    }

    public WalletTransaction txnDescription(String txnDescription) {
        this.setTxnDescription(txnDescription);
        return this;
    }

    public void setTxnDescription(String txnDescription) {
        this.txnDescription = txnDescription;
    }

    public String getTxnInternalDescription() {
        return this.txnInternalDescription;
    }

    public WalletTransaction txnInternalDescription(String txnInternalDescription) {
        this.setTxnInternalDescription(txnInternalDescription);
        return this;
    }

    public void setTxnInternalDescription(String txnInternalDescription) {
        this.txnInternalDescription = txnInternalDescription;
    }

    public Instant getCreatedOn() {
        return this.createdOn;
    }

    public WalletTransaction createdOn(Instant createdOn) {
        this.setCreatedOn(createdOn);
        return this;
    }

    public void setCreatedOn(Instant createdOn) {
        this.createdOn = createdOn;
    }

    public String getCreatedBy() {
        return this.createdBy;
    }

    public WalletTransaction createdBy(String createdBy) {
        this.setCreatedBy(createdBy);
        return this;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public TxnStatus getTxnStatus() {
        return this.txnStatus;
    }

    public WalletTransaction txnStatus(TxnStatus txnStatus) {
        this.setTxnStatus(txnStatus);
        return this;
    }

    public void setTxnStatus(TxnStatus txnStatus) {
        this.txnStatus = txnStatus;
    }

    public String getTxnDebitAccountNo() {
        return this.txnDebitAccountNo;
    }

    public WalletTransaction txnDebitAccountNo(String txnDebitAccountNo) {
        this.setTxnDebitAccountNo(txnDebitAccountNo);
        return this;
    }

    public void setTxnDebitAccountNo(String txnDebitAccountNo) {
        this.txnDebitAccountNo = txnDebitAccountNo;
    }

    public String getTxnCreditAccountNo() {
        return this.txnCreditAccountNo;
    }

    public WalletTransaction txnCreditAccountNo(String txnCreditAccountNo) {
        this.setTxnCreditAccountNo(txnCreditAccountNo);
        return this;
    }

    public void setTxnCreditAccountNo(String txnCreditAccountNo) {
        this.txnCreditAccountNo = txnCreditAccountNo;
    }

    public String getChargeDebitAccountNo() {
        return this.chargeDebitAccountNo;
    }

    public WalletTransaction chargeDebitAccountNo(String chargeDebitAccountNo) {
        this.setChargeDebitAccountNo(chargeDebitAccountNo);
        return this;
    }

    public void setChargeDebitAccountNo(String chargeDebitAccountNo) {
        this.chargeDebitAccountNo = chargeDebitAccountNo;
    }

    public String getChargeCreditAccountNo() {
        return this.chargeCreditAccountNo;
    }

    public WalletTransaction chargeCreditAccountNo(String chargeCreditAccountNo) {
        this.setChargeCreditAccountNo(chargeCreditAccountNo);
        return this;
    }

    public void setChargeCreditAccountNo(String chargeCreditAccountNo) {
        this.chargeCreditAccountNo = chargeCreditAccountNo;
    }

    public Country getCountry() {
        return this.country;
    }

    public void setCountry(Country country) {
        this.country = country;
    }

    public WalletTransaction country(Country country) {
        this.setCountry(country);
        return this;
    }

    public Currency getTxnCurrency() {
        return this.txnCurrency;
    }

    public void setTxnCurrency(Currency currency) {
        this.txnCurrency = currency;
    }

    public WalletTransaction txnCurrency(Currency currency) {
        this.setTxnCurrency(currency);
        return this;
    }

    public Currency getAccountCurrency() {
        return this.accountCurrency;
    }

    public void setAccountCurrency(Currency currency) {
        this.accountCurrency = currency;
    }

    public WalletTransaction accountCurrency(Currency currency) {
        this.setAccountCurrency(currency);
        return this;
    }

    public Currency getInstructedCurrency() {
        return this.instructedCurrency;
    }

    public void setInstructedCurrency(Currency currency) {
        this.instructedCurrency = currency;
    }

    public WalletTransaction instructedCurrency(Currency currency) {
        this.setInstructedCurrency(currency);
        return this;
    }

    public PurposeOfTxn getPurposeOfTxn() {
        return this.purposeOfTxn;
    }

    public void setPurposeOfTxn(PurposeOfTxn purposeOfTxn) {
        this.purposeOfTxn = purposeOfTxn;
    }

    public WalletTransaction purposeOfTxn(PurposeOfTxn purposeOfTxn) {
        this.setPurposeOfTxn(purposeOfTxn);
        return this;
    }

    public FundingSourceType getFundingSourceType() {
        return this.fundingSourceType;
    }

    public void setFundingSourceType(FundingSourceType fundingSourceType) {
        this.fundingSourceType = fundingSourceType;
    }

    public WalletTransaction fundingSourceType(FundingSourceType fundingSourceType) {
        this.setFundingSourceType(fundingSourceType);
        return this;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof WalletTransaction)) {
            return false;
        }
        return id != null && id.equals(((WalletTransaction) o).id);
    }

    @Override
    public int hashCode() {
        // see https://vladmihalcea.com/how-to-implement-equals-and-hashcode-using-the-jpa-entity-identifier/
        return getClass().hashCode();
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "WalletTransaction{" +
            "id=" + getId() +
            ", txnRefNo='" + getTxnRefNo() + "'" +
            ", accountNo='" + getAccountNo() + "'" +
            ", txnDate='" + getTxnDate() + "'" +
            ", txnDateInUTC='" + getTxnDateInUTC() + "'" +
            ", txnDateInLocal='" + getTxnDateInLocal() + "'" +
            ", txnDateForSettlement='" + getTxnDateForSettlement() + "'" +
            ", valueDate='" + getValueDate() + "'" +
            ", valueDateInUTC='" + getValueDateInUTC() + "'" +
            ", valueDateInLocal='" + getValueDateInLocal() + "'" +
            ", valueDateForSettlement='" + getValueDateForSettlement() + "'" +
            ", txnAmount=" + getTxnAmount() +
            ", instructedAmount=" + getInstructedAmount() +
            ", amountInAccountCurrency=" + getAmountInAccountCurrency() +
            ", amountInLocalCurrency=" + getAmountInLocalCurrency() +
            ", chargeInAccountCurrency=" + getChargeInAccountCurrency() +
            ", chargeInLocalCurrency=" + getChargeInLocalCurrency() +
            ", fundingSource='" + getFundingSource() + "'" +
            ", merchantID='" + getMerchantID() + "'" +
            ", merchantInfo='" + getMerchantInfo() + "'" +
            ", externalRefNo='" + getExternalRefNo() + "'" +
            ", txnDescription='" + getTxnDescription() + "'" +
            ", txnInternalDescription='" + getTxnInternalDescription() + "'" +
            ", createdOn='" + getCreatedOn() + "'" +
            ", createdBy='" + getCreatedBy() + "'" +
            ", txnStatus='" + getTxnStatus() + "'" +
            ", txnDebitAccountNo='" + getTxnDebitAccountNo() + "'" +
            ", txnCreditAccountNo='" + getTxnCreditAccountNo() + "'" +
            ", chargeDebitAccountNo='" + getChargeDebitAccountNo() + "'" +
            ", chargeCreditAccountNo='" + getChargeCreditAccountNo() + "'" +
            "}";
    }
}
