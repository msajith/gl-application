package com.iconnect101.glapp.domain;

import com.iconnect101.glapp.domain.enumeration.MerchantStatus;
import java.io.Serializable;
import java.time.Instant;
import javax.persistence.*;

/**
 * A Merchant.
 */
@Entity
@Table(name = "merchant")
public class Merchant implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    @Column(name = "id")
    private Long id;

    @Column(name = "merchant_id")
    private String merchantID;

    @Column(name = "merchant_name")
    private String merchantName;

    @Column(name = "merchant_category")
    private String merchantCategory;

    @Column(name = "merchant_gl_account_no")
    private String merchantGLAccountNo;

    @Column(name = "merchant_bank_account_no")
    private String merchantBankAccountNo;

    @Column(name = "merchant_bank_account_currency")
    private String merchantBankAccountCurrency;

    @Column(name = "merchant_bank_code")
    private String merchantBankCode;

    @Column(name = "merchant_bank_name")
    private String merchantBankName;

    @Column(name = "merchant_address")
    private String merchantAddress;

    @Column(name = "merchant_city")
    private String merchantCity;

    @Column(name = "created_on")
    private Instant createdOn;

    @Column(name = "created_by")
    private String createdBy;

    @Enumerated(EnumType.STRING)
    @Column(name = "merchant_stataus")
    private MerchantStatus merchantStataus;

    @ManyToOne
    private Country country;

    // jhipster-needle-entity-add-field - JHipster will add fields here

    public Long getId() {
        return this.id;
    }

    public Merchant id(Long id) {
        this.setId(id);
        return this;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getMerchantID() {
        return this.merchantID;
    }

    public Merchant merchantID(String merchantID) {
        this.setMerchantID(merchantID);
        return this;
    }

    public void setMerchantID(String merchantID) {
        this.merchantID = merchantID;
    }

    public String getMerchantName() {
        return this.merchantName;
    }

    public Merchant merchantName(String merchantName) {
        this.setMerchantName(merchantName);
        return this;
    }

    public void setMerchantName(String merchantName) {
        this.merchantName = merchantName;
    }

    public String getMerchantCategory() {
        return this.merchantCategory;
    }

    public Merchant merchantCategory(String merchantCategory) {
        this.setMerchantCategory(merchantCategory);
        return this;
    }

    public void setMerchantCategory(String merchantCategory) {
        this.merchantCategory = merchantCategory;
    }

    public String getMerchantGLAccountNo() {
        return this.merchantGLAccountNo;
    }

    public Merchant merchantGLAccountNo(String merchantGLAccountNo) {
        this.setMerchantGLAccountNo(merchantGLAccountNo);
        return this;
    }

    public void setMerchantGLAccountNo(String merchantGLAccountNo) {
        this.merchantGLAccountNo = merchantGLAccountNo;
    }

    public String getMerchantBankAccountNo() {
        return this.merchantBankAccountNo;
    }

    public Merchant merchantBankAccountNo(String merchantBankAccountNo) {
        this.setMerchantBankAccountNo(merchantBankAccountNo);
        return this;
    }

    public void setMerchantBankAccountNo(String merchantBankAccountNo) {
        this.merchantBankAccountNo = merchantBankAccountNo;
    }

    public String getMerchantBankAccountCurrency() {
        return this.merchantBankAccountCurrency;
    }

    public Merchant merchantBankAccountCurrency(String merchantBankAccountCurrency) {
        this.setMerchantBankAccountCurrency(merchantBankAccountCurrency);
        return this;
    }

    public void setMerchantBankAccountCurrency(String merchantBankAccountCurrency) {
        this.merchantBankAccountCurrency = merchantBankAccountCurrency;
    }

    public String getMerchantBankCode() {
        return this.merchantBankCode;
    }

    public Merchant merchantBankCode(String merchantBankCode) {
        this.setMerchantBankCode(merchantBankCode);
        return this;
    }

    public void setMerchantBankCode(String merchantBankCode) {
        this.merchantBankCode = merchantBankCode;
    }

    public String getMerchantBankName() {
        return this.merchantBankName;
    }

    public Merchant merchantBankName(String merchantBankName) {
        this.setMerchantBankName(merchantBankName);
        return this;
    }

    public void setMerchantBankName(String merchantBankName) {
        this.merchantBankName = merchantBankName;
    }

    public String getMerchantAddress() {
        return this.merchantAddress;
    }

    public Merchant merchantAddress(String merchantAddress) {
        this.setMerchantAddress(merchantAddress);
        return this;
    }

    public void setMerchantAddress(String merchantAddress) {
        this.merchantAddress = merchantAddress;
    }

    public String getMerchantCity() {
        return this.merchantCity;
    }

    public Merchant merchantCity(String merchantCity) {
        this.setMerchantCity(merchantCity);
        return this;
    }

    public void setMerchantCity(String merchantCity) {
        this.merchantCity = merchantCity;
    }

    public Instant getCreatedOn() {
        return this.createdOn;
    }

    public Merchant createdOn(Instant createdOn) {
        this.setCreatedOn(createdOn);
        return this;
    }

    public void setCreatedOn(Instant createdOn) {
        this.createdOn = createdOn;
    }

    public String getCreatedBy() {
        return this.createdBy;
    }

    public Merchant createdBy(String createdBy) {
        this.setCreatedBy(createdBy);
        return this;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public MerchantStatus getMerchantStataus() {
        return this.merchantStataus;
    }

    public Merchant merchantStataus(MerchantStatus merchantStataus) {
        this.setMerchantStataus(merchantStataus);
        return this;
    }

    public void setMerchantStataus(MerchantStatus merchantStataus) {
        this.merchantStataus = merchantStataus;
    }

    public Country getCountry() {
        return this.country;
    }

    public void setCountry(Country country) {
        this.country = country;
    }

    public Merchant country(Country country) {
        this.setCountry(country);
        return this;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Merchant)) {
            return false;
        }
        return id != null && id.equals(((Merchant) o).id);
    }

    @Override
    public int hashCode() {
        // see https://vladmihalcea.com/how-to-implement-equals-and-hashcode-using-the-jpa-entity-identifier/
        return getClass().hashCode();
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "Merchant{" +
            "id=" + getId() +
            ", merchantID='" + getMerchantID() + "'" +
            ", merchantName='" + getMerchantName() + "'" +
            ", merchantCategory='" + getMerchantCategory() + "'" +
            ", merchantGLAccountNo='" + getMerchantGLAccountNo() + "'" +
            ", merchantBankAccountNo='" + getMerchantBankAccountNo() + "'" +
            ", merchantBankAccountCurrency='" + getMerchantBankAccountCurrency() + "'" +
            ", merchantBankCode='" + getMerchantBankCode() + "'" +
            ", merchantBankName='" + getMerchantBankName() + "'" +
            ", merchantAddress='" + getMerchantAddress() + "'" +
            ", merchantCity='" + getMerchantCity() + "'" +
            ", createdOn='" + getCreatedOn() + "'" +
            ", createdBy='" + getCreatedBy() + "'" +
            ", merchantStataus='" + getMerchantStataus() + "'" +
            "}";
    }
}
