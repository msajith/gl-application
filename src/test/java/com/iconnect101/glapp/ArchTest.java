package com.iconnect101.glapp;

import static com.tngtech.archunit.lang.syntax.ArchRuleDefinition.noClasses;

import com.tngtech.archunit.core.domain.JavaClasses;
import com.tngtech.archunit.core.importer.ClassFileImporter;
import com.tngtech.archunit.core.importer.ImportOption;
import org.junit.jupiter.api.Test;

class ArchTest {

    @Test
    void servicesAndRepositoriesShouldNotDependOnWebLayer() {
        JavaClasses importedClasses = new ClassFileImporter()
            .withImportOption(ImportOption.Predefined.DO_NOT_INCLUDE_TESTS)
            .importPackages("com.iconnect101.glapp");

        noClasses()
            .that()
            .resideInAnyPackage("com.iconnect101.glapp.service..")
            .or()
            .resideInAnyPackage("com.iconnect101.glapp.repository..")
            .should()
            .dependOnClassesThat()
            .resideInAnyPackage("..com.iconnect101.glapp.web..")
            .because("Services and repositories should not depend on web layer")
            .check(importedClasses);
    }
}
