package com.iconnect101.glapp.domain;

import static org.assertj.core.api.Assertions.assertThat;

import com.iconnect101.glapp.web.rest.TestUtil;
import org.junit.jupiter.api.Test;

class GlAccountCategoryTest {

    @Test
    void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(GlAccountCategory.class);
        GlAccountCategory glAccountCategory1 = new GlAccountCategory();
        glAccountCategory1.setId(1L);
        GlAccountCategory glAccountCategory2 = new GlAccountCategory();
        glAccountCategory2.setId(glAccountCategory1.getId());
        assertThat(glAccountCategory1).isEqualTo(glAccountCategory2);
        glAccountCategory2.setId(2L);
        assertThat(glAccountCategory1).isNotEqualTo(glAccountCategory2);
        glAccountCategory1.setId(null);
        assertThat(glAccountCategory1).isNotEqualTo(glAccountCategory2);
    }
}
