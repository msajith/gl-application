package com.iconnect101.glapp.domain;

import static org.assertj.core.api.Assertions.assertThat;

import com.iconnect101.glapp.web.rest.TestUtil;
import org.junit.jupiter.api.Test;

class GlAccountTest {

    @Test
    void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(GlAccount.class);
        GlAccount glAccount1 = new GlAccount();
        glAccount1.setId(1L);
        GlAccount glAccount2 = new GlAccount();
        glAccount2.setId(glAccount1.getId());
        assertThat(glAccount1).isEqualTo(glAccount2);
        glAccount2.setId(2L);
        assertThat(glAccount1).isNotEqualTo(glAccount2);
        glAccount1.setId(null);
        assertThat(glAccount1).isNotEqualTo(glAccount2);
    }
}
