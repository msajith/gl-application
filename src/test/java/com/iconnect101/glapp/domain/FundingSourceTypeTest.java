package com.iconnect101.glapp.domain;

import static org.assertj.core.api.Assertions.assertThat;

import com.iconnect101.glapp.web.rest.TestUtil;
import org.junit.jupiter.api.Test;

class FundingSourceTypeTest {

    @Test
    void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(FundingSourceType.class);
        FundingSourceType fundingSourceType1 = new FundingSourceType();
        fundingSourceType1.setId(1L);
        FundingSourceType fundingSourceType2 = new FundingSourceType();
        fundingSourceType2.setId(fundingSourceType1.getId());
        assertThat(fundingSourceType1).isEqualTo(fundingSourceType2);
        fundingSourceType2.setId(2L);
        assertThat(fundingSourceType1).isNotEqualTo(fundingSourceType2);
        fundingSourceType1.setId(null);
        assertThat(fundingSourceType1).isNotEqualTo(fundingSourceType2);
    }
}
