package com.iconnect101.glapp.domain;

import static org.assertj.core.api.Assertions.assertThat;

import com.iconnect101.glapp.web.rest.TestUtil;
import org.junit.jupiter.api.Test;

class WalletTopupTransactionTest {

    @Test
    void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(WalletTopupTransaction.class);
        WalletTopupTransaction walletTopupTransaction1 = new WalletTopupTransaction();
        walletTopupTransaction1.setId(1L);
        WalletTopupTransaction walletTopupTransaction2 = new WalletTopupTransaction();
        walletTopupTransaction2.setId(walletTopupTransaction1.getId());
        assertThat(walletTopupTransaction1).isEqualTo(walletTopupTransaction2);
        walletTopupTransaction2.setId(2L);
        assertThat(walletTopupTransaction1).isNotEqualTo(walletTopupTransaction2);
        walletTopupTransaction1.setId(null);
        assertThat(walletTopupTransaction1).isNotEqualTo(walletTopupTransaction2);
    }
}
