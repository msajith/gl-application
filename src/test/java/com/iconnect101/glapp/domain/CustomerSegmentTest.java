package com.iconnect101.glapp.domain;

import static org.assertj.core.api.Assertions.assertThat;

import com.iconnect101.glapp.web.rest.TestUtil;
import org.junit.jupiter.api.Test;

class CustomerSegmentTest {

    @Test
    void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(CustomerSegment.class);
        CustomerSegment customerSegment1 = new CustomerSegment();
        customerSegment1.setId(1L);
        CustomerSegment customerSegment2 = new CustomerSegment();
        customerSegment2.setId(customerSegment1.getId());
        assertThat(customerSegment1).isEqualTo(customerSegment2);
        customerSegment2.setId(2L);
        assertThat(customerSegment1).isNotEqualTo(customerSegment2);
        customerSegment1.setId(null);
        assertThat(customerSegment1).isNotEqualTo(customerSegment2);
    }
}
