package com.iconnect101.glapp.domain;

import static org.assertj.core.api.Assertions.assertThat;

import com.iconnect101.glapp.web.rest.TestUtil;
import org.junit.jupiter.api.Test;

class GeneralLedgerTest {

    @Test
    void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(GeneralLedger.class);
        GeneralLedger generalLedger1 = new GeneralLedger();
        generalLedger1.setId(1L);
        GeneralLedger generalLedger2 = new GeneralLedger();
        generalLedger2.setId(generalLedger1.getId());
        assertThat(generalLedger1).isEqualTo(generalLedger2);
        generalLedger2.setId(2L);
        assertThat(generalLedger1).isNotEqualTo(generalLedger2);
        generalLedger1.setId(null);
        assertThat(generalLedger1).isNotEqualTo(generalLedger2);
    }
}
