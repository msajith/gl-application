package com.iconnect101.glapp.domain;

import static org.assertj.core.api.Assertions.assertThat;

import com.iconnect101.glapp.web.rest.TestUtil;
import org.junit.jupiter.api.Test;

class RemittanceTransactionTest {

    @Test
    void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(RemittanceTransaction.class);
        RemittanceTransaction remittanceTransaction1 = new RemittanceTransaction();
        remittanceTransaction1.setId(1L);
        RemittanceTransaction remittanceTransaction2 = new RemittanceTransaction();
        remittanceTransaction2.setId(remittanceTransaction1.getId());
        assertThat(remittanceTransaction1).isEqualTo(remittanceTransaction2);
        remittanceTransaction2.setId(2L);
        assertThat(remittanceTransaction1).isNotEqualTo(remittanceTransaction2);
        remittanceTransaction1.setId(null);
        assertThat(remittanceTransaction1).isNotEqualTo(remittanceTransaction2);
    }
}
