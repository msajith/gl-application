package com.iconnect101.glapp.domain;

import static org.assertj.core.api.Assertions.assertThat;

import com.iconnect101.glapp.web.rest.TestUtil;
import org.junit.jupiter.api.Test;

class PurposeOfTxnTest {

    @Test
    void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(PurposeOfTxn.class);
        PurposeOfTxn purposeOfTxn1 = new PurposeOfTxn();
        purposeOfTxn1.setId(1L);
        PurposeOfTxn purposeOfTxn2 = new PurposeOfTxn();
        purposeOfTxn2.setId(purposeOfTxn1.getId());
        assertThat(purposeOfTxn1).isEqualTo(purposeOfTxn2);
        purposeOfTxn2.setId(2L);
        assertThat(purposeOfTxn1).isNotEqualTo(purposeOfTxn2);
        purposeOfTxn1.setId(null);
        assertThat(purposeOfTxn1).isNotEqualTo(purposeOfTxn2);
    }
}
