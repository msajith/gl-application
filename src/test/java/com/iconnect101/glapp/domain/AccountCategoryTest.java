package com.iconnect101.glapp.domain;

import static org.assertj.core.api.Assertions.assertThat;

import com.iconnect101.glapp.web.rest.TestUtil;
import org.junit.jupiter.api.Test;

class AccountCategoryTest {

    @Test
    void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(AccountCategory.class);
        AccountCategory accountCategory1 = new AccountCategory();
        accountCategory1.setId(1L);
        AccountCategory accountCategory2 = new AccountCategory();
        accountCategory2.setId(accountCategory1.getId());
        assertThat(accountCategory1).isEqualTo(accountCategory2);
        accountCategory2.setId(2L);
        assertThat(accountCategory1).isNotEqualTo(accountCategory2);
        accountCategory1.setId(null);
        assertThat(accountCategory1).isNotEqualTo(accountCategory2);
    }
}
