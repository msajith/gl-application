package com.iconnect101.glapp.domain;

import static org.assertj.core.api.Assertions.assertThat;

import com.iconnect101.glapp.web.rest.TestUtil;
import org.junit.jupiter.api.Test;

class SubLedgerTest {

    @Test
    void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(SubLedger.class);
        SubLedger subLedger1 = new SubLedger();
        subLedger1.setId(1L);
        SubLedger subLedger2 = new SubLedger();
        subLedger2.setId(subLedger1.getId());
        assertThat(subLedger1).isEqualTo(subLedger2);
        subLedger2.setId(2L);
        assertThat(subLedger1).isNotEqualTo(subLedger2);
        subLedger1.setId(null);
        assertThat(subLedger1).isNotEqualTo(subLedger2);
    }
}
