package com.iconnect101.glapp.domain;

import static org.assertj.core.api.Assertions.assertThat;

import com.iconnect101.glapp.web.rest.TestUtil;
import org.junit.jupiter.api.Test;

class CasaAccountDayBalanceTest {

    @Test
    void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(CasaAccountDayBalance.class);
        CasaAccountDayBalance casaAccountDayBalance1 = new CasaAccountDayBalance();
        casaAccountDayBalance1.setId(1L);
        CasaAccountDayBalance casaAccountDayBalance2 = new CasaAccountDayBalance();
        casaAccountDayBalance2.setId(casaAccountDayBalance1.getId());
        assertThat(casaAccountDayBalance1).isEqualTo(casaAccountDayBalance2);
        casaAccountDayBalance2.setId(2L);
        assertThat(casaAccountDayBalance1).isNotEqualTo(casaAccountDayBalance2);
        casaAccountDayBalance1.setId(null);
        assertThat(casaAccountDayBalance1).isNotEqualTo(casaAccountDayBalance2);
    }
}
