package com.iconnect101.glapp.domain;

import static org.assertj.core.api.Assertions.assertThat;

import com.iconnect101.glapp.web.rest.TestUtil;
import org.junit.jupiter.api.Test;

class FxRateTest {

    @Test
    void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(FxRate.class);
        FxRate fxRate1 = new FxRate();
        fxRate1.setId(1L);
        FxRate fxRate2 = new FxRate();
        fxRate2.setId(fxRate1.getId());
        assertThat(fxRate1).isEqualTo(fxRate2);
        fxRate2.setId(2L);
        assertThat(fxRate1).isNotEqualTo(fxRate2);
        fxRate1.setId(null);
        assertThat(fxRate1).isNotEqualTo(fxRate2);
    }
}
