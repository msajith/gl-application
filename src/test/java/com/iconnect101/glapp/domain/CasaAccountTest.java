package com.iconnect101.glapp.domain;

import static org.assertj.core.api.Assertions.assertThat;

import com.iconnect101.glapp.web.rest.TestUtil;
import org.junit.jupiter.api.Test;

class CasaAccountTest {

    @Test
    void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(CasaAccount.class);
        CasaAccount casaAccount1 = new CasaAccount();
        casaAccount1.setId(1L);
        CasaAccount casaAccount2 = new CasaAccount();
        casaAccount2.setId(casaAccount1.getId());
        assertThat(casaAccount1).isEqualTo(casaAccount2);
        casaAccount2.setId(2L);
        assertThat(casaAccount1).isNotEqualTo(casaAccount2);
        casaAccount1.setId(null);
        assertThat(casaAccount1).isNotEqualTo(casaAccount2);
    }
}
