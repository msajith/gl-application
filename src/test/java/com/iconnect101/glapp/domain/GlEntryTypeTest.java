package com.iconnect101.glapp.domain;

import static org.assertj.core.api.Assertions.assertThat;

import com.iconnect101.glapp.web.rest.TestUtil;
import org.junit.jupiter.api.Test;

class GlEntryTypeTest {

    @Test
    void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(GlEntryType.class);
        GlEntryType glEntryType1 = new GlEntryType();
        glEntryType1.setId(1L);
        GlEntryType glEntryType2 = new GlEntryType();
        glEntryType2.setId(glEntryType1.getId());
        assertThat(glEntryType1).isEqualTo(glEntryType2);
        glEntryType2.setId(2L);
        assertThat(glEntryType1).isNotEqualTo(glEntryType2);
        glEntryType1.setId(null);
        assertThat(glEntryType1).isNotEqualTo(glEntryType2);
    }
}
