package com.iconnect101.glapp.domain;

import static org.assertj.core.api.Assertions.assertThat;

import com.iconnect101.glapp.web.rest.TestUtil;
import org.junit.jupiter.api.Test;

class WalletTransactionTest {

    @Test
    void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(WalletTransaction.class);
        WalletTransaction walletTransaction1 = new WalletTransaction();
        walletTransaction1.setId(1L);
        WalletTransaction walletTransaction2 = new WalletTransaction();
        walletTransaction2.setId(walletTransaction1.getId());
        assertThat(walletTransaction1).isEqualTo(walletTransaction2);
        walletTransaction2.setId(2L);
        assertThat(walletTransaction1).isNotEqualTo(walletTransaction2);
        walletTransaction1.setId(null);
        assertThat(walletTransaction1).isNotEqualTo(walletTransaction2);
    }
}
