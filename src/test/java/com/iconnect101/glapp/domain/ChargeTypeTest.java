package com.iconnect101.glapp.domain;

import static org.assertj.core.api.Assertions.assertThat;

import com.iconnect101.glapp.web.rest.TestUtil;
import org.junit.jupiter.api.Test;

class ChargeTypeTest {

    @Test
    void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(ChargeType.class);
        ChargeType chargeType1 = new ChargeType();
        chargeType1.setId(1L);
        ChargeType chargeType2 = new ChargeType();
        chargeType2.setId(chargeType1.getId());
        assertThat(chargeType1).isEqualTo(chargeType2);
        chargeType2.setId(2L);
        assertThat(chargeType1).isNotEqualTo(chargeType2);
        chargeType1.setId(null);
        assertThat(chargeType1).isNotEqualTo(chargeType2);
    }
}
