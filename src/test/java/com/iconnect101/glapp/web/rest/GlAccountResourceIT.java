package com.iconnect101.glapp.web.rest;

import static com.iconnect101.glapp.web.rest.TestUtil.sameNumber;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import com.iconnect101.glapp.IntegrationTest;
import com.iconnect101.glapp.domain.GlAccount;
import com.iconnect101.glapp.domain.enumeration.GlAccountStatus;
import com.iconnect101.glapp.domain.enumeration.GlAccountType;
import com.iconnect101.glapp.repository.GlAccountRepository;
import java.math.BigDecimal;
import java.time.Instant;
import java.time.LocalDate;
import java.time.ZoneId;
import java.time.temporal.ChronoUnit;
import java.util.List;
import java.util.Random;
import java.util.concurrent.atomic.AtomicLong;
import javax.persistence.EntityManager;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;

/**
 * Integration tests for the {@link GlAccountResource} REST controller.
 */
@IntegrationTest
@AutoConfigureMockMvc
@WithMockUser
class GlAccountResourceIT {

    private static final String DEFAULT_ACCOUNT_NO = "AAAAAAAAAA";
    private static final String UPDATED_ACCOUNT_NO = "BBBBBBBBBB";

    private static final String DEFAULT_ACCOUNT_NAME = "AAAAAAAAAA";
    private static final String UPDATED_ACCOUNT_NAME = "BBBBBBBBBB";

    private static final LocalDate DEFAULT_DATE = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_DATE = LocalDate.now(ZoneId.systemDefault());

    private static final BigDecimal DEFAULT_CLOSING_BALANCE = new BigDecimal(1);
    private static final BigDecimal UPDATED_CLOSING_BALANCE = new BigDecimal(2);

    private static final BigDecimal DEFAULT_OPENING_BALANCE = new BigDecimal(1);
    private static final BigDecimal UPDATED_OPENING_BALANCE = new BigDecimal(2);

    private static final BigDecimal DEFAULT_TOTAL_DEBIT = new BigDecimal(1);
    private static final BigDecimal UPDATED_TOTAL_DEBIT = new BigDecimal(2);

    private static final BigDecimal DEFAULT_TOTAL_CREDIT = new BigDecimal(1);
    private static final BigDecimal UPDATED_TOTAL_CREDIT = new BigDecimal(2);

    private static final BigDecimal DEFAULT_AVAILABLE_BALANCE = new BigDecimal(1);
    private static final BigDecimal UPDATED_AVAILABLE_BALANCE = new BigDecimal(2);

    private static final BigDecimal DEFAULT_CURRENT_BALANCE = new BigDecimal(1);
    private static final BigDecimal UPDATED_CURRENT_BALANCE = new BigDecimal(2);

    private static final Instant DEFAULT_CREATED_ON = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_CREATED_ON = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    private static final String DEFAULT_CREATED_BY = "AAAAAAAAAA";
    private static final String UPDATED_CREATED_BY = "BBBBBBBBBB";

    private static final GlAccountStatus DEFAULT_GL_ACCOUNT_STATUS = GlAccountStatus.ACTIVE;
    private static final GlAccountStatus UPDATED_GL_ACCOUNT_STATUS = GlAccountStatus.INACTIVE;

    private static final GlAccountType DEFAULT_GL_ACCOUNT_TYPE = GlAccountType.ASSET;
    private static final GlAccountType UPDATED_GL_ACCOUNT_TYPE = GlAccountType.LIABILITY;

    private static final String ENTITY_API_URL = "/api/gl-accounts";
    private static final String ENTITY_API_URL_ID = ENTITY_API_URL + "/{id}";

    private static Random random = new Random();
    private static AtomicLong count = new AtomicLong(random.nextInt() + (2 * Integer.MAX_VALUE));

    @Autowired
    private GlAccountRepository glAccountRepository;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restGlAccountMockMvc;

    private GlAccount glAccount;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static GlAccount createEntity(EntityManager em) {
        GlAccount glAccount = new GlAccount()
            .accountNo(DEFAULT_ACCOUNT_NO)
            .accountName(DEFAULT_ACCOUNT_NAME)
            .date(DEFAULT_DATE)
            .closingBalance(DEFAULT_CLOSING_BALANCE)
            .openingBalance(DEFAULT_OPENING_BALANCE)
            .totalDebit(DEFAULT_TOTAL_DEBIT)
            .totalCredit(DEFAULT_TOTAL_CREDIT)
            .availableBalance(DEFAULT_AVAILABLE_BALANCE)
            .currentBalance(DEFAULT_CURRENT_BALANCE)
            .createdOn(DEFAULT_CREATED_ON)
            .createdBy(DEFAULT_CREATED_BY)
            .glAccountStatus(DEFAULT_GL_ACCOUNT_STATUS)
            .glAccountType(DEFAULT_GL_ACCOUNT_TYPE);
        return glAccount;
    }

    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static GlAccount createUpdatedEntity(EntityManager em) {
        GlAccount glAccount = new GlAccount()
            .accountNo(UPDATED_ACCOUNT_NO)
            .accountName(UPDATED_ACCOUNT_NAME)
            .date(UPDATED_DATE)
            .closingBalance(UPDATED_CLOSING_BALANCE)
            .openingBalance(UPDATED_OPENING_BALANCE)
            .totalDebit(UPDATED_TOTAL_DEBIT)
            .totalCredit(UPDATED_TOTAL_CREDIT)
            .availableBalance(UPDATED_AVAILABLE_BALANCE)
            .currentBalance(UPDATED_CURRENT_BALANCE)
            .createdOn(UPDATED_CREATED_ON)
            .createdBy(UPDATED_CREATED_BY)
            .glAccountStatus(UPDATED_GL_ACCOUNT_STATUS)
            .glAccountType(UPDATED_GL_ACCOUNT_TYPE);
        return glAccount;
    }

    @BeforeEach
    public void initTest() {
        glAccount = createEntity(em);
    }

    @Test
    @Transactional
    void createGlAccount() throws Exception {
        int databaseSizeBeforeCreate = glAccountRepository.findAll().size();
        // Create the GlAccount
        restGlAccountMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(glAccount)))
            .andExpect(status().isCreated());

        // Validate the GlAccount in the database
        List<GlAccount> glAccountList = glAccountRepository.findAll();
        assertThat(glAccountList).hasSize(databaseSizeBeforeCreate + 1);
        GlAccount testGlAccount = glAccountList.get(glAccountList.size() - 1);
        assertThat(testGlAccount.getAccountNo()).isEqualTo(DEFAULT_ACCOUNT_NO);
        assertThat(testGlAccount.getAccountName()).isEqualTo(DEFAULT_ACCOUNT_NAME);
        assertThat(testGlAccount.getDate()).isEqualTo(DEFAULT_DATE);
        assertThat(testGlAccount.getClosingBalance()).isEqualByComparingTo(DEFAULT_CLOSING_BALANCE);
        assertThat(testGlAccount.getOpeningBalance()).isEqualByComparingTo(DEFAULT_OPENING_BALANCE);
        assertThat(testGlAccount.getTotalDebit()).isEqualByComparingTo(DEFAULT_TOTAL_DEBIT);
        assertThat(testGlAccount.getTotalCredit()).isEqualByComparingTo(DEFAULT_TOTAL_CREDIT);
        assertThat(testGlAccount.getAvailableBalance()).isEqualByComparingTo(DEFAULT_AVAILABLE_BALANCE);
        assertThat(testGlAccount.getCurrentBalance()).isEqualByComparingTo(DEFAULT_CURRENT_BALANCE);
        assertThat(testGlAccount.getCreatedOn()).isEqualTo(DEFAULT_CREATED_ON);
        assertThat(testGlAccount.getCreatedBy()).isEqualTo(DEFAULT_CREATED_BY);
        assertThat(testGlAccount.getGlAccountStatus()).isEqualTo(DEFAULT_GL_ACCOUNT_STATUS);
        assertThat(testGlAccount.getGlAccountType()).isEqualTo(DEFAULT_GL_ACCOUNT_TYPE);
    }

    @Test
    @Transactional
    void createGlAccountWithExistingId() throws Exception {
        // Create the GlAccount with an existing ID
        glAccount.setId(1L);

        int databaseSizeBeforeCreate = glAccountRepository.findAll().size();

        // An entity with an existing ID cannot be created, so this API call must fail
        restGlAccountMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(glAccount)))
            .andExpect(status().isBadRequest());

        // Validate the GlAccount in the database
        List<GlAccount> glAccountList = glAccountRepository.findAll();
        assertThat(glAccountList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    void checkAccountNoIsRequired() throws Exception {
        int databaseSizeBeforeTest = glAccountRepository.findAll().size();
        // set the field null
        glAccount.setAccountNo(null);

        // Create the GlAccount, which fails.

        restGlAccountMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(glAccount)))
            .andExpect(status().isBadRequest());

        List<GlAccount> glAccountList = glAccountRepository.findAll();
        assertThat(glAccountList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    void checkAccountNameIsRequired() throws Exception {
        int databaseSizeBeforeTest = glAccountRepository.findAll().size();
        // set the field null
        glAccount.setAccountName(null);

        // Create the GlAccount, which fails.

        restGlAccountMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(glAccount)))
            .andExpect(status().isBadRequest());

        List<GlAccount> glAccountList = glAccountRepository.findAll();
        assertThat(glAccountList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    void getAllGlAccounts() throws Exception {
        // Initialize the database
        glAccountRepository.saveAndFlush(glAccount);

        // Get all the glAccountList
        restGlAccountMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(glAccount.getId().intValue())))
            .andExpect(jsonPath("$.[*].accountNo").value(hasItem(DEFAULT_ACCOUNT_NO)))
            .andExpect(jsonPath("$.[*].accountName").value(hasItem(DEFAULT_ACCOUNT_NAME)))
            .andExpect(jsonPath("$.[*].date").value(hasItem(DEFAULT_DATE.toString())))
            .andExpect(jsonPath("$.[*].closingBalance").value(hasItem(sameNumber(DEFAULT_CLOSING_BALANCE))))
            .andExpect(jsonPath("$.[*].openingBalance").value(hasItem(sameNumber(DEFAULT_OPENING_BALANCE))))
            .andExpect(jsonPath("$.[*].totalDebit").value(hasItem(sameNumber(DEFAULT_TOTAL_DEBIT))))
            .andExpect(jsonPath("$.[*].totalCredit").value(hasItem(sameNumber(DEFAULT_TOTAL_CREDIT))))
            .andExpect(jsonPath("$.[*].availableBalance").value(hasItem(sameNumber(DEFAULT_AVAILABLE_BALANCE))))
            .andExpect(jsonPath("$.[*].currentBalance").value(hasItem(sameNumber(DEFAULT_CURRENT_BALANCE))))
            .andExpect(jsonPath("$.[*].createdOn").value(hasItem(DEFAULT_CREATED_ON.toString())))
            .andExpect(jsonPath("$.[*].createdBy").value(hasItem(DEFAULT_CREATED_BY)))
            .andExpect(jsonPath("$.[*].glAccountStatus").value(hasItem(DEFAULT_GL_ACCOUNT_STATUS.toString())))
            .andExpect(jsonPath("$.[*].glAccountType").value(hasItem(DEFAULT_GL_ACCOUNT_TYPE.toString())));
    }

    @Test
    @Transactional
    void getGlAccount() throws Exception {
        // Initialize the database
        glAccountRepository.saveAndFlush(glAccount);

        // Get the glAccount
        restGlAccountMockMvc
            .perform(get(ENTITY_API_URL_ID, glAccount.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(glAccount.getId().intValue()))
            .andExpect(jsonPath("$.accountNo").value(DEFAULT_ACCOUNT_NO))
            .andExpect(jsonPath("$.accountName").value(DEFAULT_ACCOUNT_NAME))
            .andExpect(jsonPath("$.date").value(DEFAULT_DATE.toString()))
            .andExpect(jsonPath("$.closingBalance").value(sameNumber(DEFAULT_CLOSING_BALANCE)))
            .andExpect(jsonPath("$.openingBalance").value(sameNumber(DEFAULT_OPENING_BALANCE)))
            .andExpect(jsonPath("$.totalDebit").value(sameNumber(DEFAULT_TOTAL_DEBIT)))
            .andExpect(jsonPath("$.totalCredit").value(sameNumber(DEFAULT_TOTAL_CREDIT)))
            .andExpect(jsonPath("$.availableBalance").value(sameNumber(DEFAULT_AVAILABLE_BALANCE)))
            .andExpect(jsonPath("$.currentBalance").value(sameNumber(DEFAULT_CURRENT_BALANCE)))
            .andExpect(jsonPath("$.createdOn").value(DEFAULT_CREATED_ON.toString()))
            .andExpect(jsonPath("$.createdBy").value(DEFAULT_CREATED_BY))
            .andExpect(jsonPath("$.glAccountStatus").value(DEFAULT_GL_ACCOUNT_STATUS.toString()))
            .andExpect(jsonPath("$.glAccountType").value(DEFAULT_GL_ACCOUNT_TYPE.toString()));
    }

    @Test
    @Transactional
    void getNonExistingGlAccount() throws Exception {
        // Get the glAccount
        restGlAccountMockMvc.perform(get(ENTITY_API_URL_ID, Long.MAX_VALUE)).andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    void putNewGlAccount() throws Exception {
        // Initialize the database
        glAccountRepository.saveAndFlush(glAccount);

        int databaseSizeBeforeUpdate = glAccountRepository.findAll().size();

        // Update the glAccount
        GlAccount updatedGlAccount = glAccountRepository.findById(glAccount.getId()).get();
        // Disconnect from session so that the updates on updatedGlAccount are not directly saved in db
        em.detach(updatedGlAccount);
        updatedGlAccount
            .accountNo(UPDATED_ACCOUNT_NO)
            .accountName(UPDATED_ACCOUNT_NAME)
            .date(UPDATED_DATE)
            .closingBalance(UPDATED_CLOSING_BALANCE)
            .openingBalance(UPDATED_OPENING_BALANCE)
            .totalDebit(UPDATED_TOTAL_DEBIT)
            .totalCredit(UPDATED_TOTAL_CREDIT)
            .availableBalance(UPDATED_AVAILABLE_BALANCE)
            .currentBalance(UPDATED_CURRENT_BALANCE)
            .createdOn(UPDATED_CREATED_ON)
            .createdBy(UPDATED_CREATED_BY)
            .glAccountStatus(UPDATED_GL_ACCOUNT_STATUS)
            .glAccountType(UPDATED_GL_ACCOUNT_TYPE);

        restGlAccountMockMvc
            .perform(
                put(ENTITY_API_URL_ID, updatedGlAccount.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(updatedGlAccount))
            )
            .andExpect(status().isOk());

        // Validate the GlAccount in the database
        List<GlAccount> glAccountList = glAccountRepository.findAll();
        assertThat(glAccountList).hasSize(databaseSizeBeforeUpdate);
        GlAccount testGlAccount = glAccountList.get(glAccountList.size() - 1);
        assertThat(testGlAccount.getAccountNo()).isEqualTo(UPDATED_ACCOUNT_NO);
        assertThat(testGlAccount.getAccountName()).isEqualTo(UPDATED_ACCOUNT_NAME);
        assertThat(testGlAccount.getDate()).isEqualTo(UPDATED_DATE);
        assertThat(testGlAccount.getClosingBalance()).isEqualTo(UPDATED_CLOSING_BALANCE);
        assertThat(testGlAccount.getOpeningBalance()).isEqualTo(UPDATED_OPENING_BALANCE);
        assertThat(testGlAccount.getTotalDebit()).isEqualTo(UPDATED_TOTAL_DEBIT);
        assertThat(testGlAccount.getTotalCredit()).isEqualTo(UPDATED_TOTAL_CREDIT);
        assertThat(testGlAccount.getAvailableBalance()).isEqualTo(UPDATED_AVAILABLE_BALANCE);
        assertThat(testGlAccount.getCurrentBalance()).isEqualTo(UPDATED_CURRENT_BALANCE);
        assertThat(testGlAccount.getCreatedOn()).isEqualTo(UPDATED_CREATED_ON);
        assertThat(testGlAccount.getCreatedBy()).isEqualTo(UPDATED_CREATED_BY);
        assertThat(testGlAccount.getGlAccountStatus()).isEqualTo(UPDATED_GL_ACCOUNT_STATUS);
        assertThat(testGlAccount.getGlAccountType()).isEqualTo(UPDATED_GL_ACCOUNT_TYPE);
    }

    @Test
    @Transactional
    void putNonExistingGlAccount() throws Exception {
        int databaseSizeBeforeUpdate = glAccountRepository.findAll().size();
        glAccount.setId(count.incrementAndGet());

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restGlAccountMockMvc
            .perform(
                put(ENTITY_API_URL_ID, glAccount.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(glAccount))
            )
            .andExpect(status().isBadRequest());

        // Validate the GlAccount in the database
        List<GlAccount> glAccountList = glAccountRepository.findAll();
        assertThat(glAccountList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithIdMismatchGlAccount() throws Exception {
        int databaseSizeBeforeUpdate = glAccountRepository.findAll().size();
        glAccount.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restGlAccountMockMvc
            .perform(
                put(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(glAccount))
            )
            .andExpect(status().isBadRequest());

        // Validate the GlAccount in the database
        List<GlAccount> glAccountList = glAccountRepository.findAll();
        assertThat(glAccountList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithMissingIdPathParamGlAccount() throws Exception {
        int databaseSizeBeforeUpdate = glAccountRepository.findAll().size();
        glAccount.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restGlAccountMockMvc
            .perform(put(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(glAccount)))
            .andExpect(status().isMethodNotAllowed());

        // Validate the GlAccount in the database
        List<GlAccount> glAccountList = glAccountRepository.findAll();
        assertThat(glAccountList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void partialUpdateGlAccountWithPatch() throws Exception {
        // Initialize the database
        glAccountRepository.saveAndFlush(glAccount);

        int databaseSizeBeforeUpdate = glAccountRepository.findAll().size();

        // Update the glAccount using partial update
        GlAccount partialUpdatedGlAccount = new GlAccount();
        partialUpdatedGlAccount.setId(glAccount.getId());

        partialUpdatedGlAccount
            .date(UPDATED_DATE)
            .totalCredit(UPDATED_TOTAL_CREDIT)
            .availableBalance(UPDATED_AVAILABLE_BALANCE)
            .createdOn(UPDATED_CREATED_ON)
            .glAccountType(UPDATED_GL_ACCOUNT_TYPE);

        restGlAccountMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedGlAccount.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedGlAccount))
            )
            .andExpect(status().isOk());

        // Validate the GlAccount in the database
        List<GlAccount> glAccountList = glAccountRepository.findAll();
        assertThat(glAccountList).hasSize(databaseSizeBeforeUpdate);
        GlAccount testGlAccount = glAccountList.get(glAccountList.size() - 1);
        assertThat(testGlAccount.getAccountNo()).isEqualTo(DEFAULT_ACCOUNT_NO);
        assertThat(testGlAccount.getAccountName()).isEqualTo(DEFAULT_ACCOUNT_NAME);
        assertThat(testGlAccount.getDate()).isEqualTo(UPDATED_DATE);
        assertThat(testGlAccount.getClosingBalance()).isEqualByComparingTo(DEFAULT_CLOSING_BALANCE);
        assertThat(testGlAccount.getOpeningBalance()).isEqualByComparingTo(DEFAULT_OPENING_BALANCE);
        assertThat(testGlAccount.getTotalDebit()).isEqualByComparingTo(DEFAULT_TOTAL_DEBIT);
        assertThat(testGlAccount.getTotalCredit()).isEqualByComparingTo(UPDATED_TOTAL_CREDIT);
        assertThat(testGlAccount.getAvailableBalance()).isEqualByComparingTo(UPDATED_AVAILABLE_BALANCE);
        assertThat(testGlAccount.getCurrentBalance()).isEqualByComparingTo(DEFAULT_CURRENT_BALANCE);
        assertThat(testGlAccount.getCreatedOn()).isEqualTo(UPDATED_CREATED_ON);
        assertThat(testGlAccount.getCreatedBy()).isEqualTo(DEFAULT_CREATED_BY);
        assertThat(testGlAccount.getGlAccountStatus()).isEqualTo(DEFAULT_GL_ACCOUNT_STATUS);
        assertThat(testGlAccount.getGlAccountType()).isEqualTo(UPDATED_GL_ACCOUNT_TYPE);
    }

    @Test
    @Transactional
    void fullUpdateGlAccountWithPatch() throws Exception {
        // Initialize the database
        glAccountRepository.saveAndFlush(glAccount);

        int databaseSizeBeforeUpdate = glAccountRepository.findAll().size();

        // Update the glAccount using partial update
        GlAccount partialUpdatedGlAccount = new GlAccount();
        partialUpdatedGlAccount.setId(glAccount.getId());

        partialUpdatedGlAccount
            .accountNo(UPDATED_ACCOUNT_NO)
            .accountName(UPDATED_ACCOUNT_NAME)
            .date(UPDATED_DATE)
            .closingBalance(UPDATED_CLOSING_BALANCE)
            .openingBalance(UPDATED_OPENING_BALANCE)
            .totalDebit(UPDATED_TOTAL_DEBIT)
            .totalCredit(UPDATED_TOTAL_CREDIT)
            .availableBalance(UPDATED_AVAILABLE_BALANCE)
            .currentBalance(UPDATED_CURRENT_BALANCE)
            .createdOn(UPDATED_CREATED_ON)
            .createdBy(UPDATED_CREATED_BY)
            .glAccountStatus(UPDATED_GL_ACCOUNT_STATUS)
            .glAccountType(UPDATED_GL_ACCOUNT_TYPE);

        restGlAccountMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedGlAccount.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedGlAccount))
            )
            .andExpect(status().isOk());

        // Validate the GlAccount in the database
        List<GlAccount> glAccountList = glAccountRepository.findAll();
        assertThat(glAccountList).hasSize(databaseSizeBeforeUpdate);
        GlAccount testGlAccount = glAccountList.get(glAccountList.size() - 1);
        assertThat(testGlAccount.getAccountNo()).isEqualTo(UPDATED_ACCOUNT_NO);
        assertThat(testGlAccount.getAccountName()).isEqualTo(UPDATED_ACCOUNT_NAME);
        assertThat(testGlAccount.getDate()).isEqualTo(UPDATED_DATE);
        assertThat(testGlAccount.getClosingBalance()).isEqualByComparingTo(UPDATED_CLOSING_BALANCE);
        assertThat(testGlAccount.getOpeningBalance()).isEqualByComparingTo(UPDATED_OPENING_BALANCE);
        assertThat(testGlAccount.getTotalDebit()).isEqualByComparingTo(UPDATED_TOTAL_DEBIT);
        assertThat(testGlAccount.getTotalCredit()).isEqualByComparingTo(UPDATED_TOTAL_CREDIT);
        assertThat(testGlAccount.getAvailableBalance()).isEqualByComparingTo(UPDATED_AVAILABLE_BALANCE);
        assertThat(testGlAccount.getCurrentBalance()).isEqualByComparingTo(UPDATED_CURRENT_BALANCE);
        assertThat(testGlAccount.getCreatedOn()).isEqualTo(UPDATED_CREATED_ON);
        assertThat(testGlAccount.getCreatedBy()).isEqualTo(UPDATED_CREATED_BY);
        assertThat(testGlAccount.getGlAccountStatus()).isEqualTo(UPDATED_GL_ACCOUNT_STATUS);
        assertThat(testGlAccount.getGlAccountType()).isEqualTo(UPDATED_GL_ACCOUNT_TYPE);
    }

    @Test
    @Transactional
    void patchNonExistingGlAccount() throws Exception {
        int databaseSizeBeforeUpdate = glAccountRepository.findAll().size();
        glAccount.setId(count.incrementAndGet());

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restGlAccountMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, glAccount.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(glAccount))
            )
            .andExpect(status().isBadRequest());

        // Validate the GlAccount in the database
        List<GlAccount> glAccountList = glAccountRepository.findAll();
        assertThat(glAccountList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithIdMismatchGlAccount() throws Exception {
        int databaseSizeBeforeUpdate = glAccountRepository.findAll().size();
        glAccount.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restGlAccountMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(glAccount))
            )
            .andExpect(status().isBadRequest());

        // Validate the GlAccount in the database
        List<GlAccount> glAccountList = glAccountRepository.findAll();
        assertThat(glAccountList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithMissingIdPathParamGlAccount() throws Exception {
        int databaseSizeBeforeUpdate = glAccountRepository.findAll().size();
        glAccount.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restGlAccountMockMvc
            .perform(
                patch(ENTITY_API_URL).contentType("application/merge-patch+json").content(TestUtil.convertObjectToJsonBytes(glAccount))
            )
            .andExpect(status().isMethodNotAllowed());

        // Validate the GlAccount in the database
        List<GlAccount> glAccountList = glAccountRepository.findAll();
        assertThat(glAccountList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void deleteGlAccount() throws Exception {
        // Initialize the database
        glAccountRepository.saveAndFlush(glAccount);

        int databaseSizeBeforeDelete = glAccountRepository.findAll().size();

        // Delete the glAccount
        restGlAccountMockMvc
            .perform(delete(ENTITY_API_URL_ID, glAccount.getId()).accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<GlAccount> glAccountList = glAccountRepository.findAll();
        assertThat(glAccountList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
