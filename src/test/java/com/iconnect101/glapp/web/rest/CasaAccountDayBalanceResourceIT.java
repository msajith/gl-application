package com.iconnect101.glapp.web.rest;

import static com.iconnect101.glapp.web.rest.TestUtil.sameNumber;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import com.iconnect101.glapp.IntegrationTest;
import com.iconnect101.glapp.domain.CasaAccountDayBalance;
import com.iconnect101.glapp.repository.CasaAccountDayBalanceRepository;
import java.math.BigDecimal;
import java.time.Instant;
import java.time.LocalDate;
import java.time.ZoneId;
import java.time.temporal.ChronoUnit;
import java.util.List;
import java.util.Random;
import java.util.concurrent.atomic.AtomicLong;
import javax.persistence.EntityManager;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;

/**
 * Integration tests for the {@link CasaAccountDayBalanceResource} REST controller.
 */
@IntegrationTest
@AutoConfigureMockMvc
@WithMockUser
class CasaAccountDayBalanceResourceIT {

    private static final String DEFAULT_ACCOUNT_NO = "AAAAAAAAAA";
    private static final String UPDATED_ACCOUNT_NO = "BBBBBBBBBB";

    private static final LocalDate DEFAULT_DATE = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_DATE = LocalDate.now(ZoneId.systemDefault());

    private static final BigDecimal DEFAULT_CLOSING_BALANCE = new BigDecimal(1);
    private static final BigDecimal UPDATED_CLOSING_BALANCE = new BigDecimal(2);

    private static final BigDecimal DEFAULT_OPENING_BALANCE = new BigDecimal(1);
    private static final BigDecimal UPDATED_OPENING_BALANCE = new BigDecimal(2);

    private static final BigDecimal DEFAULT_TOTAL_DEBIT = new BigDecimal(1);
    private static final BigDecimal UPDATED_TOTAL_DEBIT = new BigDecimal(2);

    private static final BigDecimal DEFAULT_TOTAL_CREDIT = new BigDecimal(1);
    private static final BigDecimal UPDATED_TOTAL_CREDIT = new BigDecimal(2);

    private static final BigDecimal DEFAULT_AVAILABLE_BALANCE = new BigDecimal(1);
    private static final BigDecimal UPDATED_AVAILABLE_BALANCE = new BigDecimal(2);

    private static final BigDecimal DEFAULT_CURRENT_BALANCE = new BigDecimal(1);
    private static final BigDecimal UPDATED_CURRENT_BALANCE = new BigDecimal(2);

    private static final Instant DEFAULT_CREATED_ON = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_CREATED_ON = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    private static final String DEFAULT_CREATED_BY = "AAAAAAAAAA";
    private static final String UPDATED_CREATED_BY = "BBBBBBBBBB";

    private static final String ENTITY_API_URL = "/api/casa-account-day-balances";
    private static final String ENTITY_API_URL_ID = ENTITY_API_URL + "/{id}";

    private static Random random = new Random();
    private static AtomicLong count = new AtomicLong(random.nextInt() + (2 * Integer.MAX_VALUE));

    @Autowired
    private CasaAccountDayBalanceRepository casaAccountDayBalanceRepository;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restCasaAccountDayBalanceMockMvc;

    private CasaAccountDayBalance casaAccountDayBalance;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static CasaAccountDayBalance createEntity(EntityManager em) {
        CasaAccountDayBalance casaAccountDayBalance = new CasaAccountDayBalance()
            .accountNo(DEFAULT_ACCOUNT_NO)
            .date(DEFAULT_DATE)
            .closingBalance(DEFAULT_CLOSING_BALANCE)
            .openingBalance(DEFAULT_OPENING_BALANCE)
            .totalDebit(DEFAULT_TOTAL_DEBIT)
            .totalCredit(DEFAULT_TOTAL_CREDIT)
            .availableBalance(DEFAULT_AVAILABLE_BALANCE)
            .currentBalance(DEFAULT_CURRENT_BALANCE)
            .createdOn(DEFAULT_CREATED_ON)
            .createdBy(DEFAULT_CREATED_BY);
        return casaAccountDayBalance;
    }

    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static CasaAccountDayBalance createUpdatedEntity(EntityManager em) {
        CasaAccountDayBalance casaAccountDayBalance = new CasaAccountDayBalance()
            .accountNo(UPDATED_ACCOUNT_NO)
            .date(UPDATED_DATE)
            .closingBalance(UPDATED_CLOSING_BALANCE)
            .openingBalance(UPDATED_OPENING_BALANCE)
            .totalDebit(UPDATED_TOTAL_DEBIT)
            .totalCredit(UPDATED_TOTAL_CREDIT)
            .availableBalance(UPDATED_AVAILABLE_BALANCE)
            .currentBalance(UPDATED_CURRENT_BALANCE)
            .createdOn(UPDATED_CREATED_ON)
            .createdBy(UPDATED_CREATED_BY);
        return casaAccountDayBalance;
    }

    @BeforeEach
    public void initTest() {
        casaAccountDayBalance = createEntity(em);
    }

    @Test
    @Transactional
    void createCasaAccountDayBalance() throws Exception {
        int databaseSizeBeforeCreate = casaAccountDayBalanceRepository.findAll().size();
        // Create the CasaAccountDayBalance
        restCasaAccountDayBalanceMockMvc
            .perform(
                post(ENTITY_API_URL)
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(casaAccountDayBalance))
            )
            .andExpect(status().isCreated());

        // Validate the CasaAccountDayBalance in the database
        List<CasaAccountDayBalance> casaAccountDayBalanceList = casaAccountDayBalanceRepository.findAll();
        assertThat(casaAccountDayBalanceList).hasSize(databaseSizeBeforeCreate + 1);
        CasaAccountDayBalance testCasaAccountDayBalance = casaAccountDayBalanceList.get(casaAccountDayBalanceList.size() - 1);
        assertThat(testCasaAccountDayBalance.getAccountNo()).isEqualTo(DEFAULT_ACCOUNT_NO);
        assertThat(testCasaAccountDayBalance.getDate()).isEqualTo(DEFAULT_DATE);
        assertThat(testCasaAccountDayBalance.getClosingBalance()).isEqualByComparingTo(DEFAULT_CLOSING_BALANCE);
        assertThat(testCasaAccountDayBalance.getOpeningBalance()).isEqualByComparingTo(DEFAULT_OPENING_BALANCE);
        assertThat(testCasaAccountDayBalance.getTotalDebit()).isEqualByComparingTo(DEFAULT_TOTAL_DEBIT);
        assertThat(testCasaAccountDayBalance.getTotalCredit()).isEqualByComparingTo(DEFAULT_TOTAL_CREDIT);
        assertThat(testCasaAccountDayBalance.getAvailableBalance()).isEqualByComparingTo(DEFAULT_AVAILABLE_BALANCE);
        assertThat(testCasaAccountDayBalance.getCurrentBalance()).isEqualByComparingTo(DEFAULT_CURRENT_BALANCE);
        assertThat(testCasaAccountDayBalance.getCreatedOn()).isEqualTo(DEFAULT_CREATED_ON);
        assertThat(testCasaAccountDayBalance.getCreatedBy()).isEqualTo(DEFAULT_CREATED_BY);
    }

    @Test
    @Transactional
    void createCasaAccountDayBalanceWithExistingId() throws Exception {
        // Create the CasaAccountDayBalance with an existing ID
        casaAccountDayBalance.setId(1L);

        int databaseSizeBeforeCreate = casaAccountDayBalanceRepository.findAll().size();

        // An entity with an existing ID cannot be created, so this API call must fail
        restCasaAccountDayBalanceMockMvc
            .perform(
                post(ENTITY_API_URL)
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(casaAccountDayBalance))
            )
            .andExpect(status().isBadRequest());

        // Validate the CasaAccountDayBalance in the database
        List<CasaAccountDayBalance> casaAccountDayBalanceList = casaAccountDayBalanceRepository.findAll();
        assertThat(casaAccountDayBalanceList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    void checkAccountNoIsRequired() throws Exception {
        int databaseSizeBeforeTest = casaAccountDayBalanceRepository.findAll().size();
        // set the field null
        casaAccountDayBalance.setAccountNo(null);

        // Create the CasaAccountDayBalance, which fails.

        restCasaAccountDayBalanceMockMvc
            .perform(
                post(ENTITY_API_URL)
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(casaAccountDayBalance))
            )
            .andExpect(status().isBadRequest());

        List<CasaAccountDayBalance> casaAccountDayBalanceList = casaAccountDayBalanceRepository.findAll();
        assertThat(casaAccountDayBalanceList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    void getAllCasaAccountDayBalances() throws Exception {
        // Initialize the database
        casaAccountDayBalanceRepository.saveAndFlush(casaAccountDayBalance);

        // Get all the casaAccountDayBalanceList
        restCasaAccountDayBalanceMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(casaAccountDayBalance.getId().intValue())))
            .andExpect(jsonPath("$.[*].accountNo").value(hasItem(DEFAULT_ACCOUNT_NO)))
            .andExpect(jsonPath("$.[*].date").value(hasItem(DEFAULT_DATE.toString())))
            .andExpect(jsonPath("$.[*].closingBalance").value(hasItem(sameNumber(DEFAULT_CLOSING_BALANCE))))
            .andExpect(jsonPath("$.[*].openingBalance").value(hasItem(sameNumber(DEFAULT_OPENING_BALANCE))))
            .andExpect(jsonPath("$.[*].totalDebit").value(hasItem(sameNumber(DEFAULT_TOTAL_DEBIT))))
            .andExpect(jsonPath("$.[*].totalCredit").value(hasItem(sameNumber(DEFAULT_TOTAL_CREDIT))))
            .andExpect(jsonPath("$.[*].availableBalance").value(hasItem(sameNumber(DEFAULT_AVAILABLE_BALANCE))))
            .andExpect(jsonPath("$.[*].currentBalance").value(hasItem(sameNumber(DEFAULT_CURRENT_BALANCE))))
            .andExpect(jsonPath("$.[*].createdOn").value(hasItem(DEFAULT_CREATED_ON.toString())))
            .andExpect(jsonPath("$.[*].createdBy").value(hasItem(DEFAULT_CREATED_BY)));
    }

    @Test
    @Transactional
    void getCasaAccountDayBalance() throws Exception {
        // Initialize the database
        casaAccountDayBalanceRepository.saveAndFlush(casaAccountDayBalance);

        // Get the casaAccountDayBalance
        restCasaAccountDayBalanceMockMvc
            .perform(get(ENTITY_API_URL_ID, casaAccountDayBalance.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(casaAccountDayBalance.getId().intValue()))
            .andExpect(jsonPath("$.accountNo").value(DEFAULT_ACCOUNT_NO))
            .andExpect(jsonPath("$.date").value(DEFAULT_DATE.toString()))
            .andExpect(jsonPath("$.closingBalance").value(sameNumber(DEFAULT_CLOSING_BALANCE)))
            .andExpect(jsonPath("$.openingBalance").value(sameNumber(DEFAULT_OPENING_BALANCE)))
            .andExpect(jsonPath("$.totalDebit").value(sameNumber(DEFAULT_TOTAL_DEBIT)))
            .andExpect(jsonPath("$.totalCredit").value(sameNumber(DEFAULT_TOTAL_CREDIT)))
            .andExpect(jsonPath("$.availableBalance").value(sameNumber(DEFAULT_AVAILABLE_BALANCE)))
            .andExpect(jsonPath("$.currentBalance").value(sameNumber(DEFAULT_CURRENT_BALANCE)))
            .andExpect(jsonPath("$.createdOn").value(DEFAULT_CREATED_ON.toString()))
            .andExpect(jsonPath("$.createdBy").value(DEFAULT_CREATED_BY));
    }

    @Test
    @Transactional
    void getNonExistingCasaAccountDayBalance() throws Exception {
        // Get the casaAccountDayBalance
        restCasaAccountDayBalanceMockMvc.perform(get(ENTITY_API_URL_ID, Long.MAX_VALUE)).andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    void putNewCasaAccountDayBalance() throws Exception {
        // Initialize the database
        casaAccountDayBalanceRepository.saveAndFlush(casaAccountDayBalance);

        int databaseSizeBeforeUpdate = casaAccountDayBalanceRepository.findAll().size();

        // Update the casaAccountDayBalance
        CasaAccountDayBalance updatedCasaAccountDayBalance = casaAccountDayBalanceRepository.findById(casaAccountDayBalance.getId()).get();
        // Disconnect from session so that the updates on updatedCasaAccountDayBalance are not directly saved in db
        em.detach(updatedCasaAccountDayBalance);
        updatedCasaAccountDayBalance
            .accountNo(UPDATED_ACCOUNT_NO)
            .date(UPDATED_DATE)
            .closingBalance(UPDATED_CLOSING_BALANCE)
            .openingBalance(UPDATED_OPENING_BALANCE)
            .totalDebit(UPDATED_TOTAL_DEBIT)
            .totalCredit(UPDATED_TOTAL_CREDIT)
            .availableBalance(UPDATED_AVAILABLE_BALANCE)
            .currentBalance(UPDATED_CURRENT_BALANCE)
            .createdOn(UPDATED_CREATED_ON)
            .createdBy(UPDATED_CREATED_BY);

        restCasaAccountDayBalanceMockMvc
            .perform(
                put(ENTITY_API_URL_ID, updatedCasaAccountDayBalance.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(updatedCasaAccountDayBalance))
            )
            .andExpect(status().isOk());

        // Validate the CasaAccountDayBalance in the database
        List<CasaAccountDayBalance> casaAccountDayBalanceList = casaAccountDayBalanceRepository.findAll();
        assertThat(casaAccountDayBalanceList).hasSize(databaseSizeBeforeUpdate);
        CasaAccountDayBalance testCasaAccountDayBalance = casaAccountDayBalanceList.get(casaAccountDayBalanceList.size() - 1);
        assertThat(testCasaAccountDayBalance.getAccountNo()).isEqualTo(UPDATED_ACCOUNT_NO);
        assertThat(testCasaAccountDayBalance.getDate()).isEqualTo(UPDATED_DATE);
        assertThat(testCasaAccountDayBalance.getClosingBalance()).isEqualTo(UPDATED_CLOSING_BALANCE);
        assertThat(testCasaAccountDayBalance.getOpeningBalance()).isEqualTo(UPDATED_OPENING_BALANCE);
        assertThat(testCasaAccountDayBalance.getTotalDebit()).isEqualTo(UPDATED_TOTAL_DEBIT);
        assertThat(testCasaAccountDayBalance.getTotalCredit()).isEqualTo(UPDATED_TOTAL_CREDIT);
        assertThat(testCasaAccountDayBalance.getAvailableBalance()).isEqualTo(UPDATED_AVAILABLE_BALANCE);
        assertThat(testCasaAccountDayBalance.getCurrentBalance()).isEqualTo(UPDATED_CURRENT_BALANCE);
        assertThat(testCasaAccountDayBalance.getCreatedOn()).isEqualTo(UPDATED_CREATED_ON);
        assertThat(testCasaAccountDayBalance.getCreatedBy()).isEqualTo(UPDATED_CREATED_BY);
    }

    @Test
    @Transactional
    void putNonExistingCasaAccountDayBalance() throws Exception {
        int databaseSizeBeforeUpdate = casaAccountDayBalanceRepository.findAll().size();
        casaAccountDayBalance.setId(count.incrementAndGet());

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restCasaAccountDayBalanceMockMvc
            .perform(
                put(ENTITY_API_URL_ID, casaAccountDayBalance.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(casaAccountDayBalance))
            )
            .andExpect(status().isBadRequest());

        // Validate the CasaAccountDayBalance in the database
        List<CasaAccountDayBalance> casaAccountDayBalanceList = casaAccountDayBalanceRepository.findAll();
        assertThat(casaAccountDayBalanceList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithIdMismatchCasaAccountDayBalance() throws Exception {
        int databaseSizeBeforeUpdate = casaAccountDayBalanceRepository.findAll().size();
        casaAccountDayBalance.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restCasaAccountDayBalanceMockMvc
            .perform(
                put(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(casaAccountDayBalance))
            )
            .andExpect(status().isBadRequest());

        // Validate the CasaAccountDayBalance in the database
        List<CasaAccountDayBalance> casaAccountDayBalanceList = casaAccountDayBalanceRepository.findAll();
        assertThat(casaAccountDayBalanceList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithMissingIdPathParamCasaAccountDayBalance() throws Exception {
        int databaseSizeBeforeUpdate = casaAccountDayBalanceRepository.findAll().size();
        casaAccountDayBalance.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restCasaAccountDayBalanceMockMvc
            .perform(
                put(ENTITY_API_URL)
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(casaAccountDayBalance))
            )
            .andExpect(status().isMethodNotAllowed());

        // Validate the CasaAccountDayBalance in the database
        List<CasaAccountDayBalance> casaAccountDayBalanceList = casaAccountDayBalanceRepository.findAll();
        assertThat(casaAccountDayBalanceList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void partialUpdateCasaAccountDayBalanceWithPatch() throws Exception {
        // Initialize the database
        casaAccountDayBalanceRepository.saveAndFlush(casaAccountDayBalance);

        int databaseSizeBeforeUpdate = casaAccountDayBalanceRepository.findAll().size();

        // Update the casaAccountDayBalance using partial update
        CasaAccountDayBalance partialUpdatedCasaAccountDayBalance = new CasaAccountDayBalance();
        partialUpdatedCasaAccountDayBalance.setId(casaAccountDayBalance.getId());

        partialUpdatedCasaAccountDayBalance
            .date(UPDATED_DATE)
            .closingBalance(UPDATED_CLOSING_BALANCE)
            .openingBalance(UPDATED_OPENING_BALANCE)
            .availableBalance(UPDATED_AVAILABLE_BALANCE)
            .currentBalance(UPDATED_CURRENT_BALANCE);

        restCasaAccountDayBalanceMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedCasaAccountDayBalance.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedCasaAccountDayBalance))
            )
            .andExpect(status().isOk());

        // Validate the CasaAccountDayBalance in the database
        List<CasaAccountDayBalance> casaAccountDayBalanceList = casaAccountDayBalanceRepository.findAll();
        assertThat(casaAccountDayBalanceList).hasSize(databaseSizeBeforeUpdate);
        CasaAccountDayBalance testCasaAccountDayBalance = casaAccountDayBalanceList.get(casaAccountDayBalanceList.size() - 1);
        assertThat(testCasaAccountDayBalance.getAccountNo()).isEqualTo(DEFAULT_ACCOUNT_NO);
        assertThat(testCasaAccountDayBalance.getDate()).isEqualTo(UPDATED_DATE);
        assertThat(testCasaAccountDayBalance.getClosingBalance()).isEqualByComparingTo(UPDATED_CLOSING_BALANCE);
        assertThat(testCasaAccountDayBalance.getOpeningBalance()).isEqualByComparingTo(UPDATED_OPENING_BALANCE);
        assertThat(testCasaAccountDayBalance.getTotalDebit()).isEqualByComparingTo(DEFAULT_TOTAL_DEBIT);
        assertThat(testCasaAccountDayBalance.getTotalCredit()).isEqualByComparingTo(DEFAULT_TOTAL_CREDIT);
        assertThat(testCasaAccountDayBalance.getAvailableBalance()).isEqualByComparingTo(UPDATED_AVAILABLE_BALANCE);
        assertThat(testCasaAccountDayBalance.getCurrentBalance()).isEqualByComparingTo(UPDATED_CURRENT_BALANCE);
        assertThat(testCasaAccountDayBalance.getCreatedOn()).isEqualTo(DEFAULT_CREATED_ON);
        assertThat(testCasaAccountDayBalance.getCreatedBy()).isEqualTo(DEFAULT_CREATED_BY);
    }

    @Test
    @Transactional
    void fullUpdateCasaAccountDayBalanceWithPatch() throws Exception {
        // Initialize the database
        casaAccountDayBalanceRepository.saveAndFlush(casaAccountDayBalance);

        int databaseSizeBeforeUpdate = casaAccountDayBalanceRepository.findAll().size();

        // Update the casaAccountDayBalance using partial update
        CasaAccountDayBalance partialUpdatedCasaAccountDayBalance = new CasaAccountDayBalance();
        partialUpdatedCasaAccountDayBalance.setId(casaAccountDayBalance.getId());

        partialUpdatedCasaAccountDayBalance
            .accountNo(UPDATED_ACCOUNT_NO)
            .date(UPDATED_DATE)
            .closingBalance(UPDATED_CLOSING_BALANCE)
            .openingBalance(UPDATED_OPENING_BALANCE)
            .totalDebit(UPDATED_TOTAL_DEBIT)
            .totalCredit(UPDATED_TOTAL_CREDIT)
            .availableBalance(UPDATED_AVAILABLE_BALANCE)
            .currentBalance(UPDATED_CURRENT_BALANCE)
            .createdOn(UPDATED_CREATED_ON)
            .createdBy(UPDATED_CREATED_BY);

        restCasaAccountDayBalanceMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedCasaAccountDayBalance.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedCasaAccountDayBalance))
            )
            .andExpect(status().isOk());

        // Validate the CasaAccountDayBalance in the database
        List<CasaAccountDayBalance> casaAccountDayBalanceList = casaAccountDayBalanceRepository.findAll();
        assertThat(casaAccountDayBalanceList).hasSize(databaseSizeBeforeUpdate);
        CasaAccountDayBalance testCasaAccountDayBalance = casaAccountDayBalanceList.get(casaAccountDayBalanceList.size() - 1);
        assertThat(testCasaAccountDayBalance.getAccountNo()).isEqualTo(UPDATED_ACCOUNT_NO);
        assertThat(testCasaAccountDayBalance.getDate()).isEqualTo(UPDATED_DATE);
        assertThat(testCasaAccountDayBalance.getClosingBalance()).isEqualByComparingTo(UPDATED_CLOSING_BALANCE);
        assertThat(testCasaAccountDayBalance.getOpeningBalance()).isEqualByComparingTo(UPDATED_OPENING_BALANCE);
        assertThat(testCasaAccountDayBalance.getTotalDebit()).isEqualByComparingTo(UPDATED_TOTAL_DEBIT);
        assertThat(testCasaAccountDayBalance.getTotalCredit()).isEqualByComparingTo(UPDATED_TOTAL_CREDIT);
        assertThat(testCasaAccountDayBalance.getAvailableBalance()).isEqualByComparingTo(UPDATED_AVAILABLE_BALANCE);
        assertThat(testCasaAccountDayBalance.getCurrentBalance()).isEqualByComparingTo(UPDATED_CURRENT_BALANCE);
        assertThat(testCasaAccountDayBalance.getCreatedOn()).isEqualTo(UPDATED_CREATED_ON);
        assertThat(testCasaAccountDayBalance.getCreatedBy()).isEqualTo(UPDATED_CREATED_BY);
    }

    @Test
    @Transactional
    void patchNonExistingCasaAccountDayBalance() throws Exception {
        int databaseSizeBeforeUpdate = casaAccountDayBalanceRepository.findAll().size();
        casaAccountDayBalance.setId(count.incrementAndGet());

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restCasaAccountDayBalanceMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, casaAccountDayBalance.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(casaAccountDayBalance))
            )
            .andExpect(status().isBadRequest());

        // Validate the CasaAccountDayBalance in the database
        List<CasaAccountDayBalance> casaAccountDayBalanceList = casaAccountDayBalanceRepository.findAll();
        assertThat(casaAccountDayBalanceList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithIdMismatchCasaAccountDayBalance() throws Exception {
        int databaseSizeBeforeUpdate = casaAccountDayBalanceRepository.findAll().size();
        casaAccountDayBalance.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restCasaAccountDayBalanceMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(casaAccountDayBalance))
            )
            .andExpect(status().isBadRequest());

        // Validate the CasaAccountDayBalance in the database
        List<CasaAccountDayBalance> casaAccountDayBalanceList = casaAccountDayBalanceRepository.findAll();
        assertThat(casaAccountDayBalanceList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithMissingIdPathParamCasaAccountDayBalance() throws Exception {
        int databaseSizeBeforeUpdate = casaAccountDayBalanceRepository.findAll().size();
        casaAccountDayBalance.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restCasaAccountDayBalanceMockMvc
            .perform(
                patch(ENTITY_API_URL)
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(casaAccountDayBalance))
            )
            .andExpect(status().isMethodNotAllowed());

        // Validate the CasaAccountDayBalance in the database
        List<CasaAccountDayBalance> casaAccountDayBalanceList = casaAccountDayBalanceRepository.findAll();
        assertThat(casaAccountDayBalanceList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void deleteCasaAccountDayBalance() throws Exception {
        // Initialize the database
        casaAccountDayBalanceRepository.saveAndFlush(casaAccountDayBalance);

        int databaseSizeBeforeDelete = casaAccountDayBalanceRepository.findAll().size();

        // Delete the casaAccountDayBalance
        restCasaAccountDayBalanceMockMvc
            .perform(delete(ENTITY_API_URL_ID, casaAccountDayBalance.getId()).accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<CasaAccountDayBalance> casaAccountDayBalanceList = casaAccountDayBalanceRepository.findAll();
        assertThat(casaAccountDayBalanceList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
