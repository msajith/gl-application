package com.iconnect101.glapp.web.rest;

import static com.iconnect101.glapp.web.rest.TestUtil.sameNumber;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import com.iconnect101.glapp.IntegrationTest;
import com.iconnect101.glapp.domain.WalletAccount;
import com.iconnect101.glapp.domain.enumeration.AccountStatus;
import com.iconnect101.glapp.repository.WalletAccountRepository;
import java.math.BigDecimal;
import java.time.Instant;
import java.time.LocalDate;
import java.time.ZoneId;
import java.time.temporal.ChronoUnit;
import java.util.List;
import java.util.Random;
import java.util.concurrent.atomic.AtomicLong;
import javax.persistence.EntityManager;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;

/**
 * Integration tests for the {@link WalletAccountResource} REST controller.
 */
@IntegrationTest
@AutoConfigureMockMvc
@WithMockUser
class WalletAccountResourceIT {

    private static final String DEFAULT_ACCOUNT_NO = "AAAAAAAAAA";
    private static final String UPDATED_ACCOUNT_NO = "BBBBBBBBBB";

    private static final String DEFAULT_CIF = "AAAAAAAAAA";
    private static final String UPDATED_CIF = "BBBBBBBBBB";

    private static final String DEFAULT_ACCOUNT_NAME = "AAAAAAAAAA";
    private static final String UPDATED_ACCOUNT_NAME = "BBBBBBBBBB";

    private static final LocalDate DEFAULT_DATE = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_DATE = LocalDate.now(ZoneId.systemDefault());

    private static final BigDecimal DEFAULT_CLOSING_BALANCE = new BigDecimal(1);
    private static final BigDecimal UPDATED_CLOSING_BALANCE = new BigDecimal(2);

    private static final BigDecimal DEFAULT_OPENING_BALANCE = new BigDecimal(1);
    private static final BigDecimal UPDATED_OPENING_BALANCE = new BigDecimal(2);

    private static final BigDecimal DEFAULT_TOTAL_DEBIT = new BigDecimal(1);
    private static final BigDecimal UPDATED_TOTAL_DEBIT = new BigDecimal(2);

    private static final BigDecimal DEFAULT_TOTAL_CREDIT = new BigDecimal(1);
    private static final BigDecimal UPDATED_TOTAL_CREDIT = new BigDecimal(2);

    private static final BigDecimal DEFAULT_AVAILABLE_BALANCE = new BigDecimal(1);
    private static final BigDecimal UPDATED_AVAILABLE_BALANCE = new BigDecimal(2);

    private static final BigDecimal DEFAULT_CURRENT_BALANCE = new BigDecimal(1);
    private static final BigDecimal UPDATED_CURRENT_BALANCE = new BigDecimal(2);

    private static final Instant DEFAULT_CREATED_ON = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_CREATED_ON = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    private static final String DEFAULT_CREATED_BY = "AAAAAAAAAA";
    private static final String UPDATED_CREATED_BY = "BBBBBBBBBB";

    private static final AccountStatus DEFAULT_ACCOUNT_STATUS = AccountStatus.ACTIVE;
    private static final AccountStatus UPDATED_ACCOUNT_STATUS = AccountStatus.INACTIVE;

    private static final String ENTITY_API_URL = "/api/wallet-accounts";
    private static final String ENTITY_API_URL_ID = ENTITY_API_URL + "/{id}";

    private static Random random = new Random();
    private static AtomicLong count = new AtomicLong(random.nextInt() + (2 * Integer.MAX_VALUE));

    @Autowired
    private WalletAccountRepository walletAccountRepository;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restWalletAccountMockMvc;

    private WalletAccount walletAccount;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static WalletAccount createEntity(EntityManager em) {
        WalletAccount walletAccount = new WalletAccount()
            .accountNo(DEFAULT_ACCOUNT_NO)
            .cif(DEFAULT_CIF)
            .accountName(DEFAULT_ACCOUNT_NAME)
            .date(DEFAULT_DATE)
            .closingBalance(DEFAULT_CLOSING_BALANCE)
            .openingBalance(DEFAULT_OPENING_BALANCE)
            .totalDebit(DEFAULT_TOTAL_DEBIT)
            .totalCredit(DEFAULT_TOTAL_CREDIT)
            .availableBalance(DEFAULT_AVAILABLE_BALANCE)
            .currentBalance(DEFAULT_CURRENT_BALANCE)
            .createdOn(DEFAULT_CREATED_ON)
            .createdBy(DEFAULT_CREATED_BY)
            .accountStatus(DEFAULT_ACCOUNT_STATUS);
        return walletAccount;
    }

    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static WalletAccount createUpdatedEntity(EntityManager em) {
        WalletAccount walletAccount = new WalletAccount()
            .accountNo(UPDATED_ACCOUNT_NO)
            .cif(UPDATED_CIF)
            .accountName(UPDATED_ACCOUNT_NAME)
            .date(UPDATED_DATE)
            .closingBalance(UPDATED_CLOSING_BALANCE)
            .openingBalance(UPDATED_OPENING_BALANCE)
            .totalDebit(UPDATED_TOTAL_DEBIT)
            .totalCredit(UPDATED_TOTAL_CREDIT)
            .availableBalance(UPDATED_AVAILABLE_BALANCE)
            .currentBalance(UPDATED_CURRENT_BALANCE)
            .createdOn(UPDATED_CREATED_ON)
            .createdBy(UPDATED_CREATED_BY)
            .accountStatus(UPDATED_ACCOUNT_STATUS);
        return walletAccount;
    }

    @BeforeEach
    public void initTest() {
        walletAccount = createEntity(em);
    }

    @Test
    @Transactional
    void createWalletAccount() throws Exception {
        int databaseSizeBeforeCreate = walletAccountRepository.findAll().size();
        // Create the WalletAccount
        restWalletAccountMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(walletAccount)))
            .andExpect(status().isCreated());

        // Validate the WalletAccount in the database
        List<WalletAccount> walletAccountList = walletAccountRepository.findAll();
        assertThat(walletAccountList).hasSize(databaseSizeBeforeCreate + 1);
        WalletAccount testWalletAccount = walletAccountList.get(walletAccountList.size() - 1);
        assertThat(testWalletAccount.getAccountNo()).isEqualTo(DEFAULT_ACCOUNT_NO);
        assertThat(testWalletAccount.getCif()).isEqualTo(DEFAULT_CIF);
        assertThat(testWalletAccount.getAccountName()).isEqualTo(DEFAULT_ACCOUNT_NAME);
        assertThat(testWalletAccount.getDate()).isEqualTo(DEFAULT_DATE);
        assertThat(testWalletAccount.getClosingBalance()).isEqualByComparingTo(DEFAULT_CLOSING_BALANCE);
        assertThat(testWalletAccount.getOpeningBalance()).isEqualByComparingTo(DEFAULT_OPENING_BALANCE);
        assertThat(testWalletAccount.getTotalDebit()).isEqualByComparingTo(DEFAULT_TOTAL_DEBIT);
        assertThat(testWalletAccount.getTotalCredit()).isEqualByComparingTo(DEFAULT_TOTAL_CREDIT);
        assertThat(testWalletAccount.getAvailableBalance()).isEqualByComparingTo(DEFAULT_AVAILABLE_BALANCE);
        assertThat(testWalletAccount.getCurrentBalance()).isEqualByComparingTo(DEFAULT_CURRENT_BALANCE);
        assertThat(testWalletAccount.getCreatedOn()).isEqualTo(DEFAULT_CREATED_ON);
        assertThat(testWalletAccount.getCreatedBy()).isEqualTo(DEFAULT_CREATED_BY);
        assertThat(testWalletAccount.getAccountStatus()).isEqualTo(DEFAULT_ACCOUNT_STATUS);
    }

    @Test
    @Transactional
    void createWalletAccountWithExistingId() throws Exception {
        // Create the WalletAccount with an existing ID
        walletAccount.setId(1L);

        int databaseSizeBeforeCreate = walletAccountRepository.findAll().size();

        // An entity with an existing ID cannot be created, so this API call must fail
        restWalletAccountMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(walletAccount)))
            .andExpect(status().isBadRequest());

        // Validate the WalletAccount in the database
        List<WalletAccount> walletAccountList = walletAccountRepository.findAll();
        assertThat(walletAccountList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    void checkAccountNoIsRequired() throws Exception {
        int databaseSizeBeforeTest = walletAccountRepository.findAll().size();
        // set the field null
        walletAccount.setAccountNo(null);

        // Create the WalletAccount, which fails.

        restWalletAccountMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(walletAccount)))
            .andExpect(status().isBadRequest());

        List<WalletAccount> walletAccountList = walletAccountRepository.findAll();
        assertThat(walletAccountList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    void checkCifIsRequired() throws Exception {
        int databaseSizeBeforeTest = walletAccountRepository.findAll().size();
        // set the field null
        walletAccount.setCif(null);

        // Create the WalletAccount, which fails.

        restWalletAccountMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(walletAccount)))
            .andExpect(status().isBadRequest());

        List<WalletAccount> walletAccountList = walletAccountRepository.findAll();
        assertThat(walletAccountList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    void checkAccountNameIsRequired() throws Exception {
        int databaseSizeBeforeTest = walletAccountRepository.findAll().size();
        // set the field null
        walletAccount.setAccountName(null);

        // Create the WalletAccount, which fails.

        restWalletAccountMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(walletAccount)))
            .andExpect(status().isBadRequest());

        List<WalletAccount> walletAccountList = walletAccountRepository.findAll();
        assertThat(walletAccountList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    void getAllWalletAccounts() throws Exception {
        // Initialize the database
        walletAccountRepository.saveAndFlush(walletAccount);

        // Get all the walletAccountList
        restWalletAccountMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(walletAccount.getId().intValue())))
            .andExpect(jsonPath("$.[*].accountNo").value(hasItem(DEFAULT_ACCOUNT_NO)))
            .andExpect(jsonPath("$.[*].cif").value(hasItem(DEFAULT_CIF)))
            .andExpect(jsonPath("$.[*].accountName").value(hasItem(DEFAULT_ACCOUNT_NAME)))
            .andExpect(jsonPath("$.[*].date").value(hasItem(DEFAULT_DATE.toString())))
            .andExpect(jsonPath("$.[*].closingBalance").value(hasItem(sameNumber(DEFAULT_CLOSING_BALANCE))))
            .andExpect(jsonPath("$.[*].openingBalance").value(hasItem(sameNumber(DEFAULT_OPENING_BALANCE))))
            .andExpect(jsonPath("$.[*].totalDebit").value(hasItem(sameNumber(DEFAULT_TOTAL_DEBIT))))
            .andExpect(jsonPath("$.[*].totalCredit").value(hasItem(sameNumber(DEFAULT_TOTAL_CREDIT))))
            .andExpect(jsonPath("$.[*].availableBalance").value(hasItem(sameNumber(DEFAULT_AVAILABLE_BALANCE))))
            .andExpect(jsonPath("$.[*].currentBalance").value(hasItem(sameNumber(DEFAULT_CURRENT_BALANCE))))
            .andExpect(jsonPath("$.[*].createdOn").value(hasItem(DEFAULT_CREATED_ON.toString())))
            .andExpect(jsonPath("$.[*].createdBy").value(hasItem(DEFAULT_CREATED_BY)))
            .andExpect(jsonPath("$.[*].accountStatus").value(hasItem(DEFAULT_ACCOUNT_STATUS.toString())));
    }

    @Test
    @Transactional
    void getWalletAccount() throws Exception {
        // Initialize the database
        walletAccountRepository.saveAndFlush(walletAccount);

        // Get the walletAccount
        restWalletAccountMockMvc
            .perform(get(ENTITY_API_URL_ID, walletAccount.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(walletAccount.getId().intValue()))
            .andExpect(jsonPath("$.accountNo").value(DEFAULT_ACCOUNT_NO))
            .andExpect(jsonPath("$.cif").value(DEFAULT_CIF))
            .andExpect(jsonPath("$.accountName").value(DEFAULT_ACCOUNT_NAME))
            .andExpect(jsonPath("$.date").value(DEFAULT_DATE.toString()))
            .andExpect(jsonPath("$.closingBalance").value(sameNumber(DEFAULT_CLOSING_BALANCE)))
            .andExpect(jsonPath("$.openingBalance").value(sameNumber(DEFAULT_OPENING_BALANCE)))
            .andExpect(jsonPath("$.totalDebit").value(sameNumber(DEFAULT_TOTAL_DEBIT)))
            .andExpect(jsonPath("$.totalCredit").value(sameNumber(DEFAULT_TOTAL_CREDIT)))
            .andExpect(jsonPath("$.availableBalance").value(sameNumber(DEFAULT_AVAILABLE_BALANCE)))
            .andExpect(jsonPath("$.currentBalance").value(sameNumber(DEFAULT_CURRENT_BALANCE)))
            .andExpect(jsonPath("$.createdOn").value(DEFAULT_CREATED_ON.toString()))
            .andExpect(jsonPath("$.createdBy").value(DEFAULT_CREATED_BY))
            .andExpect(jsonPath("$.accountStatus").value(DEFAULT_ACCOUNT_STATUS.toString()));
    }

    @Test
    @Transactional
    void getNonExistingWalletAccount() throws Exception {
        // Get the walletAccount
        restWalletAccountMockMvc.perform(get(ENTITY_API_URL_ID, Long.MAX_VALUE)).andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    void putNewWalletAccount() throws Exception {
        // Initialize the database
        walletAccountRepository.saveAndFlush(walletAccount);

        int databaseSizeBeforeUpdate = walletAccountRepository.findAll().size();

        // Update the walletAccount
        WalletAccount updatedWalletAccount = walletAccountRepository.findById(walletAccount.getId()).get();
        // Disconnect from session so that the updates on updatedWalletAccount are not directly saved in db
        em.detach(updatedWalletAccount);
        updatedWalletAccount
            .accountNo(UPDATED_ACCOUNT_NO)
            .cif(UPDATED_CIF)
            .accountName(UPDATED_ACCOUNT_NAME)
            .date(UPDATED_DATE)
            .closingBalance(UPDATED_CLOSING_BALANCE)
            .openingBalance(UPDATED_OPENING_BALANCE)
            .totalDebit(UPDATED_TOTAL_DEBIT)
            .totalCredit(UPDATED_TOTAL_CREDIT)
            .availableBalance(UPDATED_AVAILABLE_BALANCE)
            .currentBalance(UPDATED_CURRENT_BALANCE)
            .createdOn(UPDATED_CREATED_ON)
            .createdBy(UPDATED_CREATED_BY)
            .accountStatus(UPDATED_ACCOUNT_STATUS);

        restWalletAccountMockMvc
            .perform(
                put(ENTITY_API_URL_ID, updatedWalletAccount.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(updatedWalletAccount))
            )
            .andExpect(status().isOk());

        // Validate the WalletAccount in the database
        List<WalletAccount> walletAccountList = walletAccountRepository.findAll();
        assertThat(walletAccountList).hasSize(databaseSizeBeforeUpdate);
        WalletAccount testWalletAccount = walletAccountList.get(walletAccountList.size() - 1);
        assertThat(testWalletAccount.getAccountNo()).isEqualTo(UPDATED_ACCOUNT_NO);
        assertThat(testWalletAccount.getCif()).isEqualTo(UPDATED_CIF);
        assertThat(testWalletAccount.getAccountName()).isEqualTo(UPDATED_ACCOUNT_NAME);
        assertThat(testWalletAccount.getDate()).isEqualTo(UPDATED_DATE);
        assertThat(testWalletAccount.getClosingBalance()).isEqualTo(UPDATED_CLOSING_BALANCE);
        assertThat(testWalletAccount.getOpeningBalance()).isEqualTo(UPDATED_OPENING_BALANCE);
        assertThat(testWalletAccount.getTotalDebit()).isEqualTo(UPDATED_TOTAL_DEBIT);
        assertThat(testWalletAccount.getTotalCredit()).isEqualTo(UPDATED_TOTAL_CREDIT);
        assertThat(testWalletAccount.getAvailableBalance()).isEqualTo(UPDATED_AVAILABLE_BALANCE);
        assertThat(testWalletAccount.getCurrentBalance()).isEqualTo(UPDATED_CURRENT_BALANCE);
        assertThat(testWalletAccount.getCreatedOn()).isEqualTo(UPDATED_CREATED_ON);
        assertThat(testWalletAccount.getCreatedBy()).isEqualTo(UPDATED_CREATED_BY);
        assertThat(testWalletAccount.getAccountStatus()).isEqualTo(UPDATED_ACCOUNT_STATUS);
    }

    @Test
    @Transactional
    void putNonExistingWalletAccount() throws Exception {
        int databaseSizeBeforeUpdate = walletAccountRepository.findAll().size();
        walletAccount.setId(count.incrementAndGet());

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restWalletAccountMockMvc
            .perform(
                put(ENTITY_API_URL_ID, walletAccount.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(walletAccount))
            )
            .andExpect(status().isBadRequest());

        // Validate the WalletAccount in the database
        List<WalletAccount> walletAccountList = walletAccountRepository.findAll();
        assertThat(walletAccountList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithIdMismatchWalletAccount() throws Exception {
        int databaseSizeBeforeUpdate = walletAccountRepository.findAll().size();
        walletAccount.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restWalletAccountMockMvc
            .perform(
                put(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(walletAccount))
            )
            .andExpect(status().isBadRequest());

        // Validate the WalletAccount in the database
        List<WalletAccount> walletAccountList = walletAccountRepository.findAll();
        assertThat(walletAccountList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithMissingIdPathParamWalletAccount() throws Exception {
        int databaseSizeBeforeUpdate = walletAccountRepository.findAll().size();
        walletAccount.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restWalletAccountMockMvc
            .perform(put(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(walletAccount)))
            .andExpect(status().isMethodNotAllowed());

        // Validate the WalletAccount in the database
        List<WalletAccount> walletAccountList = walletAccountRepository.findAll();
        assertThat(walletAccountList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void partialUpdateWalletAccountWithPatch() throws Exception {
        // Initialize the database
        walletAccountRepository.saveAndFlush(walletAccount);

        int databaseSizeBeforeUpdate = walletAccountRepository.findAll().size();

        // Update the walletAccount using partial update
        WalletAccount partialUpdatedWalletAccount = new WalletAccount();
        partialUpdatedWalletAccount.setId(walletAccount.getId());

        partialUpdatedWalletAccount
            .accountName(UPDATED_ACCOUNT_NAME)
            .date(UPDATED_DATE)
            .closingBalance(UPDATED_CLOSING_BALANCE)
            .openingBalance(UPDATED_OPENING_BALANCE)
            .availableBalance(UPDATED_AVAILABLE_BALANCE)
            .createdBy(UPDATED_CREATED_BY);

        restWalletAccountMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedWalletAccount.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedWalletAccount))
            )
            .andExpect(status().isOk());

        // Validate the WalletAccount in the database
        List<WalletAccount> walletAccountList = walletAccountRepository.findAll();
        assertThat(walletAccountList).hasSize(databaseSizeBeforeUpdate);
        WalletAccount testWalletAccount = walletAccountList.get(walletAccountList.size() - 1);
        assertThat(testWalletAccount.getAccountNo()).isEqualTo(DEFAULT_ACCOUNT_NO);
        assertThat(testWalletAccount.getCif()).isEqualTo(DEFAULT_CIF);
        assertThat(testWalletAccount.getAccountName()).isEqualTo(UPDATED_ACCOUNT_NAME);
        assertThat(testWalletAccount.getDate()).isEqualTo(UPDATED_DATE);
        assertThat(testWalletAccount.getClosingBalance()).isEqualByComparingTo(UPDATED_CLOSING_BALANCE);
        assertThat(testWalletAccount.getOpeningBalance()).isEqualByComparingTo(UPDATED_OPENING_BALANCE);
        assertThat(testWalletAccount.getTotalDebit()).isEqualByComparingTo(DEFAULT_TOTAL_DEBIT);
        assertThat(testWalletAccount.getTotalCredit()).isEqualByComparingTo(DEFAULT_TOTAL_CREDIT);
        assertThat(testWalletAccount.getAvailableBalance()).isEqualByComparingTo(UPDATED_AVAILABLE_BALANCE);
        assertThat(testWalletAccount.getCurrentBalance()).isEqualByComparingTo(DEFAULT_CURRENT_BALANCE);
        assertThat(testWalletAccount.getCreatedOn()).isEqualTo(DEFAULT_CREATED_ON);
        assertThat(testWalletAccount.getCreatedBy()).isEqualTo(UPDATED_CREATED_BY);
        assertThat(testWalletAccount.getAccountStatus()).isEqualTo(DEFAULT_ACCOUNT_STATUS);
    }

    @Test
    @Transactional
    void fullUpdateWalletAccountWithPatch() throws Exception {
        // Initialize the database
        walletAccountRepository.saveAndFlush(walletAccount);

        int databaseSizeBeforeUpdate = walletAccountRepository.findAll().size();

        // Update the walletAccount using partial update
        WalletAccount partialUpdatedWalletAccount = new WalletAccount();
        partialUpdatedWalletAccount.setId(walletAccount.getId());

        partialUpdatedWalletAccount
            .accountNo(UPDATED_ACCOUNT_NO)
            .cif(UPDATED_CIF)
            .accountName(UPDATED_ACCOUNT_NAME)
            .date(UPDATED_DATE)
            .closingBalance(UPDATED_CLOSING_BALANCE)
            .openingBalance(UPDATED_OPENING_BALANCE)
            .totalDebit(UPDATED_TOTAL_DEBIT)
            .totalCredit(UPDATED_TOTAL_CREDIT)
            .availableBalance(UPDATED_AVAILABLE_BALANCE)
            .currentBalance(UPDATED_CURRENT_BALANCE)
            .createdOn(UPDATED_CREATED_ON)
            .createdBy(UPDATED_CREATED_BY)
            .accountStatus(UPDATED_ACCOUNT_STATUS);

        restWalletAccountMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedWalletAccount.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedWalletAccount))
            )
            .andExpect(status().isOk());

        // Validate the WalletAccount in the database
        List<WalletAccount> walletAccountList = walletAccountRepository.findAll();
        assertThat(walletAccountList).hasSize(databaseSizeBeforeUpdate);
        WalletAccount testWalletAccount = walletAccountList.get(walletAccountList.size() - 1);
        assertThat(testWalletAccount.getAccountNo()).isEqualTo(UPDATED_ACCOUNT_NO);
        assertThat(testWalletAccount.getCif()).isEqualTo(UPDATED_CIF);
        assertThat(testWalletAccount.getAccountName()).isEqualTo(UPDATED_ACCOUNT_NAME);
        assertThat(testWalletAccount.getDate()).isEqualTo(UPDATED_DATE);
        assertThat(testWalletAccount.getClosingBalance()).isEqualByComparingTo(UPDATED_CLOSING_BALANCE);
        assertThat(testWalletAccount.getOpeningBalance()).isEqualByComparingTo(UPDATED_OPENING_BALANCE);
        assertThat(testWalletAccount.getTotalDebit()).isEqualByComparingTo(UPDATED_TOTAL_DEBIT);
        assertThat(testWalletAccount.getTotalCredit()).isEqualByComparingTo(UPDATED_TOTAL_CREDIT);
        assertThat(testWalletAccount.getAvailableBalance()).isEqualByComparingTo(UPDATED_AVAILABLE_BALANCE);
        assertThat(testWalletAccount.getCurrentBalance()).isEqualByComparingTo(UPDATED_CURRENT_BALANCE);
        assertThat(testWalletAccount.getCreatedOn()).isEqualTo(UPDATED_CREATED_ON);
        assertThat(testWalletAccount.getCreatedBy()).isEqualTo(UPDATED_CREATED_BY);
        assertThat(testWalletAccount.getAccountStatus()).isEqualTo(UPDATED_ACCOUNT_STATUS);
    }

    @Test
    @Transactional
    void patchNonExistingWalletAccount() throws Exception {
        int databaseSizeBeforeUpdate = walletAccountRepository.findAll().size();
        walletAccount.setId(count.incrementAndGet());

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restWalletAccountMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, walletAccount.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(walletAccount))
            )
            .andExpect(status().isBadRequest());

        // Validate the WalletAccount in the database
        List<WalletAccount> walletAccountList = walletAccountRepository.findAll();
        assertThat(walletAccountList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithIdMismatchWalletAccount() throws Exception {
        int databaseSizeBeforeUpdate = walletAccountRepository.findAll().size();
        walletAccount.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restWalletAccountMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(walletAccount))
            )
            .andExpect(status().isBadRequest());

        // Validate the WalletAccount in the database
        List<WalletAccount> walletAccountList = walletAccountRepository.findAll();
        assertThat(walletAccountList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithMissingIdPathParamWalletAccount() throws Exception {
        int databaseSizeBeforeUpdate = walletAccountRepository.findAll().size();
        walletAccount.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restWalletAccountMockMvc
            .perform(
                patch(ENTITY_API_URL).contentType("application/merge-patch+json").content(TestUtil.convertObjectToJsonBytes(walletAccount))
            )
            .andExpect(status().isMethodNotAllowed());

        // Validate the WalletAccount in the database
        List<WalletAccount> walletAccountList = walletAccountRepository.findAll();
        assertThat(walletAccountList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void deleteWalletAccount() throws Exception {
        // Initialize the database
        walletAccountRepository.saveAndFlush(walletAccount);

        int databaseSizeBeforeDelete = walletAccountRepository.findAll().size();

        // Delete the walletAccount
        restWalletAccountMockMvc
            .perform(delete(ENTITY_API_URL_ID, walletAccount.getId()).accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<WalletAccount> walletAccountList = walletAccountRepository.findAll();
        assertThat(walletAccountList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
