package com.iconnect101.glapp.web.rest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import com.iconnect101.glapp.IntegrationTest;
import com.iconnect101.glapp.domain.Beneficiary;
import com.iconnect101.glapp.repository.BeneficiaryRepository;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.List;
import java.util.Random;
import java.util.concurrent.atomic.AtomicLong;
import javax.persistence.EntityManager;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;

/**
 * Integration tests for the {@link BeneficiaryResource} REST controller.
 */
@IntegrationTest
@AutoConfigureMockMvc
@WithMockUser
class BeneficiaryResourceIT {

    private static final String DEFAULT_TITLE = "AAAAAAAAAA";
    private static final String UPDATED_TITLE = "BBBBBBBBBB";

    private static final String DEFAULT_BENEFICIARY_NAME = "AAAAAAAAAA";
    private static final String UPDATED_BENEFICIARY_NAME = "BBBBBBBBBB";

    private static final String DEFAULT_BENEFICIARY_ACCOUNT_NO = "AAAAAAAAAA";
    private static final String UPDATED_BENEFICIARY_ACCOUNT_NO = "BBBBBBBBBB";

    private static final String DEFAULT_BENEFICIARYBANK_SWIFT_CODE = "AAAAAAAAAA";
    private static final String UPDATED_BENEFICIARYBANK_SWIFT_CODE = "BBBBBBBBBB";

    private static final String DEFAULT_INTERMEDIARY_BANK_SWIFT_CODE = "AAAAAAAAAA";
    private static final String UPDATED_INTERMEDIARY_BANK_SWIFT_CODE = "BBBBBBBBBB";

    private static final String DEFAULT_BENEFICIARY_BANK_NAME = "AAAAAAAAAA";
    private static final String UPDATED_BENEFICIARY_BANK_NAME = "BBBBBBBBBB";

    private static final String DEFAULT_BENEFICIARY_BANK_BRANCH_NAME = "AAAAAAAAAA";
    private static final String UPDATED_BENEFICIARY_BANK_BRANCH_NAME = "BBBBBBBBBB";

    private static final String DEFAULT_BENEFICIARY_ADDRESS_1 = "AAAAAAAAAA";
    private static final String UPDATED_BENEFICIARY_ADDRESS_1 = "BBBBBBBBBB";

    private static final String DEFAULT_BENEFICIARY_ADDRESS_2 = "AAAAAAAAAA";
    private static final String UPDATED_BENEFICIARY_ADDRESS_2 = "BBBBBBBBBB";

    private static final String DEFAULT_BENEFICIARY_ADDRESS_3 = "AAAAAAAAAA";
    private static final String UPDATED_BENEFICIARY_ADDRESS_3 = "BBBBBBBBBB";

    private static final String DEFAULT_BENEFICIARY_STATE = "AAAAAAAAAA";
    private static final String UPDATED_BENEFICIARY_STATE = "BBBBBBBBBB";

    private static final String DEFAULT_BENEFICIARY_PIN_CODE = "AAAAAAAAAA";
    private static final String UPDATED_BENEFICIARY_PIN_CODE = "BBBBBBBBBB";

    private static final Instant DEFAULT_CREATED_ON = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_CREATED_ON = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    private static final String DEFAULT_CREATED_BY = "AAAAAAAAAA";
    private static final String UPDATED_CREATED_BY = "BBBBBBBBBB";

    private static final String ENTITY_API_URL = "/api/beneficiaries";
    private static final String ENTITY_API_URL_ID = ENTITY_API_URL + "/{id}";

    private static Random random = new Random();
    private static AtomicLong count = new AtomicLong(random.nextInt() + (2 * Integer.MAX_VALUE));

    @Autowired
    private BeneficiaryRepository beneficiaryRepository;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restBeneficiaryMockMvc;

    private Beneficiary beneficiary;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Beneficiary createEntity(EntityManager em) {
        Beneficiary beneficiary = new Beneficiary()
            .title(DEFAULT_TITLE)
            .beneficiaryName(DEFAULT_BENEFICIARY_NAME)
            .beneficiaryAccountNo(DEFAULT_BENEFICIARY_ACCOUNT_NO)
            .beneficiarybankSwiftCode(DEFAULT_BENEFICIARYBANK_SWIFT_CODE)
            .intermediaryBankSwiftCode(DEFAULT_INTERMEDIARY_BANK_SWIFT_CODE)
            .beneficiaryBankName(DEFAULT_BENEFICIARY_BANK_NAME)
            .beneficiaryBankBranchName(DEFAULT_BENEFICIARY_BANK_BRANCH_NAME)
            .beneficiaryAddress1(DEFAULT_BENEFICIARY_ADDRESS_1)
            .beneficiaryAddress2(DEFAULT_BENEFICIARY_ADDRESS_2)
            .beneficiaryAddress3(DEFAULT_BENEFICIARY_ADDRESS_3)
            .beneficiaryState(DEFAULT_BENEFICIARY_STATE)
            .beneficiaryPinCode(DEFAULT_BENEFICIARY_PIN_CODE)
            .createdOn(DEFAULT_CREATED_ON)
            .createdBy(DEFAULT_CREATED_BY);
        return beneficiary;
    }

    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Beneficiary createUpdatedEntity(EntityManager em) {
        Beneficiary beneficiary = new Beneficiary()
            .title(UPDATED_TITLE)
            .beneficiaryName(UPDATED_BENEFICIARY_NAME)
            .beneficiaryAccountNo(UPDATED_BENEFICIARY_ACCOUNT_NO)
            .beneficiarybankSwiftCode(UPDATED_BENEFICIARYBANK_SWIFT_CODE)
            .intermediaryBankSwiftCode(UPDATED_INTERMEDIARY_BANK_SWIFT_CODE)
            .beneficiaryBankName(UPDATED_BENEFICIARY_BANK_NAME)
            .beneficiaryBankBranchName(UPDATED_BENEFICIARY_BANK_BRANCH_NAME)
            .beneficiaryAddress1(UPDATED_BENEFICIARY_ADDRESS_1)
            .beneficiaryAddress2(UPDATED_BENEFICIARY_ADDRESS_2)
            .beneficiaryAddress3(UPDATED_BENEFICIARY_ADDRESS_3)
            .beneficiaryState(UPDATED_BENEFICIARY_STATE)
            .beneficiaryPinCode(UPDATED_BENEFICIARY_PIN_CODE)
            .createdOn(UPDATED_CREATED_ON)
            .createdBy(UPDATED_CREATED_BY);
        return beneficiary;
    }

    @BeforeEach
    public void initTest() {
        beneficiary = createEntity(em);
    }

    @Test
    @Transactional
    void createBeneficiary() throws Exception {
        int databaseSizeBeforeCreate = beneficiaryRepository.findAll().size();
        // Create the Beneficiary
        restBeneficiaryMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(beneficiary)))
            .andExpect(status().isCreated());

        // Validate the Beneficiary in the database
        List<Beneficiary> beneficiaryList = beneficiaryRepository.findAll();
        assertThat(beneficiaryList).hasSize(databaseSizeBeforeCreate + 1);
        Beneficiary testBeneficiary = beneficiaryList.get(beneficiaryList.size() - 1);
        assertThat(testBeneficiary.getTitle()).isEqualTo(DEFAULT_TITLE);
        assertThat(testBeneficiary.getBeneficiaryName()).isEqualTo(DEFAULT_BENEFICIARY_NAME);
        assertThat(testBeneficiary.getBeneficiaryAccountNo()).isEqualTo(DEFAULT_BENEFICIARY_ACCOUNT_NO);
        assertThat(testBeneficiary.getBeneficiarybankSwiftCode()).isEqualTo(DEFAULT_BENEFICIARYBANK_SWIFT_CODE);
        assertThat(testBeneficiary.getIntermediaryBankSwiftCode()).isEqualTo(DEFAULT_INTERMEDIARY_BANK_SWIFT_CODE);
        assertThat(testBeneficiary.getBeneficiaryBankName()).isEqualTo(DEFAULT_BENEFICIARY_BANK_NAME);
        assertThat(testBeneficiary.getBeneficiaryBankBranchName()).isEqualTo(DEFAULT_BENEFICIARY_BANK_BRANCH_NAME);
        assertThat(testBeneficiary.getBeneficiaryAddress1()).isEqualTo(DEFAULT_BENEFICIARY_ADDRESS_1);
        assertThat(testBeneficiary.getBeneficiaryAddress2()).isEqualTo(DEFAULT_BENEFICIARY_ADDRESS_2);
        assertThat(testBeneficiary.getBeneficiaryAddress3()).isEqualTo(DEFAULT_BENEFICIARY_ADDRESS_3);
        assertThat(testBeneficiary.getBeneficiaryState()).isEqualTo(DEFAULT_BENEFICIARY_STATE);
        assertThat(testBeneficiary.getBeneficiaryPinCode()).isEqualTo(DEFAULT_BENEFICIARY_PIN_CODE);
        assertThat(testBeneficiary.getCreatedOn()).isEqualTo(DEFAULT_CREATED_ON);
        assertThat(testBeneficiary.getCreatedBy()).isEqualTo(DEFAULT_CREATED_BY);
    }

    @Test
    @Transactional
    void createBeneficiaryWithExistingId() throws Exception {
        // Create the Beneficiary with an existing ID
        beneficiary.setId(1L);

        int databaseSizeBeforeCreate = beneficiaryRepository.findAll().size();

        // An entity with an existing ID cannot be created, so this API call must fail
        restBeneficiaryMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(beneficiary)))
            .andExpect(status().isBadRequest());

        // Validate the Beneficiary in the database
        List<Beneficiary> beneficiaryList = beneficiaryRepository.findAll();
        assertThat(beneficiaryList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    void getAllBeneficiaries() throws Exception {
        // Initialize the database
        beneficiaryRepository.saveAndFlush(beneficiary);

        // Get all the beneficiaryList
        restBeneficiaryMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(beneficiary.getId().intValue())))
            .andExpect(jsonPath("$.[*].title").value(hasItem(DEFAULT_TITLE)))
            .andExpect(jsonPath("$.[*].beneficiaryName").value(hasItem(DEFAULT_BENEFICIARY_NAME)))
            .andExpect(jsonPath("$.[*].beneficiaryAccountNo").value(hasItem(DEFAULT_BENEFICIARY_ACCOUNT_NO)))
            .andExpect(jsonPath("$.[*].beneficiarybankSwiftCode").value(hasItem(DEFAULT_BENEFICIARYBANK_SWIFT_CODE)))
            .andExpect(jsonPath("$.[*].intermediaryBankSwiftCode").value(hasItem(DEFAULT_INTERMEDIARY_BANK_SWIFT_CODE)))
            .andExpect(jsonPath("$.[*].beneficiaryBankName").value(hasItem(DEFAULT_BENEFICIARY_BANK_NAME)))
            .andExpect(jsonPath("$.[*].beneficiaryBankBranchName").value(hasItem(DEFAULT_BENEFICIARY_BANK_BRANCH_NAME)))
            .andExpect(jsonPath("$.[*].beneficiaryAddress1").value(hasItem(DEFAULT_BENEFICIARY_ADDRESS_1)))
            .andExpect(jsonPath("$.[*].beneficiaryAddress2").value(hasItem(DEFAULT_BENEFICIARY_ADDRESS_2)))
            .andExpect(jsonPath("$.[*].beneficiaryAddress3").value(hasItem(DEFAULT_BENEFICIARY_ADDRESS_3)))
            .andExpect(jsonPath("$.[*].beneficiaryState").value(hasItem(DEFAULT_BENEFICIARY_STATE)))
            .andExpect(jsonPath("$.[*].beneficiaryPinCode").value(hasItem(DEFAULT_BENEFICIARY_PIN_CODE)))
            .andExpect(jsonPath("$.[*].createdOn").value(hasItem(DEFAULT_CREATED_ON.toString())))
            .andExpect(jsonPath("$.[*].createdBy").value(hasItem(DEFAULT_CREATED_BY)));
    }

    @Test
    @Transactional
    void getBeneficiary() throws Exception {
        // Initialize the database
        beneficiaryRepository.saveAndFlush(beneficiary);

        // Get the beneficiary
        restBeneficiaryMockMvc
            .perform(get(ENTITY_API_URL_ID, beneficiary.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(beneficiary.getId().intValue()))
            .andExpect(jsonPath("$.title").value(DEFAULT_TITLE))
            .andExpect(jsonPath("$.beneficiaryName").value(DEFAULT_BENEFICIARY_NAME))
            .andExpect(jsonPath("$.beneficiaryAccountNo").value(DEFAULT_BENEFICIARY_ACCOUNT_NO))
            .andExpect(jsonPath("$.beneficiarybankSwiftCode").value(DEFAULT_BENEFICIARYBANK_SWIFT_CODE))
            .andExpect(jsonPath("$.intermediaryBankSwiftCode").value(DEFAULT_INTERMEDIARY_BANK_SWIFT_CODE))
            .andExpect(jsonPath("$.beneficiaryBankName").value(DEFAULT_BENEFICIARY_BANK_NAME))
            .andExpect(jsonPath("$.beneficiaryBankBranchName").value(DEFAULT_BENEFICIARY_BANK_BRANCH_NAME))
            .andExpect(jsonPath("$.beneficiaryAddress1").value(DEFAULT_BENEFICIARY_ADDRESS_1))
            .andExpect(jsonPath("$.beneficiaryAddress2").value(DEFAULT_BENEFICIARY_ADDRESS_2))
            .andExpect(jsonPath("$.beneficiaryAddress3").value(DEFAULT_BENEFICIARY_ADDRESS_3))
            .andExpect(jsonPath("$.beneficiaryState").value(DEFAULT_BENEFICIARY_STATE))
            .andExpect(jsonPath("$.beneficiaryPinCode").value(DEFAULT_BENEFICIARY_PIN_CODE))
            .andExpect(jsonPath("$.createdOn").value(DEFAULT_CREATED_ON.toString()))
            .andExpect(jsonPath("$.createdBy").value(DEFAULT_CREATED_BY));
    }

    @Test
    @Transactional
    void getNonExistingBeneficiary() throws Exception {
        // Get the beneficiary
        restBeneficiaryMockMvc.perform(get(ENTITY_API_URL_ID, Long.MAX_VALUE)).andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    void putNewBeneficiary() throws Exception {
        // Initialize the database
        beneficiaryRepository.saveAndFlush(beneficiary);

        int databaseSizeBeforeUpdate = beneficiaryRepository.findAll().size();

        // Update the beneficiary
        Beneficiary updatedBeneficiary = beneficiaryRepository.findById(beneficiary.getId()).get();
        // Disconnect from session so that the updates on updatedBeneficiary are not directly saved in db
        em.detach(updatedBeneficiary);
        updatedBeneficiary
            .title(UPDATED_TITLE)
            .beneficiaryName(UPDATED_BENEFICIARY_NAME)
            .beneficiaryAccountNo(UPDATED_BENEFICIARY_ACCOUNT_NO)
            .beneficiarybankSwiftCode(UPDATED_BENEFICIARYBANK_SWIFT_CODE)
            .intermediaryBankSwiftCode(UPDATED_INTERMEDIARY_BANK_SWIFT_CODE)
            .beneficiaryBankName(UPDATED_BENEFICIARY_BANK_NAME)
            .beneficiaryBankBranchName(UPDATED_BENEFICIARY_BANK_BRANCH_NAME)
            .beneficiaryAddress1(UPDATED_BENEFICIARY_ADDRESS_1)
            .beneficiaryAddress2(UPDATED_BENEFICIARY_ADDRESS_2)
            .beneficiaryAddress3(UPDATED_BENEFICIARY_ADDRESS_3)
            .beneficiaryState(UPDATED_BENEFICIARY_STATE)
            .beneficiaryPinCode(UPDATED_BENEFICIARY_PIN_CODE)
            .createdOn(UPDATED_CREATED_ON)
            .createdBy(UPDATED_CREATED_BY);

        restBeneficiaryMockMvc
            .perform(
                put(ENTITY_API_URL_ID, updatedBeneficiary.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(updatedBeneficiary))
            )
            .andExpect(status().isOk());

        // Validate the Beneficiary in the database
        List<Beneficiary> beneficiaryList = beneficiaryRepository.findAll();
        assertThat(beneficiaryList).hasSize(databaseSizeBeforeUpdate);
        Beneficiary testBeneficiary = beneficiaryList.get(beneficiaryList.size() - 1);
        assertThat(testBeneficiary.getTitle()).isEqualTo(UPDATED_TITLE);
        assertThat(testBeneficiary.getBeneficiaryName()).isEqualTo(UPDATED_BENEFICIARY_NAME);
        assertThat(testBeneficiary.getBeneficiaryAccountNo()).isEqualTo(UPDATED_BENEFICIARY_ACCOUNT_NO);
        assertThat(testBeneficiary.getBeneficiarybankSwiftCode()).isEqualTo(UPDATED_BENEFICIARYBANK_SWIFT_CODE);
        assertThat(testBeneficiary.getIntermediaryBankSwiftCode()).isEqualTo(UPDATED_INTERMEDIARY_BANK_SWIFT_CODE);
        assertThat(testBeneficiary.getBeneficiaryBankName()).isEqualTo(UPDATED_BENEFICIARY_BANK_NAME);
        assertThat(testBeneficiary.getBeneficiaryBankBranchName()).isEqualTo(UPDATED_BENEFICIARY_BANK_BRANCH_NAME);
        assertThat(testBeneficiary.getBeneficiaryAddress1()).isEqualTo(UPDATED_BENEFICIARY_ADDRESS_1);
        assertThat(testBeneficiary.getBeneficiaryAddress2()).isEqualTo(UPDATED_BENEFICIARY_ADDRESS_2);
        assertThat(testBeneficiary.getBeneficiaryAddress3()).isEqualTo(UPDATED_BENEFICIARY_ADDRESS_3);
        assertThat(testBeneficiary.getBeneficiaryState()).isEqualTo(UPDATED_BENEFICIARY_STATE);
        assertThat(testBeneficiary.getBeneficiaryPinCode()).isEqualTo(UPDATED_BENEFICIARY_PIN_CODE);
        assertThat(testBeneficiary.getCreatedOn()).isEqualTo(UPDATED_CREATED_ON);
        assertThat(testBeneficiary.getCreatedBy()).isEqualTo(UPDATED_CREATED_BY);
    }

    @Test
    @Transactional
    void putNonExistingBeneficiary() throws Exception {
        int databaseSizeBeforeUpdate = beneficiaryRepository.findAll().size();
        beneficiary.setId(count.incrementAndGet());

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restBeneficiaryMockMvc
            .perform(
                put(ENTITY_API_URL_ID, beneficiary.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(beneficiary))
            )
            .andExpect(status().isBadRequest());

        // Validate the Beneficiary in the database
        List<Beneficiary> beneficiaryList = beneficiaryRepository.findAll();
        assertThat(beneficiaryList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithIdMismatchBeneficiary() throws Exception {
        int databaseSizeBeforeUpdate = beneficiaryRepository.findAll().size();
        beneficiary.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restBeneficiaryMockMvc
            .perform(
                put(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(beneficiary))
            )
            .andExpect(status().isBadRequest());

        // Validate the Beneficiary in the database
        List<Beneficiary> beneficiaryList = beneficiaryRepository.findAll();
        assertThat(beneficiaryList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithMissingIdPathParamBeneficiary() throws Exception {
        int databaseSizeBeforeUpdate = beneficiaryRepository.findAll().size();
        beneficiary.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restBeneficiaryMockMvc
            .perform(put(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(beneficiary)))
            .andExpect(status().isMethodNotAllowed());

        // Validate the Beneficiary in the database
        List<Beneficiary> beneficiaryList = beneficiaryRepository.findAll();
        assertThat(beneficiaryList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void partialUpdateBeneficiaryWithPatch() throws Exception {
        // Initialize the database
        beneficiaryRepository.saveAndFlush(beneficiary);

        int databaseSizeBeforeUpdate = beneficiaryRepository.findAll().size();

        // Update the beneficiary using partial update
        Beneficiary partialUpdatedBeneficiary = new Beneficiary();
        partialUpdatedBeneficiary.setId(beneficiary.getId());

        partialUpdatedBeneficiary
            .title(UPDATED_TITLE)
            .beneficiaryName(UPDATED_BENEFICIARY_NAME)
            .beneficiaryAccountNo(UPDATED_BENEFICIARY_ACCOUNT_NO)
            .beneficiarybankSwiftCode(UPDATED_BENEFICIARYBANK_SWIFT_CODE)
            .intermediaryBankSwiftCode(UPDATED_INTERMEDIARY_BANK_SWIFT_CODE)
            .beneficiaryBankBranchName(UPDATED_BENEFICIARY_BANK_BRANCH_NAME)
            .beneficiaryAddress1(UPDATED_BENEFICIARY_ADDRESS_1)
            .beneficiaryAddress2(UPDATED_BENEFICIARY_ADDRESS_2)
            .beneficiaryAddress3(UPDATED_BENEFICIARY_ADDRESS_3)
            .createdBy(UPDATED_CREATED_BY);

        restBeneficiaryMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedBeneficiary.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedBeneficiary))
            )
            .andExpect(status().isOk());

        // Validate the Beneficiary in the database
        List<Beneficiary> beneficiaryList = beneficiaryRepository.findAll();
        assertThat(beneficiaryList).hasSize(databaseSizeBeforeUpdate);
        Beneficiary testBeneficiary = beneficiaryList.get(beneficiaryList.size() - 1);
        assertThat(testBeneficiary.getTitle()).isEqualTo(UPDATED_TITLE);
        assertThat(testBeneficiary.getBeneficiaryName()).isEqualTo(UPDATED_BENEFICIARY_NAME);
        assertThat(testBeneficiary.getBeneficiaryAccountNo()).isEqualTo(UPDATED_BENEFICIARY_ACCOUNT_NO);
        assertThat(testBeneficiary.getBeneficiarybankSwiftCode()).isEqualTo(UPDATED_BENEFICIARYBANK_SWIFT_CODE);
        assertThat(testBeneficiary.getIntermediaryBankSwiftCode()).isEqualTo(UPDATED_INTERMEDIARY_BANK_SWIFT_CODE);
        assertThat(testBeneficiary.getBeneficiaryBankName()).isEqualTo(DEFAULT_BENEFICIARY_BANK_NAME);
        assertThat(testBeneficiary.getBeneficiaryBankBranchName()).isEqualTo(UPDATED_BENEFICIARY_BANK_BRANCH_NAME);
        assertThat(testBeneficiary.getBeneficiaryAddress1()).isEqualTo(UPDATED_BENEFICIARY_ADDRESS_1);
        assertThat(testBeneficiary.getBeneficiaryAddress2()).isEqualTo(UPDATED_BENEFICIARY_ADDRESS_2);
        assertThat(testBeneficiary.getBeneficiaryAddress3()).isEqualTo(UPDATED_BENEFICIARY_ADDRESS_3);
        assertThat(testBeneficiary.getBeneficiaryState()).isEqualTo(DEFAULT_BENEFICIARY_STATE);
        assertThat(testBeneficiary.getBeneficiaryPinCode()).isEqualTo(DEFAULT_BENEFICIARY_PIN_CODE);
        assertThat(testBeneficiary.getCreatedOn()).isEqualTo(DEFAULT_CREATED_ON);
        assertThat(testBeneficiary.getCreatedBy()).isEqualTo(UPDATED_CREATED_BY);
    }

    @Test
    @Transactional
    void fullUpdateBeneficiaryWithPatch() throws Exception {
        // Initialize the database
        beneficiaryRepository.saveAndFlush(beneficiary);

        int databaseSizeBeforeUpdate = beneficiaryRepository.findAll().size();

        // Update the beneficiary using partial update
        Beneficiary partialUpdatedBeneficiary = new Beneficiary();
        partialUpdatedBeneficiary.setId(beneficiary.getId());

        partialUpdatedBeneficiary
            .title(UPDATED_TITLE)
            .beneficiaryName(UPDATED_BENEFICIARY_NAME)
            .beneficiaryAccountNo(UPDATED_BENEFICIARY_ACCOUNT_NO)
            .beneficiarybankSwiftCode(UPDATED_BENEFICIARYBANK_SWIFT_CODE)
            .intermediaryBankSwiftCode(UPDATED_INTERMEDIARY_BANK_SWIFT_CODE)
            .beneficiaryBankName(UPDATED_BENEFICIARY_BANK_NAME)
            .beneficiaryBankBranchName(UPDATED_BENEFICIARY_BANK_BRANCH_NAME)
            .beneficiaryAddress1(UPDATED_BENEFICIARY_ADDRESS_1)
            .beneficiaryAddress2(UPDATED_BENEFICIARY_ADDRESS_2)
            .beneficiaryAddress3(UPDATED_BENEFICIARY_ADDRESS_3)
            .beneficiaryState(UPDATED_BENEFICIARY_STATE)
            .beneficiaryPinCode(UPDATED_BENEFICIARY_PIN_CODE)
            .createdOn(UPDATED_CREATED_ON)
            .createdBy(UPDATED_CREATED_BY);

        restBeneficiaryMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedBeneficiary.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedBeneficiary))
            )
            .andExpect(status().isOk());

        // Validate the Beneficiary in the database
        List<Beneficiary> beneficiaryList = beneficiaryRepository.findAll();
        assertThat(beneficiaryList).hasSize(databaseSizeBeforeUpdate);
        Beneficiary testBeneficiary = beneficiaryList.get(beneficiaryList.size() - 1);
        assertThat(testBeneficiary.getTitle()).isEqualTo(UPDATED_TITLE);
        assertThat(testBeneficiary.getBeneficiaryName()).isEqualTo(UPDATED_BENEFICIARY_NAME);
        assertThat(testBeneficiary.getBeneficiaryAccountNo()).isEqualTo(UPDATED_BENEFICIARY_ACCOUNT_NO);
        assertThat(testBeneficiary.getBeneficiarybankSwiftCode()).isEqualTo(UPDATED_BENEFICIARYBANK_SWIFT_CODE);
        assertThat(testBeneficiary.getIntermediaryBankSwiftCode()).isEqualTo(UPDATED_INTERMEDIARY_BANK_SWIFT_CODE);
        assertThat(testBeneficiary.getBeneficiaryBankName()).isEqualTo(UPDATED_BENEFICIARY_BANK_NAME);
        assertThat(testBeneficiary.getBeneficiaryBankBranchName()).isEqualTo(UPDATED_BENEFICIARY_BANK_BRANCH_NAME);
        assertThat(testBeneficiary.getBeneficiaryAddress1()).isEqualTo(UPDATED_BENEFICIARY_ADDRESS_1);
        assertThat(testBeneficiary.getBeneficiaryAddress2()).isEqualTo(UPDATED_BENEFICIARY_ADDRESS_2);
        assertThat(testBeneficiary.getBeneficiaryAddress3()).isEqualTo(UPDATED_BENEFICIARY_ADDRESS_3);
        assertThat(testBeneficiary.getBeneficiaryState()).isEqualTo(UPDATED_BENEFICIARY_STATE);
        assertThat(testBeneficiary.getBeneficiaryPinCode()).isEqualTo(UPDATED_BENEFICIARY_PIN_CODE);
        assertThat(testBeneficiary.getCreatedOn()).isEqualTo(UPDATED_CREATED_ON);
        assertThat(testBeneficiary.getCreatedBy()).isEqualTo(UPDATED_CREATED_BY);
    }

    @Test
    @Transactional
    void patchNonExistingBeneficiary() throws Exception {
        int databaseSizeBeforeUpdate = beneficiaryRepository.findAll().size();
        beneficiary.setId(count.incrementAndGet());

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restBeneficiaryMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, beneficiary.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(beneficiary))
            )
            .andExpect(status().isBadRequest());

        // Validate the Beneficiary in the database
        List<Beneficiary> beneficiaryList = beneficiaryRepository.findAll();
        assertThat(beneficiaryList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithIdMismatchBeneficiary() throws Exception {
        int databaseSizeBeforeUpdate = beneficiaryRepository.findAll().size();
        beneficiary.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restBeneficiaryMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(beneficiary))
            )
            .andExpect(status().isBadRequest());

        // Validate the Beneficiary in the database
        List<Beneficiary> beneficiaryList = beneficiaryRepository.findAll();
        assertThat(beneficiaryList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithMissingIdPathParamBeneficiary() throws Exception {
        int databaseSizeBeforeUpdate = beneficiaryRepository.findAll().size();
        beneficiary.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restBeneficiaryMockMvc
            .perform(
                patch(ENTITY_API_URL).contentType("application/merge-patch+json").content(TestUtil.convertObjectToJsonBytes(beneficiary))
            )
            .andExpect(status().isMethodNotAllowed());

        // Validate the Beneficiary in the database
        List<Beneficiary> beneficiaryList = beneficiaryRepository.findAll();
        assertThat(beneficiaryList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void deleteBeneficiary() throws Exception {
        // Initialize the database
        beneficiaryRepository.saveAndFlush(beneficiary);

        int databaseSizeBeforeDelete = beneficiaryRepository.findAll().size();

        // Delete the beneficiary
        restBeneficiaryMockMvc
            .perform(delete(ENTITY_API_URL_ID, beneficiary.getId()).accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<Beneficiary> beneficiaryList = beneficiaryRepository.findAll();
        assertThat(beneficiaryList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
