package com.iconnect101.glapp.web.rest;

import static com.iconnect101.glapp.web.rest.TestUtil.sameInstant;
import static com.iconnect101.glapp.web.rest.TestUtil.sameNumber;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import com.iconnect101.glapp.IntegrationTest;
import com.iconnect101.glapp.domain.SubLedger;
import com.iconnect101.glapp.domain.enumeration.EntryStatus;
import com.iconnect101.glapp.domain.enumeration.GlEntryType;
import com.iconnect101.glapp.repository.SubLedgerRepository;
import java.math.BigDecimal;
import java.time.Instant;
import java.time.LocalDate;
import java.time.ZoneId;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.time.temporal.ChronoUnit;
import java.util.List;
import java.util.Random;
import java.util.concurrent.atomic.AtomicLong;
import javax.persistence.EntityManager;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;

/**
 * Integration tests for the {@link SubLedgerResource} REST controller.
 */
@IntegrationTest
@AutoConfigureMockMvc
@WithMockUser
class SubLedgerResourceIT {

    private static final String DEFAULT_SUB_LEDGER_REF_NO = "AAAAAAAAAA";
    private static final String UPDATED_SUB_LEDGER_REF_NO = "BBBBBBBBBB";

    private static final String DEFAULT_ACCOUNT_NO = "AAAAAAAAAA";
    private static final String UPDATED_ACCOUNT_NO = "BBBBBBBBBB";

    private static final LocalDate DEFAULT_TXN_DATE = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_TXN_DATE = LocalDate.now(ZoneId.systemDefault());

    private static final Instant DEFAULT_TXN_DATE_IN_UTC = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_TXN_DATE_IN_UTC = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    private static final ZonedDateTime DEFAULT_TXN_DATE_IN_LOCAL = ZonedDateTime.ofInstant(Instant.ofEpochMilli(0L), ZoneOffset.UTC);
    private static final ZonedDateTime UPDATED_TXN_DATE_IN_LOCAL = ZonedDateTime.now(ZoneId.systemDefault()).withNano(0);

    private static final ZonedDateTime DEFAULT_TXN_DATE_FOR_SETTLEMENT = ZonedDateTime.ofInstant(Instant.ofEpochMilli(0L), ZoneOffset.UTC);
    private static final ZonedDateTime UPDATED_TXN_DATE_FOR_SETTLEMENT = ZonedDateTime.now(ZoneId.systemDefault()).withNano(0);

    private static final BigDecimal DEFAULT_AMOUNT_IN_ACCOUNT_CURRENCY = new BigDecimal(1);
    private static final BigDecimal UPDATED_AMOUNT_IN_ACCOUNT_CURRENCY = new BigDecimal(2);

    private static final BigDecimal DEFAULT_AMOUNT_IN_LOCAL_CURRENCY = new BigDecimal(1);
    private static final BigDecimal UPDATED_AMOUNT_IN_LOCAL_CURRENCY = new BigDecimal(2);

    private static final BigDecimal DEFAULT_RATE = new BigDecimal(1);
    private static final BigDecimal UPDATED_RATE = new BigDecimal(2);

    private static final String DEFAULT_ENTRY_CATEGORY = "AAAAAAAAAA";
    private static final String UPDATED_ENTRY_CATEGORY = "BBBBBBBBBB";

    private static final String DEFAULT_DESCRIPTION_1 = "AAAAAAAAAA";
    private static final String UPDATED_DESCRIPTION_1 = "BBBBBBBBBB";

    private static final String DEFAULT_DESCRIPTION_2 = "AAAAAAAAAA";
    private static final String UPDATED_DESCRIPTION_2 = "BBBBBBBBBB";

    private static final Instant DEFAULT_CREATED_ON = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_CREATED_ON = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    private static final String DEFAULT_CREATED_BY = "AAAAAAAAAA";
    private static final String UPDATED_CREATED_BY = "BBBBBBBBBB";

    private static final EntryStatus DEFAULT_ENTRY_STATUS = EntryStatus.ACTIVE;
    private static final EntryStatus UPDATED_ENTRY_STATUS = EntryStatus.CANCELLED;

    private static final GlEntryType DEFAULT_GL_ENTRY_TYPE = GlEntryType.DR;
    private static final GlEntryType UPDATED_GL_ENTRY_TYPE = GlEntryType.CR;

    private static final String ENTITY_API_URL = "/api/sub-ledgers";
    private static final String ENTITY_API_URL_ID = ENTITY_API_URL + "/{id}";

    private static Random random = new Random();
    private static AtomicLong count = new AtomicLong(random.nextInt() + (2 * Integer.MAX_VALUE));

    @Autowired
    private SubLedgerRepository subLedgerRepository;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restSubLedgerMockMvc;

    private SubLedger subLedger;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static SubLedger createEntity(EntityManager em) {
        SubLedger subLedger = new SubLedger()
            .subLedgerRefNo(DEFAULT_SUB_LEDGER_REF_NO)
            .accountNo(DEFAULT_ACCOUNT_NO)
            .txnDate(DEFAULT_TXN_DATE)
            .txnDateInUTC(DEFAULT_TXN_DATE_IN_UTC)
            .txnDateInLocal(DEFAULT_TXN_DATE_IN_LOCAL)
            .txnDateForSettlement(DEFAULT_TXN_DATE_FOR_SETTLEMENT)
            .amountInAccountCurrency(DEFAULT_AMOUNT_IN_ACCOUNT_CURRENCY)
            .amountInLocalCurrency(DEFAULT_AMOUNT_IN_LOCAL_CURRENCY)
            .rate(DEFAULT_RATE)
            .entryCategory(DEFAULT_ENTRY_CATEGORY)
            .description1(DEFAULT_DESCRIPTION_1)
            .description2(DEFAULT_DESCRIPTION_2)
            .createdOn(DEFAULT_CREATED_ON)
            .createdBy(DEFAULT_CREATED_BY)
            .entryStatus(DEFAULT_ENTRY_STATUS)
            .glEntryType(DEFAULT_GL_ENTRY_TYPE);
        return subLedger;
    }

    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static SubLedger createUpdatedEntity(EntityManager em) {
        SubLedger subLedger = new SubLedger()
            .subLedgerRefNo(UPDATED_SUB_LEDGER_REF_NO)
            .accountNo(UPDATED_ACCOUNT_NO)
            .txnDate(UPDATED_TXN_DATE)
            .txnDateInUTC(UPDATED_TXN_DATE_IN_UTC)
            .txnDateInLocal(UPDATED_TXN_DATE_IN_LOCAL)
            .txnDateForSettlement(UPDATED_TXN_DATE_FOR_SETTLEMENT)
            .amountInAccountCurrency(UPDATED_AMOUNT_IN_ACCOUNT_CURRENCY)
            .amountInLocalCurrency(UPDATED_AMOUNT_IN_LOCAL_CURRENCY)
            .rate(UPDATED_RATE)
            .entryCategory(UPDATED_ENTRY_CATEGORY)
            .description1(UPDATED_DESCRIPTION_1)
            .description2(UPDATED_DESCRIPTION_2)
            .createdOn(UPDATED_CREATED_ON)
            .createdBy(UPDATED_CREATED_BY)
            .entryStatus(UPDATED_ENTRY_STATUS)
            .glEntryType(UPDATED_GL_ENTRY_TYPE);
        return subLedger;
    }

    @BeforeEach
    public void initTest() {
        subLedger = createEntity(em);
    }

    @Test
    @Transactional
    void createSubLedger() throws Exception {
        int databaseSizeBeforeCreate = subLedgerRepository.findAll().size();
        // Create the SubLedger
        restSubLedgerMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(subLedger)))
            .andExpect(status().isCreated());

        // Validate the SubLedger in the database
        List<SubLedger> subLedgerList = subLedgerRepository.findAll();
        assertThat(subLedgerList).hasSize(databaseSizeBeforeCreate + 1);
        SubLedger testSubLedger = subLedgerList.get(subLedgerList.size() - 1);
        assertThat(testSubLedger.getSubLedgerRefNo()).isEqualTo(DEFAULT_SUB_LEDGER_REF_NO);
        assertThat(testSubLedger.getAccountNo()).isEqualTo(DEFAULT_ACCOUNT_NO);
        assertThat(testSubLedger.getTxnDate()).isEqualTo(DEFAULT_TXN_DATE);
        assertThat(testSubLedger.getTxnDateInUTC()).isEqualTo(DEFAULT_TXN_DATE_IN_UTC);
        assertThat(testSubLedger.getTxnDateInLocal()).isEqualTo(DEFAULT_TXN_DATE_IN_LOCAL);
        assertThat(testSubLedger.getTxnDateForSettlement()).isEqualTo(DEFAULT_TXN_DATE_FOR_SETTLEMENT);
        assertThat(testSubLedger.getAmountInAccountCurrency()).isEqualByComparingTo(DEFAULT_AMOUNT_IN_ACCOUNT_CURRENCY);
        assertThat(testSubLedger.getAmountInLocalCurrency()).isEqualByComparingTo(DEFAULT_AMOUNT_IN_LOCAL_CURRENCY);
        assertThat(testSubLedger.getRate()).isEqualByComparingTo(DEFAULT_RATE);
        assertThat(testSubLedger.getEntryCategory()).isEqualTo(DEFAULT_ENTRY_CATEGORY);
        assertThat(testSubLedger.getDescription1()).isEqualTo(DEFAULT_DESCRIPTION_1);
        assertThat(testSubLedger.getDescription2()).isEqualTo(DEFAULT_DESCRIPTION_2);
        assertThat(testSubLedger.getCreatedOn()).isEqualTo(DEFAULT_CREATED_ON);
        assertThat(testSubLedger.getCreatedBy()).isEqualTo(DEFAULT_CREATED_BY);
        assertThat(testSubLedger.getEntryStatus()).isEqualTo(DEFAULT_ENTRY_STATUS);
        assertThat(testSubLedger.getGlEntryType()).isEqualTo(DEFAULT_GL_ENTRY_TYPE);
    }

    @Test
    @Transactional
    void createSubLedgerWithExistingId() throws Exception {
        // Create the SubLedger with an existing ID
        subLedger.setId(1L);

        int databaseSizeBeforeCreate = subLedgerRepository.findAll().size();

        // An entity with an existing ID cannot be created, so this API call must fail
        restSubLedgerMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(subLedger)))
            .andExpect(status().isBadRequest());

        // Validate the SubLedger in the database
        List<SubLedger> subLedgerList = subLedgerRepository.findAll();
        assertThat(subLedgerList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    void getAllSubLedgers() throws Exception {
        // Initialize the database
        subLedgerRepository.saveAndFlush(subLedger);

        // Get all the subLedgerList
        restSubLedgerMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(subLedger.getId().intValue())))
            .andExpect(jsonPath("$.[*].subLedgerRefNo").value(hasItem(DEFAULT_SUB_LEDGER_REF_NO)))
            .andExpect(jsonPath("$.[*].accountNo").value(hasItem(DEFAULT_ACCOUNT_NO)))
            .andExpect(jsonPath("$.[*].txnDate").value(hasItem(DEFAULT_TXN_DATE.toString())))
            .andExpect(jsonPath("$.[*].txnDateInUTC").value(hasItem(DEFAULT_TXN_DATE_IN_UTC.toString())))
            .andExpect(jsonPath("$.[*].txnDateInLocal").value(hasItem(sameInstant(DEFAULT_TXN_DATE_IN_LOCAL))))
            .andExpect(jsonPath("$.[*].txnDateForSettlement").value(hasItem(sameInstant(DEFAULT_TXN_DATE_FOR_SETTLEMENT))))
            .andExpect(jsonPath("$.[*].amountInAccountCurrency").value(hasItem(sameNumber(DEFAULT_AMOUNT_IN_ACCOUNT_CURRENCY))))
            .andExpect(jsonPath("$.[*].amountInLocalCurrency").value(hasItem(sameNumber(DEFAULT_AMOUNT_IN_LOCAL_CURRENCY))))
            .andExpect(jsonPath("$.[*].rate").value(hasItem(sameNumber(DEFAULT_RATE))))
            .andExpect(jsonPath("$.[*].entryCategory").value(hasItem(DEFAULT_ENTRY_CATEGORY)))
            .andExpect(jsonPath("$.[*].description1").value(hasItem(DEFAULT_DESCRIPTION_1)))
            .andExpect(jsonPath("$.[*].description2").value(hasItem(DEFAULT_DESCRIPTION_2)))
            .andExpect(jsonPath("$.[*].createdOn").value(hasItem(DEFAULT_CREATED_ON.toString())))
            .andExpect(jsonPath("$.[*].createdBy").value(hasItem(DEFAULT_CREATED_BY)))
            .andExpect(jsonPath("$.[*].entryStatus").value(hasItem(DEFAULT_ENTRY_STATUS.toString())))
            .andExpect(jsonPath("$.[*].glEntryType").value(hasItem(DEFAULT_GL_ENTRY_TYPE.toString())));
    }

    @Test
    @Transactional
    void getSubLedger() throws Exception {
        // Initialize the database
        subLedgerRepository.saveAndFlush(subLedger);

        // Get the subLedger
        restSubLedgerMockMvc
            .perform(get(ENTITY_API_URL_ID, subLedger.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(subLedger.getId().intValue()))
            .andExpect(jsonPath("$.subLedgerRefNo").value(DEFAULT_SUB_LEDGER_REF_NO))
            .andExpect(jsonPath("$.accountNo").value(DEFAULT_ACCOUNT_NO))
            .andExpect(jsonPath("$.txnDate").value(DEFAULT_TXN_DATE.toString()))
            .andExpect(jsonPath("$.txnDateInUTC").value(DEFAULT_TXN_DATE_IN_UTC.toString()))
            .andExpect(jsonPath("$.txnDateInLocal").value(sameInstant(DEFAULT_TXN_DATE_IN_LOCAL)))
            .andExpect(jsonPath("$.txnDateForSettlement").value(sameInstant(DEFAULT_TXN_DATE_FOR_SETTLEMENT)))
            .andExpect(jsonPath("$.amountInAccountCurrency").value(sameNumber(DEFAULT_AMOUNT_IN_ACCOUNT_CURRENCY)))
            .andExpect(jsonPath("$.amountInLocalCurrency").value(sameNumber(DEFAULT_AMOUNT_IN_LOCAL_CURRENCY)))
            .andExpect(jsonPath("$.rate").value(sameNumber(DEFAULT_RATE)))
            .andExpect(jsonPath("$.entryCategory").value(DEFAULT_ENTRY_CATEGORY))
            .andExpect(jsonPath("$.description1").value(DEFAULT_DESCRIPTION_1))
            .andExpect(jsonPath("$.description2").value(DEFAULT_DESCRIPTION_2))
            .andExpect(jsonPath("$.createdOn").value(DEFAULT_CREATED_ON.toString()))
            .andExpect(jsonPath("$.createdBy").value(DEFAULT_CREATED_BY))
            .andExpect(jsonPath("$.entryStatus").value(DEFAULT_ENTRY_STATUS.toString()))
            .andExpect(jsonPath("$.glEntryType").value(DEFAULT_GL_ENTRY_TYPE.toString()));
    }

    @Test
    @Transactional
    void getNonExistingSubLedger() throws Exception {
        // Get the subLedger
        restSubLedgerMockMvc.perform(get(ENTITY_API_URL_ID, Long.MAX_VALUE)).andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    void putNewSubLedger() throws Exception {
        // Initialize the database
        subLedgerRepository.saveAndFlush(subLedger);

        int databaseSizeBeforeUpdate = subLedgerRepository.findAll().size();

        // Update the subLedger
        SubLedger updatedSubLedger = subLedgerRepository.findById(subLedger.getId()).get();
        // Disconnect from session so that the updates on updatedSubLedger are not directly saved in db
        em.detach(updatedSubLedger);
        updatedSubLedger
            .subLedgerRefNo(UPDATED_SUB_LEDGER_REF_NO)
            .accountNo(UPDATED_ACCOUNT_NO)
            .txnDate(UPDATED_TXN_DATE)
            .txnDateInUTC(UPDATED_TXN_DATE_IN_UTC)
            .txnDateInLocal(UPDATED_TXN_DATE_IN_LOCAL)
            .txnDateForSettlement(UPDATED_TXN_DATE_FOR_SETTLEMENT)
            .amountInAccountCurrency(UPDATED_AMOUNT_IN_ACCOUNT_CURRENCY)
            .amountInLocalCurrency(UPDATED_AMOUNT_IN_LOCAL_CURRENCY)
            .rate(UPDATED_RATE)
            .entryCategory(UPDATED_ENTRY_CATEGORY)
            .description1(UPDATED_DESCRIPTION_1)
            .description2(UPDATED_DESCRIPTION_2)
            .createdOn(UPDATED_CREATED_ON)
            .createdBy(UPDATED_CREATED_BY)
            .entryStatus(UPDATED_ENTRY_STATUS)
            .glEntryType(UPDATED_GL_ENTRY_TYPE);

        restSubLedgerMockMvc
            .perform(
                put(ENTITY_API_URL_ID, updatedSubLedger.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(updatedSubLedger))
            )
            .andExpect(status().isOk());

        // Validate the SubLedger in the database
        List<SubLedger> subLedgerList = subLedgerRepository.findAll();
        assertThat(subLedgerList).hasSize(databaseSizeBeforeUpdate);
        SubLedger testSubLedger = subLedgerList.get(subLedgerList.size() - 1);
        assertThat(testSubLedger.getSubLedgerRefNo()).isEqualTo(UPDATED_SUB_LEDGER_REF_NO);
        assertThat(testSubLedger.getAccountNo()).isEqualTo(UPDATED_ACCOUNT_NO);
        assertThat(testSubLedger.getTxnDate()).isEqualTo(UPDATED_TXN_DATE);
        assertThat(testSubLedger.getTxnDateInUTC()).isEqualTo(UPDATED_TXN_DATE_IN_UTC);
        assertThat(testSubLedger.getTxnDateInLocal()).isEqualTo(UPDATED_TXN_DATE_IN_LOCAL);
        assertThat(testSubLedger.getTxnDateForSettlement()).isEqualTo(UPDATED_TXN_DATE_FOR_SETTLEMENT);
        assertThat(testSubLedger.getAmountInAccountCurrency()).isEqualTo(UPDATED_AMOUNT_IN_ACCOUNT_CURRENCY);
        assertThat(testSubLedger.getAmountInLocalCurrency()).isEqualTo(UPDATED_AMOUNT_IN_LOCAL_CURRENCY);
        assertThat(testSubLedger.getRate()).isEqualTo(UPDATED_RATE);
        assertThat(testSubLedger.getEntryCategory()).isEqualTo(UPDATED_ENTRY_CATEGORY);
        assertThat(testSubLedger.getDescription1()).isEqualTo(UPDATED_DESCRIPTION_1);
        assertThat(testSubLedger.getDescription2()).isEqualTo(UPDATED_DESCRIPTION_2);
        assertThat(testSubLedger.getCreatedOn()).isEqualTo(UPDATED_CREATED_ON);
        assertThat(testSubLedger.getCreatedBy()).isEqualTo(UPDATED_CREATED_BY);
        assertThat(testSubLedger.getEntryStatus()).isEqualTo(UPDATED_ENTRY_STATUS);
        assertThat(testSubLedger.getGlEntryType()).isEqualTo(UPDATED_GL_ENTRY_TYPE);
    }

    @Test
    @Transactional
    void putNonExistingSubLedger() throws Exception {
        int databaseSizeBeforeUpdate = subLedgerRepository.findAll().size();
        subLedger.setId(count.incrementAndGet());

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restSubLedgerMockMvc
            .perform(
                put(ENTITY_API_URL_ID, subLedger.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(subLedger))
            )
            .andExpect(status().isBadRequest());

        // Validate the SubLedger in the database
        List<SubLedger> subLedgerList = subLedgerRepository.findAll();
        assertThat(subLedgerList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithIdMismatchSubLedger() throws Exception {
        int databaseSizeBeforeUpdate = subLedgerRepository.findAll().size();
        subLedger.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restSubLedgerMockMvc
            .perform(
                put(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(subLedger))
            )
            .andExpect(status().isBadRequest());

        // Validate the SubLedger in the database
        List<SubLedger> subLedgerList = subLedgerRepository.findAll();
        assertThat(subLedgerList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithMissingIdPathParamSubLedger() throws Exception {
        int databaseSizeBeforeUpdate = subLedgerRepository.findAll().size();
        subLedger.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restSubLedgerMockMvc
            .perform(put(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(subLedger)))
            .andExpect(status().isMethodNotAllowed());

        // Validate the SubLedger in the database
        List<SubLedger> subLedgerList = subLedgerRepository.findAll();
        assertThat(subLedgerList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void partialUpdateSubLedgerWithPatch() throws Exception {
        // Initialize the database
        subLedgerRepository.saveAndFlush(subLedger);

        int databaseSizeBeforeUpdate = subLedgerRepository.findAll().size();

        // Update the subLedger using partial update
        SubLedger partialUpdatedSubLedger = new SubLedger();
        partialUpdatedSubLedger.setId(subLedger.getId());

        partialUpdatedSubLedger
            .txnDate(UPDATED_TXN_DATE)
            .amountInAccountCurrency(UPDATED_AMOUNT_IN_ACCOUNT_CURRENCY)
            .entryCategory(UPDATED_ENTRY_CATEGORY)
            .createdOn(UPDATED_CREATED_ON)
            .createdBy(UPDATED_CREATED_BY);

        restSubLedgerMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedSubLedger.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedSubLedger))
            )
            .andExpect(status().isOk());

        // Validate the SubLedger in the database
        List<SubLedger> subLedgerList = subLedgerRepository.findAll();
        assertThat(subLedgerList).hasSize(databaseSizeBeforeUpdate);
        SubLedger testSubLedger = subLedgerList.get(subLedgerList.size() - 1);
        assertThat(testSubLedger.getSubLedgerRefNo()).isEqualTo(DEFAULT_SUB_LEDGER_REF_NO);
        assertThat(testSubLedger.getAccountNo()).isEqualTo(DEFAULT_ACCOUNT_NO);
        assertThat(testSubLedger.getTxnDate()).isEqualTo(UPDATED_TXN_DATE);
        assertThat(testSubLedger.getTxnDateInUTC()).isEqualTo(DEFAULT_TXN_DATE_IN_UTC);
        assertThat(testSubLedger.getTxnDateInLocal()).isEqualTo(DEFAULT_TXN_DATE_IN_LOCAL);
        assertThat(testSubLedger.getTxnDateForSettlement()).isEqualTo(DEFAULT_TXN_DATE_FOR_SETTLEMENT);
        assertThat(testSubLedger.getAmountInAccountCurrency()).isEqualByComparingTo(UPDATED_AMOUNT_IN_ACCOUNT_CURRENCY);
        assertThat(testSubLedger.getAmountInLocalCurrency()).isEqualByComparingTo(DEFAULT_AMOUNT_IN_LOCAL_CURRENCY);
        assertThat(testSubLedger.getRate()).isEqualByComparingTo(DEFAULT_RATE);
        assertThat(testSubLedger.getEntryCategory()).isEqualTo(UPDATED_ENTRY_CATEGORY);
        assertThat(testSubLedger.getDescription1()).isEqualTo(DEFAULT_DESCRIPTION_1);
        assertThat(testSubLedger.getDescription2()).isEqualTo(DEFAULT_DESCRIPTION_2);
        assertThat(testSubLedger.getCreatedOn()).isEqualTo(UPDATED_CREATED_ON);
        assertThat(testSubLedger.getCreatedBy()).isEqualTo(UPDATED_CREATED_BY);
        assertThat(testSubLedger.getEntryStatus()).isEqualTo(DEFAULT_ENTRY_STATUS);
        assertThat(testSubLedger.getGlEntryType()).isEqualTo(DEFAULT_GL_ENTRY_TYPE);
    }

    @Test
    @Transactional
    void fullUpdateSubLedgerWithPatch() throws Exception {
        // Initialize the database
        subLedgerRepository.saveAndFlush(subLedger);

        int databaseSizeBeforeUpdate = subLedgerRepository.findAll().size();

        // Update the subLedger using partial update
        SubLedger partialUpdatedSubLedger = new SubLedger();
        partialUpdatedSubLedger.setId(subLedger.getId());

        partialUpdatedSubLedger
            .subLedgerRefNo(UPDATED_SUB_LEDGER_REF_NO)
            .accountNo(UPDATED_ACCOUNT_NO)
            .txnDate(UPDATED_TXN_DATE)
            .txnDateInUTC(UPDATED_TXN_DATE_IN_UTC)
            .txnDateInLocal(UPDATED_TXN_DATE_IN_LOCAL)
            .txnDateForSettlement(UPDATED_TXN_DATE_FOR_SETTLEMENT)
            .amountInAccountCurrency(UPDATED_AMOUNT_IN_ACCOUNT_CURRENCY)
            .amountInLocalCurrency(UPDATED_AMOUNT_IN_LOCAL_CURRENCY)
            .rate(UPDATED_RATE)
            .entryCategory(UPDATED_ENTRY_CATEGORY)
            .description1(UPDATED_DESCRIPTION_1)
            .description2(UPDATED_DESCRIPTION_2)
            .createdOn(UPDATED_CREATED_ON)
            .createdBy(UPDATED_CREATED_BY)
            .entryStatus(UPDATED_ENTRY_STATUS)
            .glEntryType(UPDATED_GL_ENTRY_TYPE);

        restSubLedgerMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedSubLedger.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedSubLedger))
            )
            .andExpect(status().isOk());

        // Validate the SubLedger in the database
        List<SubLedger> subLedgerList = subLedgerRepository.findAll();
        assertThat(subLedgerList).hasSize(databaseSizeBeforeUpdate);
        SubLedger testSubLedger = subLedgerList.get(subLedgerList.size() - 1);
        assertThat(testSubLedger.getSubLedgerRefNo()).isEqualTo(UPDATED_SUB_LEDGER_REF_NO);
        assertThat(testSubLedger.getAccountNo()).isEqualTo(UPDATED_ACCOUNT_NO);
        assertThat(testSubLedger.getTxnDate()).isEqualTo(UPDATED_TXN_DATE);
        assertThat(testSubLedger.getTxnDateInUTC()).isEqualTo(UPDATED_TXN_DATE_IN_UTC);
        assertThat(testSubLedger.getTxnDateInLocal()).isEqualTo(UPDATED_TXN_DATE_IN_LOCAL);
        assertThat(testSubLedger.getTxnDateForSettlement()).isEqualTo(UPDATED_TXN_DATE_FOR_SETTLEMENT);
        assertThat(testSubLedger.getAmountInAccountCurrency()).isEqualByComparingTo(UPDATED_AMOUNT_IN_ACCOUNT_CURRENCY);
        assertThat(testSubLedger.getAmountInLocalCurrency()).isEqualByComparingTo(UPDATED_AMOUNT_IN_LOCAL_CURRENCY);
        assertThat(testSubLedger.getRate()).isEqualByComparingTo(UPDATED_RATE);
        assertThat(testSubLedger.getEntryCategory()).isEqualTo(UPDATED_ENTRY_CATEGORY);
        assertThat(testSubLedger.getDescription1()).isEqualTo(UPDATED_DESCRIPTION_1);
        assertThat(testSubLedger.getDescription2()).isEqualTo(UPDATED_DESCRIPTION_2);
        assertThat(testSubLedger.getCreatedOn()).isEqualTo(UPDATED_CREATED_ON);
        assertThat(testSubLedger.getCreatedBy()).isEqualTo(UPDATED_CREATED_BY);
        assertThat(testSubLedger.getEntryStatus()).isEqualTo(UPDATED_ENTRY_STATUS);
        assertThat(testSubLedger.getGlEntryType()).isEqualTo(UPDATED_GL_ENTRY_TYPE);
    }

    @Test
    @Transactional
    void patchNonExistingSubLedger() throws Exception {
        int databaseSizeBeforeUpdate = subLedgerRepository.findAll().size();
        subLedger.setId(count.incrementAndGet());

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restSubLedgerMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, subLedger.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(subLedger))
            )
            .andExpect(status().isBadRequest());

        // Validate the SubLedger in the database
        List<SubLedger> subLedgerList = subLedgerRepository.findAll();
        assertThat(subLedgerList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithIdMismatchSubLedger() throws Exception {
        int databaseSizeBeforeUpdate = subLedgerRepository.findAll().size();
        subLedger.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restSubLedgerMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(subLedger))
            )
            .andExpect(status().isBadRequest());

        // Validate the SubLedger in the database
        List<SubLedger> subLedgerList = subLedgerRepository.findAll();
        assertThat(subLedgerList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithMissingIdPathParamSubLedger() throws Exception {
        int databaseSizeBeforeUpdate = subLedgerRepository.findAll().size();
        subLedger.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restSubLedgerMockMvc
            .perform(
                patch(ENTITY_API_URL).contentType("application/merge-patch+json").content(TestUtil.convertObjectToJsonBytes(subLedger))
            )
            .andExpect(status().isMethodNotAllowed());

        // Validate the SubLedger in the database
        List<SubLedger> subLedgerList = subLedgerRepository.findAll();
        assertThat(subLedgerList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void deleteSubLedger() throws Exception {
        // Initialize the database
        subLedgerRepository.saveAndFlush(subLedger);

        int databaseSizeBeforeDelete = subLedgerRepository.findAll().size();

        // Delete the subLedger
        restSubLedgerMockMvc
            .perform(delete(ENTITY_API_URL_ID, subLedger.getId()).accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<SubLedger> subLedgerList = subLedgerRepository.findAll();
        assertThat(subLedgerList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
