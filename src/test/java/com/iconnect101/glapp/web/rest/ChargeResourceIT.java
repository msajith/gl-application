package com.iconnect101.glapp.web.rest;

import static com.iconnect101.glapp.web.rest.TestUtil.sameNumber;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import com.iconnect101.glapp.IntegrationTest;
import com.iconnect101.glapp.domain.Charge;
import com.iconnect101.glapp.domain.enumeration.RecordStatus;
import com.iconnect101.glapp.repository.ChargeRepository;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.List;
import java.util.Random;
import java.util.concurrent.atomic.AtomicLong;
import javax.persistence.EntityManager;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;

/**
 * Integration tests for the {@link ChargeResource} REST controller.
 */
@IntegrationTest
@AutoConfigureMockMvc
@WithMockUser
class ChargeResourceIT {

    private static final BigDecimal DEFAULT_CHARGE_AMOUNT_LOCAL_CCY = new BigDecimal(1);
    private static final BigDecimal UPDATED_CHARGE_AMOUNT_LOCAL_CCY = new BigDecimal(2);

    private static final BigDecimal DEFAULT_CHARGE_AMOUNT_TXN_CCY = new BigDecimal(1);
    private static final BigDecimal UPDATED_CHARGE_AMOUNT_TXN_CCY = new BigDecimal(2);

    private static final String DEFAULT_AMOUNT_SLAB = "AAAAAAAAAA";
    private static final String UPDATED_AMOUNT_SLAB = "BBBBBBBBBB";

    private static final BigDecimal DEFAULT_AMOUNT_SLAB_VALUE = new BigDecimal(1);
    private static final BigDecimal UPDATED_AMOUNT_SLAB_VALUE = new BigDecimal(2);

    private static final String DEFAULT_AMOUNT_SLAB_OPERATOR = "AAAAAAAAAA";
    private static final String UPDATED_AMOUNT_SLAB_OPERATOR = "BBBBBBBBBB";

    private static final String DEFAULT_AMOUNT_SLAB_CURRENCY = "AAAAAAAAAA";
    private static final String UPDATED_AMOUNT_SLAB_CURRENCY = "BBBBBBBBBB";

    private static final RecordStatus DEFAULT_RECORD_STATUS = RecordStatus.ACTIVE;
    private static final RecordStatus UPDATED_RECORD_STATUS = RecordStatus.INACTIVE;

    private static final LocalDate DEFAULT_VALIDFROM = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_VALIDFROM = LocalDate.now(ZoneId.systemDefault());

    private static final LocalDate DEFAULT_VALID_TO = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_VALID_TO = LocalDate.now(ZoneId.systemDefault());

    private static final String ENTITY_API_URL = "/api/charges";
    private static final String ENTITY_API_URL_ID = ENTITY_API_URL + "/{id}";

    private static Random random = new Random();
    private static AtomicLong count = new AtomicLong(random.nextInt() + (2 * Integer.MAX_VALUE));

    @Autowired
    private ChargeRepository chargeRepository;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restChargeMockMvc;

    private Charge charge;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Charge createEntity(EntityManager em) {
        Charge charge = new Charge()
            .chargeAmountLocalCcy(DEFAULT_CHARGE_AMOUNT_LOCAL_CCY)
            .chargeAmountTxnCcy(DEFAULT_CHARGE_AMOUNT_TXN_CCY)
            .amountSlab(DEFAULT_AMOUNT_SLAB)
            .amountSlabValue(DEFAULT_AMOUNT_SLAB_VALUE)
            .amountSlabOperator(DEFAULT_AMOUNT_SLAB_OPERATOR)
            .amountSlabCurrency(DEFAULT_AMOUNT_SLAB_CURRENCY)
            .recordStatus(DEFAULT_RECORD_STATUS)
            .validfrom(DEFAULT_VALIDFROM)
            .validTo(DEFAULT_VALID_TO);
        return charge;
    }

    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Charge createUpdatedEntity(EntityManager em) {
        Charge charge = new Charge()
            .chargeAmountLocalCcy(UPDATED_CHARGE_AMOUNT_LOCAL_CCY)
            .chargeAmountTxnCcy(UPDATED_CHARGE_AMOUNT_TXN_CCY)
            .amountSlab(UPDATED_AMOUNT_SLAB)
            .amountSlabValue(UPDATED_AMOUNT_SLAB_VALUE)
            .amountSlabOperator(UPDATED_AMOUNT_SLAB_OPERATOR)
            .amountSlabCurrency(UPDATED_AMOUNT_SLAB_CURRENCY)
            .recordStatus(UPDATED_RECORD_STATUS)
            .validfrom(UPDATED_VALIDFROM)
            .validTo(UPDATED_VALID_TO);
        return charge;
    }

    @BeforeEach
    public void initTest() {
        charge = createEntity(em);
    }

    @Test
    @Transactional
    void createCharge() throws Exception {
        int databaseSizeBeforeCreate = chargeRepository.findAll().size();
        // Create the Charge
        restChargeMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(charge)))
            .andExpect(status().isCreated());

        // Validate the Charge in the database
        List<Charge> chargeList = chargeRepository.findAll();
        assertThat(chargeList).hasSize(databaseSizeBeforeCreate + 1);
        Charge testCharge = chargeList.get(chargeList.size() - 1);
        assertThat(testCharge.getChargeAmountLocalCcy()).isEqualByComparingTo(DEFAULT_CHARGE_AMOUNT_LOCAL_CCY);
        assertThat(testCharge.getChargeAmountTxnCcy()).isEqualByComparingTo(DEFAULT_CHARGE_AMOUNT_TXN_CCY);
        assertThat(testCharge.getAmountSlab()).isEqualTo(DEFAULT_AMOUNT_SLAB);
        assertThat(testCharge.getAmountSlabValue()).isEqualByComparingTo(DEFAULT_AMOUNT_SLAB_VALUE);
        assertThat(testCharge.getAmountSlabOperator()).isEqualTo(DEFAULT_AMOUNT_SLAB_OPERATOR);
        assertThat(testCharge.getAmountSlabCurrency()).isEqualTo(DEFAULT_AMOUNT_SLAB_CURRENCY);
        assertThat(testCharge.getRecordStatus()).isEqualTo(DEFAULT_RECORD_STATUS);
        assertThat(testCharge.getValidfrom()).isEqualTo(DEFAULT_VALIDFROM);
        assertThat(testCharge.getValidTo()).isEqualTo(DEFAULT_VALID_TO);
    }

    @Test
    @Transactional
    void createChargeWithExistingId() throws Exception {
        // Create the Charge with an existing ID
        charge.setId(1L);

        int databaseSizeBeforeCreate = chargeRepository.findAll().size();

        // An entity with an existing ID cannot be created, so this API call must fail
        restChargeMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(charge)))
            .andExpect(status().isBadRequest());

        // Validate the Charge in the database
        List<Charge> chargeList = chargeRepository.findAll();
        assertThat(chargeList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    void getAllCharges() throws Exception {
        // Initialize the database
        chargeRepository.saveAndFlush(charge);

        // Get all the chargeList
        restChargeMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(charge.getId().intValue())))
            .andExpect(jsonPath("$.[*].chargeAmountLocalCcy").value(hasItem(sameNumber(DEFAULT_CHARGE_AMOUNT_LOCAL_CCY))))
            .andExpect(jsonPath("$.[*].chargeAmountTxnCcy").value(hasItem(sameNumber(DEFAULT_CHARGE_AMOUNT_TXN_CCY))))
            .andExpect(jsonPath("$.[*].amountSlab").value(hasItem(DEFAULT_AMOUNT_SLAB)))
            .andExpect(jsonPath("$.[*].amountSlabValue").value(hasItem(sameNumber(DEFAULT_AMOUNT_SLAB_VALUE))))
            .andExpect(jsonPath("$.[*].amountSlabOperator").value(hasItem(DEFAULT_AMOUNT_SLAB_OPERATOR)))
            .andExpect(jsonPath("$.[*].amountSlabCurrency").value(hasItem(DEFAULT_AMOUNT_SLAB_CURRENCY)))
            .andExpect(jsonPath("$.[*].recordStatus").value(hasItem(DEFAULT_RECORD_STATUS.toString())))
            .andExpect(jsonPath("$.[*].validfrom").value(hasItem(DEFAULT_VALIDFROM.toString())))
            .andExpect(jsonPath("$.[*].validTo").value(hasItem(DEFAULT_VALID_TO.toString())));
    }

    @Test
    @Transactional
    void getCharge() throws Exception {
        // Initialize the database
        chargeRepository.saveAndFlush(charge);

        // Get the charge
        restChargeMockMvc
            .perform(get(ENTITY_API_URL_ID, charge.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(charge.getId().intValue()))
            .andExpect(jsonPath("$.chargeAmountLocalCcy").value(sameNumber(DEFAULT_CHARGE_AMOUNT_LOCAL_CCY)))
            .andExpect(jsonPath("$.chargeAmountTxnCcy").value(sameNumber(DEFAULT_CHARGE_AMOUNT_TXN_CCY)))
            .andExpect(jsonPath("$.amountSlab").value(DEFAULT_AMOUNT_SLAB))
            .andExpect(jsonPath("$.amountSlabValue").value(sameNumber(DEFAULT_AMOUNT_SLAB_VALUE)))
            .andExpect(jsonPath("$.amountSlabOperator").value(DEFAULT_AMOUNT_SLAB_OPERATOR))
            .andExpect(jsonPath("$.amountSlabCurrency").value(DEFAULT_AMOUNT_SLAB_CURRENCY))
            .andExpect(jsonPath("$.recordStatus").value(DEFAULT_RECORD_STATUS.toString()))
            .andExpect(jsonPath("$.validfrom").value(DEFAULT_VALIDFROM.toString()))
            .andExpect(jsonPath("$.validTo").value(DEFAULT_VALID_TO.toString()));
    }

    @Test
    @Transactional
    void getNonExistingCharge() throws Exception {
        // Get the charge
        restChargeMockMvc.perform(get(ENTITY_API_URL_ID, Long.MAX_VALUE)).andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    void putNewCharge() throws Exception {
        // Initialize the database
        chargeRepository.saveAndFlush(charge);

        int databaseSizeBeforeUpdate = chargeRepository.findAll().size();

        // Update the charge
        Charge updatedCharge = chargeRepository.findById(charge.getId()).get();
        // Disconnect from session so that the updates on updatedCharge are not directly saved in db
        em.detach(updatedCharge);
        updatedCharge
            .chargeAmountLocalCcy(UPDATED_CHARGE_AMOUNT_LOCAL_CCY)
            .chargeAmountTxnCcy(UPDATED_CHARGE_AMOUNT_TXN_CCY)
            .amountSlab(UPDATED_AMOUNT_SLAB)
            .amountSlabValue(UPDATED_AMOUNT_SLAB_VALUE)
            .amountSlabOperator(UPDATED_AMOUNT_SLAB_OPERATOR)
            .amountSlabCurrency(UPDATED_AMOUNT_SLAB_CURRENCY)
            .recordStatus(UPDATED_RECORD_STATUS)
            .validfrom(UPDATED_VALIDFROM)
            .validTo(UPDATED_VALID_TO);

        restChargeMockMvc
            .perform(
                put(ENTITY_API_URL_ID, updatedCharge.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(updatedCharge))
            )
            .andExpect(status().isOk());

        // Validate the Charge in the database
        List<Charge> chargeList = chargeRepository.findAll();
        assertThat(chargeList).hasSize(databaseSizeBeforeUpdate);
        Charge testCharge = chargeList.get(chargeList.size() - 1);
        assertThat(testCharge.getChargeAmountLocalCcy()).isEqualTo(UPDATED_CHARGE_AMOUNT_LOCAL_CCY);
        assertThat(testCharge.getChargeAmountTxnCcy()).isEqualTo(UPDATED_CHARGE_AMOUNT_TXN_CCY);
        assertThat(testCharge.getAmountSlab()).isEqualTo(UPDATED_AMOUNT_SLAB);
        assertThat(testCharge.getAmountSlabValue()).isEqualTo(UPDATED_AMOUNT_SLAB_VALUE);
        assertThat(testCharge.getAmountSlabOperator()).isEqualTo(UPDATED_AMOUNT_SLAB_OPERATOR);
        assertThat(testCharge.getAmountSlabCurrency()).isEqualTo(UPDATED_AMOUNT_SLAB_CURRENCY);
        assertThat(testCharge.getRecordStatus()).isEqualTo(UPDATED_RECORD_STATUS);
        assertThat(testCharge.getValidfrom()).isEqualTo(UPDATED_VALIDFROM);
        assertThat(testCharge.getValidTo()).isEqualTo(UPDATED_VALID_TO);
    }

    @Test
    @Transactional
    void putNonExistingCharge() throws Exception {
        int databaseSizeBeforeUpdate = chargeRepository.findAll().size();
        charge.setId(count.incrementAndGet());

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restChargeMockMvc
            .perform(
                put(ENTITY_API_URL_ID, charge.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(charge))
            )
            .andExpect(status().isBadRequest());

        // Validate the Charge in the database
        List<Charge> chargeList = chargeRepository.findAll();
        assertThat(chargeList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithIdMismatchCharge() throws Exception {
        int databaseSizeBeforeUpdate = chargeRepository.findAll().size();
        charge.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restChargeMockMvc
            .perform(
                put(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(charge))
            )
            .andExpect(status().isBadRequest());

        // Validate the Charge in the database
        List<Charge> chargeList = chargeRepository.findAll();
        assertThat(chargeList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithMissingIdPathParamCharge() throws Exception {
        int databaseSizeBeforeUpdate = chargeRepository.findAll().size();
        charge.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restChargeMockMvc
            .perform(put(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(charge)))
            .andExpect(status().isMethodNotAllowed());

        // Validate the Charge in the database
        List<Charge> chargeList = chargeRepository.findAll();
        assertThat(chargeList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void partialUpdateChargeWithPatch() throws Exception {
        // Initialize the database
        chargeRepository.saveAndFlush(charge);

        int databaseSizeBeforeUpdate = chargeRepository.findAll().size();

        // Update the charge using partial update
        Charge partialUpdatedCharge = new Charge();
        partialUpdatedCharge.setId(charge.getId());

        partialUpdatedCharge
            .amountSlab(UPDATED_AMOUNT_SLAB)
            .amountSlabOperator(UPDATED_AMOUNT_SLAB_OPERATOR)
            .amountSlabCurrency(UPDATED_AMOUNT_SLAB_CURRENCY)
            .validTo(UPDATED_VALID_TO);

        restChargeMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedCharge.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedCharge))
            )
            .andExpect(status().isOk());

        // Validate the Charge in the database
        List<Charge> chargeList = chargeRepository.findAll();
        assertThat(chargeList).hasSize(databaseSizeBeforeUpdate);
        Charge testCharge = chargeList.get(chargeList.size() - 1);
        assertThat(testCharge.getChargeAmountLocalCcy()).isEqualByComparingTo(DEFAULT_CHARGE_AMOUNT_LOCAL_CCY);
        assertThat(testCharge.getChargeAmountTxnCcy()).isEqualByComparingTo(DEFAULT_CHARGE_AMOUNT_TXN_CCY);
        assertThat(testCharge.getAmountSlab()).isEqualTo(UPDATED_AMOUNT_SLAB);
        assertThat(testCharge.getAmountSlabValue()).isEqualByComparingTo(DEFAULT_AMOUNT_SLAB_VALUE);
        assertThat(testCharge.getAmountSlabOperator()).isEqualTo(UPDATED_AMOUNT_SLAB_OPERATOR);
        assertThat(testCharge.getAmountSlabCurrency()).isEqualTo(UPDATED_AMOUNT_SLAB_CURRENCY);
        assertThat(testCharge.getRecordStatus()).isEqualTo(DEFAULT_RECORD_STATUS);
        assertThat(testCharge.getValidfrom()).isEqualTo(DEFAULT_VALIDFROM);
        assertThat(testCharge.getValidTo()).isEqualTo(UPDATED_VALID_TO);
    }

    @Test
    @Transactional
    void fullUpdateChargeWithPatch() throws Exception {
        // Initialize the database
        chargeRepository.saveAndFlush(charge);

        int databaseSizeBeforeUpdate = chargeRepository.findAll().size();

        // Update the charge using partial update
        Charge partialUpdatedCharge = new Charge();
        partialUpdatedCharge.setId(charge.getId());

        partialUpdatedCharge
            .chargeAmountLocalCcy(UPDATED_CHARGE_AMOUNT_LOCAL_CCY)
            .chargeAmountTxnCcy(UPDATED_CHARGE_AMOUNT_TXN_CCY)
            .amountSlab(UPDATED_AMOUNT_SLAB)
            .amountSlabValue(UPDATED_AMOUNT_SLAB_VALUE)
            .amountSlabOperator(UPDATED_AMOUNT_SLAB_OPERATOR)
            .amountSlabCurrency(UPDATED_AMOUNT_SLAB_CURRENCY)
            .recordStatus(UPDATED_RECORD_STATUS)
            .validfrom(UPDATED_VALIDFROM)
            .validTo(UPDATED_VALID_TO);

        restChargeMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedCharge.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedCharge))
            )
            .andExpect(status().isOk());

        // Validate the Charge in the database
        List<Charge> chargeList = chargeRepository.findAll();
        assertThat(chargeList).hasSize(databaseSizeBeforeUpdate);
        Charge testCharge = chargeList.get(chargeList.size() - 1);
        assertThat(testCharge.getChargeAmountLocalCcy()).isEqualByComparingTo(UPDATED_CHARGE_AMOUNT_LOCAL_CCY);
        assertThat(testCharge.getChargeAmountTxnCcy()).isEqualByComparingTo(UPDATED_CHARGE_AMOUNT_TXN_CCY);
        assertThat(testCharge.getAmountSlab()).isEqualTo(UPDATED_AMOUNT_SLAB);
        assertThat(testCharge.getAmountSlabValue()).isEqualByComparingTo(UPDATED_AMOUNT_SLAB_VALUE);
        assertThat(testCharge.getAmountSlabOperator()).isEqualTo(UPDATED_AMOUNT_SLAB_OPERATOR);
        assertThat(testCharge.getAmountSlabCurrency()).isEqualTo(UPDATED_AMOUNT_SLAB_CURRENCY);
        assertThat(testCharge.getRecordStatus()).isEqualTo(UPDATED_RECORD_STATUS);
        assertThat(testCharge.getValidfrom()).isEqualTo(UPDATED_VALIDFROM);
        assertThat(testCharge.getValidTo()).isEqualTo(UPDATED_VALID_TO);
    }

    @Test
    @Transactional
    void patchNonExistingCharge() throws Exception {
        int databaseSizeBeforeUpdate = chargeRepository.findAll().size();
        charge.setId(count.incrementAndGet());

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restChargeMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, charge.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(charge))
            )
            .andExpect(status().isBadRequest());

        // Validate the Charge in the database
        List<Charge> chargeList = chargeRepository.findAll();
        assertThat(chargeList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithIdMismatchCharge() throws Exception {
        int databaseSizeBeforeUpdate = chargeRepository.findAll().size();
        charge.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restChargeMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(charge))
            )
            .andExpect(status().isBadRequest());

        // Validate the Charge in the database
        List<Charge> chargeList = chargeRepository.findAll();
        assertThat(chargeList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithMissingIdPathParamCharge() throws Exception {
        int databaseSizeBeforeUpdate = chargeRepository.findAll().size();
        charge.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restChargeMockMvc
            .perform(patch(ENTITY_API_URL).contentType("application/merge-patch+json").content(TestUtil.convertObjectToJsonBytes(charge)))
            .andExpect(status().isMethodNotAllowed());

        // Validate the Charge in the database
        List<Charge> chargeList = chargeRepository.findAll();
        assertThat(chargeList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void deleteCharge() throws Exception {
        // Initialize the database
        chargeRepository.saveAndFlush(charge);

        int databaseSizeBeforeDelete = chargeRepository.findAll().size();

        // Delete the charge
        restChargeMockMvc
            .perform(delete(ENTITY_API_URL_ID, charge.getId()).accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<Charge> chargeList = chargeRepository.findAll();
        assertThat(chargeList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
