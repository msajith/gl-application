package com.iconnect101.glapp.web.rest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import com.iconnect101.glapp.IntegrationTest;
import com.iconnect101.glapp.domain.FundingSourceType;
import com.iconnect101.glapp.repository.FundingSourceTypeRepository;
import java.util.List;
import java.util.Random;
import java.util.concurrent.atomic.AtomicLong;
import javax.persistence.EntityManager;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;

/**
 * Integration tests for the {@link FundingSourceTypeResource} REST controller.
 */
@IntegrationTest
@AutoConfigureMockMvc
@WithMockUser
class FundingSourceTypeResourceIT {

    private static final String DEFAULT_NAME = "AAAAAAAAAA";
    private static final String UPDATED_NAME = "BBBBBBBBBB";

    private static final String DEFAULT_CODE = "AAAAAAAAAA";
    private static final String UPDATED_CODE = "BBBBBBBBBB";

    private static final String ENTITY_API_URL = "/api/funding-source-types";
    private static final String ENTITY_API_URL_ID = ENTITY_API_URL + "/{id}";

    private static Random random = new Random();
    private static AtomicLong count = new AtomicLong(random.nextInt() + (2 * Integer.MAX_VALUE));

    @Autowired
    private FundingSourceTypeRepository fundingSourceTypeRepository;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restFundingSourceTypeMockMvc;

    private FundingSourceType fundingSourceType;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static FundingSourceType createEntity(EntityManager em) {
        FundingSourceType fundingSourceType = new FundingSourceType().name(DEFAULT_NAME).code(DEFAULT_CODE);
        return fundingSourceType;
    }

    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static FundingSourceType createUpdatedEntity(EntityManager em) {
        FundingSourceType fundingSourceType = new FundingSourceType().name(UPDATED_NAME).code(UPDATED_CODE);
        return fundingSourceType;
    }

    @BeforeEach
    public void initTest() {
        fundingSourceType = createEntity(em);
    }

    @Test
    @Transactional
    void createFundingSourceType() throws Exception {
        int databaseSizeBeforeCreate = fundingSourceTypeRepository.findAll().size();
        // Create the FundingSourceType
        restFundingSourceTypeMockMvc
            .perform(
                post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(fundingSourceType))
            )
            .andExpect(status().isCreated());

        // Validate the FundingSourceType in the database
        List<FundingSourceType> fundingSourceTypeList = fundingSourceTypeRepository.findAll();
        assertThat(fundingSourceTypeList).hasSize(databaseSizeBeforeCreate + 1);
        FundingSourceType testFundingSourceType = fundingSourceTypeList.get(fundingSourceTypeList.size() - 1);
        assertThat(testFundingSourceType.getName()).isEqualTo(DEFAULT_NAME);
        assertThat(testFundingSourceType.getCode()).isEqualTo(DEFAULT_CODE);
    }

    @Test
    @Transactional
    void createFundingSourceTypeWithExistingId() throws Exception {
        // Create the FundingSourceType with an existing ID
        fundingSourceType.setId(1L);

        int databaseSizeBeforeCreate = fundingSourceTypeRepository.findAll().size();

        // An entity with an existing ID cannot be created, so this API call must fail
        restFundingSourceTypeMockMvc
            .perform(
                post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(fundingSourceType))
            )
            .andExpect(status().isBadRequest());

        // Validate the FundingSourceType in the database
        List<FundingSourceType> fundingSourceTypeList = fundingSourceTypeRepository.findAll();
        assertThat(fundingSourceTypeList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    void getAllFundingSourceTypes() throws Exception {
        // Initialize the database
        fundingSourceTypeRepository.saveAndFlush(fundingSourceType);

        // Get all the fundingSourceTypeList
        restFundingSourceTypeMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(fundingSourceType.getId().intValue())))
            .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME)))
            .andExpect(jsonPath("$.[*].code").value(hasItem(DEFAULT_CODE)));
    }

    @Test
    @Transactional
    void getFundingSourceType() throws Exception {
        // Initialize the database
        fundingSourceTypeRepository.saveAndFlush(fundingSourceType);

        // Get the fundingSourceType
        restFundingSourceTypeMockMvc
            .perform(get(ENTITY_API_URL_ID, fundingSourceType.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(fundingSourceType.getId().intValue()))
            .andExpect(jsonPath("$.name").value(DEFAULT_NAME))
            .andExpect(jsonPath("$.code").value(DEFAULT_CODE));
    }

    @Test
    @Transactional
    void getNonExistingFundingSourceType() throws Exception {
        // Get the fundingSourceType
        restFundingSourceTypeMockMvc.perform(get(ENTITY_API_URL_ID, Long.MAX_VALUE)).andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    void putNewFundingSourceType() throws Exception {
        // Initialize the database
        fundingSourceTypeRepository.saveAndFlush(fundingSourceType);

        int databaseSizeBeforeUpdate = fundingSourceTypeRepository.findAll().size();

        // Update the fundingSourceType
        FundingSourceType updatedFundingSourceType = fundingSourceTypeRepository.findById(fundingSourceType.getId()).get();
        // Disconnect from session so that the updates on updatedFundingSourceType are not directly saved in db
        em.detach(updatedFundingSourceType);
        updatedFundingSourceType.name(UPDATED_NAME).code(UPDATED_CODE);

        restFundingSourceTypeMockMvc
            .perform(
                put(ENTITY_API_URL_ID, updatedFundingSourceType.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(updatedFundingSourceType))
            )
            .andExpect(status().isOk());

        // Validate the FundingSourceType in the database
        List<FundingSourceType> fundingSourceTypeList = fundingSourceTypeRepository.findAll();
        assertThat(fundingSourceTypeList).hasSize(databaseSizeBeforeUpdate);
        FundingSourceType testFundingSourceType = fundingSourceTypeList.get(fundingSourceTypeList.size() - 1);
        assertThat(testFundingSourceType.getName()).isEqualTo(UPDATED_NAME);
        assertThat(testFundingSourceType.getCode()).isEqualTo(UPDATED_CODE);
    }

    @Test
    @Transactional
    void putNonExistingFundingSourceType() throws Exception {
        int databaseSizeBeforeUpdate = fundingSourceTypeRepository.findAll().size();
        fundingSourceType.setId(count.incrementAndGet());

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restFundingSourceTypeMockMvc
            .perform(
                put(ENTITY_API_URL_ID, fundingSourceType.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(fundingSourceType))
            )
            .andExpect(status().isBadRequest());

        // Validate the FundingSourceType in the database
        List<FundingSourceType> fundingSourceTypeList = fundingSourceTypeRepository.findAll();
        assertThat(fundingSourceTypeList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithIdMismatchFundingSourceType() throws Exception {
        int databaseSizeBeforeUpdate = fundingSourceTypeRepository.findAll().size();
        fundingSourceType.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restFundingSourceTypeMockMvc
            .perform(
                put(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(fundingSourceType))
            )
            .andExpect(status().isBadRequest());

        // Validate the FundingSourceType in the database
        List<FundingSourceType> fundingSourceTypeList = fundingSourceTypeRepository.findAll();
        assertThat(fundingSourceTypeList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithMissingIdPathParamFundingSourceType() throws Exception {
        int databaseSizeBeforeUpdate = fundingSourceTypeRepository.findAll().size();
        fundingSourceType.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restFundingSourceTypeMockMvc
            .perform(
                put(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(fundingSourceType))
            )
            .andExpect(status().isMethodNotAllowed());

        // Validate the FundingSourceType in the database
        List<FundingSourceType> fundingSourceTypeList = fundingSourceTypeRepository.findAll();
        assertThat(fundingSourceTypeList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void partialUpdateFundingSourceTypeWithPatch() throws Exception {
        // Initialize the database
        fundingSourceTypeRepository.saveAndFlush(fundingSourceType);

        int databaseSizeBeforeUpdate = fundingSourceTypeRepository.findAll().size();

        // Update the fundingSourceType using partial update
        FundingSourceType partialUpdatedFundingSourceType = new FundingSourceType();
        partialUpdatedFundingSourceType.setId(fundingSourceType.getId());

        restFundingSourceTypeMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedFundingSourceType.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedFundingSourceType))
            )
            .andExpect(status().isOk());

        // Validate the FundingSourceType in the database
        List<FundingSourceType> fundingSourceTypeList = fundingSourceTypeRepository.findAll();
        assertThat(fundingSourceTypeList).hasSize(databaseSizeBeforeUpdate);
        FundingSourceType testFundingSourceType = fundingSourceTypeList.get(fundingSourceTypeList.size() - 1);
        assertThat(testFundingSourceType.getName()).isEqualTo(DEFAULT_NAME);
        assertThat(testFundingSourceType.getCode()).isEqualTo(DEFAULT_CODE);
    }

    @Test
    @Transactional
    void fullUpdateFundingSourceTypeWithPatch() throws Exception {
        // Initialize the database
        fundingSourceTypeRepository.saveAndFlush(fundingSourceType);

        int databaseSizeBeforeUpdate = fundingSourceTypeRepository.findAll().size();

        // Update the fundingSourceType using partial update
        FundingSourceType partialUpdatedFundingSourceType = new FundingSourceType();
        partialUpdatedFundingSourceType.setId(fundingSourceType.getId());

        partialUpdatedFundingSourceType.name(UPDATED_NAME).code(UPDATED_CODE);

        restFundingSourceTypeMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedFundingSourceType.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedFundingSourceType))
            )
            .andExpect(status().isOk());

        // Validate the FundingSourceType in the database
        List<FundingSourceType> fundingSourceTypeList = fundingSourceTypeRepository.findAll();
        assertThat(fundingSourceTypeList).hasSize(databaseSizeBeforeUpdate);
        FundingSourceType testFundingSourceType = fundingSourceTypeList.get(fundingSourceTypeList.size() - 1);
        assertThat(testFundingSourceType.getName()).isEqualTo(UPDATED_NAME);
        assertThat(testFundingSourceType.getCode()).isEqualTo(UPDATED_CODE);
    }

    @Test
    @Transactional
    void patchNonExistingFundingSourceType() throws Exception {
        int databaseSizeBeforeUpdate = fundingSourceTypeRepository.findAll().size();
        fundingSourceType.setId(count.incrementAndGet());

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restFundingSourceTypeMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, fundingSourceType.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(fundingSourceType))
            )
            .andExpect(status().isBadRequest());

        // Validate the FundingSourceType in the database
        List<FundingSourceType> fundingSourceTypeList = fundingSourceTypeRepository.findAll();
        assertThat(fundingSourceTypeList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithIdMismatchFundingSourceType() throws Exception {
        int databaseSizeBeforeUpdate = fundingSourceTypeRepository.findAll().size();
        fundingSourceType.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restFundingSourceTypeMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(fundingSourceType))
            )
            .andExpect(status().isBadRequest());

        // Validate the FundingSourceType in the database
        List<FundingSourceType> fundingSourceTypeList = fundingSourceTypeRepository.findAll();
        assertThat(fundingSourceTypeList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithMissingIdPathParamFundingSourceType() throws Exception {
        int databaseSizeBeforeUpdate = fundingSourceTypeRepository.findAll().size();
        fundingSourceType.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restFundingSourceTypeMockMvc
            .perform(
                patch(ENTITY_API_URL)
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(fundingSourceType))
            )
            .andExpect(status().isMethodNotAllowed());

        // Validate the FundingSourceType in the database
        List<FundingSourceType> fundingSourceTypeList = fundingSourceTypeRepository.findAll();
        assertThat(fundingSourceTypeList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void deleteFundingSourceType() throws Exception {
        // Initialize the database
        fundingSourceTypeRepository.saveAndFlush(fundingSourceType);

        int databaseSizeBeforeDelete = fundingSourceTypeRepository.findAll().size();

        // Delete the fundingSourceType
        restFundingSourceTypeMockMvc
            .perform(delete(ENTITY_API_URL_ID, fundingSourceType.getId()).accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<FundingSourceType> fundingSourceTypeList = fundingSourceTypeRepository.findAll();
        assertThat(fundingSourceTypeList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
