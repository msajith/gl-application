package com.iconnect101.glapp.web.rest;

import static com.iconnect101.glapp.web.rest.TestUtil.sameInstant;
import static com.iconnect101.glapp.web.rest.TestUtil.sameNumber;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import com.iconnect101.glapp.IntegrationTest;
import com.iconnect101.glapp.domain.WalletTopupTransaction;
import com.iconnect101.glapp.domain.enumeration.TxnStatus;
import com.iconnect101.glapp.repository.WalletTopupTransactionRepository;
import java.math.BigDecimal;
import java.time.Instant;
import java.time.LocalDate;
import java.time.ZoneId;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.time.temporal.ChronoUnit;
import java.util.List;
import java.util.Random;
import java.util.concurrent.atomic.AtomicLong;
import javax.persistence.EntityManager;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;

/**
 * Integration tests for the {@link WalletTopupTransactionResource} REST controller.
 */
@IntegrationTest
@AutoConfigureMockMvc
@WithMockUser
class WalletTopupTransactionResourceIT {

    private static final String DEFAULT_TXN_REF_NO = "AAAAAAAAAA";
    private static final String UPDATED_TXN_REF_NO = "BBBBBBBBBB";

    private static final String DEFAULT_ACCOUNT_NO = "AAAAAAAAAA";
    private static final String UPDATED_ACCOUNT_NO = "BBBBBBBBBB";

    private static final LocalDate DEFAULT_TXN_DATE = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_TXN_DATE = LocalDate.now(ZoneId.systemDefault());

    private static final Instant DEFAULT_TXN_DATE_IN_UTC = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_TXN_DATE_IN_UTC = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    private static final ZonedDateTime DEFAULT_TXN_DATE_IN_LOCAL = ZonedDateTime.ofInstant(Instant.ofEpochMilli(0L), ZoneOffset.UTC);
    private static final ZonedDateTime UPDATED_TXN_DATE_IN_LOCAL = ZonedDateTime.now(ZoneId.systemDefault()).withNano(0);

    private static final ZonedDateTime DEFAULT_TXN_DATE_FOR_SETTLEMENT = ZonedDateTime.ofInstant(Instant.ofEpochMilli(0L), ZoneOffset.UTC);
    private static final ZonedDateTime UPDATED_TXN_DATE_FOR_SETTLEMENT = ZonedDateTime.now(ZoneId.systemDefault()).withNano(0);

    private static final LocalDate DEFAULT_VALUE_DATE = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_VALUE_DATE = LocalDate.now(ZoneId.systemDefault());

    private static final Instant DEFAULT_VALUE_DATE_IN_UTC = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_VALUE_DATE_IN_UTC = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    private static final ZonedDateTime DEFAULT_VALUE_DATE_IN_LOCAL = ZonedDateTime.ofInstant(Instant.ofEpochMilli(0L), ZoneOffset.UTC);
    private static final ZonedDateTime UPDATED_VALUE_DATE_IN_LOCAL = ZonedDateTime.now(ZoneId.systemDefault()).withNano(0);

    private static final ZonedDateTime DEFAULT_VALUE_DATE_FOR_SETTLEMENT = ZonedDateTime.ofInstant(
        Instant.ofEpochMilli(0L),
        ZoneOffset.UTC
    );
    private static final ZonedDateTime UPDATED_VALUE_DATE_FOR_SETTLEMENT = ZonedDateTime.now(ZoneId.systemDefault()).withNano(0);

    private static final BigDecimal DEFAULT_TXN_AMOUNT = new BigDecimal(1);
    private static final BigDecimal UPDATED_TXN_AMOUNT = new BigDecimal(2);

    private static final BigDecimal DEFAULT_INSTRUCTED_AMOUNT = new BigDecimal(1);
    private static final BigDecimal UPDATED_INSTRUCTED_AMOUNT = new BigDecimal(2);

    private static final BigDecimal DEFAULT_AMOUNT_IN_ACCOUNT_CURRENCY = new BigDecimal(1);
    private static final BigDecimal UPDATED_AMOUNT_IN_ACCOUNT_CURRENCY = new BigDecimal(2);

    private static final BigDecimal DEFAULT_AMOUNT_IN_LOCAL_CURRENCY = new BigDecimal(1);
    private static final BigDecimal UPDATED_AMOUNT_IN_LOCAL_CURRENCY = new BigDecimal(2);

    private static final BigDecimal DEFAULT_CHARGE_IN_ACCOUNT_CURRENCY = new BigDecimal(1);
    private static final BigDecimal UPDATED_CHARGE_IN_ACCOUNT_CURRENCY = new BigDecimal(2);

    private static final BigDecimal DEFAULT_CHARGE_IN_LOCAL_CURRENCY = new BigDecimal(1);
    private static final BigDecimal UPDATED_CHARGE_IN_LOCAL_CURRENCY = new BigDecimal(2);

    private static final String DEFAULT_FUNDING_SOURCE = "AAAAAAAAAA";
    private static final String UPDATED_FUNDING_SOURCE = "BBBBBBBBBB";

    private static final String DEFAULT_MERCHANT_ID = "AAAAAAAAAA";
    private static final String UPDATED_MERCHANT_ID = "BBBBBBBBBB";

    private static final String DEFAULT_MERCHANT_INFO = "AAAAAAAAAA";
    private static final String UPDATED_MERCHANT_INFO = "BBBBBBBBBB";

    private static final String DEFAULT_BANK_CODE = "AAAAAAAAAA";
    private static final String UPDATED_BANK_CODE = "BBBBBBBBBB";

    private static final String DEFAULT_EXTERNAL_REF_NO = "AAAAAAAAAA";
    private static final String UPDATED_EXTERNAL_REF_NO = "BBBBBBBBBB";

    private static final String DEFAULT_TXN_DESCRIPTION = "AAAAAAAAAA";
    private static final String UPDATED_TXN_DESCRIPTION = "BBBBBBBBBB";

    private static final String DEFAULT_TXN_INTERNAL_DESCRIPTION = "AAAAAAAAAA";
    private static final String UPDATED_TXN_INTERNAL_DESCRIPTION = "BBBBBBBBBB";

    private static final String DEFAULT_TXN_ADDNL_INFO = "AAAAAAAAAA";
    private static final String UPDATED_TXN_ADDNL_INFO = "BBBBBBBBBB";

    private static final Instant DEFAULT_CREATED_ON = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_CREATED_ON = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    private static final String DEFAULT_CREATED_BY = "AAAAAAAAAA";
    private static final String UPDATED_CREATED_BY = "BBBBBBBBBB";

    private static final TxnStatus DEFAULT_TXN_STATUS = TxnStatus.ACTIVE;
    private static final TxnStatus UPDATED_TXN_STATUS = TxnStatus.CANCELLED;

    private static final String DEFAULT_TXN_DEBIT_ACCOUNT_NO = "AAAAAAAAAA";
    private static final String UPDATED_TXN_DEBIT_ACCOUNT_NO = "BBBBBBBBBB";

    private static final String DEFAULT_TXN_CREDIT_ACCOUNT_NO = "AAAAAAAAAA";
    private static final String UPDATED_TXN_CREDIT_ACCOUNT_NO = "BBBBBBBBBB";

    private static final String DEFAULT_CHARGE_DEBIT_ACCOUNT_NO = "AAAAAAAAAA";
    private static final String UPDATED_CHARGE_DEBIT_ACCOUNT_NO = "BBBBBBBBBB";

    private static final String DEFAULT_CHARGE_CREDIT_ACCOUNT_NO = "AAAAAAAAAA";
    private static final String UPDATED_CHARGE_CREDIT_ACCOUNT_NO = "BBBBBBBBBB";

    private static final String ENTITY_API_URL = "/api/wallet-topup-transactions";
    private static final String ENTITY_API_URL_ID = ENTITY_API_URL + "/{id}";

    private static Random random = new Random();
    private static AtomicLong count = new AtomicLong(random.nextInt() + (2 * Integer.MAX_VALUE));

    @Autowired
    private WalletTopupTransactionRepository walletTopupTransactionRepository;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restWalletTopupTransactionMockMvc;

    private WalletTopupTransaction walletTopupTransaction;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static WalletTopupTransaction createEntity(EntityManager em) {
        WalletTopupTransaction walletTopupTransaction = new WalletTopupTransaction()
            .txnRefNo(DEFAULT_TXN_REF_NO)
            .accountNo(DEFAULT_ACCOUNT_NO)
            .txnDate(DEFAULT_TXN_DATE)
            .txnDateInUTC(DEFAULT_TXN_DATE_IN_UTC)
            .txnDateInLocal(DEFAULT_TXN_DATE_IN_LOCAL)
            .txnDateForSettlement(DEFAULT_TXN_DATE_FOR_SETTLEMENT)
            .valueDate(DEFAULT_VALUE_DATE)
            .valueDateInUTC(DEFAULT_VALUE_DATE_IN_UTC)
            .valueDateInLocal(DEFAULT_VALUE_DATE_IN_LOCAL)
            .valueDateForSettlement(DEFAULT_VALUE_DATE_FOR_SETTLEMENT)
            .txnAmount(DEFAULT_TXN_AMOUNT)
            .instructedAmount(DEFAULT_INSTRUCTED_AMOUNT)
            .amountInAccountCurrency(DEFAULT_AMOUNT_IN_ACCOUNT_CURRENCY)
            .amountInLocalCurrency(DEFAULT_AMOUNT_IN_LOCAL_CURRENCY)
            .chargeInAccountCurrency(DEFAULT_CHARGE_IN_ACCOUNT_CURRENCY)
            .chargeInLocalCurrency(DEFAULT_CHARGE_IN_LOCAL_CURRENCY)
            .fundingSource(DEFAULT_FUNDING_SOURCE)
            .merchantID(DEFAULT_MERCHANT_ID)
            .merchantInfo(DEFAULT_MERCHANT_INFO)
            .bankCode(DEFAULT_BANK_CODE)
            .externalRefNo(DEFAULT_EXTERNAL_REF_NO)
            .txnDescription(DEFAULT_TXN_DESCRIPTION)
            .txnInternalDescription(DEFAULT_TXN_INTERNAL_DESCRIPTION)
            .txnAddnlInfo(DEFAULT_TXN_ADDNL_INFO)
            .createdOn(DEFAULT_CREATED_ON)
            .createdBy(DEFAULT_CREATED_BY)
            .txnStatus(DEFAULT_TXN_STATUS)
            .txnDebitAccountNo(DEFAULT_TXN_DEBIT_ACCOUNT_NO)
            .txnCreditAccountNo(DEFAULT_TXN_CREDIT_ACCOUNT_NO)
            .chargeDebitAccountNo(DEFAULT_CHARGE_DEBIT_ACCOUNT_NO)
            .chargeCreditAccountNo(DEFAULT_CHARGE_CREDIT_ACCOUNT_NO);
        return walletTopupTransaction;
    }

    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static WalletTopupTransaction createUpdatedEntity(EntityManager em) {
        WalletTopupTransaction walletTopupTransaction = new WalletTopupTransaction()
            .txnRefNo(UPDATED_TXN_REF_NO)
            .accountNo(UPDATED_ACCOUNT_NO)
            .txnDate(UPDATED_TXN_DATE)
            .txnDateInUTC(UPDATED_TXN_DATE_IN_UTC)
            .txnDateInLocal(UPDATED_TXN_DATE_IN_LOCAL)
            .txnDateForSettlement(UPDATED_TXN_DATE_FOR_SETTLEMENT)
            .valueDate(UPDATED_VALUE_DATE)
            .valueDateInUTC(UPDATED_VALUE_DATE_IN_UTC)
            .valueDateInLocal(UPDATED_VALUE_DATE_IN_LOCAL)
            .valueDateForSettlement(UPDATED_VALUE_DATE_FOR_SETTLEMENT)
            .txnAmount(UPDATED_TXN_AMOUNT)
            .instructedAmount(UPDATED_INSTRUCTED_AMOUNT)
            .amountInAccountCurrency(UPDATED_AMOUNT_IN_ACCOUNT_CURRENCY)
            .amountInLocalCurrency(UPDATED_AMOUNT_IN_LOCAL_CURRENCY)
            .chargeInAccountCurrency(UPDATED_CHARGE_IN_ACCOUNT_CURRENCY)
            .chargeInLocalCurrency(UPDATED_CHARGE_IN_LOCAL_CURRENCY)
            .fundingSource(UPDATED_FUNDING_SOURCE)
            .merchantID(UPDATED_MERCHANT_ID)
            .merchantInfo(UPDATED_MERCHANT_INFO)
            .bankCode(UPDATED_BANK_CODE)
            .externalRefNo(UPDATED_EXTERNAL_REF_NO)
            .txnDescription(UPDATED_TXN_DESCRIPTION)
            .txnInternalDescription(UPDATED_TXN_INTERNAL_DESCRIPTION)
            .txnAddnlInfo(UPDATED_TXN_ADDNL_INFO)
            .createdOn(UPDATED_CREATED_ON)
            .createdBy(UPDATED_CREATED_BY)
            .txnStatus(UPDATED_TXN_STATUS)
            .txnDebitAccountNo(UPDATED_TXN_DEBIT_ACCOUNT_NO)
            .txnCreditAccountNo(UPDATED_TXN_CREDIT_ACCOUNT_NO)
            .chargeDebitAccountNo(UPDATED_CHARGE_DEBIT_ACCOUNT_NO)
            .chargeCreditAccountNo(UPDATED_CHARGE_CREDIT_ACCOUNT_NO);
        return walletTopupTransaction;
    }

    @BeforeEach
    public void initTest() {
        walletTopupTransaction = createEntity(em);
    }

    @Test
    @Transactional
    void createWalletTopupTransaction() throws Exception {
        int databaseSizeBeforeCreate = walletTopupTransactionRepository.findAll().size();
        // Create the WalletTopupTransaction
        restWalletTopupTransactionMockMvc
            .perform(
                post(ENTITY_API_URL)
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(walletTopupTransaction))
            )
            .andExpect(status().isCreated());

        // Validate the WalletTopupTransaction in the database
        List<WalletTopupTransaction> walletTopupTransactionList = walletTopupTransactionRepository.findAll();
        assertThat(walletTopupTransactionList).hasSize(databaseSizeBeforeCreate + 1);
        WalletTopupTransaction testWalletTopupTransaction = walletTopupTransactionList.get(walletTopupTransactionList.size() - 1);
        assertThat(testWalletTopupTransaction.getTxnRefNo()).isEqualTo(DEFAULT_TXN_REF_NO);
        assertThat(testWalletTopupTransaction.getAccountNo()).isEqualTo(DEFAULT_ACCOUNT_NO);
        assertThat(testWalletTopupTransaction.getTxnDate()).isEqualTo(DEFAULT_TXN_DATE);
        assertThat(testWalletTopupTransaction.getTxnDateInUTC()).isEqualTo(DEFAULT_TXN_DATE_IN_UTC);
        assertThat(testWalletTopupTransaction.getTxnDateInLocal()).isEqualTo(DEFAULT_TXN_DATE_IN_LOCAL);
        assertThat(testWalletTopupTransaction.getTxnDateForSettlement()).isEqualTo(DEFAULT_TXN_DATE_FOR_SETTLEMENT);
        assertThat(testWalletTopupTransaction.getValueDate()).isEqualTo(DEFAULT_VALUE_DATE);
        assertThat(testWalletTopupTransaction.getValueDateInUTC()).isEqualTo(DEFAULT_VALUE_DATE_IN_UTC);
        assertThat(testWalletTopupTransaction.getValueDateInLocal()).isEqualTo(DEFAULT_VALUE_DATE_IN_LOCAL);
        assertThat(testWalletTopupTransaction.getValueDateForSettlement()).isEqualTo(DEFAULT_VALUE_DATE_FOR_SETTLEMENT);
        assertThat(testWalletTopupTransaction.getTxnAmount()).isEqualByComparingTo(DEFAULT_TXN_AMOUNT);
        assertThat(testWalletTopupTransaction.getInstructedAmount()).isEqualByComparingTo(DEFAULT_INSTRUCTED_AMOUNT);
        assertThat(testWalletTopupTransaction.getAmountInAccountCurrency()).isEqualByComparingTo(DEFAULT_AMOUNT_IN_ACCOUNT_CURRENCY);
        assertThat(testWalletTopupTransaction.getAmountInLocalCurrency()).isEqualByComparingTo(DEFAULT_AMOUNT_IN_LOCAL_CURRENCY);
        assertThat(testWalletTopupTransaction.getChargeInAccountCurrency()).isEqualByComparingTo(DEFAULT_CHARGE_IN_ACCOUNT_CURRENCY);
        assertThat(testWalletTopupTransaction.getChargeInLocalCurrency()).isEqualByComparingTo(DEFAULT_CHARGE_IN_LOCAL_CURRENCY);
        assertThat(testWalletTopupTransaction.getFundingSource()).isEqualTo(DEFAULT_FUNDING_SOURCE);
        assertThat(testWalletTopupTransaction.getMerchantID()).isEqualTo(DEFAULT_MERCHANT_ID);
        assertThat(testWalletTopupTransaction.getMerchantInfo()).isEqualTo(DEFAULT_MERCHANT_INFO);
        assertThat(testWalletTopupTransaction.getBankCode()).isEqualTo(DEFAULT_BANK_CODE);
        assertThat(testWalletTopupTransaction.getExternalRefNo()).isEqualTo(DEFAULT_EXTERNAL_REF_NO);
        assertThat(testWalletTopupTransaction.getTxnDescription()).isEqualTo(DEFAULT_TXN_DESCRIPTION);
        assertThat(testWalletTopupTransaction.getTxnInternalDescription()).isEqualTo(DEFAULT_TXN_INTERNAL_DESCRIPTION);
        assertThat(testWalletTopupTransaction.getTxnAddnlInfo()).isEqualTo(DEFAULT_TXN_ADDNL_INFO);
        assertThat(testWalletTopupTransaction.getCreatedOn()).isEqualTo(DEFAULT_CREATED_ON);
        assertThat(testWalletTopupTransaction.getCreatedBy()).isEqualTo(DEFAULT_CREATED_BY);
        assertThat(testWalletTopupTransaction.getTxnStatus()).isEqualTo(DEFAULT_TXN_STATUS);
        assertThat(testWalletTopupTransaction.getTxnDebitAccountNo()).isEqualTo(DEFAULT_TXN_DEBIT_ACCOUNT_NO);
        assertThat(testWalletTopupTransaction.getTxnCreditAccountNo()).isEqualTo(DEFAULT_TXN_CREDIT_ACCOUNT_NO);
        assertThat(testWalletTopupTransaction.getChargeDebitAccountNo()).isEqualTo(DEFAULT_CHARGE_DEBIT_ACCOUNT_NO);
        assertThat(testWalletTopupTransaction.getChargeCreditAccountNo()).isEqualTo(DEFAULT_CHARGE_CREDIT_ACCOUNT_NO);
    }

    @Test
    @Transactional
    void createWalletTopupTransactionWithExistingId() throws Exception {
        // Create the WalletTopupTransaction with an existing ID
        walletTopupTransaction.setId(1L);

        int databaseSizeBeforeCreate = walletTopupTransactionRepository.findAll().size();

        // An entity with an existing ID cannot be created, so this API call must fail
        restWalletTopupTransactionMockMvc
            .perform(
                post(ENTITY_API_URL)
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(walletTopupTransaction))
            )
            .andExpect(status().isBadRequest());

        // Validate the WalletTopupTransaction in the database
        List<WalletTopupTransaction> walletTopupTransactionList = walletTopupTransactionRepository.findAll();
        assertThat(walletTopupTransactionList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    void getAllWalletTopupTransactions() throws Exception {
        // Initialize the database
        walletTopupTransactionRepository.saveAndFlush(walletTopupTransaction);

        // Get all the walletTopupTransactionList
        restWalletTopupTransactionMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(walletTopupTransaction.getId().intValue())))
            .andExpect(jsonPath("$.[*].txnRefNo").value(hasItem(DEFAULT_TXN_REF_NO)))
            .andExpect(jsonPath("$.[*].accountNo").value(hasItem(DEFAULT_ACCOUNT_NO)))
            .andExpect(jsonPath("$.[*].txnDate").value(hasItem(DEFAULT_TXN_DATE.toString())))
            .andExpect(jsonPath("$.[*].txnDateInUTC").value(hasItem(DEFAULT_TXN_DATE_IN_UTC.toString())))
            .andExpect(jsonPath("$.[*].txnDateInLocal").value(hasItem(sameInstant(DEFAULT_TXN_DATE_IN_LOCAL))))
            .andExpect(jsonPath("$.[*].txnDateForSettlement").value(hasItem(sameInstant(DEFAULT_TXN_DATE_FOR_SETTLEMENT))))
            .andExpect(jsonPath("$.[*].valueDate").value(hasItem(DEFAULT_VALUE_DATE.toString())))
            .andExpect(jsonPath("$.[*].valueDateInUTC").value(hasItem(DEFAULT_VALUE_DATE_IN_UTC.toString())))
            .andExpect(jsonPath("$.[*].valueDateInLocal").value(hasItem(sameInstant(DEFAULT_VALUE_DATE_IN_LOCAL))))
            .andExpect(jsonPath("$.[*].valueDateForSettlement").value(hasItem(sameInstant(DEFAULT_VALUE_DATE_FOR_SETTLEMENT))))
            .andExpect(jsonPath("$.[*].txnAmount").value(hasItem(sameNumber(DEFAULT_TXN_AMOUNT))))
            .andExpect(jsonPath("$.[*].instructedAmount").value(hasItem(sameNumber(DEFAULT_INSTRUCTED_AMOUNT))))
            .andExpect(jsonPath("$.[*].amountInAccountCurrency").value(hasItem(sameNumber(DEFAULT_AMOUNT_IN_ACCOUNT_CURRENCY))))
            .andExpect(jsonPath("$.[*].amountInLocalCurrency").value(hasItem(sameNumber(DEFAULT_AMOUNT_IN_LOCAL_CURRENCY))))
            .andExpect(jsonPath("$.[*].chargeInAccountCurrency").value(hasItem(sameNumber(DEFAULT_CHARGE_IN_ACCOUNT_CURRENCY))))
            .andExpect(jsonPath("$.[*].chargeInLocalCurrency").value(hasItem(sameNumber(DEFAULT_CHARGE_IN_LOCAL_CURRENCY))))
            .andExpect(jsonPath("$.[*].fundingSource").value(hasItem(DEFAULT_FUNDING_SOURCE)))
            .andExpect(jsonPath("$.[*].merchantID").value(hasItem(DEFAULT_MERCHANT_ID)))
            .andExpect(jsonPath("$.[*].merchantInfo").value(hasItem(DEFAULT_MERCHANT_INFO)))
            .andExpect(jsonPath("$.[*].bankCode").value(hasItem(DEFAULT_BANK_CODE)))
            .andExpect(jsonPath("$.[*].externalRefNo").value(hasItem(DEFAULT_EXTERNAL_REF_NO)))
            .andExpect(jsonPath("$.[*].txnDescription").value(hasItem(DEFAULT_TXN_DESCRIPTION)))
            .andExpect(jsonPath("$.[*].txnInternalDescription").value(hasItem(DEFAULT_TXN_INTERNAL_DESCRIPTION)))
            .andExpect(jsonPath("$.[*].txnAddnlInfo").value(hasItem(DEFAULT_TXN_ADDNL_INFO)))
            .andExpect(jsonPath("$.[*].createdOn").value(hasItem(DEFAULT_CREATED_ON.toString())))
            .andExpect(jsonPath("$.[*].createdBy").value(hasItem(DEFAULT_CREATED_BY)))
            .andExpect(jsonPath("$.[*].txnStatus").value(hasItem(DEFAULT_TXN_STATUS.toString())))
            .andExpect(jsonPath("$.[*].txnDebitAccountNo").value(hasItem(DEFAULT_TXN_DEBIT_ACCOUNT_NO)))
            .andExpect(jsonPath("$.[*].txnCreditAccountNo").value(hasItem(DEFAULT_TXN_CREDIT_ACCOUNT_NO)))
            .andExpect(jsonPath("$.[*].chargeDebitAccountNo").value(hasItem(DEFAULT_CHARGE_DEBIT_ACCOUNT_NO)))
            .andExpect(jsonPath("$.[*].chargeCreditAccountNo").value(hasItem(DEFAULT_CHARGE_CREDIT_ACCOUNT_NO)));
    }

    @Test
    @Transactional
    void getWalletTopupTransaction() throws Exception {
        // Initialize the database
        walletTopupTransactionRepository.saveAndFlush(walletTopupTransaction);

        // Get the walletTopupTransaction
        restWalletTopupTransactionMockMvc
            .perform(get(ENTITY_API_URL_ID, walletTopupTransaction.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(walletTopupTransaction.getId().intValue()))
            .andExpect(jsonPath("$.txnRefNo").value(DEFAULT_TXN_REF_NO))
            .andExpect(jsonPath("$.accountNo").value(DEFAULT_ACCOUNT_NO))
            .andExpect(jsonPath("$.txnDate").value(DEFAULT_TXN_DATE.toString()))
            .andExpect(jsonPath("$.txnDateInUTC").value(DEFAULT_TXN_DATE_IN_UTC.toString()))
            .andExpect(jsonPath("$.txnDateInLocal").value(sameInstant(DEFAULT_TXN_DATE_IN_LOCAL)))
            .andExpect(jsonPath("$.txnDateForSettlement").value(sameInstant(DEFAULT_TXN_DATE_FOR_SETTLEMENT)))
            .andExpect(jsonPath("$.valueDate").value(DEFAULT_VALUE_DATE.toString()))
            .andExpect(jsonPath("$.valueDateInUTC").value(DEFAULT_VALUE_DATE_IN_UTC.toString()))
            .andExpect(jsonPath("$.valueDateInLocal").value(sameInstant(DEFAULT_VALUE_DATE_IN_LOCAL)))
            .andExpect(jsonPath("$.valueDateForSettlement").value(sameInstant(DEFAULT_VALUE_DATE_FOR_SETTLEMENT)))
            .andExpect(jsonPath("$.txnAmount").value(sameNumber(DEFAULT_TXN_AMOUNT)))
            .andExpect(jsonPath("$.instructedAmount").value(sameNumber(DEFAULT_INSTRUCTED_AMOUNT)))
            .andExpect(jsonPath("$.amountInAccountCurrency").value(sameNumber(DEFAULT_AMOUNT_IN_ACCOUNT_CURRENCY)))
            .andExpect(jsonPath("$.amountInLocalCurrency").value(sameNumber(DEFAULT_AMOUNT_IN_LOCAL_CURRENCY)))
            .andExpect(jsonPath("$.chargeInAccountCurrency").value(sameNumber(DEFAULT_CHARGE_IN_ACCOUNT_CURRENCY)))
            .andExpect(jsonPath("$.chargeInLocalCurrency").value(sameNumber(DEFAULT_CHARGE_IN_LOCAL_CURRENCY)))
            .andExpect(jsonPath("$.fundingSource").value(DEFAULT_FUNDING_SOURCE))
            .andExpect(jsonPath("$.merchantID").value(DEFAULT_MERCHANT_ID))
            .andExpect(jsonPath("$.merchantInfo").value(DEFAULT_MERCHANT_INFO))
            .andExpect(jsonPath("$.bankCode").value(DEFAULT_BANK_CODE))
            .andExpect(jsonPath("$.externalRefNo").value(DEFAULT_EXTERNAL_REF_NO))
            .andExpect(jsonPath("$.txnDescription").value(DEFAULT_TXN_DESCRIPTION))
            .andExpect(jsonPath("$.txnInternalDescription").value(DEFAULT_TXN_INTERNAL_DESCRIPTION))
            .andExpect(jsonPath("$.txnAddnlInfo").value(DEFAULT_TXN_ADDNL_INFO))
            .andExpect(jsonPath("$.createdOn").value(DEFAULT_CREATED_ON.toString()))
            .andExpect(jsonPath("$.createdBy").value(DEFAULT_CREATED_BY))
            .andExpect(jsonPath("$.txnStatus").value(DEFAULT_TXN_STATUS.toString()))
            .andExpect(jsonPath("$.txnDebitAccountNo").value(DEFAULT_TXN_DEBIT_ACCOUNT_NO))
            .andExpect(jsonPath("$.txnCreditAccountNo").value(DEFAULT_TXN_CREDIT_ACCOUNT_NO))
            .andExpect(jsonPath("$.chargeDebitAccountNo").value(DEFAULT_CHARGE_DEBIT_ACCOUNT_NO))
            .andExpect(jsonPath("$.chargeCreditAccountNo").value(DEFAULT_CHARGE_CREDIT_ACCOUNT_NO));
    }

    @Test
    @Transactional
    void getNonExistingWalletTopupTransaction() throws Exception {
        // Get the walletTopupTransaction
        restWalletTopupTransactionMockMvc.perform(get(ENTITY_API_URL_ID, Long.MAX_VALUE)).andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    void putNewWalletTopupTransaction() throws Exception {
        // Initialize the database
        walletTopupTransactionRepository.saveAndFlush(walletTopupTransaction);

        int databaseSizeBeforeUpdate = walletTopupTransactionRepository.findAll().size();

        // Update the walletTopupTransaction
        WalletTopupTransaction updatedWalletTopupTransaction = walletTopupTransactionRepository
            .findById(walletTopupTransaction.getId())
            .get();
        // Disconnect from session so that the updates on updatedWalletTopupTransaction are not directly saved in db
        em.detach(updatedWalletTopupTransaction);
        updatedWalletTopupTransaction
            .txnRefNo(UPDATED_TXN_REF_NO)
            .accountNo(UPDATED_ACCOUNT_NO)
            .txnDate(UPDATED_TXN_DATE)
            .txnDateInUTC(UPDATED_TXN_DATE_IN_UTC)
            .txnDateInLocal(UPDATED_TXN_DATE_IN_LOCAL)
            .txnDateForSettlement(UPDATED_TXN_DATE_FOR_SETTLEMENT)
            .valueDate(UPDATED_VALUE_DATE)
            .valueDateInUTC(UPDATED_VALUE_DATE_IN_UTC)
            .valueDateInLocal(UPDATED_VALUE_DATE_IN_LOCAL)
            .valueDateForSettlement(UPDATED_VALUE_DATE_FOR_SETTLEMENT)
            .txnAmount(UPDATED_TXN_AMOUNT)
            .instructedAmount(UPDATED_INSTRUCTED_AMOUNT)
            .amountInAccountCurrency(UPDATED_AMOUNT_IN_ACCOUNT_CURRENCY)
            .amountInLocalCurrency(UPDATED_AMOUNT_IN_LOCAL_CURRENCY)
            .chargeInAccountCurrency(UPDATED_CHARGE_IN_ACCOUNT_CURRENCY)
            .chargeInLocalCurrency(UPDATED_CHARGE_IN_LOCAL_CURRENCY)
            .fundingSource(UPDATED_FUNDING_SOURCE)
            .merchantID(UPDATED_MERCHANT_ID)
            .merchantInfo(UPDATED_MERCHANT_INFO)
            .bankCode(UPDATED_BANK_CODE)
            .externalRefNo(UPDATED_EXTERNAL_REF_NO)
            .txnDescription(UPDATED_TXN_DESCRIPTION)
            .txnInternalDescription(UPDATED_TXN_INTERNAL_DESCRIPTION)
            .txnAddnlInfo(UPDATED_TXN_ADDNL_INFO)
            .createdOn(UPDATED_CREATED_ON)
            .createdBy(UPDATED_CREATED_BY)
            .txnStatus(UPDATED_TXN_STATUS)
            .txnDebitAccountNo(UPDATED_TXN_DEBIT_ACCOUNT_NO)
            .txnCreditAccountNo(UPDATED_TXN_CREDIT_ACCOUNT_NO)
            .chargeDebitAccountNo(UPDATED_CHARGE_DEBIT_ACCOUNT_NO)
            .chargeCreditAccountNo(UPDATED_CHARGE_CREDIT_ACCOUNT_NO);

        restWalletTopupTransactionMockMvc
            .perform(
                put(ENTITY_API_URL_ID, updatedWalletTopupTransaction.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(updatedWalletTopupTransaction))
            )
            .andExpect(status().isOk());

        // Validate the WalletTopupTransaction in the database
        List<WalletTopupTransaction> walletTopupTransactionList = walletTopupTransactionRepository.findAll();
        assertThat(walletTopupTransactionList).hasSize(databaseSizeBeforeUpdate);
        WalletTopupTransaction testWalletTopupTransaction = walletTopupTransactionList.get(walletTopupTransactionList.size() - 1);
        assertThat(testWalletTopupTransaction.getTxnRefNo()).isEqualTo(UPDATED_TXN_REF_NO);
        assertThat(testWalletTopupTransaction.getAccountNo()).isEqualTo(UPDATED_ACCOUNT_NO);
        assertThat(testWalletTopupTransaction.getTxnDate()).isEqualTo(UPDATED_TXN_DATE);
        assertThat(testWalletTopupTransaction.getTxnDateInUTC()).isEqualTo(UPDATED_TXN_DATE_IN_UTC);
        assertThat(testWalletTopupTransaction.getTxnDateInLocal()).isEqualTo(UPDATED_TXN_DATE_IN_LOCAL);
        assertThat(testWalletTopupTransaction.getTxnDateForSettlement()).isEqualTo(UPDATED_TXN_DATE_FOR_SETTLEMENT);
        assertThat(testWalletTopupTransaction.getValueDate()).isEqualTo(UPDATED_VALUE_DATE);
        assertThat(testWalletTopupTransaction.getValueDateInUTC()).isEqualTo(UPDATED_VALUE_DATE_IN_UTC);
        assertThat(testWalletTopupTransaction.getValueDateInLocal()).isEqualTo(UPDATED_VALUE_DATE_IN_LOCAL);
        assertThat(testWalletTopupTransaction.getValueDateForSettlement()).isEqualTo(UPDATED_VALUE_DATE_FOR_SETTLEMENT);
        assertThat(testWalletTopupTransaction.getTxnAmount()).isEqualTo(UPDATED_TXN_AMOUNT);
        assertThat(testWalletTopupTransaction.getInstructedAmount()).isEqualTo(UPDATED_INSTRUCTED_AMOUNT);
        assertThat(testWalletTopupTransaction.getAmountInAccountCurrency()).isEqualTo(UPDATED_AMOUNT_IN_ACCOUNT_CURRENCY);
        assertThat(testWalletTopupTransaction.getAmountInLocalCurrency()).isEqualTo(UPDATED_AMOUNT_IN_LOCAL_CURRENCY);
        assertThat(testWalletTopupTransaction.getChargeInAccountCurrency()).isEqualTo(UPDATED_CHARGE_IN_ACCOUNT_CURRENCY);
        assertThat(testWalletTopupTransaction.getChargeInLocalCurrency()).isEqualTo(UPDATED_CHARGE_IN_LOCAL_CURRENCY);
        assertThat(testWalletTopupTransaction.getFundingSource()).isEqualTo(UPDATED_FUNDING_SOURCE);
        assertThat(testWalletTopupTransaction.getMerchantID()).isEqualTo(UPDATED_MERCHANT_ID);
        assertThat(testWalletTopupTransaction.getMerchantInfo()).isEqualTo(UPDATED_MERCHANT_INFO);
        assertThat(testWalletTopupTransaction.getBankCode()).isEqualTo(UPDATED_BANK_CODE);
        assertThat(testWalletTopupTransaction.getExternalRefNo()).isEqualTo(UPDATED_EXTERNAL_REF_NO);
        assertThat(testWalletTopupTransaction.getTxnDescription()).isEqualTo(UPDATED_TXN_DESCRIPTION);
        assertThat(testWalletTopupTransaction.getTxnInternalDescription()).isEqualTo(UPDATED_TXN_INTERNAL_DESCRIPTION);
        assertThat(testWalletTopupTransaction.getTxnAddnlInfo()).isEqualTo(UPDATED_TXN_ADDNL_INFO);
        assertThat(testWalletTopupTransaction.getCreatedOn()).isEqualTo(UPDATED_CREATED_ON);
        assertThat(testWalletTopupTransaction.getCreatedBy()).isEqualTo(UPDATED_CREATED_BY);
        assertThat(testWalletTopupTransaction.getTxnStatus()).isEqualTo(UPDATED_TXN_STATUS);
        assertThat(testWalletTopupTransaction.getTxnDebitAccountNo()).isEqualTo(UPDATED_TXN_DEBIT_ACCOUNT_NO);
        assertThat(testWalletTopupTransaction.getTxnCreditAccountNo()).isEqualTo(UPDATED_TXN_CREDIT_ACCOUNT_NO);
        assertThat(testWalletTopupTransaction.getChargeDebitAccountNo()).isEqualTo(UPDATED_CHARGE_DEBIT_ACCOUNT_NO);
        assertThat(testWalletTopupTransaction.getChargeCreditAccountNo()).isEqualTo(UPDATED_CHARGE_CREDIT_ACCOUNT_NO);
    }

    @Test
    @Transactional
    void putNonExistingWalletTopupTransaction() throws Exception {
        int databaseSizeBeforeUpdate = walletTopupTransactionRepository.findAll().size();
        walletTopupTransaction.setId(count.incrementAndGet());

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restWalletTopupTransactionMockMvc
            .perform(
                put(ENTITY_API_URL_ID, walletTopupTransaction.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(walletTopupTransaction))
            )
            .andExpect(status().isBadRequest());

        // Validate the WalletTopupTransaction in the database
        List<WalletTopupTransaction> walletTopupTransactionList = walletTopupTransactionRepository.findAll();
        assertThat(walletTopupTransactionList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithIdMismatchWalletTopupTransaction() throws Exception {
        int databaseSizeBeforeUpdate = walletTopupTransactionRepository.findAll().size();
        walletTopupTransaction.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restWalletTopupTransactionMockMvc
            .perform(
                put(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(walletTopupTransaction))
            )
            .andExpect(status().isBadRequest());

        // Validate the WalletTopupTransaction in the database
        List<WalletTopupTransaction> walletTopupTransactionList = walletTopupTransactionRepository.findAll();
        assertThat(walletTopupTransactionList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithMissingIdPathParamWalletTopupTransaction() throws Exception {
        int databaseSizeBeforeUpdate = walletTopupTransactionRepository.findAll().size();
        walletTopupTransaction.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restWalletTopupTransactionMockMvc
            .perform(
                put(ENTITY_API_URL)
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(walletTopupTransaction))
            )
            .andExpect(status().isMethodNotAllowed());

        // Validate the WalletTopupTransaction in the database
        List<WalletTopupTransaction> walletTopupTransactionList = walletTopupTransactionRepository.findAll();
        assertThat(walletTopupTransactionList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void partialUpdateWalletTopupTransactionWithPatch() throws Exception {
        // Initialize the database
        walletTopupTransactionRepository.saveAndFlush(walletTopupTransaction);

        int databaseSizeBeforeUpdate = walletTopupTransactionRepository.findAll().size();

        // Update the walletTopupTransaction using partial update
        WalletTopupTransaction partialUpdatedWalletTopupTransaction = new WalletTopupTransaction();
        partialUpdatedWalletTopupTransaction.setId(walletTopupTransaction.getId());

        partialUpdatedWalletTopupTransaction
            .accountNo(UPDATED_ACCOUNT_NO)
            .txnDate(UPDATED_TXN_DATE)
            .txnDateInUTC(UPDATED_TXN_DATE_IN_UTC)
            .valueDate(UPDATED_VALUE_DATE)
            .valueDateInUTC(UPDATED_VALUE_DATE_IN_UTC)
            .valueDateInLocal(UPDATED_VALUE_DATE_IN_LOCAL)
            .amountInAccountCurrency(UPDATED_AMOUNT_IN_ACCOUNT_CURRENCY)
            .chargeInAccountCurrency(UPDATED_CHARGE_IN_ACCOUNT_CURRENCY)
            .merchantID(UPDATED_MERCHANT_ID)
            .merchantInfo(UPDATED_MERCHANT_INFO)
            .bankCode(UPDATED_BANK_CODE)
            .externalRefNo(UPDATED_EXTERNAL_REF_NO)
            .txnStatus(UPDATED_TXN_STATUS)
            .txnCreditAccountNo(UPDATED_TXN_CREDIT_ACCOUNT_NO)
            .chargeDebitAccountNo(UPDATED_CHARGE_DEBIT_ACCOUNT_NO);

        restWalletTopupTransactionMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedWalletTopupTransaction.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedWalletTopupTransaction))
            )
            .andExpect(status().isOk());

        // Validate the WalletTopupTransaction in the database
        List<WalletTopupTransaction> walletTopupTransactionList = walletTopupTransactionRepository.findAll();
        assertThat(walletTopupTransactionList).hasSize(databaseSizeBeforeUpdate);
        WalletTopupTransaction testWalletTopupTransaction = walletTopupTransactionList.get(walletTopupTransactionList.size() - 1);
        assertThat(testWalletTopupTransaction.getTxnRefNo()).isEqualTo(DEFAULT_TXN_REF_NO);
        assertThat(testWalletTopupTransaction.getAccountNo()).isEqualTo(UPDATED_ACCOUNT_NO);
        assertThat(testWalletTopupTransaction.getTxnDate()).isEqualTo(UPDATED_TXN_DATE);
        assertThat(testWalletTopupTransaction.getTxnDateInUTC()).isEqualTo(UPDATED_TXN_DATE_IN_UTC);
        assertThat(testWalletTopupTransaction.getTxnDateInLocal()).isEqualTo(DEFAULT_TXN_DATE_IN_LOCAL);
        assertThat(testWalletTopupTransaction.getTxnDateForSettlement()).isEqualTo(DEFAULT_TXN_DATE_FOR_SETTLEMENT);
        assertThat(testWalletTopupTransaction.getValueDate()).isEqualTo(UPDATED_VALUE_DATE);
        assertThat(testWalletTopupTransaction.getValueDateInUTC()).isEqualTo(UPDATED_VALUE_DATE_IN_UTC);
        assertThat(testWalletTopupTransaction.getValueDateInLocal()).isEqualTo(UPDATED_VALUE_DATE_IN_LOCAL);
        assertThat(testWalletTopupTransaction.getValueDateForSettlement()).isEqualTo(DEFAULT_VALUE_DATE_FOR_SETTLEMENT);
        assertThat(testWalletTopupTransaction.getTxnAmount()).isEqualByComparingTo(DEFAULT_TXN_AMOUNT);
        assertThat(testWalletTopupTransaction.getInstructedAmount()).isEqualByComparingTo(DEFAULT_INSTRUCTED_AMOUNT);
        assertThat(testWalletTopupTransaction.getAmountInAccountCurrency()).isEqualByComparingTo(UPDATED_AMOUNT_IN_ACCOUNT_CURRENCY);
        assertThat(testWalletTopupTransaction.getAmountInLocalCurrency()).isEqualByComparingTo(DEFAULT_AMOUNT_IN_LOCAL_CURRENCY);
        assertThat(testWalletTopupTransaction.getChargeInAccountCurrency()).isEqualByComparingTo(UPDATED_CHARGE_IN_ACCOUNT_CURRENCY);
        assertThat(testWalletTopupTransaction.getChargeInLocalCurrency()).isEqualByComparingTo(DEFAULT_CHARGE_IN_LOCAL_CURRENCY);
        assertThat(testWalletTopupTransaction.getFundingSource()).isEqualTo(DEFAULT_FUNDING_SOURCE);
        assertThat(testWalletTopupTransaction.getMerchantID()).isEqualTo(UPDATED_MERCHANT_ID);
        assertThat(testWalletTopupTransaction.getMerchantInfo()).isEqualTo(UPDATED_MERCHANT_INFO);
        assertThat(testWalletTopupTransaction.getBankCode()).isEqualTo(UPDATED_BANK_CODE);
        assertThat(testWalletTopupTransaction.getExternalRefNo()).isEqualTo(UPDATED_EXTERNAL_REF_NO);
        assertThat(testWalletTopupTransaction.getTxnDescription()).isEqualTo(DEFAULT_TXN_DESCRIPTION);
        assertThat(testWalletTopupTransaction.getTxnInternalDescription()).isEqualTo(DEFAULT_TXN_INTERNAL_DESCRIPTION);
        assertThat(testWalletTopupTransaction.getTxnAddnlInfo()).isEqualTo(DEFAULT_TXN_ADDNL_INFO);
        assertThat(testWalletTopupTransaction.getCreatedOn()).isEqualTo(DEFAULT_CREATED_ON);
        assertThat(testWalletTopupTransaction.getCreatedBy()).isEqualTo(DEFAULT_CREATED_BY);
        assertThat(testWalletTopupTransaction.getTxnStatus()).isEqualTo(UPDATED_TXN_STATUS);
        assertThat(testWalletTopupTransaction.getTxnDebitAccountNo()).isEqualTo(DEFAULT_TXN_DEBIT_ACCOUNT_NO);
        assertThat(testWalletTopupTransaction.getTxnCreditAccountNo()).isEqualTo(UPDATED_TXN_CREDIT_ACCOUNT_NO);
        assertThat(testWalletTopupTransaction.getChargeDebitAccountNo()).isEqualTo(UPDATED_CHARGE_DEBIT_ACCOUNT_NO);
        assertThat(testWalletTopupTransaction.getChargeCreditAccountNo()).isEqualTo(DEFAULT_CHARGE_CREDIT_ACCOUNT_NO);
    }

    @Test
    @Transactional
    void fullUpdateWalletTopupTransactionWithPatch() throws Exception {
        // Initialize the database
        walletTopupTransactionRepository.saveAndFlush(walletTopupTransaction);

        int databaseSizeBeforeUpdate = walletTopupTransactionRepository.findAll().size();

        // Update the walletTopupTransaction using partial update
        WalletTopupTransaction partialUpdatedWalletTopupTransaction = new WalletTopupTransaction();
        partialUpdatedWalletTopupTransaction.setId(walletTopupTransaction.getId());

        partialUpdatedWalletTopupTransaction
            .txnRefNo(UPDATED_TXN_REF_NO)
            .accountNo(UPDATED_ACCOUNT_NO)
            .txnDate(UPDATED_TXN_DATE)
            .txnDateInUTC(UPDATED_TXN_DATE_IN_UTC)
            .txnDateInLocal(UPDATED_TXN_DATE_IN_LOCAL)
            .txnDateForSettlement(UPDATED_TXN_DATE_FOR_SETTLEMENT)
            .valueDate(UPDATED_VALUE_DATE)
            .valueDateInUTC(UPDATED_VALUE_DATE_IN_UTC)
            .valueDateInLocal(UPDATED_VALUE_DATE_IN_LOCAL)
            .valueDateForSettlement(UPDATED_VALUE_DATE_FOR_SETTLEMENT)
            .txnAmount(UPDATED_TXN_AMOUNT)
            .instructedAmount(UPDATED_INSTRUCTED_AMOUNT)
            .amountInAccountCurrency(UPDATED_AMOUNT_IN_ACCOUNT_CURRENCY)
            .amountInLocalCurrency(UPDATED_AMOUNT_IN_LOCAL_CURRENCY)
            .chargeInAccountCurrency(UPDATED_CHARGE_IN_ACCOUNT_CURRENCY)
            .chargeInLocalCurrency(UPDATED_CHARGE_IN_LOCAL_CURRENCY)
            .fundingSource(UPDATED_FUNDING_SOURCE)
            .merchantID(UPDATED_MERCHANT_ID)
            .merchantInfo(UPDATED_MERCHANT_INFO)
            .bankCode(UPDATED_BANK_CODE)
            .externalRefNo(UPDATED_EXTERNAL_REF_NO)
            .txnDescription(UPDATED_TXN_DESCRIPTION)
            .txnInternalDescription(UPDATED_TXN_INTERNAL_DESCRIPTION)
            .txnAddnlInfo(UPDATED_TXN_ADDNL_INFO)
            .createdOn(UPDATED_CREATED_ON)
            .createdBy(UPDATED_CREATED_BY)
            .txnStatus(UPDATED_TXN_STATUS)
            .txnDebitAccountNo(UPDATED_TXN_DEBIT_ACCOUNT_NO)
            .txnCreditAccountNo(UPDATED_TXN_CREDIT_ACCOUNT_NO)
            .chargeDebitAccountNo(UPDATED_CHARGE_DEBIT_ACCOUNT_NO)
            .chargeCreditAccountNo(UPDATED_CHARGE_CREDIT_ACCOUNT_NO);

        restWalletTopupTransactionMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedWalletTopupTransaction.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedWalletTopupTransaction))
            )
            .andExpect(status().isOk());

        // Validate the WalletTopupTransaction in the database
        List<WalletTopupTransaction> walletTopupTransactionList = walletTopupTransactionRepository.findAll();
        assertThat(walletTopupTransactionList).hasSize(databaseSizeBeforeUpdate);
        WalletTopupTransaction testWalletTopupTransaction = walletTopupTransactionList.get(walletTopupTransactionList.size() - 1);
        assertThat(testWalletTopupTransaction.getTxnRefNo()).isEqualTo(UPDATED_TXN_REF_NO);
        assertThat(testWalletTopupTransaction.getAccountNo()).isEqualTo(UPDATED_ACCOUNT_NO);
        assertThat(testWalletTopupTransaction.getTxnDate()).isEqualTo(UPDATED_TXN_DATE);
        assertThat(testWalletTopupTransaction.getTxnDateInUTC()).isEqualTo(UPDATED_TXN_DATE_IN_UTC);
        assertThat(testWalletTopupTransaction.getTxnDateInLocal()).isEqualTo(UPDATED_TXN_DATE_IN_LOCAL);
        assertThat(testWalletTopupTransaction.getTxnDateForSettlement()).isEqualTo(UPDATED_TXN_DATE_FOR_SETTLEMENT);
        assertThat(testWalletTopupTransaction.getValueDate()).isEqualTo(UPDATED_VALUE_DATE);
        assertThat(testWalletTopupTransaction.getValueDateInUTC()).isEqualTo(UPDATED_VALUE_DATE_IN_UTC);
        assertThat(testWalletTopupTransaction.getValueDateInLocal()).isEqualTo(UPDATED_VALUE_DATE_IN_LOCAL);
        assertThat(testWalletTopupTransaction.getValueDateForSettlement()).isEqualTo(UPDATED_VALUE_DATE_FOR_SETTLEMENT);
        assertThat(testWalletTopupTransaction.getTxnAmount()).isEqualByComparingTo(UPDATED_TXN_AMOUNT);
        assertThat(testWalletTopupTransaction.getInstructedAmount()).isEqualByComparingTo(UPDATED_INSTRUCTED_AMOUNT);
        assertThat(testWalletTopupTransaction.getAmountInAccountCurrency()).isEqualByComparingTo(UPDATED_AMOUNT_IN_ACCOUNT_CURRENCY);
        assertThat(testWalletTopupTransaction.getAmountInLocalCurrency()).isEqualByComparingTo(UPDATED_AMOUNT_IN_LOCAL_CURRENCY);
        assertThat(testWalletTopupTransaction.getChargeInAccountCurrency()).isEqualByComparingTo(UPDATED_CHARGE_IN_ACCOUNT_CURRENCY);
        assertThat(testWalletTopupTransaction.getChargeInLocalCurrency()).isEqualByComparingTo(UPDATED_CHARGE_IN_LOCAL_CURRENCY);
        assertThat(testWalletTopupTransaction.getFundingSource()).isEqualTo(UPDATED_FUNDING_SOURCE);
        assertThat(testWalletTopupTransaction.getMerchantID()).isEqualTo(UPDATED_MERCHANT_ID);
        assertThat(testWalletTopupTransaction.getMerchantInfo()).isEqualTo(UPDATED_MERCHANT_INFO);
        assertThat(testWalletTopupTransaction.getBankCode()).isEqualTo(UPDATED_BANK_CODE);
        assertThat(testWalletTopupTransaction.getExternalRefNo()).isEqualTo(UPDATED_EXTERNAL_REF_NO);
        assertThat(testWalletTopupTransaction.getTxnDescription()).isEqualTo(UPDATED_TXN_DESCRIPTION);
        assertThat(testWalletTopupTransaction.getTxnInternalDescription()).isEqualTo(UPDATED_TXN_INTERNAL_DESCRIPTION);
        assertThat(testWalletTopupTransaction.getTxnAddnlInfo()).isEqualTo(UPDATED_TXN_ADDNL_INFO);
        assertThat(testWalletTopupTransaction.getCreatedOn()).isEqualTo(UPDATED_CREATED_ON);
        assertThat(testWalletTopupTransaction.getCreatedBy()).isEqualTo(UPDATED_CREATED_BY);
        assertThat(testWalletTopupTransaction.getTxnStatus()).isEqualTo(UPDATED_TXN_STATUS);
        assertThat(testWalletTopupTransaction.getTxnDebitAccountNo()).isEqualTo(UPDATED_TXN_DEBIT_ACCOUNT_NO);
        assertThat(testWalletTopupTransaction.getTxnCreditAccountNo()).isEqualTo(UPDATED_TXN_CREDIT_ACCOUNT_NO);
        assertThat(testWalletTopupTransaction.getChargeDebitAccountNo()).isEqualTo(UPDATED_CHARGE_DEBIT_ACCOUNT_NO);
        assertThat(testWalletTopupTransaction.getChargeCreditAccountNo()).isEqualTo(UPDATED_CHARGE_CREDIT_ACCOUNT_NO);
    }

    @Test
    @Transactional
    void patchNonExistingWalletTopupTransaction() throws Exception {
        int databaseSizeBeforeUpdate = walletTopupTransactionRepository.findAll().size();
        walletTopupTransaction.setId(count.incrementAndGet());

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restWalletTopupTransactionMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, walletTopupTransaction.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(walletTopupTransaction))
            )
            .andExpect(status().isBadRequest());

        // Validate the WalletTopupTransaction in the database
        List<WalletTopupTransaction> walletTopupTransactionList = walletTopupTransactionRepository.findAll();
        assertThat(walletTopupTransactionList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithIdMismatchWalletTopupTransaction() throws Exception {
        int databaseSizeBeforeUpdate = walletTopupTransactionRepository.findAll().size();
        walletTopupTransaction.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restWalletTopupTransactionMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(walletTopupTransaction))
            )
            .andExpect(status().isBadRequest());

        // Validate the WalletTopupTransaction in the database
        List<WalletTopupTransaction> walletTopupTransactionList = walletTopupTransactionRepository.findAll();
        assertThat(walletTopupTransactionList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithMissingIdPathParamWalletTopupTransaction() throws Exception {
        int databaseSizeBeforeUpdate = walletTopupTransactionRepository.findAll().size();
        walletTopupTransaction.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restWalletTopupTransactionMockMvc
            .perform(
                patch(ENTITY_API_URL)
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(walletTopupTransaction))
            )
            .andExpect(status().isMethodNotAllowed());

        // Validate the WalletTopupTransaction in the database
        List<WalletTopupTransaction> walletTopupTransactionList = walletTopupTransactionRepository.findAll();
        assertThat(walletTopupTransactionList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void deleteWalletTopupTransaction() throws Exception {
        // Initialize the database
        walletTopupTransactionRepository.saveAndFlush(walletTopupTransaction);

        int databaseSizeBeforeDelete = walletTopupTransactionRepository.findAll().size();

        // Delete the walletTopupTransaction
        restWalletTopupTransactionMockMvc
            .perform(delete(ENTITY_API_URL_ID, walletTopupTransaction.getId()).accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<WalletTopupTransaction> walletTopupTransactionList = walletTopupTransactionRepository.findAll();
        assertThat(walletTopupTransactionList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
