package com.iconnect101.glapp.web.rest;

import static com.iconnect101.glapp.web.rest.TestUtil.sameInstant;
import static com.iconnect101.glapp.web.rest.TestUtil.sameNumber;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import com.iconnect101.glapp.IntegrationTest;
import com.iconnect101.glapp.domain.RemittanceTransaction;
import com.iconnect101.glapp.domain.enumeration.TxnStatus;
import com.iconnect101.glapp.repository.RemittanceTransactionRepository;
import java.math.BigDecimal;
import java.time.Instant;
import java.time.LocalDate;
import java.time.ZoneId;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.time.temporal.ChronoUnit;
import java.util.List;
import java.util.Random;
import java.util.concurrent.atomic.AtomicLong;
import javax.persistence.EntityManager;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;

/**
 * Integration tests for the {@link RemittanceTransactionResource} REST controller.
 */
@IntegrationTest
@AutoConfigureMockMvc
@WithMockUser
class RemittanceTransactionResourceIT {

    private static final String DEFAULT_TXN_REF_NO = "AAAAAAAAAA";
    private static final String UPDATED_TXN_REF_NO = "BBBBBBBBBB";

    private static final String DEFAULT_ACCOUNT_NO = "AAAAAAAAAA";
    private static final String UPDATED_ACCOUNT_NO = "BBBBBBBBBB";

    private static final LocalDate DEFAULT_TXN_DATE = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_TXN_DATE = LocalDate.now(ZoneId.systemDefault());

    private static final Instant DEFAULT_TXN_DATE_IN_UTC = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_TXN_DATE_IN_UTC = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    private static final ZonedDateTime DEFAULT_TXN_DATE_IN_LOCAL = ZonedDateTime.ofInstant(Instant.ofEpochMilli(0L), ZoneOffset.UTC);
    private static final ZonedDateTime UPDATED_TXN_DATE_IN_LOCAL = ZonedDateTime.now(ZoneId.systemDefault()).withNano(0);

    private static final ZonedDateTime DEFAULT_TXN_DATE_FOR_SETTLEMENT = ZonedDateTime.ofInstant(Instant.ofEpochMilli(0L), ZoneOffset.UTC);
    private static final ZonedDateTime UPDATED_TXN_DATE_FOR_SETTLEMENT = ZonedDateTime.now(ZoneId.systemDefault()).withNano(0);

    private static final LocalDate DEFAULT_VALUE_DATE = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_VALUE_DATE = LocalDate.now(ZoneId.systemDefault());

    private static final Instant DEFAULT_VALUE_DATE_IN_UTC = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_VALUE_DATE_IN_UTC = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    private static final ZonedDateTime DEFAULT_VALUE_DATE_IN_LOCAL = ZonedDateTime.ofInstant(Instant.ofEpochMilli(0L), ZoneOffset.UTC);
    private static final ZonedDateTime UPDATED_VALUE_DATE_IN_LOCAL = ZonedDateTime.now(ZoneId.systemDefault()).withNano(0);

    private static final ZonedDateTime DEFAULT_VALUE_DATE_FOR_SETTLEMENT = ZonedDateTime.ofInstant(
        Instant.ofEpochMilli(0L),
        ZoneOffset.UTC
    );
    private static final ZonedDateTime UPDATED_VALUE_DATE_FOR_SETTLEMENT = ZonedDateTime.now(ZoneId.systemDefault()).withNano(0);

    private static final String DEFAULT_BENEFICIARY_ACCOUNT_NO = "AAAAAAAAAA";
    private static final String UPDATED_BENEFICIARY_ACCOUNT_NO = "BBBBBBBBBB";

    private static final String DEFAULT_BENEFICIARY_NAME = "AAAAAAAAAA";
    private static final String UPDATED_BENEFICIARY_NAME = "BBBBBBBBBB";

    private static final String DEFAULT_BENEFICIARYBANK_SWIFT_CODE = "AAAAAAAAAA";
    private static final String UPDATED_BENEFICIARYBANK_SWIFT_CODE = "BBBBBBBBBB";

    private static final String DEFAULT_INTERMEDIARY_BANK_SWIFT_CODE = "AAAAAAAAAA";
    private static final String UPDATED_INTERMEDIARY_BANK_SWIFT_CODE = "BBBBBBBBBB";

    private static final String DEFAULT_BENEFICIARY_BANK_NAME = "AAAAAAAAAA";
    private static final String UPDATED_BENEFICIARY_BANK_NAME = "BBBBBBBBBB";

    private static final String DEFAULT_BENEFICIARY_BANK_BRANCH_NAME = "AAAAAAAAAA";
    private static final String UPDATED_BENEFICIARY_BANK_BRANCH_NAME = "BBBBBBBBBB";

    private static final String DEFAULT_BENEFICIARY_ADDRESS_1 = "AAAAAAAAAA";
    private static final String UPDATED_BENEFICIARY_ADDRESS_1 = "BBBBBBBBBB";

    private static final String DEFAULT_BENEFICIARY_ADDRESS_2 = "AAAAAAAAAA";
    private static final String UPDATED_BENEFICIARY_ADDRESS_2 = "BBBBBBBBBB";

    private static final String DEFAULT_BENEFICIARY_ADDRESS_3 = "AAAAAAAAAA";
    private static final String UPDATED_BENEFICIARY_ADDRESS_3 = "BBBBBBBBBB";

    private static final String DEFAULT_BENEFICIARY_STATE = "AAAAAAAAAA";
    private static final String UPDATED_BENEFICIARY_STATE = "BBBBBBBBBB";

    private static final String DEFAULT_BENEFICIARY_PIN_CODE = "AAAAAAAAAA";
    private static final String UPDATED_BENEFICIARY_PIN_CODE = "BBBBBBBBBB";

    private static final BigDecimal DEFAULT_TXN_AMOUNT = new BigDecimal(1);
    private static final BigDecimal UPDATED_TXN_AMOUNT = new BigDecimal(2);

    private static final BigDecimal DEFAULT_AMOUNT_IN_ACCOUNT_CURRENCY = new BigDecimal(1);
    private static final BigDecimal UPDATED_AMOUNT_IN_ACCOUNT_CURRENCY = new BigDecimal(2);

    private static final BigDecimal DEFAULT_AMOUNT_IN_LOCAL_CURRENCY = new BigDecimal(1);
    private static final BigDecimal UPDATED_AMOUNT_IN_LOCAL_CURRENCY = new BigDecimal(2);

    private static final BigDecimal DEFAULT_CHARGE_IN_ACCOUNT_CURRENCY = new BigDecimal(1);
    private static final BigDecimal UPDATED_CHARGE_IN_ACCOUNT_CURRENCY = new BigDecimal(2);

    private static final BigDecimal DEFAULT_CHARGE_IN_LOCAL_CURRENCY = new BigDecimal(1);
    private static final BigDecimal UPDATED_CHARGE_IN_LOCAL_CURRENCY = new BigDecimal(2);

    private static final String DEFAULT_FUNDING_SOURCE = "AAAAAAAAAA";
    private static final String UPDATED_FUNDING_SOURCE = "BBBBBBBBBB";

    private static final BigDecimal DEFAULT_RATE = new BigDecimal(1);
    private static final BigDecimal UPDATED_RATE = new BigDecimal(2);

    private static final BigDecimal DEFAULT_INSTRUCTED_AMOUNT = new BigDecimal(1);
    private static final BigDecimal UPDATED_INSTRUCTED_AMOUNT = new BigDecimal(2);

    private static final String DEFAULT_INSTRUCTED_AMOUNT_CURRENCY = "AAAAAAAAAA";
    private static final String UPDATED_INSTRUCTED_AMOUNT_CURRENCY = "BBBBBBBBBB";

    private static final String DEFAULT_MERCHANT_ID = "AAAAAAAAAA";
    private static final String UPDATED_MERCHANT_ID = "BBBBBBBBBB";

    private static final String DEFAULT_MERCHANT_INFO = "AAAAAAAAAA";
    private static final String UPDATED_MERCHANT_INFO = "BBBBBBBBBB";

    private static final String DEFAULT_EXTERNAL_REF_NO = "AAAAAAAAAA";
    private static final String UPDATED_EXTERNAL_REF_NO = "BBBBBBBBBB";

    private static final String DEFAULT_TXN_DESCRIPTION = "AAAAAAAAAA";
    private static final String UPDATED_TXN_DESCRIPTION = "BBBBBBBBBB";

    private static final String DEFAULT_TXN_INTERNAL_DESCRIPTION = "AAAAAAAAAA";
    private static final String UPDATED_TXN_INTERNAL_DESCRIPTION = "BBBBBBBBBB";

    private static final String DEFAULT_TXN_ADDNL_INFO = "AAAAAAAAAA";
    private static final String UPDATED_TXN_ADDNL_INFO = "BBBBBBBBBB";

    private static final Instant DEFAULT_CREATED_ON = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_CREATED_ON = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    private static final String DEFAULT_CREATED_BY = "AAAAAAAAAA";
    private static final String UPDATED_CREATED_BY = "BBBBBBBBBB";

    private static final TxnStatus DEFAULT_TXN_STATUS = TxnStatus.ACTIVE;
    private static final TxnStatus UPDATED_TXN_STATUS = TxnStatus.CANCELLED;

    private static final String DEFAULT_TXN_DEBIT_ACCOUNT_NO = "AAAAAAAAAA";
    private static final String UPDATED_TXN_DEBIT_ACCOUNT_NO = "BBBBBBBBBB";

    private static final String DEFAULT_TXN_CREDIT_ACCOUNT_NO = "AAAAAAAAAA";
    private static final String UPDATED_TXN_CREDIT_ACCOUNT_NO = "BBBBBBBBBB";

    private static final String DEFAULT_CHARGE_DEBIT_ACCOUNT_NO = "AAAAAAAAAA";
    private static final String UPDATED_CHARGE_DEBIT_ACCOUNT_NO = "BBBBBBBBBB";

    private static final String DEFAULT_CHARGE_CREDIT_ACCOUNT_NO = "AAAAAAAAAA";
    private static final String UPDATED_CHARGE_CREDIT_ACCOUNT_NO = "BBBBBBBBBB";

    private static final String ENTITY_API_URL = "/api/remittance-transactions";
    private static final String ENTITY_API_URL_ID = ENTITY_API_URL + "/{id}";

    private static Random random = new Random();
    private static AtomicLong count = new AtomicLong(random.nextInt() + (2 * Integer.MAX_VALUE));

    @Autowired
    private RemittanceTransactionRepository remittanceTransactionRepository;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restRemittanceTransactionMockMvc;

    private RemittanceTransaction remittanceTransaction;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static RemittanceTransaction createEntity(EntityManager em) {
        RemittanceTransaction remittanceTransaction = new RemittanceTransaction()
            .txnRefNo(DEFAULT_TXN_REF_NO)
            .accountNo(DEFAULT_ACCOUNT_NO)
            .txnDate(DEFAULT_TXN_DATE)
            .txnDateInUTC(DEFAULT_TXN_DATE_IN_UTC)
            .txnDateInLocal(DEFAULT_TXN_DATE_IN_LOCAL)
            .txnDateForSettlement(DEFAULT_TXN_DATE_FOR_SETTLEMENT)
            .valueDate(DEFAULT_VALUE_DATE)
            .valueDateInUTC(DEFAULT_VALUE_DATE_IN_UTC)
            .valueDateInLocal(DEFAULT_VALUE_DATE_IN_LOCAL)
            .valueDateForSettlement(DEFAULT_VALUE_DATE_FOR_SETTLEMENT)
            .beneficiaryAccountNo(DEFAULT_BENEFICIARY_ACCOUNT_NO)
            .beneficiaryName(DEFAULT_BENEFICIARY_NAME)
            .beneficiarybankSwiftCode(DEFAULT_BENEFICIARYBANK_SWIFT_CODE)
            .intermediaryBankSwiftCode(DEFAULT_INTERMEDIARY_BANK_SWIFT_CODE)
            .beneficiaryBankName(DEFAULT_BENEFICIARY_BANK_NAME)
            .beneficiaryBankBranchName(DEFAULT_BENEFICIARY_BANK_BRANCH_NAME)
            .beneficiaryAddress1(DEFAULT_BENEFICIARY_ADDRESS_1)
            .beneficiaryAddress2(DEFAULT_BENEFICIARY_ADDRESS_2)
            .beneficiaryAddress3(DEFAULT_BENEFICIARY_ADDRESS_3)
            .beneficiaryState(DEFAULT_BENEFICIARY_STATE)
            .beneficiaryPinCode(DEFAULT_BENEFICIARY_PIN_CODE)
            .txnAmount(DEFAULT_TXN_AMOUNT)
            .amountInAccountCurrency(DEFAULT_AMOUNT_IN_ACCOUNT_CURRENCY)
            .amountInLocalCurrency(DEFAULT_AMOUNT_IN_LOCAL_CURRENCY)
            .chargeInAccountCurrency(DEFAULT_CHARGE_IN_ACCOUNT_CURRENCY)
            .chargeInLocalCurrency(DEFAULT_CHARGE_IN_LOCAL_CURRENCY)
            .fundingSource(DEFAULT_FUNDING_SOURCE)
            .rate(DEFAULT_RATE)
            .instructedAmount(DEFAULT_INSTRUCTED_AMOUNT)
            .instructedAmountCurrency(DEFAULT_INSTRUCTED_AMOUNT_CURRENCY)
            .merchantID(DEFAULT_MERCHANT_ID)
            .merchantInfo(DEFAULT_MERCHANT_INFO)
            .externalRefNo(DEFAULT_EXTERNAL_REF_NO)
            .txnDescription(DEFAULT_TXN_DESCRIPTION)
            .txnInternalDescription(DEFAULT_TXN_INTERNAL_DESCRIPTION)
            .txnAddnlInfo(DEFAULT_TXN_ADDNL_INFO)
            .createdOn(DEFAULT_CREATED_ON)
            .createdBy(DEFAULT_CREATED_BY)
            .txnStatus(DEFAULT_TXN_STATUS)
            .txnDebitAccountNo(DEFAULT_TXN_DEBIT_ACCOUNT_NO)
            .txnCreditAccountNo(DEFAULT_TXN_CREDIT_ACCOUNT_NO)
            .chargeDebitAccountNo(DEFAULT_CHARGE_DEBIT_ACCOUNT_NO)
            .chargeCreditAccountNo(DEFAULT_CHARGE_CREDIT_ACCOUNT_NO);
        return remittanceTransaction;
    }

    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static RemittanceTransaction createUpdatedEntity(EntityManager em) {
        RemittanceTransaction remittanceTransaction = new RemittanceTransaction()
            .txnRefNo(UPDATED_TXN_REF_NO)
            .accountNo(UPDATED_ACCOUNT_NO)
            .txnDate(UPDATED_TXN_DATE)
            .txnDateInUTC(UPDATED_TXN_DATE_IN_UTC)
            .txnDateInLocal(UPDATED_TXN_DATE_IN_LOCAL)
            .txnDateForSettlement(UPDATED_TXN_DATE_FOR_SETTLEMENT)
            .valueDate(UPDATED_VALUE_DATE)
            .valueDateInUTC(UPDATED_VALUE_DATE_IN_UTC)
            .valueDateInLocal(UPDATED_VALUE_DATE_IN_LOCAL)
            .valueDateForSettlement(UPDATED_VALUE_DATE_FOR_SETTLEMENT)
            .beneficiaryAccountNo(UPDATED_BENEFICIARY_ACCOUNT_NO)
            .beneficiaryName(UPDATED_BENEFICIARY_NAME)
            .beneficiarybankSwiftCode(UPDATED_BENEFICIARYBANK_SWIFT_CODE)
            .intermediaryBankSwiftCode(UPDATED_INTERMEDIARY_BANK_SWIFT_CODE)
            .beneficiaryBankName(UPDATED_BENEFICIARY_BANK_NAME)
            .beneficiaryBankBranchName(UPDATED_BENEFICIARY_BANK_BRANCH_NAME)
            .beneficiaryAddress1(UPDATED_BENEFICIARY_ADDRESS_1)
            .beneficiaryAddress2(UPDATED_BENEFICIARY_ADDRESS_2)
            .beneficiaryAddress3(UPDATED_BENEFICIARY_ADDRESS_3)
            .beneficiaryState(UPDATED_BENEFICIARY_STATE)
            .beneficiaryPinCode(UPDATED_BENEFICIARY_PIN_CODE)
            .txnAmount(UPDATED_TXN_AMOUNT)
            .amountInAccountCurrency(UPDATED_AMOUNT_IN_ACCOUNT_CURRENCY)
            .amountInLocalCurrency(UPDATED_AMOUNT_IN_LOCAL_CURRENCY)
            .chargeInAccountCurrency(UPDATED_CHARGE_IN_ACCOUNT_CURRENCY)
            .chargeInLocalCurrency(UPDATED_CHARGE_IN_LOCAL_CURRENCY)
            .fundingSource(UPDATED_FUNDING_SOURCE)
            .rate(UPDATED_RATE)
            .instructedAmount(UPDATED_INSTRUCTED_AMOUNT)
            .instructedAmountCurrency(UPDATED_INSTRUCTED_AMOUNT_CURRENCY)
            .merchantID(UPDATED_MERCHANT_ID)
            .merchantInfo(UPDATED_MERCHANT_INFO)
            .externalRefNo(UPDATED_EXTERNAL_REF_NO)
            .txnDescription(UPDATED_TXN_DESCRIPTION)
            .txnInternalDescription(UPDATED_TXN_INTERNAL_DESCRIPTION)
            .txnAddnlInfo(UPDATED_TXN_ADDNL_INFO)
            .createdOn(UPDATED_CREATED_ON)
            .createdBy(UPDATED_CREATED_BY)
            .txnStatus(UPDATED_TXN_STATUS)
            .txnDebitAccountNo(UPDATED_TXN_DEBIT_ACCOUNT_NO)
            .txnCreditAccountNo(UPDATED_TXN_CREDIT_ACCOUNT_NO)
            .chargeDebitAccountNo(UPDATED_CHARGE_DEBIT_ACCOUNT_NO)
            .chargeCreditAccountNo(UPDATED_CHARGE_CREDIT_ACCOUNT_NO);
        return remittanceTransaction;
    }

    @BeforeEach
    public void initTest() {
        remittanceTransaction = createEntity(em);
    }

    @Test
    @Transactional
    void createRemittanceTransaction() throws Exception {
        int databaseSizeBeforeCreate = remittanceTransactionRepository.findAll().size();
        // Create the RemittanceTransaction
        restRemittanceTransactionMockMvc
            .perform(
                post(ENTITY_API_URL)
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(remittanceTransaction))
            )
            .andExpect(status().isCreated());

        // Validate the RemittanceTransaction in the database
        List<RemittanceTransaction> remittanceTransactionList = remittanceTransactionRepository.findAll();
        assertThat(remittanceTransactionList).hasSize(databaseSizeBeforeCreate + 1);
        RemittanceTransaction testRemittanceTransaction = remittanceTransactionList.get(remittanceTransactionList.size() - 1);
        assertThat(testRemittanceTransaction.getTxnRefNo()).isEqualTo(DEFAULT_TXN_REF_NO);
        assertThat(testRemittanceTransaction.getAccountNo()).isEqualTo(DEFAULT_ACCOUNT_NO);
        assertThat(testRemittanceTransaction.getTxnDate()).isEqualTo(DEFAULT_TXN_DATE);
        assertThat(testRemittanceTransaction.getTxnDateInUTC()).isEqualTo(DEFAULT_TXN_DATE_IN_UTC);
        assertThat(testRemittanceTransaction.getTxnDateInLocal()).isEqualTo(DEFAULT_TXN_DATE_IN_LOCAL);
        assertThat(testRemittanceTransaction.getTxnDateForSettlement()).isEqualTo(DEFAULT_TXN_DATE_FOR_SETTLEMENT);
        assertThat(testRemittanceTransaction.getValueDate()).isEqualTo(DEFAULT_VALUE_DATE);
        assertThat(testRemittanceTransaction.getValueDateInUTC()).isEqualTo(DEFAULT_VALUE_DATE_IN_UTC);
        assertThat(testRemittanceTransaction.getValueDateInLocal()).isEqualTo(DEFAULT_VALUE_DATE_IN_LOCAL);
        assertThat(testRemittanceTransaction.getValueDateForSettlement()).isEqualTo(DEFAULT_VALUE_DATE_FOR_SETTLEMENT);
        assertThat(testRemittanceTransaction.getBeneficiaryAccountNo()).isEqualTo(DEFAULT_BENEFICIARY_ACCOUNT_NO);
        assertThat(testRemittanceTransaction.getBeneficiaryName()).isEqualTo(DEFAULT_BENEFICIARY_NAME);
        assertThat(testRemittanceTransaction.getBeneficiarybankSwiftCode()).isEqualTo(DEFAULT_BENEFICIARYBANK_SWIFT_CODE);
        assertThat(testRemittanceTransaction.getIntermediaryBankSwiftCode()).isEqualTo(DEFAULT_INTERMEDIARY_BANK_SWIFT_CODE);
        assertThat(testRemittanceTransaction.getBeneficiaryBankName()).isEqualTo(DEFAULT_BENEFICIARY_BANK_NAME);
        assertThat(testRemittanceTransaction.getBeneficiaryBankBranchName()).isEqualTo(DEFAULT_BENEFICIARY_BANK_BRANCH_NAME);
        assertThat(testRemittanceTransaction.getBeneficiaryAddress1()).isEqualTo(DEFAULT_BENEFICIARY_ADDRESS_1);
        assertThat(testRemittanceTransaction.getBeneficiaryAddress2()).isEqualTo(DEFAULT_BENEFICIARY_ADDRESS_2);
        assertThat(testRemittanceTransaction.getBeneficiaryAddress3()).isEqualTo(DEFAULT_BENEFICIARY_ADDRESS_3);
        assertThat(testRemittanceTransaction.getBeneficiaryState()).isEqualTo(DEFAULT_BENEFICIARY_STATE);
        assertThat(testRemittanceTransaction.getBeneficiaryPinCode()).isEqualTo(DEFAULT_BENEFICIARY_PIN_CODE);
        assertThat(testRemittanceTransaction.getTxnAmount()).isEqualByComparingTo(DEFAULT_TXN_AMOUNT);
        assertThat(testRemittanceTransaction.getAmountInAccountCurrency()).isEqualByComparingTo(DEFAULT_AMOUNT_IN_ACCOUNT_CURRENCY);
        assertThat(testRemittanceTransaction.getAmountInLocalCurrency()).isEqualByComparingTo(DEFAULT_AMOUNT_IN_LOCAL_CURRENCY);
        assertThat(testRemittanceTransaction.getChargeInAccountCurrency()).isEqualByComparingTo(DEFAULT_CHARGE_IN_ACCOUNT_CURRENCY);
        assertThat(testRemittanceTransaction.getChargeInLocalCurrency()).isEqualByComparingTo(DEFAULT_CHARGE_IN_LOCAL_CURRENCY);
        assertThat(testRemittanceTransaction.getFundingSource()).isEqualTo(DEFAULT_FUNDING_SOURCE);
        assertThat(testRemittanceTransaction.getRate()).isEqualByComparingTo(DEFAULT_RATE);
        assertThat(testRemittanceTransaction.getInstructedAmount()).isEqualByComparingTo(DEFAULT_INSTRUCTED_AMOUNT);
        assertThat(testRemittanceTransaction.getInstructedAmountCurrency()).isEqualTo(DEFAULT_INSTRUCTED_AMOUNT_CURRENCY);
        assertThat(testRemittanceTransaction.getMerchantID()).isEqualTo(DEFAULT_MERCHANT_ID);
        assertThat(testRemittanceTransaction.getMerchantInfo()).isEqualTo(DEFAULT_MERCHANT_INFO);
        assertThat(testRemittanceTransaction.getExternalRefNo()).isEqualTo(DEFAULT_EXTERNAL_REF_NO);
        assertThat(testRemittanceTransaction.getTxnDescription()).isEqualTo(DEFAULT_TXN_DESCRIPTION);
        assertThat(testRemittanceTransaction.getTxnInternalDescription()).isEqualTo(DEFAULT_TXN_INTERNAL_DESCRIPTION);
        assertThat(testRemittanceTransaction.getTxnAddnlInfo()).isEqualTo(DEFAULT_TXN_ADDNL_INFO);
        assertThat(testRemittanceTransaction.getCreatedOn()).isEqualTo(DEFAULT_CREATED_ON);
        assertThat(testRemittanceTransaction.getCreatedBy()).isEqualTo(DEFAULT_CREATED_BY);
        assertThat(testRemittanceTransaction.getTxnStatus()).isEqualTo(DEFAULT_TXN_STATUS);
        assertThat(testRemittanceTransaction.getTxnDebitAccountNo()).isEqualTo(DEFAULT_TXN_DEBIT_ACCOUNT_NO);
        assertThat(testRemittanceTransaction.getTxnCreditAccountNo()).isEqualTo(DEFAULT_TXN_CREDIT_ACCOUNT_NO);
        assertThat(testRemittanceTransaction.getChargeDebitAccountNo()).isEqualTo(DEFAULT_CHARGE_DEBIT_ACCOUNT_NO);
        assertThat(testRemittanceTransaction.getChargeCreditAccountNo()).isEqualTo(DEFAULT_CHARGE_CREDIT_ACCOUNT_NO);
    }

    @Test
    @Transactional
    void createRemittanceTransactionWithExistingId() throws Exception {
        // Create the RemittanceTransaction with an existing ID
        remittanceTransaction.setId(1L);

        int databaseSizeBeforeCreate = remittanceTransactionRepository.findAll().size();

        // An entity with an existing ID cannot be created, so this API call must fail
        restRemittanceTransactionMockMvc
            .perform(
                post(ENTITY_API_URL)
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(remittanceTransaction))
            )
            .andExpect(status().isBadRequest());

        // Validate the RemittanceTransaction in the database
        List<RemittanceTransaction> remittanceTransactionList = remittanceTransactionRepository.findAll();
        assertThat(remittanceTransactionList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    void getAllRemittanceTransactions() throws Exception {
        // Initialize the database
        remittanceTransactionRepository.saveAndFlush(remittanceTransaction);

        // Get all the remittanceTransactionList
        restRemittanceTransactionMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(remittanceTransaction.getId().intValue())))
            .andExpect(jsonPath("$.[*].txnRefNo").value(hasItem(DEFAULT_TXN_REF_NO)))
            .andExpect(jsonPath("$.[*].accountNo").value(hasItem(DEFAULT_ACCOUNT_NO)))
            .andExpect(jsonPath("$.[*].txnDate").value(hasItem(DEFAULT_TXN_DATE.toString())))
            .andExpect(jsonPath("$.[*].txnDateInUTC").value(hasItem(DEFAULT_TXN_DATE_IN_UTC.toString())))
            .andExpect(jsonPath("$.[*].txnDateInLocal").value(hasItem(sameInstant(DEFAULT_TXN_DATE_IN_LOCAL))))
            .andExpect(jsonPath("$.[*].txnDateForSettlement").value(hasItem(sameInstant(DEFAULT_TXN_DATE_FOR_SETTLEMENT))))
            .andExpect(jsonPath("$.[*].valueDate").value(hasItem(DEFAULT_VALUE_DATE.toString())))
            .andExpect(jsonPath("$.[*].valueDateInUTC").value(hasItem(DEFAULT_VALUE_DATE_IN_UTC.toString())))
            .andExpect(jsonPath("$.[*].valueDateInLocal").value(hasItem(sameInstant(DEFAULT_VALUE_DATE_IN_LOCAL))))
            .andExpect(jsonPath("$.[*].valueDateForSettlement").value(hasItem(sameInstant(DEFAULT_VALUE_DATE_FOR_SETTLEMENT))))
            .andExpect(jsonPath("$.[*].beneficiaryAccountNo").value(hasItem(DEFAULT_BENEFICIARY_ACCOUNT_NO)))
            .andExpect(jsonPath("$.[*].beneficiaryName").value(hasItem(DEFAULT_BENEFICIARY_NAME)))
            .andExpect(jsonPath("$.[*].beneficiarybankSwiftCode").value(hasItem(DEFAULT_BENEFICIARYBANK_SWIFT_CODE)))
            .andExpect(jsonPath("$.[*].intermediaryBankSwiftCode").value(hasItem(DEFAULT_INTERMEDIARY_BANK_SWIFT_CODE)))
            .andExpect(jsonPath("$.[*].beneficiaryBankName").value(hasItem(DEFAULT_BENEFICIARY_BANK_NAME)))
            .andExpect(jsonPath("$.[*].beneficiaryBankBranchName").value(hasItem(DEFAULT_BENEFICIARY_BANK_BRANCH_NAME)))
            .andExpect(jsonPath("$.[*].beneficiaryAddress1").value(hasItem(DEFAULT_BENEFICIARY_ADDRESS_1)))
            .andExpect(jsonPath("$.[*].beneficiaryAddress2").value(hasItem(DEFAULT_BENEFICIARY_ADDRESS_2)))
            .andExpect(jsonPath("$.[*].beneficiaryAddress3").value(hasItem(DEFAULT_BENEFICIARY_ADDRESS_3)))
            .andExpect(jsonPath("$.[*].beneficiaryState").value(hasItem(DEFAULT_BENEFICIARY_STATE)))
            .andExpect(jsonPath("$.[*].beneficiaryPinCode").value(hasItem(DEFAULT_BENEFICIARY_PIN_CODE)))
            .andExpect(jsonPath("$.[*].txnAmount").value(hasItem(sameNumber(DEFAULT_TXN_AMOUNT))))
            .andExpect(jsonPath("$.[*].amountInAccountCurrency").value(hasItem(sameNumber(DEFAULT_AMOUNT_IN_ACCOUNT_CURRENCY))))
            .andExpect(jsonPath("$.[*].amountInLocalCurrency").value(hasItem(sameNumber(DEFAULT_AMOUNT_IN_LOCAL_CURRENCY))))
            .andExpect(jsonPath("$.[*].chargeInAccountCurrency").value(hasItem(sameNumber(DEFAULT_CHARGE_IN_ACCOUNT_CURRENCY))))
            .andExpect(jsonPath("$.[*].chargeInLocalCurrency").value(hasItem(sameNumber(DEFAULT_CHARGE_IN_LOCAL_CURRENCY))))
            .andExpect(jsonPath("$.[*].fundingSource").value(hasItem(DEFAULT_FUNDING_SOURCE)))
            .andExpect(jsonPath("$.[*].rate").value(hasItem(sameNumber(DEFAULT_RATE))))
            .andExpect(jsonPath("$.[*].instructedAmount").value(hasItem(sameNumber(DEFAULT_INSTRUCTED_AMOUNT))))
            .andExpect(jsonPath("$.[*].instructedAmountCurrency").value(hasItem(DEFAULT_INSTRUCTED_AMOUNT_CURRENCY)))
            .andExpect(jsonPath("$.[*].merchantID").value(hasItem(DEFAULT_MERCHANT_ID)))
            .andExpect(jsonPath("$.[*].merchantInfo").value(hasItem(DEFAULT_MERCHANT_INFO)))
            .andExpect(jsonPath("$.[*].externalRefNo").value(hasItem(DEFAULT_EXTERNAL_REF_NO)))
            .andExpect(jsonPath("$.[*].txnDescription").value(hasItem(DEFAULT_TXN_DESCRIPTION)))
            .andExpect(jsonPath("$.[*].txnInternalDescription").value(hasItem(DEFAULT_TXN_INTERNAL_DESCRIPTION)))
            .andExpect(jsonPath("$.[*].txnAddnlInfo").value(hasItem(DEFAULT_TXN_ADDNL_INFO)))
            .andExpect(jsonPath("$.[*].createdOn").value(hasItem(DEFAULT_CREATED_ON.toString())))
            .andExpect(jsonPath("$.[*].createdBy").value(hasItem(DEFAULT_CREATED_BY)))
            .andExpect(jsonPath("$.[*].txnStatus").value(hasItem(DEFAULT_TXN_STATUS.toString())))
            .andExpect(jsonPath("$.[*].txnDebitAccountNo").value(hasItem(DEFAULT_TXN_DEBIT_ACCOUNT_NO)))
            .andExpect(jsonPath("$.[*].txnCreditAccountNo").value(hasItem(DEFAULT_TXN_CREDIT_ACCOUNT_NO)))
            .andExpect(jsonPath("$.[*].chargeDebitAccountNo").value(hasItem(DEFAULT_CHARGE_DEBIT_ACCOUNT_NO)))
            .andExpect(jsonPath("$.[*].chargeCreditAccountNo").value(hasItem(DEFAULT_CHARGE_CREDIT_ACCOUNT_NO)));
    }

    @Test
    @Transactional
    void getRemittanceTransaction() throws Exception {
        // Initialize the database
        remittanceTransactionRepository.saveAndFlush(remittanceTransaction);

        // Get the remittanceTransaction
        restRemittanceTransactionMockMvc
            .perform(get(ENTITY_API_URL_ID, remittanceTransaction.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(remittanceTransaction.getId().intValue()))
            .andExpect(jsonPath("$.txnRefNo").value(DEFAULT_TXN_REF_NO))
            .andExpect(jsonPath("$.accountNo").value(DEFAULT_ACCOUNT_NO))
            .andExpect(jsonPath("$.txnDate").value(DEFAULT_TXN_DATE.toString()))
            .andExpect(jsonPath("$.txnDateInUTC").value(DEFAULT_TXN_DATE_IN_UTC.toString()))
            .andExpect(jsonPath("$.txnDateInLocal").value(sameInstant(DEFAULT_TXN_DATE_IN_LOCAL)))
            .andExpect(jsonPath("$.txnDateForSettlement").value(sameInstant(DEFAULT_TXN_DATE_FOR_SETTLEMENT)))
            .andExpect(jsonPath("$.valueDate").value(DEFAULT_VALUE_DATE.toString()))
            .andExpect(jsonPath("$.valueDateInUTC").value(DEFAULT_VALUE_DATE_IN_UTC.toString()))
            .andExpect(jsonPath("$.valueDateInLocal").value(sameInstant(DEFAULT_VALUE_DATE_IN_LOCAL)))
            .andExpect(jsonPath("$.valueDateForSettlement").value(sameInstant(DEFAULT_VALUE_DATE_FOR_SETTLEMENT)))
            .andExpect(jsonPath("$.beneficiaryAccountNo").value(DEFAULT_BENEFICIARY_ACCOUNT_NO))
            .andExpect(jsonPath("$.beneficiaryName").value(DEFAULT_BENEFICIARY_NAME))
            .andExpect(jsonPath("$.beneficiarybankSwiftCode").value(DEFAULT_BENEFICIARYBANK_SWIFT_CODE))
            .andExpect(jsonPath("$.intermediaryBankSwiftCode").value(DEFAULT_INTERMEDIARY_BANK_SWIFT_CODE))
            .andExpect(jsonPath("$.beneficiaryBankName").value(DEFAULT_BENEFICIARY_BANK_NAME))
            .andExpect(jsonPath("$.beneficiaryBankBranchName").value(DEFAULT_BENEFICIARY_BANK_BRANCH_NAME))
            .andExpect(jsonPath("$.beneficiaryAddress1").value(DEFAULT_BENEFICIARY_ADDRESS_1))
            .andExpect(jsonPath("$.beneficiaryAddress2").value(DEFAULT_BENEFICIARY_ADDRESS_2))
            .andExpect(jsonPath("$.beneficiaryAddress3").value(DEFAULT_BENEFICIARY_ADDRESS_3))
            .andExpect(jsonPath("$.beneficiaryState").value(DEFAULT_BENEFICIARY_STATE))
            .andExpect(jsonPath("$.beneficiaryPinCode").value(DEFAULT_BENEFICIARY_PIN_CODE))
            .andExpect(jsonPath("$.txnAmount").value(sameNumber(DEFAULT_TXN_AMOUNT)))
            .andExpect(jsonPath("$.amountInAccountCurrency").value(sameNumber(DEFAULT_AMOUNT_IN_ACCOUNT_CURRENCY)))
            .andExpect(jsonPath("$.amountInLocalCurrency").value(sameNumber(DEFAULT_AMOUNT_IN_LOCAL_CURRENCY)))
            .andExpect(jsonPath("$.chargeInAccountCurrency").value(sameNumber(DEFAULT_CHARGE_IN_ACCOUNT_CURRENCY)))
            .andExpect(jsonPath("$.chargeInLocalCurrency").value(sameNumber(DEFAULT_CHARGE_IN_LOCAL_CURRENCY)))
            .andExpect(jsonPath("$.fundingSource").value(DEFAULT_FUNDING_SOURCE))
            .andExpect(jsonPath("$.rate").value(sameNumber(DEFAULT_RATE)))
            .andExpect(jsonPath("$.instructedAmount").value(sameNumber(DEFAULT_INSTRUCTED_AMOUNT)))
            .andExpect(jsonPath("$.instructedAmountCurrency").value(DEFAULT_INSTRUCTED_AMOUNT_CURRENCY))
            .andExpect(jsonPath("$.merchantID").value(DEFAULT_MERCHANT_ID))
            .andExpect(jsonPath("$.merchantInfo").value(DEFAULT_MERCHANT_INFO))
            .andExpect(jsonPath("$.externalRefNo").value(DEFAULT_EXTERNAL_REF_NO))
            .andExpect(jsonPath("$.txnDescription").value(DEFAULT_TXN_DESCRIPTION))
            .andExpect(jsonPath("$.txnInternalDescription").value(DEFAULT_TXN_INTERNAL_DESCRIPTION))
            .andExpect(jsonPath("$.txnAddnlInfo").value(DEFAULT_TXN_ADDNL_INFO))
            .andExpect(jsonPath("$.createdOn").value(DEFAULT_CREATED_ON.toString()))
            .andExpect(jsonPath("$.createdBy").value(DEFAULT_CREATED_BY))
            .andExpect(jsonPath("$.txnStatus").value(DEFAULT_TXN_STATUS.toString()))
            .andExpect(jsonPath("$.txnDebitAccountNo").value(DEFAULT_TXN_DEBIT_ACCOUNT_NO))
            .andExpect(jsonPath("$.txnCreditAccountNo").value(DEFAULT_TXN_CREDIT_ACCOUNT_NO))
            .andExpect(jsonPath("$.chargeDebitAccountNo").value(DEFAULT_CHARGE_DEBIT_ACCOUNT_NO))
            .andExpect(jsonPath("$.chargeCreditAccountNo").value(DEFAULT_CHARGE_CREDIT_ACCOUNT_NO));
    }

    @Test
    @Transactional
    void getNonExistingRemittanceTransaction() throws Exception {
        // Get the remittanceTransaction
        restRemittanceTransactionMockMvc.perform(get(ENTITY_API_URL_ID, Long.MAX_VALUE)).andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    void putNewRemittanceTransaction() throws Exception {
        // Initialize the database
        remittanceTransactionRepository.saveAndFlush(remittanceTransaction);

        int databaseSizeBeforeUpdate = remittanceTransactionRepository.findAll().size();

        // Update the remittanceTransaction
        RemittanceTransaction updatedRemittanceTransaction = remittanceTransactionRepository.findById(remittanceTransaction.getId()).get();
        // Disconnect from session so that the updates on updatedRemittanceTransaction are not directly saved in db
        em.detach(updatedRemittanceTransaction);
        updatedRemittanceTransaction
            .txnRefNo(UPDATED_TXN_REF_NO)
            .accountNo(UPDATED_ACCOUNT_NO)
            .txnDate(UPDATED_TXN_DATE)
            .txnDateInUTC(UPDATED_TXN_DATE_IN_UTC)
            .txnDateInLocal(UPDATED_TXN_DATE_IN_LOCAL)
            .txnDateForSettlement(UPDATED_TXN_DATE_FOR_SETTLEMENT)
            .valueDate(UPDATED_VALUE_DATE)
            .valueDateInUTC(UPDATED_VALUE_DATE_IN_UTC)
            .valueDateInLocal(UPDATED_VALUE_DATE_IN_LOCAL)
            .valueDateForSettlement(UPDATED_VALUE_DATE_FOR_SETTLEMENT)
            .beneficiaryAccountNo(UPDATED_BENEFICIARY_ACCOUNT_NO)
            .beneficiaryName(UPDATED_BENEFICIARY_NAME)
            .beneficiarybankSwiftCode(UPDATED_BENEFICIARYBANK_SWIFT_CODE)
            .intermediaryBankSwiftCode(UPDATED_INTERMEDIARY_BANK_SWIFT_CODE)
            .beneficiaryBankName(UPDATED_BENEFICIARY_BANK_NAME)
            .beneficiaryBankBranchName(UPDATED_BENEFICIARY_BANK_BRANCH_NAME)
            .beneficiaryAddress1(UPDATED_BENEFICIARY_ADDRESS_1)
            .beneficiaryAddress2(UPDATED_BENEFICIARY_ADDRESS_2)
            .beneficiaryAddress3(UPDATED_BENEFICIARY_ADDRESS_3)
            .beneficiaryState(UPDATED_BENEFICIARY_STATE)
            .beneficiaryPinCode(UPDATED_BENEFICIARY_PIN_CODE)
            .txnAmount(UPDATED_TXN_AMOUNT)
            .amountInAccountCurrency(UPDATED_AMOUNT_IN_ACCOUNT_CURRENCY)
            .amountInLocalCurrency(UPDATED_AMOUNT_IN_LOCAL_CURRENCY)
            .chargeInAccountCurrency(UPDATED_CHARGE_IN_ACCOUNT_CURRENCY)
            .chargeInLocalCurrency(UPDATED_CHARGE_IN_LOCAL_CURRENCY)
            .fundingSource(UPDATED_FUNDING_SOURCE)
            .rate(UPDATED_RATE)
            .instructedAmount(UPDATED_INSTRUCTED_AMOUNT)
            .instructedAmountCurrency(UPDATED_INSTRUCTED_AMOUNT_CURRENCY)
            .merchantID(UPDATED_MERCHANT_ID)
            .merchantInfo(UPDATED_MERCHANT_INFO)
            .externalRefNo(UPDATED_EXTERNAL_REF_NO)
            .txnDescription(UPDATED_TXN_DESCRIPTION)
            .txnInternalDescription(UPDATED_TXN_INTERNAL_DESCRIPTION)
            .txnAddnlInfo(UPDATED_TXN_ADDNL_INFO)
            .createdOn(UPDATED_CREATED_ON)
            .createdBy(UPDATED_CREATED_BY)
            .txnStatus(UPDATED_TXN_STATUS)
            .txnDebitAccountNo(UPDATED_TXN_DEBIT_ACCOUNT_NO)
            .txnCreditAccountNo(UPDATED_TXN_CREDIT_ACCOUNT_NO)
            .chargeDebitAccountNo(UPDATED_CHARGE_DEBIT_ACCOUNT_NO)
            .chargeCreditAccountNo(UPDATED_CHARGE_CREDIT_ACCOUNT_NO);

        restRemittanceTransactionMockMvc
            .perform(
                put(ENTITY_API_URL_ID, updatedRemittanceTransaction.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(updatedRemittanceTransaction))
            )
            .andExpect(status().isOk());

        // Validate the RemittanceTransaction in the database
        List<RemittanceTransaction> remittanceTransactionList = remittanceTransactionRepository.findAll();
        assertThat(remittanceTransactionList).hasSize(databaseSizeBeforeUpdate);
        RemittanceTransaction testRemittanceTransaction = remittanceTransactionList.get(remittanceTransactionList.size() - 1);
        assertThat(testRemittanceTransaction.getTxnRefNo()).isEqualTo(UPDATED_TXN_REF_NO);
        assertThat(testRemittanceTransaction.getAccountNo()).isEqualTo(UPDATED_ACCOUNT_NO);
        assertThat(testRemittanceTransaction.getTxnDate()).isEqualTo(UPDATED_TXN_DATE);
        assertThat(testRemittanceTransaction.getTxnDateInUTC()).isEqualTo(UPDATED_TXN_DATE_IN_UTC);
        assertThat(testRemittanceTransaction.getTxnDateInLocal()).isEqualTo(UPDATED_TXN_DATE_IN_LOCAL);
        assertThat(testRemittanceTransaction.getTxnDateForSettlement()).isEqualTo(UPDATED_TXN_DATE_FOR_SETTLEMENT);
        assertThat(testRemittanceTransaction.getValueDate()).isEqualTo(UPDATED_VALUE_DATE);
        assertThat(testRemittanceTransaction.getValueDateInUTC()).isEqualTo(UPDATED_VALUE_DATE_IN_UTC);
        assertThat(testRemittanceTransaction.getValueDateInLocal()).isEqualTo(UPDATED_VALUE_DATE_IN_LOCAL);
        assertThat(testRemittanceTransaction.getValueDateForSettlement()).isEqualTo(UPDATED_VALUE_DATE_FOR_SETTLEMENT);
        assertThat(testRemittanceTransaction.getBeneficiaryAccountNo()).isEqualTo(UPDATED_BENEFICIARY_ACCOUNT_NO);
        assertThat(testRemittanceTransaction.getBeneficiaryName()).isEqualTo(UPDATED_BENEFICIARY_NAME);
        assertThat(testRemittanceTransaction.getBeneficiarybankSwiftCode()).isEqualTo(UPDATED_BENEFICIARYBANK_SWIFT_CODE);
        assertThat(testRemittanceTransaction.getIntermediaryBankSwiftCode()).isEqualTo(UPDATED_INTERMEDIARY_BANK_SWIFT_CODE);
        assertThat(testRemittanceTransaction.getBeneficiaryBankName()).isEqualTo(UPDATED_BENEFICIARY_BANK_NAME);
        assertThat(testRemittanceTransaction.getBeneficiaryBankBranchName()).isEqualTo(UPDATED_BENEFICIARY_BANK_BRANCH_NAME);
        assertThat(testRemittanceTransaction.getBeneficiaryAddress1()).isEqualTo(UPDATED_BENEFICIARY_ADDRESS_1);
        assertThat(testRemittanceTransaction.getBeneficiaryAddress2()).isEqualTo(UPDATED_BENEFICIARY_ADDRESS_2);
        assertThat(testRemittanceTransaction.getBeneficiaryAddress3()).isEqualTo(UPDATED_BENEFICIARY_ADDRESS_3);
        assertThat(testRemittanceTransaction.getBeneficiaryState()).isEqualTo(UPDATED_BENEFICIARY_STATE);
        assertThat(testRemittanceTransaction.getBeneficiaryPinCode()).isEqualTo(UPDATED_BENEFICIARY_PIN_CODE);
        assertThat(testRemittanceTransaction.getTxnAmount()).isEqualTo(UPDATED_TXN_AMOUNT);
        assertThat(testRemittanceTransaction.getAmountInAccountCurrency()).isEqualTo(UPDATED_AMOUNT_IN_ACCOUNT_CURRENCY);
        assertThat(testRemittanceTransaction.getAmountInLocalCurrency()).isEqualTo(UPDATED_AMOUNT_IN_LOCAL_CURRENCY);
        assertThat(testRemittanceTransaction.getChargeInAccountCurrency()).isEqualTo(UPDATED_CHARGE_IN_ACCOUNT_CURRENCY);
        assertThat(testRemittanceTransaction.getChargeInLocalCurrency()).isEqualTo(UPDATED_CHARGE_IN_LOCAL_CURRENCY);
        assertThat(testRemittanceTransaction.getFundingSource()).isEqualTo(UPDATED_FUNDING_SOURCE);
        assertThat(testRemittanceTransaction.getRate()).isEqualTo(UPDATED_RATE);
        assertThat(testRemittanceTransaction.getInstructedAmount()).isEqualTo(UPDATED_INSTRUCTED_AMOUNT);
        assertThat(testRemittanceTransaction.getInstructedAmountCurrency()).isEqualTo(UPDATED_INSTRUCTED_AMOUNT_CURRENCY);
        assertThat(testRemittanceTransaction.getMerchantID()).isEqualTo(UPDATED_MERCHANT_ID);
        assertThat(testRemittanceTransaction.getMerchantInfo()).isEqualTo(UPDATED_MERCHANT_INFO);
        assertThat(testRemittanceTransaction.getExternalRefNo()).isEqualTo(UPDATED_EXTERNAL_REF_NO);
        assertThat(testRemittanceTransaction.getTxnDescription()).isEqualTo(UPDATED_TXN_DESCRIPTION);
        assertThat(testRemittanceTransaction.getTxnInternalDescription()).isEqualTo(UPDATED_TXN_INTERNAL_DESCRIPTION);
        assertThat(testRemittanceTransaction.getTxnAddnlInfo()).isEqualTo(UPDATED_TXN_ADDNL_INFO);
        assertThat(testRemittanceTransaction.getCreatedOn()).isEqualTo(UPDATED_CREATED_ON);
        assertThat(testRemittanceTransaction.getCreatedBy()).isEqualTo(UPDATED_CREATED_BY);
        assertThat(testRemittanceTransaction.getTxnStatus()).isEqualTo(UPDATED_TXN_STATUS);
        assertThat(testRemittanceTransaction.getTxnDebitAccountNo()).isEqualTo(UPDATED_TXN_DEBIT_ACCOUNT_NO);
        assertThat(testRemittanceTransaction.getTxnCreditAccountNo()).isEqualTo(UPDATED_TXN_CREDIT_ACCOUNT_NO);
        assertThat(testRemittanceTransaction.getChargeDebitAccountNo()).isEqualTo(UPDATED_CHARGE_DEBIT_ACCOUNT_NO);
        assertThat(testRemittanceTransaction.getChargeCreditAccountNo()).isEqualTo(UPDATED_CHARGE_CREDIT_ACCOUNT_NO);
    }

    @Test
    @Transactional
    void putNonExistingRemittanceTransaction() throws Exception {
        int databaseSizeBeforeUpdate = remittanceTransactionRepository.findAll().size();
        remittanceTransaction.setId(count.incrementAndGet());

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restRemittanceTransactionMockMvc
            .perform(
                put(ENTITY_API_URL_ID, remittanceTransaction.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(remittanceTransaction))
            )
            .andExpect(status().isBadRequest());

        // Validate the RemittanceTransaction in the database
        List<RemittanceTransaction> remittanceTransactionList = remittanceTransactionRepository.findAll();
        assertThat(remittanceTransactionList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithIdMismatchRemittanceTransaction() throws Exception {
        int databaseSizeBeforeUpdate = remittanceTransactionRepository.findAll().size();
        remittanceTransaction.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restRemittanceTransactionMockMvc
            .perform(
                put(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(remittanceTransaction))
            )
            .andExpect(status().isBadRequest());

        // Validate the RemittanceTransaction in the database
        List<RemittanceTransaction> remittanceTransactionList = remittanceTransactionRepository.findAll();
        assertThat(remittanceTransactionList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithMissingIdPathParamRemittanceTransaction() throws Exception {
        int databaseSizeBeforeUpdate = remittanceTransactionRepository.findAll().size();
        remittanceTransaction.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restRemittanceTransactionMockMvc
            .perform(
                put(ENTITY_API_URL)
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(remittanceTransaction))
            )
            .andExpect(status().isMethodNotAllowed());

        // Validate the RemittanceTransaction in the database
        List<RemittanceTransaction> remittanceTransactionList = remittanceTransactionRepository.findAll();
        assertThat(remittanceTransactionList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void partialUpdateRemittanceTransactionWithPatch() throws Exception {
        // Initialize the database
        remittanceTransactionRepository.saveAndFlush(remittanceTransaction);

        int databaseSizeBeforeUpdate = remittanceTransactionRepository.findAll().size();

        // Update the remittanceTransaction using partial update
        RemittanceTransaction partialUpdatedRemittanceTransaction = new RemittanceTransaction();
        partialUpdatedRemittanceTransaction.setId(remittanceTransaction.getId());

        partialUpdatedRemittanceTransaction
            .txnRefNo(UPDATED_TXN_REF_NO)
            .accountNo(UPDATED_ACCOUNT_NO)
            .txnDate(UPDATED_TXN_DATE)
            .txnDateForSettlement(UPDATED_TXN_DATE_FOR_SETTLEMENT)
            .valueDateInUTC(UPDATED_VALUE_DATE_IN_UTC)
            .valueDateInLocal(UPDATED_VALUE_DATE_IN_LOCAL)
            .beneficiaryAccountNo(UPDATED_BENEFICIARY_ACCOUNT_NO)
            .beneficiaryBankName(UPDATED_BENEFICIARY_BANK_NAME)
            .beneficiaryBankBranchName(UPDATED_BENEFICIARY_BANK_BRANCH_NAME)
            .beneficiaryAddress2(UPDATED_BENEFICIARY_ADDRESS_2)
            .beneficiaryAddress3(UPDATED_BENEFICIARY_ADDRESS_3)
            .beneficiaryState(UPDATED_BENEFICIARY_STATE)
            .beneficiaryPinCode(UPDATED_BENEFICIARY_PIN_CODE)
            .txnAmount(UPDATED_TXN_AMOUNT)
            .amountInAccountCurrency(UPDATED_AMOUNT_IN_ACCOUNT_CURRENCY)
            .amountInLocalCurrency(UPDATED_AMOUNT_IN_LOCAL_CURRENCY)
            .chargeInAccountCurrency(UPDATED_CHARGE_IN_ACCOUNT_CURRENCY)
            .chargeInLocalCurrency(UPDATED_CHARGE_IN_LOCAL_CURRENCY)
            .fundingSource(UPDATED_FUNDING_SOURCE)
            .instructedAmount(UPDATED_INSTRUCTED_AMOUNT)
            .instructedAmountCurrency(UPDATED_INSTRUCTED_AMOUNT_CURRENCY)
            .merchantInfo(UPDATED_MERCHANT_INFO)
            .createdOn(UPDATED_CREATED_ON)
            .createdBy(UPDATED_CREATED_BY)
            .txnDebitAccountNo(UPDATED_TXN_DEBIT_ACCOUNT_NO)
            .chargeDebitAccountNo(UPDATED_CHARGE_DEBIT_ACCOUNT_NO)
            .chargeCreditAccountNo(UPDATED_CHARGE_CREDIT_ACCOUNT_NO);

        restRemittanceTransactionMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedRemittanceTransaction.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedRemittanceTransaction))
            )
            .andExpect(status().isOk());

        // Validate the RemittanceTransaction in the database
        List<RemittanceTransaction> remittanceTransactionList = remittanceTransactionRepository.findAll();
        assertThat(remittanceTransactionList).hasSize(databaseSizeBeforeUpdate);
        RemittanceTransaction testRemittanceTransaction = remittanceTransactionList.get(remittanceTransactionList.size() - 1);
        assertThat(testRemittanceTransaction.getTxnRefNo()).isEqualTo(UPDATED_TXN_REF_NO);
        assertThat(testRemittanceTransaction.getAccountNo()).isEqualTo(UPDATED_ACCOUNT_NO);
        assertThat(testRemittanceTransaction.getTxnDate()).isEqualTo(UPDATED_TXN_DATE);
        assertThat(testRemittanceTransaction.getTxnDateInUTC()).isEqualTo(DEFAULT_TXN_DATE_IN_UTC);
        assertThat(testRemittanceTransaction.getTxnDateInLocal()).isEqualTo(DEFAULT_TXN_DATE_IN_LOCAL);
        assertThat(testRemittanceTransaction.getTxnDateForSettlement()).isEqualTo(UPDATED_TXN_DATE_FOR_SETTLEMENT);
        assertThat(testRemittanceTransaction.getValueDate()).isEqualTo(DEFAULT_VALUE_DATE);
        assertThat(testRemittanceTransaction.getValueDateInUTC()).isEqualTo(UPDATED_VALUE_DATE_IN_UTC);
        assertThat(testRemittanceTransaction.getValueDateInLocal()).isEqualTo(UPDATED_VALUE_DATE_IN_LOCAL);
        assertThat(testRemittanceTransaction.getValueDateForSettlement()).isEqualTo(DEFAULT_VALUE_DATE_FOR_SETTLEMENT);
        assertThat(testRemittanceTransaction.getBeneficiaryAccountNo()).isEqualTo(UPDATED_BENEFICIARY_ACCOUNT_NO);
        assertThat(testRemittanceTransaction.getBeneficiaryName()).isEqualTo(DEFAULT_BENEFICIARY_NAME);
        assertThat(testRemittanceTransaction.getBeneficiarybankSwiftCode()).isEqualTo(DEFAULT_BENEFICIARYBANK_SWIFT_CODE);
        assertThat(testRemittanceTransaction.getIntermediaryBankSwiftCode()).isEqualTo(DEFAULT_INTERMEDIARY_BANK_SWIFT_CODE);
        assertThat(testRemittanceTransaction.getBeneficiaryBankName()).isEqualTo(UPDATED_BENEFICIARY_BANK_NAME);
        assertThat(testRemittanceTransaction.getBeneficiaryBankBranchName()).isEqualTo(UPDATED_BENEFICIARY_BANK_BRANCH_NAME);
        assertThat(testRemittanceTransaction.getBeneficiaryAddress1()).isEqualTo(DEFAULT_BENEFICIARY_ADDRESS_1);
        assertThat(testRemittanceTransaction.getBeneficiaryAddress2()).isEqualTo(UPDATED_BENEFICIARY_ADDRESS_2);
        assertThat(testRemittanceTransaction.getBeneficiaryAddress3()).isEqualTo(UPDATED_BENEFICIARY_ADDRESS_3);
        assertThat(testRemittanceTransaction.getBeneficiaryState()).isEqualTo(UPDATED_BENEFICIARY_STATE);
        assertThat(testRemittanceTransaction.getBeneficiaryPinCode()).isEqualTo(UPDATED_BENEFICIARY_PIN_CODE);
        assertThat(testRemittanceTransaction.getTxnAmount()).isEqualByComparingTo(UPDATED_TXN_AMOUNT);
        assertThat(testRemittanceTransaction.getAmountInAccountCurrency()).isEqualByComparingTo(UPDATED_AMOUNT_IN_ACCOUNT_CURRENCY);
        assertThat(testRemittanceTransaction.getAmountInLocalCurrency()).isEqualByComparingTo(UPDATED_AMOUNT_IN_LOCAL_CURRENCY);
        assertThat(testRemittanceTransaction.getChargeInAccountCurrency()).isEqualByComparingTo(UPDATED_CHARGE_IN_ACCOUNT_CURRENCY);
        assertThat(testRemittanceTransaction.getChargeInLocalCurrency()).isEqualByComparingTo(UPDATED_CHARGE_IN_LOCAL_CURRENCY);
        assertThat(testRemittanceTransaction.getFundingSource()).isEqualTo(UPDATED_FUNDING_SOURCE);
        assertThat(testRemittanceTransaction.getRate()).isEqualByComparingTo(DEFAULT_RATE);
        assertThat(testRemittanceTransaction.getInstructedAmount()).isEqualByComparingTo(UPDATED_INSTRUCTED_AMOUNT);
        assertThat(testRemittanceTransaction.getInstructedAmountCurrency()).isEqualTo(UPDATED_INSTRUCTED_AMOUNT_CURRENCY);
        assertThat(testRemittanceTransaction.getMerchantID()).isEqualTo(DEFAULT_MERCHANT_ID);
        assertThat(testRemittanceTransaction.getMerchantInfo()).isEqualTo(UPDATED_MERCHANT_INFO);
        assertThat(testRemittanceTransaction.getExternalRefNo()).isEqualTo(DEFAULT_EXTERNAL_REF_NO);
        assertThat(testRemittanceTransaction.getTxnDescription()).isEqualTo(DEFAULT_TXN_DESCRIPTION);
        assertThat(testRemittanceTransaction.getTxnInternalDescription()).isEqualTo(DEFAULT_TXN_INTERNAL_DESCRIPTION);
        assertThat(testRemittanceTransaction.getTxnAddnlInfo()).isEqualTo(DEFAULT_TXN_ADDNL_INFO);
        assertThat(testRemittanceTransaction.getCreatedOn()).isEqualTo(UPDATED_CREATED_ON);
        assertThat(testRemittanceTransaction.getCreatedBy()).isEqualTo(UPDATED_CREATED_BY);
        assertThat(testRemittanceTransaction.getTxnStatus()).isEqualTo(DEFAULT_TXN_STATUS);
        assertThat(testRemittanceTransaction.getTxnDebitAccountNo()).isEqualTo(UPDATED_TXN_DEBIT_ACCOUNT_NO);
        assertThat(testRemittanceTransaction.getTxnCreditAccountNo()).isEqualTo(DEFAULT_TXN_CREDIT_ACCOUNT_NO);
        assertThat(testRemittanceTransaction.getChargeDebitAccountNo()).isEqualTo(UPDATED_CHARGE_DEBIT_ACCOUNT_NO);
        assertThat(testRemittanceTransaction.getChargeCreditAccountNo()).isEqualTo(UPDATED_CHARGE_CREDIT_ACCOUNT_NO);
    }

    @Test
    @Transactional
    void fullUpdateRemittanceTransactionWithPatch() throws Exception {
        // Initialize the database
        remittanceTransactionRepository.saveAndFlush(remittanceTransaction);

        int databaseSizeBeforeUpdate = remittanceTransactionRepository.findAll().size();

        // Update the remittanceTransaction using partial update
        RemittanceTransaction partialUpdatedRemittanceTransaction = new RemittanceTransaction();
        partialUpdatedRemittanceTransaction.setId(remittanceTransaction.getId());

        partialUpdatedRemittanceTransaction
            .txnRefNo(UPDATED_TXN_REF_NO)
            .accountNo(UPDATED_ACCOUNT_NO)
            .txnDate(UPDATED_TXN_DATE)
            .txnDateInUTC(UPDATED_TXN_DATE_IN_UTC)
            .txnDateInLocal(UPDATED_TXN_DATE_IN_LOCAL)
            .txnDateForSettlement(UPDATED_TXN_DATE_FOR_SETTLEMENT)
            .valueDate(UPDATED_VALUE_DATE)
            .valueDateInUTC(UPDATED_VALUE_DATE_IN_UTC)
            .valueDateInLocal(UPDATED_VALUE_DATE_IN_LOCAL)
            .valueDateForSettlement(UPDATED_VALUE_DATE_FOR_SETTLEMENT)
            .beneficiaryAccountNo(UPDATED_BENEFICIARY_ACCOUNT_NO)
            .beneficiaryName(UPDATED_BENEFICIARY_NAME)
            .beneficiarybankSwiftCode(UPDATED_BENEFICIARYBANK_SWIFT_CODE)
            .intermediaryBankSwiftCode(UPDATED_INTERMEDIARY_BANK_SWIFT_CODE)
            .beneficiaryBankName(UPDATED_BENEFICIARY_BANK_NAME)
            .beneficiaryBankBranchName(UPDATED_BENEFICIARY_BANK_BRANCH_NAME)
            .beneficiaryAddress1(UPDATED_BENEFICIARY_ADDRESS_1)
            .beneficiaryAddress2(UPDATED_BENEFICIARY_ADDRESS_2)
            .beneficiaryAddress3(UPDATED_BENEFICIARY_ADDRESS_3)
            .beneficiaryState(UPDATED_BENEFICIARY_STATE)
            .beneficiaryPinCode(UPDATED_BENEFICIARY_PIN_CODE)
            .txnAmount(UPDATED_TXN_AMOUNT)
            .amountInAccountCurrency(UPDATED_AMOUNT_IN_ACCOUNT_CURRENCY)
            .amountInLocalCurrency(UPDATED_AMOUNT_IN_LOCAL_CURRENCY)
            .chargeInAccountCurrency(UPDATED_CHARGE_IN_ACCOUNT_CURRENCY)
            .chargeInLocalCurrency(UPDATED_CHARGE_IN_LOCAL_CURRENCY)
            .fundingSource(UPDATED_FUNDING_SOURCE)
            .rate(UPDATED_RATE)
            .instructedAmount(UPDATED_INSTRUCTED_AMOUNT)
            .instructedAmountCurrency(UPDATED_INSTRUCTED_AMOUNT_CURRENCY)
            .merchantID(UPDATED_MERCHANT_ID)
            .merchantInfo(UPDATED_MERCHANT_INFO)
            .externalRefNo(UPDATED_EXTERNAL_REF_NO)
            .txnDescription(UPDATED_TXN_DESCRIPTION)
            .txnInternalDescription(UPDATED_TXN_INTERNAL_DESCRIPTION)
            .txnAddnlInfo(UPDATED_TXN_ADDNL_INFO)
            .createdOn(UPDATED_CREATED_ON)
            .createdBy(UPDATED_CREATED_BY)
            .txnStatus(UPDATED_TXN_STATUS)
            .txnDebitAccountNo(UPDATED_TXN_DEBIT_ACCOUNT_NO)
            .txnCreditAccountNo(UPDATED_TXN_CREDIT_ACCOUNT_NO)
            .chargeDebitAccountNo(UPDATED_CHARGE_DEBIT_ACCOUNT_NO)
            .chargeCreditAccountNo(UPDATED_CHARGE_CREDIT_ACCOUNT_NO);

        restRemittanceTransactionMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedRemittanceTransaction.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedRemittanceTransaction))
            )
            .andExpect(status().isOk());

        // Validate the RemittanceTransaction in the database
        List<RemittanceTransaction> remittanceTransactionList = remittanceTransactionRepository.findAll();
        assertThat(remittanceTransactionList).hasSize(databaseSizeBeforeUpdate);
        RemittanceTransaction testRemittanceTransaction = remittanceTransactionList.get(remittanceTransactionList.size() - 1);
        assertThat(testRemittanceTransaction.getTxnRefNo()).isEqualTo(UPDATED_TXN_REF_NO);
        assertThat(testRemittanceTransaction.getAccountNo()).isEqualTo(UPDATED_ACCOUNT_NO);
        assertThat(testRemittanceTransaction.getTxnDate()).isEqualTo(UPDATED_TXN_DATE);
        assertThat(testRemittanceTransaction.getTxnDateInUTC()).isEqualTo(UPDATED_TXN_DATE_IN_UTC);
        assertThat(testRemittanceTransaction.getTxnDateInLocal()).isEqualTo(UPDATED_TXN_DATE_IN_LOCAL);
        assertThat(testRemittanceTransaction.getTxnDateForSettlement()).isEqualTo(UPDATED_TXN_DATE_FOR_SETTLEMENT);
        assertThat(testRemittanceTransaction.getValueDate()).isEqualTo(UPDATED_VALUE_DATE);
        assertThat(testRemittanceTransaction.getValueDateInUTC()).isEqualTo(UPDATED_VALUE_DATE_IN_UTC);
        assertThat(testRemittanceTransaction.getValueDateInLocal()).isEqualTo(UPDATED_VALUE_DATE_IN_LOCAL);
        assertThat(testRemittanceTransaction.getValueDateForSettlement()).isEqualTo(UPDATED_VALUE_DATE_FOR_SETTLEMENT);
        assertThat(testRemittanceTransaction.getBeneficiaryAccountNo()).isEqualTo(UPDATED_BENEFICIARY_ACCOUNT_NO);
        assertThat(testRemittanceTransaction.getBeneficiaryName()).isEqualTo(UPDATED_BENEFICIARY_NAME);
        assertThat(testRemittanceTransaction.getBeneficiarybankSwiftCode()).isEqualTo(UPDATED_BENEFICIARYBANK_SWIFT_CODE);
        assertThat(testRemittanceTransaction.getIntermediaryBankSwiftCode()).isEqualTo(UPDATED_INTERMEDIARY_BANK_SWIFT_CODE);
        assertThat(testRemittanceTransaction.getBeneficiaryBankName()).isEqualTo(UPDATED_BENEFICIARY_BANK_NAME);
        assertThat(testRemittanceTransaction.getBeneficiaryBankBranchName()).isEqualTo(UPDATED_BENEFICIARY_BANK_BRANCH_NAME);
        assertThat(testRemittanceTransaction.getBeneficiaryAddress1()).isEqualTo(UPDATED_BENEFICIARY_ADDRESS_1);
        assertThat(testRemittanceTransaction.getBeneficiaryAddress2()).isEqualTo(UPDATED_BENEFICIARY_ADDRESS_2);
        assertThat(testRemittanceTransaction.getBeneficiaryAddress3()).isEqualTo(UPDATED_BENEFICIARY_ADDRESS_3);
        assertThat(testRemittanceTransaction.getBeneficiaryState()).isEqualTo(UPDATED_BENEFICIARY_STATE);
        assertThat(testRemittanceTransaction.getBeneficiaryPinCode()).isEqualTo(UPDATED_BENEFICIARY_PIN_CODE);
        assertThat(testRemittanceTransaction.getTxnAmount()).isEqualByComparingTo(UPDATED_TXN_AMOUNT);
        assertThat(testRemittanceTransaction.getAmountInAccountCurrency()).isEqualByComparingTo(UPDATED_AMOUNT_IN_ACCOUNT_CURRENCY);
        assertThat(testRemittanceTransaction.getAmountInLocalCurrency()).isEqualByComparingTo(UPDATED_AMOUNT_IN_LOCAL_CURRENCY);
        assertThat(testRemittanceTransaction.getChargeInAccountCurrency()).isEqualByComparingTo(UPDATED_CHARGE_IN_ACCOUNT_CURRENCY);
        assertThat(testRemittanceTransaction.getChargeInLocalCurrency()).isEqualByComparingTo(UPDATED_CHARGE_IN_LOCAL_CURRENCY);
        assertThat(testRemittanceTransaction.getFundingSource()).isEqualTo(UPDATED_FUNDING_SOURCE);
        assertThat(testRemittanceTransaction.getRate()).isEqualByComparingTo(UPDATED_RATE);
        assertThat(testRemittanceTransaction.getInstructedAmount()).isEqualByComparingTo(UPDATED_INSTRUCTED_AMOUNT);
        assertThat(testRemittanceTransaction.getInstructedAmountCurrency()).isEqualTo(UPDATED_INSTRUCTED_AMOUNT_CURRENCY);
        assertThat(testRemittanceTransaction.getMerchantID()).isEqualTo(UPDATED_MERCHANT_ID);
        assertThat(testRemittanceTransaction.getMerchantInfo()).isEqualTo(UPDATED_MERCHANT_INFO);
        assertThat(testRemittanceTransaction.getExternalRefNo()).isEqualTo(UPDATED_EXTERNAL_REF_NO);
        assertThat(testRemittanceTransaction.getTxnDescription()).isEqualTo(UPDATED_TXN_DESCRIPTION);
        assertThat(testRemittanceTransaction.getTxnInternalDescription()).isEqualTo(UPDATED_TXN_INTERNAL_DESCRIPTION);
        assertThat(testRemittanceTransaction.getTxnAddnlInfo()).isEqualTo(UPDATED_TXN_ADDNL_INFO);
        assertThat(testRemittanceTransaction.getCreatedOn()).isEqualTo(UPDATED_CREATED_ON);
        assertThat(testRemittanceTransaction.getCreatedBy()).isEqualTo(UPDATED_CREATED_BY);
        assertThat(testRemittanceTransaction.getTxnStatus()).isEqualTo(UPDATED_TXN_STATUS);
        assertThat(testRemittanceTransaction.getTxnDebitAccountNo()).isEqualTo(UPDATED_TXN_DEBIT_ACCOUNT_NO);
        assertThat(testRemittanceTransaction.getTxnCreditAccountNo()).isEqualTo(UPDATED_TXN_CREDIT_ACCOUNT_NO);
        assertThat(testRemittanceTransaction.getChargeDebitAccountNo()).isEqualTo(UPDATED_CHARGE_DEBIT_ACCOUNT_NO);
        assertThat(testRemittanceTransaction.getChargeCreditAccountNo()).isEqualTo(UPDATED_CHARGE_CREDIT_ACCOUNT_NO);
    }

    @Test
    @Transactional
    void patchNonExistingRemittanceTransaction() throws Exception {
        int databaseSizeBeforeUpdate = remittanceTransactionRepository.findAll().size();
        remittanceTransaction.setId(count.incrementAndGet());

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restRemittanceTransactionMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, remittanceTransaction.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(remittanceTransaction))
            )
            .andExpect(status().isBadRequest());

        // Validate the RemittanceTransaction in the database
        List<RemittanceTransaction> remittanceTransactionList = remittanceTransactionRepository.findAll();
        assertThat(remittanceTransactionList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithIdMismatchRemittanceTransaction() throws Exception {
        int databaseSizeBeforeUpdate = remittanceTransactionRepository.findAll().size();
        remittanceTransaction.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restRemittanceTransactionMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(remittanceTransaction))
            )
            .andExpect(status().isBadRequest());

        // Validate the RemittanceTransaction in the database
        List<RemittanceTransaction> remittanceTransactionList = remittanceTransactionRepository.findAll();
        assertThat(remittanceTransactionList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithMissingIdPathParamRemittanceTransaction() throws Exception {
        int databaseSizeBeforeUpdate = remittanceTransactionRepository.findAll().size();
        remittanceTransaction.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restRemittanceTransactionMockMvc
            .perform(
                patch(ENTITY_API_URL)
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(remittanceTransaction))
            )
            .andExpect(status().isMethodNotAllowed());

        // Validate the RemittanceTransaction in the database
        List<RemittanceTransaction> remittanceTransactionList = remittanceTransactionRepository.findAll();
        assertThat(remittanceTransactionList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void deleteRemittanceTransaction() throws Exception {
        // Initialize the database
        remittanceTransactionRepository.saveAndFlush(remittanceTransaction);

        int databaseSizeBeforeDelete = remittanceTransactionRepository.findAll().size();

        // Delete the remittanceTransaction
        restRemittanceTransactionMockMvc
            .perform(delete(ENTITY_API_URL_ID, remittanceTransaction.getId()).accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<RemittanceTransaction> remittanceTransactionList = remittanceTransactionRepository.findAll();
        assertThat(remittanceTransactionList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
