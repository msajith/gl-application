package com.iconnect101.glapp.web.rest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import com.iconnect101.glapp.IntegrationTest;
import com.iconnect101.glapp.domain.GlEntryType;
import com.iconnect101.glapp.repository.GlEntryTypeRepository;
import java.util.List;
import java.util.Random;
import java.util.concurrent.atomic.AtomicLong;
import javax.persistence.EntityManager;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;

/**
 * Integration tests for the {@link GlEntryTypeResource} REST controller.
 */
@IntegrationTest
@AutoConfigureMockMvc
@WithMockUser
class GlEntryTypeResourceIT {

    private static final String DEFAULT_NAME = "AAAAAAAAAA";
    private static final String UPDATED_NAME = "BBBBBBBBBB";

    private static final String DEFAULT_CODE = "AAAAAAAAAA";
    private static final String UPDATED_CODE = "BBBBBBBBBB";

    private static final String ENTITY_API_URL = "/api/gl-entry-types";
    private static final String ENTITY_API_URL_ID = ENTITY_API_URL + "/{id}";

    private static Random random = new Random();
    private static AtomicLong count = new AtomicLong(random.nextInt() + (2 * Integer.MAX_VALUE));

    @Autowired
    private GlEntryTypeRepository glEntryTypeRepository;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restGlEntryTypeMockMvc;

    private GlEntryType glEntryType;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static GlEntryType createEntity(EntityManager em) {
        GlEntryType glEntryType = new GlEntryType().name(DEFAULT_NAME).code(DEFAULT_CODE);
        return glEntryType;
    }

    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static GlEntryType createUpdatedEntity(EntityManager em) {
        GlEntryType glEntryType = new GlEntryType().name(UPDATED_NAME).code(UPDATED_CODE);
        return glEntryType;
    }

    @BeforeEach
    public void initTest() {
        glEntryType = createEntity(em);
    }

    @Test
    @Transactional
    void createGlEntryType() throws Exception {
        int databaseSizeBeforeCreate = glEntryTypeRepository.findAll().size();
        // Create the GlEntryType
        restGlEntryTypeMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(glEntryType)))
            .andExpect(status().isCreated());

        // Validate the GlEntryType in the database
        List<GlEntryType> glEntryTypeList = glEntryTypeRepository.findAll();
        assertThat(glEntryTypeList).hasSize(databaseSizeBeforeCreate + 1);
        GlEntryType testGlEntryType = glEntryTypeList.get(glEntryTypeList.size() - 1);
        assertThat(testGlEntryType.getName()).isEqualTo(DEFAULT_NAME);
        assertThat(testGlEntryType.getCode()).isEqualTo(DEFAULT_CODE);
    }

    @Test
    @Transactional
    void createGlEntryTypeWithExistingId() throws Exception {
        // Create the GlEntryType with an existing ID
        glEntryType.setId(1L);

        int databaseSizeBeforeCreate = glEntryTypeRepository.findAll().size();

        // An entity with an existing ID cannot be created, so this API call must fail
        restGlEntryTypeMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(glEntryType)))
            .andExpect(status().isBadRequest());

        // Validate the GlEntryType in the database
        List<GlEntryType> glEntryTypeList = glEntryTypeRepository.findAll();
        assertThat(glEntryTypeList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    void getAllGlEntryTypes() throws Exception {
        // Initialize the database
        glEntryTypeRepository.saveAndFlush(glEntryType);

        // Get all the glEntryTypeList
        restGlEntryTypeMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(glEntryType.getId().intValue())))
            .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME)))
            .andExpect(jsonPath("$.[*].code").value(hasItem(DEFAULT_CODE)));
    }

    @Test
    @Transactional
    void getGlEntryType() throws Exception {
        // Initialize the database
        glEntryTypeRepository.saveAndFlush(glEntryType);

        // Get the glEntryType
        restGlEntryTypeMockMvc
            .perform(get(ENTITY_API_URL_ID, glEntryType.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(glEntryType.getId().intValue()))
            .andExpect(jsonPath("$.name").value(DEFAULT_NAME))
            .andExpect(jsonPath("$.code").value(DEFAULT_CODE));
    }

    @Test
    @Transactional
    void getNonExistingGlEntryType() throws Exception {
        // Get the glEntryType
        restGlEntryTypeMockMvc.perform(get(ENTITY_API_URL_ID, Long.MAX_VALUE)).andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    void putNewGlEntryType() throws Exception {
        // Initialize the database
        glEntryTypeRepository.saveAndFlush(glEntryType);

        int databaseSizeBeforeUpdate = glEntryTypeRepository.findAll().size();

        // Update the glEntryType
        GlEntryType updatedGlEntryType = glEntryTypeRepository.findById(glEntryType.getId()).get();
        // Disconnect from session so that the updates on updatedGlEntryType are not directly saved in db
        em.detach(updatedGlEntryType);
        updatedGlEntryType.name(UPDATED_NAME).code(UPDATED_CODE);

        restGlEntryTypeMockMvc
            .perform(
                put(ENTITY_API_URL_ID, updatedGlEntryType.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(updatedGlEntryType))
            )
            .andExpect(status().isOk());

        // Validate the GlEntryType in the database
        List<GlEntryType> glEntryTypeList = glEntryTypeRepository.findAll();
        assertThat(glEntryTypeList).hasSize(databaseSizeBeforeUpdate);
        GlEntryType testGlEntryType = glEntryTypeList.get(glEntryTypeList.size() - 1);
        assertThat(testGlEntryType.getName()).isEqualTo(UPDATED_NAME);
        assertThat(testGlEntryType.getCode()).isEqualTo(UPDATED_CODE);
    }

    @Test
    @Transactional
    void putNonExistingGlEntryType() throws Exception {
        int databaseSizeBeforeUpdate = glEntryTypeRepository.findAll().size();
        glEntryType.setId(count.incrementAndGet());

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restGlEntryTypeMockMvc
            .perform(
                put(ENTITY_API_URL_ID, glEntryType.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(glEntryType))
            )
            .andExpect(status().isBadRequest());

        // Validate the GlEntryType in the database
        List<GlEntryType> glEntryTypeList = glEntryTypeRepository.findAll();
        assertThat(glEntryTypeList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithIdMismatchGlEntryType() throws Exception {
        int databaseSizeBeforeUpdate = glEntryTypeRepository.findAll().size();
        glEntryType.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restGlEntryTypeMockMvc
            .perform(
                put(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(glEntryType))
            )
            .andExpect(status().isBadRequest());

        // Validate the GlEntryType in the database
        List<GlEntryType> glEntryTypeList = glEntryTypeRepository.findAll();
        assertThat(glEntryTypeList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithMissingIdPathParamGlEntryType() throws Exception {
        int databaseSizeBeforeUpdate = glEntryTypeRepository.findAll().size();
        glEntryType.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restGlEntryTypeMockMvc
            .perform(put(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(glEntryType)))
            .andExpect(status().isMethodNotAllowed());

        // Validate the GlEntryType in the database
        List<GlEntryType> glEntryTypeList = glEntryTypeRepository.findAll();
        assertThat(glEntryTypeList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void partialUpdateGlEntryTypeWithPatch() throws Exception {
        // Initialize the database
        glEntryTypeRepository.saveAndFlush(glEntryType);

        int databaseSizeBeforeUpdate = glEntryTypeRepository.findAll().size();

        // Update the glEntryType using partial update
        GlEntryType partialUpdatedGlEntryType = new GlEntryType();
        partialUpdatedGlEntryType.setId(glEntryType.getId());

        restGlEntryTypeMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedGlEntryType.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedGlEntryType))
            )
            .andExpect(status().isOk());

        // Validate the GlEntryType in the database
        List<GlEntryType> glEntryTypeList = glEntryTypeRepository.findAll();
        assertThat(glEntryTypeList).hasSize(databaseSizeBeforeUpdate);
        GlEntryType testGlEntryType = glEntryTypeList.get(glEntryTypeList.size() - 1);
        assertThat(testGlEntryType.getName()).isEqualTo(DEFAULT_NAME);
        assertThat(testGlEntryType.getCode()).isEqualTo(DEFAULT_CODE);
    }

    @Test
    @Transactional
    void fullUpdateGlEntryTypeWithPatch() throws Exception {
        // Initialize the database
        glEntryTypeRepository.saveAndFlush(glEntryType);

        int databaseSizeBeforeUpdate = glEntryTypeRepository.findAll().size();

        // Update the glEntryType using partial update
        GlEntryType partialUpdatedGlEntryType = new GlEntryType();
        partialUpdatedGlEntryType.setId(glEntryType.getId());

        partialUpdatedGlEntryType.name(UPDATED_NAME).code(UPDATED_CODE);

        restGlEntryTypeMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedGlEntryType.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedGlEntryType))
            )
            .andExpect(status().isOk());

        // Validate the GlEntryType in the database
        List<GlEntryType> glEntryTypeList = glEntryTypeRepository.findAll();
        assertThat(glEntryTypeList).hasSize(databaseSizeBeforeUpdate);
        GlEntryType testGlEntryType = glEntryTypeList.get(glEntryTypeList.size() - 1);
        assertThat(testGlEntryType.getName()).isEqualTo(UPDATED_NAME);
        assertThat(testGlEntryType.getCode()).isEqualTo(UPDATED_CODE);
    }

    @Test
    @Transactional
    void patchNonExistingGlEntryType() throws Exception {
        int databaseSizeBeforeUpdate = glEntryTypeRepository.findAll().size();
        glEntryType.setId(count.incrementAndGet());

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restGlEntryTypeMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, glEntryType.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(glEntryType))
            )
            .andExpect(status().isBadRequest());

        // Validate the GlEntryType in the database
        List<GlEntryType> glEntryTypeList = glEntryTypeRepository.findAll();
        assertThat(glEntryTypeList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithIdMismatchGlEntryType() throws Exception {
        int databaseSizeBeforeUpdate = glEntryTypeRepository.findAll().size();
        glEntryType.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restGlEntryTypeMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(glEntryType))
            )
            .andExpect(status().isBadRequest());

        // Validate the GlEntryType in the database
        List<GlEntryType> glEntryTypeList = glEntryTypeRepository.findAll();
        assertThat(glEntryTypeList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithMissingIdPathParamGlEntryType() throws Exception {
        int databaseSizeBeforeUpdate = glEntryTypeRepository.findAll().size();
        glEntryType.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restGlEntryTypeMockMvc
            .perform(
                patch(ENTITY_API_URL).contentType("application/merge-patch+json").content(TestUtil.convertObjectToJsonBytes(glEntryType))
            )
            .andExpect(status().isMethodNotAllowed());

        // Validate the GlEntryType in the database
        List<GlEntryType> glEntryTypeList = glEntryTypeRepository.findAll();
        assertThat(glEntryTypeList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void deleteGlEntryType() throws Exception {
        // Initialize the database
        glEntryTypeRepository.saveAndFlush(glEntryType);

        int databaseSizeBeforeDelete = glEntryTypeRepository.findAll().size();

        // Delete the glEntryType
        restGlEntryTypeMockMvc
            .perform(delete(ENTITY_API_URL_ID, glEntryType.getId()).accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<GlEntryType> glEntryTypeList = glEntryTypeRepository.findAll();
        assertThat(glEntryTypeList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
