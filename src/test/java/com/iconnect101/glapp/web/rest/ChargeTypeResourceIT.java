package com.iconnect101.glapp.web.rest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import com.iconnect101.glapp.IntegrationTest;
import com.iconnect101.glapp.domain.ChargeType;
import com.iconnect101.glapp.repository.ChargeTypeRepository;
import java.util.List;
import java.util.Random;
import java.util.concurrent.atomic.AtomicLong;
import javax.persistence.EntityManager;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;

/**
 * Integration tests for the {@link ChargeTypeResource} REST controller.
 */
@IntegrationTest
@AutoConfigureMockMvc
@WithMockUser
class ChargeTypeResourceIT {

    private static final String DEFAULT_NAME = "AAAAAAAAAA";
    private static final String UPDATED_NAME = "BBBBBBBBBB";

    private static final String DEFAULT_CODE = "AAAAAAAAAA";
    private static final String UPDATED_CODE = "BBBBBBBBBB";

    private static final String ENTITY_API_URL = "/api/charge-types";
    private static final String ENTITY_API_URL_ID = ENTITY_API_URL + "/{id}";

    private static Random random = new Random();
    private static AtomicLong count = new AtomicLong(random.nextInt() + (2 * Integer.MAX_VALUE));

    @Autowired
    private ChargeTypeRepository chargeTypeRepository;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restChargeTypeMockMvc;

    private ChargeType chargeType;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static ChargeType createEntity(EntityManager em) {
        ChargeType chargeType = new ChargeType().name(DEFAULT_NAME).code(DEFAULT_CODE);
        return chargeType;
    }

    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static ChargeType createUpdatedEntity(EntityManager em) {
        ChargeType chargeType = new ChargeType().name(UPDATED_NAME).code(UPDATED_CODE);
        return chargeType;
    }

    @BeforeEach
    public void initTest() {
        chargeType = createEntity(em);
    }

    @Test
    @Transactional
    void createChargeType() throws Exception {
        int databaseSizeBeforeCreate = chargeTypeRepository.findAll().size();
        // Create the ChargeType
        restChargeTypeMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(chargeType)))
            .andExpect(status().isCreated());

        // Validate the ChargeType in the database
        List<ChargeType> chargeTypeList = chargeTypeRepository.findAll();
        assertThat(chargeTypeList).hasSize(databaseSizeBeforeCreate + 1);
        ChargeType testChargeType = chargeTypeList.get(chargeTypeList.size() - 1);
        assertThat(testChargeType.getName()).isEqualTo(DEFAULT_NAME);
        assertThat(testChargeType.getCode()).isEqualTo(DEFAULT_CODE);
    }

    @Test
    @Transactional
    void createChargeTypeWithExistingId() throws Exception {
        // Create the ChargeType with an existing ID
        chargeType.setId(1L);

        int databaseSizeBeforeCreate = chargeTypeRepository.findAll().size();

        // An entity with an existing ID cannot be created, so this API call must fail
        restChargeTypeMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(chargeType)))
            .andExpect(status().isBadRequest());

        // Validate the ChargeType in the database
        List<ChargeType> chargeTypeList = chargeTypeRepository.findAll();
        assertThat(chargeTypeList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    void getAllChargeTypes() throws Exception {
        // Initialize the database
        chargeTypeRepository.saveAndFlush(chargeType);

        // Get all the chargeTypeList
        restChargeTypeMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(chargeType.getId().intValue())))
            .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME)))
            .andExpect(jsonPath("$.[*].code").value(hasItem(DEFAULT_CODE)));
    }

    @Test
    @Transactional
    void getChargeType() throws Exception {
        // Initialize the database
        chargeTypeRepository.saveAndFlush(chargeType);

        // Get the chargeType
        restChargeTypeMockMvc
            .perform(get(ENTITY_API_URL_ID, chargeType.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(chargeType.getId().intValue()))
            .andExpect(jsonPath("$.name").value(DEFAULT_NAME))
            .andExpect(jsonPath("$.code").value(DEFAULT_CODE));
    }

    @Test
    @Transactional
    void getNonExistingChargeType() throws Exception {
        // Get the chargeType
        restChargeTypeMockMvc.perform(get(ENTITY_API_URL_ID, Long.MAX_VALUE)).andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    void putNewChargeType() throws Exception {
        // Initialize the database
        chargeTypeRepository.saveAndFlush(chargeType);

        int databaseSizeBeforeUpdate = chargeTypeRepository.findAll().size();

        // Update the chargeType
        ChargeType updatedChargeType = chargeTypeRepository.findById(chargeType.getId()).get();
        // Disconnect from session so that the updates on updatedChargeType are not directly saved in db
        em.detach(updatedChargeType);
        updatedChargeType.name(UPDATED_NAME).code(UPDATED_CODE);

        restChargeTypeMockMvc
            .perform(
                put(ENTITY_API_URL_ID, updatedChargeType.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(updatedChargeType))
            )
            .andExpect(status().isOk());

        // Validate the ChargeType in the database
        List<ChargeType> chargeTypeList = chargeTypeRepository.findAll();
        assertThat(chargeTypeList).hasSize(databaseSizeBeforeUpdate);
        ChargeType testChargeType = chargeTypeList.get(chargeTypeList.size() - 1);
        assertThat(testChargeType.getName()).isEqualTo(UPDATED_NAME);
        assertThat(testChargeType.getCode()).isEqualTo(UPDATED_CODE);
    }

    @Test
    @Transactional
    void putNonExistingChargeType() throws Exception {
        int databaseSizeBeforeUpdate = chargeTypeRepository.findAll().size();
        chargeType.setId(count.incrementAndGet());

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restChargeTypeMockMvc
            .perform(
                put(ENTITY_API_URL_ID, chargeType.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(chargeType))
            )
            .andExpect(status().isBadRequest());

        // Validate the ChargeType in the database
        List<ChargeType> chargeTypeList = chargeTypeRepository.findAll();
        assertThat(chargeTypeList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithIdMismatchChargeType() throws Exception {
        int databaseSizeBeforeUpdate = chargeTypeRepository.findAll().size();
        chargeType.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restChargeTypeMockMvc
            .perform(
                put(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(chargeType))
            )
            .andExpect(status().isBadRequest());

        // Validate the ChargeType in the database
        List<ChargeType> chargeTypeList = chargeTypeRepository.findAll();
        assertThat(chargeTypeList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithMissingIdPathParamChargeType() throws Exception {
        int databaseSizeBeforeUpdate = chargeTypeRepository.findAll().size();
        chargeType.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restChargeTypeMockMvc
            .perform(put(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(chargeType)))
            .andExpect(status().isMethodNotAllowed());

        // Validate the ChargeType in the database
        List<ChargeType> chargeTypeList = chargeTypeRepository.findAll();
        assertThat(chargeTypeList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void partialUpdateChargeTypeWithPatch() throws Exception {
        // Initialize the database
        chargeTypeRepository.saveAndFlush(chargeType);

        int databaseSizeBeforeUpdate = chargeTypeRepository.findAll().size();

        // Update the chargeType using partial update
        ChargeType partialUpdatedChargeType = new ChargeType();
        partialUpdatedChargeType.setId(chargeType.getId());

        partialUpdatedChargeType.name(UPDATED_NAME);

        restChargeTypeMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedChargeType.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedChargeType))
            )
            .andExpect(status().isOk());

        // Validate the ChargeType in the database
        List<ChargeType> chargeTypeList = chargeTypeRepository.findAll();
        assertThat(chargeTypeList).hasSize(databaseSizeBeforeUpdate);
        ChargeType testChargeType = chargeTypeList.get(chargeTypeList.size() - 1);
        assertThat(testChargeType.getName()).isEqualTo(UPDATED_NAME);
        assertThat(testChargeType.getCode()).isEqualTo(DEFAULT_CODE);
    }

    @Test
    @Transactional
    void fullUpdateChargeTypeWithPatch() throws Exception {
        // Initialize the database
        chargeTypeRepository.saveAndFlush(chargeType);

        int databaseSizeBeforeUpdate = chargeTypeRepository.findAll().size();

        // Update the chargeType using partial update
        ChargeType partialUpdatedChargeType = new ChargeType();
        partialUpdatedChargeType.setId(chargeType.getId());

        partialUpdatedChargeType.name(UPDATED_NAME).code(UPDATED_CODE);

        restChargeTypeMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedChargeType.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedChargeType))
            )
            .andExpect(status().isOk());

        // Validate the ChargeType in the database
        List<ChargeType> chargeTypeList = chargeTypeRepository.findAll();
        assertThat(chargeTypeList).hasSize(databaseSizeBeforeUpdate);
        ChargeType testChargeType = chargeTypeList.get(chargeTypeList.size() - 1);
        assertThat(testChargeType.getName()).isEqualTo(UPDATED_NAME);
        assertThat(testChargeType.getCode()).isEqualTo(UPDATED_CODE);
    }

    @Test
    @Transactional
    void patchNonExistingChargeType() throws Exception {
        int databaseSizeBeforeUpdate = chargeTypeRepository.findAll().size();
        chargeType.setId(count.incrementAndGet());

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restChargeTypeMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, chargeType.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(chargeType))
            )
            .andExpect(status().isBadRequest());

        // Validate the ChargeType in the database
        List<ChargeType> chargeTypeList = chargeTypeRepository.findAll();
        assertThat(chargeTypeList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithIdMismatchChargeType() throws Exception {
        int databaseSizeBeforeUpdate = chargeTypeRepository.findAll().size();
        chargeType.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restChargeTypeMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(chargeType))
            )
            .andExpect(status().isBadRequest());

        // Validate the ChargeType in the database
        List<ChargeType> chargeTypeList = chargeTypeRepository.findAll();
        assertThat(chargeTypeList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithMissingIdPathParamChargeType() throws Exception {
        int databaseSizeBeforeUpdate = chargeTypeRepository.findAll().size();
        chargeType.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restChargeTypeMockMvc
            .perform(
                patch(ENTITY_API_URL).contentType("application/merge-patch+json").content(TestUtil.convertObjectToJsonBytes(chargeType))
            )
            .andExpect(status().isMethodNotAllowed());

        // Validate the ChargeType in the database
        List<ChargeType> chargeTypeList = chargeTypeRepository.findAll();
        assertThat(chargeTypeList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void deleteChargeType() throws Exception {
        // Initialize the database
        chargeTypeRepository.saveAndFlush(chargeType);

        int databaseSizeBeforeDelete = chargeTypeRepository.findAll().size();

        // Delete the chargeType
        restChargeTypeMockMvc
            .perform(delete(ENTITY_API_URL_ID, chargeType.getId()).accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<ChargeType> chargeTypeList = chargeTypeRepository.findAll();
        assertThat(chargeTypeList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
