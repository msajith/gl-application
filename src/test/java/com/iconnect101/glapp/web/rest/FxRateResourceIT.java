package com.iconnect101.glapp.web.rest;

import static com.iconnect101.glapp.web.rest.TestUtil.sameNumber;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import com.iconnect101.glapp.IntegrationTest;
import com.iconnect101.glapp.domain.FxRate;
import com.iconnect101.glapp.domain.enumeration.FxRecordStatus;
import com.iconnect101.glapp.repository.FxRateRepository;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.List;
import java.util.Random;
import java.util.concurrent.atomic.AtomicLong;
import javax.persistence.EntityManager;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;

/**
 * Integration tests for the {@link FxRateResource} REST controller.
 */
@IntegrationTest
@AutoConfigureMockMvc
@WithMockUser
class FxRateResourceIT {

    private static final String DEFAULT_TYPE = "AAAAAAAAAA";
    private static final String UPDATED_TYPE = "BBBBBBBBBB";

    private static final String DEFAULT_BASE_CURRENCY = "AAAAAAAAAA";
    private static final String UPDATED_BASE_CURRENCY = "BBBBBBBBBB";

    private static final String DEFAULT_QUOTE_CURRENCY = "AAAAAAAAAA";
    private static final String UPDATED_QUOTE_CURRENCY = "BBBBBBBBBB";

    private static final BigDecimal DEFAULT_ASK_PRICE = new BigDecimal(1);
    private static final BigDecimal UPDATED_ASK_PRICE = new BigDecimal(2);

    private static final BigDecimal DEFAULT_BID_PRICE = new BigDecimal(1);
    private static final BigDecimal UPDATED_BID_PRICE = new BigDecimal(2);

    private static final BigDecimal DEFAULT_MID_PRICE = new BigDecimal(1);
    private static final BigDecimal UPDATED_MID_PRICE = new BigDecimal(2);

    private static final BigDecimal DEFAULT_ASK_MARGIN = new BigDecimal(1);
    private static final BigDecimal UPDATED_ASK_MARGIN = new BigDecimal(2);

    private static final BigDecimal DEFAULT_BID_MARGIN = new BigDecimal(1);
    private static final BigDecimal UPDATED_BID_MARGIN = new BigDecimal(2);

    private static final String DEFAULT_SEGMENT = "AAAAAAAAAA";
    private static final String UPDATED_SEGMENT = "BBBBBBBBBB";

    private static final String DEFAULT_AMOUNT_SLAB = "AAAAAAAAAA";
    private static final String UPDATED_AMOUNT_SLAB = "BBBBBBBBBB";

    private static final BigDecimal DEFAULT_AMOUNT_SLAB_VALUE = new BigDecimal(1);
    private static final BigDecimal UPDATED_AMOUNT_SLAB_VALUE = new BigDecimal(2);

    private static final String DEFAULT_AMOUNT_SLAB_OPERATOR = "AAAAAAAAAA";
    private static final String UPDATED_AMOUNT_SLAB_OPERATOR = "BBBBBBBBBB";

    private static final String DEFAULT_AMOUNT_SLAB_CURRENCY = "AAAAAAAAAA";
    private static final String UPDATED_AMOUNT_SLAB_CURRENCY = "BBBBBBBBBB";

    private static final FxRecordStatus DEFAULT_RECORD_STATUS = FxRecordStatus.ACTIVE;
    private static final FxRecordStatus UPDATED_RECORD_STATUS = FxRecordStatus.INACTIVE;

    private static final LocalDate DEFAULT_VALIDFROM = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_VALIDFROM = LocalDate.now(ZoneId.systemDefault());

    private static final LocalDate DEFAULT_VALID_TO = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_VALID_TO = LocalDate.now(ZoneId.systemDefault());

    private static final String ENTITY_API_URL = "/api/fx-rates";
    private static final String ENTITY_API_URL_ID = ENTITY_API_URL + "/{id}";

    private static Random random = new Random();
    private static AtomicLong count = new AtomicLong(random.nextInt() + (2 * Integer.MAX_VALUE));

    @Autowired
    private FxRateRepository fxRateRepository;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restFxRateMockMvc;

    private FxRate fxRate;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static FxRate createEntity(EntityManager em) {
        FxRate fxRate = new FxRate()
            .type(DEFAULT_TYPE)
            .baseCurrency(DEFAULT_BASE_CURRENCY)
            .quoteCurrency(DEFAULT_QUOTE_CURRENCY)
            .askPrice(DEFAULT_ASK_PRICE)
            .bidPrice(DEFAULT_BID_PRICE)
            .midPrice(DEFAULT_MID_PRICE)
            .askMargin(DEFAULT_ASK_MARGIN)
            .bidMargin(DEFAULT_BID_MARGIN)
            .segment(DEFAULT_SEGMENT)
            .amountSlab(DEFAULT_AMOUNT_SLAB)
            .amountSlabValue(DEFAULT_AMOUNT_SLAB_VALUE)
            .amountSlabOperator(DEFAULT_AMOUNT_SLAB_OPERATOR)
            .amountSlabCurrency(DEFAULT_AMOUNT_SLAB_CURRENCY)
            .recordStatus(DEFAULT_RECORD_STATUS)
            .validfrom(DEFAULT_VALIDFROM)
            .validTo(DEFAULT_VALID_TO);
        return fxRate;
    }

    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static FxRate createUpdatedEntity(EntityManager em) {
        FxRate fxRate = new FxRate()
            .type(UPDATED_TYPE)
            .baseCurrency(UPDATED_BASE_CURRENCY)
            .quoteCurrency(UPDATED_QUOTE_CURRENCY)
            .askPrice(UPDATED_ASK_PRICE)
            .bidPrice(UPDATED_BID_PRICE)
            .midPrice(UPDATED_MID_PRICE)
            .askMargin(UPDATED_ASK_MARGIN)
            .bidMargin(UPDATED_BID_MARGIN)
            .segment(UPDATED_SEGMENT)
            .amountSlab(UPDATED_AMOUNT_SLAB)
            .amountSlabValue(UPDATED_AMOUNT_SLAB_VALUE)
            .amountSlabOperator(UPDATED_AMOUNT_SLAB_OPERATOR)
            .amountSlabCurrency(UPDATED_AMOUNT_SLAB_CURRENCY)
            .recordStatus(UPDATED_RECORD_STATUS)
            .validfrom(UPDATED_VALIDFROM)
            .validTo(UPDATED_VALID_TO);
        return fxRate;
    }

    @BeforeEach
    public void initTest() {
        fxRate = createEntity(em);
    }

    @Test
    @Transactional
    void createFxRate() throws Exception {
        int databaseSizeBeforeCreate = fxRateRepository.findAll().size();
        // Create the FxRate
        restFxRateMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(fxRate)))
            .andExpect(status().isCreated());

        // Validate the FxRate in the database
        List<FxRate> fxRateList = fxRateRepository.findAll();
        assertThat(fxRateList).hasSize(databaseSizeBeforeCreate + 1);
        FxRate testFxRate = fxRateList.get(fxRateList.size() - 1);
        assertThat(testFxRate.getType()).isEqualTo(DEFAULT_TYPE);
        assertThat(testFxRate.getBaseCurrency()).isEqualTo(DEFAULT_BASE_CURRENCY);
        assertThat(testFxRate.getQuoteCurrency()).isEqualTo(DEFAULT_QUOTE_CURRENCY);
        assertThat(testFxRate.getAskPrice()).isEqualByComparingTo(DEFAULT_ASK_PRICE);
        assertThat(testFxRate.getBidPrice()).isEqualByComparingTo(DEFAULT_BID_PRICE);
        assertThat(testFxRate.getMidPrice()).isEqualByComparingTo(DEFAULT_MID_PRICE);
        assertThat(testFxRate.getAskMargin()).isEqualByComparingTo(DEFAULT_ASK_MARGIN);
        assertThat(testFxRate.getBidMargin()).isEqualByComparingTo(DEFAULT_BID_MARGIN);
        assertThat(testFxRate.getSegment()).isEqualTo(DEFAULT_SEGMENT);
        assertThat(testFxRate.getAmountSlab()).isEqualTo(DEFAULT_AMOUNT_SLAB);
        assertThat(testFxRate.getAmountSlabValue()).isEqualByComparingTo(DEFAULT_AMOUNT_SLAB_VALUE);
        assertThat(testFxRate.getAmountSlabOperator()).isEqualTo(DEFAULT_AMOUNT_SLAB_OPERATOR);
        assertThat(testFxRate.getAmountSlabCurrency()).isEqualTo(DEFAULT_AMOUNT_SLAB_CURRENCY);
        assertThat(testFxRate.getRecordStatus()).isEqualTo(DEFAULT_RECORD_STATUS);
        assertThat(testFxRate.getValidfrom()).isEqualTo(DEFAULT_VALIDFROM);
        assertThat(testFxRate.getValidTo()).isEqualTo(DEFAULT_VALID_TO);
    }

    @Test
    @Transactional
    void createFxRateWithExistingId() throws Exception {
        // Create the FxRate with an existing ID
        fxRate.setId(1L);

        int databaseSizeBeforeCreate = fxRateRepository.findAll().size();

        // An entity with an existing ID cannot be created, so this API call must fail
        restFxRateMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(fxRate)))
            .andExpect(status().isBadRequest());

        // Validate the FxRate in the database
        List<FxRate> fxRateList = fxRateRepository.findAll();
        assertThat(fxRateList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    void getAllFxRates() throws Exception {
        // Initialize the database
        fxRateRepository.saveAndFlush(fxRate);

        // Get all the fxRateList
        restFxRateMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(fxRate.getId().intValue())))
            .andExpect(jsonPath("$.[*].type").value(hasItem(DEFAULT_TYPE)))
            .andExpect(jsonPath("$.[*].baseCurrency").value(hasItem(DEFAULT_BASE_CURRENCY)))
            .andExpect(jsonPath("$.[*].quoteCurrency").value(hasItem(DEFAULT_QUOTE_CURRENCY)))
            .andExpect(jsonPath("$.[*].askPrice").value(hasItem(sameNumber(DEFAULT_ASK_PRICE))))
            .andExpect(jsonPath("$.[*].bidPrice").value(hasItem(sameNumber(DEFAULT_BID_PRICE))))
            .andExpect(jsonPath("$.[*].midPrice").value(hasItem(sameNumber(DEFAULT_MID_PRICE))))
            .andExpect(jsonPath("$.[*].askMargin").value(hasItem(sameNumber(DEFAULT_ASK_MARGIN))))
            .andExpect(jsonPath("$.[*].bidMargin").value(hasItem(sameNumber(DEFAULT_BID_MARGIN))))
            .andExpect(jsonPath("$.[*].segment").value(hasItem(DEFAULT_SEGMENT)))
            .andExpect(jsonPath("$.[*].amountSlab").value(hasItem(DEFAULT_AMOUNT_SLAB)))
            .andExpect(jsonPath("$.[*].amountSlabValue").value(hasItem(sameNumber(DEFAULT_AMOUNT_SLAB_VALUE))))
            .andExpect(jsonPath("$.[*].amountSlabOperator").value(hasItem(DEFAULT_AMOUNT_SLAB_OPERATOR)))
            .andExpect(jsonPath("$.[*].amountSlabCurrency").value(hasItem(DEFAULT_AMOUNT_SLAB_CURRENCY)))
            .andExpect(jsonPath("$.[*].recordStatus").value(hasItem(DEFAULT_RECORD_STATUS.toString())))
            .andExpect(jsonPath("$.[*].validfrom").value(hasItem(DEFAULT_VALIDFROM.toString())))
            .andExpect(jsonPath("$.[*].validTo").value(hasItem(DEFAULT_VALID_TO.toString())));
    }

    @Test
    @Transactional
    void getFxRate() throws Exception {
        // Initialize the database
        fxRateRepository.saveAndFlush(fxRate);

        // Get the fxRate
        restFxRateMockMvc
            .perform(get(ENTITY_API_URL_ID, fxRate.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(fxRate.getId().intValue()))
            .andExpect(jsonPath("$.type").value(DEFAULT_TYPE))
            .andExpect(jsonPath("$.baseCurrency").value(DEFAULT_BASE_CURRENCY))
            .andExpect(jsonPath("$.quoteCurrency").value(DEFAULT_QUOTE_CURRENCY))
            .andExpect(jsonPath("$.askPrice").value(sameNumber(DEFAULT_ASK_PRICE)))
            .andExpect(jsonPath("$.bidPrice").value(sameNumber(DEFAULT_BID_PRICE)))
            .andExpect(jsonPath("$.midPrice").value(sameNumber(DEFAULT_MID_PRICE)))
            .andExpect(jsonPath("$.askMargin").value(sameNumber(DEFAULT_ASK_MARGIN)))
            .andExpect(jsonPath("$.bidMargin").value(sameNumber(DEFAULT_BID_MARGIN)))
            .andExpect(jsonPath("$.segment").value(DEFAULT_SEGMENT))
            .andExpect(jsonPath("$.amountSlab").value(DEFAULT_AMOUNT_SLAB))
            .andExpect(jsonPath("$.amountSlabValue").value(sameNumber(DEFAULT_AMOUNT_SLAB_VALUE)))
            .andExpect(jsonPath("$.amountSlabOperator").value(DEFAULT_AMOUNT_SLAB_OPERATOR))
            .andExpect(jsonPath("$.amountSlabCurrency").value(DEFAULT_AMOUNT_SLAB_CURRENCY))
            .andExpect(jsonPath("$.recordStatus").value(DEFAULT_RECORD_STATUS.toString()))
            .andExpect(jsonPath("$.validfrom").value(DEFAULT_VALIDFROM.toString()))
            .andExpect(jsonPath("$.validTo").value(DEFAULT_VALID_TO.toString()));
    }

    @Test
    @Transactional
    void getNonExistingFxRate() throws Exception {
        // Get the fxRate
        restFxRateMockMvc.perform(get(ENTITY_API_URL_ID, Long.MAX_VALUE)).andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    void putNewFxRate() throws Exception {
        // Initialize the database
        fxRateRepository.saveAndFlush(fxRate);

        int databaseSizeBeforeUpdate = fxRateRepository.findAll().size();

        // Update the fxRate
        FxRate updatedFxRate = fxRateRepository.findById(fxRate.getId()).get();
        // Disconnect from session so that the updates on updatedFxRate are not directly saved in db
        em.detach(updatedFxRate);
        updatedFxRate
            .type(UPDATED_TYPE)
            .baseCurrency(UPDATED_BASE_CURRENCY)
            .quoteCurrency(UPDATED_QUOTE_CURRENCY)
            .askPrice(UPDATED_ASK_PRICE)
            .bidPrice(UPDATED_BID_PRICE)
            .midPrice(UPDATED_MID_PRICE)
            .askMargin(UPDATED_ASK_MARGIN)
            .bidMargin(UPDATED_BID_MARGIN)
            .segment(UPDATED_SEGMENT)
            .amountSlab(UPDATED_AMOUNT_SLAB)
            .amountSlabValue(UPDATED_AMOUNT_SLAB_VALUE)
            .amountSlabOperator(UPDATED_AMOUNT_SLAB_OPERATOR)
            .amountSlabCurrency(UPDATED_AMOUNT_SLAB_CURRENCY)
            .recordStatus(UPDATED_RECORD_STATUS)
            .validfrom(UPDATED_VALIDFROM)
            .validTo(UPDATED_VALID_TO);

        restFxRateMockMvc
            .perform(
                put(ENTITY_API_URL_ID, updatedFxRate.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(updatedFxRate))
            )
            .andExpect(status().isOk());

        // Validate the FxRate in the database
        List<FxRate> fxRateList = fxRateRepository.findAll();
        assertThat(fxRateList).hasSize(databaseSizeBeforeUpdate);
        FxRate testFxRate = fxRateList.get(fxRateList.size() - 1);
        assertThat(testFxRate.getType()).isEqualTo(UPDATED_TYPE);
        assertThat(testFxRate.getBaseCurrency()).isEqualTo(UPDATED_BASE_CURRENCY);
        assertThat(testFxRate.getQuoteCurrency()).isEqualTo(UPDATED_QUOTE_CURRENCY);
        assertThat(testFxRate.getAskPrice()).isEqualTo(UPDATED_ASK_PRICE);
        assertThat(testFxRate.getBidPrice()).isEqualTo(UPDATED_BID_PRICE);
        assertThat(testFxRate.getMidPrice()).isEqualTo(UPDATED_MID_PRICE);
        assertThat(testFxRate.getAskMargin()).isEqualTo(UPDATED_ASK_MARGIN);
        assertThat(testFxRate.getBidMargin()).isEqualTo(UPDATED_BID_MARGIN);
        assertThat(testFxRate.getSegment()).isEqualTo(UPDATED_SEGMENT);
        assertThat(testFxRate.getAmountSlab()).isEqualTo(UPDATED_AMOUNT_SLAB);
        assertThat(testFxRate.getAmountSlabValue()).isEqualTo(UPDATED_AMOUNT_SLAB_VALUE);
        assertThat(testFxRate.getAmountSlabOperator()).isEqualTo(UPDATED_AMOUNT_SLAB_OPERATOR);
        assertThat(testFxRate.getAmountSlabCurrency()).isEqualTo(UPDATED_AMOUNT_SLAB_CURRENCY);
        assertThat(testFxRate.getRecordStatus()).isEqualTo(UPDATED_RECORD_STATUS);
        assertThat(testFxRate.getValidfrom()).isEqualTo(UPDATED_VALIDFROM);
        assertThat(testFxRate.getValidTo()).isEqualTo(UPDATED_VALID_TO);
    }

    @Test
    @Transactional
    void putNonExistingFxRate() throws Exception {
        int databaseSizeBeforeUpdate = fxRateRepository.findAll().size();
        fxRate.setId(count.incrementAndGet());

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restFxRateMockMvc
            .perform(
                put(ENTITY_API_URL_ID, fxRate.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(fxRate))
            )
            .andExpect(status().isBadRequest());

        // Validate the FxRate in the database
        List<FxRate> fxRateList = fxRateRepository.findAll();
        assertThat(fxRateList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithIdMismatchFxRate() throws Exception {
        int databaseSizeBeforeUpdate = fxRateRepository.findAll().size();
        fxRate.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restFxRateMockMvc
            .perform(
                put(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(fxRate))
            )
            .andExpect(status().isBadRequest());

        // Validate the FxRate in the database
        List<FxRate> fxRateList = fxRateRepository.findAll();
        assertThat(fxRateList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithMissingIdPathParamFxRate() throws Exception {
        int databaseSizeBeforeUpdate = fxRateRepository.findAll().size();
        fxRate.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restFxRateMockMvc
            .perform(put(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(fxRate)))
            .andExpect(status().isMethodNotAllowed());

        // Validate the FxRate in the database
        List<FxRate> fxRateList = fxRateRepository.findAll();
        assertThat(fxRateList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void partialUpdateFxRateWithPatch() throws Exception {
        // Initialize the database
        fxRateRepository.saveAndFlush(fxRate);

        int databaseSizeBeforeUpdate = fxRateRepository.findAll().size();

        // Update the fxRate using partial update
        FxRate partialUpdatedFxRate = new FxRate();
        partialUpdatedFxRate.setId(fxRate.getId());

        partialUpdatedFxRate
            .quoteCurrency(UPDATED_QUOTE_CURRENCY)
            .askPrice(UPDATED_ASK_PRICE)
            .midPrice(UPDATED_MID_PRICE)
            .bidMargin(UPDATED_BID_MARGIN)
            .segment(UPDATED_SEGMENT)
            .amountSlabValue(UPDATED_AMOUNT_SLAB_VALUE)
            .recordStatus(UPDATED_RECORD_STATUS)
            .validfrom(UPDATED_VALIDFROM)
            .validTo(UPDATED_VALID_TO);

        restFxRateMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedFxRate.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedFxRate))
            )
            .andExpect(status().isOk());

        // Validate the FxRate in the database
        List<FxRate> fxRateList = fxRateRepository.findAll();
        assertThat(fxRateList).hasSize(databaseSizeBeforeUpdate);
        FxRate testFxRate = fxRateList.get(fxRateList.size() - 1);
        assertThat(testFxRate.getType()).isEqualTo(DEFAULT_TYPE);
        assertThat(testFxRate.getBaseCurrency()).isEqualTo(DEFAULT_BASE_CURRENCY);
        assertThat(testFxRate.getQuoteCurrency()).isEqualTo(UPDATED_QUOTE_CURRENCY);
        assertThat(testFxRate.getAskPrice()).isEqualByComparingTo(UPDATED_ASK_PRICE);
        assertThat(testFxRate.getBidPrice()).isEqualByComparingTo(DEFAULT_BID_PRICE);
        assertThat(testFxRate.getMidPrice()).isEqualByComparingTo(UPDATED_MID_PRICE);
        assertThat(testFxRate.getAskMargin()).isEqualByComparingTo(DEFAULT_ASK_MARGIN);
        assertThat(testFxRate.getBidMargin()).isEqualByComparingTo(UPDATED_BID_MARGIN);
        assertThat(testFxRate.getSegment()).isEqualTo(UPDATED_SEGMENT);
        assertThat(testFxRate.getAmountSlab()).isEqualTo(DEFAULT_AMOUNT_SLAB);
        assertThat(testFxRate.getAmountSlabValue()).isEqualByComparingTo(UPDATED_AMOUNT_SLAB_VALUE);
        assertThat(testFxRate.getAmountSlabOperator()).isEqualTo(DEFAULT_AMOUNT_SLAB_OPERATOR);
        assertThat(testFxRate.getAmountSlabCurrency()).isEqualTo(DEFAULT_AMOUNT_SLAB_CURRENCY);
        assertThat(testFxRate.getRecordStatus()).isEqualTo(UPDATED_RECORD_STATUS);
        assertThat(testFxRate.getValidfrom()).isEqualTo(UPDATED_VALIDFROM);
        assertThat(testFxRate.getValidTo()).isEqualTo(UPDATED_VALID_TO);
    }

    @Test
    @Transactional
    void fullUpdateFxRateWithPatch() throws Exception {
        // Initialize the database
        fxRateRepository.saveAndFlush(fxRate);

        int databaseSizeBeforeUpdate = fxRateRepository.findAll().size();

        // Update the fxRate using partial update
        FxRate partialUpdatedFxRate = new FxRate();
        partialUpdatedFxRate.setId(fxRate.getId());

        partialUpdatedFxRate
            .type(UPDATED_TYPE)
            .baseCurrency(UPDATED_BASE_CURRENCY)
            .quoteCurrency(UPDATED_QUOTE_CURRENCY)
            .askPrice(UPDATED_ASK_PRICE)
            .bidPrice(UPDATED_BID_PRICE)
            .midPrice(UPDATED_MID_PRICE)
            .askMargin(UPDATED_ASK_MARGIN)
            .bidMargin(UPDATED_BID_MARGIN)
            .segment(UPDATED_SEGMENT)
            .amountSlab(UPDATED_AMOUNT_SLAB)
            .amountSlabValue(UPDATED_AMOUNT_SLAB_VALUE)
            .amountSlabOperator(UPDATED_AMOUNT_SLAB_OPERATOR)
            .amountSlabCurrency(UPDATED_AMOUNT_SLAB_CURRENCY)
            .recordStatus(UPDATED_RECORD_STATUS)
            .validfrom(UPDATED_VALIDFROM)
            .validTo(UPDATED_VALID_TO);

        restFxRateMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedFxRate.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedFxRate))
            )
            .andExpect(status().isOk());

        // Validate the FxRate in the database
        List<FxRate> fxRateList = fxRateRepository.findAll();
        assertThat(fxRateList).hasSize(databaseSizeBeforeUpdate);
        FxRate testFxRate = fxRateList.get(fxRateList.size() - 1);
        assertThat(testFxRate.getType()).isEqualTo(UPDATED_TYPE);
        assertThat(testFxRate.getBaseCurrency()).isEqualTo(UPDATED_BASE_CURRENCY);
        assertThat(testFxRate.getQuoteCurrency()).isEqualTo(UPDATED_QUOTE_CURRENCY);
        assertThat(testFxRate.getAskPrice()).isEqualByComparingTo(UPDATED_ASK_PRICE);
        assertThat(testFxRate.getBidPrice()).isEqualByComparingTo(UPDATED_BID_PRICE);
        assertThat(testFxRate.getMidPrice()).isEqualByComparingTo(UPDATED_MID_PRICE);
        assertThat(testFxRate.getAskMargin()).isEqualByComparingTo(UPDATED_ASK_MARGIN);
        assertThat(testFxRate.getBidMargin()).isEqualByComparingTo(UPDATED_BID_MARGIN);
        assertThat(testFxRate.getSegment()).isEqualTo(UPDATED_SEGMENT);
        assertThat(testFxRate.getAmountSlab()).isEqualTo(UPDATED_AMOUNT_SLAB);
        assertThat(testFxRate.getAmountSlabValue()).isEqualByComparingTo(UPDATED_AMOUNT_SLAB_VALUE);
        assertThat(testFxRate.getAmountSlabOperator()).isEqualTo(UPDATED_AMOUNT_SLAB_OPERATOR);
        assertThat(testFxRate.getAmountSlabCurrency()).isEqualTo(UPDATED_AMOUNT_SLAB_CURRENCY);
        assertThat(testFxRate.getRecordStatus()).isEqualTo(UPDATED_RECORD_STATUS);
        assertThat(testFxRate.getValidfrom()).isEqualTo(UPDATED_VALIDFROM);
        assertThat(testFxRate.getValidTo()).isEqualTo(UPDATED_VALID_TO);
    }

    @Test
    @Transactional
    void patchNonExistingFxRate() throws Exception {
        int databaseSizeBeforeUpdate = fxRateRepository.findAll().size();
        fxRate.setId(count.incrementAndGet());

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restFxRateMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, fxRate.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(fxRate))
            )
            .andExpect(status().isBadRequest());

        // Validate the FxRate in the database
        List<FxRate> fxRateList = fxRateRepository.findAll();
        assertThat(fxRateList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithIdMismatchFxRate() throws Exception {
        int databaseSizeBeforeUpdate = fxRateRepository.findAll().size();
        fxRate.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restFxRateMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(fxRate))
            )
            .andExpect(status().isBadRequest());

        // Validate the FxRate in the database
        List<FxRate> fxRateList = fxRateRepository.findAll();
        assertThat(fxRateList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithMissingIdPathParamFxRate() throws Exception {
        int databaseSizeBeforeUpdate = fxRateRepository.findAll().size();
        fxRate.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restFxRateMockMvc
            .perform(patch(ENTITY_API_URL).contentType("application/merge-patch+json").content(TestUtil.convertObjectToJsonBytes(fxRate)))
            .andExpect(status().isMethodNotAllowed());

        // Validate the FxRate in the database
        List<FxRate> fxRateList = fxRateRepository.findAll();
        assertThat(fxRateList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void deleteFxRate() throws Exception {
        // Initialize the database
        fxRateRepository.saveAndFlush(fxRate);

        int databaseSizeBeforeDelete = fxRateRepository.findAll().size();

        // Delete the fxRate
        restFxRateMockMvc
            .perform(delete(ENTITY_API_URL_ID, fxRate.getId()).accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<FxRate> fxRateList = fxRateRepository.findAll();
        assertThat(fxRateList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
