package com.iconnect101.glapp.web.rest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import com.iconnect101.glapp.IntegrationTest;
import com.iconnect101.glapp.domain.PurposeOfTxn;
import com.iconnect101.glapp.repository.PurposeOfTxnRepository;
import java.util.List;
import java.util.Random;
import java.util.concurrent.atomic.AtomicLong;
import javax.persistence.EntityManager;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;

/**
 * Integration tests for the {@link PurposeOfTxnResource} REST controller.
 */
@IntegrationTest
@AutoConfigureMockMvc
@WithMockUser
class PurposeOfTxnResourceIT {

    private static final String DEFAULT_NAME = "AAAAAAAAAA";
    private static final String UPDATED_NAME = "BBBBBBBBBB";

    private static final String DEFAULT_CODE = "AAAAAAAAAA";
    private static final String UPDATED_CODE = "BBBBBBBBBB";

    private static final String ENTITY_API_URL = "/api/purpose-of-txns";
    private static final String ENTITY_API_URL_ID = ENTITY_API_URL + "/{id}";

    private static Random random = new Random();
    private static AtomicLong count = new AtomicLong(random.nextInt() + (2 * Integer.MAX_VALUE));

    @Autowired
    private PurposeOfTxnRepository purposeOfTxnRepository;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restPurposeOfTxnMockMvc;

    private PurposeOfTxn purposeOfTxn;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static PurposeOfTxn createEntity(EntityManager em) {
        PurposeOfTxn purposeOfTxn = new PurposeOfTxn().name(DEFAULT_NAME).code(DEFAULT_CODE);
        return purposeOfTxn;
    }

    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static PurposeOfTxn createUpdatedEntity(EntityManager em) {
        PurposeOfTxn purposeOfTxn = new PurposeOfTxn().name(UPDATED_NAME).code(UPDATED_CODE);
        return purposeOfTxn;
    }

    @BeforeEach
    public void initTest() {
        purposeOfTxn = createEntity(em);
    }

    @Test
    @Transactional
    void createPurposeOfTxn() throws Exception {
        int databaseSizeBeforeCreate = purposeOfTxnRepository.findAll().size();
        // Create the PurposeOfTxn
        restPurposeOfTxnMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(purposeOfTxn)))
            .andExpect(status().isCreated());

        // Validate the PurposeOfTxn in the database
        List<PurposeOfTxn> purposeOfTxnList = purposeOfTxnRepository.findAll();
        assertThat(purposeOfTxnList).hasSize(databaseSizeBeforeCreate + 1);
        PurposeOfTxn testPurposeOfTxn = purposeOfTxnList.get(purposeOfTxnList.size() - 1);
        assertThat(testPurposeOfTxn.getName()).isEqualTo(DEFAULT_NAME);
        assertThat(testPurposeOfTxn.getCode()).isEqualTo(DEFAULT_CODE);
    }

    @Test
    @Transactional
    void createPurposeOfTxnWithExistingId() throws Exception {
        // Create the PurposeOfTxn with an existing ID
        purposeOfTxn.setId(1L);

        int databaseSizeBeforeCreate = purposeOfTxnRepository.findAll().size();

        // An entity with an existing ID cannot be created, so this API call must fail
        restPurposeOfTxnMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(purposeOfTxn)))
            .andExpect(status().isBadRequest());

        // Validate the PurposeOfTxn in the database
        List<PurposeOfTxn> purposeOfTxnList = purposeOfTxnRepository.findAll();
        assertThat(purposeOfTxnList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    void getAllPurposeOfTxns() throws Exception {
        // Initialize the database
        purposeOfTxnRepository.saveAndFlush(purposeOfTxn);

        // Get all the purposeOfTxnList
        restPurposeOfTxnMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(purposeOfTxn.getId().intValue())))
            .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME)))
            .andExpect(jsonPath("$.[*].code").value(hasItem(DEFAULT_CODE)));
    }

    @Test
    @Transactional
    void getPurposeOfTxn() throws Exception {
        // Initialize the database
        purposeOfTxnRepository.saveAndFlush(purposeOfTxn);

        // Get the purposeOfTxn
        restPurposeOfTxnMockMvc
            .perform(get(ENTITY_API_URL_ID, purposeOfTxn.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(purposeOfTxn.getId().intValue()))
            .andExpect(jsonPath("$.name").value(DEFAULT_NAME))
            .andExpect(jsonPath("$.code").value(DEFAULT_CODE));
    }

    @Test
    @Transactional
    void getNonExistingPurposeOfTxn() throws Exception {
        // Get the purposeOfTxn
        restPurposeOfTxnMockMvc.perform(get(ENTITY_API_URL_ID, Long.MAX_VALUE)).andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    void putNewPurposeOfTxn() throws Exception {
        // Initialize the database
        purposeOfTxnRepository.saveAndFlush(purposeOfTxn);

        int databaseSizeBeforeUpdate = purposeOfTxnRepository.findAll().size();

        // Update the purposeOfTxn
        PurposeOfTxn updatedPurposeOfTxn = purposeOfTxnRepository.findById(purposeOfTxn.getId()).get();
        // Disconnect from session so that the updates on updatedPurposeOfTxn are not directly saved in db
        em.detach(updatedPurposeOfTxn);
        updatedPurposeOfTxn.name(UPDATED_NAME).code(UPDATED_CODE);

        restPurposeOfTxnMockMvc
            .perform(
                put(ENTITY_API_URL_ID, updatedPurposeOfTxn.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(updatedPurposeOfTxn))
            )
            .andExpect(status().isOk());

        // Validate the PurposeOfTxn in the database
        List<PurposeOfTxn> purposeOfTxnList = purposeOfTxnRepository.findAll();
        assertThat(purposeOfTxnList).hasSize(databaseSizeBeforeUpdate);
        PurposeOfTxn testPurposeOfTxn = purposeOfTxnList.get(purposeOfTxnList.size() - 1);
        assertThat(testPurposeOfTxn.getName()).isEqualTo(UPDATED_NAME);
        assertThat(testPurposeOfTxn.getCode()).isEqualTo(UPDATED_CODE);
    }

    @Test
    @Transactional
    void putNonExistingPurposeOfTxn() throws Exception {
        int databaseSizeBeforeUpdate = purposeOfTxnRepository.findAll().size();
        purposeOfTxn.setId(count.incrementAndGet());

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restPurposeOfTxnMockMvc
            .perform(
                put(ENTITY_API_URL_ID, purposeOfTxn.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(purposeOfTxn))
            )
            .andExpect(status().isBadRequest());

        // Validate the PurposeOfTxn in the database
        List<PurposeOfTxn> purposeOfTxnList = purposeOfTxnRepository.findAll();
        assertThat(purposeOfTxnList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithIdMismatchPurposeOfTxn() throws Exception {
        int databaseSizeBeforeUpdate = purposeOfTxnRepository.findAll().size();
        purposeOfTxn.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restPurposeOfTxnMockMvc
            .perform(
                put(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(purposeOfTxn))
            )
            .andExpect(status().isBadRequest());

        // Validate the PurposeOfTxn in the database
        List<PurposeOfTxn> purposeOfTxnList = purposeOfTxnRepository.findAll();
        assertThat(purposeOfTxnList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithMissingIdPathParamPurposeOfTxn() throws Exception {
        int databaseSizeBeforeUpdate = purposeOfTxnRepository.findAll().size();
        purposeOfTxn.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restPurposeOfTxnMockMvc
            .perform(put(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(purposeOfTxn)))
            .andExpect(status().isMethodNotAllowed());

        // Validate the PurposeOfTxn in the database
        List<PurposeOfTxn> purposeOfTxnList = purposeOfTxnRepository.findAll();
        assertThat(purposeOfTxnList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void partialUpdatePurposeOfTxnWithPatch() throws Exception {
        // Initialize the database
        purposeOfTxnRepository.saveAndFlush(purposeOfTxn);

        int databaseSizeBeforeUpdate = purposeOfTxnRepository.findAll().size();

        // Update the purposeOfTxn using partial update
        PurposeOfTxn partialUpdatedPurposeOfTxn = new PurposeOfTxn();
        partialUpdatedPurposeOfTxn.setId(purposeOfTxn.getId());

        partialUpdatedPurposeOfTxn.name(UPDATED_NAME).code(UPDATED_CODE);

        restPurposeOfTxnMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedPurposeOfTxn.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedPurposeOfTxn))
            )
            .andExpect(status().isOk());

        // Validate the PurposeOfTxn in the database
        List<PurposeOfTxn> purposeOfTxnList = purposeOfTxnRepository.findAll();
        assertThat(purposeOfTxnList).hasSize(databaseSizeBeforeUpdate);
        PurposeOfTxn testPurposeOfTxn = purposeOfTxnList.get(purposeOfTxnList.size() - 1);
        assertThat(testPurposeOfTxn.getName()).isEqualTo(UPDATED_NAME);
        assertThat(testPurposeOfTxn.getCode()).isEqualTo(UPDATED_CODE);
    }

    @Test
    @Transactional
    void fullUpdatePurposeOfTxnWithPatch() throws Exception {
        // Initialize the database
        purposeOfTxnRepository.saveAndFlush(purposeOfTxn);

        int databaseSizeBeforeUpdate = purposeOfTxnRepository.findAll().size();

        // Update the purposeOfTxn using partial update
        PurposeOfTxn partialUpdatedPurposeOfTxn = new PurposeOfTxn();
        partialUpdatedPurposeOfTxn.setId(purposeOfTxn.getId());

        partialUpdatedPurposeOfTxn.name(UPDATED_NAME).code(UPDATED_CODE);

        restPurposeOfTxnMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedPurposeOfTxn.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedPurposeOfTxn))
            )
            .andExpect(status().isOk());

        // Validate the PurposeOfTxn in the database
        List<PurposeOfTxn> purposeOfTxnList = purposeOfTxnRepository.findAll();
        assertThat(purposeOfTxnList).hasSize(databaseSizeBeforeUpdate);
        PurposeOfTxn testPurposeOfTxn = purposeOfTxnList.get(purposeOfTxnList.size() - 1);
        assertThat(testPurposeOfTxn.getName()).isEqualTo(UPDATED_NAME);
        assertThat(testPurposeOfTxn.getCode()).isEqualTo(UPDATED_CODE);
    }

    @Test
    @Transactional
    void patchNonExistingPurposeOfTxn() throws Exception {
        int databaseSizeBeforeUpdate = purposeOfTxnRepository.findAll().size();
        purposeOfTxn.setId(count.incrementAndGet());

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restPurposeOfTxnMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, purposeOfTxn.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(purposeOfTxn))
            )
            .andExpect(status().isBadRequest());

        // Validate the PurposeOfTxn in the database
        List<PurposeOfTxn> purposeOfTxnList = purposeOfTxnRepository.findAll();
        assertThat(purposeOfTxnList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithIdMismatchPurposeOfTxn() throws Exception {
        int databaseSizeBeforeUpdate = purposeOfTxnRepository.findAll().size();
        purposeOfTxn.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restPurposeOfTxnMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(purposeOfTxn))
            )
            .andExpect(status().isBadRequest());

        // Validate the PurposeOfTxn in the database
        List<PurposeOfTxn> purposeOfTxnList = purposeOfTxnRepository.findAll();
        assertThat(purposeOfTxnList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithMissingIdPathParamPurposeOfTxn() throws Exception {
        int databaseSizeBeforeUpdate = purposeOfTxnRepository.findAll().size();
        purposeOfTxn.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restPurposeOfTxnMockMvc
            .perform(
                patch(ENTITY_API_URL).contentType("application/merge-patch+json").content(TestUtil.convertObjectToJsonBytes(purposeOfTxn))
            )
            .andExpect(status().isMethodNotAllowed());

        // Validate the PurposeOfTxn in the database
        List<PurposeOfTxn> purposeOfTxnList = purposeOfTxnRepository.findAll();
        assertThat(purposeOfTxnList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void deletePurposeOfTxn() throws Exception {
        // Initialize the database
        purposeOfTxnRepository.saveAndFlush(purposeOfTxn);

        int databaseSizeBeforeDelete = purposeOfTxnRepository.findAll().size();

        // Delete the purposeOfTxn
        restPurposeOfTxnMockMvc
            .perform(delete(ENTITY_API_URL_ID, purposeOfTxn.getId()).accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<PurposeOfTxn> purposeOfTxnList = purposeOfTxnRepository.findAll();
        assertThat(purposeOfTxnList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
