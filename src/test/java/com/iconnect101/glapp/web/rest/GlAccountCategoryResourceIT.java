package com.iconnect101.glapp.web.rest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import com.iconnect101.glapp.IntegrationTest;
import com.iconnect101.glapp.domain.GlAccountCategory;
import com.iconnect101.glapp.repository.GlAccountCategoryRepository;
import java.util.List;
import java.util.Random;
import java.util.concurrent.atomic.AtomicLong;
import javax.persistence.EntityManager;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;

/**
 * Integration tests for the {@link GlAccountCategoryResource} REST controller.
 */
@IntegrationTest
@AutoConfigureMockMvc
@WithMockUser
class GlAccountCategoryResourceIT {

    private static final String DEFAULT_NAME = "AAAAAAAAAA";
    private static final String UPDATED_NAME = "BBBBBBBBBB";

    private static final String DEFAULT_CODE = "AAAAAAAAAA";
    private static final String UPDATED_CODE = "BBBBBBBBBB";

    private static final String ENTITY_API_URL = "/api/gl-account-categories";
    private static final String ENTITY_API_URL_ID = ENTITY_API_URL + "/{id}";

    private static Random random = new Random();
    private static AtomicLong count = new AtomicLong(random.nextInt() + (2 * Integer.MAX_VALUE));

    @Autowired
    private GlAccountCategoryRepository glAccountCategoryRepository;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restGlAccountCategoryMockMvc;

    private GlAccountCategory glAccountCategory;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static GlAccountCategory createEntity(EntityManager em) {
        GlAccountCategory glAccountCategory = new GlAccountCategory().name(DEFAULT_NAME).code(DEFAULT_CODE);
        return glAccountCategory;
    }

    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static GlAccountCategory createUpdatedEntity(EntityManager em) {
        GlAccountCategory glAccountCategory = new GlAccountCategory().name(UPDATED_NAME).code(UPDATED_CODE);
        return glAccountCategory;
    }

    @BeforeEach
    public void initTest() {
        glAccountCategory = createEntity(em);
    }

    @Test
    @Transactional
    void createGlAccountCategory() throws Exception {
        int databaseSizeBeforeCreate = glAccountCategoryRepository.findAll().size();
        // Create the GlAccountCategory
        restGlAccountCategoryMockMvc
            .perform(
                post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(glAccountCategory))
            )
            .andExpect(status().isCreated());

        // Validate the GlAccountCategory in the database
        List<GlAccountCategory> glAccountCategoryList = glAccountCategoryRepository.findAll();
        assertThat(glAccountCategoryList).hasSize(databaseSizeBeforeCreate + 1);
        GlAccountCategory testGlAccountCategory = glAccountCategoryList.get(glAccountCategoryList.size() - 1);
        assertThat(testGlAccountCategory.getName()).isEqualTo(DEFAULT_NAME);
        assertThat(testGlAccountCategory.getCode()).isEqualTo(DEFAULT_CODE);
    }

    @Test
    @Transactional
    void createGlAccountCategoryWithExistingId() throws Exception {
        // Create the GlAccountCategory with an existing ID
        glAccountCategory.setId(1L);

        int databaseSizeBeforeCreate = glAccountCategoryRepository.findAll().size();

        // An entity with an existing ID cannot be created, so this API call must fail
        restGlAccountCategoryMockMvc
            .perform(
                post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(glAccountCategory))
            )
            .andExpect(status().isBadRequest());

        // Validate the GlAccountCategory in the database
        List<GlAccountCategory> glAccountCategoryList = glAccountCategoryRepository.findAll();
        assertThat(glAccountCategoryList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    void getAllGlAccountCategories() throws Exception {
        // Initialize the database
        glAccountCategoryRepository.saveAndFlush(glAccountCategory);

        // Get all the glAccountCategoryList
        restGlAccountCategoryMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(glAccountCategory.getId().intValue())))
            .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME)))
            .andExpect(jsonPath("$.[*].code").value(hasItem(DEFAULT_CODE)));
    }

    @Test
    @Transactional
    void getGlAccountCategory() throws Exception {
        // Initialize the database
        glAccountCategoryRepository.saveAndFlush(glAccountCategory);

        // Get the glAccountCategory
        restGlAccountCategoryMockMvc
            .perform(get(ENTITY_API_URL_ID, glAccountCategory.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(glAccountCategory.getId().intValue()))
            .andExpect(jsonPath("$.name").value(DEFAULT_NAME))
            .andExpect(jsonPath("$.code").value(DEFAULT_CODE));
    }

    @Test
    @Transactional
    void getNonExistingGlAccountCategory() throws Exception {
        // Get the glAccountCategory
        restGlAccountCategoryMockMvc.perform(get(ENTITY_API_URL_ID, Long.MAX_VALUE)).andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    void putNewGlAccountCategory() throws Exception {
        // Initialize the database
        glAccountCategoryRepository.saveAndFlush(glAccountCategory);

        int databaseSizeBeforeUpdate = glAccountCategoryRepository.findAll().size();

        // Update the glAccountCategory
        GlAccountCategory updatedGlAccountCategory = glAccountCategoryRepository.findById(glAccountCategory.getId()).get();
        // Disconnect from session so that the updates on updatedGlAccountCategory are not directly saved in db
        em.detach(updatedGlAccountCategory);
        updatedGlAccountCategory.name(UPDATED_NAME).code(UPDATED_CODE);

        restGlAccountCategoryMockMvc
            .perform(
                put(ENTITY_API_URL_ID, updatedGlAccountCategory.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(updatedGlAccountCategory))
            )
            .andExpect(status().isOk());

        // Validate the GlAccountCategory in the database
        List<GlAccountCategory> glAccountCategoryList = glAccountCategoryRepository.findAll();
        assertThat(glAccountCategoryList).hasSize(databaseSizeBeforeUpdate);
        GlAccountCategory testGlAccountCategory = glAccountCategoryList.get(glAccountCategoryList.size() - 1);
        assertThat(testGlAccountCategory.getName()).isEqualTo(UPDATED_NAME);
        assertThat(testGlAccountCategory.getCode()).isEqualTo(UPDATED_CODE);
    }

    @Test
    @Transactional
    void putNonExistingGlAccountCategory() throws Exception {
        int databaseSizeBeforeUpdate = glAccountCategoryRepository.findAll().size();
        glAccountCategory.setId(count.incrementAndGet());

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restGlAccountCategoryMockMvc
            .perform(
                put(ENTITY_API_URL_ID, glAccountCategory.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(glAccountCategory))
            )
            .andExpect(status().isBadRequest());

        // Validate the GlAccountCategory in the database
        List<GlAccountCategory> glAccountCategoryList = glAccountCategoryRepository.findAll();
        assertThat(glAccountCategoryList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithIdMismatchGlAccountCategory() throws Exception {
        int databaseSizeBeforeUpdate = glAccountCategoryRepository.findAll().size();
        glAccountCategory.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restGlAccountCategoryMockMvc
            .perform(
                put(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(glAccountCategory))
            )
            .andExpect(status().isBadRequest());

        // Validate the GlAccountCategory in the database
        List<GlAccountCategory> glAccountCategoryList = glAccountCategoryRepository.findAll();
        assertThat(glAccountCategoryList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithMissingIdPathParamGlAccountCategory() throws Exception {
        int databaseSizeBeforeUpdate = glAccountCategoryRepository.findAll().size();
        glAccountCategory.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restGlAccountCategoryMockMvc
            .perform(
                put(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(glAccountCategory))
            )
            .andExpect(status().isMethodNotAllowed());

        // Validate the GlAccountCategory in the database
        List<GlAccountCategory> glAccountCategoryList = glAccountCategoryRepository.findAll();
        assertThat(glAccountCategoryList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void partialUpdateGlAccountCategoryWithPatch() throws Exception {
        // Initialize the database
        glAccountCategoryRepository.saveAndFlush(glAccountCategory);

        int databaseSizeBeforeUpdate = glAccountCategoryRepository.findAll().size();

        // Update the glAccountCategory using partial update
        GlAccountCategory partialUpdatedGlAccountCategory = new GlAccountCategory();
        partialUpdatedGlAccountCategory.setId(glAccountCategory.getId());

        partialUpdatedGlAccountCategory.name(UPDATED_NAME);

        restGlAccountCategoryMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedGlAccountCategory.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedGlAccountCategory))
            )
            .andExpect(status().isOk());

        // Validate the GlAccountCategory in the database
        List<GlAccountCategory> glAccountCategoryList = glAccountCategoryRepository.findAll();
        assertThat(glAccountCategoryList).hasSize(databaseSizeBeforeUpdate);
        GlAccountCategory testGlAccountCategory = glAccountCategoryList.get(glAccountCategoryList.size() - 1);
        assertThat(testGlAccountCategory.getName()).isEqualTo(UPDATED_NAME);
        assertThat(testGlAccountCategory.getCode()).isEqualTo(DEFAULT_CODE);
    }

    @Test
    @Transactional
    void fullUpdateGlAccountCategoryWithPatch() throws Exception {
        // Initialize the database
        glAccountCategoryRepository.saveAndFlush(glAccountCategory);

        int databaseSizeBeforeUpdate = glAccountCategoryRepository.findAll().size();

        // Update the glAccountCategory using partial update
        GlAccountCategory partialUpdatedGlAccountCategory = new GlAccountCategory();
        partialUpdatedGlAccountCategory.setId(glAccountCategory.getId());

        partialUpdatedGlAccountCategory.name(UPDATED_NAME).code(UPDATED_CODE);

        restGlAccountCategoryMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedGlAccountCategory.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedGlAccountCategory))
            )
            .andExpect(status().isOk());

        // Validate the GlAccountCategory in the database
        List<GlAccountCategory> glAccountCategoryList = glAccountCategoryRepository.findAll();
        assertThat(glAccountCategoryList).hasSize(databaseSizeBeforeUpdate);
        GlAccountCategory testGlAccountCategory = glAccountCategoryList.get(glAccountCategoryList.size() - 1);
        assertThat(testGlAccountCategory.getName()).isEqualTo(UPDATED_NAME);
        assertThat(testGlAccountCategory.getCode()).isEqualTo(UPDATED_CODE);
    }

    @Test
    @Transactional
    void patchNonExistingGlAccountCategory() throws Exception {
        int databaseSizeBeforeUpdate = glAccountCategoryRepository.findAll().size();
        glAccountCategory.setId(count.incrementAndGet());

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restGlAccountCategoryMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, glAccountCategory.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(glAccountCategory))
            )
            .andExpect(status().isBadRequest());

        // Validate the GlAccountCategory in the database
        List<GlAccountCategory> glAccountCategoryList = glAccountCategoryRepository.findAll();
        assertThat(glAccountCategoryList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithIdMismatchGlAccountCategory() throws Exception {
        int databaseSizeBeforeUpdate = glAccountCategoryRepository.findAll().size();
        glAccountCategory.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restGlAccountCategoryMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(glAccountCategory))
            )
            .andExpect(status().isBadRequest());

        // Validate the GlAccountCategory in the database
        List<GlAccountCategory> glAccountCategoryList = glAccountCategoryRepository.findAll();
        assertThat(glAccountCategoryList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithMissingIdPathParamGlAccountCategory() throws Exception {
        int databaseSizeBeforeUpdate = glAccountCategoryRepository.findAll().size();
        glAccountCategory.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restGlAccountCategoryMockMvc
            .perform(
                patch(ENTITY_API_URL)
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(glAccountCategory))
            )
            .andExpect(status().isMethodNotAllowed());

        // Validate the GlAccountCategory in the database
        List<GlAccountCategory> glAccountCategoryList = glAccountCategoryRepository.findAll();
        assertThat(glAccountCategoryList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void deleteGlAccountCategory() throws Exception {
        // Initialize the database
        glAccountCategoryRepository.saveAndFlush(glAccountCategory);

        int databaseSizeBeforeDelete = glAccountCategoryRepository.findAll().size();

        // Delete the glAccountCategory
        restGlAccountCategoryMockMvc
            .perform(delete(ENTITY_API_URL_ID, glAccountCategory.getId()).accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<GlAccountCategory> glAccountCategoryList = glAccountCategoryRepository.findAll();
        assertThat(glAccountCategoryList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
