package com.iconnect101.glapp.web.rest;

import static com.iconnect101.glapp.web.rest.TestUtil.sameInstant;
import static com.iconnect101.glapp.web.rest.TestUtil.sameNumber;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import com.iconnect101.glapp.IntegrationTest;
import com.iconnect101.glapp.domain.WalletTransaction;
import com.iconnect101.glapp.domain.enumeration.TxnStatus;
import com.iconnect101.glapp.repository.WalletTransactionRepository;
import java.math.BigDecimal;
import java.time.Instant;
import java.time.LocalDate;
import java.time.ZoneId;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.time.temporal.ChronoUnit;
import java.util.List;
import java.util.Random;
import java.util.concurrent.atomic.AtomicLong;
import javax.persistence.EntityManager;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;

/**
 * Integration tests for the {@link WalletTransactionResource} REST controller.
 */
@IntegrationTest
@AutoConfigureMockMvc
@WithMockUser
class WalletTransactionResourceIT {

    private static final String DEFAULT_TXN_REF_NO = "AAAAAAAAAA";
    private static final String UPDATED_TXN_REF_NO = "BBBBBBBBBB";

    private static final String DEFAULT_ACCOUNT_NO = "AAAAAAAAAA";
    private static final String UPDATED_ACCOUNT_NO = "BBBBBBBBBB";

    private static final LocalDate DEFAULT_TXN_DATE = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_TXN_DATE = LocalDate.now(ZoneId.systemDefault());

    private static final Instant DEFAULT_TXN_DATE_IN_UTC = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_TXN_DATE_IN_UTC = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    private static final ZonedDateTime DEFAULT_TXN_DATE_IN_LOCAL = ZonedDateTime.ofInstant(Instant.ofEpochMilli(0L), ZoneOffset.UTC);
    private static final ZonedDateTime UPDATED_TXN_DATE_IN_LOCAL = ZonedDateTime.now(ZoneId.systemDefault()).withNano(0);

    private static final ZonedDateTime DEFAULT_TXN_DATE_FOR_SETTLEMENT = ZonedDateTime.ofInstant(Instant.ofEpochMilli(0L), ZoneOffset.UTC);
    private static final ZonedDateTime UPDATED_TXN_DATE_FOR_SETTLEMENT = ZonedDateTime.now(ZoneId.systemDefault()).withNano(0);

    private static final LocalDate DEFAULT_VALUE_DATE = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_VALUE_DATE = LocalDate.now(ZoneId.systemDefault());

    private static final Instant DEFAULT_VALUE_DATE_IN_UTC = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_VALUE_DATE_IN_UTC = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    private static final ZonedDateTime DEFAULT_VALUE_DATE_IN_LOCAL = ZonedDateTime.ofInstant(Instant.ofEpochMilli(0L), ZoneOffset.UTC);
    private static final ZonedDateTime UPDATED_VALUE_DATE_IN_LOCAL = ZonedDateTime.now(ZoneId.systemDefault()).withNano(0);

    private static final ZonedDateTime DEFAULT_VALUE_DATE_FOR_SETTLEMENT = ZonedDateTime.ofInstant(
        Instant.ofEpochMilli(0L),
        ZoneOffset.UTC
    );
    private static final ZonedDateTime UPDATED_VALUE_DATE_FOR_SETTLEMENT = ZonedDateTime.now(ZoneId.systemDefault()).withNano(0);

    private static final BigDecimal DEFAULT_TXN_AMOUNT = new BigDecimal(1);
    private static final BigDecimal UPDATED_TXN_AMOUNT = new BigDecimal(2);

    private static final BigDecimal DEFAULT_INSTRUCTED_AMOUNT = new BigDecimal(1);
    private static final BigDecimal UPDATED_INSTRUCTED_AMOUNT = new BigDecimal(2);

    private static final BigDecimal DEFAULT_AMOUNT_IN_ACCOUNT_CURRENCY = new BigDecimal(1);
    private static final BigDecimal UPDATED_AMOUNT_IN_ACCOUNT_CURRENCY = new BigDecimal(2);

    private static final BigDecimal DEFAULT_AMOUNT_IN_LOCAL_CURRENCY = new BigDecimal(1);
    private static final BigDecimal UPDATED_AMOUNT_IN_LOCAL_CURRENCY = new BigDecimal(2);

    private static final BigDecimal DEFAULT_CHARGE_IN_ACCOUNT_CURRENCY = new BigDecimal(1);
    private static final BigDecimal UPDATED_CHARGE_IN_ACCOUNT_CURRENCY = new BigDecimal(2);

    private static final BigDecimal DEFAULT_CHARGE_IN_LOCAL_CURRENCY = new BigDecimal(1);
    private static final BigDecimal UPDATED_CHARGE_IN_LOCAL_CURRENCY = new BigDecimal(2);

    private static final String DEFAULT_FUNDING_SOURCE = "AAAAAAAAAA";
    private static final String UPDATED_FUNDING_SOURCE = "BBBBBBBBBB";

    private static final String DEFAULT_MERCHANT_ID = "AAAAAAAAAA";
    private static final String UPDATED_MERCHANT_ID = "BBBBBBBBBB";

    private static final String DEFAULT_MERCHANT_INFO = "AAAAAAAAAA";
    private static final String UPDATED_MERCHANT_INFO = "BBBBBBBBBB";

    private static final String DEFAULT_EXTERNAL_REF_NO = "AAAAAAAAAA";
    private static final String UPDATED_EXTERNAL_REF_NO = "BBBBBBBBBB";

    private static final String DEFAULT_TXN_DESCRIPTION = "AAAAAAAAAA";
    private static final String UPDATED_TXN_DESCRIPTION = "BBBBBBBBBB";

    private static final String DEFAULT_TXN_INTERNAL_DESCRIPTION = "AAAAAAAAAA";
    private static final String UPDATED_TXN_INTERNAL_DESCRIPTION = "BBBBBBBBBB";

    private static final Instant DEFAULT_CREATED_ON = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_CREATED_ON = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    private static final String DEFAULT_CREATED_BY = "AAAAAAAAAA";
    private static final String UPDATED_CREATED_BY = "BBBBBBBBBB";

    private static final TxnStatus DEFAULT_TXN_STATUS = TxnStatus.ACTIVE;
    private static final TxnStatus UPDATED_TXN_STATUS = TxnStatus.CANCELLED;

    private static final String DEFAULT_TXN_DEBIT_ACCOUNT_NO = "AAAAAAAAAA";
    private static final String UPDATED_TXN_DEBIT_ACCOUNT_NO = "BBBBBBBBBB";

    private static final String DEFAULT_TXN_CREDIT_ACCOUNT_NO = "AAAAAAAAAA";
    private static final String UPDATED_TXN_CREDIT_ACCOUNT_NO = "BBBBBBBBBB";

    private static final String DEFAULT_CHARGE_DEBIT_ACCOUNT_NO = "AAAAAAAAAA";
    private static final String UPDATED_CHARGE_DEBIT_ACCOUNT_NO = "BBBBBBBBBB";

    private static final String DEFAULT_CHARGE_CREDIT_ACCOUNT_NO = "AAAAAAAAAA";
    private static final String UPDATED_CHARGE_CREDIT_ACCOUNT_NO = "BBBBBBBBBB";

    private static final String ENTITY_API_URL = "/api/wallet-transactions";
    private static final String ENTITY_API_URL_ID = ENTITY_API_URL + "/{id}";

    private static Random random = new Random();
    private static AtomicLong count = new AtomicLong(random.nextInt() + (2 * Integer.MAX_VALUE));

    @Autowired
    private WalletTransactionRepository walletTransactionRepository;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restWalletTransactionMockMvc;

    private WalletTransaction walletTransaction;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static WalletTransaction createEntity(EntityManager em) {
        WalletTransaction walletTransaction = new WalletTransaction()
            .txnRefNo(DEFAULT_TXN_REF_NO)
            .accountNo(DEFAULT_ACCOUNT_NO)
            .txnDate(DEFAULT_TXN_DATE)
            .txnDateInUTC(DEFAULT_TXN_DATE_IN_UTC)
            .txnDateInLocal(DEFAULT_TXN_DATE_IN_LOCAL)
            .txnDateForSettlement(DEFAULT_TXN_DATE_FOR_SETTLEMENT)
            .valueDate(DEFAULT_VALUE_DATE)
            .valueDateInUTC(DEFAULT_VALUE_DATE_IN_UTC)
            .valueDateInLocal(DEFAULT_VALUE_DATE_IN_LOCAL)
            .valueDateForSettlement(DEFAULT_VALUE_DATE_FOR_SETTLEMENT)
            .txnAmount(DEFAULT_TXN_AMOUNT)
            .instructedAmount(DEFAULT_INSTRUCTED_AMOUNT)
            .amountInAccountCurrency(DEFAULT_AMOUNT_IN_ACCOUNT_CURRENCY)
            .amountInLocalCurrency(DEFAULT_AMOUNT_IN_LOCAL_CURRENCY)
            .chargeInAccountCurrency(DEFAULT_CHARGE_IN_ACCOUNT_CURRENCY)
            .chargeInLocalCurrency(DEFAULT_CHARGE_IN_LOCAL_CURRENCY)
            .fundingSource(DEFAULT_FUNDING_SOURCE)
            .merchantID(DEFAULT_MERCHANT_ID)
            .merchantInfo(DEFAULT_MERCHANT_INFO)
            .externalRefNo(DEFAULT_EXTERNAL_REF_NO)
            .txnDescription(DEFAULT_TXN_DESCRIPTION)
            .txnInternalDescription(DEFAULT_TXN_INTERNAL_DESCRIPTION)
            .createdOn(DEFAULT_CREATED_ON)
            .createdBy(DEFAULT_CREATED_BY)
            .txnStatus(DEFAULT_TXN_STATUS)
            .txnDebitAccountNo(DEFAULT_TXN_DEBIT_ACCOUNT_NO)
            .txnCreditAccountNo(DEFAULT_TXN_CREDIT_ACCOUNT_NO)
            .chargeDebitAccountNo(DEFAULT_CHARGE_DEBIT_ACCOUNT_NO)
            .chargeCreditAccountNo(DEFAULT_CHARGE_CREDIT_ACCOUNT_NO);
        return walletTransaction;
    }

    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static WalletTransaction createUpdatedEntity(EntityManager em) {
        WalletTransaction walletTransaction = new WalletTransaction()
            .txnRefNo(UPDATED_TXN_REF_NO)
            .accountNo(UPDATED_ACCOUNT_NO)
            .txnDate(UPDATED_TXN_DATE)
            .txnDateInUTC(UPDATED_TXN_DATE_IN_UTC)
            .txnDateInLocal(UPDATED_TXN_DATE_IN_LOCAL)
            .txnDateForSettlement(UPDATED_TXN_DATE_FOR_SETTLEMENT)
            .valueDate(UPDATED_VALUE_DATE)
            .valueDateInUTC(UPDATED_VALUE_DATE_IN_UTC)
            .valueDateInLocal(UPDATED_VALUE_DATE_IN_LOCAL)
            .valueDateForSettlement(UPDATED_VALUE_DATE_FOR_SETTLEMENT)
            .txnAmount(UPDATED_TXN_AMOUNT)
            .instructedAmount(UPDATED_INSTRUCTED_AMOUNT)
            .amountInAccountCurrency(UPDATED_AMOUNT_IN_ACCOUNT_CURRENCY)
            .amountInLocalCurrency(UPDATED_AMOUNT_IN_LOCAL_CURRENCY)
            .chargeInAccountCurrency(UPDATED_CHARGE_IN_ACCOUNT_CURRENCY)
            .chargeInLocalCurrency(UPDATED_CHARGE_IN_LOCAL_CURRENCY)
            .fundingSource(UPDATED_FUNDING_SOURCE)
            .merchantID(UPDATED_MERCHANT_ID)
            .merchantInfo(UPDATED_MERCHANT_INFO)
            .externalRefNo(UPDATED_EXTERNAL_REF_NO)
            .txnDescription(UPDATED_TXN_DESCRIPTION)
            .txnInternalDescription(UPDATED_TXN_INTERNAL_DESCRIPTION)
            .createdOn(UPDATED_CREATED_ON)
            .createdBy(UPDATED_CREATED_BY)
            .txnStatus(UPDATED_TXN_STATUS)
            .txnDebitAccountNo(UPDATED_TXN_DEBIT_ACCOUNT_NO)
            .txnCreditAccountNo(UPDATED_TXN_CREDIT_ACCOUNT_NO)
            .chargeDebitAccountNo(UPDATED_CHARGE_DEBIT_ACCOUNT_NO)
            .chargeCreditAccountNo(UPDATED_CHARGE_CREDIT_ACCOUNT_NO);
        return walletTransaction;
    }

    @BeforeEach
    public void initTest() {
        walletTransaction = createEntity(em);
    }

    @Test
    @Transactional
    void createWalletTransaction() throws Exception {
        int databaseSizeBeforeCreate = walletTransactionRepository.findAll().size();
        // Create the WalletTransaction
        restWalletTransactionMockMvc
            .perform(
                post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(walletTransaction))
            )
            .andExpect(status().isCreated());

        // Validate the WalletTransaction in the database
        List<WalletTransaction> walletTransactionList = walletTransactionRepository.findAll();
        assertThat(walletTransactionList).hasSize(databaseSizeBeforeCreate + 1);
        WalletTransaction testWalletTransaction = walletTransactionList.get(walletTransactionList.size() - 1);
        assertThat(testWalletTransaction.getTxnRefNo()).isEqualTo(DEFAULT_TXN_REF_NO);
        assertThat(testWalletTransaction.getAccountNo()).isEqualTo(DEFAULT_ACCOUNT_NO);
        assertThat(testWalletTransaction.getTxnDate()).isEqualTo(DEFAULT_TXN_DATE);
        assertThat(testWalletTransaction.getTxnDateInUTC()).isEqualTo(DEFAULT_TXN_DATE_IN_UTC);
        assertThat(testWalletTransaction.getTxnDateInLocal()).isEqualTo(DEFAULT_TXN_DATE_IN_LOCAL);
        assertThat(testWalletTransaction.getTxnDateForSettlement()).isEqualTo(DEFAULT_TXN_DATE_FOR_SETTLEMENT);
        assertThat(testWalletTransaction.getValueDate()).isEqualTo(DEFAULT_VALUE_DATE);
        assertThat(testWalletTransaction.getValueDateInUTC()).isEqualTo(DEFAULT_VALUE_DATE_IN_UTC);
        assertThat(testWalletTransaction.getValueDateInLocal()).isEqualTo(DEFAULT_VALUE_DATE_IN_LOCAL);
        assertThat(testWalletTransaction.getValueDateForSettlement()).isEqualTo(DEFAULT_VALUE_DATE_FOR_SETTLEMENT);
        assertThat(testWalletTransaction.getTxnAmount()).isEqualByComparingTo(DEFAULT_TXN_AMOUNT);
        assertThat(testWalletTransaction.getInstructedAmount()).isEqualByComparingTo(DEFAULT_INSTRUCTED_AMOUNT);
        assertThat(testWalletTransaction.getAmountInAccountCurrency()).isEqualByComparingTo(DEFAULT_AMOUNT_IN_ACCOUNT_CURRENCY);
        assertThat(testWalletTransaction.getAmountInLocalCurrency()).isEqualByComparingTo(DEFAULT_AMOUNT_IN_LOCAL_CURRENCY);
        assertThat(testWalletTransaction.getChargeInAccountCurrency()).isEqualByComparingTo(DEFAULT_CHARGE_IN_ACCOUNT_CURRENCY);
        assertThat(testWalletTransaction.getChargeInLocalCurrency()).isEqualByComparingTo(DEFAULT_CHARGE_IN_LOCAL_CURRENCY);
        assertThat(testWalletTransaction.getFundingSource()).isEqualTo(DEFAULT_FUNDING_SOURCE);
        assertThat(testWalletTransaction.getMerchantID()).isEqualTo(DEFAULT_MERCHANT_ID);
        assertThat(testWalletTransaction.getMerchantInfo()).isEqualTo(DEFAULT_MERCHANT_INFO);
        assertThat(testWalletTransaction.getExternalRefNo()).isEqualTo(DEFAULT_EXTERNAL_REF_NO);
        assertThat(testWalletTransaction.getTxnDescription()).isEqualTo(DEFAULT_TXN_DESCRIPTION);
        assertThat(testWalletTransaction.getTxnInternalDescription()).isEqualTo(DEFAULT_TXN_INTERNAL_DESCRIPTION);
        assertThat(testWalletTransaction.getCreatedOn()).isEqualTo(DEFAULT_CREATED_ON);
        assertThat(testWalletTransaction.getCreatedBy()).isEqualTo(DEFAULT_CREATED_BY);
        assertThat(testWalletTransaction.getTxnStatus()).isEqualTo(DEFAULT_TXN_STATUS);
        assertThat(testWalletTransaction.getTxnDebitAccountNo()).isEqualTo(DEFAULT_TXN_DEBIT_ACCOUNT_NO);
        assertThat(testWalletTransaction.getTxnCreditAccountNo()).isEqualTo(DEFAULT_TXN_CREDIT_ACCOUNT_NO);
        assertThat(testWalletTransaction.getChargeDebitAccountNo()).isEqualTo(DEFAULT_CHARGE_DEBIT_ACCOUNT_NO);
        assertThat(testWalletTransaction.getChargeCreditAccountNo()).isEqualTo(DEFAULT_CHARGE_CREDIT_ACCOUNT_NO);
    }

    @Test
    @Transactional
    void createWalletTransactionWithExistingId() throws Exception {
        // Create the WalletTransaction with an existing ID
        walletTransaction.setId(1L);

        int databaseSizeBeforeCreate = walletTransactionRepository.findAll().size();

        // An entity with an existing ID cannot be created, so this API call must fail
        restWalletTransactionMockMvc
            .perform(
                post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(walletTransaction))
            )
            .andExpect(status().isBadRequest());

        // Validate the WalletTransaction in the database
        List<WalletTransaction> walletTransactionList = walletTransactionRepository.findAll();
        assertThat(walletTransactionList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    void getAllWalletTransactions() throws Exception {
        // Initialize the database
        walletTransactionRepository.saveAndFlush(walletTransaction);

        // Get all the walletTransactionList
        restWalletTransactionMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(walletTransaction.getId().intValue())))
            .andExpect(jsonPath("$.[*].txnRefNo").value(hasItem(DEFAULT_TXN_REF_NO)))
            .andExpect(jsonPath("$.[*].accountNo").value(hasItem(DEFAULT_ACCOUNT_NO)))
            .andExpect(jsonPath("$.[*].txnDate").value(hasItem(DEFAULT_TXN_DATE.toString())))
            .andExpect(jsonPath("$.[*].txnDateInUTC").value(hasItem(DEFAULT_TXN_DATE_IN_UTC.toString())))
            .andExpect(jsonPath("$.[*].txnDateInLocal").value(hasItem(sameInstant(DEFAULT_TXN_DATE_IN_LOCAL))))
            .andExpect(jsonPath("$.[*].txnDateForSettlement").value(hasItem(sameInstant(DEFAULT_TXN_DATE_FOR_SETTLEMENT))))
            .andExpect(jsonPath("$.[*].valueDate").value(hasItem(DEFAULT_VALUE_DATE.toString())))
            .andExpect(jsonPath("$.[*].valueDateInUTC").value(hasItem(DEFAULT_VALUE_DATE_IN_UTC.toString())))
            .andExpect(jsonPath("$.[*].valueDateInLocal").value(hasItem(sameInstant(DEFAULT_VALUE_DATE_IN_LOCAL))))
            .andExpect(jsonPath("$.[*].valueDateForSettlement").value(hasItem(sameInstant(DEFAULT_VALUE_DATE_FOR_SETTLEMENT))))
            .andExpect(jsonPath("$.[*].txnAmount").value(hasItem(sameNumber(DEFAULT_TXN_AMOUNT))))
            .andExpect(jsonPath("$.[*].instructedAmount").value(hasItem(sameNumber(DEFAULT_INSTRUCTED_AMOUNT))))
            .andExpect(jsonPath("$.[*].amountInAccountCurrency").value(hasItem(sameNumber(DEFAULT_AMOUNT_IN_ACCOUNT_CURRENCY))))
            .andExpect(jsonPath("$.[*].amountInLocalCurrency").value(hasItem(sameNumber(DEFAULT_AMOUNT_IN_LOCAL_CURRENCY))))
            .andExpect(jsonPath("$.[*].chargeInAccountCurrency").value(hasItem(sameNumber(DEFAULT_CHARGE_IN_ACCOUNT_CURRENCY))))
            .andExpect(jsonPath("$.[*].chargeInLocalCurrency").value(hasItem(sameNumber(DEFAULT_CHARGE_IN_LOCAL_CURRENCY))))
            .andExpect(jsonPath("$.[*].fundingSource").value(hasItem(DEFAULT_FUNDING_SOURCE)))
            .andExpect(jsonPath("$.[*].merchantID").value(hasItem(DEFAULT_MERCHANT_ID)))
            .andExpect(jsonPath("$.[*].merchantInfo").value(hasItem(DEFAULT_MERCHANT_INFO)))
            .andExpect(jsonPath("$.[*].externalRefNo").value(hasItem(DEFAULT_EXTERNAL_REF_NO)))
            .andExpect(jsonPath("$.[*].txnDescription").value(hasItem(DEFAULT_TXN_DESCRIPTION)))
            .andExpect(jsonPath("$.[*].txnInternalDescription").value(hasItem(DEFAULT_TXN_INTERNAL_DESCRIPTION)))
            .andExpect(jsonPath("$.[*].createdOn").value(hasItem(DEFAULT_CREATED_ON.toString())))
            .andExpect(jsonPath("$.[*].createdBy").value(hasItem(DEFAULT_CREATED_BY)))
            .andExpect(jsonPath("$.[*].txnStatus").value(hasItem(DEFAULT_TXN_STATUS.toString())))
            .andExpect(jsonPath("$.[*].txnDebitAccountNo").value(hasItem(DEFAULT_TXN_DEBIT_ACCOUNT_NO)))
            .andExpect(jsonPath("$.[*].txnCreditAccountNo").value(hasItem(DEFAULT_TXN_CREDIT_ACCOUNT_NO)))
            .andExpect(jsonPath("$.[*].chargeDebitAccountNo").value(hasItem(DEFAULT_CHARGE_DEBIT_ACCOUNT_NO)))
            .andExpect(jsonPath("$.[*].chargeCreditAccountNo").value(hasItem(DEFAULT_CHARGE_CREDIT_ACCOUNT_NO)));
    }

    @Test
    @Transactional
    void getWalletTransaction() throws Exception {
        // Initialize the database
        walletTransactionRepository.saveAndFlush(walletTransaction);

        // Get the walletTransaction
        restWalletTransactionMockMvc
            .perform(get(ENTITY_API_URL_ID, walletTransaction.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(walletTransaction.getId().intValue()))
            .andExpect(jsonPath("$.txnRefNo").value(DEFAULT_TXN_REF_NO))
            .andExpect(jsonPath("$.accountNo").value(DEFAULT_ACCOUNT_NO))
            .andExpect(jsonPath("$.txnDate").value(DEFAULT_TXN_DATE.toString()))
            .andExpect(jsonPath("$.txnDateInUTC").value(DEFAULT_TXN_DATE_IN_UTC.toString()))
            .andExpect(jsonPath("$.txnDateInLocal").value(sameInstant(DEFAULT_TXN_DATE_IN_LOCAL)))
            .andExpect(jsonPath("$.txnDateForSettlement").value(sameInstant(DEFAULT_TXN_DATE_FOR_SETTLEMENT)))
            .andExpect(jsonPath("$.valueDate").value(DEFAULT_VALUE_DATE.toString()))
            .andExpect(jsonPath("$.valueDateInUTC").value(DEFAULT_VALUE_DATE_IN_UTC.toString()))
            .andExpect(jsonPath("$.valueDateInLocal").value(sameInstant(DEFAULT_VALUE_DATE_IN_LOCAL)))
            .andExpect(jsonPath("$.valueDateForSettlement").value(sameInstant(DEFAULT_VALUE_DATE_FOR_SETTLEMENT)))
            .andExpect(jsonPath("$.txnAmount").value(sameNumber(DEFAULT_TXN_AMOUNT)))
            .andExpect(jsonPath("$.instructedAmount").value(sameNumber(DEFAULT_INSTRUCTED_AMOUNT)))
            .andExpect(jsonPath("$.amountInAccountCurrency").value(sameNumber(DEFAULT_AMOUNT_IN_ACCOUNT_CURRENCY)))
            .andExpect(jsonPath("$.amountInLocalCurrency").value(sameNumber(DEFAULT_AMOUNT_IN_LOCAL_CURRENCY)))
            .andExpect(jsonPath("$.chargeInAccountCurrency").value(sameNumber(DEFAULT_CHARGE_IN_ACCOUNT_CURRENCY)))
            .andExpect(jsonPath("$.chargeInLocalCurrency").value(sameNumber(DEFAULT_CHARGE_IN_LOCAL_CURRENCY)))
            .andExpect(jsonPath("$.fundingSource").value(DEFAULT_FUNDING_SOURCE))
            .andExpect(jsonPath("$.merchantID").value(DEFAULT_MERCHANT_ID))
            .andExpect(jsonPath("$.merchantInfo").value(DEFAULT_MERCHANT_INFO))
            .andExpect(jsonPath("$.externalRefNo").value(DEFAULT_EXTERNAL_REF_NO))
            .andExpect(jsonPath("$.txnDescription").value(DEFAULT_TXN_DESCRIPTION))
            .andExpect(jsonPath("$.txnInternalDescription").value(DEFAULT_TXN_INTERNAL_DESCRIPTION))
            .andExpect(jsonPath("$.createdOn").value(DEFAULT_CREATED_ON.toString()))
            .andExpect(jsonPath("$.createdBy").value(DEFAULT_CREATED_BY))
            .andExpect(jsonPath("$.txnStatus").value(DEFAULT_TXN_STATUS.toString()))
            .andExpect(jsonPath("$.txnDebitAccountNo").value(DEFAULT_TXN_DEBIT_ACCOUNT_NO))
            .andExpect(jsonPath("$.txnCreditAccountNo").value(DEFAULT_TXN_CREDIT_ACCOUNT_NO))
            .andExpect(jsonPath("$.chargeDebitAccountNo").value(DEFAULT_CHARGE_DEBIT_ACCOUNT_NO))
            .andExpect(jsonPath("$.chargeCreditAccountNo").value(DEFAULT_CHARGE_CREDIT_ACCOUNT_NO));
    }

    @Test
    @Transactional
    void getNonExistingWalletTransaction() throws Exception {
        // Get the walletTransaction
        restWalletTransactionMockMvc.perform(get(ENTITY_API_URL_ID, Long.MAX_VALUE)).andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    void putNewWalletTransaction() throws Exception {
        // Initialize the database
        walletTransactionRepository.saveAndFlush(walletTransaction);

        int databaseSizeBeforeUpdate = walletTransactionRepository.findAll().size();

        // Update the walletTransaction
        WalletTransaction updatedWalletTransaction = walletTransactionRepository.findById(walletTransaction.getId()).get();
        // Disconnect from session so that the updates on updatedWalletTransaction are not directly saved in db
        em.detach(updatedWalletTransaction);
        updatedWalletTransaction
            .txnRefNo(UPDATED_TXN_REF_NO)
            .accountNo(UPDATED_ACCOUNT_NO)
            .txnDate(UPDATED_TXN_DATE)
            .txnDateInUTC(UPDATED_TXN_DATE_IN_UTC)
            .txnDateInLocal(UPDATED_TXN_DATE_IN_LOCAL)
            .txnDateForSettlement(UPDATED_TXN_DATE_FOR_SETTLEMENT)
            .valueDate(UPDATED_VALUE_DATE)
            .valueDateInUTC(UPDATED_VALUE_DATE_IN_UTC)
            .valueDateInLocal(UPDATED_VALUE_DATE_IN_LOCAL)
            .valueDateForSettlement(UPDATED_VALUE_DATE_FOR_SETTLEMENT)
            .txnAmount(UPDATED_TXN_AMOUNT)
            .instructedAmount(UPDATED_INSTRUCTED_AMOUNT)
            .amountInAccountCurrency(UPDATED_AMOUNT_IN_ACCOUNT_CURRENCY)
            .amountInLocalCurrency(UPDATED_AMOUNT_IN_LOCAL_CURRENCY)
            .chargeInAccountCurrency(UPDATED_CHARGE_IN_ACCOUNT_CURRENCY)
            .chargeInLocalCurrency(UPDATED_CHARGE_IN_LOCAL_CURRENCY)
            .fundingSource(UPDATED_FUNDING_SOURCE)
            .merchantID(UPDATED_MERCHANT_ID)
            .merchantInfo(UPDATED_MERCHANT_INFO)
            .externalRefNo(UPDATED_EXTERNAL_REF_NO)
            .txnDescription(UPDATED_TXN_DESCRIPTION)
            .txnInternalDescription(UPDATED_TXN_INTERNAL_DESCRIPTION)
            .createdOn(UPDATED_CREATED_ON)
            .createdBy(UPDATED_CREATED_BY)
            .txnStatus(UPDATED_TXN_STATUS)
            .txnDebitAccountNo(UPDATED_TXN_DEBIT_ACCOUNT_NO)
            .txnCreditAccountNo(UPDATED_TXN_CREDIT_ACCOUNT_NO)
            .chargeDebitAccountNo(UPDATED_CHARGE_DEBIT_ACCOUNT_NO)
            .chargeCreditAccountNo(UPDATED_CHARGE_CREDIT_ACCOUNT_NO);

        restWalletTransactionMockMvc
            .perform(
                put(ENTITY_API_URL_ID, updatedWalletTransaction.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(updatedWalletTransaction))
            )
            .andExpect(status().isOk());

        // Validate the WalletTransaction in the database
        List<WalletTransaction> walletTransactionList = walletTransactionRepository.findAll();
        assertThat(walletTransactionList).hasSize(databaseSizeBeforeUpdate);
        WalletTransaction testWalletTransaction = walletTransactionList.get(walletTransactionList.size() - 1);
        assertThat(testWalletTransaction.getTxnRefNo()).isEqualTo(UPDATED_TXN_REF_NO);
        assertThat(testWalletTransaction.getAccountNo()).isEqualTo(UPDATED_ACCOUNT_NO);
        assertThat(testWalletTransaction.getTxnDate()).isEqualTo(UPDATED_TXN_DATE);
        assertThat(testWalletTransaction.getTxnDateInUTC()).isEqualTo(UPDATED_TXN_DATE_IN_UTC);
        assertThat(testWalletTransaction.getTxnDateInLocal()).isEqualTo(UPDATED_TXN_DATE_IN_LOCAL);
        assertThat(testWalletTransaction.getTxnDateForSettlement()).isEqualTo(UPDATED_TXN_DATE_FOR_SETTLEMENT);
        assertThat(testWalletTransaction.getValueDate()).isEqualTo(UPDATED_VALUE_DATE);
        assertThat(testWalletTransaction.getValueDateInUTC()).isEqualTo(UPDATED_VALUE_DATE_IN_UTC);
        assertThat(testWalletTransaction.getValueDateInLocal()).isEqualTo(UPDATED_VALUE_DATE_IN_LOCAL);
        assertThat(testWalletTransaction.getValueDateForSettlement()).isEqualTo(UPDATED_VALUE_DATE_FOR_SETTLEMENT);
        assertThat(testWalletTransaction.getTxnAmount()).isEqualTo(UPDATED_TXN_AMOUNT);
        assertThat(testWalletTransaction.getInstructedAmount()).isEqualTo(UPDATED_INSTRUCTED_AMOUNT);
        assertThat(testWalletTransaction.getAmountInAccountCurrency()).isEqualTo(UPDATED_AMOUNT_IN_ACCOUNT_CURRENCY);
        assertThat(testWalletTransaction.getAmountInLocalCurrency()).isEqualTo(UPDATED_AMOUNT_IN_LOCAL_CURRENCY);
        assertThat(testWalletTransaction.getChargeInAccountCurrency()).isEqualTo(UPDATED_CHARGE_IN_ACCOUNT_CURRENCY);
        assertThat(testWalletTransaction.getChargeInLocalCurrency()).isEqualTo(UPDATED_CHARGE_IN_LOCAL_CURRENCY);
        assertThat(testWalletTransaction.getFundingSource()).isEqualTo(UPDATED_FUNDING_SOURCE);
        assertThat(testWalletTransaction.getMerchantID()).isEqualTo(UPDATED_MERCHANT_ID);
        assertThat(testWalletTransaction.getMerchantInfo()).isEqualTo(UPDATED_MERCHANT_INFO);
        assertThat(testWalletTransaction.getExternalRefNo()).isEqualTo(UPDATED_EXTERNAL_REF_NO);
        assertThat(testWalletTransaction.getTxnDescription()).isEqualTo(UPDATED_TXN_DESCRIPTION);
        assertThat(testWalletTransaction.getTxnInternalDescription()).isEqualTo(UPDATED_TXN_INTERNAL_DESCRIPTION);
        assertThat(testWalletTransaction.getCreatedOn()).isEqualTo(UPDATED_CREATED_ON);
        assertThat(testWalletTransaction.getCreatedBy()).isEqualTo(UPDATED_CREATED_BY);
        assertThat(testWalletTransaction.getTxnStatus()).isEqualTo(UPDATED_TXN_STATUS);
        assertThat(testWalletTransaction.getTxnDebitAccountNo()).isEqualTo(UPDATED_TXN_DEBIT_ACCOUNT_NO);
        assertThat(testWalletTransaction.getTxnCreditAccountNo()).isEqualTo(UPDATED_TXN_CREDIT_ACCOUNT_NO);
        assertThat(testWalletTransaction.getChargeDebitAccountNo()).isEqualTo(UPDATED_CHARGE_DEBIT_ACCOUNT_NO);
        assertThat(testWalletTransaction.getChargeCreditAccountNo()).isEqualTo(UPDATED_CHARGE_CREDIT_ACCOUNT_NO);
    }

    @Test
    @Transactional
    void putNonExistingWalletTransaction() throws Exception {
        int databaseSizeBeforeUpdate = walletTransactionRepository.findAll().size();
        walletTransaction.setId(count.incrementAndGet());

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restWalletTransactionMockMvc
            .perform(
                put(ENTITY_API_URL_ID, walletTransaction.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(walletTransaction))
            )
            .andExpect(status().isBadRequest());

        // Validate the WalletTransaction in the database
        List<WalletTransaction> walletTransactionList = walletTransactionRepository.findAll();
        assertThat(walletTransactionList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithIdMismatchWalletTransaction() throws Exception {
        int databaseSizeBeforeUpdate = walletTransactionRepository.findAll().size();
        walletTransaction.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restWalletTransactionMockMvc
            .perform(
                put(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(walletTransaction))
            )
            .andExpect(status().isBadRequest());

        // Validate the WalletTransaction in the database
        List<WalletTransaction> walletTransactionList = walletTransactionRepository.findAll();
        assertThat(walletTransactionList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithMissingIdPathParamWalletTransaction() throws Exception {
        int databaseSizeBeforeUpdate = walletTransactionRepository.findAll().size();
        walletTransaction.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restWalletTransactionMockMvc
            .perform(
                put(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(walletTransaction))
            )
            .andExpect(status().isMethodNotAllowed());

        // Validate the WalletTransaction in the database
        List<WalletTransaction> walletTransactionList = walletTransactionRepository.findAll();
        assertThat(walletTransactionList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void partialUpdateWalletTransactionWithPatch() throws Exception {
        // Initialize the database
        walletTransactionRepository.saveAndFlush(walletTransaction);

        int databaseSizeBeforeUpdate = walletTransactionRepository.findAll().size();

        // Update the walletTransaction using partial update
        WalletTransaction partialUpdatedWalletTransaction = new WalletTransaction();
        partialUpdatedWalletTransaction.setId(walletTransaction.getId());

        partialUpdatedWalletTransaction
            .txnRefNo(UPDATED_TXN_REF_NO)
            .txnDateInUTC(UPDATED_TXN_DATE_IN_UTC)
            .txnDateInLocal(UPDATED_TXN_DATE_IN_LOCAL)
            .valueDate(UPDATED_VALUE_DATE)
            .txnAmount(UPDATED_TXN_AMOUNT)
            .chargeInAccountCurrency(UPDATED_CHARGE_IN_ACCOUNT_CURRENCY)
            .fundingSource(UPDATED_FUNDING_SOURCE)
            .txnDescription(UPDATED_TXN_DESCRIPTION)
            .txnStatus(UPDATED_TXN_STATUS)
            .txnDebitAccountNo(UPDATED_TXN_DEBIT_ACCOUNT_NO);

        restWalletTransactionMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedWalletTransaction.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedWalletTransaction))
            )
            .andExpect(status().isOk());

        // Validate the WalletTransaction in the database
        List<WalletTransaction> walletTransactionList = walletTransactionRepository.findAll();
        assertThat(walletTransactionList).hasSize(databaseSizeBeforeUpdate);
        WalletTransaction testWalletTransaction = walletTransactionList.get(walletTransactionList.size() - 1);
        assertThat(testWalletTransaction.getTxnRefNo()).isEqualTo(UPDATED_TXN_REF_NO);
        assertThat(testWalletTransaction.getAccountNo()).isEqualTo(DEFAULT_ACCOUNT_NO);
        assertThat(testWalletTransaction.getTxnDate()).isEqualTo(DEFAULT_TXN_DATE);
        assertThat(testWalletTransaction.getTxnDateInUTC()).isEqualTo(UPDATED_TXN_DATE_IN_UTC);
        assertThat(testWalletTransaction.getTxnDateInLocal()).isEqualTo(UPDATED_TXN_DATE_IN_LOCAL);
        assertThat(testWalletTransaction.getTxnDateForSettlement()).isEqualTo(DEFAULT_TXN_DATE_FOR_SETTLEMENT);
        assertThat(testWalletTransaction.getValueDate()).isEqualTo(UPDATED_VALUE_DATE);
        assertThat(testWalletTransaction.getValueDateInUTC()).isEqualTo(DEFAULT_VALUE_DATE_IN_UTC);
        assertThat(testWalletTransaction.getValueDateInLocal()).isEqualTo(DEFAULT_VALUE_DATE_IN_LOCAL);
        assertThat(testWalletTransaction.getValueDateForSettlement()).isEqualTo(DEFAULT_VALUE_DATE_FOR_SETTLEMENT);
        assertThat(testWalletTransaction.getTxnAmount()).isEqualByComparingTo(UPDATED_TXN_AMOUNT);
        assertThat(testWalletTransaction.getInstructedAmount()).isEqualByComparingTo(DEFAULT_INSTRUCTED_AMOUNT);
        assertThat(testWalletTransaction.getAmountInAccountCurrency()).isEqualByComparingTo(DEFAULT_AMOUNT_IN_ACCOUNT_CURRENCY);
        assertThat(testWalletTransaction.getAmountInLocalCurrency()).isEqualByComparingTo(DEFAULT_AMOUNT_IN_LOCAL_CURRENCY);
        assertThat(testWalletTransaction.getChargeInAccountCurrency()).isEqualByComparingTo(UPDATED_CHARGE_IN_ACCOUNT_CURRENCY);
        assertThat(testWalletTransaction.getChargeInLocalCurrency()).isEqualByComparingTo(DEFAULT_CHARGE_IN_LOCAL_CURRENCY);
        assertThat(testWalletTransaction.getFundingSource()).isEqualTo(UPDATED_FUNDING_SOURCE);
        assertThat(testWalletTransaction.getMerchantID()).isEqualTo(DEFAULT_MERCHANT_ID);
        assertThat(testWalletTransaction.getMerchantInfo()).isEqualTo(DEFAULT_MERCHANT_INFO);
        assertThat(testWalletTransaction.getExternalRefNo()).isEqualTo(DEFAULT_EXTERNAL_REF_NO);
        assertThat(testWalletTransaction.getTxnDescription()).isEqualTo(UPDATED_TXN_DESCRIPTION);
        assertThat(testWalletTransaction.getTxnInternalDescription()).isEqualTo(DEFAULT_TXN_INTERNAL_DESCRIPTION);
        assertThat(testWalletTransaction.getCreatedOn()).isEqualTo(DEFAULT_CREATED_ON);
        assertThat(testWalletTransaction.getCreatedBy()).isEqualTo(DEFAULT_CREATED_BY);
        assertThat(testWalletTransaction.getTxnStatus()).isEqualTo(UPDATED_TXN_STATUS);
        assertThat(testWalletTransaction.getTxnDebitAccountNo()).isEqualTo(UPDATED_TXN_DEBIT_ACCOUNT_NO);
        assertThat(testWalletTransaction.getTxnCreditAccountNo()).isEqualTo(DEFAULT_TXN_CREDIT_ACCOUNT_NO);
        assertThat(testWalletTransaction.getChargeDebitAccountNo()).isEqualTo(DEFAULT_CHARGE_DEBIT_ACCOUNT_NO);
        assertThat(testWalletTransaction.getChargeCreditAccountNo()).isEqualTo(DEFAULT_CHARGE_CREDIT_ACCOUNT_NO);
    }

    @Test
    @Transactional
    void fullUpdateWalletTransactionWithPatch() throws Exception {
        // Initialize the database
        walletTransactionRepository.saveAndFlush(walletTransaction);

        int databaseSizeBeforeUpdate = walletTransactionRepository.findAll().size();

        // Update the walletTransaction using partial update
        WalletTransaction partialUpdatedWalletTransaction = new WalletTransaction();
        partialUpdatedWalletTransaction.setId(walletTransaction.getId());

        partialUpdatedWalletTransaction
            .txnRefNo(UPDATED_TXN_REF_NO)
            .accountNo(UPDATED_ACCOUNT_NO)
            .txnDate(UPDATED_TXN_DATE)
            .txnDateInUTC(UPDATED_TXN_DATE_IN_UTC)
            .txnDateInLocal(UPDATED_TXN_DATE_IN_LOCAL)
            .txnDateForSettlement(UPDATED_TXN_DATE_FOR_SETTLEMENT)
            .valueDate(UPDATED_VALUE_DATE)
            .valueDateInUTC(UPDATED_VALUE_DATE_IN_UTC)
            .valueDateInLocal(UPDATED_VALUE_DATE_IN_LOCAL)
            .valueDateForSettlement(UPDATED_VALUE_DATE_FOR_SETTLEMENT)
            .txnAmount(UPDATED_TXN_AMOUNT)
            .instructedAmount(UPDATED_INSTRUCTED_AMOUNT)
            .amountInAccountCurrency(UPDATED_AMOUNT_IN_ACCOUNT_CURRENCY)
            .amountInLocalCurrency(UPDATED_AMOUNT_IN_LOCAL_CURRENCY)
            .chargeInAccountCurrency(UPDATED_CHARGE_IN_ACCOUNT_CURRENCY)
            .chargeInLocalCurrency(UPDATED_CHARGE_IN_LOCAL_CURRENCY)
            .fundingSource(UPDATED_FUNDING_SOURCE)
            .merchantID(UPDATED_MERCHANT_ID)
            .merchantInfo(UPDATED_MERCHANT_INFO)
            .externalRefNo(UPDATED_EXTERNAL_REF_NO)
            .txnDescription(UPDATED_TXN_DESCRIPTION)
            .txnInternalDescription(UPDATED_TXN_INTERNAL_DESCRIPTION)
            .createdOn(UPDATED_CREATED_ON)
            .createdBy(UPDATED_CREATED_BY)
            .txnStatus(UPDATED_TXN_STATUS)
            .txnDebitAccountNo(UPDATED_TXN_DEBIT_ACCOUNT_NO)
            .txnCreditAccountNo(UPDATED_TXN_CREDIT_ACCOUNT_NO)
            .chargeDebitAccountNo(UPDATED_CHARGE_DEBIT_ACCOUNT_NO)
            .chargeCreditAccountNo(UPDATED_CHARGE_CREDIT_ACCOUNT_NO);

        restWalletTransactionMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedWalletTransaction.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedWalletTransaction))
            )
            .andExpect(status().isOk());

        // Validate the WalletTransaction in the database
        List<WalletTransaction> walletTransactionList = walletTransactionRepository.findAll();
        assertThat(walletTransactionList).hasSize(databaseSizeBeforeUpdate);
        WalletTransaction testWalletTransaction = walletTransactionList.get(walletTransactionList.size() - 1);
        assertThat(testWalletTransaction.getTxnRefNo()).isEqualTo(UPDATED_TXN_REF_NO);
        assertThat(testWalletTransaction.getAccountNo()).isEqualTo(UPDATED_ACCOUNT_NO);
        assertThat(testWalletTransaction.getTxnDate()).isEqualTo(UPDATED_TXN_DATE);
        assertThat(testWalletTransaction.getTxnDateInUTC()).isEqualTo(UPDATED_TXN_DATE_IN_UTC);
        assertThat(testWalletTransaction.getTxnDateInLocal()).isEqualTo(UPDATED_TXN_DATE_IN_LOCAL);
        assertThat(testWalletTransaction.getTxnDateForSettlement()).isEqualTo(UPDATED_TXN_DATE_FOR_SETTLEMENT);
        assertThat(testWalletTransaction.getValueDate()).isEqualTo(UPDATED_VALUE_DATE);
        assertThat(testWalletTransaction.getValueDateInUTC()).isEqualTo(UPDATED_VALUE_DATE_IN_UTC);
        assertThat(testWalletTransaction.getValueDateInLocal()).isEqualTo(UPDATED_VALUE_DATE_IN_LOCAL);
        assertThat(testWalletTransaction.getValueDateForSettlement()).isEqualTo(UPDATED_VALUE_DATE_FOR_SETTLEMENT);
        assertThat(testWalletTransaction.getTxnAmount()).isEqualByComparingTo(UPDATED_TXN_AMOUNT);
        assertThat(testWalletTransaction.getInstructedAmount()).isEqualByComparingTo(UPDATED_INSTRUCTED_AMOUNT);
        assertThat(testWalletTransaction.getAmountInAccountCurrency()).isEqualByComparingTo(UPDATED_AMOUNT_IN_ACCOUNT_CURRENCY);
        assertThat(testWalletTransaction.getAmountInLocalCurrency()).isEqualByComparingTo(UPDATED_AMOUNT_IN_LOCAL_CURRENCY);
        assertThat(testWalletTransaction.getChargeInAccountCurrency()).isEqualByComparingTo(UPDATED_CHARGE_IN_ACCOUNT_CURRENCY);
        assertThat(testWalletTransaction.getChargeInLocalCurrency()).isEqualByComparingTo(UPDATED_CHARGE_IN_LOCAL_CURRENCY);
        assertThat(testWalletTransaction.getFundingSource()).isEqualTo(UPDATED_FUNDING_SOURCE);
        assertThat(testWalletTransaction.getMerchantID()).isEqualTo(UPDATED_MERCHANT_ID);
        assertThat(testWalletTransaction.getMerchantInfo()).isEqualTo(UPDATED_MERCHANT_INFO);
        assertThat(testWalletTransaction.getExternalRefNo()).isEqualTo(UPDATED_EXTERNAL_REF_NO);
        assertThat(testWalletTransaction.getTxnDescription()).isEqualTo(UPDATED_TXN_DESCRIPTION);
        assertThat(testWalletTransaction.getTxnInternalDescription()).isEqualTo(UPDATED_TXN_INTERNAL_DESCRIPTION);
        assertThat(testWalletTransaction.getCreatedOn()).isEqualTo(UPDATED_CREATED_ON);
        assertThat(testWalletTransaction.getCreatedBy()).isEqualTo(UPDATED_CREATED_BY);
        assertThat(testWalletTransaction.getTxnStatus()).isEqualTo(UPDATED_TXN_STATUS);
        assertThat(testWalletTransaction.getTxnDebitAccountNo()).isEqualTo(UPDATED_TXN_DEBIT_ACCOUNT_NO);
        assertThat(testWalletTransaction.getTxnCreditAccountNo()).isEqualTo(UPDATED_TXN_CREDIT_ACCOUNT_NO);
        assertThat(testWalletTransaction.getChargeDebitAccountNo()).isEqualTo(UPDATED_CHARGE_DEBIT_ACCOUNT_NO);
        assertThat(testWalletTransaction.getChargeCreditAccountNo()).isEqualTo(UPDATED_CHARGE_CREDIT_ACCOUNT_NO);
    }

    @Test
    @Transactional
    void patchNonExistingWalletTransaction() throws Exception {
        int databaseSizeBeforeUpdate = walletTransactionRepository.findAll().size();
        walletTransaction.setId(count.incrementAndGet());

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restWalletTransactionMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, walletTransaction.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(walletTransaction))
            )
            .andExpect(status().isBadRequest());

        // Validate the WalletTransaction in the database
        List<WalletTransaction> walletTransactionList = walletTransactionRepository.findAll();
        assertThat(walletTransactionList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithIdMismatchWalletTransaction() throws Exception {
        int databaseSizeBeforeUpdate = walletTransactionRepository.findAll().size();
        walletTransaction.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restWalletTransactionMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(walletTransaction))
            )
            .andExpect(status().isBadRequest());

        // Validate the WalletTransaction in the database
        List<WalletTransaction> walletTransactionList = walletTransactionRepository.findAll();
        assertThat(walletTransactionList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithMissingIdPathParamWalletTransaction() throws Exception {
        int databaseSizeBeforeUpdate = walletTransactionRepository.findAll().size();
        walletTransaction.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restWalletTransactionMockMvc
            .perform(
                patch(ENTITY_API_URL)
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(walletTransaction))
            )
            .andExpect(status().isMethodNotAllowed());

        // Validate the WalletTransaction in the database
        List<WalletTransaction> walletTransactionList = walletTransactionRepository.findAll();
        assertThat(walletTransactionList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void deleteWalletTransaction() throws Exception {
        // Initialize the database
        walletTransactionRepository.saveAndFlush(walletTransaction);

        int databaseSizeBeforeDelete = walletTransactionRepository.findAll().size();

        // Delete the walletTransaction
        restWalletTransactionMockMvc
            .perform(delete(ENTITY_API_URL_ID, walletTransaction.getId()).accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<WalletTransaction> walletTransactionList = walletTransactionRepository.findAll();
        assertThat(walletTransactionList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
