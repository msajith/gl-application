package com.iconnect101.glapp.web.rest;

import static com.iconnect101.glapp.web.rest.TestUtil.sameNumber;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import com.iconnect101.glapp.IntegrationTest;
import com.iconnect101.glapp.domain.CasaAccount;
import com.iconnect101.glapp.domain.enumeration.AccountStatus;
import com.iconnect101.glapp.repository.CasaAccountRepository;
import java.math.BigDecimal;
import java.time.Instant;
import java.time.LocalDate;
import java.time.ZoneId;
import java.time.temporal.ChronoUnit;
import java.util.List;
import java.util.Random;
import java.util.concurrent.atomic.AtomicLong;
import javax.persistence.EntityManager;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;

/**
 * Integration tests for the {@link CasaAccountResource} REST controller.
 */
@IntegrationTest
@AutoConfigureMockMvc
@WithMockUser
class CasaAccountResourceIT {

    private static final String DEFAULT_ACCOUNT_NO = "AAAAAAAAAA";
    private static final String UPDATED_ACCOUNT_NO = "BBBBBBBBBB";

    private static final String DEFAULT_ACCOUNT_NAME = "AAAAAAAAAA";
    private static final String UPDATED_ACCOUNT_NAME = "BBBBBBBBBB";

    private static final LocalDate DEFAULT_DATE = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_DATE = LocalDate.now(ZoneId.systemDefault());

    private static final BigDecimal DEFAULT_CLOSING_BALANCE = new BigDecimal(1);
    private static final BigDecimal UPDATED_CLOSING_BALANCE = new BigDecimal(2);

    private static final BigDecimal DEFAULT_OPENING_BALANCE = new BigDecimal(1);
    private static final BigDecimal UPDATED_OPENING_BALANCE = new BigDecimal(2);

    private static final BigDecimal DEFAULT_TOTAL_DEBIT = new BigDecimal(1);
    private static final BigDecimal UPDATED_TOTAL_DEBIT = new BigDecimal(2);

    private static final BigDecimal DEFAULT_TOTAL_CREDIT = new BigDecimal(1);
    private static final BigDecimal UPDATED_TOTAL_CREDIT = new BigDecimal(2);

    private static final BigDecimal DEFAULT_AVAILABLE_BALANCE = new BigDecimal(1);
    private static final BigDecimal UPDATED_AVAILABLE_BALANCE = new BigDecimal(2);

    private static final BigDecimal DEFAULT_CURRENT_BALANCE = new BigDecimal(1);
    private static final BigDecimal UPDATED_CURRENT_BALANCE = new BigDecimal(2);

    private static final Instant DEFAULT_CREATED_ON = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_CREATED_ON = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    private static final String DEFAULT_CREATED_BY = "AAAAAAAAAA";
    private static final String UPDATED_CREATED_BY = "BBBBBBBBBB";

    private static final AccountStatus DEFAULT_ACCOUNT_STATUS = AccountStatus.ACTIVE;
    private static final AccountStatus UPDATED_ACCOUNT_STATUS = AccountStatus.INACTIVE;

    private static final String ENTITY_API_URL = "/api/casa-accounts";
    private static final String ENTITY_API_URL_ID = ENTITY_API_URL + "/{id}";

    private static Random random = new Random();
    private static AtomicLong count = new AtomicLong(random.nextInt() + (2 * Integer.MAX_VALUE));

    @Autowired
    private CasaAccountRepository casaAccountRepository;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restCasaAccountMockMvc;

    private CasaAccount casaAccount;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static CasaAccount createEntity(EntityManager em) {
        CasaAccount casaAccount = new CasaAccount()
            .accountNo(DEFAULT_ACCOUNT_NO)
            .accountName(DEFAULT_ACCOUNT_NAME)
            .date(DEFAULT_DATE)
            .closingBalance(DEFAULT_CLOSING_BALANCE)
            .openingBalance(DEFAULT_OPENING_BALANCE)
            .totalDebit(DEFAULT_TOTAL_DEBIT)
            .totalCredit(DEFAULT_TOTAL_CREDIT)
            .availableBalance(DEFAULT_AVAILABLE_BALANCE)
            .currentBalance(DEFAULT_CURRENT_BALANCE)
            .createdOn(DEFAULT_CREATED_ON)
            .createdBy(DEFAULT_CREATED_BY)
            .accountStatus(DEFAULT_ACCOUNT_STATUS);
        return casaAccount;
    }

    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static CasaAccount createUpdatedEntity(EntityManager em) {
        CasaAccount casaAccount = new CasaAccount()
            .accountNo(UPDATED_ACCOUNT_NO)
            .accountName(UPDATED_ACCOUNT_NAME)
            .date(UPDATED_DATE)
            .closingBalance(UPDATED_CLOSING_BALANCE)
            .openingBalance(UPDATED_OPENING_BALANCE)
            .totalDebit(UPDATED_TOTAL_DEBIT)
            .totalCredit(UPDATED_TOTAL_CREDIT)
            .availableBalance(UPDATED_AVAILABLE_BALANCE)
            .currentBalance(UPDATED_CURRENT_BALANCE)
            .createdOn(UPDATED_CREATED_ON)
            .createdBy(UPDATED_CREATED_BY)
            .accountStatus(UPDATED_ACCOUNT_STATUS);
        return casaAccount;
    }

    @BeforeEach
    public void initTest() {
        casaAccount = createEntity(em);
    }

    @Test
    @Transactional
    void createCasaAccount() throws Exception {
        int databaseSizeBeforeCreate = casaAccountRepository.findAll().size();
        // Create the CasaAccount
        restCasaAccountMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(casaAccount)))
            .andExpect(status().isCreated());

        // Validate the CasaAccount in the database
        List<CasaAccount> casaAccountList = casaAccountRepository.findAll();
        assertThat(casaAccountList).hasSize(databaseSizeBeforeCreate + 1);
        CasaAccount testCasaAccount = casaAccountList.get(casaAccountList.size() - 1);
        assertThat(testCasaAccount.getAccountNo()).isEqualTo(DEFAULT_ACCOUNT_NO);
        assertThat(testCasaAccount.getAccountName()).isEqualTo(DEFAULT_ACCOUNT_NAME);
        assertThat(testCasaAccount.getDate()).isEqualTo(DEFAULT_DATE);
        assertThat(testCasaAccount.getClosingBalance()).isEqualByComparingTo(DEFAULT_CLOSING_BALANCE);
        assertThat(testCasaAccount.getOpeningBalance()).isEqualByComparingTo(DEFAULT_OPENING_BALANCE);
        assertThat(testCasaAccount.getTotalDebit()).isEqualByComparingTo(DEFAULT_TOTAL_DEBIT);
        assertThat(testCasaAccount.getTotalCredit()).isEqualByComparingTo(DEFAULT_TOTAL_CREDIT);
        assertThat(testCasaAccount.getAvailableBalance()).isEqualByComparingTo(DEFAULT_AVAILABLE_BALANCE);
        assertThat(testCasaAccount.getCurrentBalance()).isEqualByComparingTo(DEFAULT_CURRENT_BALANCE);
        assertThat(testCasaAccount.getCreatedOn()).isEqualTo(DEFAULT_CREATED_ON);
        assertThat(testCasaAccount.getCreatedBy()).isEqualTo(DEFAULT_CREATED_BY);
        assertThat(testCasaAccount.getAccountStatus()).isEqualTo(DEFAULT_ACCOUNT_STATUS);
    }

    @Test
    @Transactional
    void createCasaAccountWithExistingId() throws Exception {
        // Create the CasaAccount with an existing ID
        casaAccount.setId(1L);

        int databaseSizeBeforeCreate = casaAccountRepository.findAll().size();

        // An entity with an existing ID cannot be created, so this API call must fail
        restCasaAccountMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(casaAccount)))
            .andExpect(status().isBadRequest());

        // Validate the CasaAccount in the database
        List<CasaAccount> casaAccountList = casaAccountRepository.findAll();
        assertThat(casaAccountList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    void checkAccountNoIsRequired() throws Exception {
        int databaseSizeBeforeTest = casaAccountRepository.findAll().size();
        // set the field null
        casaAccount.setAccountNo(null);

        // Create the CasaAccount, which fails.

        restCasaAccountMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(casaAccount)))
            .andExpect(status().isBadRequest());

        List<CasaAccount> casaAccountList = casaAccountRepository.findAll();
        assertThat(casaAccountList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    void checkAccountNameIsRequired() throws Exception {
        int databaseSizeBeforeTest = casaAccountRepository.findAll().size();
        // set the field null
        casaAccount.setAccountName(null);

        // Create the CasaAccount, which fails.

        restCasaAccountMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(casaAccount)))
            .andExpect(status().isBadRequest());

        List<CasaAccount> casaAccountList = casaAccountRepository.findAll();
        assertThat(casaAccountList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    void getAllCasaAccounts() throws Exception {
        // Initialize the database
        casaAccountRepository.saveAndFlush(casaAccount);

        // Get all the casaAccountList
        restCasaAccountMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(casaAccount.getId().intValue())))
            .andExpect(jsonPath("$.[*].accountNo").value(hasItem(DEFAULT_ACCOUNT_NO)))
            .andExpect(jsonPath("$.[*].accountName").value(hasItem(DEFAULT_ACCOUNT_NAME)))
            .andExpect(jsonPath("$.[*].date").value(hasItem(DEFAULT_DATE.toString())))
            .andExpect(jsonPath("$.[*].closingBalance").value(hasItem(sameNumber(DEFAULT_CLOSING_BALANCE))))
            .andExpect(jsonPath("$.[*].openingBalance").value(hasItem(sameNumber(DEFAULT_OPENING_BALANCE))))
            .andExpect(jsonPath("$.[*].totalDebit").value(hasItem(sameNumber(DEFAULT_TOTAL_DEBIT))))
            .andExpect(jsonPath("$.[*].totalCredit").value(hasItem(sameNumber(DEFAULT_TOTAL_CREDIT))))
            .andExpect(jsonPath("$.[*].availableBalance").value(hasItem(sameNumber(DEFAULT_AVAILABLE_BALANCE))))
            .andExpect(jsonPath("$.[*].currentBalance").value(hasItem(sameNumber(DEFAULT_CURRENT_BALANCE))))
            .andExpect(jsonPath("$.[*].createdOn").value(hasItem(DEFAULT_CREATED_ON.toString())))
            .andExpect(jsonPath("$.[*].createdBy").value(hasItem(DEFAULT_CREATED_BY)))
            .andExpect(jsonPath("$.[*].accountStatus").value(hasItem(DEFAULT_ACCOUNT_STATUS.toString())));
    }

    @Test
    @Transactional
    void getCasaAccount() throws Exception {
        // Initialize the database
        casaAccountRepository.saveAndFlush(casaAccount);

        // Get the casaAccount
        restCasaAccountMockMvc
            .perform(get(ENTITY_API_URL_ID, casaAccount.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(casaAccount.getId().intValue()))
            .andExpect(jsonPath("$.accountNo").value(DEFAULT_ACCOUNT_NO))
            .andExpect(jsonPath("$.accountName").value(DEFAULT_ACCOUNT_NAME))
            .andExpect(jsonPath("$.date").value(DEFAULT_DATE.toString()))
            .andExpect(jsonPath("$.closingBalance").value(sameNumber(DEFAULT_CLOSING_BALANCE)))
            .andExpect(jsonPath("$.openingBalance").value(sameNumber(DEFAULT_OPENING_BALANCE)))
            .andExpect(jsonPath("$.totalDebit").value(sameNumber(DEFAULT_TOTAL_DEBIT)))
            .andExpect(jsonPath("$.totalCredit").value(sameNumber(DEFAULT_TOTAL_CREDIT)))
            .andExpect(jsonPath("$.availableBalance").value(sameNumber(DEFAULT_AVAILABLE_BALANCE)))
            .andExpect(jsonPath("$.currentBalance").value(sameNumber(DEFAULT_CURRENT_BALANCE)))
            .andExpect(jsonPath("$.createdOn").value(DEFAULT_CREATED_ON.toString()))
            .andExpect(jsonPath("$.createdBy").value(DEFAULT_CREATED_BY))
            .andExpect(jsonPath("$.accountStatus").value(DEFAULT_ACCOUNT_STATUS.toString()));
    }

    @Test
    @Transactional
    void getNonExistingCasaAccount() throws Exception {
        // Get the casaAccount
        restCasaAccountMockMvc.perform(get(ENTITY_API_URL_ID, Long.MAX_VALUE)).andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    void putNewCasaAccount() throws Exception {
        // Initialize the database
        casaAccountRepository.saveAndFlush(casaAccount);

        int databaseSizeBeforeUpdate = casaAccountRepository.findAll().size();

        // Update the casaAccount
        CasaAccount updatedCasaAccount = casaAccountRepository.findById(casaAccount.getId()).get();
        // Disconnect from session so that the updates on updatedCasaAccount are not directly saved in db
        em.detach(updatedCasaAccount);
        updatedCasaAccount
            .accountNo(UPDATED_ACCOUNT_NO)
            .accountName(UPDATED_ACCOUNT_NAME)
            .date(UPDATED_DATE)
            .closingBalance(UPDATED_CLOSING_BALANCE)
            .openingBalance(UPDATED_OPENING_BALANCE)
            .totalDebit(UPDATED_TOTAL_DEBIT)
            .totalCredit(UPDATED_TOTAL_CREDIT)
            .availableBalance(UPDATED_AVAILABLE_BALANCE)
            .currentBalance(UPDATED_CURRENT_BALANCE)
            .createdOn(UPDATED_CREATED_ON)
            .createdBy(UPDATED_CREATED_BY)
            .accountStatus(UPDATED_ACCOUNT_STATUS);

        restCasaAccountMockMvc
            .perform(
                put(ENTITY_API_URL_ID, updatedCasaAccount.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(updatedCasaAccount))
            )
            .andExpect(status().isOk());

        // Validate the CasaAccount in the database
        List<CasaAccount> casaAccountList = casaAccountRepository.findAll();
        assertThat(casaAccountList).hasSize(databaseSizeBeforeUpdate);
        CasaAccount testCasaAccount = casaAccountList.get(casaAccountList.size() - 1);
        assertThat(testCasaAccount.getAccountNo()).isEqualTo(UPDATED_ACCOUNT_NO);
        assertThat(testCasaAccount.getAccountName()).isEqualTo(UPDATED_ACCOUNT_NAME);
        assertThat(testCasaAccount.getDate()).isEqualTo(UPDATED_DATE);
        assertThat(testCasaAccount.getClosingBalance()).isEqualTo(UPDATED_CLOSING_BALANCE);
        assertThat(testCasaAccount.getOpeningBalance()).isEqualTo(UPDATED_OPENING_BALANCE);
        assertThat(testCasaAccount.getTotalDebit()).isEqualTo(UPDATED_TOTAL_DEBIT);
        assertThat(testCasaAccount.getTotalCredit()).isEqualTo(UPDATED_TOTAL_CREDIT);
        assertThat(testCasaAccount.getAvailableBalance()).isEqualTo(UPDATED_AVAILABLE_BALANCE);
        assertThat(testCasaAccount.getCurrentBalance()).isEqualTo(UPDATED_CURRENT_BALANCE);
        assertThat(testCasaAccount.getCreatedOn()).isEqualTo(UPDATED_CREATED_ON);
        assertThat(testCasaAccount.getCreatedBy()).isEqualTo(UPDATED_CREATED_BY);
        assertThat(testCasaAccount.getAccountStatus()).isEqualTo(UPDATED_ACCOUNT_STATUS);
    }

    @Test
    @Transactional
    void putNonExistingCasaAccount() throws Exception {
        int databaseSizeBeforeUpdate = casaAccountRepository.findAll().size();
        casaAccount.setId(count.incrementAndGet());

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restCasaAccountMockMvc
            .perform(
                put(ENTITY_API_URL_ID, casaAccount.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(casaAccount))
            )
            .andExpect(status().isBadRequest());

        // Validate the CasaAccount in the database
        List<CasaAccount> casaAccountList = casaAccountRepository.findAll();
        assertThat(casaAccountList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithIdMismatchCasaAccount() throws Exception {
        int databaseSizeBeforeUpdate = casaAccountRepository.findAll().size();
        casaAccount.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restCasaAccountMockMvc
            .perform(
                put(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(casaAccount))
            )
            .andExpect(status().isBadRequest());

        // Validate the CasaAccount in the database
        List<CasaAccount> casaAccountList = casaAccountRepository.findAll();
        assertThat(casaAccountList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithMissingIdPathParamCasaAccount() throws Exception {
        int databaseSizeBeforeUpdate = casaAccountRepository.findAll().size();
        casaAccount.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restCasaAccountMockMvc
            .perform(put(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(casaAccount)))
            .andExpect(status().isMethodNotAllowed());

        // Validate the CasaAccount in the database
        List<CasaAccount> casaAccountList = casaAccountRepository.findAll();
        assertThat(casaAccountList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void partialUpdateCasaAccountWithPatch() throws Exception {
        // Initialize the database
        casaAccountRepository.saveAndFlush(casaAccount);

        int databaseSizeBeforeUpdate = casaAccountRepository.findAll().size();

        // Update the casaAccount using partial update
        CasaAccount partialUpdatedCasaAccount = new CasaAccount();
        partialUpdatedCasaAccount.setId(casaAccount.getId());

        partialUpdatedCasaAccount.totalDebit(UPDATED_TOTAL_DEBIT).createdOn(UPDATED_CREATED_ON).createdBy(UPDATED_CREATED_BY);

        restCasaAccountMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedCasaAccount.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedCasaAccount))
            )
            .andExpect(status().isOk());

        // Validate the CasaAccount in the database
        List<CasaAccount> casaAccountList = casaAccountRepository.findAll();
        assertThat(casaAccountList).hasSize(databaseSizeBeforeUpdate);
        CasaAccount testCasaAccount = casaAccountList.get(casaAccountList.size() - 1);
        assertThat(testCasaAccount.getAccountNo()).isEqualTo(DEFAULT_ACCOUNT_NO);
        assertThat(testCasaAccount.getAccountName()).isEqualTo(DEFAULT_ACCOUNT_NAME);
        assertThat(testCasaAccount.getDate()).isEqualTo(DEFAULT_DATE);
        assertThat(testCasaAccount.getClosingBalance()).isEqualByComparingTo(DEFAULT_CLOSING_BALANCE);
        assertThat(testCasaAccount.getOpeningBalance()).isEqualByComparingTo(DEFAULT_OPENING_BALANCE);
        assertThat(testCasaAccount.getTotalDebit()).isEqualByComparingTo(UPDATED_TOTAL_DEBIT);
        assertThat(testCasaAccount.getTotalCredit()).isEqualByComparingTo(DEFAULT_TOTAL_CREDIT);
        assertThat(testCasaAccount.getAvailableBalance()).isEqualByComparingTo(DEFAULT_AVAILABLE_BALANCE);
        assertThat(testCasaAccount.getCurrentBalance()).isEqualByComparingTo(DEFAULT_CURRENT_BALANCE);
        assertThat(testCasaAccount.getCreatedOn()).isEqualTo(UPDATED_CREATED_ON);
        assertThat(testCasaAccount.getCreatedBy()).isEqualTo(UPDATED_CREATED_BY);
        assertThat(testCasaAccount.getAccountStatus()).isEqualTo(DEFAULT_ACCOUNT_STATUS);
    }

    @Test
    @Transactional
    void fullUpdateCasaAccountWithPatch() throws Exception {
        // Initialize the database
        casaAccountRepository.saveAndFlush(casaAccount);

        int databaseSizeBeforeUpdate = casaAccountRepository.findAll().size();

        // Update the casaAccount using partial update
        CasaAccount partialUpdatedCasaAccount = new CasaAccount();
        partialUpdatedCasaAccount.setId(casaAccount.getId());

        partialUpdatedCasaAccount
            .accountNo(UPDATED_ACCOUNT_NO)
            .accountName(UPDATED_ACCOUNT_NAME)
            .date(UPDATED_DATE)
            .closingBalance(UPDATED_CLOSING_BALANCE)
            .openingBalance(UPDATED_OPENING_BALANCE)
            .totalDebit(UPDATED_TOTAL_DEBIT)
            .totalCredit(UPDATED_TOTAL_CREDIT)
            .availableBalance(UPDATED_AVAILABLE_BALANCE)
            .currentBalance(UPDATED_CURRENT_BALANCE)
            .createdOn(UPDATED_CREATED_ON)
            .createdBy(UPDATED_CREATED_BY)
            .accountStatus(UPDATED_ACCOUNT_STATUS);

        restCasaAccountMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedCasaAccount.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedCasaAccount))
            )
            .andExpect(status().isOk());

        // Validate the CasaAccount in the database
        List<CasaAccount> casaAccountList = casaAccountRepository.findAll();
        assertThat(casaAccountList).hasSize(databaseSizeBeforeUpdate);
        CasaAccount testCasaAccount = casaAccountList.get(casaAccountList.size() - 1);
        assertThat(testCasaAccount.getAccountNo()).isEqualTo(UPDATED_ACCOUNT_NO);
        assertThat(testCasaAccount.getAccountName()).isEqualTo(UPDATED_ACCOUNT_NAME);
        assertThat(testCasaAccount.getDate()).isEqualTo(UPDATED_DATE);
        assertThat(testCasaAccount.getClosingBalance()).isEqualByComparingTo(UPDATED_CLOSING_BALANCE);
        assertThat(testCasaAccount.getOpeningBalance()).isEqualByComparingTo(UPDATED_OPENING_BALANCE);
        assertThat(testCasaAccount.getTotalDebit()).isEqualByComparingTo(UPDATED_TOTAL_DEBIT);
        assertThat(testCasaAccount.getTotalCredit()).isEqualByComparingTo(UPDATED_TOTAL_CREDIT);
        assertThat(testCasaAccount.getAvailableBalance()).isEqualByComparingTo(UPDATED_AVAILABLE_BALANCE);
        assertThat(testCasaAccount.getCurrentBalance()).isEqualByComparingTo(UPDATED_CURRENT_BALANCE);
        assertThat(testCasaAccount.getCreatedOn()).isEqualTo(UPDATED_CREATED_ON);
        assertThat(testCasaAccount.getCreatedBy()).isEqualTo(UPDATED_CREATED_BY);
        assertThat(testCasaAccount.getAccountStatus()).isEqualTo(UPDATED_ACCOUNT_STATUS);
    }

    @Test
    @Transactional
    void patchNonExistingCasaAccount() throws Exception {
        int databaseSizeBeforeUpdate = casaAccountRepository.findAll().size();
        casaAccount.setId(count.incrementAndGet());

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restCasaAccountMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, casaAccount.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(casaAccount))
            )
            .andExpect(status().isBadRequest());

        // Validate the CasaAccount in the database
        List<CasaAccount> casaAccountList = casaAccountRepository.findAll();
        assertThat(casaAccountList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithIdMismatchCasaAccount() throws Exception {
        int databaseSizeBeforeUpdate = casaAccountRepository.findAll().size();
        casaAccount.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restCasaAccountMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(casaAccount))
            )
            .andExpect(status().isBadRequest());

        // Validate the CasaAccount in the database
        List<CasaAccount> casaAccountList = casaAccountRepository.findAll();
        assertThat(casaAccountList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithMissingIdPathParamCasaAccount() throws Exception {
        int databaseSizeBeforeUpdate = casaAccountRepository.findAll().size();
        casaAccount.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restCasaAccountMockMvc
            .perform(
                patch(ENTITY_API_URL).contentType("application/merge-patch+json").content(TestUtil.convertObjectToJsonBytes(casaAccount))
            )
            .andExpect(status().isMethodNotAllowed());

        // Validate the CasaAccount in the database
        List<CasaAccount> casaAccountList = casaAccountRepository.findAll();
        assertThat(casaAccountList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void deleteCasaAccount() throws Exception {
        // Initialize the database
        casaAccountRepository.saveAndFlush(casaAccount);

        int databaseSizeBeforeDelete = casaAccountRepository.findAll().size();

        // Delete the casaAccount
        restCasaAccountMockMvc
            .perform(delete(ENTITY_API_URL_ID, casaAccount.getId()).accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<CasaAccount> casaAccountList = casaAccountRepository.findAll();
        assertThat(casaAccountList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
