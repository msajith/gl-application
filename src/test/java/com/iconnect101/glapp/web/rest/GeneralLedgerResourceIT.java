package com.iconnect101.glapp.web.rest;

import static com.iconnect101.glapp.web.rest.TestUtil.sameInstant;
import static com.iconnect101.glapp.web.rest.TestUtil.sameNumber;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import com.iconnect101.glapp.IntegrationTest;
import com.iconnect101.glapp.domain.GeneralLedger;
import com.iconnect101.glapp.domain.enumeration.EntryStatus;
import com.iconnect101.glapp.domain.enumeration.GlEntryType;
import com.iconnect101.glapp.repository.GeneralLedgerRepository;
import java.math.BigDecimal;
import java.time.Instant;
import java.time.LocalDate;
import java.time.ZoneId;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.time.temporal.ChronoUnit;
import java.util.List;
import java.util.Random;
import java.util.concurrent.atomic.AtomicLong;
import javax.persistence.EntityManager;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;

/**
 * Integration tests for the {@link GeneralLedgerResource} REST controller.
 */
@IntegrationTest
@AutoConfigureMockMvc
@WithMockUser
class GeneralLedgerResourceIT {

    private static final String DEFAULT_GL_REF_NO = "AAAAAAAAAA";
    private static final String UPDATED_GL_REF_NO = "BBBBBBBBBB";

    private static final String DEFAULT_GL_ACCOUNT_NO = "AAAAAAAAAA";
    private static final String UPDATED_GL_ACCOUNT_NO = "BBBBBBBBBB";

    private static final LocalDate DEFAULT_TXN_DATE = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_TXN_DATE = LocalDate.now(ZoneId.systemDefault());

    private static final Instant DEFAULT_TXN_DATE_IN_UTC = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_TXN_DATE_IN_UTC = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    private static final ZonedDateTime DEFAULT_TXN_DATE_IN_LOCAL = ZonedDateTime.ofInstant(Instant.ofEpochMilli(0L), ZoneOffset.UTC);
    private static final ZonedDateTime UPDATED_TXN_DATE_IN_LOCAL = ZonedDateTime.now(ZoneId.systemDefault()).withNano(0);

    private static final ZonedDateTime DEFAULT_TXN_DATE_FOR_SETTLEMENT = ZonedDateTime.ofInstant(Instant.ofEpochMilli(0L), ZoneOffset.UTC);
    private static final ZonedDateTime UPDATED_TXN_DATE_FOR_SETTLEMENT = ZonedDateTime.now(ZoneId.systemDefault()).withNano(0);

    private static final BigDecimal DEFAULT_AMOUNT_IN_ACCOUNT_CURRENCY = new BigDecimal(1);
    private static final BigDecimal UPDATED_AMOUNT_IN_ACCOUNT_CURRENCY = new BigDecimal(2);

    private static final BigDecimal DEFAULT_AMOUNT_IN_LOCAL_CURRENCY = new BigDecimal(1);
    private static final BigDecimal UPDATED_AMOUNT_IN_LOCAL_CURRENCY = new BigDecimal(2);

    private static final BigDecimal DEFAULT_RATE = new BigDecimal(1);
    private static final BigDecimal UPDATED_RATE = new BigDecimal(2);

    private static final String DEFAULT_ENTRY_CATEGORY = "AAAAAAAAAA";
    private static final String UPDATED_ENTRY_CATEGORY = "BBBBBBBBBB";

    private static final String DEFAULT_DESCRIPTION_1 = "AAAAAAAAAA";
    private static final String UPDATED_DESCRIPTION_1 = "BBBBBBBBBB";

    private static final String DEFAULT_DESCRIPTION_2 = "AAAAAAAAAA";
    private static final String UPDATED_DESCRIPTION_2 = "BBBBBBBBBB";

    private static final Instant DEFAULT_CREATED_ON = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_CREATED_ON = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    private static final String DEFAULT_CREATED_BY = "AAAAAAAAAA";
    private static final String UPDATED_CREATED_BY = "BBBBBBBBBB";

    private static final EntryStatus DEFAULT_ENTRY_STATUS = EntryStatus.ACTIVE;
    private static final EntryStatus UPDATED_ENTRY_STATUS = EntryStatus.CANCELLED;

    private static final GlEntryType DEFAULT_GL_ENTRY_TYPE = GlEntryType.DR;
    private static final GlEntryType UPDATED_GL_ENTRY_TYPE = GlEntryType.CR;

    private static final String ENTITY_API_URL = "/api/general-ledgers";
    private static final String ENTITY_API_URL_ID = ENTITY_API_URL + "/{id}";

    private static Random random = new Random();
    private static AtomicLong count = new AtomicLong(random.nextInt() + (2 * Integer.MAX_VALUE));

    @Autowired
    private GeneralLedgerRepository generalLedgerRepository;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restGeneralLedgerMockMvc;

    private GeneralLedger generalLedger;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static GeneralLedger createEntity(EntityManager em) {
        GeneralLedger generalLedger = new GeneralLedger()
            .glRefNo(DEFAULT_GL_REF_NO)
            .glAccountNo(DEFAULT_GL_ACCOUNT_NO)
            .txnDate(DEFAULT_TXN_DATE)
            .txnDateInUTC(DEFAULT_TXN_DATE_IN_UTC)
            .txnDateInLocal(DEFAULT_TXN_DATE_IN_LOCAL)
            .txnDateForSettlement(DEFAULT_TXN_DATE_FOR_SETTLEMENT)
            .amountInAccountCurrency(DEFAULT_AMOUNT_IN_ACCOUNT_CURRENCY)
            .amountInLocalCurrency(DEFAULT_AMOUNT_IN_LOCAL_CURRENCY)
            .rate(DEFAULT_RATE)
            .entryCategory(DEFAULT_ENTRY_CATEGORY)
            .description1(DEFAULT_DESCRIPTION_1)
            .description2(DEFAULT_DESCRIPTION_2)
            .createdOn(DEFAULT_CREATED_ON)
            .createdBy(DEFAULT_CREATED_BY)
            .entryStatus(DEFAULT_ENTRY_STATUS)
            .glEntryType(DEFAULT_GL_ENTRY_TYPE);
        return generalLedger;
    }

    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static GeneralLedger createUpdatedEntity(EntityManager em) {
        GeneralLedger generalLedger = new GeneralLedger()
            .glRefNo(UPDATED_GL_REF_NO)
            .glAccountNo(UPDATED_GL_ACCOUNT_NO)
            .txnDate(UPDATED_TXN_DATE)
            .txnDateInUTC(UPDATED_TXN_DATE_IN_UTC)
            .txnDateInLocal(UPDATED_TXN_DATE_IN_LOCAL)
            .txnDateForSettlement(UPDATED_TXN_DATE_FOR_SETTLEMENT)
            .amountInAccountCurrency(UPDATED_AMOUNT_IN_ACCOUNT_CURRENCY)
            .amountInLocalCurrency(UPDATED_AMOUNT_IN_LOCAL_CURRENCY)
            .rate(UPDATED_RATE)
            .entryCategory(UPDATED_ENTRY_CATEGORY)
            .description1(UPDATED_DESCRIPTION_1)
            .description2(UPDATED_DESCRIPTION_2)
            .createdOn(UPDATED_CREATED_ON)
            .createdBy(UPDATED_CREATED_BY)
            .entryStatus(UPDATED_ENTRY_STATUS)
            .glEntryType(UPDATED_GL_ENTRY_TYPE);
        return generalLedger;
    }

    @BeforeEach
    public void initTest() {
        generalLedger = createEntity(em);
    }

    @Test
    @Transactional
    void createGeneralLedger() throws Exception {
        int databaseSizeBeforeCreate = generalLedgerRepository.findAll().size();
        // Create the GeneralLedger
        restGeneralLedgerMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(generalLedger)))
            .andExpect(status().isCreated());

        // Validate the GeneralLedger in the database
        List<GeneralLedger> generalLedgerList = generalLedgerRepository.findAll();
        assertThat(generalLedgerList).hasSize(databaseSizeBeforeCreate + 1);
        GeneralLedger testGeneralLedger = generalLedgerList.get(generalLedgerList.size() - 1);
        assertThat(testGeneralLedger.getGlRefNo()).isEqualTo(DEFAULT_GL_REF_NO);
        assertThat(testGeneralLedger.getGlAccountNo()).isEqualTo(DEFAULT_GL_ACCOUNT_NO);
        assertThat(testGeneralLedger.getTxnDate()).isEqualTo(DEFAULT_TXN_DATE);
        assertThat(testGeneralLedger.getTxnDateInUTC()).isEqualTo(DEFAULT_TXN_DATE_IN_UTC);
        assertThat(testGeneralLedger.getTxnDateInLocal()).isEqualTo(DEFAULT_TXN_DATE_IN_LOCAL);
        assertThat(testGeneralLedger.getTxnDateForSettlement()).isEqualTo(DEFAULT_TXN_DATE_FOR_SETTLEMENT);
        assertThat(testGeneralLedger.getAmountInAccountCurrency()).isEqualByComparingTo(DEFAULT_AMOUNT_IN_ACCOUNT_CURRENCY);
        assertThat(testGeneralLedger.getAmountInLocalCurrency()).isEqualByComparingTo(DEFAULT_AMOUNT_IN_LOCAL_CURRENCY);
        assertThat(testGeneralLedger.getRate()).isEqualByComparingTo(DEFAULT_RATE);
        assertThat(testGeneralLedger.getEntryCategory()).isEqualTo(DEFAULT_ENTRY_CATEGORY);
        assertThat(testGeneralLedger.getDescription1()).isEqualTo(DEFAULT_DESCRIPTION_1);
        assertThat(testGeneralLedger.getDescription2()).isEqualTo(DEFAULT_DESCRIPTION_2);
        assertThat(testGeneralLedger.getCreatedOn()).isEqualTo(DEFAULT_CREATED_ON);
        assertThat(testGeneralLedger.getCreatedBy()).isEqualTo(DEFAULT_CREATED_BY);
        assertThat(testGeneralLedger.getEntryStatus()).isEqualTo(DEFAULT_ENTRY_STATUS);
        assertThat(testGeneralLedger.getGlEntryType()).isEqualTo(DEFAULT_GL_ENTRY_TYPE);
    }

    @Test
    @Transactional
    void createGeneralLedgerWithExistingId() throws Exception {
        // Create the GeneralLedger with an existing ID
        generalLedger.setId(1L);

        int databaseSizeBeforeCreate = generalLedgerRepository.findAll().size();

        // An entity with an existing ID cannot be created, so this API call must fail
        restGeneralLedgerMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(generalLedger)))
            .andExpect(status().isBadRequest());

        // Validate the GeneralLedger in the database
        List<GeneralLedger> generalLedgerList = generalLedgerRepository.findAll();
        assertThat(generalLedgerList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    void getAllGeneralLedgers() throws Exception {
        // Initialize the database
        generalLedgerRepository.saveAndFlush(generalLedger);

        // Get all the generalLedgerList
        restGeneralLedgerMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(generalLedger.getId().intValue())))
            .andExpect(jsonPath("$.[*].glRefNo").value(hasItem(DEFAULT_GL_REF_NO)))
            .andExpect(jsonPath("$.[*].glAccountNo").value(hasItem(DEFAULT_GL_ACCOUNT_NO)))
            .andExpect(jsonPath("$.[*].txnDate").value(hasItem(DEFAULT_TXN_DATE.toString())))
            .andExpect(jsonPath("$.[*].txnDateInUTC").value(hasItem(DEFAULT_TXN_DATE_IN_UTC.toString())))
            .andExpect(jsonPath("$.[*].txnDateInLocal").value(hasItem(sameInstant(DEFAULT_TXN_DATE_IN_LOCAL))))
            .andExpect(jsonPath("$.[*].txnDateForSettlement").value(hasItem(sameInstant(DEFAULT_TXN_DATE_FOR_SETTLEMENT))))
            .andExpect(jsonPath("$.[*].amountInAccountCurrency").value(hasItem(sameNumber(DEFAULT_AMOUNT_IN_ACCOUNT_CURRENCY))))
            .andExpect(jsonPath("$.[*].amountInLocalCurrency").value(hasItem(sameNumber(DEFAULT_AMOUNT_IN_LOCAL_CURRENCY))))
            .andExpect(jsonPath("$.[*].rate").value(hasItem(sameNumber(DEFAULT_RATE))))
            .andExpect(jsonPath("$.[*].entryCategory").value(hasItem(DEFAULT_ENTRY_CATEGORY)))
            .andExpect(jsonPath("$.[*].description1").value(hasItem(DEFAULT_DESCRIPTION_1)))
            .andExpect(jsonPath("$.[*].description2").value(hasItem(DEFAULT_DESCRIPTION_2)))
            .andExpect(jsonPath("$.[*].createdOn").value(hasItem(DEFAULT_CREATED_ON.toString())))
            .andExpect(jsonPath("$.[*].createdBy").value(hasItem(DEFAULT_CREATED_BY)))
            .andExpect(jsonPath("$.[*].entryStatus").value(hasItem(DEFAULT_ENTRY_STATUS.toString())))
            .andExpect(jsonPath("$.[*].glEntryType").value(hasItem(DEFAULT_GL_ENTRY_TYPE.toString())));
    }

    @Test
    @Transactional
    void getGeneralLedger() throws Exception {
        // Initialize the database
        generalLedgerRepository.saveAndFlush(generalLedger);

        // Get the generalLedger
        restGeneralLedgerMockMvc
            .perform(get(ENTITY_API_URL_ID, generalLedger.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(generalLedger.getId().intValue()))
            .andExpect(jsonPath("$.glRefNo").value(DEFAULT_GL_REF_NO))
            .andExpect(jsonPath("$.glAccountNo").value(DEFAULT_GL_ACCOUNT_NO))
            .andExpect(jsonPath("$.txnDate").value(DEFAULT_TXN_DATE.toString()))
            .andExpect(jsonPath("$.txnDateInUTC").value(DEFAULT_TXN_DATE_IN_UTC.toString()))
            .andExpect(jsonPath("$.txnDateInLocal").value(sameInstant(DEFAULT_TXN_DATE_IN_LOCAL)))
            .andExpect(jsonPath("$.txnDateForSettlement").value(sameInstant(DEFAULT_TXN_DATE_FOR_SETTLEMENT)))
            .andExpect(jsonPath("$.amountInAccountCurrency").value(sameNumber(DEFAULT_AMOUNT_IN_ACCOUNT_CURRENCY)))
            .andExpect(jsonPath("$.amountInLocalCurrency").value(sameNumber(DEFAULT_AMOUNT_IN_LOCAL_CURRENCY)))
            .andExpect(jsonPath("$.rate").value(sameNumber(DEFAULT_RATE)))
            .andExpect(jsonPath("$.entryCategory").value(DEFAULT_ENTRY_CATEGORY))
            .andExpect(jsonPath("$.description1").value(DEFAULT_DESCRIPTION_1))
            .andExpect(jsonPath("$.description2").value(DEFAULT_DESCRIPTION_2))
            .andExpect(jsonPath("$.createdOn").value(DEFAULT_CREATED_ON.toString()))
            .andExpect(jsonPath("$.createdBy").value(DEFAULT_CREATED_BY))
            .andExpect(jsonPath("$.entryStatus").value(DEFAULT_ENTRY_STATUS.toString()))
            .andExpect(jsonPath("$.glEntryType").value(DEFAULT_GL_ENTRY_TYPE.toString()));
    }

    @Test
    @Transactional
    void getNonExistingGeneralLedger() throws Exception {
        // Get the generalLedger
        restGeneralLedgerMockMvc.perform(get(ENTITY_API_URL_ID, Long.MAX_VALUE)).andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    void putNewGeneralLedger() throws Exception {
        // Initialize the database
        generalLedgerRepository.saveAndFlush(generalLedger);

        int databaseSizeBeforeUpdate = generalLedgerRepository.findAll().size();

        // Update the generalLedger
        GeneralLedger updatedGeneralLedger = generalLedgerRepository.findById(generalLedger.getId()).get();
        // Disconnect from session so that the updates on updatedGeneralLedger are not directly saved in db
        em.detach(updatedGeneralLedger);
        updatedGeneralLedger
            .glRefNo(UPDATED_GL_REF_NO)
            .glAccountNo(UPDATED_GL_ACCOUNT_NO)
            .txnDate(UPDATED_TXN_DATE)
            .txnDateInUTC(UPDATED_TXN_DATE_IN_UTC)
            .txnDateInLocal(UPDATED_TXN_DATE_IN_LOCAL)
            .txnDateForSettlement(UPDATED_TXN_DATE_FOR_SETTLEMENT)
            .amountInAccountCurrency(UPDATED_AMOUNT_IN_ACCOUNT_CURRENCY)
            .amountInLocalCurrency(UPDATED_AMOUNT_IN_LOCAL_CURRENCY)
            .rate(UPDATED_RATE)
            .entryCategory(UPDATED_ENTRY_CATEGORY)
            .description1(UPDATED_DESCRIPTION_1)
            .description2(UPDATED_DESCRIPTION_2)
            .createdOn(UPDATED_CREATED_ON)
            .createdBy(UPDATED_CREATED_BY)
            .entryStatus(UPDATED_ENTRY_STATUS)
            .glEntryType(UPDATED_GL_ENTRY_TYPE);

        restGeneralLedgerMockMvc
            .perform(
                put(ENTITY_API_URL_ID, updatedGeneralLedger.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(updatedGeneralLedger))
            )
            .andExpect(status().isOk());

        // Validate the GeneralLedger in the database
        List<GeneralLedger> generalLedgerList = generalLedgerRepository.findAll();
        assertThat(generalLedgerList).hasSize(databaseSizeBeforeUpdate);
        GeneralLedger testGeneralLedger = generalLedgerList.get(generalLedgerList.size() - 1);
        assertThat(testGeneralLedger.getGlRefNo()).isEqualTo(UPDATED_GL_REF_NO);
        assertThat(testGeneralLedger.getGlAccountNo()).isEqualTo(UPDATED_GL_ACCOUNT_NO);
        assertThat(testGeneralLedger.getTxnDate()).isEqualTo(UPDATED_TXN_DATE);
        assertThat(testGeneralLedger.getTxnDateInUTC()).isEqualTo(UPDATED_TXN_DATE_IN_UTC);
        assertThat(testGeneralLedger.getTxnDateInLocal()).isEqualTo(UPDATED_TXN_DATE_IN_LOCAL);
        assertThat(testGeneralLedger.getTxnDateForSettlement()).isEqualTo(UPDATED_TXN_DATE_FOR_SETTLEMENT);
        assertThat(testGeneralLedger.getAmountInAccountCurrency()).isEqualTo(UPDATED_AMOUNT_IN_ACCOUNT_CURRENCY);
        assertThat(testGeneralLedger.getAmountInLocalCurrency()).isEqualTo(UPDATED_AMOUNT_IN_LOCAL_CURRENCY);
        assertThat(testGeneralLedger.getRate()).isEqualTo(UPDATED_RATE);
        assertThat(testGeneralLedger.getEntryCategory()).isEqualTo(UPDATED_ENTRY_CATEGORY);
        assertThat(testGeneralLedger.getDescription1()).isEqualTo(UPDATED_DESCRIPTION_1);
        assertThat(testGeneralLedger.getDescription2()).isEqualTo(UPDATED_DESCRIPTION_2);
        assertThat(testGeneralLedger.getCreatedOn()).isEqualTo(UPDATED_CREATED_ON);
        assertThat(testGeneralLedger.getCreatedBy()).isEqualTo(UPDATED_CREATED_BY);
        assertThat(testGeneralLedger.getEntryStatus()).isEqualTo(UPDATED_ENTRY_STATUS);
        assertThat(testGeneralLedger.getGlEntryType()).isEqualTo(UPDATED_GL_ENTRY_TYPE);
    }

    @Test
    @Transactional
    void putNonExistingGeneralLedger() throws Exception {
        int databaseSizeBeforeUpdate = generalLedgerRepository.findAll().size();
        generalLedger.setId(count.incrementAndGet());

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restGeneralLedgerMockMvc
            .perform(
                put(ENTITY_API_URL_ID, generalLedger.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(generalLedger))
            )
            .andExpect(status().isBadRequest());

        // Validate the GeneralLedger in the database
        List<GeneralLedger> generalLedgerList = generalLedgerRepository.findAll();
        assertThat(generalLedgerList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithIdMismatchGeneralLedger() throws Exception {
        int databaseSizeBeforeUpdate = generalLedgerRepository.findAll().size();
        generalLedger.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restGeneralLedgerMockMvc
            .perform(
                put(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(generalLedger))
            )
            .andExpect(status().isBadRequest());

        // Validate the GeneralLedger in the database
        List<GeneralLedger> generalLedgerList = generalLedgerRepository.findAll();
        assertThat(generalLedgerList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithMissingIdPathParamGeneralLedger() throws Exception {
        int databaseSizeBeforeUpdate = generalLedgerRepository.findAll().size();
        generalLedger.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restGeneralLedgerMockMvc
            .perform(put(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(generalLedger)))
            .andExpect(status().isMethodNotAllowed());

        // Validate the GeneralLedger in the database
        List<GeneralLedger> generalLedgerList = generalLedgerRepository.findAll();
        assertThat(generalLedgerList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void partialUpdateGeneralLedgerWithPatch() throws Exception {
        // Initialize the database
        generalLedgerRepository.saveAndFlush(generalLedger);

        int databaseSizeBeforeUpdate = generalLedgerRepository.findAll().size();

        // Update the generalLedger using partial update
        GeneralLedger partialUpdatedGeneralLedger = new GeneralLedger();
        partialUpdatedGeneralLedger.setId(generalLedger.getId());

        partialUpdatedGeneralLedger
            .glRefNo(UPDATED_GL_REF_NO)
            .txnDate(UPDATED_TXN_DATE)
            .txnDateInLocal(UPDATED_TXN_DATE_IN_LOCAL)
            .amountInAccountCurrency(UPDATED_AMOUNT_IN_ACCOUNT_CURRENCY)
            .amountInLocalCurrency(UPDATED_AMOUNT_IN_LOCAL_CURRENCY)
            .rate(UPDATED_RATE)
            .entryCategory(UPDATED_ENTRY_CATEGORY)
            .description1(UPDATED_DESCRIPTION_1)
            .createdOn(UPDATED_CREATED_ON)
            .createdBy(UPDATED_CREATED_BY)
            .glEntryType(UPDATED_GL_ENTRY_TYPE);

        restGeneralLedgerMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedGeneralLedger.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedGeneralLedger))
            )
            .andExpect(status().isOk());

        // Validate the GeneralLedger in the database
        List<GeneralLedger> generalLedgerList = generalLedgerRepository.findAll();
        assertThat(generalLedgerList).hasSize(databaseSizeBeforeUpdate);
        GeneralLedger testGeneralLedger = generalLedgerList.get(generalLedgerList.size() - 1);
        assertThat(testGeneralLedger.getGlRefNo()).isEqualTo(UPDATED_GL_REF_NO);
        assertThat(testGeneralLedger.getGlAccountNo()).isEqualTo(DEFAULT_GL_ACCOUNT_NO);
        assertThat(testGeneralLedger.getTxnDate()).isEqualTo(UPDATED_TXN_DATE);
        assertThat(testGeneralLedger.getTxnDateInUTC()).isEqualTo(DEFAULT_TXN_DATE_IN_UTC);
        assertThat(testGeneralLedger.getTxnDateInLocal()).isEqualTo(UPDATED_TXN_DATE_IN_LOCAL);
        assertThat(testGeneralLedger.getTxnDateForSettlement()).isEqualTo(DEFAULT_TXN_DATE_FOR_SETTLEMENT);
        assertThat(testGeneralLedger.getAmountInAccountCurrency()).isEqualByComparingTo(UPDATED_AMOUNT_IN_ACCOUNT_CURRENCY);
        assertThat(testGeneralLedger.getAmountInLocalCurrency()).isEqualByComparingTo(UPDATED_AMOUNT_IN_LOCAL_CURRENCY);
        assertThat(testGeneralLedger.getRate()).isEqualByComparingTo(UPDATED_RATE);
        assertThat(testGeneralLedger.getEntryCategory()).isEqualTo(UPDATED_ENTRY_CATEGORY);
        assertThat(testGeneralLedger.getDescription1()).isEqualTo(UPDATED_DESCRIPTION_1);
        assertThat(testGeneralLedger.getDescription2()).isEqualTo(DEFAULT_DESCRIPTION_2);
        assertThat(testGeneralLedger.getCreatedOn()).isEqualTo(UPDATED_CREATED_ON);
        assertThat(testGeneralLedger.getCreatedBy()).isEqualTo(UPDATED_CREATED_BY);
        assertThat(testGeneralLedger.getEntryStatus()).isEqualTo(DEFAULT_ENTRY_STATUS);
        assertThat(testGeneralLedger.getGlEntryType()).isEqualTo(UPDATED_GL_ENTRY_TYPE);
    }

    @Test
    @Transactional
    void fullUpdateGeneralLedgerWithPatch() throws Exception {
        // Initialize the database
        generalLedgerRepository.saveAndFlush(generalLedger);

        int databaseSizeBeforeUpdate = generalLedgerRepository.findAll().size();

        // Update the generalLedger using partial update
        GeneralLedger partialUpdatedGeneralLedger = new GeneralLedger();
        partialUpdatedGeneralLedger.setId(generalLedger.getId());

        partialUpdatedGeneralLedger
            .glRefNo(UPDATED_GL_REF_NO)
            .glAccountNo(UPDATED_GL_ACCOUNT_NO)
            .txnDate(UPDATED_TXN_DATE)
            .txnDateInUTC(UPDATED_TXN_DATE_IN_UTC)
            .txnDateInLocal(UPDATED_TXN_DATE_IN_LOCAL)
            .txnDateForSettlement(UPDATED_TXN_DATE_FOR_SETTLEMENT)
            .amountInAccountCurrency(UPDATED_AMOUNT_IN_ACCOUNT_CURRENCY)
            .amountInLocalCurrency(UPDATED_AMOUNT_IN_LOCAL_CURRENCY)
            .rate(UPDATED_RATE)
            .entryCategory(UPDATED_ENTRY_CATEGORY)
            .description1(UPDATED_DESCRIPTION_1)
            .description2(UPDATED_DESCRIPTION_2)
            .createdOn(UPDATED_CREATED_ON)
            .createdBy(UPDATED_CREATED_BY)
            .entryStatus(UPDATED_ENTRY_STATUS)
            .glEntryType(UPDATED_GL_ENTRY_TYPE);

        restGeneralLedgerMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedGeneralLedger.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedGeneralLedger))
            )
            .andExpect(status().isOk());

        // Validate the GeneralLedger in the database
        List<GeneralLedger> generalLedgerList = generalLedgerRepository.findAll();
        assertThat(generalLedgerList).hasSize(databaseSizeBeforeUpdate);
        GeneralLedger testGeneralLedger = generalLedgerList.get(generalLedgerList.size() - 1);
        assertThat(testGeneralLedger.getGlRefNo()).isEqualTo(UPDATED_GL_REF_NO);
        assertThat(testGeneralLedger.getGlAccountNo()).isEqualTo(UPDATED_GL_ACCOUNT_NO);
        assertThat(testGeneralLedger.getTxnDate()).isEqualTo(UPDATED_TXN_DATE);
        assertThat(testGeneralLedger.getTxnDateInUTC()).isEqualTo(UPDATED_TXN_DATE_IN_UTC);
        assertThat(testGeneralLedger.getTxnDateInLocal()).isEqualTo(UPDATED_TXN_DATE_IN_LOCAL);
        assertThat(testGeneralLedger.getTxnDateForSettlement()).isEqualTo(UPDATED_TXN_DATE_FOR_SETTLEMENT);
        assertThat(testGeneralLedger.getAmountInAccountCurrency()).isEqualByComparingTo(UPDATED_AMOUNT_IN_ACCOUNT_CURRENCY);
        assertThat(testGeneralLedger.getAmountInLocalCurrency()).isEqualByComparingTo(UPDATED_AMOUNT_IN_LOCAL_CURRENCY);
        assertThat(testGeneralLedger.getRate()).isEqualByComparingTo(UPDATED_RATE);
        assertThat(testGeneralLedger.getEntryCategory()).isEqualTo(UPDATED_ENTRY_CATEGORY);
        assertThat(testGeneralLedger.getDescription1()).isEqualTo(UPDATED_DESCRIPTION_1);
        assertThat(testGeneralLedger.getDescription2()).isEqualTo(UPDATED_DESCRIPTION_2);
        assertThat(testGeneralLedger.getCreatedOn()).isEqualTo(UPDATED_CREATED_ON);
        assertThat(testGeneralLedger.getCreatedBy()).isEqualTo(UPDATED_CREATED_BY);
        assertThat(testGeneralLedger.getEntryStatus()).isEqualTo(UPDATED_ENTRY_STATUS);
        assertThat(testGeneralLedger.getGlEntryType()).isEqualTo(UPDATED_GL_ENTRY_TYPE);
    }

    @Test
    @Transactional
    void patchNonExistingGeneralLedger() throws Exception {
        int databaseSizeBeforeUpdate = generalLedgerRepository.findAll().size();
        generalLedger.setId(count.incrementAndGet());

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restGeneralLedgerMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, generalLedger.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(generalLedger))
            )
            .andExpect(status().isBadRequest());

        // Validate the GeneralLedger in the database
        List<GeneralLedger> generalLedgerList = generalLedgerRepository.findAll();
        assertThat(generalLedgerList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithIdMismatchGeneralLedger() throws Exception {
        int databaseSizeBeforeUpdate = generalLedgerRepository.findAll().size();
        generalLedger.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restGeneralLedgerMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(generalLedger))
            )
            .andExpect(status().isBadRequest());

        // Validate the GeneralLedger in the database
        List<GeneralLedger> generalLedgerList = generalLedgerRepository.findAll();
        assertThat(generalLedgerList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithMissingIdPathParamGeneralLedger() throws Exception {
        int databaseSizeBeforeUpdate = generalLedgerRepository.findAll().size();
        generalLedger.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restGeneralLedgerMockMvc
            .perform(
                patch(ENTITY_API_URL).contentType("application/merge-patch+json").content(TestUtil.convertObjectToJsonBytes(generalLedger))
            )
            .andExpect(status().isMethodNotAllowed());

        // Validate the GeneralLedger in the database
        List<GeneralLedger> generalLedgerList = generalLedgerRepository.findAll();
        assertThat(generalLedgerList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void deleteGeneralLedger() throws Exception {
        // Initialize the database
        generalLedgerRepository.saveAndFlush(generalLedger);

        int databaseSizeBeforeDelete = generalLedgerRepository.findAll().size();

        // Delete the generalLedger
        restGeneralLedgerMockMvc
            .perform(delete(ENTITY_API_URL_ID, generalLedger.getId()).accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<GeneralLedger> generalLedgerList = generalLedgerRepository.findAll();
        assertThat(generalLedgerList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
