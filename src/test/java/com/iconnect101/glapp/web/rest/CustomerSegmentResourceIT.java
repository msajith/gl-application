package com.iconnect101.glapp.web.rest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import com.iconnect101.glapp.IntegrationTest;
import com.iconnect101.glapp.domain.CustomerSegment;
import com.iconnect101.glapp.repository.CustomerSegmentRepository;
import java.util.List;
import java.util.Random;
import java.util.concurrent.atomic.AtomicLong;
import javax.persistence.EntityManager;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;

/**
 * Integration tests for the {@link CustomerSegmentResource} REST controller.
 */
@IntegrationTest
@AutoConfigureMockMvc
@WithMockUser
class CustomerSegmentResourceIT {

    private static final String DEFAULT_NAME = "AAAAAAAAAA";
    private static final String UPDATED_NAME = "BBBBBBBBBB";

    private static final String DEFAULT_CODE = "AAAAAAAAAA";
    private static final String UPDATED_CODE = "BBBBBBBBBB";

    private static final String ENTITY_API_URL = "/api/customer-segments";
    private static final String ENTITY_API_URL_ID = ENTITY_API_URL + "/{id}";

    private static Random random = new Random();
    private static AtomicLong count = new AtomicLong(random.nextInt() + (2 * Integer.MAX_VALUE));

    @Autowired
    private CustomerSegmentRepository customerSegmentRepository;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restCustomerSegmentMockMvc;

    private CustomerSegment customerSegment;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static CustomerSegment createEntity(EntityManager em) {
        CustomerSegment customerSegment = new CustomerSegment().name(DEFAULT_NAME).code(DEFAULT_CODE);
        return customerSegment;
    }

    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static CustomerSegment createUpdatedEntity(EntityManager em) {
        CustomerSegment customerSegment = new CustomerSegment().name(UPDATED_NAME).code(UPDATED_CODE);
        return customerSegment;
    }

    @BeforeEach
    public void initTest() {
        customerSegment = createEntity(em);
    }

    @Test
    @Transactional
    void createCustomerSegment() throws Exception {
        int databaseSizeBeforeCreate = customerSegmentRepository.findAll().size();
        // Create the CustomerSegment
        restCustomerSegmentMockMvc
            .perform(
                post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(customerSegment))
            )
            .andExpect(status().isCreated());

        // Validate the CustomerSegment in the database
        List<CustomerSegment> customerSegmentList = customerSegmentRepository.findAll();
        assertThat(customerSegmentList).hasSize(databaseSizeBeforeCreate + 1);
        CustomerSegment testCustomerSegment = customerSegmentList.get(customerSegmentList.size() - 1);
        assertThat(testCustomerSegment.getName()).isEqualTo(DEFAULT_NAME);
        assertThat(testCustomerSegment.getCode()).isEqualTo(DEFAULT_CODE);
    }

    @Test
    @Transactional
    void createCustomerSegmentWithExistingId() throws Exception {
        // Create the CustomerSegment with an existing ID
        customerSegment.setId(1L);

        int databaseSizeBeforeCreate = customerSegmentRepository.findAll().size();

        // An entity with an existing ID cannot be created, so this API call must fail
        restCustomerSegmentMockMvc
            .perform(
                post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(customerSegment))
            )
            .andExpect(status().isBadRequest());

        // Validate the CustomerSegment in the database
        List<CustomerSegment> customerSegmentList = customerSegmentRepository.findAll();
        assertThat(customerSegmentList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    void getAllCustomerSegments() throws Exception {
        // Initialize the database
        customerSegmentRepository.saveAndFlush(customerSegment);

        // Get all the customerSegmentList
        restCustomerSegmentMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(customerSegment.getId().intValue())))
            .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME)))
            .andExpect(jsonPath("$.[*].code").value(hasItem(DEFAULT_CODE)));
    }

    @Test
    @Transactional
    void getCustomerSegment() throws Exception {
        // Initialize the database
        customerSegmentRepository.saveAndFlush(customerSegment);

        // Get the customerSegment
        restCustomerSegmentMockMvc
            .perform(get(ENTITY_API_URL_ID, customerSegment.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(customerSegment.getId().intValue()))
            .andExpect(jsonPath("$.name").value(DEFAULT_NAME))
            .andExpect(jsonPath("$.code").value(DEFAULT_CODE));
    }

    @Test
    @Transactional
    void getNonExistingCustomerSegment() throws Exception {
        // Get the customerSegment
        restCustomerSegmentMockMvc.perform(get(ENTITY_API_URL_ID, Long.MAX_VALUE)).andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    void putNewCustomerSegment() throws Exception {
        // Initialize the database
        customerSegmentRepository.saveAndFlush(customerSegment);

        int databaseSizeBeforeUpdate = customerSegmentRepository.findAll().size();

        // Update the customerSegment
        CustomerSegment updatedCustomerSegment = customerSegmentRepository.findById(customerSegment.getId()).get();
        // Disconnect from session so that the updates on updatedCustomerSegment are not directly saved in db
        em.detach(updatedCustomerSegment);
        updatedCustomerSegment.name(UPDATED_NAME).code(UPDATED_CODE);

        restCustomerSegmentMockMvc
            .perform(
                put(ENTITY_API_URL_ID, updatedCustomerSegment.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(updatedCustomerSegment))
            )
            .andExpect(status().isOk());

        // Validate the CustomerSegment in the database
        List<CustomerSegment> customerSegmentList = customerSegmentRepository.findAll();
        assertThat(customerSegmentList).hasSize(databaseSizeBeforeUpdate);
        CustomerSegment testCustomerSegment = customerSegmentList.get(customerSegmentList.size() - 1);
        assertThat(testCustomerSegment.getName()).isEqualTo(UPDATED_NAME);
        assertThat(testCustomerSegment.getCode()).isEqualTo(UPDATED_CODE);
    }

    @Test
    @Transactional
    void putNonExistingCustomerSegment() throws Exception {
        int databaseSizeBeforeUpdate = customerSegmentRepository.findAll().size();
        customerSegment.setId(count.incrementAndGet());

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restCustomerSegmentMockMvc
            .perform(
                put(ENTITY_API_URL_ID, customerSegment.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(customerSegment))
            )
            .andExpect(status().isBadRequest());

        // Validate the CustomerSegment in the database
        List<CustomerSegment> customerSegmentList = customerSegmentRepository.findAll();
        assertThat(customerSegmentList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithIdMismatchCustomerSegment() throws Exception {
        int databaseSizeBeforeUpdate = customerSegmentRepository.findAll().size();
        customerSegment.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restCustomerSegmentMockMvc
            .perform(
                put(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(customerSegment))
            )
            .andExpect(status().isBadRequest());

        // Validate the CustomerSegment in the database
        List<CustomerSegment> customerSegmentList = customerSegmentRepository.findAll();
        assertThat(customerSegmentList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithMissingIdPathParamCustomerSegment() throws Exception {
        int databaseSizeBeforeUpdate = customerSegmentRepository.findAll().size();
        customerSegment.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restCustomerSegmentMockMvc
            .perform(
                put(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(customerSegment))
            )
            .andExpect(status().isMethodNotAllowed());

        // Validate the CustomerSegment in the database
        List<CustomerSegment> customerSegmentList = customerSegmentRepository.findAll();
        assertThat(customerSegmentList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void partialUpdateCustomerSegmentWithPatch() throws Exception {
        // Initialize the database
        customerSegmentRepository.saveAndFlush(customerSegment);

        int databaseSizeBeforeUpdate = customerSegmentRepository.findAll().size();

        // Update the customerSegment using partial update
        CustomerSegment partialUpdatedCustomerSegment = new CustomerSegment();
        partialUpdatedCustomerSegment.setId(customerSegment.getId());

        restCustomerSegmentMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedCustomerSegment.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedCustomerSegment))
            )
            .andExpect(status().isOk());

        // Validate the CustomerSegment in the database
        List<CustomerSegment> customerSegmentList = customerSegmentRepository.findAll();
        assertThat(customerSegmentList).hasSize(databaseSizeBeforeUpdate);
        CustomerSegment testCustomerSegment = customerSegmentList.get(customerSegmentList.size() - 1);
        assertThat(testCustomerSegment.getName()).isEqualTo(DEFAULT_NAME);
        assertThat(testCustomerSegment.getCode()).isEqualTo(DEFAULT_CODE);
    }

    @Test
    @Transactional
    void fullUpdateCustomerSegmentWithPatch() throws Exception {
        // Initialize the database
        customerSegmentRepository.saveAndFlush(customerSegment);

        int databaseSizeBeforeUpdate = customerSegmentRepository.findAll().size();

        // Update the customerSegment using partial update
        CustomerSegment partialUpdatedCustomerSegment = new CustomerSegment();
        partialUpdatedCustomerSegment.setId(customerSegment.getId());

        partialUpdatedCustomerSegment.name(UPDATED_NAME).code(UPDATED_CODE);

        restCustomerSegmentMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedCustomerSegment.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedCustomerSegment))
            )
            .andExpect(status().isOk());

        // Validate the CustomerSegment in the database
        List<CustomerSegment> customerSegmentList = customerSegmentRepository.findAll();
        assertThat(customerSegmentList).hasSize(databaseSizeBeforeUpdate);
        CustomerSegment testCustomerSegment = customerSegmentList.get(customerSegmentList.size() - 1);
        assertThat(testCustomerSegment.getName()).isEqualTo(UPDATED_NAME);
        assertThat(testCustomerSegment.getCode()).isEqualTo(UPDATED_CODE);
    }

    @Test
    @Transactional
    void patchNonExistingCustomerSegment() throws Exception {
        int databaseSizeBeforeUpdate = customerSegmentRepository.findAll().size();
        customerSegment.setId(count.incrementAndGet());

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restCustomerSegmentMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, customerSegment.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(customerSegment))
            )
            .andExpect(status().isBadRequest());

        // Validate the CustomerSegment in the database
        List<CustomerSegment> customerSegmentList = customerSegmentRepository.findAll();
        assertThat(customerSegmentList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithIdMismatchCustomerSegment() throws Exception {
        int databaseSizeBeforeUpdate = customerSegmentRepository.findAll().size();
        customerSegment.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restCustomerSegmentMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(customerSegment))
            )
            .andExpect(status().isBadRequest());

        // Validate the CustomerSegment in the database
        List<CustomerSegment> customerSegmentList = customerSegmentRepository.findAll();
        assertThat(customerSegmentList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithMissingIdPathParamCustomerSegment() throws Exception {
        int databaseSizeBeforeUpdate = customerSegmentRepository.findAll().size();
        customerSegment.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restCustomerSegmentMockMvc
            .perform(
                patch(ENTITY_API_URL)
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(customerSegment))
            )
            .andExpect(status().isMethodNotAllowed());

        // Validate the CustomerSegment in the database
        List<CustomerSegment> customerSegmentList = customerSegmentRepository.findAll();
        assertThat(customerSegmentList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void deleteCustomerSegment() throws Exception {
        // Initialize the database
        customerSegmentRepository.saveAndFlush(customerSegment);

        int databaseSizeBeforeDelete = customerSegmentRepository.findAll().size();

        // Delete the customerSegment
        restCustomerSegmentMockMvc
            .perform(delete(ENTITY_API_URL_ID, customerSegment.getId()).accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<CustomerSegment> customerSegmentList = customerSegmentRepository.findAll();
        assertThat(customerSegmentList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
