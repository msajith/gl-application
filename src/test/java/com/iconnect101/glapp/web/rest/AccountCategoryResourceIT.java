package com.iconnect101.glapp.web.rest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import com.iconnect101.glapp.IntegrationTest;
import com.iconnect101.glapp.domain.AccountCategory;
import com.iconnect101.glapp.repository.AccountCategoryRepository;
import java.util.List;
import java.util.Random;
import java.util.concurrent.atomic.AtomicLong;
import javax.persistence.EntityManager;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;

/**
 * Integration tests for the {@link AccountCategoryResource} REST controller.
 */
@IntegrationTest
@AutoConfigureMockMvc
@WithMockUser
class AccountCategoryResourceIT {

    private static final String DEFAULT_NAME = "AAAAAAAAAA";
    private static final String UPDATED_NAME = "BBBBBBBBBB";

    private static final String DEFAULT_CODE = "AAAAAAAAAA";
    private static final String UPDATED_CODE = "BBBBBBBBBB";

    private static final String ENTITY_API_URL = "/api/account-categories";
    private static final String ENTITY_API_URL_ID = ENTITY_API_URL + "/{id}";

    private static Random random = new Random();
    private static AtomicLong count = new AtomicLong(random.nextInt() + (2 * Integer.MAX_VALUE));

    @Autowired
    private AccountCategoryRepository accountCategoryRepository;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restAccountCategoryMockMvc;

    private AccountCategory accountCategory;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static AccountCategory createEntity(EntityManager em) {
        AccountCategory accountCategory = new AccountCategory().name(DEFAULT_NAME).code(DEFAULT_CODE);
        return accountCategory;
    }

    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static AccountCategory createUpdatedEntity(EntityManager em) {
        AccountCategory accountCategory = new AccountCategory().name(UPDATED_NAME).code(UPDATED_CODE);
        return accountCategory;
    }

    @BeforeEach
    public void initTest() {
        accountCategory = createEntity(em);
    }

    @Test
    @Transactional
    void createAccountCategory() throws Exception {
        int databaseSizeBeforeCreate = accountCategoryRepository.findAll().size();
        // Create the AccountCategory
        restAccountCategoryMockMvc
            .perform(
                post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(accountCategory))
            )
            .andExpect(status().isCreated());

        // Validate the AccountCategory in the database
        List<AccountCategory> accountCategoryList = accountCategoryRepository.findAll();
        assertThat(accountCategoryList).hasSize(databaseSizeBeforeCreate + 1);
        AccountCategory testAccountCategory = accountCategoryList.get(accountCategoryList.size() - 1);
        assertThat(testAccountCategory.getName()).isEqualTo(DEFAULT_NAME);
        assertThat(testAccountCategory.getCode()).isEqualTo(DEFAULT_CODE);
    }

    @Test
    @Transactional
    void createAccountCategoryWithExistingId() throws Exception {
        // Create the AccountCategory with an existing ID
        accountCategory.setId(1L);

        int databaseSizeBeforeCreate = accountCategoryRepository.findAll().size();

        // An entity with an existing ID cannot be created, so this API call must fail
        restAccountCategoryMockMvc
            .perform(
                post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(accountCategory))
            )
            .andExpect(status().isBadRequest());

        // Validate the AccountCategory in the database
        List<AccountCategory> accountCategoryList = accountCategoryRepository.findAll();
        assertThat(accountCategoryList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    void getAllAccountCategories() throws Exception {
        // Initialize the database
        accountCategoryRepository.saveAndFlush(accountCategory);

        // Get all the accountCategoryList
        restAccountCategoryMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(accountCategory.getId().intValue())))
            .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME)))
            .andExpect(jsonPath("$.[*].code").value(hasItem(DEFAULT_CODE)));
    }

    @Test
    @Transactional
    void getAccountCategory() throws Exception {
        // Initialize the database
        accountCategoryRepository.saveAndFlush(accountCategory);

        // Get the accountCategory
        restAccountCategoryMockMvc
            .perform(get(ENTITY_API_URL_ID, accountCategory.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(accountCategory.getId().intValue()))
            .andExpect(jsonPath("$.name").value(DEFAULT_NAME))
            .andExpect(jsonPath("$.code").value(DEFAULT_CODE));
    }

    @Test
    @Transactional
    void getNonExistingAccountCategory() throws Exception {
        // Get the accountCategory
        restAccountCategoryMockMvc.perform(get(ENTITY_API_URL_ID, Long.MAX_VALUE)).andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    void putNewAccountCategory() throws Exception {
        // Initialize the database
        accountCategoryRepository.saveAndFlush(accountCategory);

        int databaseSizeBeforeUpdate = accountCategoryRepository.findAll().size();

        // Update the accountCategory
        AccountCategory updatedAccountCategory = accountCategoryRepository.findById(accountCategory.getId()).get();
        // Disconnect from session so that the updates on updatedAccountCategory are not directly saved in db
        em.detach(updatedAccountCategory);
        updatedAccountCategory.name(UPDATED_NAME).code(UPDATED_CODE);

        restAccountCategoryMockMvc
            .perform(
                put(ENTITY_API_URL_ID, updatedAccountCategory.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(updatedAccountCategory))
            )
            .andExpect(status().isOk());

        // Validate the AccountCategory in the database
        List<AccountCategory> accountCategoryList = accountCategoryRepository.findAll();
        assertThat(accountCategoryList).hasSize(databaseSizeBeforeUpdate);
        AccountCategory testAccountCategory = accountCategoryList.get(accountCategoryList.size() - 1);
        assertThat(testAccountCategory.getName()).isEqualTo(UPDATED_NAME);
        assertThat(testAccountCategory.getCode()).isEqualTo(UPDATED_CODE);
    }

    @Test
    @Transactional
    void putNonExistingAccountCategory() throws Exception {
        int databaseSizeBeforeUpdate = accountCategoryRepository.findAll().size();
        accountCategory.setId(count.incrementAndGet());

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restAccountCategoryMockMvc
            .perform(
                put(ENTITY_API_URL_ID, accountCategory.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(accountCategory))
            )
            .andExpect(status().isBadRequest());

        // Validate the AccountCategory in the database
        List<AccountCategory> accountCategoryList = accountCategoryRepository.findAll();
        assertThat(accountCategoryList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithIdMismatchAccountCategory() throws Exception {
        int databaseSizeBeforeUpdate = accountCategoryRepository.findAll().size();
        accountCategory.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restAccountCategoryMockMvc
            .perform(
                put(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(accountCategory))
            )
            .andExpect(status().isBadRequest());

        // Validate the AccountCategory in the database
        List<AccountCategory> accountCategoryList = accountCategoryRepository.findAll();
        assertThat(accountCategoryList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithMissingIdPathParamAccountCategory() throws Exception {
        int databaseSizeBeforeUpdate = accountCategoryRepository.findAll().size();
        accountCategory.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restAccountCategoryMockMvc
            .perform(
                put(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(accountCategory))
            )
            .andExpect(status().isMethodNotAllowed());

        // Validate the AccountCategory in the database
        List<AccountCategory> accountCategoryList = accountCategoryRepository.findAll();
        assertThat(accountCategoryList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void partialUpdateAccountCategoryWithPatch() throws Exception {
        // Initialize the database
        accountCategoryRepository.saveAndFlush(accountCategory);

        int databaseSizeBeforeUpdate = accountCategoryRepository.findAll().size();

        // Update the accountCategory using partial update
        AccountCategory partialUpdatedAccountCategory = new AccountCategory();
        partialUpdatedAccountCategory.setId(accountCategory.getId());

        partialUpdatedAccountCategory.code(UPDATED_CODE);

        restAccountCategoryMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedAccountCategory.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedAccountCategory))
            )
            .andExpect(status().isOk());

        // Validate the AccountCategory in the database
        List<AccountCategory> accountCategoryList = accountCategoryRepository.findAll();
        assertThat(accountCategoryList).hasSize(databaseSizeBeforeUpdate);
        AccountCategory testAccountCategory = accountCategoryList.get(accountCategoryList.size() - 1);
        assertThat(testAccountCategory.getName()).isEqualTo(DEFAULT_NAME);
        assertThat(testAccountCategory.getCode()).isEqualTo(UPDATED_CODE);
    }

    @Test
    @Transactional
    void fullUpdateAccountCategoryWithPatch() throws Exception {
        // Initialize the database
        accountCategoryRepository.saveAndFlush(accountCategory);

        int databaseSizeBeforeUpdate = accountCategoryRepository.findAll().size();

        // Update the accountCategory using partial update
        AccountCategory partialUpdatedAccountCategory = new AccountCategory();
        partialUpdatedAccountCategory.setId(accountCategory.getId());

        partialUpdatedAccountCategory.name(UPDATED_NAME).code(UPDATED_CODE);

        restAccountCategoryMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedAccountCategory.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedAccountCategory))
            )
            .andExpect(status().isOk());

        // Validate the AccountCategory in the database
        List<AccountCategory> accountCategoryList = accountCategoryRepository.findAll();
        assertThat(accountCategoryList).hasSize(databaseSizeBeforeUpdate);
        AccountCategory testAccountCategory = accountCategoryList.get(accountCategoryList.size() - 1);
        assertThat(testAccountCategory.getName()).isEqualTo(UPDATED_NAME);
        assertThat(testAccountCategory.getCode()).isEqualTo(UPDATED_CODE);
    }

    @Test
    @Transactional
    void patchNonExistingAccountCategory() throws Exception {
        int databaseSizeBeforeUpdate = accountCategoryRepository.findAll().size();
        accountCategory.setId(count.incrementAndGet());

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restAccountCategoryMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, accountCategory.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(accountCategory))
            )
            .andExpect(status().isBadRequest());

        // Validate the AccountCategory in the database
        List<AccountCategory> accountCategoryList = accountCategoryRepository.findAll();
        assertThat(accountCategoryList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithIdMismatchAccountCategory() throws Exception {
        int databaseSizeBeforeUpdate = accountCategoryRepository.findAll().size();
        accountCategory.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restAccountCategoryMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(accountCategory))
            )
            .andExpect(status().isBadRequest());

        // Validate the AccountCategory in the database
        List<AccountCategory> accountCategoryList = accountCategoryRepository.findAll();
        assertThat(accountCategoryList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithMissingIdPathParamAccountCategory() throws Exception {
        int databaseSizeBeforeUpdate = accountCategoryRepository.findAll().size();
        accountCategory.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restAccountCategoryMockMvc
            .perform(
                patch(ENTITY_API_URL)
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(accountCategory))
            )
            .andExpect(status().isMethodNotAllowed());

        // Validate the AccountCategory in the database
        List<AccountCategory> accountCategoryList = accountCategoryRepository.findAll();
        assertThat(accountCategoryList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void deleteAccountCategory() throws Exception {
        // Initialize the database
        accountCategoryRepository.saveAndFlush(accountCategory);

        int databaseSizeBeforeDelete = accountCategoryRepository.findAll().size();

        // Delete the accountCategory
        restAccountCategoryMockMvc
            .perform(delete(ENTITY_API_URL_ID, accountCategory.getId()).accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<AccountCategory> accountCategoryList = accountCategoryRepository.findAll();
        assertThat(accountCategoryList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
