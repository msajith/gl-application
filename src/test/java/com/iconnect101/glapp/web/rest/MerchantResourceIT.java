package com.iconnect101.glapp.web.rest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import com.iconnect101.glapp.IntegrationTest;
import com.iconnect101.glapp.domain.Merchant;
import com.iconnect101.glapp.domain.enumeration.MerchantStatus;
import com.iconnect101.glapp.repository.MerchantRepository;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.List;
import java.util.Random;
import java.util.concurrent.atomic.AtomicLong;
import javax.persistence.EntityManager;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;

/**
 * Integration tests for the {@link MerchantResource} REST controller.
 */
@IntegrationTest
@AutoConfigureMockMvc
@WithMockUser
class MerchantResourceIT {

    private static final String DEFAULT_MERCHANT_ID = "AAAAAAAAAA";
    private static final String UPDATED_MERCHANT_ID = "BBBBBBBBBB";

    private static final String DEFAULT_MERCHANT_NAME = "AAAAAAAAAA";
    private static final String UPDATED_MERCHANT_NAME = "BBBBBBBBBB";

    private static final String DEFAULT_MERCHANT_CATEGORY = "AAAAAAAAAA";
    private static final String UPDATED_MERCHANT_CATEGORY = "BBBBBBBBBB";

    private static final String DEFAULT_MERCHANT_GL_ACCOUNT_NO = "AAAAAAAAAA";
    private static final String UPDATED_MERCHANT_GL_ACCOUNT_NO = "BBBBBBBBBB";

    private static final String DEFAULT_MERCHANT_BANK_ACCOUNT_NO = "AAAAAAAAAA";
    private static final String UPDATED_MERCHANT_BANK_ACCOUNT_NO = "BBBBBBBBBB";

    private static final String DEFAULT_MERCHANT_BANK_ACCOUNT_CURRENCY = "AAAAAAAAAA";
    private static final String UPDATED_MERCHANT_BANK_ACCOUNT_CURRENCY = "BBBBBBBBBB";

    private static final String DEFAULT_MERCHANT_BANK_CODE = "AAAAAAAAAA";
    private static final String UPDATED_MERCHANT_BANK_CODE = "BBBBBBBBBB";

    private static final String DEFAULT_MERCHANT_BANK_NAME = "AAAAAAAAAA";
    private static final String UPDATED_MERCHANT_BANK_NAME = "BBBBBBBBBB";

    private static final String DEFAULT_MERCHANT_ADDRESS = "AAAAAAAAAA";
    private static final String UPDATED_MERCHANT_ADDRESS = "BBBBBBBBBB";

    private static final String DEFAULT_MERCHANT_CITY = "AAAAAAAAAA";
    private static final String UPDATED_MERCHANT_CITY = "BBBBBBBBBB";

    private static final Instant DEFAULT_CREATED_ON = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_CREATED_ON = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    private static final String DEFAULT_CREATED_BY = "AAAAAAAAAA";
    private static final String UPDATED_CREATED_BY = "BBBBBBBBBB";

    private static final MerchantStatus DEFAULT_MERCHANT_STATAUS = MerchantStatus.ACTIVE;
    private static final MerchantStatus UPDATED_MERCHANT_STATAUS = MerchantStatus.INACTIVE;

    private static final String ENTITY_API_URL = "/api/merchants";
    private static final String ENTITY_API_URL_ID = ENTITY_API_URL + "/{id}";

    private static Random random = new Random();
    private static AtomicLong count = new AtomicLong(random.nextInt() + (2 * Integer.MAX_VALUE));

    @Autowired
    private MerchantRepository merchantRepository;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restMerchantMockMvc;

    private Merchant merchant;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Merchant createEntity(EntityManager em) {
        Merchant merchant = new Merchant()
            .merchantID(DEFAULT_MERCHANT_ID)
            .merchantName(DEFAULT_MERCHANT_NAME)
            .merchantCategory(DEFAULT_MERCHANT_CATEGORY)
            .merchantGLAccountNo(DEFAULT_MERCHANT_GL_ACCOUNT_NO)
            .merchantBankAccountNo(DEFAULT_MERCHANT_BANK_ACCOUNT_NO)
            .merchantBankAccountCurrency(DEFAULT_MERCHANT_BANK_ACCOUNT_CURRENCY)
            .merchantBankCode(DEFAULT_MERCHANT_BANK_CODE)
            .merchantBankName(DEFAULT_MERCHANT_BANK_NAME)
            .merchantAddress(DEFAULT_MERCHANT_ADDRESS)
            .merchantCity(DEFAULT_MERCHANT_CITY)
            .createdOn(DEFAULT_CREATED_ON)
            .createdBy(DEFAULT_CREATED_BY)
            .merchantStataus(DEFAULT_MERCHANT_STATAUS);
        return merchant;
    }

    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Merchant createUpdatedEntity(EntityManager em) {
        Merchant merchant = new Merchant()
            .merchantID(UPDATED_MERCHANT_ID)
            .merchantName(UPDATED_MERCHANT_NAME)
            .merchantCategory(UPDATED_MERCHANT_CATEGORY)
            .merchantGLAccountNo(UPDATED_MERCHANT_GL_ACCOUNT_NO)
            .merchantBankAccountNo(UPDATED_MERCHANT_BANK_ACCOUNT_NO)
            .merchantBankAccountCurrency(UPDATED_MERCHANT_BANK_ACCOUNT_CURRENCY)
            .merchantBankCode(UPDATED_MERCHANT_BANK_CODE)
            .merchantBankName(UPDATED_MERCHANT_BANK_NAME)
            .merchantAddress(UPDATED_MERCHANT_ADDRESS)
            .merchantCity(UPDATED_MERCHANT_CITY)
            .createdOn(UPDATED_CREATED_ON)
            .createdBy(UPDATED_CREATED_BY)
            .merchantStataus(UPDATED_MERCHANT_STATAUS);
        return merchant;
    }

    @BeforeEach
    public void initTest() {
        merchant = createEntity(em);
    }

    @Test
    @Transactional
    void createMerchant() throws Exception {
        int databaseSizeBeforeCreate = merchantRepository.findAll().size();
        // Create the Merchant
        restMerchantMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(merchant)))
            .andExpect(status().isCreated());

        // Validate the Merchant in the database
        List<Merchant> merchantList = merchantRepository.findAll();
        assertThat(merchantList).hasSize(databaseSizeBeforeCreate + 1);
        Merchant testMerchant = merchantList.get(merchantList.size() - 1);
        assertThat(testMerchant.getMerchantID()).isEqualTo(DEFAULT_MERCHANT_ID);
        assertThat(testMerchant.getMerchantName()).isEqualTo(DEFAULT_MERCHANT_NAME);
        assertThat(testMerchant.getMerchantCategory()).isEqualTo(DEFAULT_MERCHANT_CATEGORY);
        assertThat(testMerchant.getMerchantGLAccountNo()).isEqualTo(DEFAULT_MERCHANT_GL_ACCOUNT_NO);
        assertThat(testMerchant.getMerchantBankAccountNo()).isEqualTo(DEFAULT_MERCHANT_BANK_ACCOUNT_NO);
        assertThat(testMerchant.getMerchantBankAccountCurrency()).isEqualTo(DEFAULT_MERCHANT_BANK_ACCOUNT_CURRENCY);
        assertThat(testMerchant.getMerchantBankCode()).isEqualTo(DEFAULT_MERCHANT_BANK_CODE);
        assertThat(testMerchant.getMerchantBankName()).isEqualTo(DEFAULT_MERCHANT_BANK_NAME);
        assertThat(testMerchant.getMerchantAddress()).isEqualTo(DEFAULT_MERCHANT_ADDRESS);
        assertThat(testMerchant.getMerchantCity()).isEqualTo(DEFAULT_MERCHANT_CITY);
        assertThat(testMerchant.getCreatedOn()).isEqualTo(DEFAULT_CREATED_ON);
        assertThat(testMerchant.getCreatedBy()).isEqualTo(DEFAULT_CREATED_BY);
        assertThat(testMerchant.getMerchantStataus()).isEqualTo(DEFAULT_MERCHANT_STATAUS);
    }

    @Test
    @Transactional
    void createMerchantWithExistingId() throws Exception {
        // Create the Merchant with an existing ID
        merchant.setId(1L);

        int databaseSizeBeforeCreate = merchantRepository.findAll().size();

        // An entity with an existing ID cannot be created, so this API call must fail
        restMerchantMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(merchant)))
            .andExpect(status().isBadRequest());

        // Validate the Merchant in the database
        List<Merchant> merchantList = merchantRepository.findAll();
        assertThat(merchantList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    void getAllMerchants() throws Exception {
        // Initialize the database
        merchantRepository.saveAndFlush(merchant);

        // Get all the merchantList
        restMerchantMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(merchant.getId().intValue())))
            .andExpect(jsonPath("$.[*].merchantID").value(hasItem(DEFAULT_MERCHANT_ID)))
            .andExpect(jsonPath("$.[*].merchantName").value(hasItem(DEFAULT_MERCHANT_NAME)))
            .andExpect(jsonPath("$.[*].merchantCategory").value(hasItem(DEFAULT_MERCHANT_CATEGORY)))
            .andExpect(jsonPath("$.[*].merchantGLAccountNo").value(hasItem(DEFAULT_MERCHANT_GL_ACCOUNT_NO)))
            .andExpect(jsonPath("$.[*].merchantBankAccountNo").value(hasItem(DEFAULT_MERCHANT_BANK_ACCOUNT_NO)))
            .andExpect(jsonPath("$.[*].merchantBankAccountCurrency").value(hasItem(DEFAULT_MERCHANT_BANK_ACCOUNT_CURRENCY)))
            .andExpect(jsonPath("$.[*].merchantBankCode").value(hasItem(DEFAULT_MERCHANT_BANK_CODE)))
            .andExpect(jsonPath("$.[*].merchantBankName").value(hasItem(DEFAULT_MERCHANT_BANK_NAME)))
            .andExpect(jsonPath("$.[*].merchantAddress").value(hasItem(DEFAULT_MERCHANT_ADDRESS)))
            .andExpect(jsonPath("$.[*].merchantCity").value(hasItem(DEFAULT_MERCHANT_CITY)))
            .andExpect(jsonPath("$.[*].createdOn").value(hasItem(DEFAULT_CREATED_ON.toString())))
            .andExpect(jsonPath("$.[*].createdBy").value(hasItem(DEFAULT_CREATED_BY)))
            .andExpect(jsonPath("$.[*].merchantStataus").value(hasItem(DEFAULT_MERCHANT_STATAUS.toString())));
    }

    @Test
    @Transactional
    void getMerchant() throws Exception {
        // Initialize the database
        merchantRepository.saveAndFlush(merchant);

        // Get the merchant
        restMerchantMockMvc
            .perform(get(ENTITY_API_URL_ID, merchant.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(merchant.getId().intValue()))
            .andExpect(jsonPath("$.merchantID").value(DEFAULT_MERCHANT_ID))
            .andExpect(jsonPath("$.merchantName").value(DEFAULT_MERCHANT_NAME))
            .andExpect(jsonPath("$.merchantCategory").value(DEFAULT_MERCHANT_CATEGORY))
            .andExpect(jsonPath("$.merchantGLAccountNo").value(DEFAULT_MERCHANT_GL_ACCOUNT_NO))
            .andExpect(jsonPath("$.merchantBankAccountNo").value(DEFAULT_MERCHANT_BANK_ACCOUNT_NO))
            .andExpect(jsonPath("$.merchantBankAccountCurrency").value(DEFAULT_MERCHANT_BANK_ACCOUNT_CURRENCY))
            .andExpect(jsonPath("$.merchantBankCode").value(DEFAULT_MERCHANT_BANK_CODE))
            .andExpect(jsonPath("$.merchantBankName").value(DEFAULT_MERCHANT_BANK_NAME))
            .andExpect(jsonPath("$.merchantAddress").value(DEFAULT_MERCHANT_ADDRESS))
            .andExpect(jsonPath("$.merchantCity").value(DEFAULT_MERCHANT_CITY))
            .andExpect(jsonPath("$.createdOn").value(DEFAULT_CREATED_ON.toString()))
            .andExpect(jsonPath("$.createdBy").value(DEFAULT_CREATED_BY))
            .andExpect(jsonPath("$.merchantStataus").value(DEFAULT_MERCHANT_STATAUS.toString()));
    }

    @Test
    @Transactional
    void getNonExistingMerchant() throws Exception {
        // Get the merchant
        restMerchantMockMvc.perform(get(ENTITY_API_URL_ID, Long.MAX_VALUE)).andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    void putNewMerchant() throws Exception {
        // Initialize the database
        merchantRepository.saveAndFlush(merchant);

        int databaseSizeBeforeUpdate = merchantRepository.findAll().size();

        // Update the merchant
        Merchant updatedMerchant = merchantRepository.findById(merchant.getId()).get();
        // Disconnect from session so that the updates on updatedMerchant are not directly saved in db
        em.detach(updatedMerchant);
        updatedMerchant
            .merchantID(UPDATED_MERCHANT_ID)
            .merchantName(UPDATED_MERCHANT_NAME)
            .merchantCategory(UPDATED_MERCHANT_CATEGORY)
            .merchantGLAccountNo(UPDATED_MERCHANT_GL_ACCOUNT_NO)
            .merchantBankAccountNo(UPDATED_MERCHANT_BANK_ACCOUNT_NO)
            .merchantBankAccountCurrency(UPDATED_MERCHANT_BANK_ACCOUNT_CURRENCY)
            .merchantBankCode(UPDATED_MERCHANT_BANK_CODE)
            .merchantBankName(UPDATED_MERCHANT_BANK_NAME)
            .merchantAddress(UPDATED_MERCHANT_ADDRESS)
            .merchantCity(UPDATED_MERCHANT_CITY)
            .createdOn(UPDATED_CREATED_ON)
            .createdBy(UPDATED_CREATED_BY)
            .merchantStataus(UPDATED_MERCHANT_STATAUS);

        restMerchantMockMvc
            .perform(
                put(ENTITY_API_URL_ID, updatedMerchant.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(updatedMerchant))
            )
            .andExpect(status().isOk());

        // Validate the Merchant in the database
        List<Merchant> merchantList = merchantRepository.findAll();
        assertThat(merchantList).hasSize(databaseSizeBeforeUpdate);
        Merchant testMerchant = merchantList.get(merchantList.size() - 1);
        assertThat(testMerchant.getMerchantID()).isEqualTo(UPDATED_MERCHANT_ID);
        assertThat(testMerchant.getMerchantName()).isEqualTo(UPDATED_MERCHANT_NAME);
        assertThat(testMerchant.getMerchantCategory()).isEqualTo(UPDATED_MERCHANT_CATEGORY);
        assertThat(testMerchant.getMerchantGLAccountNo()).isEqualTo(UPDATED_MERCHANT_GL_ACCOUNT_NO);
        assertThat(testMerchant.getMerchantBankAccountNo()).isEqualTo(UPDATED_MERCHANT_BANK_ACCOUNT_NO);
        assertThat(testMerchant.getMerchantBankAccountCurrency()).isEqualTo(UPDATED_MERCHANT_BANK_ACCOUNT_CURRENCY);
        assertThat(testMerchant.getMerchantBankCode()).isEqualTo(UPDATED_MERCHANT_BANK_CODE);
        assertThat(testMerchant.getMerchantBankName()).isEqualTo(UPDATED_MERCHANT_BANK_NAME);
        assertThat(testMerchant.getMerchantAddress()).isEqualTo(UPDATED_MERCHANT_ADDRESS);
        assertThat(testMerchant.getMerchantCity()).isEqualTo(UPDATED_MERCHANT_CITY);
        assertThat(testMerchant.getCreatedOn()).isEqualTo(UPDATED_CREATED_ON);
        assertThat(testMerchant.getCreatedBy()).isEqualTo(UPDATED_CREATED_BY);
        assertThat(testMerchant.getMerchantStataus()).isEqualTo(UPDATED_MERCHANT_STATAUS);
    }

    @Test
    @Transactional
    void putNonExistingMerchant() throws Exception {
        int databaseSizeBeforeUpdate = merchantRepository.findAll().size();
        merchant.setId(count.incrementAndGet());

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restMerchantMockMvc
            .perform(
                put(ENTITY_API_URL_ID, merchant.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(merchant))
            )
            .andExpect(status().isBadRequest());

        // Validate the Merchant in the database
        List<Merchant> merchantList = merchantRepository.findAll();
        assertThat(merchantList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithIdMismatchMerchant() throws Exception {
        int databaseSizeBeforeUpdate = merchantRepository.findAll().size();
        merchant.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restMerchantMockMvc
            .perform(
                put(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(merchant))
            )
            .andExpect(status().isBadRequest());

        // Validate the Merchant in the database
        List<Merchant> merchantList = merchantRepository.findAll();
        assertThat(merchantList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithMissingIdPathParamMerchant() throws Exception {
        int databaseSizeBeforeUpdate = merchantRepository.findAll().size();
        merchant.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restMerchantMockMvc
            .perform(put(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(merchant)))
            .andExpect(status().isMethodNotAllowed());

        // Validate the Merchant in the database
        List<Merchant> merchantList = merchantRepository.findAll();
        assertThat(merchantList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void partialUpdateMerchantWithPatch() throws Exception {
        // Initialize the database
        merchantRepository.saveAndFlush(merchant);

        int databaseSizeBeforeUpdate = merchantRepository.findAll().size();

        // Update the merchant using partial update
        Merchant partialUpdatedMerchant = new Merchant();
        partialUpdatedMerchant.setId(merchant.getId());

        partialUpdatedMerchant
            .merchantName(UPDATED_MERCHANT_NAME)
            .merchantCategory(UPDATED_MERCHANT_CATEGORY)
            .merchantGLAccountNo(UPDATED_MERCHANT_GL_ACCOUNT_NO)
            .merchantBankCode(UPDATED_MERCHANT_BANK_CODE)
            .merchantAddress(UPDATED_MERCHANT_ADDRESS);

        restMerchantMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedMerchant.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedMerchant))
            )
            .andExpect(status().isOk());

        // Validate the Merchant in the database
        List<Merchant> merchantList = merchantRepository.findAll();
        assertThat(merchantList).hasSize(databaseSizeBeforeUpdate);
        Merchant testMerchant = merchantList.get(merchantList.size() - 1);
        assertThat(testMerchant.getMerchantID()).isEqualTo(DEFAULT_MERCHANT_ID);
        assertThat(testMerchant.getMerchantName()).isEqualTo(UPDATED_MERCHANT_NAME);
        assertThat(testMerchant.getMerchantCategory()).isEqualTo(UPDATED_MERCHANT_CATEGORY);
        assertThat(testMerchant.getMerchantGLAccountNo()).isEqualTo(UPDATED_MERCHANT_GL_ACCOUNT_NO);
        assertThat(testMerchant.getMerchantBankAccountNo()).isEqualTo(DEFAULT_MERCHANT_BANK_ACCOUNT_NO);
        assertThat(testMerchant.getMerchantBankAccountCurrency()).isEqualTo(DEFAULT_MERCHANT_BANK_ACCOUNT_CURRENCY);
        assertThat(testMerchant.getMerchantBankCode()).isEqualTo(UPDATED_MERCHANT_BANK_CODE);
        assertThat(testMerchant.getMerchantBankName()).isEqualTo(DEFAULT_MERCHANT_BANK_NAME);
        assertThat(testMerchant.getMerchantAddress()).isEqualTo(UPDATED_MERCHANT_ADDRESS);
        assertThat(testMerchant.getMerchantCity()).isEqualTo(DEFAULT_MERCHANT_CITY);
        assertThat(testMerchant.getCreatedOn()).isEqualTo(DEFAULT_CREATED_ON);
        assertThat(testMerchant.getCreatedBy()).isEqualTo(DEFAULT_CREATED_BY);
        assertThat(testMerchant.getMerchantStataus()).isEqualTo(DEFAULT_MERCHANT_STATAUS);
    }

    @Test
    @Transactional
    void fullUpdateMerchantWithPatch() throws Exception {
        // Initialize the database
        merchantRepository.saveAndFlush(merchant);

        int databaseSizeBeforeUpdate = merchantRepository.findAll().size();

        // Update the merchant using partial update
        Merchant partialUpdatedMerchant = new Merchant();
        partialUpdatedMerchant.setId(merchant.getId());

        partialUpdatedMerchant
            .merchantID(UPDATED_MERCHANT_ID)
            .merchantName(UPDATED_MERCHANT_NAME)
            .merchantCategory(UPDATED_MERCHANT_CATEGORY)
            .merchantGLAccountNo(UPDATED_MERCHANT_GL_ACCOUNT_NO)
            .merchantBankAccountNo(UPDATED_MERCHANT_BANK_ACCOUNT_NO)
            .merchantBankAccountCurrency(UPDATED_MERCHANT_BANK_ACCOUNT_CURRENCY)
            .merchantBankCode(UPDATED_MERCHANT_BANK_CODE)
            .merchantBankName(UPDATED_MERCHANT_BANK_NAME)
            .merchantAddress(UPDATED_MERCHANT_ADDRESS)
            .merchantCity(UPDATED_MERCHANT_CITY)
            .createdOn(UPDATED_CREATED_ON)
            .createdBy(UPDATED_CREATED_BY)
            .merchantStataus(UPDATED_MERCHANT_STATAUS);

        restMerchantMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedMerchant.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedMerchant))
            )
            .andExpect(status().isOk());

        // Validate the Merchant in the database
        List<Merchant> merchantList = merchantRepository.findAll();
        assertThat(merchantList).hasSize(databaseSizeBeforeUpdate);
        Merchant testMerchant = merchantList.get(merchantList.size() - 1);
        assertThat(testMerchant.getMerchantID()).isEqualTo(UPDATED_MERCHANT_ID);
        assertThat(testMerchant.getMerchantName()).isEqualTo(UPDATED_MERCHANT_NAME);
        assertThat(testMerchant.getMerchantCategory()).isEqualTo(UPDATED_MERCHANT_CATEGORY);
        assertThat(testMerchant.getMerchantGLAccountNo()).isEqualTo(UPDATED_MERCHANT_GL_ACCOUNT_NO);
        assertThat(testMerchant.getMerchantBankAccountNo()).isEqualTo(UPDATED_MERCHANT_BANK_ACCOUNT_NO);
        assertThat(testMerchant.getMerchantBankAccountCurrency()).isEqualTo(UPDATED_MERCHANT_BANK_ACCOUNT_CURRENCY);
        assertThat(testMerchant.getMerchantBankCode()).isEqualTo(UPDATED_MERCHANT_BANK_CODE);
        assertThat(testMerchant.getMerchantBankName()).isEqualTo(UPDATED_MERCHANT_BANK_NAME);
        assertThat(testMerchant.getMerchantAddress()).isEqualTo(UPDATED_MERCHANT_ADDRESS);
        assertThat(testMerchant.getMerchantCity()).isEqualTo(UPDATED_MERCHANT_CITY);
        assertThat(testMerchant.getCreatedOn()).isEqualTo(UPDATED_CREATED_ON);
        assertThat(testMerchant.getCreatedBy()).isEqualTo(UPDATED_CREATED_BY);
        assertThat(testMerchant.getMerchantStataus()).isEqualTo(UPDATED_MERCHANT_STATAUS);
    }

    @Test
    @Transactional
    void patchNonExistingMerchant() throws Exception {
        int databaseSizeBeforeUpdate = merchantRepository.findAll().size();
        merchant.setId(count.incrementAndGet());

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restMerchantMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, merchant.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(merchant))
            )
            .andExpect(status().isBadRequest());

        // Validate the Merchant in the database
        List<Merchant> merchantList = merchantRepository.findAll();
        assertThat(merchantList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithIdMismatchMerchant() throws Exception {
        int databaseSizeBeforeUpdate = merchantRepository.findAll().size();
        merchant.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restMerchantMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(merchant))
            )
            .andExpect(status().isBadRequest());

        // Validate the Merchant in the database
        List<Merchant> merchantList = merchantRepository.findAll();
        assertThat(merchantList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithMissingIdPathParamMerchant() throws Exception {
        int databaseSizeBeforeUpdate = merchantRepository.findAll().size();
        merchant.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restMerchantMockMvc
            .perform(patch(ENTITY_API_URL).contentType("application/merge-patch+json").content(TestUtil.convertObjectToJsonBytes(merchant)))
            .andExpect(status().isMethodNotAllowed());

        // Validate the Merchant in the database
        List<Merchant> merchantList = merchantRepository.findAll();
        assertThat(merchantList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void deleteMerchant() throws Exception {
        // Initialize the database
        merchantRepository.saveAndFlush(merchant);

        int databaseSizeBeforeDelete = merchantRepository.findAll().size();

        // Delete the merchant
        restMerchantMockMvc
            .perform(delete(ENTITY_API_URL_ID, merchant.getId()).accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<Merchant> merchantList = merchantRepository.findAll();
        assertThat(merchantList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
