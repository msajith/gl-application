--
-- PostgreSQL database dump
--

-- Dumped from database version 13.2
-- Dumped by pg_dump version 13.2

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- Data for Name: customer; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO public.customer (id, cif, first_name, middle_name, last_name, name_in_local_lang, telephone, mobile, email, created_on, created_by, customer_status, nationality_id, customer_type_id, customer_segment_id) VALUES (1002, 'R-75971234654', 'Trang', 'Varghese', 'K', 'Trang K', '02-2323231', '055-2010090', 'trang@gmail.com', '2019-04-24 02:46:00', 'Ahmed Farook', 'ACTIVE', 32, 1, 1);
INSERT INTO public.customer (id, cif, first_name, middle_name, last_name, name_in_local_lang, telephone, mobile, email, created_on, created_by, customer_status, nationality_id, customer_type_id, customer_segment_id) VALUES (1003, 'R-90871233434', 'Sam', 'M', 'Varghese', 'Sam Varghese', '02-26456098', '055-8764267', 'sam@gmail.com', '2019-04-24 02:46:00', 'Ahmed Farook', 'ACTIVE', 32, 1, 1);
INSERT INTO public.customer (id, cif, first_name, middle_name, last_name, name_in_local_lang, telephone, mobile, email, created_on, created_by, customer_status, nationality_id, customer_type_id, customer_segment_id) VALUES (1001, 'C-781-90897766', 'South African', 'Airlines', 'PJSC', 'South African Airlines PJSC', '02-1293567', '055-1010090', 'accounts@southafricanair.com', '2019-04-24 02:46:00', 'Ahmed Farook', 'ACTIVE', 32, 2, 2);
INSERT INTO public.customer (id, cif, first_name, middle_name, last_name, name_in_local_lang, telephone, mobile, email, created_on, created_by, customer_status, nationality_id, customer_type_id, customer_segment_id) VALUES (5000, 'G-99999999991', 'Back office', 'Operations', 'Sub GL', 'Operations Sub GL Accounts', '02-3434343', '055-4545454', 'backoffice@gmail.com', '2019-04-24 02:46:00', 'Ahmed Farook', 'ACTIVE', 32, 2, 2);


--
-- PostgreSQL database dump complete
--

